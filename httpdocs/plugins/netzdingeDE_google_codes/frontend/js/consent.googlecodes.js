document.addEventListener('consent.ready', function(e) {
	giveConsentGC(e.detail);
});
document.addEventListener('consent.updated', function(e) {
	giveConsentGC(e.detail);
});
function giveConsentGC(detail) {
	if (detail !== null && typeof detail.gc_analytics !== 'undefined') {
		if (typeof gtag !== 'undefined') {
			if (detail.gc_analytics === true) {
				gtag('consent', 'update', {
				  //ad_storage: 'denied',
				  analytics_storage: 'granted'
				});
				console.log('Consent für Analytics (gtag) gewährt');
			} else {
				gtag('consent', 'update', {
				  //ad_storage: 'denied',
				  analytics_storage: 'denied'
				});
				console.log('Consent für Analytics (gtag) entzogen');
			}
		}
	}
	if (detail !== null && typeof detail.gc_adwords !== 'undefined') {
		if (typeof gtag !== 'undefined') {
			if (detail.gc_adwords === true) {
				gtag('consent', 'update', {
				  //ad_storage: 'denied',
				  ad_storage: 'granted'
				});
				gtag('set', 'ads_data_redaction', false);
				console.log('Consent für Adwords gewährt');
			} else {
				gtag('consent', 'update', {
				  //ad_storage: 'denied',
				  ad_storage: 'denied'
				});
				gtag('set', 'ads_data_redaction', true);
				console.log('Consent für Adwords entzogen');

			}
		}
	}
	if (detail !== null && typeof detail.gc_universal_analytics !== 'undefined') {
		if (detail.gc_universal_analytics === true) {
			$('head script[data-gc-cmp-id="analytics"]').each(function (index) {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.innerHTML = $(this).text();
				document.head.appendChild(script);
			});
			$('body script[data-gc-cmp-id="analytics"]').each(function (index) {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.innerHTML = $(this).text();
				document.body.appendChild(script);
			});
			console.log('Consent für Universal Analytics gewährt');
		} else {
			console.log('Consent für Universal Analytics entzogen');
		}
	}
}