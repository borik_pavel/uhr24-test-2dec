<?php declare(strict_types=1);

namespace Plugin\hb_legaltext\Migrations;

use JTL\Plugin\Migration;
use JTL\Update\IMigration;

/**
 * Class Migration20200616000000
 * @package Plugin\hb_legaltext\Migrations
 */
class Migration20200616000000 extends Migration implements IMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `xplugin_hb_legaltext` (
                  `id` int UNSIGNED NOT NULL,
                  `did` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
                  `mode` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
                  `active` TINYINT UNSIGNED NOT NULL,
                  `updated` DATETIME,
                  `updated_state` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
                  `name` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
                  `helper_name` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
                  `type` TINYINT UNSIGNED NOT NULL,
                  PRIMARY KEY (`id`)
            )  ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );

        $this->execute(
            "INSERT INTO `xplugin_hb_legaltext` (`id`, `did`, `mode`, `active`, `name`, `helper_name`, `type`) VALUES
                (1, '12766C46A8A', 'text', 0, 'Allgemeine Geschäftsbedingungen', 'AGB', 0),
                (2, '12766C46A8A', 'html', 0, 'Allgemeine Geschäftsbedingungen', 'AGB', 0),
                (3, '12766C53647', 'text', 0, 'Widerrufsrecht', 'WRB', 0),
                (4, '12766C53647', 'html', 0, 'Widerrufsrecht', 'WRB', 0),
                (5, '160DEDA9674', 'text', 0, 'Datenschutzerklärung', 'DSE', 0),
                (6, '160DEDA9674', 'html', 0, 'Datenschutzerklärung', 'DSE', 0),
                (7, '1293C20B491', 'html', 0, 'Impressum', 'Impressum', 27),
                (8, '134CBB4D101', 'html', 0, 'Batteriehinweise', 'Batteriehinweise', 30)"
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->doDeleteData()) {
            $this->execute('DROP TABLE IF EXISTS `xplugin_hb_legaltext`');
        }
    }
}
