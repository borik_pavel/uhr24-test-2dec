<?php declare(strict_types=1);

namespace Plugin\hb_legaltext;

class LegalTextHelper
{
    /**
     * LegalTextHelper constructor.
     */
    private function __construct() {}

    /**
     * Return mode of requested body.
     *
     * @param object $legalText
     * @return string
     */
    public static function getMode($legalText) : string
    {
        if (isset($legalText->mode) && $legalText->mode === 'html') {
            return 'classes';
        }

        return 'plain';
    }

    /**
     * Return bootstrap 4 info alert.
     * Todo: Maybe use JTL notify system.
     *
     * @param string $message
     * @return string
     */
    public static function getInfoAlert(string $message) : string
    {
        return '<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' . $message . '</div>';
    }
}
