{if !empty($articles)}
{if empty($type)}{assign var="type" value="gallery"}{/if}
{if empty($rowItems)}{assign var="rowItems" value=3}{/if}
{assign var="colClass" value="col-xs-6 col-lg-4"}
{if $rowItems eq 1}{assign var="colClass" value="col-xs-12"}{/if}
{if $rowItems eq 2}{assign var="colClass" value="col-xs-6"}{/if}
{if $rowItems eq 3}{assign var="colClass" value="col-xs-6 col-lg-4"}{/if}
{if $rowItems eq 4}{assign var="colClass" value="col-xs-6 col-lg-3"}{/if}
{if $rowItems eq 6}{assign var="colClass" value="col-xs-6 col-sm-4 col-md-3 col-lg-2"}{/if}
{assign var="listingGuid" value=1|mt_rand:9999}

<div class="kk-int-evo-article-listing kk-int-evo-article-listing-{$listingGuid}">
	<div class="row {if $type !== 'list'}row-eq-height row-eq-img-height{/if} {$type}" itemprop="mainEntity" itemscope itemtype="http://schema.org/ItemList">
	    {foreach name=article from=$articles item=article}
			<div class="product-wrapper {if $type === 'gallery'}{$colClass}{else if $type === 'list'}col-xs-12{/if}" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
	            <meta itemprop="position" content="{$smarty.foreach.articles.iteration}">
	            {if $type === 'list'}
	                {include file='productlist/item_list.tpl' tplscope=$type Artikel=$article}
	            {else}
	                {include file='productlist/item_box.tpl' tplscope=$type class='thumbnail' Artikel=$article}
	            {/if}
	        </div>
	    {/foreach}
	</div>
</div>

<script type="text/javascript">
DropperFramework.init('.kk-int-evo-article-listing-{$listingGuid}', function(drop) {ldelim}{literal}

	// we need evo class to work
	if (!$ || !$.evo) return;

	// when this listing becomes visible, we want to load images and set the height
	drop.visible(function() {

		// remove all existing event handlers that might have been
		// bound to the article stuff already
		setTimeout(function() {
		
			// unattach all registered actions
			drop.$.find("*").addBack().off();

			// inits images, implements autoheight and registers preview buttons
			if (typeof $.evo.imagebox === 'function') {
				$.evo.imagebox(drop.$);
			}
			
			// autoheight
			if (typeof $.evo.autoheight === 'function') $.evo.autoheight();
	
			// register preview actions
			if (typeof $.evo.article === 'function') {
				var evoArticle = $.evo.article();
				if (typeof(evoArticle.register) === 'function') {
					evoArticle.register(drop.$);
				}
			}
		}, 100);

	});

{/literal}{rdelim});
</script>
{/if}