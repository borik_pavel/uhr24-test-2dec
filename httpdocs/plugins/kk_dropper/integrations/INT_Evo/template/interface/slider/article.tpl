{if kkDropperHelper::isModern()}
	{* We do not support lazy loading of images. They are normally handled by the slick slider in EVO for Shop 5. Get rid of lazy sources *}
	{include file="productlist/item_slider.tpl" Artikel=$article tplscope='slider' assign='evoArticleDOM'}
	{$evoArticleDOM|replace:'data-lazy=':'src='}
{else}
	{include file="productlist/item_slider.tpl" Artikel=$article tplscope='slider'}
{/if}