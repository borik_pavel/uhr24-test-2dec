# Dokumentation

Dropper Template Integration für das TINY Template (JTL Shop 3) und davon abgeleiteten Templates.

# Changelog

**1.0.1 - 05.03.2018**

- Unterstützung für Dropper 100.15

**1.0.0 - 01.03.2018**

- Initiales Release
