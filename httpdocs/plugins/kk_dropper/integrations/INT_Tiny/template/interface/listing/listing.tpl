{if !empty($articles)}
{if empty($type)}{assign var="type" value="gallery"}{/if}
{if empty($rowItems)}{assign var="rowItems" value=3}{/if}
{assign var="listingGuid" value=1|mt_rand:9999}
{assign var="idGuid" value="kk-int-tiny-listing-"|cat:$listingGuid|cat:"-"}

<div class="kk-int-tiny-article-listing kk-int-tiny-article-listing-{$listingGuid}">

	<ul class="styled_view">
		{foreach name=articles from=$articles item=article}
		<li class="{$type}">
			<div class="article_wrapper">
				<h3>
					<a href="{$article->cURL}">{$article->cName}</a>
				</h3>
				
				{if isset($article->fDurchschnittsBewertung) && $article->fDurchschnittsBewertung > 0}
					<span class="stars p{$article->fDurchschnittsBewertung|replace:'.':'_'}"></span>
				{/if}
				 
				<!-- image -->
				<div class="article_image">
					<div class="image_overlay_wrapper" id="{$idGuid}image_drag_article{$article->kArtikel}">
						<div class="image_overlay" id="{$idGuid}overlay{$article->kArtikel}"></div>
						<a href="{$article->cURL}" {if $Einstellungen.template.articleoverview.article_image_preview == 'Y'}class="image_preview"{/if} ref="{$article->Bilder[0]->cPfadNormal}">
							{counter assign=imgcounter print=0}
							<img src="{$article->cVorschaubild}" alt="{$article->Bilder[0]->cAltAttribut|strip_tags|escape:"quotes"|truncate:60}" id="{$idGuid}image{$article->kArtikel}_{$imgcounter}" class="image" />
						</a>
						{if isset($article->oSuchspecialBild)}
							<script type="text/javascript">
								set_overlay('#{$idGuid}image{$article->kArtikel}_{$imgcounter}', '{$article->oSuchspecialBild->nPosition}', '{$article->oSuchspecialBild->nMargin}', '{$article->oSuchspecialBild->cPfadKlein}');
							</script>
						{/if}
					</div>
				</div>
				
				<form id="{$idGuid}buy_form{$article->kArtikel}" action="navi.php" method="post">
					{$jtl_token}
					<fieldset class="outer">
						<input type="hidden" name="a" value="{$article->kArtikel}" />
						<input type="hidden" name="wke" value="1" />
						<input type="hidden" name="overview" value="1" />
						
						<!-- article informationen -->      
						<div class="article_info_wrapper">
							<ul class="article_info">
								{assign var=anzeige value=$Einstellungen.artikeluebersicht.artikeluebersicht_lagerbestandsanzeige}
								{if $article->nErscheinendesProdukt}
										<li>{lang key="productAvailable" section="global"}: <strong>{$article->Erscheinungsdatum_de}</strong></li>
									{if $Einstellungen.global.global_erscheinende_kaeuflich=="Y"}
										<li><strong>{lang key="preorderPossible" section="global"}</strong></li>
									{/if}
								{elseif $anzeige != "nichts" && $article->cLagerBeachten == "Y" && ($article->cLagerKleinerNull == "N" || $Einstellungen.artikeluebersicht.artikeluebersicht_lagerbestandanzeige_anzeigen == 'U') && $article->fLagerbestand <= 0 && $article->fZulauf > 0 && isset($article->dZulaufDatum_de)}
									{assign var=cZulauf value=$article->fZulauf|cat:":::"|cat:$article->dZulaufDatum_de}
									<li class="clean popover">
										<span class="signal_image a1">{lang key="productInflowing" section="productDetails" printf=$cZulauf}</span>
										{include file="tpl_inc/artikel_warenlager.tpl"}
									</li>
								{elseif $anzeige != "nichts" && $Einstellungen.artikeluebersicht.artikeluebersicht_lagerbestandanzeige_anzeigen != 'N' && $article->cLagerBeachten == "Y" && $article->fLagerbestand <= 0 && $article->fLieferantenlagerbestand > 0 && $article->fLieferzeit > 0 && ($article->cLagerKleinerNull == "N" || $Einstellungen.artikeluebersicht.artikeluebersicht_lagerbestandanzeige_anzeigen == 'U')}
									<li class="clean">
										<span class="signal_image a1">{lang key="supplierStockNotice" section="global" printf=$article->fLieferzeit}</span>
									</li>
								{elseif $anzeige=='verfuegbarkeit' || $anzeige=='genau'}
									<li class="clean popover">
										 <span class="signal_image a{$article->Lageranzeige->nStatus}">{$article->Lageranzeige->cLagerhinweis[$anzeige]}</span>
										 {include file="tpl_inc/artikel_warenlager.tpl"}
									</li>
								{elseif $anzeige=='ampel'}
									<li class="clean popover">
										<span class="signal_image a{$article->Lageranzeige->nStatus}">{$article->Lageranzeige->AmpelText}</span>
										{include file="tpl_inc/artikel_warenlager.tpl"}
									</li>
								{/if}
								
								{if $Einstellungen.template.articleoverview.article_show_no == 'Y'}
									<li>{lang key="productNo" section="global"}: <strong>{$article->cArtNr}</strong></li>
								{/if}
								
								{if $article->bMHD && isset($article->dMHD) && isset($article->dMHD_de)}
									<li title="{lang key='productMHDTool' section='global'}" class="best-before"><strong>{lang key="productMHD" section="global"}:</strong> {$article->dMHD_de}</li>
								{/if}
								
								{if $Einstellungen.artikeluebersicht.artikeluebersicht_hersteller_anzeigen == "Y" && $article->cName_thersteller}
									<li>{lang key="manufacturerSingle" section="productOverview"}: {if $article->cHomepage_thersteller}<a href="{$article->cHomepage_thersteller}">{/if} <strong>{$article->cName_thersteller}</strong> {if $article->cHomepage_thersteller}</a>{/if}</li>
								{elseif $Einstellungen.artikeluebersicht.artikeluebersicht_hersteller_anzeigen == "BT" && $article->cName_thersteller && $article->cBildpfad_thersteller}
									<li>{lang key="manufacturerSingle" section="productOverview"}: {if $article->cHomepage_thersteller}<a href="{$article->cHomepage_thersteller}">{/if} <strong>{$article->cName_thersteller}</strong> {if $article->cHomepage_thersteller}</a>{/if}</li>
									<li><a href="{$article->cHomepage_thersteller}"><img src="{$article->cBildpfad_thersteller}" alt="" /></a></li>
								{elseif $Einstellungen.artikeluebersicht.artikeluebersicht_hersteller_anzeigen == "B" && $article->cBildpfad_thersteller}
									<li>{lang key="manufacturerSingle" section="productOverview"}: {if $article->cHomepage_thersteller}<a href="{$article->cHomepage_thersteller}">{/if} <img src="{$article->cBildpfad_thersteller}" alt="" /> {if $article->cHomepage_thersteller}</a>{/if}</li>
								{/if}
								{if isset($article->cGewicht) && $Einstellungen.artikeluebersicht.artikeluebersicht_gewicht_anzeigen == "Y" && $article->fGewicht > 0}
									<li>{lang key="shippingWeight" section="global"}: <strong>{$article->cGewicht} kg</strong></li>
								{/if}
								
								{if isset($article->cArtikelgewicht) && $Einstellungen.artikeluebersicht.artikeluebersicht_artikelgewicht_anzeigen == "Y" && $article->fArtikelgewicht > 0}
									<li>{lang key="productWeight" section="global"}: <strong>{$article->cArtikelgewicht} kg</strong></li>
								{/if}
								
								{if $Einstellungen.artikeluebersicht.artikeluebersicht_artikelintervall_anzeigen == "Y" && $article->fAbnahmeintervall > 0}
									<li>{lang key="purchaseIntervall" section="productOverview"}: <strong>{$article->fAbnahmeintervall} {$article->cEinheit}</strong></li>
								{/if}
							
								{if count($article->Variationen)>0}
									<li>{lang key="variationsIn" section="productOverview"}: <strong>{foreach name=variationen from=$article->Variationen item=Variation}{if !$smarty.foreach.variationen.first}, {/if}{$Variation->cName}{/foreach}</strong>
									{if $article->oVariationKombiVorschau_arr|@count > 0 && $article->oVariationKombiVorschau_arr && $Einstellungen.artikeluebersicht.artikeluebersicht_varikombi_anzahl > 0}
										<ul class="articles_combi">
											{foreach name=varikombis from=$article->oVariationKombiVorschau_arr item=oVariationKombiVorschau}
												<li><a href="{$oVariationKombiVorschau->cURL}" onmouseover="imageSwitch('{$article->kArtikel}', '{$oVariationKombiVorschau->cBildKlein}');" onmouseout="imageSwitch('{$article->kArtikel}');"><img src="{$oVariationKombiVorschau->cBildMini}" alt="" /></a></li>
											{/foreach}
										</ul>
									{/if}
									</li>
								{/if}
							
								<li class="clean clear">
									<ul class="actions">
										{if $Einstellungen.artikeluebersicht.artikeluebersicht_vergleichsliste_anzeigen == "Y"}
											<li><button name="Vergleichsliste" type="submit" class="compare"><span>{lang key="addToCompare" section="productOverview"}</span></button></li>
										{/if}
										{if $Einstellungen.artikeluebersicht.artikeluebersicht_wunschzettel_anzeigen == "Y"}
											<li><button name="Wunschliste" type="submit" class="wishlist">{lang key="addToWishlist" section="productDetails"}</button></li>
										{/if}
										{if $article->verfuegbarkeitsBenachrichtigung == 3 && (($article->cLagerBeachten == 'Y' && $article->cLagerKleinerNull != 'Y') || $article->cLagerBeachten != 'Y')}
											<li><button type="button" id="{$idGuid}n{$article->kArtikel}" class="popup notification">{lang key="requestNotification" section="global"}</button></li>
										{/if}
									</ul>
								</li>
								{if $Einstellungen.template.articleoverview.article_show_short_desc == 'Y' && $article->cKurzBeschreibung|@count_characters > 0}
									<li id="{$idGuid}article_short_desc" class="clean custom_content">
										{$article->cKurzBeschreibung}
									</li>
								{/if}
							</ul>
							
							{if isset($Einstellungen.artikeluebersicht.artikeluebersicht_finanzierung_anzeigen) && $Einstellungen.artikeluebersicht.artikeluebersicht_finanzierung_anzeigen == "Y" && isset($article->oRateMin)}
								<div class="article_financing financing">
									<p><strong>{lang key="fromJust" section="global"} {$article->oRateMin->cRateLocalized} {lang key="monthlyPer" section="global"}</strong></p>
									<small>{$article->oRateMin->cHinweisHTML}</small>
								</div>
							{/if}
						
							<!-- Warenkorb / Anzahl -->
							<div class="article_price">
								<ul>
								{if $smarty.session.Kundengruppe->darfPreiseSehen}
									{if $article->Preise->fVKNetto==0 && $article->bHasKonfig}
										<li class="price price_as_configured">{lang key="priceAsConfigured" section="productDetails"}</li>
									{elseif $article->Preise->fVKNetto==0 && $Einstellungen.global.global_preis0=="N"}
										<li class="price">{lang key="priceOnApplication" section="global"}</li>
									{else}
										{if $article->Preise->rabatt>0 && !$article->Preise->Sonderpreis_aktiv}
											{if $Einstellungen.artikeluebersicht.artikeluebersicht_rabattanzeige==2 || $Einstellungen.artikeluebersicht.artikeluebersicht_rabattanzeige==4}
												<li><small>{lang key="discount" section="global"}: {$article->Preise->rabatt}%</small></li>
											{/if}
											{if $Einstellungen.artikeluebersicht.artikeluebersicht_rabattanzeige==3 || $Einstellungen.artikeluebersicht.artikeluebersicht_rabattanzeige==4}
												<li><small>{lang key="oldPrice" section="global"}: <del>{$article->Preise->alterVKLocalized[$NettoPreise]}</del></small></li>
											{/if}
										{/if}
										{if $article->Preise->Sonderpreis_aktiv && $Einstellungen.artikeluebersicht.artikeluebersicht_sonderpreisanzeige==2}
											<li><small>{lang key="insteadOf" section="global"}: <del>{$article->Preise->alterVKLocalized[$NettoPreise]}</del></small></li>
										{/if}
										<li>
											<span class="price_label">
											{if $article->Preise->Sonderpreis_aktiv}
												{lang key="specialPrice" section="global"}
											{else}
												{if $article->nVariationsAufpreisVorhanden == 1 || $article->bHasKonfig}
													{lang key="priceStarting" section="global"}
												{elseif $article->Preise->rabatt>0}
													{lang key="nowOnly" section="global"}
												{else}
													{lang key="only" section="global"}
												{/if}
											{/if}
											</span>
											{if $article->Preise->strPreisGrafik_Suche}
												<span class="price_image">{$article->Preise->strPreisGrafik_Suche}</span>
											{else}
												<span class="price">{$article->Preise->cVKLocalized[$NettoPreise]}</span>
											{/if}
										</li>
										{if $article->cLocalizedVPE}
											<li>
												<small>
													 <b>{lang key="basePrice" section="global"}:</b> 
													 {if $article->nVariationsAufpreisVorhanden == 1 || $article->bHasKonfig}
														 {lang key="priceStarting" section="global"} 
													 {/if}
													 {$article->cLocalizedVPE[$NettoPreise]}
												</small>
											</li>
										{/if}
										<li><span class="vat_info">{$article->cMwstVersandText}</span></li>
										
										{if $article->Preise->fPreis1>0 && $article->Preise->nAnzahl1>0}
											{include file='tpl_inc/staffelpreise_inc.tpl'}
										{/if}
										
									{/if}
								{else}
									{lang key="priceHidden" section="global"}
								{/if}
								{if $article->cEstimatedDelivery}
									<li class="estimated_delivery a{$article->Lageranzeige->nStatus}">
										<small><b>{lang key="shippingTime" section="global"}</b>: {$article->cEstimatedDelivery}</small>
									</li>
								{/if}
								</ul>
								<div class="article_buy">
									<fieldset>
									{if ($article->inWarenkorbLegbar == 1 || ($article->nErscheinendesProdukt == 1 && $Einstellungen.global.global_erscheinende_kaeuflich == "Y")) && $article->nIstVater == 0 && $article->Variationen|@count == 0 && !$article->bHasKonfig}
										<span><input type="text" onfocus="this.setAttribute('autocomplete', 'off');" id="{$idGuid}quantity{$article->kArtikel}" class="quantity" name="anzahl" {if $article->fAbnahmeintervall}value="{$article->fAbnahmeintervall}" onblur="javascript:gibAbnahmeIntervall(this, {$article->fAbnahmeintervall});"{else}value="1"{/if} /></span>
										{if $Einstellungen.artikeluebersicht.artikeluebersicht_anzahl_pfeile == "Y" || ($Einstellungen.artikeluebersicht.artikeluebersicht_anzahl_pfeile == "I" && $article->fAbnahmeintervall > 1)}
										<span class="change_quantity">
											<a href="#" onclick="javascript:erhoeheArtikelAnzahl('{$idGuid}quantity{$article->kArtikel}', {if $article->fAbnahmeintervall > 0}true, {$article->fAbnahmeintervall}{else}false, 0{/if});return false;">+</a>
											<a href="#" onclick="javascript:erniedrigeArtikelAnzahl('{$idGuid}quantity{$article->kArtikel}', {if $article->fAbnahmeintervall > 0}true, {$article->fAbnahmeintervall}{else}false, 0{/if});return false;">-</a>
										</span>
										{/if}
										{$article->cEinheit}
										<input type="submit" id="{$idGuid}submit{$article->kArtikel}" {if $Einstellungen.template.articleoverview.article_pushed_to_basket == "Y"}onclick="return addToBasket('{$article->kArtikel}', $('#{$idGuid}quantity{$article->kArtikel}').val(), '#{$idGuid}image_drag_article'{if $Einstellungen.template.articleoverview.article_pushed_to_basket_animation == "Y"}, true{/if});"{/if} value="{lang key="addToCart" section="global"}" />
									{else}
										<button type="button" onclick="window.location.href='{$article->cURL}'">{lang key="details"}</button>
									{/if}
									</fieldset>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
				
				{if $article->verfuegbarkeitsBenachrichtigung==3}    
					<div id="{$idGuid}popupn{$article->kArtikel}" class="hidden">
						{include file='tpl_inc/artikel_produktverfuegbarformular.tpl' scope='artikeldetails'}
					</div>
				{/if}
				
				{if isset($Einstellungen.artikeluebersicht.artikeluebersicht_finanzierung_anzeigen) && $Einstellungen.artikeluebersicht.artikeluebersicht_finanzierung_anzeigen == "Y" && isset($article->oRateMin)}
					<div id="{$idGuid}popupf{$article->kArtikel}" class="hidden">
						{include file='tpl_inc/suche_finanzierung.tpl'}
					</div>
				{/if}
				
				<div class="clear"></div>
			</div>
		</li>
		{/foreach}
	</ul>

</div>
{/if}