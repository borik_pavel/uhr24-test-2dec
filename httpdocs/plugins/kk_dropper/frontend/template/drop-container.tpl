<div class="drop-container {$container.class|default:''}"
	style="{$container.margin}{$container.padding}{if !empty($container.textAlign) && $container.textAlign !== 'inherit'}text-align:{$container.textAlign};{/if}"
	data-container-drop="{$drop._internal.guid}">
	{$dropContent}
</div>