<div class="btn-toolbar alert toolbar-top" id="topbar">
	<!-- Backup handling -->
	<div class="pull-left btn-group">
		<span class="btn no-bg btn-large dropdown-toggle" data-toggle="dropdown" kk-tooltip="Backup wiederherstellen">
			<small><i class="icon-time"></i> Backups</small>
		</span>
		<ul class="dropdown-menu nav" style="max-height: 400px; max-width: 300px; overflow-y: auto;">
			<li class="nav-header">Export / Import</li>
			<li ng-click="showImportExport('export')"><a><small><i class="icon-download"></i> Export</small></a></li>
			<li ng-click="showImportExport('import')"><a><small><i class="icon-upload"></i> Import</small></a></li>
			<li class="nav-header">Speicherpunkte</li>
			<li ng-repeat="backup in saveHistory | orderBy:'timestamp':true" ng-click="restoreBackup(backup)" title="{{backup.note}}">
				<a><small>{{ backup.label }}<br><span ng-if="backup.note" class="ellipsis o-07 lh-14">{{backup.note}}</span></small></a>
			</li>
		</ul>
	</div>
	<!-- Media files -->
	<div class="pull-left btn-group">
		<span class="btn no-bg btn-large" kk-tooltip="Mediengalerie anzeigen" ng-click="showMediaGallery()"><small><i class="icon-picture"></i> Medien</small></span>
	</div>
	<!-- Notifications -->
	<kk-notifications></kk-notifications>
	<div class="center">

		<!-- Drop Navigation -->
		<div class="btn btn-large no-bg dashboard-toggle" ng-class="{'active': state.dashboardOpen}" ng-click="state.toggleDashboard()">
			<small><i class="icon-th"></i><span>&nbsp;Dashboard</span></small>
		</div>

		<!-- Save handling -->
		<div class="btn btn-large no-bg" ng-class="{'save' : state.dirty}" ng-mousedown="prepareSave()" ng-mouseup="finishSave()">
			<small>
				<i ng-if="state.saveInProgress" class="spinner"></i>
				<i class="icon-retweet" ng-if="!state.saveInProgress" ng-show="state.dirty"></i>
				<i class=" icon-ok" ng-if="!state.saveInProgress" ng-show="!state.dirty"></i>
				<span> &nbsp;{{state.dirty ? 'Speichern' : 'Gespeichert'}}</span>
			</small>
		</div>
	</div>
	<!-- Languages -->
	<div class="btn-group languages" ng-if="environment.getLanguages().length>1 && !state.dashbaordOpen">
		<strong style="text-transform: uppercase" class="btn btn-mini no-bg" ng-repeat="lang in environment.getLanguages()" ng-click="environment.setLanguage(lang)" ng-class="{'active': environment.language.cISO == lang.cISO}">{{lang.cISO}}</strong>
	</div>
</div>