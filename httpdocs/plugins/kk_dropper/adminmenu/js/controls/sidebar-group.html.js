<div class="kk-sidebar-group group" ng-class="{'open': isOpen(), 'root': parent.root}">

	<!-- group header -->
	<div 
		class="group-head group-item"
		tabindex="0"
		ng-class="{ open: isOpen(), root: parent.root, search: isSearching(), 'highlight': groupState.highlight === group.path }"
		ng-if="!group.root"
		ng-click="toggle()"
		ng-style="{ 'padding-left': HEAD_OFFSET }"
		data-sidebar-group="{{::group.path}}"
		kk-draggable kk-draggable-data="group" kk-draggable-type="'group'" kk-draggable-name="group.name" kk-draggable-info="'Gruppe verschieben'"
		kk-drop-droppable kk-drop-dropped="moveDropToGroup" kk-drop-dropped-data="group" kk-drop-dropped-group="group.path"
	>
		<div class="tree-line" ng-if="group.depth > 1"></div>
		<div class="tree-marker" ng-if="group.depth >= 1 && isOpen() && !group.root" ng-style="{ 'margin-left': HEAD_OFFSET }"></div>
		<div class="tree-marker clearer" ng-if="!group.root && isLast && !parent.drops.length" ng-style="{ 'margin-left': HEAD_OFFSET }"></div>
		<div class="group-selection"></div>
		<div class="sidebar-group-icon" ng-class="{'open': isOpen()}"></div>
		<div class="group-label" ng-hide="isRenaming()" title="{{::group.name}}">{{::group.name}}</div>
		<div class="group-label group-rename" ng-mousedown="$event.stopPropagation()" ng-click="$event.stopPropagation()" contenteditable ng-show="isRenaming()" ng-model="groupState.renaming.name"></div>
		<div class="drop-actions" ng-if="!isSearching()" ng-click="$event.stopPropagation(); $event.preventDefault();">
			<i class="icon-double-angle-up fadebtn fs-12" ng-if="group.depth > 1" ng-click="moveUp(group)" kk-tooltip="Eine Ebene nach oben verschieben"></i>
			<i class="fadebtn fs-15 fw-bold" ng-click="createDrop(group)" kk-tooltip="Neues Drop erstellen">+</i>
			<i class="icon-pencil fadebtn" ng-click="rename(group)" kk-tooltip="Gruppe umbenennen"></i>
			<i class="icon-random fadebtn" ng-click="tryGroupDuplicate(group)" kk-tooltip="Gruppe duplizieren"></i>
			<i class="icon-trash fadebtn" ng-click="tryGroupRemove(group)" kk-tooltip="Gruppe entfernen"></i>
		</div>
		<div class="group-count" ng-class="{'inset': isOpen()}">{{group.count}}</div>
		<div class="group-droppable-catcher" kk-droppable kk-droppable-drop="moveGroupToGroup" kk-droppable-type="'group'" kk-droppable-data="group"></div>
	</div>
	
	<!-- child groups -->
	<kk-sidebar-group ng-if="isOpen() && group.children" ng-repeat="child in group.children|toArray|orderBy: 'path'" parent="group" is-last="$last && !drops.length" group="child" group-state="groupState"></kk-sidebar-group>

	<!-- drops -->
	<div class="group-drops" ng-if="isOpen()">

		<div class="group-drop-wrapper" ng-repeat="dropInfo in group.drops">

			<div 
				class="group-drop group-item"
				tabindex="0"
				ng-click="utils.Drops.activateDrop(dropInfo.drop);" 
				ng-class="{'active': state.activeDropLoading === dropInfo.drop, 'filtered': state.activeDrop === dropInfo.drop && groupState.activeDropFiltered, 'highlight': groupState.highlight === dropInfo.drop._internal.guid, 'item-inactive': dropInfo.drop._internal.visibility == 'inactive', 'last': $last, root: group.root}" 
				ng-style="{ 'padding-left': DROP_OFFSET }"
				kk-drop-draggable kk-drop="dropInfo.drop" 
				data-sidebar-drop="{{dropInfo.drop._internal.guid}}"
			>
				<div class="tree-line" ng-if="group.depth >= 1"></div>
				<div class="group-selection"></div>
				<div class="drop-label" title="{{dropInfo.name}}">
					<i ng-if="!core.templates[dropInfo.drop._internal.dropId] || !core.dropLicenses[dropInfo.drop._internal.dropId].isValid || core.dropLicenses[dropInfo.drop._internal.dropId].isError" title="Drop inaktiv" class="icon-exclamation-sign fadebtn" ng-click="utils.Drops.showDropInfo(dropInfo.drop, 'licence', $event)"></i>
					<i ng-if="core.dropLicenses[dropInfo.drop._internal.dropId].canExpire" title="Testlizenz aktiv" class="icon-time fadebtn" ng-click="utils.Drops.showDropInfo(dropInfo.drop, 'licence', $event)"></i>
					{{dropInfo.name}}
				</div>
				<div class="drop-type" title="{{utils.Drops.getTemplateName(dropInfo.drop)}}">
					{{::utils.Drops.getTemplateName(dropInfo.drop)}}
				</div>
				<div class="drop-actions" ng-click="$event.stopPropagation(); $event.preventDefault();">
					<i class="icon-random fadebtn" ng-click="utils.Drops.duplicateDrop(dropInfo.drop)" kk-tooltip="Drop duplizieren"></i>
					<i class="icon-trash fadebtn" kk-tooltip="Drop entfernen" ng-click="tryDropRemove(dropInfo.drop)"></i>
				</div>
			</div>

			<!-- linked drops -->
			<div 
				class="linked-drops" 
				ng-if="groupState.linked.length && state.activeDrop === dropInfo.drop" 
				ng-style="{ 'padding-left': DROP_OFFSET }"
			>
				<div class="tree-marker"></div>
				<div class="tree-marker clearer" ng-if="$last"></div>
				<div 
					ng-repeat="dropInfo in groupState.linked"
					ng-click="utils.Drops.activateDrop(dropInfo.drop)" 
					kk-drop-draggable kk-drop="dropInfo.drop"
					class="linked-drop group-item"
					title="{{dropInfo.drop._internal.name}}"
				>
					<div class="tree-line"></div>
					<div class="icon-link m-05-left m-025-right o-07"></div>
					<div class="drop-label">{{dropInfo.name}}</div>
	
				</div>
			</div>
		</div>
	</div>
</div>