<div class="dropper-dashboard" id="drop-select" ng-style="{left: sidebarWidth + 'px'}">
	<div class="dashboard-arrow"></div>
	<div class="scroll-y">
		<div class="head">
			<div class="btn-group pull-left">
				<div class="btn btn-small" ng-click="view.type = 'installed';" ng-class="{'active': view.type == 'installed'}">Installierte Drops</div>
				<div class="btn btn-small" ng-click="view.type = 'store';" ng-class="{'active': view.type == 'store'}">Drop Store</div>
				<a class="btn btn-small" href="https://kreativkonzentrat.de/Wiki/dropper/overview" target="_blank">Dropper Wiki</a>
			</div>
			<div class="btn-group pull-left">
				<div class="btn btn-small" ng-click="showPackageManager()">Paketverwaltung<span class="m-05-left badge" ng-if="getUpgradesCount() > 0">{{getUpgradesCount()}}</span></div>
			</div>
			<span class="form-search pull-right">
				<input class="search" placeholder="Suchen" type="text" ng-model="search"></input>
			</span>
		</div>
		<!-- List installed Drops -->
		<div class="installed" ng-if="view.type == 'installed'">
			<div class="clear"></div>
			<h1>Erstelle neue Inhalte mit deinen Drops</h1>
			<!-- drops installed? -->
			<div class="drop-list" ng-if="utils.countObject(filterTemplates(core.templates)) > 0">
				<div ng-if="search.length && dropPagination.itemCount === 0" class="tcenter">
					<h2 class="light tcenter m-5-top">Nichts gefunden</h2>
					<p class="fs-12 o-08 m-1-top">Deine Suche nach <i>{{search}}</i> hat keine Teffer ergeben.</p>
				</div>
				<div 
					class="drop" 
					ng-repeat="(templateKey, template) in filterTemplates(core.templates) | filterObj:'name':search | filterPaginate:dropPagination:6" 
					ng-click="utils.Drops.showDropInfo(templateKey, hasValidLicense(templateKey) ? 'infos' : 'licence')" 
					ng-class="{'inactive': !hasValidLicense(templateKey)}"
				>
					<span ng-class="{ 'no-img': !template._icon, 'o-03': !hasValidLicense(templateKey) }" class="drop-image" style="background-image:url({{template._icon}})"/></span>
					<div class="name" ng-class="{'o-07': !hasValidLicense(templateKey)}">{{template.name}}</div>
					<div class="desc">{{template.description}}</div>
					<div class="label label-important" ng-if="!hasValidLicense(templateKey)">inaktiv</div>
					<div class="label label-info" ng-if="hasValidLicense(templateKey) && core.dropLicenses[templateKey].canExpire">demo</div>
					<div class="label label-info" ng-if="packageManager.isUpgradeable(templateKey)"><i class="icon-upload"></i> update</div>
				</div>
				<div ng-if="dropPagination.canPaginate()">
					<div class="move-back" ng-class="{'o-02' : dropPagination.isFirstPage()}" ng-click="dropPagination.prev()"></div>
					<div class="move-forward" ng-class="{'o-02' : dropPagination.isLastPage()}" ng-click="dropPagination.next()"></div>
				</div>
			</div>
			<!-- no drops installed? -->
			<div class="drop-list tcenter p-5-top" ng-if="utils.countObject(filterTemplates(core.templates)) == 0">
				<i class="icon-download fs-7 o-02" ng-click="view.type = 'store'"></i>
				<h2 class="light tcenter m-05-top">Du hast noch keine Drops installiert</h2>
				<p class="fs-12 o-08 m-1-top">Schaue doch mal im <a ng-click="view.type = 'store'">Drop Store</a> vorbei um neue Drops zu installieren.</p>
			</div>
		</div>
		<!-- List Drops from the Store -->
		<div class="store tcenter" ng-if="view.type == 'store'">
			<div ng-if="packageManager.hasInstallablePackages()">
				<h1 class="m-05-bottom">Installiere neue Drops aus dem Drop Store</h1>
				<div class="tcenter m-2-bottom">
					<span class="btn-group tleft d-inline-block">
						<a class="btn btn-small dropdown-toggle" data-toggle="dropdown">
							<span ng-if="view.filter === 'all'">Alle Drops</span> 
							<span ng-if="view.filter === 'free'">Free Drops</span> 
							<span ng-if="view.filter === 'paid'">Paid Drops</span>
							<span ng-if="view.filter === 'integrations'">Template Integrationen</span>
							<span class="caret m-025-left"></span>
						</a>
						<ul class="dropdown-menu">
							<li ng-click="view.filter = 'all'"><a>Alle Drops</a></li>
							<li ng-click="view.filter = 'free'"><a>Free Drops</a></li>
							<li ng-click="view.filter = 'paid'"><a>Paid Drops</a></li>
							<li ng-click="view.filter = 'integrations'"><a>Template Integrationen</a></li>
						</ul>
					</span>
				</div>
				<div class="drop-list">
					<div ng-if="search.length && storePagination.itemCount === 0" class="tcenter">
						<h2 class="light tcenter m-5-top">Nichts gefunden</h2>
						<p class="fs-12 o-08 m-1-top">Deine Suche nach <i>{{search}}</i> hat keine Teffer ergeben.</p>
					</div>
					<div class="drop" ng-click="showPackageManager('install', package)" ng-repeat="package in packageManager.getInstallable() | filterObj:'meta.name':search | filterPkgList:view.filter | filterPaginate:storePagination:6">
						<span ng-class="{ 'no-img': !package.meta.icon || package.meta.icon == '' }" class="drop-image" style="background-image:url({{package.meta.icon}})"/></span>
						<div class="name">{{package.meta.name}}</div>
						<div class="desc">{{package.meta.description}}</div>
						<div class="label label-warning" ng-if="package.meta.price > 0 && !core.dropperLicense.license.attributes.dropFlatrate">{{package.meta.price}} Euro</div>
						<div class="label label-info" ng-if="package.meta.price === 0">free</div>
					</div>
					<div ng-if="storePagination.canPaginate()">
						<div class="move-back" ng-class="{'o-02' : storePagination.isFirstPage()}" ng-click="storePagination.prev()"></div>
						<div class="move-forward" ng-class="{'o-02' : storePagination.isLastPage()}" ng-click="storePagination.next()"></div>
					</div>
				</div>
			</div>
			<div ng-if="!packageManager.hasInstallablePackages()">
				<h1>Installiere neue Drops aus dem Drop Store</h1>
				<div class="drop-list">
					<h2 style="margin-top: 10%" class="light tcenter">
						Nichts gefunden oder<br/>du hast bereits alle Drops installiert.
					</h2>
				</div>
			</div>
		</div>
		<!-- Information about the Dropper License -->
		<div class="foot tcenter"><a ng-click="showSettings()"><i class="icon-gear"></i> Dropper {{getLicenceTypeName()}} v{{core.dropperVersion}} ({{core.dropperLicense.type}})</a> &ndash; {{core.dropperLicense.reason}}</div>
	</div>
</div>