<div class="kk-propertygrid" autocomplete="off">
	<ul class="nav nav-tabs" ng-class="{'nav-tabs-sm': kkOpts.tabsSmall, 'nav-tabs-contained': kkOpts.tabsContained}" ng-if="!hasOnlyDefaultSection()">
		<li ng-repeat="(section, options) in sections" ng-show="isSectionVisible(options)" ng-class="{'active': isActiveSection(section)}" ng-click="setActiveSection(section)"><a>{{section}}</a></li>
	</ul>
	<div ng-repeat="(section, options) in sections" class="content-box" ng-class="{'tab-content': !hasOnlyDefaultSection()}" ng-show="isActiveSection(section) && isSectionVisible(options)">
		<div class="dropItem-properties properties content-box">
			<div ng-repeat="option in options" class="control-group" ng-if="canShowProperty(option, kkModel, kkModelScope)">

				<span ng-show="evalProp(option, kkModel)"></span>

				<label class="control-label" ng-if="option.type != 'invisible' && option.name" for="{{::option.property}}">
					<span kk-tooltip="{{::option.description}}">{{::option.name}} <a ng-if="option.multilanguage" ng-click="toggleNextLanguage()" style="color:inherit">({{ environment.language.cNameDeutsch }})</a></span>
				</label>

				<div ng-switch on="option.type" class="controls" ng-class="{'o-07': usesLangFallbackValue(kkModel, option)}">

					<div class="lang-fallback-info" ng-if="usesLangFallbackValue(kkModel, option)">
						<span kk-tooltip="&laquo;{{::option.name}}&raquo; ist für diese Sprache nicht gesetzt. Es wird daher der Wert in der Standardsprache als Fallback genutzt. Du kannst den Wert aus der Standardsprache bearbeiten oder ihn für diese Sprache übertragen.">{{environment.languageDefault.cNameDeutsch}}er Wert wird als Fallback benutzt.</span>
						<a ng-click="environment.setLanguage(environment.languageDefault)" title="Bearbeite den Wert aus der Fallback-Sprache ({{environment.languageDefault.cNameDeutsch}})">Bearbeiten</a>&nbsp;|&nbsp;<a title="Kopiere den Wert aus der Fallback-Sprache ({{environment.languageDefault.cNameDeutsch}}) in die aktuelle Sprache ({{environment.language.cNameDeutsch}})"	ng-click="copyLangValue(environment.languageDefault.cISO,environment.language.cISO, kkModel[option.property])">Übertragen</a>
					</div>

					<hr ng-switch-when="divider"/>

					<div ng-switch-when="checkbox">
						<input type="checkbox" ng-model="kkModel[option.property]" name="{{::option.property}}"/>
					</div>
		
					<div ng-switch-when="list">
						<kk-custom-control control-name="'list'" kk-model="kkModel[option.property]" kk-data="kkModel" kk-data-parent="kkModelScope" kk-config="kkConfig" kk-option="option" kk-path="kkPath + '.' + option.property"></kk-custom-control>
					</div>
		
					<div ng-switch-when="invisible"></div>
		
					<div ng-switch-default>
						<kk-custom-control ng-if="option.multilanguage" control-name="option.type" kk-model="kkModel[option.property][environment.language.cISO]" kk-data="kkModel" kk-data-parent="kkModelScope" kk-config="kkConfig" kk-option="option" kk-path="kkPath + '.' + option.property"></kk-custom-control>
						<kk-custom-control ng-if="!option.multilanguage" control-name="option.type" kk-model="kkModel[option.property]" kk-data="kkModel" kk-data-parent="kkModelScope" kk-config="kkConfig" kk-option="option" kk-path="kkPath + '.' + option.property"></kk-custom-control>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>