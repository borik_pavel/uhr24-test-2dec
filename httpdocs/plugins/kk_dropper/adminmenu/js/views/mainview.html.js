<div id="main-view" class="flex-vertical" ng-cloak>

	<!-- Menu Bar -->
	<div id="menu_wrapper">
		<div id="menu"></div>
	</div>

	<!-- Top Bar -->
	<kk-topbar></kk-topbar>

	<div class="flex-horizontal">

		<!-- Sidebar -->
		<kk-sidebar></kk-sidebar>
	
		<!-- Edit windows -->
		<div id="edit-windows"></div>

	</div>

	<!-- Dashboard -->
	<kk-dashboard></kk-dashboard>

</div>
