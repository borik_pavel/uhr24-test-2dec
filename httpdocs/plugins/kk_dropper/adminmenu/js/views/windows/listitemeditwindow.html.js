<!-- Item Detail Bar -->
<button type="button" class="close" ng-click="close()">&times;</button>
<div class="list-item-filter btn btn-small no-bg" ng-if="useConditional && item && item._internal" kk-tooltip="Anzeige einschränken" ng-click="showConditionalModal()"><i class="icon-filter"></i><div class="badge badge-warning" ng-if="item._internal.conditionalSettings.length">{{item._internal.conditionalSettings.length}}</div></div></div>

<h1 ng-if="!nameBinding" contenteditable spellcheck="false" ng-model="item._internal.name" ng-model-options="{updateOn: 'default blur', debounce: { default: 500, blur: 0 }}"></h1>
<h1 ng-if="nameBinding" style="opacity: 0.65; cursor: not-allowed">{{item._internal.name|stripTags}}</h1>

<div class="toggle-visibility">
	<div class="dropdown-toogle" data-toggle="dropdown" ng-switch on="item._internal.visibility" kk-tooltip="Lege fest, ob dieses Element aktiv ist.<br/><strong>Tipp: </strong>Inaktive DropItems kannst du trotzdem sehen, wenn du ein <strong><i><nobr>?preview</nobr></i></strong> and die URL deiner Shopseite anhängst.">
		<i ng-switch-when="active" class="icon-eye-open"></i>
		<i ng-switch-when="inactive" class="icon-eye-close"></i>
	</div>
	<ul class="dropdown-menu">
		<li ng-click="item._internal.visibility = 'active'" ng-class="{'active': item._internal.visibility == 'active'}"><a><i class="icon-eye-open"></i> Aktiv</a></li>
		<li ng-click="item._internal.visibility = 'inactive'" ng-class="{'active': item._internal.visibility == 'inactive'}"><a><i class="icon-eye-close"></i> Inaktiv</a></li>
	</ul>
</div>
<br/>
<br/>
<!-- display options -->
<kk-propertygrid ng-if="item && itemSettings" class="form" kk-model="item" kk-model-scope="itemParent" kk-config="itemSettings" kk-path="itemPath"></kk-propertygrid>