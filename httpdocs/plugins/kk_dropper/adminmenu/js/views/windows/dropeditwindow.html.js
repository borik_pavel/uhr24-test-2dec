<!-- Drop Details -->
<div ng-if="!activeDrop">
	<h4 class="tcenter o-07"><i class="spinner o-08"></i> Lade...</h4>
</div>
<div ng-if="activeDrop">
	<div class="edit-window-header">
		<h1 contenteditable spellcheck="false" ng-model="activeDrop._internal.name" ng-model-options="{updateOn: 'default blur', debounce: { default: 500, blur: 0 }}"></h1>
		<div class="toggle-visibility">
			<div class="dropdown-toogle" data-toggle="dropdown" ng-switch on="activeDrop._internal.visibility" kk-tooltip="Lege fest, ob dieses Drop aktiv ist.<br/><strong>Tipp: </strong>Inaktive Drops kannst du trotzdem sehen, wenn du ein <strong><i><nobr>?preview</nobr></i></strong> and die URL deiner Shopseite anhängst.">
				<i ng-switch-when="active" class="icon-eye-open"></i>
				<i ng-switch-when="inactive" class="icon-eye-close"></i>
			</div>
			<ul class="dropdown-menu">
				<li ng-click="activeDrop._internal.visibility = 'active'" ng-class="{'active': activeDrop._internal.visibility == 'active'}"><a><i class="icon-eye-open"></i> Aktiv</a></li>
				<li ng-click="activeDrop._internal.visibility = 'inactive'" ng-class="{'active': activeDrop._internal.visibility == 'inactive'}"><a><i class="icon-eye-close"></i> Inaktiv</a></li>
			</ul>
		</div>
		<div class="show-docs">
			<div class="d-inline-block m-05" ng-click="loadBackupHistory()">
				<i class="fadebtn icon-undo dropdown-toggle" kk-tooltip="Alten Speicherstand wiederherstellen" data-toggle="dropdown"></i>
				<ul class="dropdown-menu nav" style="width:300px; right:-1em; left:auto; max-height:300px; overflow-y:auto">
					<li class="nav-header">History</li>
					<li ng-if="backupHistory === null"><a><span class="o-07"><i class="spinner o-08"></i> Lade...</span></a></li>
					<li ng-if="backupHistory.length" ng-repeat="backup in backupHistory" ng-click="restoreBackupState(backup)" title="{{backup.note}}">
						<a>
							<div><small>{{backup.date}}</small></div>
							<div class="ellipsis"><small class="o-07" ng-if="backup.note">{{backup.note}}</small></div>
						</a>
					</li>
				</ul>
			</div>
			<div class="fadebtn d-inline-block m-05 m-0-right" kk-tooltip="{{utils.Drops.getTemplateName(activeDrop)}} Dokumentation" ng-click="utils.Drops.showDropInfo(activeDrop, 'infos')"><i class="icon-question-sign"></i></div>
		</div>
	</div>
	<div>
		<div ng-if="isInsertable() || isConfigureable() || hasContainerSettings() || hasLangSettings()">
			<legend><h4 title="{{utils.Drops.getTemplateName(activeDrop)}} Konfiguration">Konfiguration</h4></legend>
			<small class="light">Konfiguriere dieses Drop und bestimme wo und unter welchen Bedingungen es angezeigt wird.</small>
			<div class="btn-toolbar">
				<div class="btn-group same-size">
					<a class="btn btn-warning btn-icon-only btn-drop-config-modal" ng-if="isConfigureable()" kk-tooltip="Konfiguriere dieses Drop" ng-click="showDropConfigModal()"><i class="icon-cog icon-white"></i></a>
					<a class="btn" ng-if="isInsertable()" kk-tooltip="Einfügeoptionen" ng-click="showInsertConditionsModal()"><i class="icon-map-marker"></i> <small><strong>Wann</strong><span ng-if="!state.isFrontendMode"> &amp; <strong>wo</strong></span> anzeigen</small></a>
				</div>
				<a class="btn pull-right" ng-if="hasLangSettings()" kk-tooltip="Sprachoptionen" ng-click="showLanguageModal()"><i class="icon-flag"></i></a>
				<a class="btn pull-right relative" ng-if="hasContainerSettings()" kk-tooltip="Container-Einstellungen ({{activeDrop._internal.container.active ? 'aktiv' : 'inaktiv'}})" ng-click="showDropContainerModal()">
					<i class="icon-crop"></i>
				  <i class="container-status icon-check-sign" ng-if="activeDrop._internal.container.active"></i>
				</a>
			</div>
		</div>
		<!-- Drop Options Outside -->
	
		<!-- default display propertygrid -->
		<div ng-show="hasSettings() && (!activeTemplate.presentation.settings || activeTemplate.presentation.settings == 'default')" >
			<legend><h4  title="{{utils.Drops.getTemplateName(activeDrop)}} Einstellungen">Einstellungen</h4></legend>
			<kk-propertygrid ng-if="!activeTemplate.presentation.settings || activeTemplate.presentation.settings == 'default'" class="form" kk-model="activeDrop" kk-model-scope="null" kk-config="activeTemplate.settings" kk-path="path"></kk-propertygrid>
		</div>
	
		<!-- custom display -->
		<div ng-if="hasSettings() && activeTemplate.presentation && activeTemplate.presentation.settings">
			<legend><h4>Einstellungen</h4></legend>
			<kk-custom-control control-name="activeTemplate.presentation.settings" kk-model="activeDrop" kk-config="activeTemplate" kk-path="path"></kk-custom-control>
		</div>
	
		<!-- legacy drop items -->
		<div ng-if="hasSettings() && activeTemplate.presentation && activeTemplate.presentation.dropItemList && activeTemplate.presentation.dropItemList != 'default'">
			<legend><h4>{{activeTemplate.itemsLabel && activeTemplate.itemsLabel || 'Elemente'}}</h4></legend>
			<small class="light">Dieses Drop kann {{activeTemplate.itemsLabel && activeTemplate.itemsLabel || 'Elemente'}} enthalten. Erstelle, bearbeite oder lösche {{activeTemplate.itemsLabel && activeTemplate.itemsLabel || 'Elemente'}} hier.</small>
			<kk-custom-control control-name="activeTemplate.presentation.dropItemList" kk-model="activeDrop" kk-config="activeTemplate" kk-path="path"></kk-custom-control>
		</div>
	</div>
</div>
