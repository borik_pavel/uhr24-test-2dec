<!-- Media Gallery -->
<div class="mediaGalleryModal" style="overflow:hidden">
	<div class="modal-header" ng-click="exitPreview()">
		<div class="close" ng-click="close()">×</div>

		<h4 ng-if="!state.selectmode" class="tcenter">Mediendateien</h4>
		<h4 ng-if="state.selectmode" class="tcenter">Mediendatei ausw&auml;hlen</h4>

		<div class="btn-group">
			<span class="btn" ng-class="{'active': state.state === 'browse'}" ng-click="toggleBrowse()"><i class="icon-th"></i></span>
			<span class="btn" ng-class="{'active': state.state === 'search'}" ng-click="toggleSearch()"><i class="icon-search"></i></span>
			<span class="btn btn-upload" ng-class="{'active': state.state == 'upload'}" ng-click="toggleUpload()"><i class="icon-upload"></i></span>
		</div>
		<div ng-show="state.state==='search'"><input class="search" type="text" placeholder="Suchen" ng-model="search.filename"></input></div>
		<div ng-show="state.state==='upload'" class="uploader"></div>
	</div>
	<div class="modal-body">
		<ul class="mediafiles" ng-if="!state.nofiles">

			<div ng-if="paginate.canPaginate()" class="tcenter">
				<div class="btn-group">
					<div class ="btn btn-small no-bg" ng-class="{'disabled': paginate.isFirstPage()}" ng-click="paginate.prev()">&laquo;</div>
					<div class ="btn btn-small no-bg" ng-repeat="num in paginate.getPageNumbers()" ng-class="{'active': paginate.currentPage == num}" ng-click="paginate.currentPage=num">{{num}}</div>
					<div class ="btn btn-small no-bg" ng-class="{'disabled': paginate.isLastPage()}" ng-click="paginate.next()">&raquo;</div>
				</div>
				<hr style="margin: 15px 0"/>
			</div>

			<li ng-repeat="mediafile in searchFiles|orderBy:'modified':true|filterPaginate:paginate:12" class="mediafile" ng-class="{'loading': !previewOpen && activefile === mediafile}" ng-click="preview(mediafile)">
				<img ng-src="{{getThumb(mediafile, 160)}}"/>
				<div class="mediafile-name">
					{{mediafile.filename}}<br/>
					<small ng-if="mediafile.width && mediafile.height">({{mediafile.width}}x{{mediafile.height}})</small>
					<small ng-if="!mediafile.isImage">&nbsp;</small>
				</div>
				<div class="refcount badge badge-info" ng-if="getFileReferences(mediafile).length">{{getFileReferences(mediafile).length}}</div>
			</li>
		</ul>
		<div ng-if="state.nofiles" class="tcenter">
			<br/>
			<h3>Noch keine Mediendateien vorhanden.</h3>
			<p>Hier findest Medien, sobald du welche hochgeladen hast</p>
		</div>
	</div>
	<div class="media-gallery-preview" ng-class="{'preview-visible': previewOpen}">
		<div class="file-info modal-header">
			<div class="close" ng-click="exitPreview()">×</div>
			<h4 class="tcenter">{{activefile.filename}}</h4>

			<div class="btn-group" ng-show="!state.selectmode">
				<span class="btn btn-small" ng-class="{'active': state.previewstate == 'references'}" ng-if="fileReferences.length" ng-click="state.previewstate='references'">Links <span class="media-link-count">{{fileReferences.length}}</span></span>
				<span class="btn btn-small" ng-class="{'active': state.previewstate == 'info'}" ng-if="activefile.isImage" ng-click="state.previewstate='info'">Info</span>
				<span class="btn btn-small" ng-class="{'active': state.previewstate == 'tag'}" ng-if="activefile.isImage" ng-click="state.previewstate='tag'">Tag</span>
				<span class="btn btn-small" ng-class="{'active': state.previewstate == 'url'}" ng-click="state.previewstate='url'">URL</span>
			</div>
		</div>

		<div class="media-prev-wrap">
			<div ng-show="!state.selectmode">
				<div class="overlay" ng-if="state.previewstate === 'references' && fileReferences.length"><span class="badge badge-info media-reference" ng-repeat="drop in fileReferences" ng-click="utils.Drops.activateDrop(drop); close();"><i class="icon-link"></i> {{drop._internal.name}}</span></div>
				<div class="overlay" ng-if="state.previewstate === 'info' && activefile.isImage">{{activefile.width}}x{{activefile.height}}</div>
				<div class="overlay" ng-if="state.previewstate === 'tag'  && activefile.isImage" ng-mousedown="showCopyTag()">&lt;img src="{{activefile.path}}" alt="" width="{{activefile.width}}" height="{{activefile.height}}"/&gt;</div>
				<div class="overlay" ng-if="state.previewstate === 'url'">{{activefile.path}}</div>
			</div>
			<div class="overlay" ng-if="state.selectmode && !isSelected(activefile)">Klicke auf das Vorschaubild, um es auszuw&auml;hlen</div>

			<img ng-show="activefile.isImage" ng-src="{{getThumb(activefile, 650)}}" ng-click="select(activefile)"/>
			<h2 ng-show="!activefile.isImage" ng-click="select(activefile)" style="color: rgba(255,255,255,0.5); margin-top:60px;">Keine Vorschau verf&uuml;gbar</h2>

			<i class="selected-indicator icon-ok-sign" ng-class="{'in': isSelected(activefile)}"></i>
			<div class="media-item-delete" ng-if="!state.selectmode" ng-click="state.deleteMode=true"><i class="icon-trash"></i></div>
			<div class="media-delete-panel tcenter" ng-if="!state.selectmode" ng-class="{'visible': state.deleteMode}">
				<!-- no references -->
				<div ng-if="!fileReferences.length">
					<h3>Datei wirklich löschen?</h3>
					<p>Die Datei wird dadurch für immer auf dem Server verloren gehen.</p>
					<div class="btn-group">
						<div class="btn btn-invert" ng-click="removeMediaItem(activefile)">Löschen</div>
						<div class="btn" ng-click="state.deleteMode=false">Abbrechen</div>
					</div>
				</div>
				<!-- references found -->
				<div ng-if="fileReferences.length">
					<h3>Datei in Benutzung!</h3>
					<p>Folgende Drops verwenden diese Datei: <span ng-repeat="drop in fileReferences" ng-click="utils.Drops.activateDrop(drop); close();"><span class="inline-reference">{{drop._internal.name}}</span><span ng-if="!$last">, </span></span>. Sie sollte daher nicht gelöscht werden! Wenn du sie trotzdem löscht, gehe sicher, einen Ersatz mit gleichem Dateinamen hochzuladen!</p>
					<div class="btn-group">
						<div class="btn btn-invert" ng-click="removeMediaItem(activefile, true)">Trotzdem löschen</div>
						<div class="btn" ng-click="state.deleteMode=false">Datei behalten</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>