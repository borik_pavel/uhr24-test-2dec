<div class="modal-header tcenter">
	<button type="button" class="close" ng-click="close()">×</button>
	<h4>Erstelle ein neues Drop</h4>
	<p>Wähle ein Drop aus, mit dem du Inhalte erstellen möchtest.</p>
	<div><input class="search" id="drop-user-select-search" type="text" placeholder="Suchen" ng-model="search" autocomplete="off"></input></div>
</div>
<div class="modal-body no-padding">

	<!-- Drops -->
	<div class="drop-list list-style" ng-if="utils.countObject(filterTemplates()) > 0">
		<div ng-repeat="template in filterTemplates(true)|toArray|filter:{name: search}|orderBy:['-_instanceCount', 'name']">
			<div class="drop" ng-click="close(template.$key)">
				<span ng-class="{ 'no-img': !template._icon }" class="drop-image" style="background-image:url({{template._icon}})"/></span>
				<div class="name">{{template.name}}</div>
				<div class="desc">{{template.description}}</div>
				<div class="label label-info" ng-if="core.dropLicenses[template.$key].isValid && core.dropLicenses[template.$key].canExpire">demo</div>
			</div>
		</div>
	</div>

	<!-- Droplets -->
	<div class="drop-list list-style" ng-if="droplets && droplets.length">
		<div ng-repeat="droplet in droplets | filter: { _template: {name: search}}">
			<h4 ng-if="$first" class="tcenter bbottom p-1 m-0">Droplets</h4>
			<div class="drop" ng-click="close(droplet)">
				<span class="drop-image" style="background-image:url({{core.adminURL}}img/droplet.png)"/></span>
				<div class="name">{{droplet._template.name}}</div>
				<div class="desc">{{droplet._template.description}}</div>
				<div class="label label-important" ng-if="!core.dropLicenses['KK_Droplet'].isValid">inaktiv</div>
				<div class="label label-info" ng-if="core.dropLicenses['KK_Droplet'].isValid && core.dropLicenses['KK_Droplet'].canExpire">demo</div>
			</div>
		</div>
	</div>

	<!-- Inaktive Drops -->
	<div class="drop-list list-style" ng-if="utils.countObject(filterTemplates()) > 0">
		<div ng-repeat="template in filterTemplates(false)|toArray|filter:{name: search}|orderBy:'name'">
			<h4 ng-if="$first" class="tcenter bbottom p-1 m-0">Nicht verfügbar</h4>
			<div class="drop" ng-click="close(template.$key)">
				<span ng-class="{ 'no-img': !template._icon }" class="drop-image o-03" style="background-image:url({{template._icon}})"/></span>
				<div class="name o-07">{{template.name}}</div>
				<div class="desc">{{template.description}}</div>
				<div class="label label-important" ng-if="!core.dropLicenses[template.$key].isValid">inaktiv</div>
			</div>
		</div>
	</div>

	<div ng-if="utils.countObject(core.templates) == 0" class="tcenter p-2">
		<h2 class="light tcenter m-05-top m-0-bottom fs-15">Du hast noch keine Drops installiert</h2>
		<p class="fs-1 o-08 m-05-top">Schaue doch mal im <a ng-click="showPackageManager()">Drop Store</a> vorbei um neue Drops zu installieren.</p>
	</div>
</div>
<div class="modal-footer">
	<div class="btn btn-primary right" ng-click="close()">Abbrechen</div>
</div>
