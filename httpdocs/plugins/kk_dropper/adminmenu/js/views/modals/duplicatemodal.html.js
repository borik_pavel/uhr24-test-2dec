<!-- Drop Informations -->
<div class="duplicateModal">
	<div class="modal-header tcenter">
		<div class="close" ng-click="close()">×</div>
		<h4>Drop<span ng-if="drops.length > 1">s</span> duplizieren</h4>
		<p ng-if="drops.length < depTree.length">
			<span ng-if="drops.length === 1 && drops.length < depTree.length">Das Drop &quot;{{drops[0]._internal.name}}&quot; hat andere Drops in sich verlinkt.</span>
			<span ng-if="drops.length > 1 && drops.length < depTree.length">Die Drops haben weitere Drops in sich verlinkt.</span>
			Bestimme welche Originale verlinkt bleiben sollen und von welchen verlinkten Drops du Klone anlegen möchtest.
		</p>
	</div>
	<div class="modal-body form">
		<div class="form well well-small">
			<label class="control-label" kk-tooltip="Kopierte Drops werden in diese Gruppe gepackt">Gruppe für kopierte Drops</label>
			<input type="text" ng-model="group.path"/>
		</div>
		<div class="control-group">
			<ul class="nav nav-tabs nav-stacked">
				<li ng-repeat="dropDep in depTree" class="clearfix" style="padding-left: {{dropDep.level*1.5}}em;" ng-class="{'light disabled' : !canClone(dropDep), 'disabled' : dropDep.level == 0}" ng-click="toggleMode(dropDep)">
					<a style="color: inherit">
						<span>
							<i kk-tooltip="Original bleibt verlinkt" class="icon-link" ng-show="!shouldClone(dropDep)"></i>
							<i kk-tooltip="Drop wird dupliziert" class="icon-copy" ng-show="shouldClone(dropDep)"></i>
						</span>&nbsp;
						<span class="">{{getName(dropDep)}}</span>&nbsp;&nbsp;<span class="light pull-right">{{utils.Drops.getTemplateName(dropDep.drop)}}</span><span class="badge badge-info" ng-show="dropDep.clone">neu</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="modal-footer">
		<div class="btn right" ng-click="close()">Abbrechen</div>
		<div class="btn btn-primary right" ng-click="applyOperations()">Duplizieren</div>
	</div>
</div>