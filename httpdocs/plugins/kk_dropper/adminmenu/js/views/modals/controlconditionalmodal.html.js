<div class="controlConditionalModal">
	<!-- Insert Conditions modal -->
	<div class="modal-header tcenter" ng-click="setPickerVisible(false)">
		<button type="button" class="close" ng-click="close()">×</button>
		<h4>Konditionen</h4>
		<p>Lege Filterkonditionen an um deine Auswahl einzuschränken</p>
	</div>
	<div class="modal-body relative">
		<conditional kk-settings="conditionalSettings">
		    <div>Du hast derzeit keine Filterkonditionen angelegt. Lege Filterkonditionen an, um die Auswahl einzuschränken. Weitere Informationen zu den Darstellungsfiltern findest du in unserem <a target="_blank" href="https://kreativkonzentrat.de/Wiki?page=dropper/details/einfuegemethoden#cmsanchor-darstellungsfilter">Wiki</a>.</div>
		</conditional>
	</div>
	<div class="modal-footer">
		<div class="btn-group">
			<div class="btn btn-primary" data-dismiss="modal" ng-click="apply()">Übernehmen</div>
			<div class="btn" ng-click="close()">Abbrechen</div>
		</div>
	</div>
</div>