<!-- Import / Export -->
<div class="dropImportExportModal">
	<div class="modal-header tcenter">
		<div class="close" ng-click="close()">×</div>
		<h4>Import / Export</h4>
		<div class="btn-group">
			<span class="btn btn-small" ng-class="{'active': tab == 'export'}" ng-click="setTab('export')"><i class="icon-download"></i> Export</span>
			<span class="btn btn-small" ng-class="{'active': tab == 'import'}" ng-click="setTab('import')"><i class="icon-upload"></i> Import</span>
		</div>
	</div>
	<div class="modal-body">
		<input type="file" class="import-uploader" style="display:none" accept=".json,.zip"/>

		<!-- Export -->
		<div ng-show="tab == 'export'" class="form">
			<p class="alert alert-info">Wähle die Drops aus, die du exportieren möchtest. Du erhältst dann eine Export-Datei zum Download, die du mit dem Importer wieder einspielen kannst.</p>
			<div class="control-group" ng-if="exportDrops.items.length !== core.settings.drops.length">
				<label class="control-label">Drops zum Export auswählen</label>
				<control-multiselect kk-option="exportOption" kk-model="exportDrops.items"></control-multiselect>
			</div>
			<div class="control-group">
				<label class="control-label">Schnellauswahl</label>
				<div class="btn-group">
					<div class="btn btn-mini" ng-click="selectAllForExport()" ng-class="{'active': exportDrops.items.length == core.settings.drops.length}">Alle</div>
					<div class="btn btn-mini" ng-click="selectNoneForExport()" ng-class="{'active': exportDrops.items.length == 0}">Keine</div>
				</div>
			</div>
			<hr style="margin: 1em 0"/>
			<kk-propertygrid kk-model="exportDrops.settings" kk-model-scope="exportDrops" kk-config="exportSettingsConfig"></kk-propertygrid>
		</div>

		<!-- Import -->
		<div ng-show="tab == 'import'" class="form">
			<div ng-if="!importDrops.drops.length">
				<p class="alert alert-info">Wähle einen Export aus, den du importieren möchtest. Du hast dann die Wahl welche Drops aus dem Export du in das System einspielst.</p>
				<div class="btn btn-primary" ng-click="showUploader()"><i class="icon-file-text"></i> Importdatei laden</div>
			</div>
			<div ng-if="importDrops.drops.length > 0">
				<div class="d-inline-block p100">
					<h4 class="pull-left m-0 m-025-top">Export von {{importDrops.user}} - {{getImportDate()}}</h4>
					<span class="form-search pull-right">
						<input class="search ng-pristine ng-valid ng-empty ng-touched" placeholder="Suchen" type="text" ng-model="dropSearch">
					</span>
				</div>
				
				<ul class="nav nav-list ball m-1-top m-1-bottom" style="max-height: 140px; overflow-y: auto">
					<li ng-repeat="drop in importDrops.drops | filter:dropSearch" ng-click="importToggle(drop)" class="import-item">
						<i class="icon-check" ng-if="isImportSelected(drop)"></i><i class="icon-check-empty" ng-if="!isImportSelected(drop)"></i> {{drop._internal.name}}&nbsp;&nbsp;
						<span ng-switch="getImportState(drop)">
							<span class="badge badge-success pull-right" ng-switch-when="new">Neu</span>
							<span class="badge badge-info pull-right" ng-switch-when="update">Geändert</span>
							<span class="badge badge-info pull-right" ng-switch-when="update_older">Älter</span>
							<span class="badge badge-info pull-right" ng-switch-when="update_newer">Neuer</span>
							<span class="badge badge pull-right" ng-switch-when="same">Identisch</span>
							<span class="badge badge pull-right m-05-right" ng-if="importIndex[drop._internal.guid].mediafiles.length">{{importIndex[drop._internal.guid].mediafiles.length}} Bilder</span>
							<span class="badge badge pull-right m-05-right" ng-if="importIndex[drop._internal.guid].storage.length">{{importIndex[drop._internal.guid].storage.length}} Datensätze</span>
						</span>
						<span class="badge badge-important" ng-if="!canImport(drop)">Drop "{{utils.Drops.getTemplateName(drop)}}" nicht ausführbar</span>
					</li>
				</ul>
				<div class="properties">
					<div class="control-group" ng-if="!dropSearch.length">
						<div class="btn-group">
							<div class="btn btn-mini" ng-click="selectAllForImport()" ng-class="{'active': importDrops.selected.length == importDrops.drops.length}">Alle</div>
							<div class="btn btn-mini" ng-click="selectNoneForImport()" ng-class="{'active': importDrops.selected.length == 0}">Keine</div>
						</div>
					</div>
					<kk-propertygrid kk-model="importDrops" kk-model-scope="" kk-config="importSettingsConfig"></kk-propertygrid>
				</div>
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<div class="btn-group tright">
			<div class="btn btn-primary" ng-click="doExport()" ng-class="{'disabled': exportDrops.items.length == 0 || exportDrops.inProgress}" ng-if="tab=='export'"><i ng-class="{'icon-circle-arrow-down': !exportDrops.inProgress, 'icon-spinner': exportDrops.inProgress }"></i> {{exportDrops.items.length}} Drops exportieren</div>
			<div class="btn btn-primary" ng-click="doImport()" ng-class="{'disabled': importDrops.selected.length == 0 || importDrops.inProgress}" ng-if="tab=='import'"><i ng-class="{'icon-circle-arrow-up': !importDrops.inProgress, 'icon-spinner': importDrops.inProgress }"></i> <span ng-if="!importDrops.inProgress">{{importDrops.selected.length}} Drops importieren</span><span ng-if="importDrops.inProgress">{{importDrops.progress}}</span></div>
			<div class="btn" ng-click="abort()">Abbrechen</div>
		</div>
	</div>
</div>