<div class="modal-header tcenter">
	<div class="close" ng-click="close()">×</div>
	<h4>{{template.name}} Sprachvariablen</h4>
	<p>Hier kannst du Sprachvariablen für alle {{template.name}} Drops verwalten.</p>
	<p ng-if="shouldShowSearch()"><input class="search" type="text" ng-model="langSearch.value" placeholder="Suchen"/></p>
	<div class="btn-group" ng-if="environment.getLanguages().length > 1">
		<span class="btn btn-small" ng-repeat="lang in environment.getLanguages()" ng-class="{'active': activeLang.cISO == lang.cISO}" ng-click="setActiveLang(lang)">{{lang.cNameDeutsch}}</span>
	</div>
</div>
<div class="modal-body">
	<div class="form form-horizontal">
		<div class="control-group" ng-repeat="(key, value) in langSettings | filterObj:activeLang.cISO:langSearch.value">
			<label class="control-label">{{key}} <i class="icon-alert icon-remove-sign fadebtn" ng-click="resetToOriginal(key)" kk-tooltip="Zurücksetzen auf Original" ng-show="isUserChanged(key)"></i></label>
			<div class="controls">
				<input type="text" ng-model="value[activeLang.cISO]"/>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="btn-group tright">
		<div class="btn btn-primary" ng-click="close(diff(template.lang, langSettings))">Übernehmen</div>
		<div class="btn" ng-click="close()">Abbrechen</div>
	</div>
</div>
