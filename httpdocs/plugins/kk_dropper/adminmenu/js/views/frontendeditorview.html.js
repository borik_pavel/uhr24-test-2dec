<div id="main-view" class="flex-vertical" ng-cloak>
	<div class="flex-horizontal">
		<!-- Edit windows -->
		<div id="edit-windows"></div>
	</div>
</div>
<div class="frontend-editor-actions" ng-class="{ available: actionsAvailable }">
	<div style="order: 100" class="action abort" ng-click="close(null, false)" kk-tooltip="Änderungen verwerfen"><i class="icon-remove"></i></div>
	<div style="order: 200" class ="action apply" ng-click="apply()" kk-tooltip="{{ edit.persist ? 'Änderungen speichern' : 'Änderungen anwenden' }}"><i class="icon-ok"></i></div>
	<div ng-repeat="btn in customBtns" class="action" ng-class="{'divider divider-right': btn.divider === 'right', 'divider divider-left': btn.divider === 'left'}" ng-style="{'background-color': btn.background, 'color': btn.color, 'order': btn.order, 'width': ((btn.width*2)+'em')}" ng-click="btn.click()" kk-tooltip="{{btn.title}}"><i class="{{btn.icon}}"></i></div>
</div>
<kk-notifications basic="true"></kk-notifications>
