{if !kkDropperOPC::isPreviewRequest()}
<script type="text/javascript">
$(function() { 
	window.kkDropEditor = new kkDropperFrontendEditor({
		isEditMode: {if !empty($isEditMode)}true{else}false{/if},
		shopUrl: '{$ShopURL|default:""}',
		preventNavigation: true
	}); 
	{if $isEditMode}
	window.kkDropEditor.init(function() {
		window.kkDropEditor.setEditorLanguage('{$lang}');
	});
	{/if}
});
</script>
{/if}