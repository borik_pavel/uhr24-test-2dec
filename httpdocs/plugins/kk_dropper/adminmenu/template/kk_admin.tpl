<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="language" content="deutsch, de" />
		<meta name="robots" content="noarchive" />
		<meta name="robots" content="nofollow" />
		<meta name="robots" content="noindex" />

		{* use the admin secret as token for io calls from the admin backend *}
		<meta name="kk-dropper-io-token" content="{$kk_admin_secret}">

		<title>Dropper</title>
		<link type="image/x-icon" href="favicon.ico" rel="icon" />
		<link type="image/x-icon" href="favicon.ico" rel="shortcut icon" />
	</head>

	<body class="{if !empty($dropperFrontendMode)}frontend-mode{/if}">
		<!--[if IE]>
		
		<script type="text/javascript">
		alert('Du benutzt den Internet Explorer! Bei der Nutzung des Internet Explorers kann es leider zu Fehlern in der Darstellung von Dropper kommen. Wir empfehlen daher die Nutzung von Google Chrome oder Firefox als Webbrowser.');
		</script>

		<![endif]-->

		<!-- General Styles -->
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/open-sans.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/bootstrap.min.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/font-awesome.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/select2.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/dragula.min.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/bootstrap-colorpicker.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/generic.css?v={$kk_dropper_version}"/>
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/style.css?v={$kk_dropper_version}"/>
		{if $dropperFrontendMode}
		<link rel="stylesheet" type="text/css" href="{$dropperAdminURL}css/style-fe.css?v={$kk_dropper_version}" />
		{/if}
		
		<div id="js-start"></div>

			<!-- Libs -->
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/scroll-into-view.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/fileuploader.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/jquery-1.9.1.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/jquery-ui.custom.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$oPlugin->cDropsPfadURL}Shared/framework.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$oPlugin->cDropsPfadURL}Shared/libs/post-robot.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/select2.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/ace.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-html.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-drop.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			
			{* NOTE: these will be dynamicly loaded when needed *}
			{*
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-css.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-less.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-sass.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-markdown.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-smarty.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-javascript.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/ace/mode-php.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			*}
			
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/jquery.json-2.3.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/bootstrap.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/angular.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/angular-route.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/angular-dragula.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/datetime-directive.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/oc.lazyload.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/bootstrap-colorpicker.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/libs/jszip.min.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/application.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			
			<!-- Services -->
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/core.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/bootstrap.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/message.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/environment.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/delta.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/state.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/editwindow.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/modal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/datasource.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/media.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/packagemanager.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/upgrade.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/utils.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/services/legacy.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			
			<!-- Controls and Views -->
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/directives.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/controls.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/grid-control.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/productfilter-control.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/history-switcher.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/topbar.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/sidebar.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/controls/dashboard.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/mainview.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/frontendeditorview.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/expired.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			
			<!-- Windows -->
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/windows/dropeditwindow.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/windows/listitemeditwindow.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			
			<!-- Modals -->
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/settingsmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/dropconfigmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/dropcontainermodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/saveconflictmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/newdropmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/dropinfomodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/duplicatemodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/dropremovemodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/mediagallerymodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/importexportmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/insertconditionsmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/urlpickermodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/languagemodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/packagemanagermodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/controlconditionalmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/groupimprovemodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			<script type="text/javascript" data-cfasync="false" src="{$dropperAdminURL}js/views/modals/htmlinsertmodal.js?v={$kk_dropper_version}" charset="UTF-8"></script>
			
		<div id="js-end"></div>

		<script type="text/javascript">

			{if !empty($redirectBackendUrl)}
				alert('Das Protokoll (http / https) des Admin-Backends und dem Shop-Frontend sind unterschiedlich!\nDu wirst jetzt automatisch weitergeleitet, um Cross-Site-Scripting Fehlern vorzubeugen.\n\nWeitere Informationen zu dem Problem:\n\nhttps://kreativkonzentrat.de/Wiki?page=dropper/faq#dropper-backend-wird-nicht-geladen-und-zeigt-eine-fehlermeldung-5ab6');
				window.location.href = '{$redirectBackendUrl}';
			{/if}
			
			var kk_shop_url = "{$shop_url}";
			var kk_plugin_admin_url = "{$pluginAdminUrl}";
			var kk_shop_admin_backend_url = "{$shopAdminBackendUrl}";
			var kk_dropper_path_url = "{$oPlugin->cDropsPfadURL}";
			var kk_admin_url = "{$dropperAdminURL}";
			var kk_admin_user = "{$kk_admin_user}";
			var kk_admin_secret = "{$kk_admin_secret}";
			var kk_cache_id = "{$smarty.now}";
			{if $dropperFrontendMode === false}
				var kk_backend_errors = {$kk_backend_errors_json};
			{else}
				var kk_backend_errors = [];
				{if $kk_backend_errors_json !== '[]'}
				console.warn('Dropper-Backend has errors:', {$kk_backend_errors_json});
				{/if}
			{/if}
			var kk_frontend_mode = {if $dropperFrontendMode}true{else}false{/if};
			var conditional = {ldelim}{rdelim};
			
		</script>

		<div id="content" ng-app="app" ng-controller="AppCtrl">
			<!-- View -->
			<ng-view ng-if="ready" id="view"></ng-view>
			<kk-notifications kk-if="!ready" basic="true"></kk-notifications>
		</div>
	</body>
</html>