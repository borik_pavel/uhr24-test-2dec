# Dokumentation

Mit dem Plain Drop kannst du Inhalte einfach und schnell in deinen Shop einfügen. Dabei hast du die Möglichkeit, **HTML** Formatierungen und auch **Smarty** zu verwenden. Wie das genau funktioniert, erfährst du hier.

## Einstellungen

### Inhalt

Hier kannst du den Inhalt deines Plain Drops eingeben und diesen mit [HTML](http://wiki.selfhtml.org/wiki/Startseite) formatieren. Mehr Informationen zum HTML Control [findest du in unserem Wiki](https://kreativkonzentrat.de/Wiki/dropper/details/controls#cmsanchor-htmlcontrol).

Die Templatesprache [Smarty](http://www.smarty.net/) kannst du ebenfalls im HTML Control verwenden. Wenn du Smarty benötigst, achte darauf, dass du in den Einstellungen auch die Option **Smarty evaluieren** gesetzt hast. Im Standard ist diese Option deaktiviert.
   
### Smarty evaluieren

Möchtest du [Smarty](http://www.smarty.net/) verwenden, um bspw. auf Templatevariablen zuzugreifen, muss diese Option aktiviert sein. Das kann sich jedoch auf die Performance deines Shops auswirken. Bitte lies hierzu die [ausführlichen Informationen in unserem Wiki](https://kreativkonzentrat.de/Wiki/dropper/details/controls#cmsanchor-smartyindropcontrols)!

# Changelog

**1.0.2 - 18.03.2021**

- JTL Shop 5 Support
- Option "Smarty evaluieren" in dein Haupteinstellungsbereich verschoben

**1.0.1 - 28.03.2017**

- Performance: Gecachtes eval nutzen, wenn Smarty evaluieren aktiviert ist