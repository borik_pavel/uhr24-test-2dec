# Dokumentation

Das Vendor Box Drop ermöglicht es dir, beliebig viele Listen von Herstellern in deinen Shop zu integrieren. Wähle verschiedene Templates und bestimme mit wenigen Klicks, welche Herstellerlogos du in Slidern oder welche Hersteller du in einfachen Link-Listen darstellen möchtest. Hier erfährst du, wie das geht.

## Einstellungen

### Als Herstellerfilter verwenden (Artikellistings)

Wenn diese Option aktiviert ist, werden die Hersteller automatisch aus der Herstellerauswahl des Shops generiert und filtern bei einem Klick die aktuelle Artikelliste nach dem gewählten Hersteller.
**Achtung**: Die Vendorbox funktioniert dann nur auf Artikellisten (z.B. Kategorie- / Merkmalseiten). Damit diese Option funktioniert ist es zudem erforderlich dass die **Shopeinstellung 522** `Herstellerfilter benutzen` auf `Ja` gestellt ist.

### Hersteller einschliessen oder ausschliessen

Mit dieser Option wählst du, ob du deine Hersteller explizit angeben möchtest um sie darzustellen oder ob du alle Hersteller deines Shops anzeigen willst und dabei ggf. nur einige ausgeschlossen werden sollen.

### Hersteller

Hier legst du fest, welche Hersteller du anzeigen oder ausblenden möchtest. Wenn du Hersteller explizit einträgst, um Sie darzustellen, kannst du die Sortierung manuell über dieses Control vornehmen.  

### Hersteller ohne Artikel ausschliessen

Lege fest, ob Hersteller die keine Artikel haben nicht von der Auswahl berücksichtigt werden sollen.

### Sortierung der Hersteller

Wenn du Hersteller ausschließen möchtest erscheint diese Option, um die Sortierung der einzelnen Einträge festzulegen. **Hinweis:** wenn du Hersteller explizit angibst, um Sie darzustellen, entfällt diese Option. Die Sortierung kannst du dann über das Hersteller Control manuell vornehmen. 

### Template

Hier kannst du das Template für die Darstellung deiner Hersteller festlegen. Zu Wahl stehen:

* Flach: eine einfache Liste der Herstellerlogos mit Links zu den Herstellerseiten 
* Slider: ein Slider, der zusätzliche Konfigurationsmöglichkeiten bietet
* Liste: eine einfache Liste, die Links zu deinen Herstellerseiten enthält (ohne Bilder)
* Index: ein Markenindex, der Hersteller flach nach Anfangsbuchstaben sortiert anzeigt (ohne Bilder)
* Eigenes: ein eigenes Template

Möchtest du ein eigenes Template umsetzen, kannst du das in der Datei `/templates/user.tpl` tun. In den anderen Dateien in diesem Ordner findest du Hinweise darauf, wie du auf deine Herstellerliste zugreifst und Namen, Logos und Links des entsprechenden Herstellers darstellst.

### Breite Herstellerbilder

Diese Option erscheint, wenn in deinem Template Bilder dargestellt werden. Mit ihr kannst du die Breite der Herstellerlogos festlegen. Die Angabe ist optional: lässt du sie weg, werden die Herstellerbilder in der Größe **normal** dargestellt.

**Beachte**: die Bilder werden nicht wirklich skaliert, sondern ausgehend von deiner Angabe per CSS verkleinert bzw. vergrößert. Bitte prüfe die [Bildeinstellungen in deinem JTL Shop](http://wiki.jtl-software.de/index.php?title=Einstellungen:Bilder), um optimale Ergebnisse zu erhalten. Wir verwenden in den mitgelieferten Templates immer die Bildgröße **normal**, deine angegebene Breite sollte daher unter der Breite der Originalbilder liegen, um einen Qualitätsverlust zu vermeiden.   

### Bilder einbetten

In manchen Fällen kann es aus Gründer der Performance sinnvoll sein, Bilder mit BASE64 zu kodieren und direkt in den Quellcode der Seite einzufügen. Üblicherweise macht dies aber nur bei sehr kleinen Logos und einer großen Anzahl von darzustellenden Herstellern Sinn.

## Slider Template Einstellungen

Wenn du das Slider Template verwendet, kannst du den Slider zusätzlich über folgende Einstellungen konfigurieren.

### Navigation anzeigen

Aktiviere diese Option, um Navigationspfeile links und rechts von den Herstellern zu aktivieren.

### Slider automatisch starten

Wenn du diese Option aktivierst, scrollt der SLider automatisch, nachdem die Seite geladen wurde.

### Pausezeit Autostart 

Wenn du deinen Slider automatisch starten lässt, kannst du mit dieser Option die Dauer der Pause in Millisekunden festlegen, bevor der Slider das nächste Scrollen startet.

### Scrollsteps

Hiermit legst du fest, wieviele Elemente während einer Animation gewechselt werden sollen. 

### Scrollzeit

Hiermit gibst du an, wieviele Millisekunden eine Scroll-Animation dauern soll.

### Umbruch am Ende

Aktivierst du diese Option, springt der Slider zurück zum Anfang wenn das Ende des Sliders erreicht ist und man "vor" navigiert. 
Das funktioniert in beide Richtungen: befindet man sich am Anfang des Sliders und navigiert "zurück", springt der Slider ans Ende.

## Konfiguration

### CSS Container Klassen

Hier kannst du zusätzliche CSS Klassen angeben, die im äußeren Containerelement deines Templates hinzugefügt werden. Trenne Mehrfachnennungen mit einem Leerzeichen.

# Changelog

**1.0.16 - 26.05.2021**

- Verbesserter Support für JTL-Shop 5

**1.0.15 - 15.04.2021**

- Nachricht im Editiermodus / OPC wenn keine Hersteller konfiguriert sind

**1.0.14 - 30.03.2021**

- JTL Shop 5 Support

**1.0.13 - 03.09.2020**

- Vorbereitung auf Shop 5

**1.0.12 - 25.02.2020**

- Feature: Zufällige Sortierung von Herstellern.

**1.0.11 - 15.01.2019**

- Bugfix: PHP Notice behoben, wenn keine CSS Klasse gesetzt war.

**1.0.10 - 11.11.2016**

- Bugfix: Besseres Ladeverhalten (keine Umbrüche) beim Slider Template.

**1.0.9 - 07.11.2016**

- Bugfix: Hersteller ohne Artikel ausschliessen hat Artikellagerbestände nicht beachtet.

**1.0.8 - 21.09.2016**

- Feature: Buchstabenfilternavigation beim "Index" Template.
- Feature: Optimierteres Spaltenlayout beim "Index" Template.

**1.0.7 - 12.08.2016**

- Feature: Konfigurierbare Spaltenanzahl beim "Index" Template.
- Bugfix: Verbessertes Spaltenlayout beim "Index" Template.

**1.0.6 - 06.06.2016**

- Feature: Hersteller ohne Artikel ausschliessen.
- Feature: Als Herstellerfilter verwenden (Auf Artikellisting-Seiten).

**1.0.5 - 07.04.2016**

- Ausgraueffekt entfernt.
- Bugfix: Smarty Warnung behoben.
- Bugfix: Links im "Liste" Template unterstreichen.

**1.0.4 - 19.05.2015**

- Shop 4 Kompatiblität.

**1.0.3 - 19.05.2015**

- neue Darstellung: Markenindex.

**1.0.2 - 10.02.2015**

- custom.tpl umbenannt in user.tpl.
- diverse kleinere Verbesserungen.

**1.0.1 - 12.08.2014**

- Sortierungmethode für "exclude" Modus.
- neue Methoden zum Erstellen der Herstellerobjekte.
- ausblenden der Marken bei Logo-basierten Templates wenn das Bild nicht existiert.
- picture_width als freie Eingabe.