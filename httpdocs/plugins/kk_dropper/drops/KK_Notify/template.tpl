<script type="text/javascript">
	DropperFramework.init('.kk-notifier-{$drop._internal.guid}', function(drop){ldelim}
		setTimeout(function(){ldelim}
			drop.$.slideDown({$drop.animation_time});
		{rdelim}, {$drop.delay});

		{if $drop.close_delay neq "0" or $drop.close_delay eq ""}
			setTimeout(function(){ldelim}
				drop.$.slideUp({$drop.animation_time});
			{rdelim}, ({$drop.delay} + {$drop.close_delay}));
		{/if}

	{rdelim});
</script>

<div class="kk-notifier kk-notifier-{$drop._internal.guid}{if $drop.fixed} fixed{/if}" style="{if $drop.animation_time gt 0}display: none{/if}">
	<div class="kk-notifier-box {if $drop.status eq 'custom'}{$drop.class}{else}box_{$drop.status}{/if}">
		{if isset($drop.heading) and $drop.heading neq ""}<div class="kk-notifier-heading">{if $drop.eval_smarty}{eval var=$drop.heading}{else}{$drop.heading}{/if}</div>{/if}
		<div class="kk-notifier-message">{if $drop.eval_smarty}{eval var=$drop.message}{else}{$drop.message}{/if}</div>
	</div>
</div>