# Dokumentation

Mit dem Flex Menu Drop erstellst du im Handumdrehen ein horizontales Navigationmenü für deinen JTL Shop, das noch dazu responsive ist. Durch zahlreiche Optionen kannst du das Menu genau nach deinen Wünschen gestalten und mit quasi beliebigen Inhalten befüllen. 

Diese Dokumentaion stellt dir die umfangreichen Möglichkeiten des Drops vor.   

## Die Menupunkte

Mit den Menupunkten gestaltest du die erste Ebene deines Menus. Folgende Typen sind möglich: Kategorie, Hersteller, Merkmalwert, Link, Suche, Logo und Warenkorb. 

Du erstellst einen Menupunkt über den Klick auf **+**. Im daraufhin erscheinenden Dialog wählst du den entsprechenden Typ aus und konfigurierst den Meupunkt mit weiteren Controls. 

![Erstellen eines Kategorie Menupunktes](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_add_entry.gif)

Jeder Menupunkt bringt spezielle und allgemeine Einstellungen mit. Ein Kategorie-Menupunkt bietet bspw. die Auswahl der Kategorie an, ein Hersteller-Menupunkt die Auswahl eines Herstellers usw.
Diese Einstellungen sind weitestgehend selbsterklärend. Auf einige Besonderheiten der allgemeinen Optionen gehen wir in den folgenden Abschnitten ein.

### Organisation der Menupunkte

Mit einem Klick in die Titelleiste des Menueintrages, kannst du zur besseren Übersicht im Backend ein gesonderten Titel für diesen Eintrag festlegen.
Durch die Wahl der Option **Manueller Titel** kannst du zusätzlich einen eigenen Menupunktnamen vergeben, der dann als Titel des Menupunktes im Frontend verwendet wird. 
Das **Umsortieren der Menupunkte** gelingt einfach per Drag'n'Drop.

![Umbenennung und Sortierung von Menupunkten](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_organize_entries.gif)

### Grundlegende optische Anpassungen

Mit der **Ausrichtung** legst du die Orientierung deines Menupunktes fest. Den Warenkorb oder auch die Suche kannst du so bspw. rechts im Menu anordnen, alle anderen Einträge links (Standard).

Mit der Option **Icon verwenden** kannst du wahlweise ein Bild (Upload) oder ein Font-Icon vor deinen Menueintrag setzen. 
Evo, das Standard Template im JTL-Shop 4, bringt mit [Font Awesome bereits zahlreiche Icons](https://fortawesome.github.io/Font-Awesome/icons/) mit. 
Solltest du dieses Template verwenden, setze für **Font Icon CSS Klasse** den Wert `fa fa-home`, um vor deinem Menupunkt ein Haus als Icon einzufügen.    

Schließlich kannst du deinen Menupunkten noch gesonderte **CSS Klassen** zuweisen, um diese [gezielt stylen](https://kreativkonzentrat.de/Wiki/dropper/details/styling#cmsanchor-anpassungenpercss) zu können.

Solltest du deinen Menupunkt nur unter bestimmten Bedingungen anzeigen wollen, z.B. für eine bestimmte Kundengruppe, kann dir die Option **Darstellungsfilter** weiterhelfen. Diese erlaubt es dir, ähnlich wie bei einem Drop selbst, die Darstellung des Menupunktes feingranular mit Filtern zu steuern.

Übrigens: das Flex Menu Drop bringt eine **smarte Designerkennung** mit. Schriftgrößen und Farben deines Menus passen sich mit einem Klick an die Optik deines Shops an. 
Mehr dazu im Abschnitt Konfiguration.

## Inhalte der Menupunkte

Zu den Menupunkten Kategorie, Hersteller, Merkmalwert und Link kannst du zusätzlich Inhalte wie Beschreibungen, Bilder oder Unterkategorien hinzufügen, die dann im Inhalt des Dropdowns bei Aktivierung des Menupunktes erscheinen.   

![Inhalte bei Menupunkten hinzufügen und verschieben](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_entry_content.gif)

Du kannst die Inhalte deines Menupunktes durch einen Klick auf das Plus Icon in einen der 5 vorgegebenen Bereiche hinzufügen und diese später auch per Drag'n'Drop umsortieren.   
Freiräume füllt das Drop übrigens automatisch, es entstehen im Frontend also keine unschöne Lücken. Beachte: es sind sogar mehrere Inhalte in einem Bereich möglich! Die Einzelnen Inhalte werden automatisch für dich responsive optimal angeordnet.

Es gibt verschiedene Arten von Inhalten, einige sind auch nur bei bestimmten Menupunkten möglich: 

Menuinhalt | Wo möglich? | Beschreibung 
---------- | ----------- | ---------
Beschreibung | überall| eine Beschreibung mit Bild, Titel und (HTML-) Text
HTML Inhalt | überall | beliebiger HTML-Inhalt und **Drops**
Linkliste | überall | einzelne Links oder eine komplette Linkgruppe
Kategorieliste | überall | eine Liste bestimmter Kategorien
Herstellerliste | überall | eine Liste bestimmter Hersteller bzw. Marken
Merkmalwertliste | überall | eine Liste aller Werte eines bestimmten Merkmals
Unterkategorien | Kategorie | die Unterkategorien der Kategorie 
Hersteller | Kategorie, Merkmalwert | Herstellerfilter des Menupunktes
Merkmalwerte | Kategorie, Hersteller | Merkmalfilter des Menupunktes
Kategorien | Hersteller, Merkmalwert | Kategoriefilter des Menupunktes
Spezialseite | Kategorie, Hersteller, Merkmalwert | Spezialseitenfilter des Menupunktes

### Konfiguration der Inhalte

Die Konfiguration der Inhalte unterscheidet sich stark nach deren Art, ist aber weitestgehend selbsterklärend. Bitte beachte hier auch die Tooltips im Flex Menu Drop selbst. Einige Anwendungsfälle werden wir nachfolgend jedoch detaillierter beschreiben.

Bei umfangreichen Inhalten ist auch das Verhalten auf mobilen Geräten wichtig. Erfahre dazu mehr im Abschnitt **Responsives Verhalten**. 

### Unterkategorien

Der wohl gängigste Anwendungsfall für Inhalte von Kategorie-Menupunkten sind Unterkategorien. Für diese kannst du verschiedene Darstellungsformen wählen: Bild und Titel, Nur Bild, Linkliste, Slider und Index.

![Verschiedene Dartstellungsarten der Unterkategorien im Flex Menu Backend](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Unterkategorien_Darstellung.gif)

**Einfache Darstellung mit Bild und Titel**

![Unterkategorien im Flex Menu mit Bildern und Titeln](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Unterkategorien_bild-titel.jpg)

**Unterkategorien als Slider**

![Unterkategorien im Flex Menu als Slider](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Unterkategorien_slider.jpg)

**Ein Index für umfangreiche Kategorielisten**

![Unterkategorien im Flex Menu als Index](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Unterkategorien_index.jpg)

Je nach **Darstellungsform** der Unterkategorien unterscheiden sich auch deren Einstellungen, insbesondere in Sachen Kategorietiefe: bei der **Linkliste** können über die Einstellung **Tiefe** auch Unterkategorien der zweiten Ebene angezeigt werden. 

**Linkliste mit zweiter Kategoriebene im Back- und Frontend**

![Unterkategorien bis zur Tiefe 2 mit der Linkliste im Backend](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Unterkategorien_Backend.png)

![Unterkategorien mit Tiefe 2 im Frontend](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Unterkategorien_linkliste.jpg)

### Kombinationsinhalte 

Mit sogenannten **Kombinationsinhalten** kannst du mit wenigen Klicks in gefilterte Artikellisten (also Kategorien, Hersteller oder Merkmalwerte) verlinken. 

Ein Beispiel: du führst viele verschiedenfarbige Artikel eines Herstellers. Bei diesen Artikeln hast du die Farbe als Merkmal gepflegt. Erstellst du nun im Hersteller-Menupunkt den Kombinationsinhalt **Merkmalwerte** mit dem Merkmal Farbe, erzeugt das Flex Menu direkt Links auf diese Herstellerseite mit dem aktiven Farbfilter, bspw. `/Hersteller__Blau`.

![Kombinationsinhalte Merkmalwerte und Kategorien in verschiedenen Darstellungsformen](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Kombiinhalte.png)

Die **Darstellungsform** von Kombinationsinhalten kannst du ebenfalls bestimmen. Im obigen Beispiel siehst du für die Merkmalwerte die Darstellung **Nur Bild** und für die Kategorieliste **Linkliste**, auch möglich sind **Bild und Titel** sowie **Slider**.

Abschließend alle Kombinationationsinhalte mit einigen Beispiel-URLs (Kategorien: Schuhe, Hersteller: Nike, Merkmalwert: Farbe = rot, Spezialseite: Bestseller):

Menupunkt | Kombinationsinhalt | Beispiel URL
--------- | ------------------ | -----------------
Kategorie | Hersteller | `/Schuhe::Nike`
Kategorie | Merkmalwerte | `/Schuhe__rot`
Kategorie | Spezialseite | `/Schuhe?qf=1`
Hersteller | Kategorien | `/Nike:Schuhe`
Hersteller | Merkmalwerte | `/Nike__rot`
Hersteller | Spezialseite | `/Nike?qf=1`
Merkmal | Kategorien | `/rot:Schuhe`
Merkmal | Hersteller | `/rot::Nike`
Merkmal | Spezialseite | `/rot?qf=1`
Merkmal | Merkmalwerte | `/Made-In-Germany__rot`
Spezialseite | Kategorien | `/Sonderangebote:Schuhe`
Spezialseite | Hersteller | `/Sonderangebote::Nike`
Spezialseite | Merkmalwerte | `/Sonderangebote__rot`

**Tipp:** wenn du Kombinationsinhalte nutzt, prüfe bitte auch, ob die Indizierungseinstellungen des JTL-Shops deinen Anforderungen entspricht. Durch die teils identischen Ergebnislisten (im Beispiel: `/Schuhe::Nike` und `/Nike:Schuhe`) ist die Gefahr von Duplicate Content groß, weshalb der JTL-Shop viele der Seiten von der Indizierung durch Suchmaschinen ausschließt.

### Boxendarstellung der Inhalte

Jeder Inhalt kann optional mit einer betitelten Box umschlossen werden. Auch bei umfangreichen Inhalten kannst du so Struktur und Übersichtlichkeit bewahren.

![Boxendarstellung und eigene CSS Klassen](https://kreativkonzentrat.de/storedocs/img/drops/Flex-Menu_Boxendarstellung.gif)

Mit der Angabe eigener **CSS Klassen** für die Box hast du außerdem die Möglichkeit, dein Inhalt gezielt per CSS zu stylen. Die Mitgabe dieser CSS Klassen funktioniert übrigens auch, wenn die Box keinen Titel und keinen Rahmen hat, also für den Besucher unsichtbar ist.  

### Beliebige Drops verlinken

Über den Menuinhalt **HTML Inhalt** ist es dir möglich, beliebige Drops in deinem Flex Menu Dropdown zu verlinken! 
Ziehe dazu dein Drop einfach per Drag'n'Drop aus der Sidebar in den gewünschten Bereich.

![Ein Article Slider Drop in ein Flex Menu Dropdown einfügen](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_entry_content_drop.gif)

## Responsives Verhalten

Bei der Entwicklung des Flex Menu Drops haben wir viel Wert auf ein optimales responsives Verhalten gelegt. 
Die Menupunkte bringen bereits gute Voreinstellungen mit, wir bieten dir aber an zahlreichen Stellen die Möglichkeit, das Verhalten der Menueinträge und Inhalte zu konfigurieren. 

Wichtig zu wissen ist hierbei, dass das Flex Menu Drop immer versuchen wird, möglichst viele Menueinträge in **einer Zeile** darzustellen.
Wie man die Sichtbarkeiten der Einträge auch auf kleineren Bildschrimen steuert erfährst du in den nächsten Abschnitten. 

### Anzeigepriorität im Menupunkt

Die Anzeigepriorität eines Menupunktes legt dessen Verhalten fest, falls nicht mehr genügend Raum vorhanden sein sollte, um alle Punkte deines Menus anzuzeigen. 
Folgende Optionen sind möglich:

* Sehr Niedrig: der Menupunkt wird im Standard immer versteckt 
* Niedrig: falls die Breite nicht mehr ausreicht, um alle Menupunkte **gleichzeitig** darzustellen, wird dieser Menupunkt, zusammen mit allen anderen Menupunkten, die ebenfalls eine niedgrige Anzeigepriorität haben, versteckt
* Mittel: wird versteckt, falls der Platz nur noch für Menupunkte mit hoher Anzeigepriorität ausreicht oder zu viele Menupunkte mit mittlerer Priorität existieren
* Hoch: das Flex Menu Drop versucht, den Menupunkt solange wie möglich sichtbar zu halten

"Versteckt" bedeutet in diesem Zusammenhang übrigens nicht, dass der Menupunkt gänzlich verschwindet. Er wird nur in ein Alternativmenu verschoben, 
dessen Inhalte über eine spezielles [Hamburger-Menu-Icon](https://de.wikipedia.org/wiki/Hamburger-Men%C3%BC-Icon) erreichbar ist.   

![Alternatives Menu im Flex Menu Drop](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_hamburger.gif)

### Sichtbarkeiten eines Menupunktes

Neben der Anzeigepriorität können noch spezielle Bedingungen zur Sichtbarkeit des Menupunktes definiert werden. 
So ist es möglich, bestimmte Punkte nur auf mobilen Geräten oder bei angeheftetem Menu anzeigen zu lassen. Sollten die hier gegebenen Konditionen nicht zutreffen, wird der Menupunkt ausgeblendet und ist dann auch nicht mehr im Alternativmenu auffindbar.

![Sichtbarkeiten am Beispiel eines Sucheintrages](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_visibilities.png)

Der Menupunkt Warenkorb ist im Standard bspw. so konfiguriert, dass er nur auf mobilen Geräten oder bei angehefteten Menus angezeigt wird. 

### Dropdown Verhalten

Auf mobilen Geräten ist häufig nicht der Platz vorhanden, um die mitunter komplexen Inhalte in den Dropdowns deiner Menueinträge anzuzeigen.  
Im Konfigurationsbereich des Mega Menu Drops findest du daher die Einstellung **Dropdowns anzeigen auf**, mit der du die Gerätetypen bestimmen kannst, auf dem Dropdowns angezeigt werden sollen.

## Konfiguration

In der Konfiguration des Flex Menu Drops kannst du einige allgemeine Einstellungen festlegen, die in Reiter aufgeteilt sind. 

### Allgemein

Solltest du das Evo Template (Standard im JTL-Shop 4) verwenden, kannst du das von JTL ausgelieferte Flex Menu mit der Option **Standard Menu entfernen?** entfernen.

Ein **Caching** bieten wir an, jedoch zunächst nur auf **experimentieller Basis**. Aktiviere die Option **Caching nutzen?** also nur, wenn du dir sicher bist und das Verhalten ausgiebig getestet hast. 
Und insbesondere nur dann, wenn die Inhalte deines Menus sich auf verschiedenen Shopseiten nicht ändern. Ein Beispiel hierfür wären in Menuinhalten verlinkte Drops, deren Sichtbarkeit zusätzlich über Darstellungsfilter gesteuert werden.

### Dropdowns

Im Abschnitt zum Responsiven Verhalten spielte dieser Reiter bereits eine Rolle. Hier konfigurierst du das responsive Verhalten und einige optische Feinheiten der Dropdowns deiner Menueinträge.

Mit der Option **Dropdowns anzeigen auf** legst du fest, auf welchen Endgeräten Dropdowns angezeigt werden sollen. Bei mobilen Geräten bietet es sich evtl. an, diese auszublenden.

Du kannst sogar **Dropdown Inhalte via Ajax laden** lassen. Das ist besonders dann sinnvoll, wenn du viele (große) Inhalte in den Dropdowns deines Flex Menus anzeigen lässt: mit aktivierten Option werden die Inhalte erst bei der Aktivierung des Menueintrags geladen und die initiale Ladezeit deiner Seite veringert sich dadurch. 

Schließlich hast du noch die Möglichkeit, den Typ der **Dropdown Animation** und die Werte für die **Einblendeverzögerung** und **Ausblendeverzögerung** der Dropdowns anzupassen.
 
### Verhalten

In diesem Reiter kannst du über die Option **Menu beim Scrollen anheften?** steuern, ob dein Flex Menu am oberen Bildschrim angeheftet werden soll, sobald der User auf der Seite herunterscrollt. Wähle die Gerätetypen aus, auf welchen das Verhalten gewünscht ist.

![Mitlaufendes Flex Menu](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_fixed.gif)

Beachte, dass du über die Option Sichtbarkeit in jedem Menupunkt gezielt steuern kannst, ob dieser bei einem angehefteten Menu angezeigt oder ausgeblendet werden soll.   

### Design

Im Reiter Design wartet eine besonders clevere Hilfe auf dich. Mit einem Klick auf den Button **Designerkennung starten** wird das Flex Menu Drop dein Template untersuchen und optisch passende Schriftarten, Farben und Schriftgrößen setzen.    
Natürlich kannst du die gesetzten Einstellungen noch weiter optimieren und mit deinen Wunschwerten ersetzen. 

![Designerkennung in der Drop Konfiguration](https://kreativkonzentrat.de/storedocs/img/drops/mega_menu_design.gif)

# Changelog

**1.0.25 - 02.09.2021**

- Bugfix: Option "Leere Kategorien ignorieren" war fehlerhaft. 

**1.0.24 - 24.08.2021**

- Bugfix: Behebt Probleme mit Ajax-Menus im Checkout (Shop 5)

**1.0.23 - 18.05.2021**

- Feature: Verbesserter Icon-Picker für Menupunkte.
- SEO: Eigene Icon-Font entfernt. Das Menu nutzt jetzt die in Dropper integrierten SVG-Icons.
- Bugfix: Option 'Leere Kategorien ignorieren?' funktionierte im Shop 5 nicht.

**1.0.22 - 10.05.2021**

- Template-Zone '@page.header.flexMenu' wird als Standard Einfügemethode für neue Menus genutzt.

**1.0.21 - 15.04.2021**

- Nova: Burger Menu entfernen
- Bugfix: Suchspezialfilter Kombinationsinhalte funktionierten nicht mehr
- Bugfix: Linkgruppen Menuinhalte funktionierten nicht mehr
- Bugfix: Einheitliche Dropper Cache-Infrastruktur nutzen

**1.0.20 - 30.03.2021**

- JTL Shop 5 Support
- Verbessertes Scrollverhalten in mobilen Aufklappmenus

**1.0.19 - 03.09.2020**

- Feature: Frontend-Editor Support

**1.0.18 - 12.11.2019**

- Bugfix: HTML-Menuinhalte die mit Smarty ausgewertet werden und ein Drop verlinken (via {drop ...}) waren fehlerhaft.
- Feature: Verbesserte Erkennung aktiver Links

**1.0.17 - 23.10.2019**

- Feature: Neue Kategorieattribute "kk_mega_menu_name" und "kk_mega_menu_class" um Kategorietitel für das Menu zu überschreiben und CSS-Klassen auf dargestellten Kategorien zu setzen.

**1.0.16 - 23.05.2019**

- Feature: Neue Option um das Menu auf der Seite zu zentrieren (Benötigt ein bootstrapfähiges Template).

**1.0.15 - 08.05.2019**

- Bugfix: Behebt einen Javascript-Fehler, wenn Breadcrumb-Einträge nicht valide sind.

**1.0.14 - 08.05.2019**

- Bugfix: Behebt ein Problem mit falschen Umbrüchen bei großen Grid-Inhalten.

**1.0.13 - 22.10.2018**

- Bugfix/ Performance: Neues Berechnungsmodell für das Verstecken und Einblenden von Menüeinträgen. Behebt Darstellungsfehler in Grenzfällen und verbessert die Performance bei der Berechnung.

**1.0.12 - 12.10.2018**

- Bugfix: Fehlerhafte Layoutberechnung, wenn die Menuschriftart (Webfont) verzögert geladen wurde.

**1.0.11 - 22.08.2018**

- Feature: Kombinationsinhalt "Merkmalwerte" ist jetzt mit dem Menupunkt "Merkmalwert" kombinierbar.

**1.0.10 - 16.07.2018**

- Feature: Platzierung des Hamburgermenu-Buttons ist jetzt einstellbar (links | rechts).
- Bugfix: Menuinhalte nicht via Ajax laden, wenn die Einstellung inaktiv ist.

**1.0.9 - 13.07.2018**

- Bugfix: Menuinhalte wurden nicht via Ajax geladen, wenn der Wartungsmodus aktiv war.
- Performance: Pre-Fetching aller Menuinhalte, wenn diese per Ajax geladen werden sollen.

**1.0.8 - 22.06.2018**

- Bugfix: Behebt einen kritischen Smarty-Fehler in Kategorielisten, der durch Version 1.0.7 eingeführt wurde.

**1.0.7 - 21.06.2018**

- Feature: In Kategorielisten, Merkmalwertlisten und Herstellerlisten kann jetzt angegeben werden, ob Einträge ausgeblendet werden sollen, falls keine Artikel dazu gefunden werden.
- Feature: Neuer Menupunkt "Spezialseite" für Sonderseiten des Shops, z.B. Bestseller. Kombinationsinhalte (Hersteller, Merkmale, Kategorien) sind mit diesem Menupunkt möglich!
- Feature: Neuer Kombinationsinhalt "Spezialseite", z.B. Bestseller, um deinen Menupunkt nach "Suchspecials" zu filtern. Möglich für Menupunkte vom Typ Kategorie, Hersteller und Merkmalwert.
- Feature: Menupunkt "Warenkorb" kann jetzt ein Dropdown mit Inhalten haben.
- Feature: Die Sichtbarkeit von Menupunkten kann jetzt zusätzlich durch Darstellungsfilter eingeschränkt werden. Das ermöglicht beispielsweise die Anzeige von Menupunkten für bestimmte Kundengruppen.
- Bugfix: Wenn eine Kategorie-Menueintrag nicht sichtbar war, aber ein Icon angegeben hatte, wurde der Eintrag trotzdem angezeigt.
- Bugfix: Aktive Menueinträge werden besser berechnet (z.B. aktiver Kategorie-Menupunkt, wenn ein Artikel in der Kategorie angeschaut wird).
- Bugfix: Automatische Designerkennung unter Firefox schlug fehl.

**1.0.6 - 05.02.2018**

- Bugfix: Aufklappmenus wurden bei bestimmten Custom-Stylings des Menus nicht mehr angezeigt

**1.0.5 - 29.01.2018**

- Feature: Automatische Aktualisierung des Warenkorb Menupunktes, wenn ein Artikel via Ajax in den Warenkorb gelegt wurde
- Feature: Das Label des Hamburgermenus kann in den Spracheinstellungen des Drops festgelegt werden
- Performance: Scolling-Optimierungen auf mobilen Geräten mit angeheftetem Menu
- Performance: Geschwindigkeitsoptimierungen beim Aufbau des Menus mit Javascript
- Bugfix: Suche hat auf Android-Geräten sporadisch den Fokus verloren
- Bugfix: Initialisierungsfehler auf Seiten, die keine Breadcrumb hatten
- Bugfix: Alt-Attribut für Bilder in Menupunkten wird gesetzt

**1.0.4 - 13.10.2016**

- Bugfix: Manuelle Dropdownbreite wurde ignoriert

**1.0.3 - 22.08.2016**

- Feature: Einstellung, leere Kategorien (Kategorien ohne Artikel und Unterkategorien) nicht zu betrachten wurde eingeführt.
- Feature: Dropdown Ausrichtung kann festgelegt werden und die Dropdownbreite beschränkt werden
- Feature: Menupunkttyp "Konto" hinzugefügt
- Bugfix: Effizienteres Javascript Layouting
- Bugfix: Sortierung von Kategorien zweiter Ebene wurde nicht beachtet
- Bugfix: Z-Index Probleme bei zwei aufeinanderfolgenden Flex Menus behoben

**1.0.2 - 22.04.2016**

- Bugfix: Seite war kaputt, wenn der Menueintrag Warenkorb genutzt wurde bei PHP < v5.5

**1.0.1 - 18.04.2016**

- Feature: Sticky Verhalten ist jetzt separat nach Gerätetyp (Mobiltelefon, Tablet, Desktop) aktivierbar
- Bugfix: Kleinere Layoutingfehler im Firefox behoben

**1.0.0 - 18.04.2016**

- erstes Release