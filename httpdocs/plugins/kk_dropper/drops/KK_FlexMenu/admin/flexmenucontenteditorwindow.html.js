<button type="button" class="close" ng-click="close()">&times;</button>
<h4 class="m-0-top"><i class="{{provider.icon}} m-05-right"></i>{{provider.name}} konfigurieren</h4>
<div class="light">{{provider.description}}</div>

<ul class="nav nav-tabs m-15-top">
	<li class="" ng-class="{'active': contentTab == 'settings'}" ng-click="setContentTab('settings')"><a>Einstellungen</a></li>
	<li class="" ng-class="{'active': contentTab == 'display'}" ng-click="setContentTab('display')"><a>Boxendarstellung</a></li>
</ul>
<div class="clearfix"></div>
<div class="tab-content">
	<div class="tab-pane active" kk-if="contentTab=='settings'">
		<kk-propertygrid class="form clearfix" kk-model="content.settings" kk-model-scope="content.settings" kk-config="provider.settings"></kk-propertygrid>
	</div>
	<div class="tab-pane active" kk-if="contentTab=='display'">
		<kk-propertygrid class="form clearfix" kk-model="content" kk-model-scope="content" kk-config="contentSettings"></kk-propertygrid>
	</div>
</div>