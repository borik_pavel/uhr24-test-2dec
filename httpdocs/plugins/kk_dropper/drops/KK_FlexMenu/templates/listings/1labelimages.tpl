{if !empty($items)}
{if !empty($settings.imageSize) and $settings.imageSize|lower !== 'auto'}{assign var="imageSize" value=$settings.imageSize|cat:"px"}{else}{assign var="imageSize" value="none"}{/if}
{if !empty($settings.titleTruncate)}{assign var="titleTruncate" value=$settings.titleTruncate}{else}{assign var="titleTruncate" value="1000"}{/if}
<div class="block-menu kk-fm-listing--center-wrapper">
	<div class="flex-block kk-fm-listing--labelimages kk-fm-listing {if !empty($settings.gridUse) and $settings.gridUse eq true}kk-fm-grid {if isset($settings.gridDistribute) and $settings.gridDistribute eq true}kk-fm-grid-distribute{/if} {if isset($settings.gridCenter) and $settings.gridCenter eq true}kk-fm-grid-center{/if}{/if}">
		{foreach from=$items item="item"}
		<a class="kk-fm-link kk-fm-listing--item kk-fm-grid-item {if !empty($item.type)}kk-fm-listing--item-{$item.type}{/if} {if !empty($item.type) && !empty($item.id)}kk-fm-listing--item-{$item.type}-{$item.id}{/if} {if !empty($item.class)}{$item.class}{/if}" href="{$item.url}" title="{$item.title}">
			<div class="kk-fm-listing--item-wrapper">
				<img src="{$drop.actions->helper->getSuitedImage($item, $imageSize)}" class="kk-fm-listing--item-image kk-fm-img" alt="{$item.name}" style="max-height: {$imageSize}"/><br/>
				<span class="kk-fm-listing--item-name">{$item.name|truncate:$titleTruncate:"..."}</strong>
			</div>
		</a>
		{/foreach}
		
		<div class="related-prods">
		
		<div class="product-wrapper product-wrapper-product text-center-util slick-slide slick-active" itemprop="about" itemscope="" itemtype="http://schema.org/Product" style="width: 270px;" data-slick-index="1" aria-hidden="false">
                    <a href="https://u23974fz.test3.jtl-hosting.de/swatch/color-square-suon125/" tabindex="0">
     <div class="item-slider productbox-image square square-image">
                <div class="inner">
                                                                                            
                <img src="https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/sm/suon125.jpg" srcset="https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/xs/suon125.jpg 30w,  https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/sm/suon125.jpg 210w, https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/md/suon125.jpg 420w, https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/lg/suon125.jpg 600w" data-srcset="https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/xs/suon125.jpg 30w,  https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/sm/suon125.jpg 210w, https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/md/suon125.jpg 420w, https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/lg/suon125.jpg 600w" data-sizes="auto" class="product-image 
             img-fluid                                                                         lazyautosizes lazyloaded" alt="COLOR SQUARE" sizes="238px">
    
                    <meta itemprop="image" content="https://u23974fz.test3.jtl-hosting.de/media/image/product/114860/md/suon125.jpg">
                    <meta itemprop="url" content="https://u23974fz.test3.jtl-hosting.de/swatch/color-square-suon125/">                </div>
            </div>
</a>
<a href="https://u23974fz.test3.jtl-hosting.de/swatch/color-square-suon125/" tabindex="0">
    <span class="item-slider-desc text-clamp-2"><span itemprop="name">COLOR SQUARE</span></span>
</a>

<div class="item-slider-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
            <div class="price_wrapper">
<div class="price productbox-price special-price">
<span>61,49 €<span class="footnote-reference">*</span></span>
</div>
</div>
                            </div>
                </div>
		
		
		</div>
		
	</div>
</div>
{/if}




<style>
.block-menu {
max-height: 500px;
    min-height: 300px;
    display: flex;
    flex-wrap: nowrap;
    align-content: center;
    justify-content: center;
    
}

.flex-block {
width: 70%;
}

.related-prods {


}
</style>