{if !empty($items)}
{if !empty($settings.imageSize) and $settings.imageSize|lower !== 'auto'}{assign var="imageSize" value=$settings.imageSize|cat:"px"}{else}{assign var="imageSize" value="none"}{/if}
{if !empty($settings.titleTruncate)}{assign var="titleTruncate" value=$settings.titleTruncate}{else}{assign var="titleTruncate" value="1000"}{/if}
<div class="kk-fm-listing--center-wrapper">
	<div class="kk-fm-listing--labelimages kk-fm-listing {if !empty($settings.gridUse) and $settings.gridUse eq true}kk-fm-grid {if isset($settings.gridDistribute) and $settings.gridDistribute eq true}kk-fm-grid-distribute{/if} {if isset($settings.gridCenter) and $settings.gridCenter eq true}kk-fm-grid-center{/if}{/if}">
		{foreach from=$items item="item"}
		<a class="kk-fm-link kk-fm-listing--item kk-fm-grid-item {if !empty($item.type)}kk-fm-listing--item-{$item.type}{/if} {if !empty($item.type) && !empty($item.id)}kk-fm-listing--item-{$item.type}-{$item.id}{/if} {if !empty($item.class)}{$item.class}{/if}" href="{$item.url}" title="{$item.title}">
			<div class="kk-fm-listing--item-wrapper">
				<img src="{$drop.actions->helper->getSuitedImage($item, $imageSize)}" class="kk-fm-listing--item-image kk-fm-img" alt="{$item.name}" style="max-height: {$imageSize}"/><br/>
				<span class="kk-fm-listing--item-name">{$item.name|truncate:$titleTruncate:"..."}</strong>
			</div>
		</a>
		{/foreach}
	</div>
</div>
{/if}