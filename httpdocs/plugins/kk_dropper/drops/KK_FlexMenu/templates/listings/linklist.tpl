{if !empty($items)}
{if !empty($settings.titleTruncate)}{assign var="titleTruncate" value=$settings.titleTruncate}{else}{assign var="titleTruncate" value="1000"}{/if}
{assign var="hasSecondLevel" value=$drop.actions->helper->oneHasSubitems($items)}
<div class="kk-fm-listing--linklist {if !empty($settings.gridUse) and $settings.gridUse eq true}kk-fm-grid {if isset($settings.gridDistribute) and $settings.gridDistribute eq true}kk-fm-grid-distribute{/if} {if isset($settings.gridCenter) and $settings.gridCenter eq true}kk-fm-grid-center{/if}{/if}" data-grid-max-columnwidth="220">
	{foreach from=$items item="item"}
		{if (isset($presort) and $presort eq true and !empty($item.items)) or (empty($presort) or $presort eq false)}
		<div class="kk-fm-listing--linklist-level0 kk-fm-grid-item {if !empty($item.type)}kk-fm-listing--item-{$item.type}{/if} {if !empty($item.items)}kk-fm-listing--linklist-has-children{/if} {if !empty($item.type) && !empty($item.id)}kk-fm-listing--item-{$item.type}-{$item.id}{/if} {if !empty($item.class)}{$item.class}{/if}">
			<a class="kk-fm-listing--linklist-label kk-fm-link {if $hasSecondLevel}kk-fm-section-heading{/if}" href="{$item.url}" title="{$item.title}">{$item.name|truncate:$titleTruncate}</a>
			{if !empty($item.items)}
			<div class="kk-fm-listing--linklist-group">
				{assign var="subitemCount" value=0}
				{foreach from=$item.items item="subitem"}
					{if empty($settings.subcatLimit) or $subitemCount < $settings.subcatLimit}
					<div class="kk-fm-listing--linklist-level1 {if !empty($subitem.type)}kk-fm-listing--item-{$subitem.type}{/if} {if !empty($subitem.type) && !empty($subitem.id)}kk-fm-listing--item-{$subitem.type}-{$subitem.id}{/if} {if !empty($subitem.class)}{$subitem.class}{/if}">
						<a class="kk-fm-listing--linklist-label kk-fm-link" href="{$subitem.url}" title="{$subitem.title}">{$subitem.name|truncate:$titleTruncate}</a>
					</div>
					{/if}
					{math equation="a+1" a=$subitemCount assign="subitemCount"}
				{/foreach}
				{if !empty($settings.subcatLimit) and $subitemCount gt $settings.subcatLimit}
					<div class="kk-fm-listing--linklist-level1 {if !empty($subitem.type)}kk-fm-listing--item-{$subitem.type}{/if} kk-fm-listing--item-more">
						{assign var="subitemCount" value=count($item.items)}
						<a class="kk-fm-listing--linklist-label kk-fm-link" href="{$item.url}" title="{$item.title}">{"show-all-subcats"|i18n:$subitemCount}</a>
					</div>
				{/if}
			</div>
			{/if}
		</div>
		{/if}
	{/foreach}
	{foreach from=$items item="item"}
		{if isset($presort) and $presort eq true and empty($item.items)}
		<div class="kk-fm-listing--linklist-level0 kk-fm-grid-item {if !empty($item.type)}kk-fm-listing--item-{$item.type}{/if} {if !empty($item.items)}kk-fm-listing--linklist-has-children{/if} {if !empty($item.type) && !empty($item.id)}kk-fm-listing--item-{$item.type}-{$item.id}{/if} {if !empty($item.class)}{$item.class}{/if}">
			<a class="kk-fm-listing--linklist-label kk-fm-link {if $hasSecondLevel}kk-fm-section-heading{/if}" href="{$item.url}" title="{$item.title}">{$item.name|truncate:$titleTruncate}</a>
		</div>
		{/if}
	{/foreach}
</div>
{/if}