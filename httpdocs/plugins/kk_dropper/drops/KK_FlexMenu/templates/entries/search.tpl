<span class="kk-fm-entry-label">
	<form class="kk-fm-search--wrapper" action="navi.php" method="GET">
		<input type="text" spellcheck="false" name="suchausdruck" class="ac_input kk-fm-search--input" placeholder="{lang section='global' key='search'}"/>
		<span class="kk-fm-search-icon" onclick="$(this).closest('form').submit()">{'kk-icon-fa5-search'|drop_icon}</span>
	</form>
</span>