{assign var="fmCartItemCount" value=$WarenkorbArtikelPositionenanzahl}
<a class="kk-fm-entry-label kk-fm-link {if !empty($fmCartItemCount) && $fmCartItemCount gt 0}kk-fm-cart-filled{/if}" href="warenkorb.php" title="{lang section='global' key='basket'}" data-parent-toggled-class="text-primary">
	{include file="templates/entries/sub/icon.tpl"|drop_tpl entry=$entry}{if !empty($WarenkorbArtikelPositionenanzahl) and $entry.showArticleCount eq true}<sup class="kk-fm-cart-count">{$fmCartItemCount}</sup>{/if}{if $entry.showPrice eq true} <span class="kk-fm-cart-value">{$WarensummeLocalized[$NettoPreise]}</span>{/if}{if $menu.hasDropdown}</span><span class="kk-fm-caret-down">{'kk-icon-material-arrow_drop_down'|drop_icon}</span>{/if}
</a>
