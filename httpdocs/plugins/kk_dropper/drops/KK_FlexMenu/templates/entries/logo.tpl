<a class="kk-fm-entry-label kk-fm-link" {if !empty($entry.item.url)}href="{$entry.item.url}"{/if}  title="{if !empty($entry.item.title)}{$entry.item.title}{/if}">
	{include file="templates/entries/sub/icon.tpl"|drop_tpl entry=$entry}
</a>