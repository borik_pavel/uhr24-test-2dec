{if isset($entry.iconType) and $entry.iconType !== 'none'}
	{if $entry.iconType === 'image' and !empty($entry.icon)}<img class="kk-fm-entry-icon" src="{$entry.icon}" alt="{if !empty($entry.item.title)}{$entry.item.title|escape}{/if}"/>{/if}
	{if $entry.iconType === 'font' and !empty($entry.fontIcon)}<span class="kk-fm-entry-font-icon">{$entry.fontIcon}</span>{/if}
{/if}