{assign var="subcats" value=$drop.actions->helper->getSubCategories($entry.item.id, $settings.levels, $settings.sort, !empty($settings.skipEmptyCategories))}
{if !empty($subcats)}
	{include file="templates/listings/"|cat:$settings.template|cat:".tpl"|drop_tpl items=$subcats presort=$settings.presortSubcats}
{/if}