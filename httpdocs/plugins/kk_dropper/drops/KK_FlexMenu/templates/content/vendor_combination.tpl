{assign var="vendors" value=$drop.actions->helper->getItemVendors($entry.item)}

{if !empty($vendors)}
	{assign var="vendors" value=$drop.actions->helper->sortShopObjects($vendors, $settings.sort)}
	{include file="templates/listings/"|cat:$settings.template|cat:".tpl"|drop_tpl items=$vendors}
{/if}