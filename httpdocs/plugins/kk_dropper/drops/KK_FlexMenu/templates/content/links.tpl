{assign var="fmLinks" value=false}
{if $settings.linkSource eq 'linkgroup' and isset($settings.linkgroup)}
	{assign var="fmLinks" value=$drop.actions->helper->getLinkgroupLinks($settings.linkgroup)}
{/if}
{if $settings.linkSource eq 'manual' and !empty($settings.links)}
	{assign var="fmLinks" value=$drop.actions->helper->getShopObjects('link', $settings.links)}
{/if}

{if !empty($fmLinks) and $fmLinks !== false}
	{assign var="fmLinks" value=$drop.actions->helper->sortShopObjects($fmLinks, $settings.sort)}
	{include file="templates/listings/"|cat:$settings.template|cat:".tpl"|drop_tpl items=$fmLinks}
{/if}