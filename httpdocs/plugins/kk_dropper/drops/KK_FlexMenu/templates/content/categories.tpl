{assign var="subcats" value=$drop.actions->helper->getShopObjects('category', $settings.categories, !empty($settings.skipEmptyCategories))}
{if !empty($subcats)}
    {$drop.actions->helper->resolveSubCategories($subcats, $settings.levels, $settings.sort, !empty($settings.skipEmptyCategories))}
    {assign var="subcats" value=$drop.actions->helper->sortShopObjects($subcats, $settings.sort)}
	{include file="templates/listings/"|cat:$settings.template|cat:".tpl"|drop_tpl items=$subcats presort=$settings.presortSubcats}
{/if}