{* Render mobile interaction helper *}
<div class="kk-fm-submenu--info">
	<a class="kk-fm-link kk-fm-submenu--info-goto" {if !empty($entry.item.url)}href="{$entry.item.url}"{/if} {if !empty($entry.item.title)}title="{$entry.item.title}"{/if}>{if !empty($entry.item.url)}{if !empty($entry.item.name)}{"submenu-goto"|i18n:$entry.item.name}{else}{"submenu-goto"|i18n:$entry.item.title}{/if}&nbsp;{'kk-icon-uil-angle-double-right'|drop_icon:'1.25em'}{else}{$entry.item.name}{/if}</a>
	<div class="kk-fm-submenu--close">&times;</div>
</div>
{* render content boxes *}
<div class="kk-fm-boxarea">
{foreach from=$content.boxes key="boxId" item="box"}
	{if !empty($box)}
		{capture name="mmBoxCapture" assign="mmBoxCapture"}
			{foreach from=$box item="boxContent"}
				{capture name="mmBoxContentCapture" assign="mmBoxContentCapture"}
					{include file="templates/content/"|cat:$boxContent.provider|cat:".tpl"|drop_tpl menu=$menu entry=$menu.entry content=$menu.content box=$box content=$boxContent settings=$boxContent.settings}
				{/capture}
				{if !$drop.actions->helper->isEmptyContent($mmBoxContentCapture)}
				<div class="kk-fm-content-wrapper">
					<div class="kk-fm-content kk-fm-content-{$boxContent.provider} {if $boxContent.framed eq true}kk-fm-content-framed{else}kk-fm-content-unframed{/if} {if !empty($boxContent.classes)}{$boxContent.classes}{/if}">
						{if $boxContent.showTitle eq true and !empty($boxContent.contentTitle)}
							<div class="kk-fm-content-title"><span class="kk-fm-section-heading">{$boxContent.contentTitle}</span></div>
						{/if}
						<div class="kk-fm-content-body {if isset($boxContent.noPadding) and $boxContent.noPadding eq true}kk-fm-content-body--nopadding{/if}">{$mmBoxContentCapture}</div>
					</div>
				</div>
				{/if}
			{/foreach}
		{/capture}

		{if !$drop.actions->helper->isEmptyContent($mmBoxCapture)}
		<div class="kk-fm-box kk-fm-box-{$boxId}">
			{$mmBoxCapture}
		</div>
		{/if}
	{/if}
{/foreach}
</div>