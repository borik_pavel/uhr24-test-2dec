{capture name="kkmmEntryLabel" assign="kkmmEntryLabel"}{include file=$drop.actions->getEntryTemplate($menu.entry)|drop_tpl menu=$menu entry=$menu.entry content=$menu.content}{/capture}
{if !$drop.actions->helper->isEmptyContent($kkmmEntryLabel)}
<div class="kk-fm-entry kk-fm-entry-type-{$menu.entry.type} {if !empty($menu.entry.class)}{$menu.entry.class}{/if} {if !empty($menu.entry.item.id)}kk-fm-entry-{$menu.entry.type}-{$menu.entry.item.id}{/if} {if !empty($menu.entry.item.class)}{$menu.entry.item.class}{/if} {if $menu.hasDropdown eq true}kk-fm-toggle kk-fm-entry-has-submenu{/if}{if !empty($menu.entry.visibility)}{foreach from=$menu.entry.visibility item='visibleClass'} kk-fm-entry-{$visibleClass}{/foreach}{/if}" data-menu-group="{$group}" data-menu-guid="{$menu.guid}" data-menu-priority="{$menu.entry.priority}" data-menu-sort="{$sort}">
	{$kkmmEntryLabel}
	{if $menu.hasDropdown}
		{include file="templates/base/submenu.tpl"|drop_tpl menu=$menu entry=$menu.entry content=$menu.content}
	{/if}
</div>
{/if}