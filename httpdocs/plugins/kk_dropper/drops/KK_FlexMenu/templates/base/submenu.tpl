{if $content.visible eq true}
{assign var="dropdownSized" value=false}{if isset($content.dropdownAlignment) and $content.dropdownAlignment neq 'full' and !empty($content.dropdownMaxWidth)}{assign var="dropdownSized" value=true}{/if}

<div class="kk-fm-submenu {if $dropdownSized}kk-fm-submenu-sized{/if}" data-dropdown-align="{if $dropdownSized}{$content.dropdownAlignment}{else}full{/if}" data-guid="{$menu.guid}" style="{if $dropdownSized}max-width:{$content.dropdownMaxWidth}px;{/if}">
	{if $drop.ajaxMenus eq true}
		<div class="kk-fm-submenu-loader"></div>
		<div class="kk-fm-submenu-ajax-content"></div>
	{else}
		{$drop.actions->renderSubmenuContent($menu.guid)}
	{/if}
</div>
{/if}