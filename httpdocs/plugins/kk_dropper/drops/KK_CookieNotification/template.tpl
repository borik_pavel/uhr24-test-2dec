{if $drop.actions->shouldDisplayNotification()}
	{include file="templates/style.tpl"|drop_tpl}
	{include file="templates/script.tpl"|drop_tpl}
{else}
	<!-- KK_CookieNotification: already dismissed by user and no opt-out possible: no need to render cookie notification drop -->
{/if}