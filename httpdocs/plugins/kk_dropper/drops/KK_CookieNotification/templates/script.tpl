<script type="text/javascript">

	// init cookieconsent
	window.addEventListener("load", function() {ldelim}
		
		// get cookie consent config from drop config
		var cookieConsentConfig = {ldelim}
			
			"message": "{'message'|i18n}",
			"dismiss": "{'button-text'|i18n}",
			"deny": "{'deny-text'|i18n}",
			"link": "{'link-text'|i18n}",
			"href": "{if $drop.showLink && !empty($drop.link.url)}{$drop.link.url}{/if}",
			"theme": "{$drop.theme}",
			"position": "{$drop.position}",
			"isStatic": {if $drop.position === "top" && $drop.doPushdown === true}true{else}false{/if},
			"showLink": {if !$drop.showLink || empty($drop.link.url)}false{else}true{/if},
			"expiryDays": {if !empty($drop.expiryDays) && $drop.expiryDays|intval > -1}{$drop.expiryDays}{else}365{/if},
			"bodyClass": "kk-cookie-notification kk-cookie-notification-{$drop._internal.guid} cc-window {if !empty($drop.cssClass)}{$drop.cssClass}{/if}",
			"dismissBtnClass": "cc-btn cc-dismiss {if !empty($drop.cssClassOkButton)}{$drop.cssClassOkButton}{/if}",
			"denyBtnClass": "cc-btn cc-deny {if !empty($drop.cssClassDenyButton)}{$drop.cssClassDenyButton}{/if}",
			"type": {if $drop.offerOptOut === true}"opt-out"{else}"info"{/if}

		{rdelim};
		
		// get needless cookies from drop config
		var needlessCookieStr = "{$drop.needlessCookies}";
		
		// create & init cookieNotification and therefore cookie consent 
		var cookieNotification = new KK_CookieNotification(cookieConsentConfig, needlessCookieStr);

	{rdelim});
	
</script>