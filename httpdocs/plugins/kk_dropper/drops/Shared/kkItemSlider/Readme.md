# Itemslider

## Features

A responsive slider for your templates. It's main use-case is when you want to display several items in a row and make them slideable.
The Itemslider will automaticly adjust the size and number of visible Items, based on your configuration options, making it insanely responsive.

## Prerequisites

Make sure to include kkItemSlider.js and kkItemSlider.css into your document.

## Initialization

To intialize your slider build a DOM structure like this:

~~~html
<div class="my-container">
	<div class="my-item">
	... // item contents here
	</div>
    <div class="my-item">...</div>
    <div class="my-item">...</div>
</div>
~~~

You can also use any other html element like ul and li's here, but i've taken 'div' because it is not so much styled already.

Next step: init your Slider:

~~~js

$(function(){
	new kkItemSlider({
			containerSelector: ".my-container",		// selector of the root container
			itemSelector: ".my-item",				// selector of the items inside the container
			minItemSize: 200,						// min width or height of items. if smaller, amount of visible items are reduced
			maxItemScroll: 10,						// maximum amount of items that are overscrolled, e.g. 1 if you only want to scroll by one item at a time
			scrollTime: 300,						// scroll Time in milliseconds
			showNavigation: true,					// should we see the navigation arrows?
			clickToggle: false,						// does clicking switch to the next slides?
			autoStart: false,						// should the slider automaticly rotate?
			autoStartSwitchDelay: 4000,				// delay between switches when autostart is enabled
			allowOverscroll: false					// when at the end of the queue, should "next" start at the beginning?
		});	
});

~~~

That's it. Your slider should now work.


## Container Styling

The Slider tries to respect your Container styling as much as possible (margin, padding, width). So to layout the Slider, you are best suited when simply layouting your Container:

~~~css

.my-container{
	padding: 20px; /* some breathing room */
	margin: 10px 0;
	width: 300px;	
}

~~~

## Item Styling

Don't go too deep into styling the item-layouts themselve. The alignment is done for you autmaticly. All you have to do is to set a width for your items.

~~~css

.my-item{
	width: 25%; /* align 4 items by default... you can also use px here */
	background: red;
	...
}

~~~

## Set an Item to be visible on init

Sometimes you want an item to displayed in the slider when it is initialized, e.g. when you have a selected item. To do this add the attribute `data-start-item` to the item. When set this item is scrolled to after the Slider is being initialized.

~~~html

<div class="my-container">
	<div class="my-item">
	... // item contents here
	</div>
    <div class="my-item">...</div>
    <div class="my-item" data-start-item>...</div>
</div>

~~~