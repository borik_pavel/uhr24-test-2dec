{$dropInstance = $portlet->getDropInstance($instance)}
<div 
	class="drop-portlet {$instance->getAnimationClass()}" 
	style="{$instance->getStyleString()}"
	data-portlet-drop="{$dropInstance->getDropGuid()}"
	{$instance->getAnimationDataAttributeString()}
	{if $isPreview}
	id="{$instance->getUid()}"
	data-portlet-uid="{$instance->getUid()}"
	data-portlet="{$instance->getDataAttribute()}"
	{/if}
>
{if !$dropInstance->hasValidDropperLicense()}
	{include 
		file=$portlet->getBasePath()|cat:"templates/error.tpl" 
		portlet=$portlet instance=$instance 
		dropInstance=$dropInstance 
		error="Keine gültige Dropper-Lizenz gefunden. Drop-Portlet deaktiviert."
	}
{else if $isPreview}
	{include 
		file=$portlet->getBasePath()|cat:"templates/preview.tpl" 
		portlet=$portlet 
		instance=$instance 
		dropInstance=$dropInstance
}
{else}
	{include 
		file=$portlet->getBasePath()|cat:"templates/live.tpl" 
		portlet=$portlet 
		instance=$instance 
		dropInstance=$dropInstance
	}
{/if}
</div>