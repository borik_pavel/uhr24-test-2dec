{foreach $dropperOPC.styles as $style}
<link rel="stylesheet" href="{$style}"/>
{/foreach}
{foreach $dropperOPC.scripts as $script}
<script type="text/javascript" src="{$script}"></script>
{/foreach}


<script type="text/javascript">
new kkDropperOpcIntegration({
		dropperVersion: '{$dropperOPC.dropperVersion}',
		shopUrl: '{$dropperOPC.paths.shopUrl}',
		pageId: '{$dropperOPC.pageId}',
		lang: '{$dropperOPC.langIso}'
});
</script>