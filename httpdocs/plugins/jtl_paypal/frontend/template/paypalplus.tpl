{if !empty($hinweis)}
    <div class="alert alert-danger">{$hinweis}</div>
{/if}
{row}
    {col cols=12}
        {card bg-variant="wrap"}
            <fieldset>
            {if !empty($cFehler)}
                <div class="alert alert-danger">{$cFehler}</div>
            {/if}
            <div id="pp-plus">
                <div id="ppp-container"></div>
            </div>
            </fieldset>
            {if !$embedded}
                {block name="checkout-payment-options-body"}
                <form id="zahlung" method="post" action="{get_static_route id='bestellvorgang.php'}" class="form">
                    {if isset($jtl_paypal_token)}
                        {$jtl_paypal_token}
                    {/if}
                    <fieldset>
                        <ul class="list-group">
                            {foreach $Zahlungsarten as $zahlungsart}
                                <li id="{$zahlungsart->cModulId}" class="list-group-item">
                                    <div class="radio">
                                        <label for="payment{$zahlungsart->kZahlungsart}" class="btn-block">
                                            <input name="Zahlungsart" value="{$zahlungsart->kZahlungsart}" type="radio" id="payment{$zahlungsart->kZahlungsart}"{if $Zahlungsarten|@count == 1} checked{/if}{if $zahlungsart@first} required{/if}>
                                                {if $zahlungsart->cBild}
                                                    <img src="{$zahlungsart->cBild}" alt="{$zahlungsart->angezeigterName|trans}" class="vmiddle">
                                                {else}
                                                    <strong>{$zahlungsart->angezeigterName|trans}</strong>
                                                {/if}
                                            {if $zahlungsart->fAufpreis != 0}
                                                <span class="badge pull-right">
                                                {if $zahlungsart->cGebuehrname|has_trans}
                                                    <span>{$zahlungsart->cGebuehrname|trans} </span>
                                                {/if}
                                                {$zahlungsart->cPreisLocalized}
                                                </span>
                                            {/if}
                                            {if $zahlungsart->cHinweisText|has_trans}
                                                <p class="small text-muted">{$zahlungsart->cHinweisText|trans}</p>
                                            {/if}
                                        </label>
                                    </div>
                                </li>
                            {/foreach}
                        </ul>
                        <input name="Zahlungsart" value="0" type="radio" id="payment0" class="hidden d-none" checked="checked">
                        <input type="hidden" name="zahlungsartwahl" value="1" />
                    </fieldset>
                </form>
                {/block}
            {/if}
        {/card}
    {/col}
{/row}

<div class="modal modal-center fade" id="ppp-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2 id="pp-loading-body"><i class="fa fa-spinner fa-spin fa-fw"></i> {lang key="redirect"}</h2>
            </div>
        </div>
    </div>
</div>

{inline_script}<script>
{literal}
var $submit   = $('form input[type="submit"], form button[type="submit"]', '#checkout');
var payments  = 'input[name="Zahlungsart"]';
var $payForm  = $submit.parents('form:first');
var tsProduct = null;
var ppConfig  = {
    approvalUrl: {/literal}"{$approvalUrl}"{literal},
    placeholder: 'ppp-container',
    mode: {/literal}"{$mode}"{literal},
{/literal}
{if $mode === 'sandbox'}
    showPuiOnSandbox: true,
{/if}
{literal}
    buttonLocation: 'outside',
    preselection: 'paypal',
    disableContinue: function() {
        if (ppActive()) {
            $(payments + ':first')
                .prop('checked', true);
        }
    },
    enableContinue: function() {
        $('#payment0')
            .prop('checked', true);
    },
    showLoadingIndicator: true,
    language: {/literal}"{$language}",{literal}
    country: {/literal}"{$country}",{literal}
    onContinue: function() {
        if (ppIsThirdParty()) {
            var paymentMethod = ppGetThirdPartyMethod(ppp.getPaymentMethod());
            if (paymentMethod) {
                url = paymentMethod.redirectUrl;
                window.location.href = url + '&params=' + encodeURIComponent($payForm.serialize());
                return;
            } else {
                PAYPAL.apps.PPP.doCheckout();
            }
        } else {
            $('#ppp-modal').modal();
            $submit.attr('disabled', true);

            let params = {
                s: {/literal}"{$linkId}"{literal},
                a: 'payment_patch',
                id: {/literal}"{$paymentId}"{literal},
                params: $payForm.serialize()
            };
            $.get("index.php", params)
                .done(function() {
                    PAYPAL.apps.PPP.doCheckout();
                })
                .fail(function(res) {
                    var $ppModal = $('#ppp-modal');
                    $submit.attr('disabled', false);
                    $ppModal
                        .find('.modal-content')
                        .replaceWith($(res.responseText));
                    $ppModal.modal('handleUpdate');
                });
        }
    }
{/literal}
{if $styles}
    ,styles: {$styles|@json_encode}
{/if}
{if $thirdPartyPaymentMethods|@count > 0}
    ,thirdPartyPaymentMethods: {$thirdPartyPaymentMethods|@json_encode}
{/if}
{literal}
};

var ppIsThirdParty = function() {
    return (ppp.getPaymentMethod().substr(0, 3) !== 'pp-');
};

var ppGetThirdPartyMethod = function(name) {
    for (var p in ppConfig.thirdPartyPaymentMethods) {
        var payment = ppConfig.thirdPartyPaymentMethods[p];
        if (payment.methodName === name) {
            return payment;
        }
    }
    return null;
};

var ppActive = function() {
    return !parseInt($(payments + ':checked').val());
};

var ppp = null;

$(document).ready(function() {
    jtl_paypal().loadPaymentWall(function() {
        try {
            ppp = PAYPAL.apps.PPP(ppConfig);

            $payForm.on('submit', function () {
                if (!ppActive()) {
                    return true;
                }
                ppp.doContinue();
                return false;
            });

            $(payments).change(function() {
                ppp.deselectPaymentMethod();
            });
        }
        catch (e) {
            if (console) {
                console.error(e.message);
            }
        }
    });
});
{/literal}
</script>{/inline_script}
