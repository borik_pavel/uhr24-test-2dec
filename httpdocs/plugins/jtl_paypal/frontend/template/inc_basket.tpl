{inline_script}
<script>
    var method = '{$pqMethodCart}',
        selector = '{$pqSelectorCart}';

    $(function () {
        _push_paypalexpress();
        $(document).on('evo:loaded.io.request', function () {
            _push_paypalexpress();
        });
    });

    function _push_paypalexpress() {
        $('.btn-ppe-cart-container').each(function (i, item) {
            if ($(item).parents('#paypalexpress-basket').length === 0) {
                $(item).remove();
            }
        });
        if ($(selector).length > 0) {
            $(selector)[method]($('#paypalexpress-basket').html());
        }
    }
</script>
{/inline_script}

<div id="paypalexpress-basket" style="display: none">
    <li class="btn-ppe-cart-container">
        <a href="index.php?s={$link->getID()}&jtl_paypal_checkout_cart=1" class="paypalexpress btn-ppe-cart-popup">
            <img src="{$ppCheckout}" alt="{$oPlugin->getMeta()->getName()}"/>
        </a>
    </li>
</div>
