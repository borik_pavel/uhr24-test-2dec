<div class="modal modal-center fade" id="ppp-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2 id="pp-loading-body"><i class="fa fa-spinner fa-spin fa-fw"></i> {$l10n['jtl_paypal_finalize_order']}</h2>
            </div>
        </div>
    </div>
</div>

{inline_script}<script>
$(function() {
    var submitted = false;
    $('#complete_order').on('submit', function() {
        submitted = true;
        $(this).find('input[type="submit"]')
            .addClass('disabled')
            .attr('disabled', true);
        var $ppModal = $('#ppp-modal');
        $ppModal.modal({
            backdrop: 'static'
        });
        $ppModal.modal('show');
        history.pushState(null, null, '{$reloadUrl}');

        return true;
    });
});
</script>{/inline_script}
