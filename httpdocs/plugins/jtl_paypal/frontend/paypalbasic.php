<?php

namespace Plugin\jtl_paypal;

use JTL\Shop;
use Plugin\jtl_paypal\paymentmethod\PayPalBasic;
use Plugin\jtl_paypal\paymentmethod\PayPalHelper;

$paypal = new PayPalBasic();
$type   = $_GET['t'] ?? null;

switch ($type) {
    case 's':
        $return = isset($_GET['r']) && (int)$_GET['r'] > 0;
        $helper = Shop::Container()->getLinkService();
        if ($return === true) {
            $token   = $_GET['token'] ?? null;
            $payerID = $_GET['PayerID'] ?? null;
            $result  = $paypal->getExpressCheckoutDetails($token);

            $_SESSION['Zahlungsart'] = $paypal->zahlungsartsession();
            PayPalHelper::addSurcharge();

            \header('Location: ' . $helper->getStaticRoute('bestellvorgang.php'));
        } else {
            \header('Location: ' . $helper->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1');
        }

        break;
    default:
        break;
}
