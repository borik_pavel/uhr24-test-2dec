<?php

namespace Plugin\jtl_paypal;

use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_paypal\paymentmethod\PayPalExpress;
use Plugin\jtl_paypal\paymentmethod\PayPalHelper;
use stdClass;

/** @global PluginInterface $oPlugin */
/** @global string $step */

$helper = Shop::Container()->getLinkService();

if (!isset($_GET['jtl_paypal_redirect'])) {
    $base = $helper->getStaticRoute('warenkorb.php');
    if (isset($_GET['return'])) {
        \header('Location: ' . $base . '?return=1&jtl_paypal_redirect=1');
    } else {
        \header('Location: ' . $base . '?jtl_paypal_redirect=1');
    }
    exit;
}

require_once \PFAD_ROOT . \PFAD_INCLUDES . 'mailTools.php';
require_once \PFAD_INCLUDES . 'bestellvorgang_inc.php';
require_once \PFAD_INCLUDES . 'registrieren_inc.php';

$paypal = new PayPalExpress();

if (isset($_GET['return']) && $_GET['return'] === '1') {
    $session  = Frontend::getInstance();
    $conf     = Shop::getConfig([\CONF_KUNDEN]);
    $response = $paypal->GetExpressCheckoutDetails($_SESSION['reshash']['Token']);

    unset($_SESSION['Versandart'], $_SESSION['AktiveVersandart']);

    $_SESSION['reshash']['Token']   = $response->Token;
    $_SESSION['reshash']['Payer']   = $response->PayerInfo->Payer;
    $_SESSION['reshash']['PayerID'] = $response->PayerInfo->PayerID;

    $name   = PayPalHelper::extractName($response->PayerInfo->Address->Name);
    $street = PayPalHelper::extractStreet($response->PayerInfo->Address->Street1);
    $phone  = \current(\array_filter([
        $response->ContactPhone,
        $response->PayerInfo->ContactPhone,
        $response->PayerInfo->Address->Phone
    ]));

    $_POST['anrede']       = '';
    $_POST['vorname']      = $name->first;
    $_POST['nachname']     = $name->last;
    $_POST['strasse']      = $street->name;
    $_POST['hausnummer']   = $street->number;
    $_POST['tel']          = $phone;
    $_POST['adresszusatz'] = $response->PayerInfo->Address->Street2;
    $_POST['bundesland']   = $response->PayerInfo->Address->StateOrProvince;
    $_POST['plz']          = $response->PayerInfo->Address->PostalCode;
    $_POST['ort']          = $response->PayerInfo->Address->CityName;
    $_POST['land']         = $response->PayerInfo->Address->Country;
    $_POST['email']        = $response->PayerInfo->Payer;

    if ($conf['kunden']['kundenregistrierung_abfragen_anrede'] === 'Y') {
        $_POST['anrede'] = 'm';
    }

    if ($conf['kunden']['kundenregistrierung_abfragen_firma'] === 'Y') {
        $_POST['firma'] = 'NOCOMPANY';
    }

    if ($conf['kunden']['kundenregistrierung_abfragen_ustid'] === 'Y') {
        $_POST['ustid'] = 'NOVATID';
    }

    if ($conf['kunden']['kundenregistrierung_abfragen_geburtstag'] === 'Y') {
        $_POST['geburtstag'] = '01.01.1970';
    }

    $customer = null;

    if (Frontend::getCustomer()->getID() > 0) {
        $customer = Frontend::getCustomer();
        if (isset($customer->cSperre) && $customer->cSperre === 'Y') { //customer is blocked
            \header('Location: ' . $helper->getStaticRoute('warenkorb.php') . '?fillOut=ppexpress_blocked');
            exit;
        }

        if (isset($customer->cAktiv) && $customer->cAktiv === 'N') { //customer is not active
            \header('Location: ' . $helper->getStaticRoute('warenkorb.php') . '?fillOut=ppexpress_inactive');
            exit;
        }
    } else {
        unset($_SESSION['Kunde']);

        $customer = \getKundendaten($_POST, 0, 0);

        $customer->kKundengruppe = Frontend::getCustomerGroup()->getID();
        $customer->kSprache      = Shop::getLanguageID();
        $customer->cAbgeholt     = 'N';
        $customer->cAktiv        = 'Y';
        $customer->cSperre       = 'N';
        $customer->nRegistriert  = 1;
        $customer->dErstellt     = \date_format(\date_create(), 'Y-m-d');

        if (!isset($customer->cAnrede) || $customer->cAnrede === null) {
            $customer->cAnrede = '';
        }

        $session->setCustomer($customer);
        \setzeLieferadresseAusRechnungsadresse();
    }

    $step = 'Zahlung';

    $_SESSION['Zahlungsart']                 = $paypal->zahlungsartsession();
    $_SESSION['paypalexpress']               = $_SESSION['paypalexpress'] ?? new stdClass();
    $_SESSION['paypalexpress']->sZahlungsart = $_SESSION['Zahlungsart'];
    $_POST['Zahlungsart']                    = $_SESSION['Zahlungsart']->kZahlungsart;
    $_POST['zahlungsartwahl']                = '1';

    \pruefeZahlungsartwahlStep($_POST);

    //workaround since the session is deleted in pruefeZahlungsartwahlStep()

    $_SESSION['Zahlungsart']  = $_SESSION['paypalexpress']->sZahlungsart;
    $_POST['Zahlungsart']     = $_SESSION['Zahlungsart']->kZahlungsart;
    $_POST['zahlungsartwahl'] = '1';

    PayPalHelper::addSurcharge();

    \header('Location: ' . $helper->getStaticRoute('bestellvorgang.php') . '?refresh=1');
    exit;
}

$products = PayPalHelper::getProducts();

$min = $oPlugin->getConfig()->getValue('kPlugin_' . $oPlugin->getID() . '_paypalexpress_min');
$max = $oPlugin->getConfig()->getValue('kPlugin_' . $oPlugin->getID() . '_paypalexpress_max');
$sum = Frontend::getCart()->gibGesamtsummeWarenExt([\C_WARENKORBPOS_TYP_ARTIKEL], true);

if ($max > 0 && $max < $sum) {
    \header('Location: ' . $helper->getStaticRoute('warenkorb.php') . '?fillOut=ppexpress_max&max=' . $max);
    exit;
}

if ($min > 0 && $min > $sum) {
    \header('Location: ' . $helper->getStaticRoute('warenkorb.php') . '?fillOut=ppexpress_min&min=' . $min);
    exit;
}

if ($paypal->zahlungErlaubt($products) === false) {
    \header('Location: ' . $helper->getStaticRoute('warenkorb.php') . '?fillOut=ppexpress_notallowed');
    exit;
}

$paypal->prepareExpressCheckout();
