<?php

namespace Plugin\jtl_paypal;

use JTL\Checkout\Bestellung;
use JTL\Plugin\Helper;
use JTL\Shop;
use Plugin\jtl_paypal\paymentmethod\PayPalBasic;
use Plugin\jtl_paypal\paymentmethod\PayPalExpress;
use Plugin\jtl_paypal\paymentmethod\PayPalHelper;

\ob_start();
\error_reporting(0);
\ini_set('display_errors', 0);

$bootstrapper = __DIR__ . '/../../../includes/globalinclude.php';

$exit = static function ($error = false) {
    \ob_end_clean();
    \http_response_code(($error !== true) ? 200 : 503);
    exit;
};

if (!\file_exists($bootstrapper)) {
    $exit(true);
}

require_once $bootstrapper;

Shop::Container()->getLogService()->debug('PayPal Notify: Received an IPN from ' . $_SERVER['REMOTE_ADDR']);

$oPlugin = Helper::getPluginById('jtl_paypal');

if ($oPlugin === null) {
    Shop::Container()->getLogService()->error("PayPal Notify: Plugin 'jtl_paypal' not found");
    $exit(true);
}

$payment = null;

switch (@$_GET['type']) {
    case 'basic':
        $payment = new PayPalBasic();
        break;

    case 'express':
        $payment = new PayPalExpress();
        break;
}

if ($payment === null) {
    Shop::Container()->getLogService()->error('PayPal Notify: Missing payment provider');
    $exit(true);
}

$mode   = \ucfirst($payment->getMode());
$notify = $payment->handleNotify();
$result = $notify->getRawData();

$r = \print_r($result, true);
$payment->doLog("PayPal Notify: GetRawData:\n\n<pre>" . $r . '</pre>');

if ($payment->isLive() === true && $notify->validate() === false) {
    $payment->doLog('PayPal Notify: Validation failed', \LOGLEVEL_ERROR);
    $exit(true);
}

$paymentStatus = $result['payment_status'];
$txId          = $notify->getTransactionId();
$invoice       = $result['invoice'];

$payment->doLog('PayPal Notify: (' . $mode . ") Received new status '" . $paymentStatus
    . "' for transaction '" . $txId . "'");

$orderId = $result['custom'];
$order   = new Bestellung((int)$orderId);
$order->fuelleBestellung(false, 0, false);

if ((int)$order->kBestellung === 0) {
    $payment->doLog("PayPal Notify: Order id '" . $orderId . "' not found", \LOGLEVEL_ERROR);
    $exit(503);
}

// validation
if (!\in_array((int)$order->cStatus, [\BESTELLUNG_STATUS_OFFEN, \BESTELLUNG_STATUS_IN_BEARBEITUNG], true)) {
    // order status has already been set
    $exit();
}

switch ($paymentStatus) {
    case 'Completed':
        $payment->addIncomingPayment($order, (object)[
            'fBetrag'          => $result['mc_gross'],
            'fZahlungsgebuehr' => $result['mc_fee'],
            'cISO'             => $result['mc_currency'],
            'cEmpfaenger'      => $result['receiver_email'],
            'cZahler'          => $result['payer_email'],
            'cHinweis'         => $notify->getTransactionId(),
        ]);

        $diffAmount = \abs((float)$order->fGesamtsummeKundenwaehrung - (float)$result['mc_gross']);

        if ($diffAmount <= 1) {
            $payment->setOrderStatusToPaid($order);
            $payment->doLog("PayPal Notify: Order status changed to 'paid'");
        } else {
            $payment->doLog('PayPal Notify: Order payment has been received');
        }
        break;

    case 'Declined':
        PayPalHelper::sendPaymentDeniedMail($order->oKunde, $order);
        break;
}

$exit();
