<?php

namespace Plugin\jtl_paypal;

\ob_start();
\error_reporting(0);
\ini_set('display_errors', 0);

require_once \PFAD_ROOT . \PFAD_INCLUDES . 'bestellabschluss_inc.php';

use Exception;
use JTL\Helpers\Text;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use PayPal\Api\Error;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\Payment;
use PayPal\Api\WebhookEvent;
use Plugin\jtl_paypal\paymentmethod\PayPalHelper;
use Plugin\jtl_paypal\paymentmethod\PayPalPlus;

/**
 * @param int  $code
 * @param null $content
 */
function _exit($code = 500, $content = null)
{
    $headers = [
        200 => 'OK',
        400 => 'Bad Request',
        500 => 'Internal Server Error',
    ];
    if (!\array_key_exists($code, $headers)) {
        $code = 500;
    }
    \header(\sprintf('%s %d %s', $_SERVER['SERVER_PROTOCOL'], $code, $headers[$code]));
    if (\is_string($content)) {
        \ob_end_clean();
        echo $content;
    }
    exit;
}

/**
 * @param $to
 */
function _redirect($to)
{
    \header(\sprintf('location: %s', $to));
    exit;
}

if (!isset($_GET['a'])) {
    _exit(400);
}

$api        = new PayPalPlus();
$apiContext = $api->getContext();
$action     = $_GET['a'] ?? '';

/** @global PluginInterface $oPlugin */

switch ($action) {
    case 'payment_method':
        if (!isset($_GET['id'])) {
            _exit(400);
        }

        Shop::setPageType(\PAGE_BESTELLVORGANG);

        require_once \PFAD_ROOT . \PFAD_INCLUDES . 'bestellvorgang_inc.php';

        $_POST['zahlungsartwahl'] = 1;
        \parse_str($_GET['params'] ?? '', $params);
        $params = Text::filterXSS($params);
        \pruefeZahlungsartwahlStep([
            'Zahlungsart'     => (int)$_GET['id'],
            'zahlungsartwahl' => 1,
        ]);
        \pruefeVersandartWahl($api->getPaymentId(), $params);

        _redirect(Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php'));
        break;

    case 'payment_patch':
        if (!isset($_GET['id'])) {
            _exit(400);
        }

        $_POST['zahlungsartwahl'] = 1;
        \parse_str($_GET['params'] ?? '', $params);
        $params = Text::filterXSS($params);
        $maps   = [
            'city'  => 'city',
            'state' => 'state',
            'zip'   => 'plz',
            'line1' => 'street',
        ];
        $data   = [
            'city'  => 'city',
            'state' => 'state',
            'zip'   => 'postal_code',
            'line1' => 'line1',
        ];

        PayPalHelper::addSurcharge($api->getPaymentId(), $params);

        $basket       = PayPalHelper::getBasket();
        $patchRequest = new PatchRequest();

        $customer        = Frontend::getCustomer();
        $shippingAddress = $api->prepareShippingAddress(Frontend::getDeliveryAddress());
        $billingAddress  = $api->prepareBillingAddress($customer);
        $amount          = $api->prepareAmount($basket);

        try {
            $payment      = Payment::get($_GET['id'], $apiContext);
            $patchRequest = new PatchRequest();
            $patchBilling = new Patch();
            $patchBilling->setOp('add')
                ->setPath('/payer/payer_info')
                ->setValue([
                    'first_name'      => $customer->cVorname,
                    'last_name'       => $customer->cNachname,
                    'email'           => $customer->cMail,
                    'billing_address' => $billingAddress
                ]);

            $patchRequest->addPatch($patchBilling);

            $patchShipping = new Patch();
            $patchShipping->setOp('add')
                ->setPath('/transactions/0/item_list/shipping_address')
                ->setValue($shippingAddress);

            $patchRequest->addPatch($patchShipping);

            if ((float)$payment->getTransactions()[0]->getAmount()->getTotal() !== (float)$amount->getTotal()) {
                $patchAmount = new Patch();
                $patchAmount->setOp('replace')
                    ->setPath('/transactions/0/amount')
                    ->setValue($amount);
                $patchRequest->addPatch($patchAmount);
            }

            $payment->update($patchRequest, $apiContext);
            $api->logResult('Patch', $patchRequest, $payment);

            _exit(200);
        } catch (Exception $ex) {
            $api->handleException('Patch', $patchRequest, $ex);

            try {
                $error = new Error($ex->getData());

                if ($error->getName() !== 'VALIDATION_ERROR') {
                    throw $ex;
                }

                $address = $shippingAddress->toArray();

                foreach ($error->getDetails() as $detail) {
                    $fields = \explode('.', $detail->getField());
                    $field  = \end($fields);
                    if (!\array_key_exists($field, $maps)) {
                        throw $ex;
                    }
                    $detail->setField($maps[$field]);
                    $detail->setIssue($address[$data[$field]]);
                }

                Shop::Smarty()
                    ->assign('error', $error);
            } catch (Exception $ex) {
            }

            $tplData = Shop::Smarty()
                ->assign('l10n', $oPlugin->getLocalization()->getTranslations())
                ->assign('sameAsBilling', (int)$_SESSION['Bestellung']->kLieferadresse === 0)
                ->fetch($oPlugin->getPaths()->getFrontendPath() . 'template/paypalplus-patch.tpl');

            _exit(500, $tplData);
        }
        break;

    case 'return':
        $success = isset($_GET['r']) && $_GET['r'] === 'true';

        if (!$success) {
            $api->doLog('PayPal redirect (user canceled)');

            _redirect(Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1');
        }

        $args = [
            'paymentId' => $_GET['paymentId'],
            'token'     => $_GET['token'],
            'payerId'   => $_GET['PayerID']
        ];

        $validArgs = \array_filter($args);

        if (\count($args) !== \count($validArgs)) {
            $api->logResult('PayPal redirect (missing arguments)', (object)$args);

            _redirect(Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1');
        }

        foreach ($validArgs as $k => $v) {
            $api->addCache($k, $v);
        }

        try {
            $payment = Payment::get($validArgs['paymentId'], $apiContext);

            $api->addCache('payment', $payment->toJson());
            $api->createPaymentSession();

            _redirect(Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php'));
        } catch (Exception $ex) {
            $api->handleException('GetPayment', $validArgs['paymentId'], $ex);

            _redirect(Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1');
        }
        break;

    case 'webhook':
        $bodyReceived == null;
        try {
            $bodyReceived = \file_get_contents('php://input');
            if (empty($bodyReceived)) {
                _exit(500, 'Body cannot be null or empty');
            }
            // @todo
            $event    = WebhookEvent::validateAndGetReceivedEvent($bodyReceived, $apiContext);
            $resource = $event->getResource();
            $type     = $event->getResourceType();
            $api->logResult('Webhook', $event);
            if ($type === 'sale' && $resource->state === 'completed') {
                $paymentId = $resource->parent_payment;
                $db        = Shop::Container()->getDB();
                $order     = $db->select('tbestellung', 'cSession', $paymentId);
                if (\is_object($order) && (int)$order->kBestellung > 0) {
                    $api->doLog('Incoming payment for order (id: ' . $order->kBestellung
                        . ' / paypal id: ' . $resource->id . ') already received (skipped)');

                    $incomingPayment = $db->select(
                        'tzahlungseingang',
                        'kBestellung',
                        $order->kBestellung,
                        'cHinweis',
                        $resource->id
                    );
                    if (\is_object($incomingPayment) && (int)$incomingPayment->kZahlungseingang > 0) {
                        $api->doLog('Incoming payment for order (id: ' . $order->kBestellung
                            . ' / paypal id: ' . $resource->id
                            . ') already received (skipped)');
                    } else {
                        $amount          = $resource->amount;
                        $incomingPayment = (object)[
                            'cISO'             => $amount->currency,
                            'cHinweis'         => $resource->id,
                            'fBetrag'          => (float)($amount->total),
                            'dZeit'            => \date('Y-m-d H:i:s', \strtotime($resource->create_time)),
                            'fZahlungsgebuehr' => (float)($amount->details->handling_fee),
                        ];
                        $api->addIncomingPayment($order, $incomingPayment);
                        $api->sendConfirmationMail($order);
                        $api->doLog('Incoming payment for order (id: ' . $order->kBestellung
                            . ' / paypal id: ' . $resource->id
                            . ') added');
                    }
                } else {
                    $api->doLog(\sprintf("No order found with paymentId '%s'", $paymentId), \LOGLEVEL_ERROR);
                }
                _exit(200);
            }
        } catch (Exception $ex) {
            $api->handleException('Webhook', $bodyReceived, $ex);
        }
        break;
}

_exit(400);
