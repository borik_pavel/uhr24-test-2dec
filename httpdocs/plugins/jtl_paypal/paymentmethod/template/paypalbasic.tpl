{if $redirect}
    <div id="paypal-basic-redirect">
        <p class="text-muted">{lang key='redirect'}</p>
        {inline_script}<script>
            $(function() {
                window.location.href = {$redirect|json_encode};
            });
        </script>{/inline_script}
    </div>
{/if}