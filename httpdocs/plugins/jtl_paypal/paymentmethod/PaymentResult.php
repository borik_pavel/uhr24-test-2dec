<?php declare(strict_types=1);
/**
 * This file is part of JTL-Shop which is released under MIT License.
 * See file LICENSE.md for full license details.
 */

namespace Plugin\jtl_paypal\paymentmethod;

use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\PaymentInfoType;

/**
 * Class PaymentResult
 * @package Plugin\jtl_paypal\paymentmethod
 */
class PaymentResult
{
    /** @var PaymentDetailsType */
    private $paymentDetails;

    /** @var PaymentInfoType */
    private $paymentInfo;

    /** @var string */
    private $paymentError;

    public const STATE_UNKNOWN   = 0xFF;
    public const STATE_FAILURE   = 0x00;
    public const STATE_SUCCESS   = 0x01;
    public const STATE_COMPLETED = 0x03;

    /** @var int */
    public $state = self::STATE_UNKNOWN;

    /**
     * PaymentResult constructor.
     * @param int $state
     */
    public function __construct(int $state)
    {
        $this->state = $state;
    }

    /**
     * @return PaymentDetailsType|null
     */
    public function getPaymentDetails(): ?PaymentDetailsType
    {
        return $this->paymentDetails;
    }

    /**
     * @param PaymentDetailsType $paymentDetails
     */
    public function setPaymentDetails(PaymentDetailsType $paymentDetails): void
    {
        $this->paymentDetails = $paymentDetails;
    }

    /**
     * @return PaymentInfoType|null
     */
    public function getPaymentInfo(): ?PaymentInfoType
    {
        return $this->paymentInfo;
    }

    /**
     * @param PaymentInfoType $paymentInfo
     */
    public function setPaymentInfo(PaymentInfoType $paymentInfo): void
    {
        $this->paymentInfo = $paymentInfo;
    }

    /**
     * @return string
     */
    public function getPaymentError(): string
    {
        return $this->paymentError ?? '';
    }

    /**
     * @param string $paymentError
     */
    public function setPaymentError(string $paymentError): void
    {
        $this->paymentError = $paymentError;
    }
}
