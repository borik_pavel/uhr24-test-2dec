<?php

namespace Plugin\jtl_paypal\paymentmethod;

use Exception;
use JTL\Cart\CartHelper;
use JTL\Checkout\Bestellung;
use JTL\Checkout\Zahlungsart;
use JTL\Customer\Customer;
use JTL\Customer\CustomerGroup;
use JTL\DB\ReturnType;
use JTL\Helpers\Text;
use JTL\Language\LanguageHelper;
use JTL\Link\Link;
use JTL\Mail\Mail\Mail;
use JTL\Mail\Mailer;
use JTL\Plugin\Helper;
use JTL\Plugin\Payment\MethodInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Staat;
use PayPal\Api\Webhook;
use PayPal\Api\WebhookEventType;
use PayPal\Api\WebhookList;
use PayPal\PayPalAPI;
use PayPal\Service\PayPalAPIInterfaceServiceService;
use stdClass;

/**
 * Class PayPalHelper
 * @package Plugin\jtl_paypal\paymentmethod
 */
class PayPalHelper
{
    /**
     * https://developer.paypal.com/docs/classic/api/locale_codes/
     */
    public static $locales = [
        'AL' => 'en_US',  // ALBANIA
        'DZ' => 'ar_EG',  // ALGERIA
        'AD' => 'en_US',  // ANDORRA
        'AO' => 'en_US',  // ANGOLA
        'AI' => 'en_US',  // ANGUILLA
        'AG' => 'en_US',  // ANTIGUA & BARBUDA
        'AR' => 'es_XC',  // ARGENTINA
        'AM' => 'en_US',  // ARMENIA
        'AW' => 'en_US',  // ARUBA
        'AU' => 'en_AU',  // AUSTRALIA
        'AT' => 'de_DE',  // AUSTRIA
        'AZ' => 'en_US',  // AZERBAIJAN
        'BS' => 'en_US',  // BAHAMAS
        'BH' => 'ar_EG',  // BAHRAIN
        'BB' => 'en_US',  // BARBADOS
        'BY' => 'en_US',  // BELARUS
        'BE' => 'en_US',  // BELGIUM
        'BZ' => 'es_XC',  // BELIZE
        'BJ' => 'fr_XC',  // BENIN
        'BM' => 'en_US',  // BERMUDA
        'BT' => 'en_US',  // BHUTAN
        'BO' => 'es_XC',  // BOLIVIA
        'BA' => 'en_US',  // BOSNIA & HERZEGOVINA
        'BW' => 'en_US',  // BOTSWANA
        'BR' => 'pt_BR',  // BRAZIL
        'VG' => 'en_US',  // BRITISH VIRGIN ISLANDS
        'BN' => 'en_US',  // BRUNEI
        'BG' => 'en_US',  // BULGARIA
        'BF' => 'fr_XC',  // BURKINA FASO
        'BI' => 'fr_XC',  // BURUNDI
        'KH' => 'en_US',  // CAMBODIA
        'CM' => 'fr_XC',  // CAMEROON
        'CA' => 'en_US',  // CANADA
        'CV' => 'en_US',  // CAPE VERDE
        'KY' => 'en_US',  // CAYMAN ISLANDS
        'TD' => 'fr_XC',  // CHAD
        'CL' => 'es_XC',  // CHILE
        'CN' => 'zh_CN',  // CHINA
        'C2' => 'zh_XC',  // CHINA WORLDWIDE
        'CO' => 'es_XC',  // COLOMBIA
        'KM' => 'fr_XC',  // COMOROS
        'CG' => 'en_US',  // CONGO - BRAZZAVILLE
        'CD' => 'fr_XC',  // CONGO - KINSHASA
        'CK' => 'en_US',  // COOK ISLANDS
        'CR' => 'es_XC',  // COSTA RICA
        'CI' => 'fr_XC',  // CÔTE D’IVOIRE
        'HR' => 'en_US',  // CROATIA
        'CY' => 'en_US',  // CYPRUS
        'CZ' => 'en_US',  // CZECH REPUBLIC
        'DK' => 'da_DK',  // DENMARK
        'DJ' => 'fr_XC',  // DJIBOUTI
        'DM' => 'en_US',  // DOMINICA
        'DO' => 'es_XC',  // DOMINICAN REPUBLIC
        'EC' => 'es_XC',  // ECUADOR
        'EG' => 'ar_EG',  // EGYPT
        'SV' => 'es_XC',  // EL SALVADOR
        'ER' => 'en_US',  // ERITREA
        'EE' => 'en_US',  // ESTONIA
        'ET' => 'en_US',  // ETHIOPIA
        'FK' => 'en_US',  // FALKLAND ISLANDS
        'FO' => 'da_DK',  // FAROE ISLANDS
        'FJ' => 'en_US',  // FIJI
        'FI' => 'en_US',  // FINLAND
        'FR' => 'fr_FR',  // FRANCE
        'GF' => 'en_US',  // FRENCH GUIANA
        'PF' => 'en_US',  // FRENCH POLYNESIA
        'GA' => 'fr_XC',  // GABON
        'GM' => 'en_US',  // GAMBIA
        'GE' => 'en_US',  // GEORGIA
        'DE' => 'de_DE',  // GERMANY
        'GI' => 'en_US',  // GIBRALTAR
        'GR' => 'en_US',  // GREECE
        'GL' => 'da_DK',  // GREENLAND
        'GD' => 'en_US',  // GRENADA
        'GP' => 'en_US',  // GUADELOUPE
        'GT' => 'es_XC',  // GUATEMALA
        'GN' => 'fr_XC',  // GUINEA
        'GW' => 'en_US',  // GUINEA-BISSAU
        'GY' => 'en_US',  // GUYANA
        'HN' => 'es_XC',  // HONDURAS
        'HK' => 'en_GB',  // HONG KONG SAR CHINA
        'HU' => 'en_US',  // HUNGARY
        'IS' => 'en_US',  // ICELAND
        'IN' => 'en_GB',  // INDIA
        'ID' => 'id_ID',  // INDONESIA
        'IE' => 'en_US',  // IRELAND
        'IL' => 'he_IL',  // ISRAEL
        'IT' => 'it_IT',  // ITALY
        'JM' => 'es_XC',  // JAMAICA
        'JP' => 'ja_JP',  // JAPAN
        'JO' => 'ar_EG',  // JORDAN
        'KZ' => 'en_US',  // KAZAKHSTAN
        'KE' => 'en_US',  // KENYA
        'KI' => 'en_US',  // KIRIBATI
        'KW' => 'ar_EG',  // KUWAIT
        'KG' => 'en_US',  // KYRGYZSTAN
        'LA' => 'en_US',  // LAOS
        'LV' => 'en_US',  // LATVIA
        'LS' => 'en_US',  // LESOTHO
        'LI' => 'en_US',  // LIECHTENSTEIN
        'LT' => 'en_US',  // LITHUANIA
        'LU' => 'en_US',  // LUXEMBOURG
        'MK' => 'en_US',  // MACEDONIA
        'MG' => 'en_US',  // MADAGASCAR
        'MW' => 'en_US',  // MALAWI
        'MY' => 'en_US',  // MALAYSIA
        'MV' => 'en_US',  // MALDIVES
        'ML' => 'fr_XC',  // MALI
        'MT' => 'en_US',  // MALTA
        'MH' => 'en_US',  // MARSHALL ISLANDS
        'MQ' => 'en_US',  // MARTINIQUE
        'MR' => 'en_US',  // MAURITANIA
        'MU' => 'en_US',  // MAURITIUS
        'YT' => 'en_US',  // MAYOTTE
        'MX' => 'es_XC',  // MEXICO
        'FM' => 'en_US',  // MICRONESIA
        'MD' => 'en_US',  // MOLDOVA
        'MC' => 'fr_XC',  // MONACO
        'MN' => 'en_US',  // MONGOLIA
        'ME' => 'en_US',  // MONTENEGRO
        'MS' => 'en_US',  // MONTSERRAT
        'MA' => 'ar_EG',  // MOROCCO
        'MZ' => 'en_US',  // MOZAMBIQUE
        'NA' => 'en_US',  // NAMIBIA
        'NR' => 'en_US',  // NAURU
        'NP' => 'en_US',  // NEPAL
        'NL' => 'nl_NL',  // NETHERLANDS
        'NC' => 'en_US',  // NEW CALEDONIA
        'NZ' => 'en_US',  // NEW ZEALAND
        'NI' => 'es_XC',  // NICARAGUA
        'NE' => 'fr_XC',  // NIGER
        'NG' => 'en_US',  // NIGERIA
        'NU' => 'en_US',  // NIUE
        'NF' => 'en_US',  // NORFOLK ISLAND
        'NO' => 'no_NO',  // NORWAY
        'OM' => 'ar_EG',  // OMAN
        'PW' => 'en_US',  // PALAU
        'PA' => 'es_XC',  // PANAMA
        'PG' => 'en_US',  // PAPUA NEW GUINEA
        'PY' => 'es_XC',  // PARAGUAY
        'PE' => 'es_XC',  // PERU
        'PH' => 'en_US',  // PHILIPPINES
        'PN' => 'en_US',  // PITCAIRN ISLANDS
        'PL' => 'pl_PL',  // POLAND
        'PT' => 'pt_PT',  // PORTUGAL
        'QA' => 'en_US',  // QATAR
        'RE' => 'en_US',  // RÉUNION
        'RO' => 'en_US',  // ROMANIA
        'RU' => 'ru_RU',  // RUSSIA
        'RW' => 'fr_XC',  // RWANDA
        'WS' => 'en_US',  // SAMOA
        'SM' => 'en_US',  // SAN MARINO
        'ST' => 'en_US',  // SÃO TOMÉ & PRÍNCIPE
        'SA' => 'ar_EG',  // SAUDI ARABIA
        'SN' => 'fr_XC',  // SENEGAL
        'RS' => 'en_US',  // SERBIA
        'SC' => 'fr_XC',  // SEYCHELLES
        'SL' => 'en_US',  // SIERRA LEONE
        'SG' => 'en_GB',  // SINGAPORE
        'SK' => 'en_US',  // SLOVAKIA
        'SI' => 'en_US',  // SLOVENIA
        'SB' => 'en_US',  // SOLOMON ISLANDS
        'SO' => 'en_US',  // SOMALIA
        'ZA' => 'en_US',  // SOUTH AFRICA
        'KR' => 'ko_KR',  // SOUTH KOREA
        'ES' => 'es_ES',  // SPAIN
        'LK' => 'en_US',  // SRI LANKA
        'SH' => 'en_US',  // ST. HELENA
        'KN' => 'en_US',  // ST. KITTS & NEVIS
        'LC' => 'en_US',  // ST. LUCIA
        'PM' => 'en_US',  // ST. PIERRE & MIQUELON
        'VC' => 'en_US',  // ST. VINCENT & GRENADINES
        'SR' => 'en_US',  // SURINAME
        'SJ' => 'en_US',  // SVALBARD & JAN MAYEN
        'SZ' => 'en_US',  // SWAZILAND
        'SE' => 'sv_SE',  // SWEDEN
        'CH' => 'de_DE',  // SWITZERLAND
        'TW' => 'zh_TW',  // TAIWAN
        'TJ' => 'en_US',  // TAJIKISTAN
        'TZ' => 'en_US',  // TANZANIA
        'TH' => 'th_TH',  // THAILAND
        'TG' => 'fr_XC',  // TOGO
        'TO' => 'en_US',  // TONGA
        'TT' => 'en_US',  // TRINIDAD & TOBAGO
        'TN' => 'ar_EG',  // TUNISIA
        'TM' => 'en_US',  // TURKMENISTAN
        'TC' => 'en_US',  // TURKS & CAICOS ISLANDS
        'TV' => 'en_US',  // TUVALU
        'UG' => 'en_US',  // UGANDA
        'UA' => 'en_US',  // UKRAINE
        'AE' => 'en_US',  // UNITED ARAB EMIRATES
        'GB' => 'en_GB',  // UNITED KINGDOM
        'US' => 'en_US',  // UNITED STATES
        'UY' => 'es_XC',  // URUGUAY
        'VU' => 'en_US',  // VANUATU
        'VA' => 'en_US',  // VATICAN CITY
        'VE' => 'es_XC',  // VENEZUELA
        'VN' => 'en_US',  // VIETNAM
        'WF' => 'en_US',  // WALLIS & FUTUNA
        'YE' => 'ar_EG',  // YEMEN
        'ZM' => 'en_US',  // ZAMBIA
        'ZW' => 'en_US',  // ZIMBABWE
        'EN' => 'en_GB',  // EN DEFAULT
    ];

    /**
     * @param $iso
     * @return mixed
     */
    public static function getDefaultLocale($iso)
    {
        return static::$locales[\mb_strtoupper($iso)] ?? 'DE';
    }

    /**
     * @param array $config
     * @param bool  $apiCall
     * @return array
     */
    public static function test(array $config, bool $apiCall = true): array
    {
        $error  = false;
        $result = [
            'status' => 'success',
            'code'   => 0,
            'msg'    => '',
            'mode'   => $config['mode'],
        ];

        $response      = new stdClass();
        $response->Ack = 'Failure';
        if (empty($config['acct1.UserName'])) {
            $error            = true;
            $result['status'] = 'failure';
            $result['code']   = 1;
            $result['msg']   .= __('User name not set. ');
        }
        if (empty($config['acct1.Password'])) {
            $error            = true;
            $result['status'] = 'failure';
            $result['code']   = 1;
            $result['msg']   .= __('Password not set. ');
        }
        if (empty($config['acct1.Signature'])) {
            $error            = true;
            $result['status'] = 'failure';
            $result['code']   = 1;
            $result['msg']   .= __('Signature not set. ');
        }

        if ($apiCall === false) {
            return $result;
        }

        if ($error === false) {
            $getBalanceReq                          = new PayPalAPI\GetBalanceReq();
            $getBalanceRequest                      = new PayPalAPI\GetBalanceRequestType();
            $getBalanceRequest->ReturnAllCurrencies = '1';
            $getBalanceReq->GetBalanceRequest       = $getBalanceRequest;
            $service                                = new PayPalAPIInterfaceServiceService($config);
            try {
                $response = $service->GetBalance($getBalanceReq);
            } catch (Exception $e) {
                $result['msg']   .= $e->getMessage();
                $result['code']   = 2;
                $result['status'] = 'failure';

                return $result;
            }
        }

        $result['status'] = \mb_strtolower($response->Ack);
        if (isset($response->Errors)) {
            foreach ($response->Errors as $_error) {
                $result['msg'] .= $_error->ShortMessage;
            }
            $result['code'] = 3;
        }

        return $result;
    }

    /**
     * @return string
     */
    public static function getLanguageISO(): string
    {
        return self::toValidISO(Shop::getLanguageCode());
    }

    /**
     * @return int
     */
    public static function getCustomerGroupId(): int
    {
        $groupID = Frontend::getCustomer()->getGroupID();

        return $groupID > 0 ? $groupID : CustomerGroup::getDefaultGroupID();
    }

    /**
     * @return false|mixed|string|string[]|null
     */
    public static function getCountryISO()
    {
        $countryIso = Frontend::getCustomer()->cLand ?: '';

        if (\mb_strlen($countryIso) > 2 && ($iso = LanguageHelper::getIsoCodeByCountryName($countryIso)) !== 'noISO') {
            $countryIso = $iso;
        }

        return self::toValidISO($countryIso);
    }

    /**
     * @param $address
     * @return string|null
     */
    public static function getState($address): ?string
    {
        if (!isset($address->cLand, $address->cBundesland)) {
            return null;
        }

        if (\in_array($address->cLand, ['AR', 'BR', 'IN', 'US', 'CA', 'IT', 'JP', 'MX', 'TH'])) {
            $state = Staat::getRegionByName($address->cBundesland);
            if ($state !== null) {
                return $state->cCode;
            }
        }

        return $address->cBundesland;
    }

    /**
     * @param string $iso
     * @return string
     */
    protected static function toValidISO($iso): string
    {
        if (\mb_strlen($iso) === 3) {
            $iso = Text::convertISO2ISO639($iso);
        }

        $iso = \mb_strtoupper($iso);

        if (\mb_strlen($iso) !== 2) {
            $iso = 'DE';
        }

        return $iso;
    }

    /**
     * @param string $str
     * @return string
     */
    public static function toASCI127($str): string
    {
        $a = ['Ä', 'Ö', 'Ü', 'ß', 'ä', 'ö', 'ü', 'æ'];
        $b = ['Ae', 'Oe', 'Ue', 'ss', 'ae', 'oe', 'ue', 'ae'];

        return \preg_replace('/[^\pL\d\-\/_ ]+/u', '', \str_replace($a, $b, $str));
    }

    /**
     * @param string $name
     * @return object
     */
    public static function extractName($name)
    {
        $parts = \explode(' ', $name, 2);
        if (\count($parts) === 1) {
            \array_unshift($parts, '');
        }

        return (object)[
            'first' => \trim($parts[0]),
            'last'  => \trim($parts[1]),
        ];
    }

    /**
     * @param string $street
     * @return object
     */
    public static function extractStreet($street)
    {
        $re     = "/^(\\d*[\\wäöüß '\\-\\.]+)[,\\s]+(\\d+)\\s*([\\wäöüß\\-\\/]*)$/ui";
        $number = '';
        if (\preg_match($re, $street, $matches)) {
            $offset = \mb_strlen($matches[1]);
            $number = \mb_substr($street, $offset);
            $street = \mb_substr($street, 0, $offset);
        }

        return (object)[
            'name'   => \trim($street, '-:, '),
            'number' => \trim($number, '-:, ')
        ];
    }

    /**
     * @param string $invoice
     * @return bool|int
     */
    public static function getOrderId(string $invoice)
    {
        $result = Shop::Container()->getDB()->queryPrepared(
            'SELECT kBestellung AS id FROM tbestellung WHERE cBestellNr = :invoice',
            ['invoice' => $invoice],
            ReturnType::SINGLE_OBJECT
        );
        if (isset($result->id) && (int)$result->id > 0) {
            return (int)$result->id;
        }

        return false;
    }

    /**
     * @param string $message
     */
    public static function setFlashMessage($message): void
    {
        if (!isset($_SESSION['jtl_paypal_jtl']) || !\is_array($_SESSION['jtl_paypal_jtl'])) {
            $_SESSION['jtl_paypal_jtl'] = [];
        }
        $_SESSION['jtl_paypal_jtl']['flash'] = $message;
    }

    /**
     * @return mixed|null
     */
    public static function getFlashMessage()
    {
        return $_SESSION['jtl_paypal_jtl']['flash'] ?? null;
    }

    public static function clearFlashMessage(): void
    {
        unset($_SESSION['jtl_paypal_jtl']['flash']);
    }

    /**
     * @param PluginInterface $plugin
     * @param string          $name
     * @return Link|mixed|null
     */
    public static function getLinkByName($plugin, $name)
    {
        foreach ($plugin->getLinks()->getLinks() as $link) {
            /** @var Link $link */
            if (\strcasecmp($link->getDisplayName(), $name) === 0) {
                return $link;
            }
        }

        return null;
    }

    /**
     *
     */
    public static function dropPaymentPositions(): void
    {
        Frontend::getCart()
            ->loescheSpezialPos(\C_WARENKORBPOS_TYP_ZAHLUNGSART)
            ->loescheSpezialPos(\C_WARENKORBPOS_TYP_ZINSAUFSCHLAG)
            ->loescheSpezialPos(\C_WARENKORBPOS_TYP_BEARBEITUNGSGEBUEHR)
            ->loescheSpezialPos(\C_WARENKORBPOS_TYP_NACHNAHMEGEBUEHR);
    }

    /**
     * @param int        $paymentId
     * @param array|null $formParams
     */
    public static function addSurcharge($paymentId = 0, $formParams = null): void
    {
        require_once \PFAD_ROOT . \PFAD_INCLUDES . 'bestellvorgang_inc.php';

        $paymentId = $paymentId <= 0 && isset($_SESSION['Zahlungsart'], $_SESSION['Zahlungsart']->kZahlungsart)
            ? (int)$_SESSION['Zahlungsart']->kZahlungsart
            : $paymentId;

        $shippingId = isset($_SESSION['Versandart'], $_SESSION['Versandart']->kVersandart)
            ? (int)$_SESSION['Versandart']->kVersandart
            : 0;

        if ($shippingId <= 0 || $paymentId <= 0) {
            return;
        }
        $formParams['kVerpackung'] = $formParams['kVerpackung'] ?? [];
        if (!\versandartKorrekt($shippingId, $formParams)) {
            return;
        }

        $paymentMethod = new Zahlungsart();
        $paymentMethod->load($paymentId);

        if ((int)$paymentMethod->kZahlungsart <= 0) {
            return;
        }

        $surcharge = Shop::Container()->getDB()->selectSingleRow(
            'tversandartzahlungsart',
            'kVersandart',
            $shippingId,
            'kZahlungsart',
            $paymentId
        );

        if ($surcharge !== null && \is_object($surcharge)) {
            if (isset($surcharge->cAufpreisTyp) && $surcharge->cAufpreisTyp === 'prozent') {
                $amount = (Frontend::getCart()->gibGesamtsummeWarenExt(['1'], 1) * $surcharge->fAufpreis) / 100.0;
            } else {
                $amount = $surcharge->fAufpreis ?? 0;
            }

            $name = $paymentMethod->cName;

            if (isset($_SESSION['Zahlungsart'])) {
                $name                                  = $_SESSION['Zahlungsart']->angezeigterName;
                $_SESSION['Zahlungsart']->fAufpreis    = $surcharge->fAufpreis;
                $_SESSION['Zahlungsart']->cAufpreisTyp = $surcharge->cAufpreisTyp;
            }

            if ($amount !== 0) {
                Frontend::getCart()->erstelleSpezialPos(
                    $name,
                    1,
                    $amount,
                    Frontend::getCart()->gibVersandkostenSteuerklasse(),
                    \C_WARENKORBPOS_TYP_ZAHLUNGSART
                );
            }
        }

        if (!\function_exists('plausiNeukundenKupon')) {
            require_once \PFAD_ROOT . \PFAD_INCLUDES . 'bestellvorgang_inc.php';
        }

        \plausiNeukundenKupon();
    }

    /**
     * @return array
     */
    public static function getProducts(): array
    {
        $products = [];
        foreach (Frontend::getCart()->PositionenArr as $item) {
            if ($item->nPosTyp === \C_WARENKORBPOS_TYP_ARTIKEL) {
                $products[] = $item->Artikel;
            }
        }

        return $products;
    }

    /**
     * @param null $helper
     * @return stdClass
     */
    public static function getBasket($helper = null): stdClass
    {
        if ($helper === null) {
            $helper = new CartHelper();
        }

        $cart     = $helper->getTotal(4);
        $rounding = static function ($prop) {
            return [
                CartHelper::NET   => \round($prop[CartHelper::NET], 2),
                CartHelper::GROSS => \round($prop[CartHelper::GROSS], 2),
            ];
        };

        $product = [
            CartHelper::NET   => 0,
            CartHelper::GROSS => 0,
        ];

        foreach ($cart->items as $i => $p) {
            $p->amount = $rounding($p->amount);

            $product[CartHelper::NET]   += $p->amount[CartHelper::NET] * $p->quantity;
            $product[CartHelper::GROSS] += $p->amount[CartHelper::GROSS] * $p->quantity;
        }

        $cart->article   = $rounding($product);
        $cart->shipping  = $rounding($cart->shipping);
        $cart->discount  = $rounding($cart->discount);
        $cart->surcharge = $rounding($cart->surcharge);
        $cart->total     = $rounding($cart->total);

        $calculated = [
            CartHelper::NET   => 0,
            CartHelper::GROSS => 0,
        ];

        $calculated[CartHelper::NET]   = $cart->article[CartHelper::NET]
            + $cart->shipping[CartHelper::NET]
            - $cart->discount[CartHelper::NET]
            + $cart->surcharge[CartHelper::NET];
        $calculated[CartHelper::GROSS] = $cart->article[CartHelper::GROSS]
            + $cart->shipping[CartHelper::GROSS]
            - $cart->discount[CartHelper::GROSS]
            + $cart->surcharge[CartHelper::GROSS];

        $calculated = $rounding($calculated);

        $difference = [
            CartHelper::NET   => $cart->total[CartHelper::NET] - $calculated[CartHelper::NET],
            CartHelper::GROSS => $cart->total[CartHelper::GROSS] - $calculated[CartHelper::GROSS],
        ];

        $difference = $rounding($difference);

        $addDifference = static function ($difference, $type) use (&$cart) {
            if ($difference[$type] < 0.0) {
                if ($cart->shipping[$type] >= $difference[$type] * -1) {
                    $cart->shipping[$type] += $difference[$type];
                } else {
                    $cart->discount[$type] += $difference[$type] * -1;
                }
            } else {
                $cart->surcharge[$type] += $difference[$type];
            }
        };

        $addDifference($difference, CartHelper::NET);
        $addDifference($difference, CartHelper::GROSS);

        return $cart;
    }

    /**
     * @param Customer   $customer
     * @param Bestellung $order
     * @return bool
     * @throws \PHPMailer\PHPMailer\Exception
     * @throws \SmartyException
     */
    public static function sendPaymentDeniedMail($customer, $order): bool
    {
        $plugin = Helper::getPluginById('jtl_paypal');
        if ($plugin === null) {
            return false;
        }
        $data              = new stdClass();
        $data->tkunde      = $customer;
        $data->tbestellung = $order;
        $mailer            = Shop::Container()->get(Mailer::class);
        $mail              = new Mail();
        /** @var Mailer $mailer */
        $mail = $mail->createFromTemplateID('kPlugin_' . $plugin->getID() . '_pppd', $data);

        return $mailer->send($mail);
    }

    /**
     * @param MethodInterface $paymentMethod
     * @return WebhookList|null
     */
    public static function getWebhooks(MethodInterface $paymentMethod): ?WebhookList
    {
        if (!$paymentMethod->isConfigured()) {
            return null;
        }

        try {
            return Webhook::getAllWithParams([], $paymentMethod->getContext());
        } catch (Exception $ex) {
        }

        return null;
    }

    /**
     * @param MethodInterface $paymentMethod
     * @return mixed
     */
    public static function getWebhookUrl(MethodInterface $paymentMethod)
    {
        return \str_replace('http://', 'https://', $paymentMethod->getCallbackUrl(['a' => 'webhook'], true));
    }

    /**
     * @param MethodInterface $paymentMethod
     * @return bool|null
     */
    public static function deleteWebhook(MethodInterface $paymentMethod): ?bool
    {
        if (!$paymentMethod->isConfigured()) {
            return null;
        }

        try {
            if ($hook = static::getWebhook($paymentMethod)) {
                return $hook->delete($paymentMethod->getContext());
            }
        } catch (Exception $e) {
        }

        return false;
    }

    /**
     * @param MethodInterface $paymentMethod
     * @return Webhook|null
     */
    public static function setWebhook(MethodInterface $paymentMethod): ?Webhook
    {
        if (!$paymentMethod->isConfigured()) {
            return null;
        }

        try {
            $webhook = new Webhook();
            $webhook->setUrl(static::getWebhookUrl($paymentMethod))
                ->setEventTypes([new WebhookEventType('{ "name": "*" }')]);

            return $webhook->create($paymentMethod->getContext());
        } catch (Exception $e) {
        }

        return null;
    }

    /**
     * @param MethodInterface $paymentMethod
     * @return Webhook|null
     */
    public static function getWebhook(MethodInterface $paymentMethod): ?Webhook
    {
        try {
            $url = static::getWebhookUrl($paymentMethod);
            if ($list = self::getWebhooks($paymentMethod)) {
                foreach ($list->getWebhooks() as $hook) {
                    if ($hook->getUrl() === $url) {
                        return $hook;
                    }
                }
            }
        } catch (Exception $e) {
        }

        return null;
    }

    /**
     * @param array|mixed $d
     * @return object|mixed
     */
    public static function toObject($d)
    {
        return \is_array($d) ? (object)\array_map([__CLASS__, __METHOD__], $d) : $d;
    }

    /**
     * @param object|mixed $d
     * @return array|mixed
     */
    public static function toArray($d)
    {
        $d = \is_object($d) ? \get_object_vars($d) : $d;

        return \is_array($d) ? \array_map([__CLASS__, __METHOD__], $d) : $d;
    }

    /**
     * @param $paymentId
     * @return bool
     */
    public static function isLinked($paymentId): bool
    {
        $sql = 'SELECT count(tversandart.kVersandart) as cnt
            FROM tversandart
            LEFT JOIN tversandartzahlungsart
                ON tversandartzahlungsart.kVersandart = tversandart.kVersandart
            WHERE tversandartzahlungsart.kZahlungsart = :paymentID';
        $c   = Shop::Container()->getDB()->queryPrepared(
            $sql,
            ['paymentID' => (int)$paymentId],
            ReturnType::SINGLE_OBJECT
        );

        return \is_object($c) && (int)$c->cnt > 0;
    }

    /**
     * @param $config
     * @param $prefix
     * @param $from
     * @param $to
     * @param $keys
     */
    public static function copyCredentials($config, $prefix, $from, $to, $keys): void
    {
        $db = Shop::Container()->getDB();
        foreach ($keys as $key) {
            $fromKey = $prefix . $from . '_' . $key;
            $toKey   = $prefix . $to . '_' . $key;
            if ((empty($config[$fromKey]) && !empty($config[$toKey]))
                || (!empty($config[$fromKey]) && empty($config[$toKey]))
            ) {
                $k = empty($config[$fromKey]) ? $fromKey : $toKey;
                $v = empty($config[$fromKey]) ? $config[$toKey] : $config[$fromKey];

                $db->queryPrepared(
                    'UPDATE tplugineinstellungen
                        SET `cWert` = :value
                        WHERE `cName` = :name',
                    [
                        'value' => $v,
                        'name'  => $k,
                    ],
                    ReturnType::DEFAULT
                );
            }
        }
    }

    /**
     * shorten a long payment-name to 'nLimit', by inserting a placholder 'szPlaceHolder' and
     * show the 'nLastShownChars' characters
     *
     * @param string $text
     * @return string
     */
    public static function shortenPaymentName($text): string
    {
        $limit          = 25; // hard limit of the payment-wall (should not be altered)
        $lastShownChars = 0;  // (maybe 3 looks good)
        $placeHolder    = '...';
        $shortened      = $text;
        if ($limit < \mb_strlen($text)) {
            $shortened =
                \mb_substr($text, 0, ($limit - (\mb_strlen($placeHolder) + $lastShownChars)))
                . $placeHolder
                . ($lastShownChars ? \mb_substr($text, -$lastShownChars) : '');
        }

        return $shortened;
    }

    /**
     * @param string $moduleID
     * @return bool
     */
    public static function isPPModuleID(string $moduleID): bool
    {
        static $methods = null;

        if ($methods === null) {
            $plugin = Helper::getPluginById('jtl_paypal');
            if ($plugin === null) {
                return false;
            }

            $methods = \array_map(static function ($method) {
                return $method->getModuleID();
            }, $plugin->getPaymentMethods()->getMethods());
        }

        foreach ($methods as $method) {
            if ($method === $moduleID) {
                return true;
            }
        }

        return false;
    }
}
