<?php declare(strict_types=1);
/**
 * This file is part of JTL-Shop which is released under MIT License.
 * See file LICENSE.md for full license details.
 */

namespace Plugin\jtl_paypal\paymentmethod;

/**
 * Interface NotificationInterface
 * @package Plugin\jtl_paypal\paymentmethod
 */
interface NotificationInterface
{
    public const STATE_VALID           = 0;
    public const STATE_NOT_CONFIGURED  = 0x10;
    public const STATE_DURING_CHECKOUT = 0x20;
    public const STATE_SANDBOX         = 0x01;
    public const STATE_INVALID         = 0xF0;

    /**
     * @return int
     */
    public function getNotificationState(): int;

    /**
     * @return string
     */
    public function getNotification(): string;

    /**
     * @return bool
     */
    public function notificationActive(): bool;
}
