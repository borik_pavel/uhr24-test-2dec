<?php declare(strict_types=1);

namespace Plugin\jtl_paypal;

use Exception;
use JTL\Alert\Alert;
use JTL\Catalog\Product\Artikel;
use JTL\Catalog\Product\Preise;
use JTL\Consent\Item;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Helpers\Form;
use JTL\Helpers\Request;
use JTL\Helpers\ShippingMethod;
use JTL\Helpers\Text;
use JTL\IO\IO;
use JTL\IO\IOResponse;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use JTL\Visitor;
use Plugin\jtl_paypal\adminmenu\PPCBannerConfig;
use Plugin\jtl_paypal\paymentmethod\PayPalExpress;
use Plugin\jtl_paypal\paymentmethod\PayPalHelper;
use Plugin\jtl_paypal\paymentmethod\PayPalPlus;
use Plugin\jtl_paypal\paymentmethod\PendingPayment;
use SmartyException;
use stdClass;

/**
 * Class Handler
 * @package Plugin\jtl_paypal
 */
class Handler
{
    /**
     * @var DbInterface
     */
    private $db;

    /**
     * @var PluginInterface
     */
    private $plugin;

    /**
     * @var array
     */
    private static $allowedIsoCodes = ['de', 'en', 'es', 'fr', 'nl'];

    /**
     * Handler constructor.
     * @param PluginInterface $plugin
     * @param DbInterface     $db
     */
    public function __construct(PluginInterface $plugin, DbInterface $db)
    {
        $this->db     = $db;
        $this->plugin = $plugin;
    }

    /**
     * @return string
     */
    private function getLocalCode(): string
    {
        if (Shop::isFrontend()) {
            return PayPalHelper::getDefaultLocale(PayPalHelper::getLanguageISO());
        }

        return \str_replace('-', '_', $_SESSION['AdminAccount']->language);
    }

    /**
     * @param string $original
     * @return string
     */
    private function getBackendTransalation(string $original): string
    {
        if (!Shop::isFrontend()) {
            return __($original);
        }
        $getText   = Shop::Container()->getGetText();
        $oldLocale = $getText->getLanguage();
        $locale    = \str_replace('_', '-', PayPalHelper::getDefaultLocale(PayPalHelper::getLanguageISO()));

        if ($oldLocale !== $locale) {
            $getText->setLanguage($locale);
            $translate = __($original);
            $getText->setLanguage($oldLocale);
        } else {
            $translate = __($original);
        }

        return $translate;
    }

    /**
     * @param array $args
     */
    public function fetchTemplate(array $args): void
    {
        $tpls = ['checkout/step4_payment_options.tpl', 'tpl_inc/bestellvorgang_zahlung.tpl'];
        if (\in_array($args['original'], $tpls, true)
            && Shop::Smarty()->getTemplateVars('payPalPlus') === true
            && (new PayPalPlus())->isValidIntern(['customer' => Frontend::getCustomer(), 'cart' => Frontend::getCart()])
        ) {
            $args['out'] = $this->plugin->getPaths()->getFrontendPath() . 'template/' . 'paypalplus.tpl';
        }
    }

    /**
     * hook 144/HOOK_NOTIFY_HASHPARAMETER_DEFINITION
     *
     * @throws Exception
     */
    public function hash(): void
    {
        if (!isset($_GET['payment_method'], $_GET['token']) || $_GET['payment_method'] !== 'jtl_paypal') {
            return;
        }
        $session = Frontend::getInstance();
        require_once \PFAD_ROOT . \PFAD_INCLUDES . 'bestellabschluss_inc.php';
        if (isset($_SESSION['jtl_paypal']['Token']) && $_SESSION['jtl_paypal']['Token'] === $_GET['token']) {
            global $moduleId, $order, $paymentHash;

            $paymentHash = null;
            $moduleId    = $_SESSION['Zahlungsart']->cModulId;
            $order       = \finalisiereBestellung();

            $session->cleanUp();
        }
    }

    /**
     * @throws SmartyException
     * @throws Exception
     */
    public function smarty(): void
    {
        $smarty       = Shop::Smarty();
        $pageType     = Shop::getPageType();
        if ($pageType === \PAGE_ARTIKEL) {
            $this->addExpressButton($pageType, $smarty);
            $this->addPPCBanner(PPCBannerConfig::POSITION_PAGE_PRODUCT, $smarty);
        } elseif ($pageType === \PAGE_WARENKORB) {
            $this->addExpressButton($pageType, $smarty);
            $this->addPPCBanner(PPCBannerConfig::POSITION_PAGE_CART, $smarty);
        } elseif ($pageType === \PAGE_BESTELLVORGANG) {
            $this->smartyOrderPage($smarty);
            $this->addPPCBanner(PPCBannerConfig::POSITION_PAGE_PAYMENT, $smarty);
        } elseif ($pageType === \PAGE_BESTELLABSCHLUSS) {
            $this->smartyOrderfinishPage($smarty);
        } elseif (Frontend::getCart()->istBestellungMoeglich() === 10) {
            $this->addPPCBanner(PPCBannerConfig::POSITION_POPUP_CART, $smarty);
        }
        if (Frontend::getCart()->istBestellungMoeglich() === 10) {
            $this->addExpressPopup($smarty);
        }
    }

    /**
     * @param JTLSmarty $smarty
     * @throws SmartyException
     */
    private function smartyOrderPage(JTLSmarty $smarty): void
    {
        $config = $this->plugin->getConfig();
        if ($GLOBALS['step'] === 'accountwahl' && $config->getValue('jtl_paypal_express_checkout') === 'Y') {
            $payPalExpress = new PayPalExpress();
            $pqMethodCart  = $config->getValue('jtl_paypal_express_checkout_method');
            $btnType       = $config->getValue('jtl_paypal_express_checkout_button_type') === 'silver' ? '-alt' : '';
            $btnSize       = $config->getValue('jtl_paypal_express_checkout_button_size') ?? 'medium';
            $iso           = Text::convertISO2ISO639(Frontend::getInstance()->getLanguage()->getIso());
            $iso           = (!\in_array($iso, self::$allowedIsoCodes, true)) ? 'de' : $iso;
            $ppCheckout    = $this->plugin->getPaths()->getFrontendURL() . 'images/buttons/' . $iso
                . '/checkout-logo-' . $btnSize . $btnType . '-' . $iso . '.png';

            if ($payPalExpress->zahlungErlaubt(PayPalHelper::getProducts())) {
                $link = PayPalHelper::getLinkByName($this->plugin, 'PayPalExpress');
                if ($link !== null) {
                    \pq($config->getValue('jtl_paypal_express_checkout_selector'))->$pqMethodCart(
                        '<a href="index.php?s=' .
                        $link->getID() .
                        '&jtl_paypal_checkout_cart=1" class="mt-3 paypalexpress btn-ppe-checkout">' .
                        '  <img src="' . $ppCheckout . '" alt="' . $this->plugin->getMeta()->getName() . '" />' .
                        '</a>'
                    );
                }
            }
        }
        if ($GLOBALS['step'] === 'Bestaetigung' && PayPalHelper::isPPModuleID($_SESSION['Zahlungsart']->cModulId)) {
            $tpl = $smarty->assign('l10n', $this->plugin->getLocalization()->getTranslations())
                ->assign(
                    'reloadUrl',
                    Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?pendingPayment='
                    . (PendingPayment::load($this->db, Request::getVar('pendingPayment'))
                        ?? PendingPayment::recycle($this->db, null, $_SESSION['Zahlungsart']->cModulId))->getPendingID()
                )
                ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/inc_order_confirmation.tpl');
            \pq('body')->append($tpl);
        }
    }

    /**
     * @param JTLSmarty $smarty
     * @throws \SmartyException
     */
    private function smartyOrderfinishPage(JTLSmarty $smarty): void
    {
        $pendingPayment = PendingPayment::load($this->db, Request::getVar('pendingPayment'));
        if ($pendingPayment !== null) {
            $tpl = $smarty->assign('pendingPayment', $pendingPayment->getPendingID())
                ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/inc_order_finalize.tpl');
            \pq('body')->append($tpl);
        }
    }

    /**
     * @param JTLSmarty $smarty
     * @throws Exception
     */
    private function addExpressPopup(JTLSmarty $smarty): void
    {
        $config = $this->plugin->getConfig();
        if ($config->getValue('jtl_paypal_express_cart_popup') !== 'Y') {
            return;
        }
        $express  = new PayPalExpress();
        $btnType  = $config->getValue('jtl_paypal_express_cart_popup_button_type') === 'silver' ? '-alt' : '';
        $btnSize  = $config->getValue('jtl_paypal_express_cart_popup_size') ?? 'medium';
        $iso      = Text::convertISO2ISO639(Frontend::getInstance()->getLanguage()->getIso());
        $iso      = (!\in_array($iso, self::$allowedIsoCodes, true)) ? 'de' : $iso;
        $checkout = $this->plugin->getPaths()->getFrontendURL() . 'images/buttons/' . $iso
            . '/checkout-logo-' . $btnSize . $btnType . '-' . $iso . '.png';
        if ($express->zahlungErlaubt(PayPalHelper::getProducts(), (int)($_SESSION['Versandart']->kVersandart ?? 0))) {
            $link = PayPalHelper::getLinkByName($this->plugin, 'PayPalExpress');
            if ($link !== null) {
                $smarty->assign('link', $link)
                    ->assign('oPlugin', $this->plugin)
                    ->assign('ppCheckout', $checkout)
                    ->assign('pqMethodCart', $config->getValue('jtl_paypal_express_cart_popup_method'))
                    ->assign('pqSelectorCart', $config->getValue('jtl_paypal_express_cart_popup_selector'));
                $tpl = $smarty->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/inc_basket.tpl');
                \pq('body')->append($tpl);
            }
        }
    }

    /**
     * @param int       $pageType
     * @param JTLSmarty $smarty
     * @throws Exception
     */
    private function addExpressButton(int $pageType, JTLSmarty $smarty): void
    {
        $config = $this->plugin->getConfig();
        if ($config->getValue('jtl_paypal_express_article') !== 'Y'
            && $config->getValue('jtl_paypal_express_cart_button') !== 'Y'
        ) {
            return;
        }
        $payPalExpress   = new PayPalExpress();
        $pqMethodCart    = $config->getValue('jtl_paypal_express_cart_button_method');
        $pqMethodArticle = $config->getValue('jtl_paypal_express_article_method');
        $articleBtnType  = $config->getValue('jtl_paypal_express_article_button_type') === 'silver' ? '-alt' : '';
        $cartBtnType     = $config->getValue('jtl_paypal_express_cart_button_type') === 'silver' ? '-alt' : '';
        $articleBtnSize  = $config->getValue('jtl_paypal_express_article_button_size') ?? 'medium';
        $cartBtnSize     = $config->getValue('jtl_paypal_express_cart_button_size') ?? 'medium';
        $iso             = Text::convertISO2ISO639(Frontend::getInstance()->getLanguage()->getIso());
        $iso             = !\in_array($iso, self::$allowedIsoCodes, true) ? 'de' : $iso;
        $btnImage        = $this->plugin->getPaths()->getFrontendURL() . 'images/buttons/' . $iso
            . '/checkout-logo-' . $articleBtnSize . $articleBtnType . '-' . $iso . '.png';
        $cartImage       = $this->plugin->getPaths()->getFrontendURL() . 'images/buttons/' . $iso
            . '/checkout-logo-' . $cartBtnSize . $cartBtnType . '-' . $iso . '.png';

        if ($pageType === \PAGE_WARENKORB && $config->getValue('jtl_paypal_express_cart_button') === 'Y') {
            if ($payPalExpress->zahlungErlaubt(PayPalHelper::getProducts())) {
                $link = PayPalHelper::getLinkByName($this->plugin, 'PayPalExpress');
                if ($link !== null) {
                    \pq($config->getValue('jtl_paypal_express_cart_button_selector'))->$pqMethodCart(
                        '<a href="index.php?s=' .
                        $link->getID() .
                        '&jtl_paypal_checkout_cart=1" class="btn paypalexpress btn-ppe-cart">' .
                        '  <img src="' . $cartImage . '" alt="' . $this->plugin->getMeta()->getName() . '" />' .
                        '</a>'
                    );
                }
            }
        } elseif ($pageType === \PAGE_ARTIKEL && $config->getValue('jtl_paypal_express_article') === 'Y') {
            $product = $smarty->getTemplateVars('Artikel');
            if ($payPalExpress->zahlungErlaubt([$product])) {
                \pq($config->getValue('jtl_paypal_express_article_selector'))->$pqMethodArticle(
                    '<button name="jtl_paypal_redirect" type="submit" value="2"' .
                    ' class="paypalexpress btn-ppe-article">' .
                    '  <img src="' . $btnImage . '" alt="' . $this->plugin->getMeta()->getName() . '" />' .
                    '</button>'
                );
            }
        }
    }

    /**
     * @param string    $position
     * @param JTLSmarty $smarty
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    private function addPPCBanner(string $position, JTLSmarty $smarty): void
    {
        $ppcConfig = PPCBannerConfig::instance($this->plugin, $this->db);
        if (!$ppcConfig->isPPCBannerActiv()) {
            return;
        }

        $settings    = $ppcConfig->getConfigItemsByPrefix(PPCBannerConfig::PREFIX);
        $apiClientID = $settings->get('api_client_id');
        if (empty($apiClientID)) {
            return;
        }
        $positions = [];
        foreach ([$position, PPCBannerConfig::POSITION_POPUP_CART] as $pos) {
            if ($ppcConfig->isPPCBannerActiv($pos)) {
                $positions[$pos] = [
                    'selector' => $settings->get($pos . '_query_selector'),
                    'method'   => $settings->get($pos . '_query_method', 'append')
                ];
            }
        }
        if (\count($positions) === 0) {
            return;
        }
        /** @var Artikel $product */
        $product = $smarty->getTemplateVars('Artikel');
        $cmAvail = Shop::getConfigValue(\CONF_CONSENTMANAGER, 'consent_manager_active') ?? 'N';
        try {
            $banner = $smarty
                ->assign('settings', $settings)
                ->assign('positions', $positions)
                ->assign('apiNamespace', PPCBannerConfig::PREFIX . 'widget')
                ->assign('apiURL', $ppcConfig->getAPIUrl($apiClientID, Frontend::getCurrency()->getCode()))
                ->assign('productPrice', $product instanceof Artikel && $product->inWarenkorbLegbar
                    ? $product->Preise->fVK[Frontend::getCustomerGroup()->getIsMerchant()]
                    : 0)
                ->assign('wkPrice', Frontend::getCart()->gibGesamtsummeWaren(
                    !Frontend::getCustomerGroup()->isMerchant()
                ))
                ->assign('consentID', PPCBannerConfig::CONSENT_ITEM_ID)
                ->assign('hasConsent', $cmAvail !== 'Y' || $settings->get('use_consent', 'N') === 'N'
                    || Shop::Container()->getConsentManager()->hasConsent(PPCBannerConfig::CONSENT_ITEM_ID))
                ->assign('consentName', $this->getBackendTransalation('PayPal Ratenzahlung'))
                ->assign('consentPlaceholder', $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_ppcbanner_consent_placeholder'
                ))
                ->fetch($this->plugin->getPaths()->getFrontendPath() . '/template/ppc_banner.tpl');
        } catch (SmartyException | Exception $e) {
            Shop::Container()->getLogService()->error($e->getMessage());

            return;
        }
        \pq('body')->append($banner);
    }

    /**
     * @return void
     */
    public function postGet(): void
    {
        // redirect to paypal express when button on product details page was clicked
        if (isset($_POST['jtl_paypal_redirect']) && $_POST['jtl_paypal_redirect'] === '2'
            && $this->plugin->getConfig()->getValue('jtl_paypal_express_article') === 'Y'
        ) {
            $conf = Shop::getSettings([\CONF_GLOBAL]);
            if (!isset($_SESSION['variBoxAnzahl_arr']) && $conf['global']['global_warenkorb_weiterleitung'] === 'Y') {
                $_SESSION['variBoxAnzahl_arr'] = [];
            }
        }

        // track campaign
        if ((isset($_POST['jtl_paypal_redirect']) && $_POST['jtl_paypal_redirect'] === '2')
            || (isset($_GET['jtl_paypal_checkout_cart']) && $_GET['jtl_paypal_checkout_cart'] === '1'
                && !isset($_SESSION['jtl_paypal_set_campaign']))
        ) {
            $campaign = false;
            // product details page button
            if (isset($_POST['jtl_paypal_redirect']) && $_POST['jtl_paypal_redirect'] === '2') {
                $sql      = "SELECT * FROM tkampagne WHERE cParameter LIKE 'jtl_paypal_redirect'";
                $campaign = $this->db->query($sql, ReturnType::SINGLE_OBJECT);
            }
            // cart button
            if (isset($_GET['jtl_paypal_checkout_cart']) && $_GET['jtl_paypal_checkout_cart'] === '1') {
                $sql      = "SELECT * FROM tkampagne WHERE cParameter LIKE 'jtl_paypal_checkout_cart'";
                $campaign = $this->db->query($sql, ReturnType::SINGLE_OBJECT);
            }
            if ($campaign !== false && $campaign !== null) {
                $event               = new stdClass();
                $event->kKampagne    = $campaign->kKampagne;
                $event->kKampagneDef = 1;
                $event->kKey         = $_SESSION['oBesucher']->kBesucher;
                $event->fWert        = 1.0;
                $event->cParamWert   = $campaign->cWert;
                $event->dErstellt    = 'now()';
                $event->cCustomData  = $_SERVER['REQUEST_URI'] . ';' . Visitor::getReferer();

                $this->db->insert('tkampagnevorgang', $event);
                $_SESSION['jtl_paypal_set_campaign'] = 1;
            }
        }

        if (isset($_POST['jtl_paypal_redirect'])) {
            \header('Location: ' . Shop::Container()->getLinkService()->getStaticRoute('warenkorb.php') .
                '?jtl_paypal_redirect=1');
        }
    }

    /**
     * @return void
     */
    public function lastInclude(): void
    {
        if (($message = PayPalHelper::getFlashMessage())) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $message,
                'paypal_message',
                ['saveInSession' => true]
            );
            PayPalHelper::clearFlashMessage();
        }
    }

    /**
     * @return void
     */
    public function cartPage(): void
    {
        if (isset($_GET['jtl_paypal_redirect'])) {
            if (isset($_SESSION['variBoxAnzahl_arr']) && empty($_SESSION['variBoxAnzahl_arr'])) {
                unset($_SESSION['variBoxAnzahl_arr']);
            }

            $link = PayPalHelper::getLinkByName($this->plugin, 'PayPalExpress');
            if ($link !== null) {
                $loc = Shop::getURL() . '/?s=' . $link->getID();
                if (isset($_GET['return'])) {
                    \header('Location: ' . $loc . '&return=1&jtl_paypal_redirect=1');
                } else {
                    \header('Location: ' . $loc . '&jtl_paypal_redirect=1');
                }
            }
            exit;
        }

        if (isset($_GET['fillOut'])) {
            $message = '';
            switch ($_GET['fillOut']) {
                case 'ppexpress_max':
                    $message = \str_replace(
                        '%Wert%',
                        Preise::getLocalizedPriceString($_GET['max']),
                        $this->plugin->getLocalization()->getTranslation('jtl_paypal_warenkorb_max')
                    );
                    break;
                case 'ppexpress_min':
                    $message = \str_replace(
                        '%Wert%',
                        Preise::getLocalizedPriceString($_GET['min']),
                        $this->plugin->getLocalization()->getTranslation('jtl_paypal_warenkorb_min')
                    );
                    break;
                case 'ppexpress_notallowed':
                    $message = $this->plugin->getLocalization()->getTranslation('jtl_paypal_notallowed');
                    break;
                case 'ppexpress_blocked':
                    $message = Shop::Lang()->get('accountLocked');
                    break;
                case 'ppexpress_inactive':
                    $message = Shop::Lang()->get('accountInactive');
                    break;
                case 'ppexpress_internal':
                case 'ppbasic_internal':
                    $message = Shop::Lang()->get('paypalHttpError', 'paymentMethods');
                    break;
            }
            if (!empty($message)) {
                Shop::Smarty()->assign('MsgWarning', $message);
            }
        }
    }

    /**
     * @return void
     */
    public function shippingPlausi(): void
    {
        if (!isset($_SESSION['paypalexpress'], $_SESSION['Versandart']->kVersandart)) {
            return;
        }
        $paypalexpress = new PayPalExpress();
        $products      = PayPalHelper::getProducts();

        if ($paypalexpress->zahlungErlaubt($products, (int)$_SESSION['Versandart']->kVersandart)) {
            $paypalexpress->zahlungsartsession($_SESSION['Versandart']->kVersandart);

            PayPalHelper::addSurcharge();
        }
    }

    /**
     * @param array $args
     */
    public function checkPendingPayment(array $args): void
    {
        if (!isset($_GET['pendingPayment'])
            || !\in_array((int)$args['pageType'], [\PAGE_WARENKORB, \PAGE_BESTELLVORGANG], true)
        ) {
            return;
        }

        $pendingPayment = PendingPayment::load($this->db, Request::getVar('pendingPayment'));
        if ($pendingPayment === null || $pendingPayment->state() === PendingPayment::STATE_NONE) {
            return;
        }

        $this->checkPendingRedirect($pendingPayment);
        $this->checkPendingOrderSuccess($pendingPayment);
        $link = PayPalHelper::getLinkByName($this->plugin, 'PayPalCheckPayment');
        if (\is_a($link, LinkInterface::class)) {
            \header('Location: ' . $link->getURL() . '?pendingPayment=' . $pendingPayment->getPendingID());
        }
    }

    /**
     * @param PendingPayment $pendingPayment
     * @return string|null
     */
    private function getPendingOrderHash(PendingPayment $pendingPayment): ?string
    {
        if ($pendingPayment->state() === PendingPayment::STATE_FINISHED) {
            $lastOrder = $this->db->queryPrepared(
                'SELECT cId FROM tbestellid WHERE kBestellung = :orderId',
                ['orderId' => $pendingPayment->getOrderID()],
                ReturnType::SINGLE_OBJECT
            );

            if ($lastOrder && !empty($lastOrder->cId)) {
                return $lastOrder->cId;
            }
        }

        return null;
    }

    /**
     * @param PendingPayment $pendingPayment
     */
    public function checkPendingOrderSuccess(PendingPayment $pendingPayment): void
    {
        if (($lastOrderHash = $this->getPendingOrderHash($pendingPayment)) !== null) {
            \header('Location: ' . Shop::Container()->getLinkService()->getStaticRoute('bestellabschluss.php')
                . '?i=' . $lastOrderHash
                . '&pendingPayment=' . $pendingPayment->getPendingID());
            exit;
        }
    }

    /**
     * @param PendingPayment $pendingPayment
     */
    public function checkPendingRedirect(PendingPayment $pendingPayment): void
    {
        if (!empty($redirect = $pendingPayment->getRedirect())) {
            $pendingPayment->setRedirect(null)->write();
            \header('Location: ' . $redirect);
            exit;
        }
    }

    /**
     * @param string $pendingID
     * @return void
     */
    public function resetLastOrder(string $pendingID): void
    {
        $pendingPayment = PendingPayment::load($this->db, $pendingID);
        if ($pendingPayment !== null) {
            $pendingPayment->finish();
        }
    }

    /**
     * @param string $pendingID
     * @return object
     */
    public function checkLastOrder(string $pendingID): object
    {
        $pendingPayment = PendingPayment::load($this->db, $pendingID);
        $result         = new IOResponse();

        if ($pendingPayment !== null) {
            if (!empty($redirect = $pendingPayment->getRedirect())) {
                $result->setClientRedirect($redirect);
                $pendingPayment->setRedirect(null)->write();
            } elseif (($lastOrderHash = $this->getPendingOrderHash($pendingPayment)) !== null) {
                $result->setClientRedirect(Shop::Container()->getLinkService()->getStaticRoute('bestellabschluss.php')
                    . '?i=' . $lastOrderHash
                    . '&pendingPayment=' . $pendingPayment->getPendingID());
            } elseif ($pendingPayment->isTimeout()) {
                $pendingPayment->finish();
                $result->assignVar('timeout', true);
            } else {
                $result->assignVar('timeout', false);
            }
        } else {
            $result->assignVar('timeout', true);
        }

        return $result;
    }

    /**
     * @return object
     */
    public function getWKSum(): object
    {
        $result = new IOResponse();
        $result->assignVar('wkSum', Frontend::getCart()->gibGesamtsummeWaren(
            !Frontend::getCustomerGroup()->isMerchant()
        ));

        return $result;
    }

    /**
     * @param array $args
     * @throws Exception
     */
    public function ioRequest(array $args): void
    {
        /** @var IO $io */
        $io = $args['io'];
        $io->register('jtl_paypal.resetLastOrder', [$this, 'resetLastOrder'])
            ->register('jtl_paypal.checkLastOrder', [$this, 'checkLastOrder'])
            ->register('jtl_paypal.getWKSum', [$this, 'getWKSum']);
    }

    /**
     * @param array $args
     */
    public function addConsentItem(array $args): void
    {
        $lastID  = $args['items']->reduce(static function ($result, Item $item) {
                $value = $item->getID();

                return $result === null || $value > $result ? $value : $result;
            }) ?? 0;
        $locale  = $this->plugin->getLocalization();
        $cmAvail = Shop::getConfigValue(\CONF_CONSENTMANAGER, 'consent_manager_active') ?? 'N';
        if ($cmAvail === 'Y' && PPCBannerConfig::instance($this->plugin, $this->db)
                ->getConfigItem(PPCBannerConfig::PREFIX . 'use_consent', 'N') === 'Y'
        ) {
            $langISO = Shop::getLanguageCode();
            $item    = new Item();
            $item->setName($this->getBackendTransalation('PayPal Ratenzahlung'));
            $item->setID(++$lastID);
            $item->setItemID(PPCBannerConfig::CONSENT_ITEM_ID);
            $item->setDescription($locale->getTranslation('jtl_paypal_ppcbanner_consent_description', $langISO));
            $item->setPurpose($locale->getTranslation('jtl_paypal_ppcbanner_consent_purpose', $langISO));
            $item->setPrivacyPolicy(
                'https://www.paypal.com/de/webapps/mpp/ua/privacy-full?locale.x=' . $this->getLocalCode()
            );
            $item->setCompany('PayPal');
            $args['items']->push($item);
        }
    }

    /**
     * @throws Exception
     */
    public function paymentStep(): void
    {
        if (isset($_GET['refresh'])) {
            \header('Location: ' . Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php'));
            exit;
        }
        $smarty         = Shop::Smarty();
        $newMethods     = [];
        $paymentMethods = $smarty->getTemplateVars('Zahlungsarten') ?? [];
        foreach ($paymentMethods as $key => $method) {
            if (!isset($method->cModulId) || \mb_strpos($method->cModulId, 'paypalexpress') === false) {
                $newMethods[] = $method;
            }
        }
        $api = new PayPalPlus();
        if (!$api->isConfigured()
            || !$api->isUseable(PayPalHelper::getProducts(), $_SESSION['Versandart']->kVersandart)
        ) {
            return;
        }
        $payment = $api->createPayment();
        if ($payment === null) {
            return;
        }
        $settings = $api->getSettings();
        $embedded = (int)$settings['jtl_paypal_psp_type'] === 0;
        $link     = PayPalHelper::getLinkByName($this->plugin, 'PayPalPLUS');
        $language = Text::convertISO2ISO639(Shop::getLanguageCode());


        $smarty->assign('language', \sprintf('%s_%s', \mb_strtolower($language), \mb_strtoupper($language)))
            ->assign('Zahlungsarten', $newMethods)
            ->assign('country', $_SESSION['cLieferlandISO'])
            ->assign('embedded', $embedded)
            ->assign('payPalPlus', true)
            ->assign('mode', $api->getMode())
            ->assign('approvalUrl', $payment->getApprovalLink())
            ->assign('paymentId', $payment->getId())
            ->assign('linkId', $link->getID())
            ->assign('styles', $this->getStyles($settings))
            ->assign('jtl_paypal_token', Form::getTokenInput())
            ->assign('thirdPartyPaymentMethods', $embedded ? $this->getThirdPartyMethods($link) : []);
    }

    /**
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    public function orderPage(): void
    {
        global $step;
        if (!isset($_SESSION['paypalexpress']) || $step !== 'Versand') {
            return;
        }
        $smarty = Shop::Smarty();
        \pruefeVersandkostenfreiKuponVorgemerkt();
        $deliveryCountry = Frontend::getDeliveryAddress()->cLand;
        if (!$deliveryCountry) {
            $deliveryCountry = Frontend::getCustomer()->cLand;
        }
        $plz = Frontend::getDeliveryAddress()->cPLZ;
        if (!$plz) {
            $plz = Frontend::getCustomer()->cPLZ;
        }
        $customerGroupID = Frontend::getCustomer()->kKundengruppe;
        if (!$customerGroupID) {
            $customerGroupID = Frontend::getCustomerGroup()->getID();
        }
        $shippingClasses = ShippingMethod::getShippingClasses(Frontend::getCart());
        $methods         = ShippingMethod::getPossibleShippingMethods(
            $deliveryCountry,
            $plz,
            $shippingClasses,
            $customerGroupID
        );
        $packagings      = ShippingMethod::getPossiblePackagings(Frontend::getCustomerGroup()->getID());
        foreach ($methods as $key => $method) {
            $pp_in          = false;
            $paymentMethods = \gibZahlungsarten($method->kVersandart, Frontend::getCustomerGroup()->getID());
            foreach ($paymentMethods as $paymentMethod) {
                if ((int)$paymentMethod->kZahlungsart === (int)$_SESSION['paypalexpress']->sZahlungsart->kZahlungsart) {
                    $pp_in = true;
                }
            }
            if ($pp_in === false) {
                unset($methods[$key]);
            }
        }
        \sort($methods);

        if ((\is_array($methods) && \count($methods) > 1)
            || (\is_array($methods) && \count($methods) === 1
                && \is_array($packagings) && \count($packagings) > 0)
        ) {
            $smarty->assign('Versandarten', $methods)
                ->assign('Verpackungsarten', $packagings);
        } elseif (\is_array($methods)
            && \count($methods) === 1
            && (\is_array($packagings)
                && \count($packagings) === 0)
        ) {
            \pruefeVersandartWahl((int)$methods[0]->kVersandart);
        } elseif (!\is_array($methods) || \count($methods) === 0) {
            Shop::Container()->getLogService()->addError(
                'Es konnte keine Versandart fuer folgende Daten gefunden werden: '
                . 'Lieferland: ' . $deliveryCountry
                . ', PLZ: ' . $plz
                . ', Versandklasse: ' . $shippingClasses
                . ', Kundengruppe: ' . $customerGroupID
            );
        }
    }

    /**
     * @param LinkInterface $link
     * @return array
     */
    private function getThirdPartyMethods(LinkInterface $link): array
    {
        $shopUrl                  = Shop::getURL(true);
        $languageCode             = Shop::getLanguageCode();
        $defaultPayments          = Shop::Smarty()->getTemplateVars('Zahlungsarten');
        $sortedPayments           = [];
        $thirdPartyPaymentMethods = [];
        $availablePayments        = $this->db->query(
            'SELECT * FROM xplugin_jtl_paypal_additional_payment ORDER BY sort',
            ReturnType::ARRAY_OF_OBJECTS
        );
        foreach ($availablePayments as $p) {
            foreach ($defaultPayments as $d) {
                if ((int)$p->paymentId === (int)$d->kZahlungsart) {
                    $sortedPayments[] = $d;
                    break;
                }
            }
        }
        foreach ($sortedPayments as $i => $p) {
            if ($i >= 5) {
                break;
            }
            $thirdParty = [
                'methodName'  => PayPalHelper::shortenPaymentName($p->angezeigterName[$languageCode]),
                'redirectUrl' => \sprintf(
                    '%s/index.php?s=%d&a=payment_method&id=%d',
                    $shopUrl,
                    $link->getID(),
                    $p->kZahlungsart
                )
            ];
            if (!empty($p->cBild)) {
                if (\mb_strpos($p->cBild, 'http') !== 0) {
                    $p->cBild = $shopUrl . '/' . \ltrim($p->cBild, '/');
                }
                $thirdParty['imageUrl'] = \str_replace('http://', 'https://', $p->cBild);
            }
            if (!empty($p->cHinweisText[$languageCode])) {
                $thirdParty['description'] = $p->cHinweisText[$languageCode];
            }
            $thirdPartyPaymentMethods[] = $thirdParty;
        }

        return $thirdPartyPaymentMethods;
    }

    /**
     * @param array $settings
     * @return stdClass|null
     */
    private function getStyles(array $settings): ?stdClass
    {
        if ($settings['jtl_paypal_style_enabled'] !== 'Y') {
            return null;
        }

        return (object)[
            'psp'     => (object)[
                'font-family' => $settings['jtl_paypal_style_font_family'],
                'color'       => $settings['jtl_paypal_style_link_color'],
                'font-size'   => $settings['jtl_paypal_style_font_size'],
                'font-style'  => $settings['jtl_paypal_style_font_style'],
            ],
            'link'    => (object)[
                'color'           => $settings['jtl_paypal_style_link_color'],
                'text-decoration' => $settings['jtl_paypal_style_link_text_decoration'],
            ],
            'visited' => (object)[
                'color'           => $settings['jtl_paypal_style_visited_color'],
                'text-decoration' => $settings['jtl_paypal_style_visited_text_decoration'],
            ],
            'active'  => (object)[
                'color'           => $settings['jtl_paypal_style_active_color'],
                'text-decoration' => $settings['jtl_paypal_style_active_text_decoration'],
            ],
            'hover'   => (object)[
                'color'           => $settings['jtl_paypal_style_hover_color'],
                'text-decoration' => $settings['jtl_paypal_style_hover_text_decoration'],
            ],
        ];
    }
}
