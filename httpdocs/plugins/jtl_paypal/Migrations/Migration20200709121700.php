<?php

namespace Plugin\jtl_paypal\Migrations;

use JTL\DB\ReturnType;
use JTL\Plugin\Migration;
use JTL\Update\IMigration;

/**
 * Class Migration20191024124500
 * @package Plugin\jtl_paypal\Migrations
 */
class Migration20200709121700 extends Migration implements IMigration
{
    /**
     * @var string
     */
    protected $description = /** @lang text */
        'Create table for pending payments';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->getDB()->executeQuery(
            "CREATE TABLE xplugin_jtl_paypal_pending_payment (
                id INT NOT NULL AUTO_INCREMENT,
                pending_id VARCHAR(128) NOT NULL,
                module_id  VARCHAR(64)  NOT NULL,
                order_no   VARCHAR(20)      NULL,
                order_id   INT              NULL,
                redirect   VARCHAR(1024)    NULL,
                created    DATETIME     NOT NULL,
                PRIMARY KEY (id),
                UNIQUE KEY idx_pending_uq (pending_id),
                INDEX      idx_created(created)
            ) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci'",
            ReturnType::DEFAULT
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->getDB()->executeQuery('DROP TABLE IF EXISTS xplugin_jtl_paypal_pending_payment', ReturnType::DEFAULT);
    }
}
