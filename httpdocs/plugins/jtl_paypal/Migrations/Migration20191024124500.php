<?php

namespace Plugin\jtl_paypal\Migrations;

use JTL\DB\ReturnType;
use JTL\Plugin\Helper;
use JTL\Plugin\Migration;
use JTL\Shop;
use JTL\Update\IMigration;

/**
 * Class Migration20191024124500
 * @package Plugin\jtl_paypal\Migrations
 */
class Migration20191024124500 extends Migration implements IMigration
{
    /**
     * @var array
     */
    private $addSelectors = [
        'jtl_paypal_express_cart_button_selector' => '#cart-checkout-btn',
        'jtl_paypal_express_article_selector'     => '#add-to-cart.product-buy > div:last',
        'jtl_paypal_express_cart_popup_selector'  => '.cart-icon-dropdown.nav-item .dropdown-body > ul > li:last'
    ];

    /**
     * @var array
     */
    private $removeSelectors = [
        'jtl_paypal_express_article_selector'     => '#add-to-cart .form-inline',
        'jtl_paypal_express_cart_button_selector' => '.panel-note, #basket_checkout, .basket-well .proceed'
    ];

    private $updateMethods = [
        'jtl_paypal_express_cart_button_method' => 'append:after',
        'jtl_paypal_express_article_method'     => 'after:before',
    ];

    /**
     * @var string
     */
    protected $description = 'Add new PP-Express selectors for NOVA';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $db = $this->getDB();
        foreach ($this->addSelectors as $name => $value) {
            $db->queryPrepared(
                "UPDATE tplugineinstellungen SET cWert = CONCAT(cWert, ', ', :settingValue)
                    WHERE cName = :settingName
                        AND cWert NOT LIKE CONCAT('%', :settingValue, '%')",
                [
                    'settingName'  => $name,
                    'settingValue' => $value,
                ],
                ReturnType::DEFAULT
            );
        }
        foreach ($this->removeSelectors as $name => $value) {
            $db->queryPrepared(
                "UPDATE tplugineinstellungen
                    SET cWert = TRIM(BOTH ', ' FROM REPLACE(REPLACE(cWert, :settingValue, ''), ', ,', ','))
                    WHERE cName = :settingName
                        AND cWert LIKE CONCAT('%', :settingValue, '%')",
                [
                    'settingName'  => $name,
                    'settingValue' => $value,
                ],
                ReturnType::DEFAULT
            );
        }
        foreach ($this->updateMethods as $name => $value) {
            $method = \explode(':', $value);
            $db->queryPrepared(
                'UPDATE tplugineinstellungen SET cWert = :newValue
                    WHERE cName = :settingName
                        AND cWert = :oldValue',
                [
                    'settingName' => $name,
                    'newValue'    => $method[1],
                    'oldValue'    => $method[0]
                ],
                ReturnType::DEFAULT
            );
        }
        Shop::Container()->getCache()->flushTags([\CACHING_GROUP_PLUGIN . '_' . Helper::getIDByPluginID('jtl_paypal')]);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $db = $this->getDB();
        foreach ($this->addSelectors as $name => $value) {
            $db->queryPrepared(
                "UPDATE tplugineinstellungen
                    SET cWert = TRIM(BOTH ', ' FROM REPLACE(REPLACE(cWert, :settingValue, ''), ', ,', ','))
                    WHERE cName = :settingName
                        AND cWert LIKE CONCAT('%', :settingValue, '%')",
                [
                    'settingName'  => $name,
                    'settingValue' => $value,
                ],
                ReturnType::DEFAULT
            );
        }
        foreach ($this->removeSelectors as $name => $value) {
            $db->queryPrepared(
                "UPDATE tplugineinstellungen SET cWert = CONCAT(cWert, ', ', :settingValue)
                    WHERE cName = :settingName
                        AND cWert NOT LIKE CONCAT('%', :settingValue, '%')",
                [
                    'settingName'  => $name,
                    'settingValue' => $value,
                ],
                ReturnType::DEFAULT
            );
        }
        foreach ($this->updateMethods as $name => $value) {
            $method = \explode(':', $value);
            $db->queryPrepared(
                'UPDATE tplugineinstellungen SET cWert = :oldValue
                    WHERE cName = :settingName
                        AND cWert = :newValue',
                [
                    'settingName' => $name,
                    'newValue'    => $method[1],
                    'oldValue'    => $method[0]
                ],
                ReturnType::DEFAULT
            );
        }
        Shop::Container()->getCache()->flushTags([\CACHING_GROUP_PLUGIN . '_' . Helper::getIDByPluginID('jtl_paypal')]);
    }
}
