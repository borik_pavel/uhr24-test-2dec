<?php

namespace PayPal\Api;

use InvalidArgumentException;
use PayPal\Rest\ApiContext;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\UrlValidator;

/**
 * Class WebhookSimulate.
 *
 * One or more webhook objects.
 *
 *
 * @property string           url
 * @property WebhookEventType event_type
 */
class WebhookSimulate extends WebhookEventType
{
    /**
     * @param string $url
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setUrl($url): self
    {
        UrlValidator::validate($url, 'Url');
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param WebhookEventType $event_type
     * @return $this
     */
    public function setEventType($event_type): self
    {
        $this->event_type = $event_type;

        return $this;
    }

    /**
     * @return WebhookEventType
     */
    public function getEventType()
    {
        return $this->event_type;
    }

    /**
     * @param ApiContext     $apiContext can be used to pass dynamic configuration and credentials.
     * @param PayPalRestCall $restCall the Rest Call Service that is used to make rest calls
     * @return WebhookEventType
     */
    public function simulate($apiContext = null, $restCall = null): WebhookEventType
    {
        $payLoad = $this->toJSON();
        $json    = self::executeCall(
            '/v1/notifications/simulate-event',
            'POST',
            $payLoad,
            null,
            $apiContext,
            $restCall
        );
        $ret     = new WebhookEventType();
        $ret->fromJson($json);

        return $ret;
    }
}
