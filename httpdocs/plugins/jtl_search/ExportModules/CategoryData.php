<?php

namespace Plugin\jtl_search\ExportModules;

use Exception;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Helpers\URL;
use JTL\Language\LanguageModel;
use JTL\Media\Image;
use JTL\Shop;
use Psr\Log\LoggerInterface;
use stdClass;

/**
 * Class CategoryData
 * @package Plugin\jtl_search\ExportModules
 */
class CategoryData implements IItemData
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var stdClass[]
     */
    private $languages;

    /**
     * @var int[]
     */
    private $customerGroups;

    /**
     * @var stdClass|null
     */
    private $categoryData;

    /**
     * @var DbInterface
     */
    private $db;

    /**
     * CategoryData constructor.
     * @param LoggerInterface $logger
     * @param DbInterface     $db
     * @param int             $categoryID
     */
    public function __construct(LoggerInterface $logger, DbInterface $db, int $categoryID = 0)
    {
        $this->logger = $logger;
        $this->db     = $db;
        try {
            $langData      = $this->db->query(
                'SELECT tsprache.* 
                    FROM tsprache 
                    JOIN tjtlsearchexportlanguage 
                        ON tsprache.cISO = tjtlsearchexportlanguage.cISO 
                    ORDER BY cShopStandard DESC',
                ReturnType::ARRAY_OF_OBJECTS
            );
            foreach ($langData as $item) {
                $this->languages[] = LanguageModel::load($item, $this->db);
            }
            $this->customerGroups = $this->db->query(
                'SELECT kKundengruppe FROM tkundengruppe',
                ReturnType::COLLECTION
            )->map(static function ($e) {
                return (int)$e->kKundengruppe;
            });
            if ($categoryID > 0) {
                $this->loadFromDB($categoryID);
            }
        } catch (Exception $e) {
            $this->logger->warning(__FILE__ . ':' . __CLASS__ . '->' . __METHOD__ .
                ': ' . \__('loggerErrorCreateCategoryObject'));
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return (int)$this->db->query(
            'SELECT COUNT(*) AS cnt FROM tkategorie',
            ReturnType::SINGLE_OBJECT
        )->cnt;
    }

    /**
     * @inheritDoc
     */
    public static function getItemKeys(DbInterface $db, int $nLimitN, int $nLimitM): array
    {
        return $db->query(
            'SELECT kKategorie AS kItem 
                FROM tkategorie 
                ORDER BY kKategorie 
                LIMIT ' . $nLimitN . ', ' . $nLimitM,
            ReturnType::ARRAY_OF_OBJECTS
        );
    }

    /**
     * @inheritDoc
     */
    public function loadFromDB(int $id, int $languageID = 0, bool $noCache = true)
    {
        unset($this->categoryData);
        if ($id > 0) {
            $category = $this->db->queryPrepared(
                "SELECT tkategorie.*, tkategoriepict.cPfad,
                    (SELECT cWert 
                        FROM tkategorieattribut 
                        WHERE cName = 'meta_keywords' 
                            AND kKategorie = :cid 
                        LIMIT 0, 1) AS cKeywords
                    FROM tkategorie 
                    LEFT JOIN tkategoriepict 
                        ON tkategoriepict.kKategorie = tkategorie.kKategorie
                    WHERE tkategorie.kKategorie = :cid",
                ['cid' => $id],
                ReturnType::SINGLE_OBJECT
            );

            if ($category === false && !\is_object($category)) {
                $this->logger->warning(
                    __FILE__ . ':' . __CLASS__ . '->' . __METHOD__ . '; ' . \__('loggerErrorLoadCategory')
                );
                unset($this->categoryData);
            } else {
                $this->categoryData = $category;
                $this->loadCategoryLanguageFromDB()
                    ->loadCategoryVisibilityFromDB();
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function loadCategoryLanguageFromDB(): self
    {
        $localizations = false;
        if (isset($this->categoryData->kKategorie)) {
            $localizations = $this->db->selectAll(
                'tkategoriesprache',
                'kKategorie',
                $this->categoryData->kKategorie
            );
        }

        if ($localizations === false || !\is_array($localizations)) {
            $this->categoryData->oCategoryLanguage_arr = [];
        } else {
            $this->categoryData->oCategoryLanguage_arr = [];
            foreach ($localizations as $localization) {
                $this->categoryData->oCategoryLanguage_arr[$localization->kSprache] = $localization;
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function loadCategoryVisibilityFromDB(): self
    {
        if (isset($this->categoryData->kKategorie)) {
            $data = $this->db->selectAll(
                'tkategoriesichtbarkeit',
                'kKategorie',
                $this->categoryData->kKategorie
            );
            foreach ($this->customerGroups as $groupID) {
                $this->categoryData->bUsergroupVisible[$groupID] = true;
            }
            if ($data !== false && \is_array($data)) {
                foreach ($data as $item) {
                    $this->categoryData->bUsergroupVisible[(int)$item->kKundengruppe] = false;
                }
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    private function getDefaultLanguage()
    {
        $bLanguage = false;

        foreach ($this->languages as $lang) {
            if ($lang->cShopStandard === 'Y') {
                return $lang;
            }
            $bLanguage = true;
        }

        if ($bLanguage) {
            $this->logger->warning(__FILE__ . ':' . __CLASS__ . '->' . __METHOD__ . '; ' . \__('errorStandardLanguage'));
            die(\__('searchErrorNoStandardLanguage'));
        }

        $this->logger->warning(__FILE__ . ':' . __CLASS__ . '->' . __METHOD__ . '; ' . \__('errorNoLanguage'));
        die(\__('searchErrorNoLanguages'));
    }

    /**
     * @return Category
     */
    public function getFilledObject(): Category
    {
        $defaultLanguage = $this->getDefaultLanguage();
        $shopURL         = Shop::getURL() . '/';
        $category        = new Category();
        if (!empty($this->categoryData->cPfad)) {
            $category->setPictureURL(Shop::getImageBaseURL() .
                \JTL\Media\Image\Product::getThumb(
                    Image::TYPE_CATEGORY,
                    $this->categoryData->kKategorie,
                    $this->categoryData,
                    Image::SIZE_SM,
                    0
                ));
        } else {
            $category->setPictureURL(Shop::getImageBaseURL() . \BILD_KEIN_KATEGORIEBILD_VORHANDEN);
        }
        $category->setId($this->categoryData->kKategorie)
            ->setMasterCategory($this->categoryData->kOberKategorie)
            ->setPriority(5);
        if ($defaultLanguage->cStandard === $defaultLanguage->cShopStandard) {
            $category->setName($this->categoryData->cName, $defaultLanguage->cISO)
                ->setDescription($this->categoryData->cBeschreibung, $defaultLanguage->cISO)
                ->setKeywords($this->categoryData->cKeywords, $defaultLanguage->cISO)
                ->setURL(
                    $shopURL . URL::buildURL($this->categoryData, \URLART_KATEGORIE),
                    $defaultLanguage->cISO
                );
        }

        $_SESSION['Sprachen'] = $this->languages;
        foreach ($this->languages as $language) {
            if (isset($this->categoryData->oCategoryLanguage_arr[$language->kSprache])) {
                $category->setName(
                    $this->categoryData->oCategoryLanguage_arr[$language->kSprache]->cName,
                    $language->cISO
                )->setDescription(
                    $this->categoryData->oCategoryLanguage_arr[$language->kSprache]->cBeschreibung,
                    $language->cISO
                );
                if (isset($this->categoryData->oCategoryLanguage_arr[$language->kSprache]->cKeywords)) {
                    $category->setKeywords(
                        $this->categoryData->oCategoryLanguage_arr[$language->kSprache]->cKeywords,
                        $language->cISO
                    );
                }
                $_SESSION['kSprache']    = $language->kSprache;
                $_SESSION['cISOSprache'] = $language->cISO;
                $category->setURL(
                    $shopURL .
                    URL::buildURL($this->categoryData->oCategoryLanguage_arr[$language->kSprache], \URLART_KATEGORIE),
                    $language->cISO
                );
                unset($_SESSION['kSprache'], $_SESSION['cISOSprache']);
            }
        }
        unset($_SESSION['Sprachen']);

        foreach ($this->customerGroups as $customerGroupID) {
            $category->setVisibility(
                $this->categoryData->bUsergroupVisible[$customerGroupID],
                $customerGroupID
            );
        }

        return $category;
    }
}
