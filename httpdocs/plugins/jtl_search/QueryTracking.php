<?php

namespace Plugin\jtl_search;

/**
 * Class QueryTracking
 * @package Plugin\jtl_search
 */
class QueryTracking
{
    /**
     * @param array $products
     * @return array
     */
    public static function filterProductKeys(array $products): array
    {
        $productKeys = [];
        if (\is_array($products) && count($products) > 0) {
            foreach ($products as $Product) {
                $productKeys[] = (int)$Product->nId;
            }
        }

        return $productKeys;
    }

    /**
     * @param array $products
     * @param array $productsExist
     * @return int
     */
    public static function addProducts(array $products, array &$productsExist): int
    {
        $i = 0;
        if (\is_array($products) && \count($products) > 0 && \is_array($productsExist)) {
            foreach ($products as $product) {
                if (!\in_array($product, $productsExist)) {
                    $productsExist[] = $product;
                    $i++;
                }
            }
        }

        return $i;
    }

    /**
     * @param array $queryTrackings
     * @return array|null
     */
    public static function orderQueryTrackings(array $queryTrackings): ?array
    {
        if (\is_array($queryTrackings) && \count($queryTrackings) > 0) {
            $ordered = [];
            foreach ($queryTrackings as $queryTracking) {
                $ordered[$queryTracking->nQueryTracking] = $queryTracking;
            }
            \ksort($ordered);

            return \array_reverse($ordered);
        }

        return null;
    }
}
