# Changelog

## 1.20.2
* Darstellungsfehler im Evo-Template behoben
* Auswahl von Sortiermöglichkeiten in normalen Produktlisten behoben
* Sortierung wird auch ohne Neuladen einer Suchergebnisseite übernommen

## 1.20.1
* Fehler mit Sprachen in Shop 5 behoben
* Probleme beim Scrollen in mobiler Ansicht behoben
* JQuery und JQueryUI werden nicht mehr via CDN ausgeliefert

## 1.20.0
* Shop5-Kompatibilität
