<?php

use JTL\Plugin\Helper;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_search\JtlSearch;

require_once __DIR__ . '/../../../includes/globalinclude.php';

Session::getInstance();
require_once PFAD_ROOT . PFAD_INCLUDES . 'smartyInclude.php';

$plugin = Helper::getPluginById('jtl_search');
$query  = trim($_POST['k']);

if ($plugin !== null && $plugin->getID() > 0 && strlen($query) >= 3) {
    $templatePath     = $plugin->getPaths()->getFrontendPath() . PFAD_PLUGIN_TEMPLATE;
    $templateSettings = Shop::getConfig([CONF_TEMPLATE])['template'];
    if (strlen($plugin->getConfig()->getValue('cProjectId')) > 0
        && strlen($plugin->getConfig()->getValue('cAuthHash')) > 0
        && strlen($plugin->getConfig()->getValue('cServerUrl')) > 0
    ) {
        require_once $plugin->getPaths()->getBasePath() . 'includes/defines_inc.php';

        $response = null;
        $queries  = JtlSearch::doSuggest(
            Frontend::getCustomerGroup()->getID(),
            Shop::getLanguageCode(),
            Frontend::getCurrency()->getCode(),
            $query,
            $plugin->getConfig()->getValue('cProjectId'),
            $plugin->getConfig()->getValue('cAuthHash'),
            urldecode($plugin->getConfig()->getValue('cServerUrl')),
            $response
        );
        if (is_array($queries) && count($queries) > 0) {
            Shop::Smarty()->assign('cSearch', StringHandler::filterXSS($query))
                ->assign('queries', $queries)
                ->assign('localization', $plugin->getLocalization())
                ->assign('cTemplatePath', $templatePath)
                ->assign('oSearchResponse', $response)
                ->assign('bBranding', $plugin->getConfig()->getValue('jtlsearch_branding') !== '0')
                ->assign('isNova', ($templateSettings['general']['is_nova'] ?? 'N') === 'Y')
                ->assign('noImagePath', Shop::getImageBaseURL() . BILD_KEIN_ARTIKELBILD_VORHANDEN)
                ->display($templatePath . 'results.tpl');
        }
    }
}
