<?php declare(strict_types=1);
namespace Plugin\jtl_google_recaptcha;

/**
 * Interface CaptchaInterface
 * @package Plugin\jtl_google_recaptcha
 */
interface CaptchaInterface
{
    /**
     * @return string|null
     */
    public function getSecretKey(): ?string;

    /**
     * @return string|null
     */
    public function getSiteKey(): ?string;

    /**
     * @return bool
     */
    public function isConfigured(): bool;

    /**
     * @return string
     */
    public function getMarkup(): string;

    /**
     * @param array $requestData
     * @return bool
     */
    public function validate(array $requestData): bool;
}
