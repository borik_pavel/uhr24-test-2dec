<label>{lang key='captcha_code_active' section='global'}</label>
<div class="modal fade reCaptchaModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="display: inline-block">{__('Loading spam protection')}</h4>
                <button type="button" class="x close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">{__('Close')}</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="reCaptchaContainer"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger reCaptchaSend" data-dismiss="modal">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>&nbsp;{__('Send')}
                </button>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    {literal}
    if (typeof reCaptchaCallback === 'undefined') {
        var reCaptchaCallback = '';
        var reCaptchaKey      = {/literal}'{$reCaptchaSitekey}'{literal};
        var reCaptchaSize     = 'invisible';
        var reCaptchaBadge    = 'inline';

        let script = document.createElement('script');
        script.setAttribute('type', 'application/javascript');
        script.setAttribute('async', 'async');
        script.setAttribute('defer', 'defer');
        script.setAttribute('src', {/literal}'{$reCaptchaJSPath}'{literal});
        document.getElementsByTagName('head')[0].appendChild(script);
    }
    {/literal}
</script>