<?php declare(strict_types=1);

namespace Plugin\jtl_google_recaptcha;

use JTL\Backend\Notification;
use JTL\Backend\NotificationEntry;
use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_google_recaptcha\backend\CustomLink;

/**
 * Class Bootstrap
 * @package Plugin\jtl_google_recaptcha
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function boot(Dispatcher $dispatcher)
    {
        parent::boot($dispatcher);

        $dispatcher->listen('backend.notification', [$this, 'checkNotification']);
        $dispatcher->listen('shop.hook.' . \HOOK_CAPTCHA_CONFIGURED, [$this, 'reCaptchaConfigured']);
        $dispatcher->listen('shop.hook.' . \HOOK_CAPTCHA_MARKUP, [$this, 'reCaptchaMarkup']);
        $dispatcher->listen('shop.hook.' . \HOOK_CAPTCHA_VALIDATE, [$this, 'reCaptchaValidate']);
    }

    /**
     * @inheritDoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $plugin = $this->getPlugin();
        $menu   = $plugin->getAdminMenu()->getItems()->first(static function ($item) use ($menuID) {
            return $item->kPluginAdminMenu === $menuID;
        });

        if ($tabName === 'Testing') {
            return CustomLink::handleRequest($plugin, $menu, $_POST['reCaptchaTest'] ?? [])->getOutput();
        }

        return parent::renderAdminMenuTab($tabName, $menuID, $smarty);
    }

    /**
     * @param Notification $notification
     * @return void
     */
    public function checkNotification(Notification $notification): void
    {
        $plugin  = $this->getPlugin();
        $captcha = Captcha::getCaptcha($plugin->getConfig()->getValue('jtl_google_recaptcha_type'), $plugin);
        if (!$captcha->isConfigured()) {
            $notification->add(
                NotificationEntry::TYPE_WARNING,
                $this->getPlugin()->getMeta()->getName(),
                __('Not configured'),
                'plugin.php?kPlugin=' . $this->getPlugin()->getID()
            );
        }
    }

    /**
     * @return CaptchaInterface
     */
    protected function getCaptcha(): CaptchaInterface
    {
        static $captcha;

        if ($captcha === null) {
            $plugin  = $this->getPlugin();
            $captcha = Captcha::getCaptcha($plugin->getConfig()->getValue('jtl_google_recaptcha_type'), $plugin);
        }

        return $captcha;
    }

    /**
     * @param array $args
     * @return void
     */
    public function reCaptchaConfigured(array $args): void
    {
        $args['isConfigured'] = $this->getCaptcha()->isConfigured();
    }

    /**
     * @param array $args
     * @return void
     */
    public function reCaptchaMarkup(array $args): void
    {
        $args['markup'] = (isset($args['getBody']) && $args['getBody'])
            ? $this->getCaptcha()->getMarkup()
            : '';
    }

    /**
     * @param array $args
     * @return void
     */
    public function reCaptchaValidate(array $args): void
    {
        $args['isValid'] = $this->getCaptcha()->validate($args['requestData'] ?? []);
    }
}
