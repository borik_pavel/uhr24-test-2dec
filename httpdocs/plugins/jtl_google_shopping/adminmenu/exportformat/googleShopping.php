<?php declare(strict_types=1);

use JTL\Alert\Alert;
use JTL\Cron\QueueEntry;
use JTL\Shop;
use Plugin\jtl_google_shopping\Exportformat\GoogleShoppingExport;

/** @var \JTL\Exportformat $this */
global $exportformat, $queue, $oJobQueue, $ExportEinstellungen;

try {
    $started = GoogleShoppingExport::export($exportformat, $ExportEinstellungen, Shop::Container()->getDB())
        ->setQueueEntry(is_a($queue, QueueEntry::class) ? $queue : $oJobQueue, isset($oJobQueue))
        ->setExportSQL($this->getExportSQL())
        ->run();

    if ($started) {
        $this->setZuletztErstellt((new DateTime())->format('Y-m-d H:i:s'));
        $this->update();
    }
} catch (Exception $e) {
    Shop::Container()->getAlertService()->addAlert(Alert::TYPE_ERROR, $e->getMessage(), 'googleShopping');
}
