<?php declare(strict_types=1);

namespace Plugin\jtl_google_shopping\Exportformat;

use JTL\Catalog\Product\Artikel;

/**
 * Class Product
 * @package Plugin\jtl_google_shopping\Exportformat
 */
class Product extends Artikel
{
    /**
     * @var string
     */
    public $cDeeplink;

    /**
     * @var string
     */
    public $cVaterArtNr;

    /**
     * @var string
     */
    public $cGroesse;

    /**
     * @var string
     */
    public $cFarbe;

    /**
     * @var string
     */
    public $cGeschlecht;

    /**
     * @var string
     */
    public $cAltersgruppe;

    /**
     * @var string
     */
    public $cMuster;

    /**
     * @var string
     */
    public $cMaterial;

    /**
     * @var string
     */
    public $cVerfuegbarkeit;

    /**
     * @var string
     */
    public $Artikelbild;

    /**
     * @var array
     */
    public $cArtikelbild_arr = [];

    /**
     * @var string
     */
    public $cGtin;

    /**
     * @var string
     */
    public $salePriceEffectiveDate;

    /**
     * @var string
     */
    public $unitPricingMeasure;

    /**
     * @var string
     */
    public $unitPricingBaseMeasure;

    /**
     * @var string
     */
    public $cLieferland;

    /**
     * @var string
     */
    public $Versandkosten;

    /**
     * @var array
     */
    public $cCategorie_arr = [];

    /**
     * @var array
     */
    public $cGoogleCategorie = [];

    /**
     * @var string
     */
    public $cBeschreibungHTML;

    /**
     * @var string
     */
    public $cKurzBeschreibungHTML;

    /**
     * @var string
     */
    public $cZustand;

    /**
     * @var string
     */
    public $bIsBundle;

    /**
     * @var float
     */
    public $salePrice;

    /**
     * @var float
     */
    public $fUst;

    /**
     * @var float
     */
    public $fVKBrutto;
}
