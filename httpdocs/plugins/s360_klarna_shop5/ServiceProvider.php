<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5;

use JTL\Shop;
use JTL\Services\Container;
use JTL\Plugin\Helper;
use JTL\Plugin\PluginInterface;
use JTL\Mail\Mailer;
use JTL\Mail\Mail\Mail;
use Plugin\s360_klarna_shop5\src\Utils\AlertHelper;
use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Utils\Translation;
use Plugin\s360_klarna_shop5\src\Api\Client;
use Plugin\s360_klarna_shop5\src\Api\OrderEndpoint;
use Plugin\s360_klarna_shop5\src\Api\SessionsEndpoint;
use Plugin\s360_klarna_shop5\src\Utils\Session;
use Plugin\s360_klarna_shop5\src\Payments\Model;
use Plugin\s360_klarna_shop5\src\Controllers\PaymentController;
use Plugin\s360_klarna_shop5\src\Payments\Validator;
use Plugin\s360_klarna_shop5\src\Controllers\AdminController;
use Plugin\s360_klarna_shop5\src\Controllers\AjaxOrderManagementController;
use Plugin\s360_klarna_shop5\src\Controllers\OnSiteMessagingController;
use Plugin\s360_klarna_shop5\src\Controllers\SyncController;
use Plugin\s360_klarna_shop5\src\Mappers\Factory;
use Plugin\s360_klarna_shop5\src\Mappers\OrderMapper;
use Plugin\s360_klarna_shop5\src\Mappers\UserDataMapper;
use Plugin\s360_klarna_shop5\src\Messaging\Model as MessagingModel;
use Plugin\s360_klarna_shop5\src\Messaging\OnSiteMessaging;
use Plugin\s360_klarna_shop5\src\Utils\DynamicPaymentNames;

/**
 * Service Provider inspired by laravel service providers.
 * Register and boot all classes and dependencies needed for the plugin.
 *
 * @package Plugin\s360_klarna_shop5
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
final class ServiceProvider
{
    /**
     * @var Container
     */
    protected $app;

    /**
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * Register Services and add them to the app container.
     *
     * @return void
     */
    public function register() : void
    {
        // Session Utility
        $this->app->setFactory(Session::class, function () : Session {
            return new Session();
        });

        // Alias for Helper::getPluginById made into a singleton
        $this->app->setSingleton(Config::PLUGIN_ID, function () : ?PluginInterface {
            return Helper::getPluginById(Config::PLUGIN_ID);
        });

        // Config Class
        $this->app->setSingleton(Config::class, function () : Config {
            return new Config($this->app->get(Config::PLUGIN_ID));
        });

        // Klarna API Client
        $this->app->setSingleton(Client::class, function () : Client {
            return new Client($this->app->get(Config::class), $this->app);
        });

        // Klarna Translation Utility
        $this->app->setSingleton(Translation::class, function () : Translation {
            return new Translation(
                $this->app->get(Config::PLUGIN_ID)->getLocalization(),
                $this->app->getGetText()
            );
        });

        // Klarna AlertHelper Utility
        $this->app->setSingleton(AlertHelper::class, function () : AlertHelper {
            return new AlertHelper(
                $this->app->getAlertService(),
                $this->app->getLinkService(),
                $this->app->get(Translation::class)
            );
        });

        // Mapper Factory
        $this->app->setFactory(Factory::class, function () : Factory {
            $cfg = $this->app->get(Config::class);
            $model = $this->app->get(Model::class);
            $translation = $this->app->get(Translation::class);

            return new Factory(
                new OrderMapper($cfg, $model, $translation),
                new UserDataMapper($cfg, $model, $translation),
                $this->app->getDB()
            );
        });

        // Klarna Payment Validator
        $this->app->setFactory(Validator::class, function () : Validator {
            return new Validator(
                $this->app->get(Session::class),
                $this->app->get(AlertHelper::class)
            );
        });

        // Payment Controller
        $this->app->setFactory(PaymentController::class, function () : PaymentController {
            return new PaymentController(
                $this->app->get(Session::class),
                $this->app->get(Config::PLUGIN_ID),
                Shop::Smarty(),
                $this->app->get(Client::class),
                $this->app->get(Config::class),
                $this->app->get(Translation::class)
            );
        });

        // Sync Controller
        $this->app->setFactory(SyncController::class, function () : SyncController {
            $controller = new SyncController(
                $this->app->get(Session::class),
                $this->app->get(Config::PLUGIN_ID),
                Shop::Smarty(),
                $this->app->get(Client::class),
                $this->app->get(Config::class),
                $this->app->get(Translation::class)
            );

            $controller->setModel($this->app->get(Model::class));
            $controller->setMapper($this->app->get(Factory::class));

            return $controller;
        });

        // Admin Controller
        $this->app->setFactory(AdminController::class, function () : AdminController {
            return new AdminController(
                $this->app->get(Session::class),
                $this->app->get(Config::PLUGIN_ID),
                Shop::Smarty(),
                $this->app->get(Client::class),
                $this->app->get(Config::class),
                $this->app->get(Translation::class)
            );
        });
    
        $this->app->setFactory(AjaxOrderManagementController::class, function () : AjaxOrderManagementController {
            return new AjaxOrderManagementController(
                $this->app->get(Session::class),
                $this->app->get(Config::PLUGIN_ID),
                Shop::Smarty(),
                $this->app->get(Client::class),
                $this->app->get(Config::class),
                $this->app->get(Translation::class)
            );
        });

        $this->app->setFactory(OnSiteMessagingController::class, function () : OnSiteMessagingController {
            return new OnSiteMessagingController(
                $this->app->get(Session::class),
                $this->app->get(Config::PLUGIN_ID),
                Shop::Smarty(),
                $this->app->get(Client::class),
                $this->app->get(Config::class),
                $this->app->get(Translation::class)
            );
        });

        $this->app->setFactory(MessagingModel::class, function () : MessagingModel {
            return new MessagingModel($this->app->getDB(), $this->app->get(Session::class));
        });

        $this->app->setFactory(OnSiteMessaging::class, function () : OnSiteMessaging {
            return new OnSiteMessaging(
                $this->app->get(MessagingModel::class),
                Shop::getPageType(),
                $this->app->get(Config::class)
            );
        });

        $this->app->setFactory(DynamicPaymentNames::class, function () : DynamicPaymentNames {
            return new DynamicPaymentNames(
                $this->app->get(Config::PLUGIN_ID),
                $this->app->get(Model::class),
                $this->app->get(Session::class),
                Shop::Smarty()
            );
        });
    }

    /**
     * This method is called after all other service providers have been registered,
     * meaning you have access to all other services that have been registered.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->app->setFactory(Model::class, function () : Model {
            return new Model($this->app->getDB(), $this->app->get(Config::class));
        });

        $this->app->setFactory(OrderEndpoint::class, function () : OrderEndpoint {
            return new OrderEndpoint(
                $this->app->get(Mailer::class),
                new Mail(),
                $this->app->get(AlertHelper::class),
                $this->app->get(Translation::class),
                Helper::getPluginById(Config::PLUGIN_ID)->getId()
            );
        });

        $this->app->setFactory(SessionsEndpoint::class, function () : SessionsEndpoint {
            return new SessionsEndpoint(
                $this->app->get(Mailer::class),
                new Mail(),
                $this->app->get(AlertHelper::class),
                $this->app->get(Translation::class),
                Helper::getPluginById(Config::PLUGIN_ID)->getId()
            );
        });
    }
}
