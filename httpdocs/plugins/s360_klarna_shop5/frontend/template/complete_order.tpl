<input type="hidden" class="klarna-auth" name="_klarna_auth" value="{$s360_klarna_auth}" /> 

<script>
window.klarnaFinalize = true;
window.onKlarnaFinalize = function(klarna) {
    klarna.setFormSelector('{$s360_klarna_form_selector}');
    klarna.setPaymentMethods({json_encode($s360_klarna_payment_methods)});
    klarna.setSelectedPayment('{$s360_klarna_selected_payment}');
    {if isset($s360_klarna_reauthorize_needed) && $s360_klarna_reauthorize_needed}
        klarna.setReauthorizeNeeded(true);
    {/if}
};
</script>