<input type="hidden" class="klarna-auth" name="_klarna_auth" /> 
<div
    id="s360_klarna_container--{$s360_klarna_context}"
    class="s360-klarna-payment"
    style="display:none"
    data-payment-id="{$s360_klarna_payment->kZahlungsart}"
></div>

<script>
window.klarnaPayments = window.klarnaPayments || [];
window.klarnaPayments.push({
    'id': '{$s360_klarna_payment->kZahlungsart}', 'container': '#s360_klarna_container--{$s360_klarna_context}', 'instance': '{$s360_klarna_context}'
});
</script>