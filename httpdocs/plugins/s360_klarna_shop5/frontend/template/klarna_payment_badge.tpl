<div class="klarna-title">
    <span class="klarna_badge klarna_badge--{$s360_klarna_context}">
        <img src="{if isset($s360_klarna_badges[$s360_klarna_context])}{$s360_klarna_badges[$s360_klarna_context]['descriptive']}{/if}" />
    </span>

    {$s360_klarna_title}
</div>