(function($, window, document, undefined) {
    var $body = $(window.klarnaCheckoutBodySelector || 'body[data-page=11]');
    var isFinalizing = false;

    /**
     * @param {Event} e Event
     * @returns boolean
     */
    function preventUnloadEventHandler(e) {
        var event = e || window.event;
        event.returnValue = null;
        return event.returnValue;
    }

    /**
     * Handle the fullscreen toggle events
     * @param {JQuery<HTMLElement>} $btn
     */
    var fullscreenToggleEvent = function($btn) {
        // Disable the submit button to prevent multiple finalization calls
        Klarna.Payments.on('fullscreenOverlayShown', function () {
            $btn.attr('disabled', 'disabled');
        });

        // Reanable the submit button
        Klarna.Payments.on('fullscreenOverlayHidden', function () {
            $btn.attr('disabled', null);
        });
    };

    /**
     * Called with the result of the load operation.
     * @param {Object} payment Payment object
     * @param {String} payment.container Container Selector
     * @param {String} payment.id Payment id
     * @param {String} payment.instance Payment instance id
     * @param {Object} res Response
     * @param {Boolean} res.show_form A boolean indicating whether to keep showing the form or to remove it.
     * @param {Object} res.error Only available in case of solvable errors.
     */
    var loadCallback = function (payment, res) {
        // console.debug('Callback Response', payment, res);
    };

    /**
     * Called with the result of the authorize operation.
     * @param {JQuery<HTMLElement>} $form
     * @param {JQuery<HTMLElement>} $checkbox
     * @param {Object} res Response
     * @param {String} res.authorization_token  If credit is approved, needed to place order.
     * @param {Boolean} res.show_form A boolean indicating whether to keep showing the form or to remove it.
     * @param {Boolean} res.approved A boolean indicating the result of the credit assessment.
     * @param {Boolean} res.finalize_required A boolean indicating that the finalize method should be called in order to complete the authorization.
     * @param {Object} res.error Only available in case of solvable errors.
     */
    var authorizeCallback = function ($form, $checkbox, res) {
        var $btn = $form.find('[type="submit"]');

        if (res.approved) {
            $btn.attr('disabled', 'disabled');
            $('.klarna-auth').val(res.finalize_required ? 'FINALIZED_REQUIRED' : res.authorization_token);
            $form.off('submit').trigger('submit');
            return;
        }

        $btn.find('.klarna-loading').remove();

        if (!res.show_form && !res.approved) {
            $checkbox.attr('checked', null).attr('disabled', 'disabled');
            $checkbox.parent().fadeOut();
            // console.debug('Checkbox', $checkbox);
        }
    };

    /**
     * Called with the result of the reauthorize operation.
     * @this {Finalize}
     * @param {JQuery<HTMLElement>} $form
     * @param {Object} res Response
     * @param {String} res.authorization_token If credit is approved, needed to place order.
     * @param {Boolean} res.show_form A boolean indicating whether to keep showing the form or to remove it.
     * @param {Boolean} res.approved A boolean indicating the result of the credit assessment.
     * @param {Object} res.error Only available in case of solvable errors.
     */
    var reauthorizeCallback = function ($form, res) {
        // console.debug('Reauthorize Callback Response', res);
        if (res.approved) {
            $('.klarna-auth').val(res.authorization_token);
            this.finalizeKlarnaOrder($form);
        }
    };

    /**
     * Called with the result of the finalize operation.
     * @param {JQuery<HTMLElement>} $form
     * @param {Object} res Response
     * @param {String} res.authorization_token If credit is approved, needed to place order.
     * @param {Boolean} res.show_form A boolean indicating whether to keep showing the form or to remove it.
     * @param {Boolean} res.approved A boolean indicating the result of the credit assessment.
     * @param {Object} res.error Only available in case of solvable errors.
     */
    var finalizeCallback = function ($form, res) {
        var $btn = $form.find('[type="submit"]');
        // console.debug('Finalize Callback Response', res);
        window.removeEventListener('beforeunload', preventUnloadEventHandler);

        if (res.approved) {
            $btn.attr('disabled', 'disabled');
            $('.klarna-auth').val(res.authorization_token);
            $form.off('submit').trigger('submit');
            return;
        }

        $btn.find('.klarna-loading').remove();
        isFinalizing = false;
    };

    /**
     * Handle Klarna Finalization.
     * @param {Object} session Klarna Session data
     */
    var Finalize = function (session) {
        this.formSelector = '';
        this.paymentMethods = [];
        this.selectedPayment = '';
        this.session = session || {};
        this.reauthorizeNeeded = false;

        window.klarnaAsyncCallback = this.finalizeCallback.bind(this);
    };

    /**
     * Set the form selector.
     * @param {String} selector form selector
     */
    Finalize.prototype.setFormSelector = function (selector) {
        this.formSelector = selector;
    };

    /**
     * Set payment methods.
     * @param {Array} methods
     */
    Finalize.prototype.setPaymentMethods = function (methods) {
        this.paymentMethods = methods;
    };

    /**
     * Set the currently selected payment.
     * @param {string} selectedPayment
     */
    Finalize.prototype.setSelectedPayment = function (selectedPayment) {
        this.selectedPayment = selectedPayment;
    };

    /**
     * Set the flag to determine whether a reauth call is needed or not.
     * @param {Boolean} reauthorizeNeeded
     */
    Finalize.prototype.setReauthorizeNeeded = function (reauthorizeNeeded) {
        this.reauthorizeNeeded = reauthorizeNeeded;
    };

    /**
     * Finalize the order on klarna's side.
     * @param {JQuery<HTMLElement>} $form
     */
    Finalize.prototype.finalizeKlarnaOrder = function ($form) {
        var options = {
            payment_method_categories: this.paymentMethods,
            payment_method_category: this.selectedPayment,
            instance_id: 'klarna-payments-instance--' + this.selectedPayment
        };

        Klarna.Payments.finalize(options, {}, finalizeCallback.bind(this, $form));
    };

    /**
     * Reauthorize the order on klarna's side.
     * @param {JQuery<HTMLElement>} $form
     */
    Finalize.prototype.reauthorizeKlarnaOrder = function($form) {
        Klarna.Payments.reauthorize({ payment_method_category: this.selectedPayment }, {}, reauthorizeCallback.bind(this, $form));
    };

    /**
     * Klarna Callback for finalization.
     */
    Finalize.prototype.finalizeCallback = function () {
        var self = this;

        // Call onKlarnaFinalize Callback if it exists
        if (window.onKlarnaFinalize && window.onKlarnaFinalize instanceof Function) {
            window.onKlarnaFinalize(this);
        }

        // Step 1: Initialize SDK
        try {
            Klarna.Payments.init({client_token: this.session.client_token});
        } catch (e) {
            console.error(e);
        }

        // Step 2: finalize the order
        $body.find(this.formSelector).on('submit', function () {
            var $this = $(this);
            var $btn = $this.find('[type=submit]');

            if (isFinalizing) {
                // console.log('Already finalizing. Please wait...');
                return false;
            }

            // Prevent User from accidentally closing tab
            window.addEventListener('beforeunload', preventUnloadEventHandler);

            // Handle Events
            $btn.prepend('<i class="klarna-loading fa fa-spinner fa-spin" aria-hidden="true"></i>');
            fullscreenToggleEvent($btn);
            isFinalizing = true;

            try {
                // Reauthorize the order if needed
                if (self.reauthorizeNeeded) {
                    self.reauthorizeKlarnaOrder($this);
                    return false;
                }

                // Finalize the Order
                self.finalizeKlarnaOrder($this);
            } catch (e) {
                window.removeEventListener('beforeunload', preventUnloadEventHandler);
                console.error(e);
                return false;
            }

            return false;
        });
    };

    /**
    * Handle Klarna Payments Authorization
    * @param {Array} payments Available payment methods
    * @param {Object} session Klarna Session
    * @param {Object} orderDetails Shipping and Billing Address for Authorize Call
    * @param {Boolean} containerAsBlock Whether or not custom-control-inline should be displayed as a block elemen (needed for NOVA)
    */
    var Payments = function (payments, session, orderDetails, containerAsBlock) {
        this.payments = payments || [];
        this.session = session || {};
        this.orderDetails = orderDetails || {};
        this.bindEvents();

        // call when payment methods got reloaded via ajax after selecting a shipping method
        var self = this;
        $(document).on('evo:loaded.evo.content', function(e, data) {
            if (data !== null && typeof data.url === 'string' && data.url.includes('kVersandart')) {
                // Hide Original Radio Button
                var $combinedInputs = $('.klarna_combined_input');
                $combinedInputs.closest('.custom-control-inline').removeClass('custom-control-inline').removeClass('custom-radio').removeClass('custom-control').find('input[name="Zahlungsart"]').hide(); // NOVA
                $combinedInputs.closest('label').removeClass('custom-control-label').find('input[name="Zahlungsart"]').hide(); // EVO / NOVA

                self.authorizeCallback();
            }
        });

        if (typeof containerAsBlock === 'undefined' || !!containerAsBlock) {
            $('.s360-klarna-payment').closest('.custom-control-inline').css('display', 'block');
        }
    };

    /**
     * Bind Events
     */
    Payments.prototype.bindEvents = function() {
        var self = this;
        var $combinedInputs = $('.klarna_combined_input');

        if ($body.length) {
            window.klarnaAsyncCallback = this.authorizeCallback.bind(this);

            $(function () {
                // Container is preselected -> fadein
                self.fadeContainer($body.find('[name=Zahlungsart]:checked'));

                // Fade Container on change
                $body.on('change', '[name=Zahlungsart]', function () {
                    if ($(this).parent().find('.klarna_combined_payment').length == 0) {
                        // Uncheck combined input
                        $body.find('.s360-klarna-payment-combined').fadeOut();
                        $combinedInputs.prop('checked', false);

                        self.fadeContainer($(this));
                    }
                });

                // Hide Original Radio Button
                $combinedInputs.closest('.custom-control-inline').removeClass('custom-control-inline').removeClass('custom-radio').removeClass('custom-control').find('input[name="Zahlungsart"]').hide(); // NOVA
                $combinedInputs.closest('label').removeClass('custom-control-label').find('input[name="Zahlungsart"]').hide(); // EVO / NOVA

                // Fade Combined Container
                $body.on('click', '.klarna-title', function () {
                    var $combinedInputs = $('.klarna_combined_input');

                    // Hide inactive containers and Show Active Container
                    $body.find('.s360-klarna-payment').fadeOut();
                    $('.klarna_combined_payment').removeClass('active');
                    $(this).closest('.klarna_combined_payment').addClass('active').find('.s360-klarna-payment-combined').fadeIn();

                    // Mark Active Checkbox as Checked
                    $combinedInputs.closest('label').find('input[name="Zahlungsart"]').prop('checked', true); // EVO / NOVA
                    $combinedInputs.prop('checked', false);
                    $(this).find('.klarna_combined_input').prop('checked', true);
                });
            });
        }
    };

    /**
     * Check if payment is a klarna payment registered with this class.
     * @param {String} payment Internal Payment Id
     */
    Payments.prototype.isKlarnaPayment = function (payment) {
        var paymentIds = this.payments.map(function (value) {
            return value.id;
        });

        return paymentIds.indexOf(payment) !== -1;
    };

    /**
     * Get the payment data by the internal payment id.
     * @param {String} id Internal Payment Id
     */
    Payments.prototype.getPaymentbyId = function (id) {
        for (var i = 0; i < this.payments.length; i++) {
            if (this.payments[i].id == id) {
                return this.payments[i];
            }
        }

        return null;
    };

    /**
     * Get the payment data by the container selector.
     * @param {String} selector
     */
    Payments.prototype.getPaymentbyContainer = function (selector) {
        for (var i = 0; i < this.payments.length; i++) {
            if (this.payments[i].container == selector) {
                return this.payments[i];
            }
        }

        return null;
    };

    /**
     * Only show currently selected klarna payment.
     * @param {JQuery<HTMLElement>} $el Currently selected checkbox
     */
    Payments.prototype.fadeContainer = function ($el) {
        if ($el.length < 1) {
            return;
        }

        var payment = $el.val().toString();
        var $container = $('.s360-klarna-payment[data-payment-id="' + payment + '"]');

        $('.s360-klarna-payment').fadeOut();

        if (!this.isKlarnaPayment(payment)) {
            return;
        }

        $container.fadeIn();
    };

    /**
     * Klarna Authorization Callback
     */
    Payments.prototype.authorizeCallback = function () {
        var self = this;

        try {
            // Step 1: Initialize SDK
            Klarna.Payments.init({client_token: this.session.client_token});

            // Step 2: Load the Klarna Widget for each payment method category
            var instances = [];
            var payments = this.payments.filter(function(value) {
                if (instances.includes(value.instance)) {
                    return false;
                }

                instances.push(value.instance);
                return true;
            });

            payments.forEach(function (payment) {
                var options = {
                    container: payment.container,
                    instance_id: 'klarna-payments-instance--' + payment.instance,
                    payment_method_categories: [payment.instance],
                    payment_method_category: payment.instance
                };

                Klarna.Payments.load(options, loadCallback.bind(this, payment));
            });

            // Step 3: authorize but do not finalize:
            $body.find('form').has('[name="Zahlungsart"]').on('submit', function () {
                var $this = $(this);
                var $checkbox = $this.find('[name=Zahlungsart]:checked');
                var $btn = $this.find('[type=submit]');
                var payment = $checkbox.val();
                var paymentdata = self.getPaymentbyId(payment.toString());
                var $combined = $checkbox.parent().find('.klarna_combined_payment.active');

                // Abort if not a klarna payment method
                if (!self.isKlarnaPayment(payment.toString())) {
                    // $this.off('submit').trigger('submit');
                    return true;
                }

                // Get correct paymentdata for combined payment options
                if ($combined.length) {
                    paymentdata = self.getPaymentbyContainer('#' + $combined.find('.s360-klarna-payment-combined').attr('id'));
                    $this
                        .remove('input.selected_combined_payment')
                        .append('<input type="hidden" class="selected_combined_payment" name="selected_combined_payment" value="' + paymentdata.instance + '" />');
                }

                // Handle Events
                $btn.remove('.klarna-loading').prepend('<i class="klarna-loading fa fa-spinner fa-spin" aria-hidden="true"></i>');
                fullscreenToggleEvent($btn);

                // Authorize the Payment
                var options = {
                    instance_id: 'klarna-payments-instance--' + paymentdata.instance,
                    payment_method_category: paymentdata.instance,
                    auto_finalize: false
                };

                Klarna.Payments.authorize(options, self.orderDetails, authorizeCallback.bind(this, $this, $checkbox));

                return false;
            });

        } catch (e) {
            console.error(e);
        }
    };

    // Init Auth or Finalize class depending on the needs.
    if (window.klarnaFinalize) {
        new Finalize(window.klarnaSession);
    } else {
        new Payments(window.klarnaPayments, window.klarnaSession, window.klarnaOrderDetails, window.klarnaContainerBlock);
    }
})(window.jQuery, window, window.document);