(function ($, window, document, undefined) {
    // Klarna OSM Triggers
    var $placements = $('klarna-placement');

    if ($placements.length > 0) {
        var price = Math.floor(parseFloat($('.product-offer .price > span').text().replace(',', '.')) * 100);
        window.KlarnaOnsiteService = window.KlarnaOnsiteService || [];

        $('#quantity.quantity').on('change keyup', function () {
            $placements.attr('data-purchase_amount', this.value * price); // data('purchase_amount', x) does not work here
            window.KlarnaOnsiteService.push({ eventName: 'refresh-placements' });
        });
    }
})(window.jQuery, window, window.document);