<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\paymentmethod;

use JTL\Checkout\Bestellung;
use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Payments\KlarnaPayment;

/**
 * Klarna SliceIt Payment Method Handler Class.
 * @package Plugin\s360_klarna_shop5\paymentmethod
 */
class SliceIt extends KlarnaPayment
{
    /**
     * @param string $moduleID
     * @param integer $nAgainCheckout
     */
    public function __construct($moduleID, $nAgainCheckout = 0)
    {
        parent::__construct($moduleID, $nAgainCheckout);

        $this->paymentId = Config::SLICE_IT_ID;
    }

    /**
     * Do not send a payment mail as this is not true for this payment method.
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param Bestellung $order
     * @return SliceIt
     */
    public function sendConfirmationMail(Bestellung $order) : self
    {
        return $this;
    }
}
