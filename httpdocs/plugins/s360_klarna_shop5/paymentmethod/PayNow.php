<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\paymentmethod;

use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Payments\KlarnaPayment;

/**
 * Klarna PayNow Payment Method Handler Class.
 * @package Plugin\s360_klarna_shop5\paymentmethod
 */
class PayNow extends KlarnaPayment
{
    /**
     * @var array
     */
    protected $combinedPayments = [];

    /**
     * @param string $moduleID
     * @param integer $nAgainCheckout
     */
    public function __construct($moduleID, $nAgainCheckout = 0)
    {
        parent::__construct($moduleID, $nAgainCheckout);

        $this->paymentId = Config::PAY_NOW_ID;
        $this->combinedPayments = [
            Config::PAY_NOW_ID,
            Config::PAY_NOW_DIRECT_BANK_TRANSFER_ID,
            Config::PAY_NOW_DIRECT_DEBIT_ID,
            Config::PAY_NOW_CARD_ID
        ];
    }

    /**
     * Check if customer should be able to select a pay now payment method.
     *
     * @return boolean
     */
    public function isSelectable() : bool
    {
        $this->debugLog('Called isSelectable', $this->paymentId);

        // Klarna Pre-Assessment
        if (!$this->session->get('session')) {
            $this->debugLog('Create a new Klarna session to get the pre-assessment result', $this->paymentId);

            $session = $this->mapper->mapForSession($this->session);
            $this->session->set('session', $this->client->getSessionsEndpoint()->create($session)->getArrayCopy());
        }

        // If one of the combined payment children can be selected, parent container pay now should be selectable
        foreach ($this->combinedPayments as $paymentId) {
            if ($this->validator->preAssessment($paymentId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * This method is called after the paymethod selection screen, before the confirmation screen.
     *
     * @param array $args
     * @return bool
     */
    public function handleAdditional($args = []) : bool
    {
        $payment = $args['selected_combined_payment'] ?? $this->paymentId;

        $paymentCategories = array_column(
            $this->session->get('session')['payment_method_categories'] ?? [],
            'identifier'
        );

        if (!in_array($payment, $paymentCategories)) {
            foreach ($this->combinedPayments as $paymentId) {
                if (in_array($paymentId, $paymentCategories)) {
                    $payment = $paymentId;
                }
            }
        }

        // Temporarily use selected pament id and call parent method with it
        $pid = $this->paymentId;
        $this->paymentId = $payment;
        $check = parent::handleAdditional($args);
        $this->paymentId = $pid;

        return $check;
    }
}
