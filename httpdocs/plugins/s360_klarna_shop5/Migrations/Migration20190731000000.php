<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\Migrations;

use JTL\DB\ReturnType;
use JTL\Plugin\Migration;
use JTL\Update\IMigration;
use Plugin\s360_klarna_shop5\src\Utils\Config;

/**
 * Migration for the Klarna Shop Order.
 */
class Migration20190731000000 extends Migration implements IMigration
{
    /**
     * Create the klarna - jtl order mapping table
     *
     * @return void
     */
    public function up() : void
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `xplugin_' . Config::PLUGIN_ID . '_order` (
            `jtl_order_id` INT(10) NOT NULL,    /* jtl order table id (kBestellung) */
            `jtl_order_number` VARCHAR(255),    /* jtl order number (cBestellNr) */
            `klarna_order_id` VARCHAR(255),     /* order id from klarna */
            `klarna_status` VARCHAR(255),       /* klarna status of the order (AUTHORIZED, PART_CAPTURED, ...) */
            `klarna_fraud_status` VARCHAR(255), /* klarna fraud status of the order (ACCEPTED, PENDING or REJECTED) */
            `payment_method` VARCHAR(255),      /* klarna payment method */
            `payment_method_type` VARCHAR(255), /* klarna payment method type (ie invoice, card, etc)*/
            `server` VARCHAR(255),              /* server this order belongs to, i.e. test or live */
            `activation` INT(10),                /* timestamp of the activation for the order */
            PRIMARY KEY (`jtl_order_id`, `klarna_order_id`)
        ); ENGINE=InnoDB COLLATE utf8_unicode_ci');
    }

     /**
     * Delete the klarna - jtl order mapping table
     *
     * @return void
     */
    public function down() : void
    {
        if ($this->doDeleteData()) {
            $this->getDB()->executeQuery(
                'DROP TABLE IF EXISTS `xplugin_' . Config::PLUGIN_ID . '_order`',
                ReturnType::DEFAULT
            );
        }
    }
}
