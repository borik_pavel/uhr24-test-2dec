<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\Migrations;

use JTL\DB\ReturnType;
use JTL\Plugin\Migration;
use JTL\Update\IMigration;
use Plugin\s360_klarna_shop5\src\Utils\Config;

/**
 * Migration for the Klarna Shop Deliveries.
 */
class Migration20190816000000 extends Migration implements IMigration
{
    /**
     * Create the delivery status table
     *
     * @return void
     */
    public function up() : void
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `xplugin_' . Config::PLUGIN_ID . '_deliveries` (
            `id` INT(10) NOT NULL,
            `number` VARCHAR(75) NOT NULL,
            `processed` TINYINT(1) NOT NULL DEFAULT 0,
            PRIMARY KEY (`id`, `number`)
        ); ENGINE=InnoDB COLLATE utf8_unicode_ci');
    }

    /**
    * Delete the delivery status table
    *
    * @return void
    */
    public function down() : void
    {
        if ($this->doDeleteData()) {
            $this->getDB()->executeQuery(
                'DROP TABLE IF EXISTS `xplugin_' . Config::PLUGIN_ID . '_deliveries`',
                ReturnType::DEFAULT
            );
        }
    }
}
