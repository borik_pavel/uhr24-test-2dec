<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Controllers;

use Exception;
use JTL\Checkout\Bestellung;
use JTL\Mail\Mailer;
use JTL\Mail\Mail\Mail;
use Plugin\s360_klarna_shop5\src\Mappers\Factory;
use Plugin\s360_klarna_shop5\src\Mappers\OrderMapper;
use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Utils\JtlLoggerTrait;
use Plugin\s360_klarna_shop5\src\Payments\Model;
use Plugin\s360_klarna_shop5\src\Utils\Translation;

/**
 * Sync Controller handles the Sync with JTL WaWi.
 * @package Plugin\s360_klarna_shop5\src\Controllers
 */
class SyncController extends Controller
{
    use JtlLoggerTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Factory
     */
    protected $mapper;

    /**
     * Set the model dependency.
     *
     * @param Model $model
     * @return void
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Set the mapper dependency
     *
     * @param Factory $mapper
     * @return void
     */
    public function setMapper(Factory $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Handle cancelled order sent from JTL WaWi.
     *
     * @param Bestellung $order
     * @return void
     */
    public function handleOrderCancellation(Bestellung $order) : void
    {
        // Ignore any order that is not paid with Klarna
        if (!isset($order->kBestellung) || ($klarnaOrder = $this->model->getOrderById($order->kBestellung)) === null) {
            return;
        }

        $this->debugLog('Handle Order Cancellation of order: ' . print_r($order, true), 'WaWi-Sync');
        $this->client->getOrderEndpoint()->cancel($klarnaOrder->klarna_order_id);
        $this->model->cancelOrder($klarnaOrder->klarna_order_id);

        $this->debugLog('Order was canceled.', 'WaWi-Sync');
    }

    /**
     * Handle the WaWi Sync orders to capture amount.
     *
     * @param Bestellung|stdClass $order
     * @param Mailer $mailer
     * @param Mail $mail
     * @return void
     */
    public function handleOrderUpdate($order, Mailer $mailer, Mail $mail) : void
    {
        $this->debugLog('WaWi-Sync Order Content: ' . print_r($order, true));

        // Ignore any order that is not paid with Klarna
        if (!isset($order->kBestellung) || ($klarnaOrder = $this->model->getOrderById($order->kBestellung)) === null) {
            return;
        }

        // reload order from the database, this is the order after the sync.
        $newOrder = new Bestellung($order->kBestellung, true);
        // $order->cStatus = (string) BESTELLUNG_STATUS_IN_BEARBEITUNG; // !just for testing purposes!
        $this->debugLog('Start WaWi-Sync for order: ' . $order->cBestellNr, 'WaWi-Sync');
        // $this->debugLog('Order New Content: ' . print_r($newOrder, true), 'WaWi-Sync');

        try {
            $this->client->enableExceptionPassthrough();

            // if the Order was not shipped before, i.e. order switched to teilversandt or versandt for the first time.
            if (!$this->orderIsDelivered($order) && !$this->orderIsPartiallyDelivered($order)) {
                $this->newlyShippedOrder($order->cBestellNr, $newOrder, $klarnaOrder->klarna_order_id);
            } elseif ($this->orderIsPartiallyDelivered($order) &&
                ($this->orderIsDelivered($newOrder) || $this->orderIsPartiallyDelivered($newOrder))
            ) {
                // If the order was shipped before and is now partially or fully delivered
                $this->subsequentlyShippedOrder($order->cBestellNr, $newOrder, $klarnaOrder->klarna_order_id);
            }

            // Sync Capture with WaWi Payments
            $this->syncPayments($order, $newOrder, $klarnaOrder->klarna_order_id, $mailer, $mail);

            $this->client->disableExceptionPassthrough();
        } catch (Exception $e) {
            $this->errorLog($e->getMessage(), 'WaWi-Sync');
        }
    }

    /**
     * Sync Payments with wawi
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @param Bestellung|stdClass $oldOrder
     * @param Bestellung $order
     * @param string $klarnaOrderId
     * @param Mailer $mailer
     * @param Mail $mail
     * @return void
     */
    private function syncPayments(
        $oldOrder,
        Bestellung $order,
        string $klarnaOrderId,
        Mailer $mailer,
        Mail $mail
    ) : void {
        // Plugin was reinstalled and therefore jtl cannot find the right payment method
        if (is_null($order->Zahlungsart) || empty($order->Zahlungsart->cModulId)) {
            $this->noticeLog(
                'Plugin was reinstalled and therefore order ' . $order->cBestellNr . ' hasn\'t the right ' .
                'Zahlungsart anymore. Status must be changed by hand in the WaWi.'
            );

            $this->handleEscalation(
                $klarnaOrderId,
                $this->translation->get(Translation::KEY_UNKOWN_PAYMENT_METHOD),
                $this->translation->get(Translation::KEY_ERROR_REINSTALLED_PLUGIN),
                $mailer,
                $mail
            );
            return;
        }

        /** @var \Plugin\s360_klarna_shop5\src\Payments\KlarnaPayment $paymentMethod */
        $paymentMethod = \JTL\Plugin\Payment\Method::create($order->Zahlungsart->cModulId);

        if (empty($paymentMethod)) {
            $this->errorLog(
                'Could not create a Payment Method instance for: ' . $order->Zahlungsart->cModulId . '. ' .
                'Status must be changed by hand in the WaWi.'
            );
            $this->handleEscalation(
                $klarnaOrderId,
                $this->translation->get(Translation::KEY_UNKOWN_PAYMENT_METHOD),
                'Could not create a Payment Method instance for: ' . $order->Zahlungsart->cModulId . '. ' .
                'Status must be changed by hand in the WaWi.',
                $mailer,
                $mail
            );
            return;
        }

        $paymentMethod->createPayment($order, $klarnaOrderId, $oldOrder);
    }

    /**
     * Handle a new partial-/full delivery.
     *
     * @param Bestellung|stdClass $order
     * @param Bestellung $newOrder
     * @param string $klarnaOrderId
     * @return void
     */
    private function newlyShippedOrder(string $orderNumber, Bestellung $newOrder, string $klarnaOrderId) : void
    {
        $klarnaOrder = $this->client->getOrderEndpoint()->fetch($klarnaOrderId);
        $idempotency = [$klarnaOrder['remaining_authorized_amount'], count($klarnaOrder['captures'])];

        if ($klarnaOrder['status'] === Model::CAPTURED_STATUS) {
            $this->noticeLog('Order "' . $orderNumber . '" is already captured. Do not capture again!', 'WaWi-Sync');
            return;
        }

        $this->debugLog('Order "' . $orderNumber . '" was partially-/fully delivered for the first time', 'WaWi-Sync');

        if ($this->orderIsPartiallyDelivered($newOrder)) {
            // Handle a partially delivered order
            $this->debugLog(
                'Order "' . $orderNumber . '" was partially delivered. Capture only shipped items',
                'WaWi-Sync'
            );

            // check if shipping costs, discounts/vouchers should be part of the initial capture of an order
            $includeExtra = false;
            if ($this->config->get(Config::KEY_CAPTURE_TIME) === Config::VALUE_CAPTURE_TIME_START) {
                $this->debugLog('Include shipping costs and discounts/vouchers in this capture.', 'WaWi-Sync');
                $includeExtra = true;
            }

            $this->client->getOrderEndpoint()->authorize(
                $klarnaOrderId,
                $this->mapper->mapForCapture($newOrder, OrderMapper::PART_CAPTURE, $includeExtra),
                $idempotency
            );

            $this->model->updateOrderDetails(['status' => Model::PARTLY_CAPTURED_STATUS], $klarnaOrderId);
        } elseif ($this->orderIsDelivered($newOrder)) {
            // Capture the complete order if the order is sent fully
            $this->debugLog(
                'Order "' . $orderNumber . '" was fully delivered. Capture the complete order amount',
                'WaWi-Sync'
            );

            $this->client->getOrderEndpoint()->authorize(
                $klarnaOrderId,
                $this->mapper->mapForCapture($newOrder, OrderMapper::FULL_CAPTURE, true),
                $idempotency
            );

            $this->model->updateOrderDetails(['status' => Model::CAPTURED_STATUS], $klarnaOrderId);
        }
    }

    /**
     * Handle a subsequently shipped order, i.e. an order that was partially delivered before.
     *
     * @param Bestellung $order
     * @param string $klarnaOrderId
     * @return void
     */
    private function subsequentlyShippedOrder(string $orderNumber, Bestellung $order, string $klarnaOrderId) : void
    {
        $klarnaOrder = $this->client->getOrderEndpoint()->fetch($klarnaOrderId);
        $idempotency = [$klarnaOrder['remaining_authorized_amount'], count($klarnaOrder['captures'])];

        if ($klarnaOrder['status'] === Model::CAPTURED_STATUS) {
            $this->noticeLog('Order "' . $orderNumber . '" is already captured. Do not capture again!', 'WaWi-Sync');
            return;
        }

        $this->debugLog(
            'Order "' . $orderNumber . '" was partially delivered before and is now partially-/fully'
                . ' delivered. Capture only amount of shipped items',
            'WaWi-Sync'
        );

        // check if shipping costs, discounts/vouchers should be part of the final capture of an order
        $includeExtra = false;
        if ($this->orderIsDelivered($order)
            && $this->config->get(Config::KEY_CAPTURE_TIME) === Config::VALUE_CAPTURE_TIME_END
        ) {
            $this->debugLog('Include shipping costs and discount codes/vouchers in this capture.', 'WaWi-Sync');
            $includeExtra = true;
        }

        $capture = $this->mapper->mapForCapture($order, OrderMapper::PART_CAPTURE, $includeExtra);
        $this->client->getOrderEndpoint()->authorize($klarnaOrderId, $capture, $idempotency);
        $this->model->updateOrderDetails(['status' => Model::PARTLY_CAPTURED_STATUS], $klarnaOrderId);

        if ($this->orderIsDelivered($order)) {
            $this->model->updateOrderDetails(['status' => Model::CAPTURED_STATUS], $klarnaOrderId);
        }
    }

    /**
     * Checks if order is fully delivered.
     *
     * @param Bestellung|stdClass $order
     * @return boolean
     */
    private function orderIsDelivered($order) : bool
    {
        return (int) $order->cStatus === (int) BESTELLUNG_STATUS_VERSANDT;
    }

    /**
     * Checks if order is partially delivered.
     *
     * @param Bestellung|stdClass $order
     * @return boolean
     */
    private function orderIsPartiallyDelivered($order) : bool
    {
        return (int) $order->cStatus === (int) BESTELLUNG_STATUS_TEILVERSANDT;
    }

    /**
     * Handle escalation email
     *
     * @param string $klarnaOrderId
     * @param Mailer $mailer
     * @param Mail $mail
     * @return void
     */
    private function handleEscalation(
        string $klarnaOrderId,
        string $error,
        string $message,
        Mailer $mailer,
        Mail $mail
    ): void {
        try {
            $data = new \stdClass();
            $data->klarna_error = $error;
            $data->klarna_message = $message;
            $data->klarna_order_id = $klarnaOrderId;

            $mailTemplate = $mail->createFromTemplateID(
                'kPlugin_' . $this->config->getPluginId() . '_klarnaescalation',
                $data
            );
            $mailTemplate->setToMail($this->client->getEscalationMail());
            $mailer->send($mailTemplate);
        } catch (Exception $ex) {
            $this->errorLog(
                'Failed to handle email escalation for order id ' . $klarnaOrderId .
                '. Exception: ' . print_r($ex, true),
                get_called_class()
            );
        }
    }
}
