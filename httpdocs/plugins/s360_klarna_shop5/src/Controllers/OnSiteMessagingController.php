<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Controllers;

use Exception;
use InvalidArgumentException;
use JTL\Catalog\Currency;
use JTL\Helpers\Form;
use JTL\Helpers\Text;
use JTL\Language\LanguageHelper;
use JTL\Session\Frontend;
use Plugin\s360_klarna_shop5\src\Messaging\Model;
use Plugin\s360_klarna_shop5\src\Messaging\Placement;
use Plugin\s360_klarna_shop5\src\Utils\Config;

/**
 * Handle the On-Site Messaging Settings and Configuration.
 *
 * @package Plugin\s360_klarna_shop5\src\Controllers
 */
class OnSiteMessagingController extends AjaxController
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Overview Action (non-ajax).
     *
     * @return void
     */
    public function overviewAction() : void
    {
        $path = $this->plugin->getPaths()->getAdminPath();
        print_r($this->view->fetch($path . 'template/' . Config::TEMPLATE_ONSITE_MESSAGING_SETTINGS));
    }

    /**
     * Route the ajax request to the correct method.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @param Model $messaging
     * @return void
     */
    public function handleAjax(Model $messaging) : void
    {
        $this->model = $messaging;
        $action = $this->request['action'] ?? null;
        switch ($action) {
            case 'loadEntries':
                $this->response = $this->handleLoadEntries();
                break;

            case 'getDetails':
                $this->response = $this->handleGetDetails();
                break;

            case 'delete':
                $this->response = $this->handleDelete();
                break;
            
            case 'create':
                $this->response = $this->handleCreate();
                break;
            
            case 'save':
                $this->response = $this->handleSave();
                break;

            default:
                throw new InvalidArgumentException('Unrecognized action ' . Text::filterXSS($action));
                break;
        }
    }

    /**
     * Load all placement entries.
     *
     * @return array
     */
    protected function handleLoadEntries() : array
    {
        $placements = $this->model->all();

        foreach ($placements as &$placement) {
            $this->view->assign('placement', $placement);
            $placement->view = $this->fetchSnippet(Config::TEMPLATE_PLACEMENT_ITEM);
        }

        return [
            'result' => self::RESULT_SUCCESS,
            'placements' => $placements
        ];
    }

    /**
     * Get placement details.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return array
     */
    protected function handleGetDetails() : array
    {
        // Return error if id does not exist
        $pid = $this->request['id'];
        if (empty($pid)) {
            return ['result' => self::RESULT_ERROR, 'message' => 'Missing parameter id'];
        }

        Currency::setCurrencies(true); // Make Sure Currencies are loaded!
        $placement = $this->model->get((int) $pid);
        $languages = array_filter(LanguageHelper::getAvailable(), [$this->model, 'isSupportedLang']);
        $currencies = array_filter(Frontend::getCurrencies(), [$this->model, 'isSupportedCurrency']);

        $this->view->assign('placement', $placement);
        $this->view->assign('languages', $languages);
        $this->view->assign('currencies', $currencies);
        $this->view->assign(
            'klarnaPortalUrl',
            sprintf('https://%s/', Config::PORTAL_URL)
        );
        $placement->view = $this->fetchSnippet(Config::TEMPLATE_PLACEMENT_DETAIL);

        return ['result' => self::RESULT_SUCCESS, 'placement' => $placement];
    }

    /**
     * Load create placement form.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return array
     */
    protected function handleCreate() : array
    {
        Currency::setCurrencies(true); // Make Sure Currencies are loaded!
        $placement = new Placement();
        $languages = array_filter(LanguageHelper::getAvailable(), [$this->model, 'isSupportedLang']);
        $currencies = array_filter(Frontend::getCurrencies(), [$this->model, 'isSupportedCurrency']);

        $this->view->assign('languages', $languages);
        $this->view->assign('currencies', $currencies);
        $this->view->assign('placement', $placement);
        $this->view->assign(
            'klarnaPortalUrl',
            sprintf('https://%s/', Config::PORTAL_URL)
        );
        $placement->view = $this->fetchSnippet(Config::TEMPLATE_PLACEMENT_DETAIL);

        return ['result' => self::RESULT_SUCCESS, 'placement' => $placement];
    }

    /**
     * Save Placement.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return array
     */
    protected function handleSave() : array
    {
        if (!Form::validateToken()) {
            return ['result' => self::RESULT_ERROR, 'message' => 'Invalid CSRF token'];
        }

        try {
            $this->model->save(
                new Placement([
                    'id'                 => $this->request['id'] ?? null,
                    'kSprache'           => $this->request['country'],
                    'kWaehrung'          => $this->request['currency'],
                    'active'             => $this->request['active'] ?? 0,
                    'placement_type'     => $this->request['placement_type'],
                    'placement_position' => $this->request['placement_position'] ?? [],
                    'placement_location' => $this->request['placement_location'],
                    'javascript_tag'     => $this->request['script'],
                    'placement_tag'      => $this->request['placement_tag'],
                ])
            );

            return [
                'result' => self::RESULT_SUCCESS,
                'message' => __('Das Placement wurde erfolgreich gespeichert.')
            ];
        } catch (Exception $exc) {
            return ['result' => self::RESULT_ERROR, 'message' => $exc->getMessage()];
        }
    }

    /**
     * Delete a placement.
     *
     * @return array
     */
    protected function handleDelete() : array
    {
        // Return error if id does not exist
        $pid = $this->request['id'];
        if (empty($pid)) {
            return ['result' => self::RESULT_ERROR, 'message' => 'Missing parameter id'];
        }

        try {
            $this->model->delete((int) $pid);

            return [
                'result' => self::RESULT_SUCCESS,
                'message' => __('Das Placement wurde erfolgreich gelöscht.')
            ];
        } catch (Exception $exc) {
            return ['result' => self::RESULT_ERROR, 'message' => $exc->getMessage()];
        }
    }

    /**
     * Fetch a snippet file.
     *
     * @param string $snippet
     * @return string
     */
    private function fetchSnippet(string $snippet) : string
    {
        $path = $this->plugin->getPaths()->getAdminPath();
        return $this->view->fetch($path . 'template/' . Config::TEMPLATE_SNIPPET_DIR . '/' . $snippet);
    }
}
