<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Controllers;

use JTL\Shop;
use Plugin\s360_klarna_shop5\src\Utils\Config;

/**
 * Controller which handles all admin related stuff
 *
 * @package Plugin\s360_klarna_shop5\src\Controllers
 */
class AdminController extends Controller
{
    /**
     * Assign some general options to the view.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return void
     */
    public function preDispatch() : void
    {
        $paths = $this->plugin->getPaths();
        $options = [
            'admin_url' => Shop::getURL(true) . '/admin/plugin.php?kPlugin=' . $this->plugin->getID(),
            'admin_template_url' => $paths->getAdminURL() . 'template/',
            'admin_snippet_path' => $paths->getAdminPath() . 'template/' . Config::TEMPLATE_SNIPPET_DIR . '/'
        ];

        $snippets = [
            'empty_search_result'           => __(
                '<b>Suche erfolglos:</b> Keine Bestellung für "%s" gefunden.'
            ),
            'authorize_order'               => __('Betrag manuell aktivieren?'),
            'authorize_order_notice'        => __(
                'Zu aktivierender Betrag <u>%s</u>.<br/>' .
                '<em>Der Betrag der einzelnen Bestellpositionen wird automatisch beim Versand aktiviert!</em>'
            ),
            'confirm_authorize_order'       => __('Ja, Bestellung aktivieren'),
            'extend_authorization'          => __('Reservierungszeit verlängern?'),
            'extend_authorization_notice'   => __(
                'Soll der Zeitraum bis die Bestellung mit der Klarna Bestell-ID <u>%s</u> abläuft verlängert werden?'
            ),
            'confirm_extend_authorization'  => __('Ja, Zeitraum verlängern'),
            'release_authorization'         => __('Genehmigten Betrag entfernen?'),
            'release_authorization_notice'  => __(
                'Soll der restliche genehmigte Betrag (:amount: :currency:) der Bestellung mit der Klarna Bestell-ID ' .
                '<u>:orderid:</u> entfernt werden?'
            ),
            'confirm_release_authorization' => __('Ja, genehmigten Betrag entfernen'),
            'update_merchant_refs'          => __('Bestellnummern ändern'),
            'merchant_ref'                  => __('Bestellnummer'),
            'confirm_update_merchant_refs'  => __('Ja, Bestellnummern ändern'),
            'shipping_data'                 => __('Versandinformationen hinzufügen?'),
            'confirm_shipping_data'         => __('Ja, Versandinformation hinzufügen'),
            'label_shipping_company'        => __('Versandunternehmen'),
            'label_shipping_method'         => __('Versandart'),
            'label_tracking_nr'             => __('Sendungsnummer'),
            'label_tracking_uri'            => __('Link zur Sendungsverfolgung'),
            'label_return_shipping_company' => __('Retoure Versandunternehmen'),
            'label_return_tracking_nr'      => __('Retoure Sendungsnummer'),
            'label_return_tracking_uri'     => __('Link zur Retoure Sendungsverfolgung'),
            'pickupstore'                   => __('Abholladen'),
            'home'                          => __('Zuhause'),
            'boxreg'                        => __('BoxReg'),
            'boxunreg'                      => __('BoxUnreg'),
            'pickuppoint'                   => __('Abholstation'),
            'own'                           => __('Eigen'),
            'label_description'             => __('Beschreibung'),
            'go_back'                       => __('Nein, zurück'),
        ];

        $this->view->assign('klarna_snippets', json_encode($snippets));
        $this->view->assign('klarnaMessages', $this->selfCheck());
        $this->view->assign('s360_klarna_options', $options);
    }

    /**
     * Check different settings and dependencies that are needed for the plugin.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return array
     */
    private function selfCheck() : array
    {
        $bag = ['errors' => [], 'warnings' => [], 'notes' => [], 'successes' => []];

        // Sandbox
        if ($this->client->isSandbox()) {
            array_push(
                $bag['notes'],
                __(
                    'Sie haben momentan den Sandbox-Modus aktiviert. ' .
                    'Es können <strong>keine echten</strong> Bestellungen/Zahlungen durchgeführt werden! ' .
                    'Ändern Sie den Modus im Tab "Einstellungen", sobald Sie bereit sind.'
                )
            );
        }

        // Shop Version
        if (!version_compare(Shop::getApplicationVersion(), '5.0.0', '>=')) {
            array_push(
                $bag['notes'],
                __(
                    'Sie benutzen eine eine zu niedrige Shop-Version. ' .
                    'Es kann zu Problemen mit dem Betrieb des Plugins kommen. ' .
                    'Bitte benutzen Sie mindestens Version 5.0.0.'
                )
            );
        }

        // Api Config
        $messages = [];
        $settings = [
            'merchant' => $this->config->get(Config::KEY_MERCHANT_ID),
            'secret' => $this->config->get(Config::KEY_SHARED_SECRET),
            'escalation' => $this->config->get(Config::KEY_ESCALATION_MAIL),
        ];

        if (empty($settings['merchant']) || $settings['merchant'] === '') {
            $messages[] = __(
                'Es ist noch keine <strong>Merchant-ID/Händlernummer</strong> konfiguriert.'
            );
        }

        if (empty($settings['secret']) || $settings['secret'] === '') {
            $messages[] = __(
                'Es ist noch kein <strong>Secret Key/Passwort</strong> konfiguriert.'
            );
        }
        if (empty($settings['escalation']) || $settings['escalation'] === '') {
            $messages[] = __(
                'Es ist noch keine <strong>Eskalations-Emailadresse</strong> konfiguriert.'
            );
        }

        if (!empty($messages)) {
            $messages[] = '<br/>' . __(
                'Sie können diese Einstellungen im Tab "Einstellungen" setzen.'
            );
            array_push($bag['warnings'], '<br/>' . implode('<br/>', $messages));
        }

        return $bag;
    }

    /**
     * Load the orders view
     *
     * @return void
     */
    public function handleOrders() : void
    {
        $path = $this->plugin->getPaths()->getAdminPath();
        print_r($this->view->fetch($path . 'template/' . Config::TEMPLATE_ORDERS_LIST));
    }
}
