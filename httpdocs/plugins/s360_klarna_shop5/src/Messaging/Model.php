<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Messaging;

use InvalidArgumentException;
use JTL\Catalog\Currency;
use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Utils\Session;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Language\LanguageModel;

/**
 * Handles access to the klanra messaging data in the DB.
 *
 * @package Plugin\s360_klarna_shop5\src\Messaging
 */
class Model
{
    public const MESSAGING_TABLE = 'xplugin_' . Config::PLUGIN_ID . '_messaging';

    /**
     * Supported Currencies and Languages for OSM
     */
    public const SUPPORTED_LANGUAGES = [
        'en', 'nl', 'de', 'sv', 'no', 'fi', 'da'
    ];

    public const SUPPORTED_CURRENCIES = [
        'GBP', 'EUR', 'SEK', 'NOK', 'DKK'
    ];

    /**
     * @var DBInterface
     */
    protected $database;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param DbInterface $database
     * @param Session $session
     */
    public function __construct(DbInterface $database, Session $session)
    {
        $this->database = $database;
        $this->session = $session;
    }

    /**
     * Checks if language is supported by osm
     *
     * @param LanguageModel $lang
     * @return boolean
     */
    public function isSupportedLang(LanguageModel $lang) : bool
    {
        return in_array($lang->getIso639(), self::SUPPORTED_LANGUAGES);
    }

    /**
     * Checks if currency is supported by osm.
     *
     * @param Currency $currency
     * @return boolean
     */
    public function isSupportedCurrency(Currency $currency) : bool
    {
        return in_array($currency->getCode(), self::SUPPORTED_CURRENCIES);
    }

    /**
     * Checks if lang/currency combination is supported by osm
     *
     * @param LanguageModel $lang
     * @param Currency $currency
     * @return boolean
     */
    public function isSupported(LanguageModel $lang, Currency $currency) : bool
    {
        return $this->isSupportedLang($lang) && $this->isSupportedCurrency($currency);
    }

    /**
     * Save messaging into db.
     *
     * @param Placement $data
     * @return integer
     */
    public function save(Placement $data) : int
    {
        unset($data->currency);
        unset($data->country);

        if (!empty($data->id)) {
            return $this->database->update(self::MESSAGING_TABLE, 'id', (int) $data->id, $data);
        }

        return $this->database->insert(self::MESSAGING_TABLE, $data);
    }

    /**
     * Delete a placement
     *
     * @param integer $id
     * @return integer
     */
    public function delete(int $id) : int
    {
        return $this->database->delete(self::MESSAGING_TABLE, 'id', $id);
    }

    /**
     * Load all messaging data
     *
     * @param integer $offset
     * @param integer $limit
     * @return array
     */
    public function all() : array
    {
        $query = 'SELECT m.*, s.cISO as country, w.cISO as currency FROM ' . self::MESSAGING_TABLE . ' as m ' .
            'JOIN tsprache as s ON s.kSprache = m.kSprache ' .
            'JOIN twaehrung as w ON w.kWaehrung = m.kWaehrung ';
        $result = $this->database->query($query, ReturnType::ARRAY_OF_ASSOC_ARRAYS);

        if (empty($result)) {
            return [];
        }

        $placements = [];
        foreach ($result as $row) {
            $placements[] = new Placement($row);
        }

        return $placements;
    }

    /**
     * Get messaging data
     *
     * @param integer $id
     * @return Placement|null
     */
    public function get(int $id) : ?Placement
    {
        $query = 'SELECT m.*, s.cISO as country, w.cISO as currency FROM ' . self::MESSAGING_TABLE . ' as m ' .
            'JOIN tsprache as s ON s.kSprache = m.kSprache ' .
            'JOIN twaehrung as w ON w.kWaehrung = m.kWaehrung ' .
            'WHERE m.id = :id';

        $row = $this->database->queryPrepared($query, ['id' => $id], ReturnType::SINGLE_ASSOC_ARRAY);

        if ($row) {
            return new Placement($row);
        }

        return null;
    }

    /**
     * Get messaging data by its country and currency combination
     *
     * @param integer $country
     * @param integer $currency
     * @return Placement|null
     */
    public function getBy(int $country, int $currency) : ?Placement
    {
        $row = $this->queryGetBy($country, $currency, ReturnType::SINGLE_ASSOC_ARRAY);

        if ($row) {
            return new Placement($row);
        }

        return null;
    }

     /**
     * Get messaging data by its country and currency combination
     *
     * @param integer $country
     * @param integer $currency
     * @return Placement[]|null
     */
    public function getAllBy(int $country, int $currency) : ?array
    {
        $rows = $this->queryGetBy($country, $currency);
        $data = [];

        if ($rows) {
            foreach ($rows as $row) {
                $data[] = new Placement($row);
            }

            return $data;
        }

        return null;
    }

    /**
     * Get messaging data by the current country and currency combination
     *
     * @throws InvalidArgumentException if lang/currency combination is not supported
     * @return Placement[]|null
     */
    public function getCurrent() : ?array
    {
        /** @var \JTL\Language\LanguageModel $lang */
        /** @var \JTL\Catalog\Currency $currency */
        $lang = $this->session->getJtl('currentLanguage');
        $currency = $this->session->getJtl('Waehrung');

        // Unsupported combination
        if (!$this->isSupported($lang, $currency)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid currency or language. Combination of "%s" and "%s" is not supported by OSM!',
                    $lang->getIso639(),
                    $currency->getCode()
                )
            );
        }
    
        return $this->getAllBy(
            $this->session->getJtl('currentLanguage')->getId(),
            $this->session->getJtl('Waehrung')->getId()
        );
    }

    /**
     * Get by query.
     *
     * @param integer $country
     * @param integer $currency
     * @param integer $type
     * @return array|integer|null
     */
    private function queryGetBy(int $country, int $currency, int $type = ReturnType::ARRAY_OF_ASSOC_ARRAYS)
    {
        $query = 'SELECT m.*, s.cISO as country, w.cISO as currency FROM ' . self::MESSAGING_TABLE . ' as m ' .
            'JOIN tsprache as s ON s.kSprache = m.kSprache ' .
            'JOIN twaehrung as w ON w.kWaehrung = m.kWaehrung ' .
            'WHERE m.kSprache = :country AND m.kWaehrung = :currency';

        $params = ['country' => $country, 'currency' => $currency];
        return $this->database->queryPrepared($query, $params, $type);
    }
}
