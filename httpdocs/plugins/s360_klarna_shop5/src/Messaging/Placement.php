<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Messaging;

use JTL\Helpers\Request;

/**
 * Handles the placement of the onsite messaging tags.
 *
 * @package Plugin\s360_klarna_shop5\src\Messaging
 */
class Placement
{
    public const BEFORE = 'before';
    public const AFTER = 'after';
    public const ITEM_PAGE = 'item';
    public const BASKET_PAGE = 'basket';
    public const HOME_PAGE = 'home';

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $kSprache;

    /**
     * @var int
     */
    public $kWaehrung;

    /**
     * @var int
     */
    public $active;

    /**
     * @var string
     */
    public $placement_type;

    /**
     * @var array
     */
    public $placement_position = [];

    /**
     * @var string
     */
    public $placement_location;

    /**
     * @var string
     */
    public $placement_tag = '';

    /**
     * @var string
     */
    public $javascript_tag = '';

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $currency;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            $this->id = (int) $data['id'] ?? null;
            $this->kSprache = (int) $data['kSprache'];
            $this->kWaehrung = (int) $data['kWaehrung'];
            $this->active = (int) $data['active'];
            $this->placement_type = $data['placement_type'];
            $this->placement_location = $data['placement_location'];
            $this->placement_tag = $data['placement_tag'];
            $this->javascript_tag = $data['javascript_tag'];
            $this->country = $data['country'] ?? null;
            $this->currency = $data['currency'] ?? null;

            $pos = $data['placement_position'];
            $this->placement_position = is_string($pos) ? json_decode($pos) : json_encode($pos);
        }
    }

    /**
     * Place an active placement tag on the site
     *
     * @param string $selector
     * @param integer $amount
     * @return void
     */
    public function place(string $selector, int $amount) : void
    {
        if (!$this->active) {
            return;
        }

        pq('body')->prepend($this->javascript_tag);

        $tag = $this->prepareTag($amount);

        if ($this->placement_location === self::BEFORE) {
            pq($selector)->prepend($tag);
        } elseif ($this->placement_location === self::AFTER) {
            pq($selector)->append($tag);
        }

        // Refresh placements on ajax request
        if (Request::isAjaxRequest()) {
            pq($selector)->append(
                '<script>window.KlarnaOnsiteService = window.KlarnaOnsiteService || [];' .
                'window.KlarnaOnsiteService.push({ eventName: \'refresh-placements\' });</script>'
            );
        }
    }

    /**
     * Prepare the placement tag.
     *
     * @param integer $amount
     * @return string
     */
    public function prepareTag(int $amount) : string
    {
        if ($amount > 0 && strpos($this->placement_tag, '<klarna-placement') !== false) {
            $pattern = '/<klarna-placement/ism';
            $replace = '<klarna-placement data-purchase-amount="' . $amount . '"';

            // Old purchase amount tag
            if (strpos($this->placement_tag, 'data-purchase_amount=""') !== false) {
                return str_replace(
                    'data-purchase_amount=""',
                    'data-purchase_amount="' . $amount . '"',
                    $this->placement_tag
                );
            }

            if (strpos($this->placement_tag, 'data-purchase_amount=') !== false) {
                $pattern = '/data-purchase_amount="(.*)"/ism';
                $replace = 'data-purchase_amount="' . $amount . '"';
            }

            // New Purchase Amount tag
            if (strpos($this->placement_tag, 'data-purchase-amount=""') !== false) {
                return str_replace(
                    'data-purchase-amount=""',
                    'data-purchase-amount="' . $amount . '"',
                    $this->placement_tag
                );
            }

            if (strpos($this->placement_tag, 'data-purchase-amount=') !== false) {
                $pattern = '/data-purchase-amount="(.*)"/ism';
                $replace = 'data-purchase-amount="' . $amount . '"';
            }

            return preg_replace($pattern, $replace, $this->placement_tag);
        }

        return $this->placement_tag;
    }

    /**
     * Check if the placement is active on a specific position.
     *
     * @param string $position
     * @return boolean
     */
    public function hasPosition(string $position) : bool
    {
        return !empty($position) && in_array($position, $this->placement_position);
    }

    /**
     * Get the localized positions
     *
     * @return string
     */
    public function localizedPositions() : string
    {
        $output = [];
        $translations = [
            self::BASKET_PAGE => __('Warenkorb-Seite'),
            self::HOME_PAGE => __('Homepage'),
            self::ITEM_PAGE => __('Artikel-Seite'),
        ];

        foreach ($this->placement_position as $pos) {
            $output[] = $translations[$pos] ?? __($pos);
        }

        return implode(', ', $output);
    }
}
