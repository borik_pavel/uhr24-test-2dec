<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Mappers;

/**
 * Container for the mapped data.
 *
 * @package Plugin\s360_klarna_shop5\src\Mappers
 */
class MappedData implements \ArrayAccess
{
    /**
     * @var array
     */
    private $container = [];

    /**
     * Fill Data structure
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->container = $data;
    }

    /**
     * Return Data as array
     *
     * @return array
     */
    public function toArray() : array
    {
        return $this->container;
    }

    /**
     * Set value for a data entry.
     *
     * @param string|integer|null $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value) : void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
            return;
        }

        $this->container[$offset] = $value;
    }

    /**
     * Check if an offset exists.
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) : bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Unset an offset.
     *
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset($offset) : void
    {
        unset($this->container[$offset]);
    }

    /**
     * Get the value of an offset.
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }
}
