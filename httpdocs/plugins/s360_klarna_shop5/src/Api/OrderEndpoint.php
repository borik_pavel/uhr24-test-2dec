<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Api;

use Klarna\Rest\Payments\Orders;
use Plugin\s360_klarna_shop5\src\Api\SDK\Capture;
use Plugin\s360_klarna_shop5\src\Api\SDK\Order;
// use Klarna\Rest\OrderManagement\Order;
// use Klarna\Rest\OrderManagement\Capture;
use Klarna\Rest\Resource;

/**
 * Klarna Session Endpoint Wrapper.
 *
 * @package Plugin\s360_klarna_shop5\src\Api
 */
class OrderEndpoint extends AbstractEndpoint
{
    /**
     * Create new OrderManagement instance
     *
     * @param string $orderId
     * @param array $args
     * @return Order|Capture
     */
    protected function factory(string $orderId = null, array $args = []) : Resource
    {
        $order = new Order($this->client->getConnector(), $orderId);

        if (array_key_exists('capture_id', $args)) {
            return $order->fetchCapture($args['capture_id']);
        }

        return $order;
    }

    /**
     * Place the order after it was authorized
     *
     * @param array $data
     * @param string $authToken
     * @return array
     */
    public function place(array $data, string $authToken) : array
    {
        $order = new Orders($this->client->getConnector(), $authToken);
        return $order->create($data);
    }

    /**
     * Fetch a single order
     *
     * @param string $orderId
     * @return Order
     */
    public function fetch(string $orderId) : Order
    {
        return $this->passthrough('fetch', ['id' => $orderId]);
    }

    /**
     * Acknowledge an authorized order.
     *
     * Merchants will receive the order confirmation push until the order
     * has been acknowledged.
     *
     * @param string $orderId
     * @return Order
     */
    public function acknowledge(string $orderId) : Order
    {
        return $this->passthrough('acknowledge', ['id' => $orderId]);
    }

    /**
     * Cancel an authorized order.
     *
     * For a cancellation to be successful, there must be no captures on the order.
     *
     * @param string $orderId
     * @return Order|null
     */
    public function cancel(string $orderId) : ?Order
    {
        return $this->withEscalation()->passthrough('cancel', ['id' => $orderId]);
    }

    /**
     * Creates a new capture.
     *
     * Use this call when fulfillment is completed, e.g. physical goods are being shipped to the customer.
     *
     * @param string $orderId
     * @param array $data
     * @param array $idempotency
     * @return Capture|null
     */
    public function authorize(string $orderId, array $data, array $idempotency = []) : ?Capture
    {
        return $this->withEscalation()->passthrough('createCapture', ['id' => $orderId, 'data' => $data], $idempotency);
    }

    /**
     * Update the merchant references
     *
     * @param string $orderId
     * @param array $refs
     * @return Order
     */
    public function updateMerchantReferences(string $orderId, array $refs) : Order
    {
        return $this->passthrough('updateMerchantReferences', ['id' => $orderId, 'data' => $refs]);
    }

    /**
     * Extend the order's authorization by default period according to merchant contract.
     *
     * @param string $orderId
     * @param string $expires
     * @return Order
     */
    public function extendAuthorization(string $orderId, string $expires) : Order
    {
        return $this->passthrough('extendAuthorizationTime', ['id' => $orderId], [$expires]);
    }

    /**
     * Release the remaining authorization for an order.
     *
     * Signal that there is no intention to perform further captures.
     *
     * @param string $orderId
     * @return Order|null
     */
    public function releaseAuthorization(string $orderId) : ?Order
    {
        return $this->withEscalation()->passthrough('releaseRemainingAuthorization', ['id' => $orderId]);
    }

    /**
     * Add shipping data to a capture.
     *
     * @param string $orderId
     * @param string $captureId
     * @param array $data
     * @return Capture
     */
    public function addShippingData(string $orderId, string $captureId, array $data) : Capture
    {
        return $this->passthrough('addShippingInfo', ['id' => $orderId, 'capture_id' => $captureId, 'data' => $data]);
    }
}
