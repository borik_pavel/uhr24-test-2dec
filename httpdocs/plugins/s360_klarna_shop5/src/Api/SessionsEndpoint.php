<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Api;

use Klarna\Rest\Payments\Sessions;
use Klarna\Rest\Transport\Exception\ConnectorException;

/**
 * Klarna Session Endpoint Wrapper.
 *
 * @package Plugin\s360_klarna_shop5\src\Api
 */
class SessionsEndpoint extends AbstractEndpoint
{
    /**
     * Create new Session instance.
     *
     * @param string $sessionId
     * @param array $args
     * @return Sessions
     */
    protected function factory(string $sessionId = null, array $args = []) : Sessions
    {
        return new Sessions($this->client->getConnector(), $sessionId);
    }

    /**
     * Create a Klarna session for the customer with the following data:
     *  - session_id: used for server-side updates on the session via the server-side REST API
     *  - client_token: used to initialize the widget
     *  - payment_method_category: represents what categories of payment methods that are available
     *    for this purchase (required when loading a widget)
     *
     * @param array $data
     * @return Sessions|null
     */
    public function create(array $data) : ?Sessions
    {
        return $this->withRedirect('bestellvorgang.php?editZahlungsart=1&sessionError=1')->passthrough('create', ['data' => $data]);
    }

    /**
     * Update the session on page reload and fetch the current session data.
     *
     * @SuppressWarnings(PHPMD.Superglobals)
     * @param string $sessionId
     * @param array $data
     * @return SessionEntity
     */
    public function update(string $sessionId, array $data) : Sessions
    {
        $this->client->enableExceptionPassthrough();

        try {
            return $this
                ->withRedirect('bestellvorgang.php?editZahlungsart=1')
                ->passthrough('update', ['id' => $sessionId, 'data' => $data]);
        } catch (ConnectorException $exc) {
            unset($_SESSION['s360_klarna']['session']);
            $this->client->disableExceptionPassthrough();
            $this->withRedirect('bestellvorgang.php?editZahlungsart=1')->exceptionHandler(
                $sessionId,
                'klarna_api_' . $exc->getErrorCode(),
                $exc,
                "Connector-Exception in passthrough of " . get_class($this) . "::update",
                implode('<br/>', $exc->getMessages())
            );
        }

        $this->client->disableExceptionPassthrough();
    }

    /**
     * Fetch an existing credit session.
     *
     * @param string $sessionId
     * @return Sessions|null
     */
    public function fetch(string $sessionId) : ?Sessions
    {
        return $this->withRedirect('bestellvorgang.php?editZahlungsart=1&sessionError=1')->passthrough('fetch', ['id' => $sessionId]);
    }
}
