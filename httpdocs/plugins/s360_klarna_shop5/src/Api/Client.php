<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Api;

use Exception;
use JTL\Services\Container;
use Klarna\Rest\Transport\UserAgent;
use Klarna\Rest\Transport\GuzzleConnector;
use Klarna\Rest\Transport\ConnectorInterface as IConnector;
use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Utils\JtlLoggerTrait;
use JTL\Shop;
use Klarna\Rest\Transport\ConnectorInterface;

/**
 * Klarna API Client.
 *
 * Acts as a wrapper for the Klarna REST SDK.
 *
 * @package Plugin\s360_klarna_shop5\src\Api
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class Client
{
    use JtlLoggerTrait;

    /**
     * @var string|null
     */
    protected $mode;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var IConnector
     */
    protected $connector;

    /**
     * Api Endpoints
     *
     * @var array
     */
    protected $endpoints = [];

    /**
     * Whether or not execptions should be catched by the endpoint
     * or should be passed through.
     *
     * @var bool
     */
    protected $exceptionPassthrough = false;

    /**
     * Undocumented variable
     *
     * @var string|null
     */
    protected $escalationMail;

    /**
     * Initialize the klarna api.
     */
    public function __construct(Config $config, Container $container)
    {
        // Use a custom user agent to transfer used SDK, PHP, JTL and Plugin.
        $customUserAgent = new UserAgent();
        $customUserAgent->setField('SDK', UserAgent::NAME, UserAgent::VERSION)
            ->setField('Language', 'PHP', phpversion())
            ->setField('Platform', 'JTL', Shop::getApplicationVersion())
            ->setField('Plugin', $config::PLUGIN_ID, $config->getPluginVersion());

        // Setup Connector
        $this->mode      = $config->get('sandbox');
        $this->baseUrl   = $this->isSandbox() ? IConnector::EU_TEST_BASE_URL : IConnector::EU_BASE_URL;
        $this->connector = GuzzleConnector::create(
            $config->get(Config::KEY_MERCHANT_ID),
            $config->get(Config::KEY_SHARED_SECRET),
            $this->baseUrl,
            $customUserAgent
        );

        $this->escalationMail = $config->get(Config::KEY_ESCALATION_MAIL);

        // Set Endpoints
        $this->setSessionsEndpoint($container);
        $this->setOrderEndpoint($container);
    }

    /**
     * Enable the exception passthrough for endpoints.
     *
     * @return void
     */
    public function enableExceptionPassthrough() : void
    {
        $this->exceptionPassthrough = true;
    }

    /**
     * Disable the exception passthrough for endpoints.
     *
     * @return void
     */
    public function disableExceptionPassthrough() : void
    {
        $this->exceptionPassthrough = false;
    }

    /**
     * Check if the exception passthrough for endpoints is enabled.
     *
     * @return bool
     */
    public function isExceptionPassthroughEnabled() : bool
    {
        return $this->exceptionPassthrough;
    }

    /**
     * Get the currently used connector.
     *
     * @return ConnectorInterface
     */
    public function getConnector() : ConnectorInterface
    {
        return $this->connector;
    }

    /**
     * Get the merchant mail address for the escalation mails.
     *
     * @return string
     */
    public function getEscalationMail() : string
    {
        return $this->escalationMail ?? '';
    }

    /**
     * Set the sessions endpoint.
     *
     * @param Contaienr $container
     * @return void
     */
    public function setSessionsEndpoint(Container $container) : void
    {
        $this->endpoints[SessionsEndpoint::class] = $container->get(SessionsEndpoint::class);
        $this->endpoints[SessionsEndpoint::class]->setClient($this);
    }

    /**
     * Get the Sessions Endpoint.
     *
     * @return SessionsEndpoint
     */
    public function getSessionsEndpoint() : ?SessionsEndpoint
    {
        return $this->getEnpoint(SessionsEndpoint::class);
    }

    /**
     * Set the sessions endpoint.
     *
     * @param Contaienr $container
     * @return void
     */
    public function setOrderEndpoint(Container $container) : void
    {
        $this->endpoints[OrderEndpoint::class] = $container->get(OrderEndpoint::class);
        $this->endpoints[OrderEndpoint::class]->setClient($this);
    }

    /**
     * Get the Sessions Endpoint.
     *
     * @return OrderEndpoint
     */
    public function getOrderEndpoint() : ?OrderEndpoint
    {
        return $this->getEnpoint(OrderEndpoint::class);
    }
    
    /**
     * Get an api endpoint handler.
     *
     * @param string $endpoint
     * @return AbstractEndpoint|null
     */
    protected function getEnpoint($endpoint) : ?AbstractEndpoint
    {
        try {
            return $this->endpoints[$endpoint];
        } catch (Exception $e) {
            $this->errorLog($e->getMessage(), 'No Endpoint found for ' . $endpoint);
        }

        return null;
    }

    /**
     * Checks if we are in sandbox mode or not.
     *
     * @return bool
     */
    public function isSandbox() : bool
    {
        return $this->mode === Config::SANDBOX_MODE;
    }
}
