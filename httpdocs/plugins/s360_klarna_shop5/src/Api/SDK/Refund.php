<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Api\SDK;

use Klarna\Rest\OrderManagement\Refund as KlarnaRefund;

/**
 * Extend the Klarna REST SDK so that it supports Idempotency Keys.
 *
 * @package Plugin\s360_klarna_shop5\src\Api\SDK
 */
class Refund extends KlarnaRefund
{
    use IdempotencyKeyTrait;

    /**
     * {@inheritDoc}
     */
    public function create(array $data)
    {
        $url = $this->getLocation();
        $headers = [
            'Content-Type'           => 'application/json',
            'Klarna-Idempotency-Key' => $this->generateIdempotencyKey($url, $data)
        ];

        $url = $this->request('POST', $url, $headers, $data !== null ? \json_encode($data) : null)
            ->expectSuccessfull()
            ->status('201')
            ->getLocation();

        $this->setLocation($url);

        return $this;
    }
}
