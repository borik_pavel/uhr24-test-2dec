<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Api\SDK;

use Klarna\Rest\OrderManagement\Capture as KlarnaCapture;

/**
 * Extend the Klarna REST SDK so that it supports Idempotency Keys.
 *
 * @package Plugin\s360_klarna_shop5\src\Api\SDK
 */
class Capture extends KlarnaCapture
{
    use IdempotencyKeyTrait;

    /**
     * {@inheritDoc}
     */
    public function create(array $data)
    {
        $url = $this->getLocation();
        $headers = [
            'Content-Type'           => 'application/json',
            'Klarna-Idempotency-Key' => $this->generateIdempotencyKey($url, $data)
        ];

        $url = $this->request('POST', $url, $headers, $data !== null ? \json_encode($data) : null)
            ->expectSuccessfull()
            ->status('201')
            ->getLocation();

        $this->setLocation($url);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addShippingInfo(array $data)
    {
        $url = $this->getLocation() . '/shipping-info';
        $headers = [
            'Content-Type'           => 'application/json',
            'Klarna-Idempotency-Key' => $this->generateIdempotencyKey($url, $data)
        ];

        $this->request('POST', $url, $headers, $data !== null ? \json_encode($data) : null)
            ->expectSuccessfull()
            ->status('204');

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function updateCustomerDetails(array $data)
    {
        $url = $this->getLocation() . '/customer-details';
        $headers = [
            'Content-Type'           => 'application/json',
            'Klarna-Idempotency-Key' => $this->generateIdempotencyKey($url, $data)
        ];

        $this->request('PATCH', $url, $headers, $data !== null ? \json_encode($data) : null)
            ->expectSuccessfull()
            ->status('204');

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function triggerSendout()
    {
        $url = $this->getLocation() . '/trigger-send-out';
        $headers = [
            'Content-Type'           => 'application/json',
            'Klarna-Idempotency-Key' => $this->generateIdempotencyKey($url)
        ];

        $this->request('POST', $url, $headers)
            ->expectSuccessfull()
            ->status('204');

        return $this;
    }
}
