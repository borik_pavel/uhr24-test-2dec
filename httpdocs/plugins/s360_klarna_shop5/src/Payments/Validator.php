<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Payments;

use JTL\Cart\Cart;
use JTL\Alert\Alert;
use JTL\Shop;
use Plugin\s360_klarna_shop5\src\Utils\Session;
use Plugin\s360_klarna_shop5\src\Utils\Config;
use Plugin\s360_klarna_shop5\src\Utils\JtlLoggerTrait;
use Plugin\s360_klarna_shop5\src\Utils\AlertHelper;
use Plugin\s360_klarna_shop5\src\Utils\Translation;

/**
 * Validator Class to handle the klarna payment validation.
 *
 * @package Plugin\s360_klarna_shop5\src\Payments
 */
class Validator
{
    use JtlLoggerTrait;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AlertHelper
     */
    protected $alert;

    /**
     * @param Session $session
     * @param AlertHelper $alert
     */
    public function __construct(Session $session, AlertHelper $alert)
    {
        $this->session = $session;
        $this->alert = $alert;
    }

    /**
     * Get the auth token.
     * If the token does not exist, redirect and show an error message.
     *
     * @return string
     */
    public function getValidatedAuthToken() : string
    {
        $authtoken = $this->session->getJtl('cPost_arr')['_klarna_auth'] ?? null;

        if (!$authtoken) {
            $this->errorLog('No authentication token available');
            $this->alert->flash(
                Alert::TYPE_ERROR,
                Translation::KEY_MISSING_AUTH_TOKEN,
                'klarnaError',
                Config::PAYMENT_ERROR_RETURN_URL
            );
        }

        return $authtoken;
    }

    /**
     * Handle an authentication exception
     *
     * @param string $message        Log Message
     * @param string $context        Log Context
     * @param string $translationKey User Error Message Key
     * @return void
     */
    public function handleAuthException(string $message, string $context, string $translationKey) : void
    {
        $this->errorLog($message, $context);
        $this->alert->flash(Alert::TYPE_DANGER, $translationKey, 'klarnaError', Config::PAYMENT_ERROR_RETURN_URL);
    }

    /**
     * Run klarna's preassessment to determine if $paymentId is available to the user.
     *
     * @param string $paymentId
     * @return boolean
     */
    public function preAssessment(string $paymentId) : bool
    {
        // Klarna Pre-Assessment
        if (!array_key_exists('payment_method_categories', $this->session->get('session'))) {
            $this->debugLog('No klarna payment methods available for this customer', $paymentId);
            return false;
        }

        // Check Klarna's evaluation if the payment method should be available.
        $paymentMethods = $this->session->get('session')['payment_method_categories'] ?? [];

        if (!in_array($paymentId, array_column($paymentMethods, 'identifier'))) {
            $this->noticeLog(
                'The pre-assessment showed that this payment method should not available for this customer',
                $paymentId
            );

            return false;
        }

        return true;
    }

    /**
     * Checks if the payment method is okay to use before the confirmation screen is loaded.
     *
     * @param string $paymentId Id of payment which is checked
     * @param array $request    Assoc array of request data
     * @return bool
     */
    public function checkBeforeConfirm(string $paymentId, array $request = []) : bool
    {
        // Check if we need an additional klarna auth call because it was skipped due to preselected payment
        $correctRequest = empty($request) || !array_key_exists('zahlungsartwahl', $request);
        if ($correctRequest && is_null($this->session->get('selected_payment'))) {
            $this->debugLog(
                'Skipped payment selection -> not authorized. Redirect to payment selection',
                'checkBeforeConfirm'
            );
            $url = Shop::Container()->getLinkService();
            header('Location: ' . $url->getStaticRoute(Config::PAYMENT_ERROR_RETURN_URL));
            exit;
        }

        // Changed cart on already authorized payment -> needs reauth
        $reauthAt = $this->session->get('reauthorize_at') ?? time() + (60*60);
        $selected = $this->session->get('selected_payment');
        $finalizeRequired = array_key_exists('_klarna_auth', $request) && $request['_klarna_auth'] === 'FINALIZE_REQUIRED';

        if (($selected === $paymentId && $this->cartChanged()) || $finalizeRequired || $reauthAt < time()) {
            $this->session->set('reauth_needed', true);
        }

        // No Reauth needed, as user changed payment method, i.e. new authorization
        if ($selected && $selected !== $paymentId) {
            $this->session->set('reauth_needed', false);
        }

        $this->session->set('selected_payment', $paymentId);
        $this->session->set('auth_token', $request['_klarna_auth'] ?? null);
        $this->session->set('authorized', true);
        $this->setCartChecks();

        return true;
    }

    /**
     * This method is called when the user clicks the BUY NOW button, but before any other handling happens.
     * This is a great spot to handle changed basket checksums.
     *
     * @param string $paymentId Id of payment which is checked
     * @return void
     */
    public function checkAfterConfirm(string $paymentId) : void
    {
        // Check if we should reauthorize the user (if we don't already know that its needed)
        if ($this->needsReauthCall() && !$this->session->get('reauth_needed')) {
            $this->noticeLog(
                'The users authentication token expired. Redirect the user to reauthorize.',
                $paymentId
            );

            $msg = Translation::KEY_AUTH_TOKEN_EXPIRED;
            $this->alert->flash(Alert::TYPE_WARNING, $msg, 'klarnaError', Config::REAUTH_ERROR_RETURN_URL);
        }

        /**
         * If the cart content changed, redirect the user to the error screen and reauthorize,
         * as we cannot update the klarna session and call the reauthorize API endpoint anymore
         * (Order is already authorized and finalized on klarna's side).
         *
         * JTLs should have already redirected the user automatically, but if not, we do it ourself!
         */
        if ($this->cartChanged()) {
            $this->noticeLog(
                'The users cart changed since he selected the payment method and is therefore redirected.',
                $paymentId
            );

            $msg = Translation::KEY_BASKET_CHANGE;
            $this->alert->flash(Alert::TYPE_WARNING, $msg, 'klarnaError', Config::PAYMENT_ERROR_RETURN_URL);
        }
    }

    /**
     * Update checksum of the cart and also the sum of the cart.
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @return void
     */
    public function setCartChecks() : void
    {
        /** @var Cart $cart */
        $cart = $this->session->getJtl('Warenkorb');
        $this->session->set('cart_checksum', Cart::getChecksum($cart));
        $this->session->set('cart_sum', $cart->gibGesamtsummeWaren(true, true));
    }

    /**
     * Check if the contents of the cart did change.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return boolean
     */
    public function cartChanged() : bool
    {
        /** @var Cart $cart */
        $sum = $this->session->get('cart_sum');
        $cart = $this->session->getJtl('Warenkorb');
        $checksum = $this->session->get('cart_checksum');

        return $checksum !== Cart::getChecksum($cart) || $sum !== $cart->gibGesamtsummeWaren(true, true);
    }

    /**
     * Check if we should reauthorize the user. Happens for the following cases:
     *  - Cart Changed
     *  - Reauth needed for payment option (FINALIZED_REQUIRED)
     *  - Session expired
     *  - Skipped payment selection screen (and therefore auth) due to JTL preferred payment
     * @return boolean
     */
    public function needsReauthCall() : bool
    {
        $reauthAt = $this->session->get('reauthorize_at') ?? time() + (60 * 60);

        return $this->cartChanged() || $this->session->get('reauth_needed') || $reauthAt < time();
    }
}
