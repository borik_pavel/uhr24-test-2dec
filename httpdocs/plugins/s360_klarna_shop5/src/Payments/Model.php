<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Payments;

use Plugin\s360_klarna_shop5\src\Utils\Config;
use JTL\DB\DbInterface;
use JTL\Checkout\Bestellung;
use JTL\DB\ReturnType;
use JTL\Customer\Customer;
use stdClass;
use DateTime;

/**
 * Handles access to the klanra payments data in the DB.
 *
 * @package Plugin\s360_klarna_shop5\src\Payments
 */
class Model
{
    public const ORDER_ATTR_ORDER_ID = 'klarna_order_id';
    public const ORDER_TABLE = 'xplugin_' . Config::PLUGIN_ID . '_order';
    public const DELIVERY_TABLE = 'xplugin_' . Config::PLUGIN_ID . '_deliveries';
    public const CANCEL_STATUS = 'CANCELLED';
    public const DEFAULT_STATUS = 'AUTHORIZED';
    public const CAPTURED_STATUS = 'CAPTURED';
    public const PARTLY_CAPTURED_STATUS = 'PART_CAPTURED';

    /**
     * @var DBInterface
     */
    protected $database;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @param DbInterface $database
     * @param Config $config
     */
    public function __construct(DbInterface $database, Config $config)
    {
        $this->database = $database;
        $this->config = $config;
    }

    /**
     * Get Access to the db interface
     *
     * @return DBInterface
     */
    public function getDatabase() : DbInterface
    {
        return $this->database;
    }

    /**
     * Get the purchase history of a customer.
     *
     * @param Customer $customer
     * @return array
     */
    public function getPurchaseHistory(Customer $customer) : array
    {
        // Get Customer Email as Identifaction
        $mail = $customer->cMail;
        if (empty($mail)) {
            return [];
        }

        // Get the customers potentially different keys kKunde.
        $params = ['mail' => $mail];
        $keysSql = 'SELECT * FROM tkunde WHERE cMail = :mail';
        $keysResult = $this->database->executeQueryPrepared($keysSql, $params, ReturnType::ARRAY_OF_OBJECTS);

        if (empty($keysResult)) {
            return [];
        }

        $keys = [];
        foreach ($keysResult as $res) {
            if ($res->kKunde > 0) {
                $keys[] = intval($res->kKunde);
            }
        }

        if (empty($keys)) {
            return [];
        }

        // Build Result Array with types.
        foreach (Config::PAYMENT_OPTION_TYPES as $type) {
            $result[$type] = [
                'name' => $type,
                'count' => 0,
                'amount' => 0,
                'date_last' => null,
                'date_first' => null
            ];
        }

        // Get all orders that are paid by that customer.
        $params = ['date' => '0000-00-00'];
        $orderSql = 'SELECT * FROM tbestellung WHERE dBezahltDatum != :date AND kKunde IN (' . join(',', $keys) . ')';
        $orderResult = $this->database->executeQueryPrepared($orderSql, $params, ReturnType::ARRAY_OF_OBJECTS);

        if (empty($orderResult)) {
            return [];
        }

        foreach ($orderResult as $order) {
            $type = Config::PAYMENT_OPTION_MAP[$order->cZahlungsartName] ?? 'other';
            $result[$type]['count'] += 1;
            $result[$type]['amount'] += floatval($order->fGesamtsumme);

            // Update dates if needed
            $date = new DateTime($order->dErstellt);
            $dateFirst = $result[$type]['date_first'];
            $dateLast = $result[$type]['date_last'];

            if ($dateFirst === null || new DateTime($dateFirst) > $date) {
                $result[$type]['date_first'] = $date->format(Config::KLARNA_DATETIME_FORMAT);
            }
            if ($dateLast === null || new DateTime($dateLast) < $date) {
                $result[$type]['date_last'] = $date->format(Config::KLARNA_DATETIME_FORMAT);
            }
        }

        // Filter Result array, so that only used payments types (i.e. count > 0) are returned.
        $filtered = [];
        foreach ($result as $key => $value) {
            if ($value['count'] > 0) {
                $filtered[$key] = $value;
            }
        }

        return $filtered;
    }

    /**
     * Insert Klarna Order into db.
     *
     * @param Bestellung $order
     * @param array $klarna
     * @return integer
     */
    public function insertOrder(?Bestellung $order, array $klarna, ?string $status) : int
    {
        $data = new stdClass;
        $data->jtl_order_id = $order->kBestellung;
        $data->jtl_order_number = $order->cBestellNr;
        $data->klarna_order_id = $klarna['order_id'];
        $data->klarna_status = $status ?? self::DEFAULT_STATUS;
        $data->klarna_fraud_status = $klarna['fraud_status'];
        $data->payment_method = $order->cZahlungsartName;
        $data->payment_method_type = $klarna['authorized_payment_method']['type'] ?? null;
        // $data->country = '@TODO';
        $data->server = $this->config->get('sandbox');
        $data->activation = $status == self::CAPTURED_STATUS ? 1 : -1;

        return $this->database->insert(self::ORDER_TABLE, $data);
    }

    /**
     * Loads orders paginated, including some information from tbestellung
     *
     * @param integer $offset
     * @param integer $limit
     * @return array
     */
    public function loadOrders(int $offset, int $limit) : array
    {
        $sql =
            'SELECT * FROM ' . self::ORDER_TABLE .
            ' LEFT JOIN tbestellung ON tbestellung.kBestellung = ' . self::ORDER_TABLE . '.jtl_order_id
             ORDER BY jtl_order_id DESC LIMIT :limit OFFSET :offset';

        $params = ['limit' => $limit, 'offset' => $offset];
        $result = $this->database->executeQueryPrepared($sql, $params, ReturnType::ARRAY_OF_OBJECTS);

        if (empty($result)) {
            return [];
        }

        $this->addPaymentNames($result);

        return $result;
    }

    /**
     * Searches orders by klarna_order_id or order number.
     *
     * @param $searchValue
     * @return array
     */
    public function searchOrders($searchValue) : array
    {
        // we do not allow empty searches
        if (empty($searchValue)) {
            return [];
        }

        $sql = 'SELECT * FROM ' . self::ORDER_TABLE . '
             LEFT JOIN tbestellung ON tbestellung.kBestellung = ' . self::ORDER_TABLE . '.jtl_order_id
             WHERE jtl_order_number LIKE :searchValue OR klarna_order_id LIKE :searchValue
             ORDER BY jtl_order_id DESC LIMIT 100';

        $params = ['searchValue' => '%' . $searchValue . '%'];
        $result = $this->database->executeQueryPrepared($sql, $params, ReturnType::ARRAY_OF_OBJECTS);

        if (empty($result)) {
            return [];
        }

        $this->addPaymentNames($result);

        return $result;
    }

    /**
     * Mark a klarna order as canceled
     *
     * @param string $orderId
     * @return boolean
     */
    public function cancelOrder(string $orderId)
    {
        $order = $this->database->select(self::ORDER_TABLE, 'klarna_order_id', $orderId);

        if (is_null($order)) {
            return;
        }

        $this->updateOrderDetails(['status' => self::CANCEL_STATUS], $order->klarna_order_id);
    }

    /**
     * Get an order by JTLs internal order id
     *
     * @param int $orderId
     * @return object|null
     */
    public function getOrderById(int $orderId) : ?object
    {
        if (empty($orderId)) {
            return null;
        }

        $result = $this->database->select(self::ORDER_TABLE, 'jtl_order_id', $orderId);

        if (!empty($result)) {
            return $result;
        }

        return null;
    }

     /**
     * Get an order by Klarnas order id
     *
     * @param string $orderId
     * @return object|null
     */
    public function getOrderByKlarnaId(string $orderId) : ?object
    {
        if (empty($orderId)) {
            return null;
        }

        $result = $this->database->select(self::ORDER_TABLE, 'klarna_order_id', $orderId);

        if (!empty($result)) {
            return $result;
        }

        return null;
    }

    /**
     * Get a klarna order by its order number (jtl).
     *
     * @param string $orderNumber
     * @return object|null
     */
    public function getOrderByOrderNumber(string $orderNumber) : ?object
    {
        $order = $this->database->query(
            'SELECT kBestellung
                FROM tbestellung
                WHERE cBestellNr LIKE "%' .  $this->database->escape($orderNumber) . '%"',
            ReturnType::SINGLE_OBJECT
        );

        if ($order) {
            return $this->getOrderById((int) $order->kBestellung);
        }

        return null;
    }

    /**
     * Update the locally save order details
     *
     * @param array $details
     * @param string $orderId
     * @return void
     */
    public function updateOrderDetails(array $details, string $orderId) : void
    {
        $data = new stdClass;

        if (isset($details['status'])) {
            $data->klarna_status = $details['status'];
            $data->activation = $details['status'] == self::CAPTURED_STATUS;
        }

        if (isset($details['fraud_status'])) {
            $data->klarna_fraud_status = $details['fraud_status'];
        }

        if (isset($details['purchase_country'])) {
            $data->country = $details['purchase_country'];
        }


        $this->database->update(self::ORDER_TABLE, 'klarna_order_id', $orderId, $data);
    }

    /**
     * Get all processed deliveries.
     *
     * @return array
     */
    public function getProcessedDeliveries() : array
    {
        return $this->database->selectAll(self::DELIVERY_TABLE, 'processed', 1);
    }

    /**
     * Saven a delivery as processed.
     *
     * @param int $deliveryId
     * @param string $number
     * @return void
     */
    public function saveProcessedDelivery($deliveryId, $number) : void
    {
        $data = new stdClass;
        $data->id = $deliveryId;
        $data->number = $number;
        $data->processed = 1;

        $this->database->insert(self::DELIVERY_TABLE, $data);
    }

    /**
     * Save order attributes
     *
     * @param Bestellung $order
     * @param array $attributes
     * @return void
     */
    public function saveOrderAttributes(Bestellung $order, array $attributes): void
    {
        foreach ($attributes as $key => $value) {
            $attr = new stdClass;
            $attr->kBestellung = $order->kBestellung;
            $attr->cName = $key;
            $attr->cValue = $value;
            $this->database->insert('tbestellattribut', $attr);
        }
    }

    /**
     * Add Payment names to the result
     *
     * @param array $result
     * @return void
     */
    private function addPaymentNames(array &$result) : void
    {
        $sql = 'SELECT * FROM tzahlungsartsprache WHERE cName LIKE "%Klarna%"';
        $paymentNames = $this->database->executeQuery($sql, ReturnType::ARRAY_OF_OBJECTS);
        $names = [];
        $langs = [];
        $keys = [];

        foreach ($paymentNames as $name) {
            $keys[$name->cISOSprache] = null;
            $langs[$name->kZahlungsart][$name->cISOSprache] = $name->cName;
        }

        $keys = array_keys($keys);
        foreach ($keys as $lang) {
            $names += array_column($langs, null, $lang);
        }

        foreach ($result as &$row) {
            if (array_key_exists($row->payment_method, $names)) {
                $row->payment_method_names = $names[$row->payment_method];
            }
        }
    }
}
