<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Utils;

use JTL\Plugin\Data\Config as PluginConfig;
use JTL\Plugin\PluginInterface;

/**
 * Wrapper for the plugin configuration.
 *
 * @package Plugin\s360_klarna_shop5\src\Utils
 */
class Config
{
    /**
     * Basic Plugin Config Constants
     */
    public const SANDBOX_MODE = 'sandbox';
    public const PLUGIN_ID = 's360_klarna_shop5';
    public const LOGPREFIX = 'Klarna: ';
    public const PAY_LATER_ID = 'pay_later';
    public const PAY_NOW_ID = 'pay_now';
    public const PAY_NOW_DIRECT_BANK_TRANSFER_ID = 'direct_bank_transfer';
    public const PAY_NOW_DIRECT_DEBIT_ID = 'direct_debit';
    public const PAY_NOW_CARD_ID = 'card';
    public const SLICE_IT_ID = 'pay_over_time';
    public const PENDING_STATUS = 'PENDING';
    public const ACCEPTED_STATUS = 'ACCEPTED';
    public const CUSTOM_EMD_FILE = 'emd_custom.php';
    public const KLARNA_DATETIME_FORMAT = 'Y-m-d\TH:i:s\Z';
    public const KLARNA_ORDER_COMMENT_PREFIX = 'Klarna Bestell-ID: ';
    public const KLARNA_ORDER_COMMENT_SUFFIX = '';

    /**
     * ULS
     */
    public const PAYMENT_ERROR_RETURN_URL = 'bestellvorgang.php?editZahlungsart=1';
    public const REAUTH_ERROR_RETURN_URL = 'bestellvorgang.php';
    public const PORTAL_TEST_URL = 'playground.eu.portal.klarna.com';
    public const PORTAL_URL = 'eu.portal.klarna.com';
    public const FRONTEND_LINK_CONFIRMATION = 'confirmation.php';

    /**
     * Article Number Typ References.
     */
    public const ARTNO_TYP_VERSANDZUSCHLAG = 'SHIPPING_SURCHARGE';
    public const ARTNO_TYP_VERSANDPOS = 'SHIPPING_POSITION';
    public const ARTNO_TYP_VERPACKUNG = 'PACKAGING';
    public const ARTNO_TYP_ARTIKELABHAENGIG = 'ITEM_DEPENDENT';
    public const UNKOWN_REFERENCE = 'UNKNOWN';
    public const DISCOUNT_REFERENCE = 'DISCOUNT';

    /**
     * Config Option Keys/Values used in the backend settings.
     */
    public const KEY_CAPTURE_TIME = 'capture_time_of_shipping_and_discounts';
    public const KEY_MERCHANT_ID = 'merchantId';
    public const KEY_SHARED_SECRET = 'sharedSecret';
    public const KEY_ESCALATION_MAIL = 'escalationMail';
    public const KEY_ACTIVATE_AUTO_CAPTURE = 'activate_autocapture';
    public const VALUE_CAPTURE_TIME_START = 'start';
    public const VALUE_CAPTURE_TIME_END = 'end';

    /**
     * Templates paths and files.
     */
    public const TEMPLATE_SNIPPET_DIR = 'snippets';
    public const TEMPLATE_ORDERS_LIST = 'orders.tpl';
    public const TEMPLATE_ORDER_ITEM = 'order_item.tpl';
    public const TEMPLATE_ORDER_DETAIL= 'order_detail.tpl';
    public const TEMPLATE_ORDER_REVIEW = 'complete_order';
    public const TEMPLATE_PLACEMENT_DETAIL= 'placement_form.tpl';
    public const TEMPLATE_PLACEMENT_ITEM= 'placement_list_item.tpl';
    public const TEMPLATE_SNIPPET_BODY = 'snippets/body';
    public const TEMPLATE_SNIPPET_HEAD = 'snippets/head';
    public const TEMPLATE_ONSITE_MESSAGING_SETTINGS = 'onsite_messaging.tpl';
    public const TEMPLATE_KLARNA_PAYMENT_OPTION = 'klarna_payment';
    public const TEMPLATE_KLARNA_PAYMENT_OPTION_COMBINED = 'klarna_payment_combined';
    public const TEMPLATE_KLARNA_PAYMENT_OPTION_BADGE = 'klarna_payment_badge';

    /**
     * Maps the name of a payment method to the paymentOption type known by Klarna.
     * "card", "direct banking", "non klarna credit", "sms", "other"
     */
    public const PAYMENT_OPTION_TYPES = ['card', 'direct banking', 'non klarna credit', 'sms', 'other'];
    public const PAYMENT_OPTION_MAP = [
        "Vorkasse Überweisung" => "direct banking",
        "Nachnahme" => "other",
        "Rechnung" => "non klarna credit",
        "Lastschrift" => "direct banking",
        "Kreditkarte" => "card",
        "Barzahlung" => "other",
        "PayPal" => "other",
        "WorldPay" => "other",
        "iPayment" => "other",
        "SOFORT Überweisung" => "direct banking",
        "Safetypay" => "other",
        "PaymentPartner" => "other",
        "PostFinance" => "other",
        "Saferpay" => "other",
        "Wirecard" => "other",
        "Moneybookers eWallet" => "other",
        "All Credit Cards" => "card",
        "Lastschrift (ELV)" => "direct banking",
        "giropay" => "direct banking",
        "Mastercard" => "card",
        "VISA" => "card",
        "Maestro" => "card",
        "iDeal" => "other",
        "Carte Bleue" => "card",
        "CartaSi" => "card",
        "Dankort" => "card",
        "Nordea Solo Sweden" => "direct banking",
        "eNTES" => "other",
        "Laser" => "other",
        "EPS" => "other",
        "POLi" => "other",
        "Postepay" => "other",
        "Przelewy" => "other",
        "Solo" => "card",
        "Nordea Solo Finland" => "direct banking",
        "EOS Lastschrift" => "direct banking",
        "EOS Kreditkarte" => "card",
        "EOS Sofortberweisung" => "direct banking",
        "EOS eWallet" => "other",
        "Billpay" => "other",
        "Online Bank Transfer" => "direct banking",
        "ClickandBuy" => "other",
        "BillPay Rechnung" => "non klarna credit",
        "BillPay Lastschrift" => "direct banking",
        "BillPay Ratenkauf" => "non klarna credit",
        "BillPay PayLater Ratenkauf" => "non klarna credit"
    ];

    /**
     * @var PluginConfig
     */
    private $data;

    /**
     * @var PluginInterface
     */
    private $plugin;

    /**
     * Load Plugin Config.
     *
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        $this->data = $plugin->getConfig();
        $this->plugin = $plugin;
    }

    /**
     * Get the plugin version number.
     *
     * @return string
     */
    public function getPluginVersion() : string
    {
        return (string) $this->plugin->getMeta()->getVersion();
    }

    /**
     * Get the internal plugin id
     *
     * @return integer
     */
    public function getPluginId() : int
    {
        return (int) $this->plugin->getID();
    }

    /**
     * Get the full path for the custom emd file.
     *
     * @return string
     */
    public function getCustomEmdFile() : string
    {
        return $this->plugin->getPaths()->getBasePath() . self::CUSTOM_EMD_FILE;
    }

    /**
     * Get the url to an order in the klarna order portal.
     *
     * @param string $orderId
     * @param string $serverMode
     * @return string
     */
    public function getPortalOrderUrl(string $orderId, string $serverMode) : string
    {
        $portalUrl = self::SANDBOX_MODE === $serverMode ? self::PORTAL_TEST_URL : self::PORTAL_URL;
        list($merchant) = explode('_', $this->get(self::KEY_MERCHANT_ID) ?? '');
        return sprintf('https://%s/orders/merchants/%s/orders/%s', $portalUrl, $merchant, $orderId);
    }

    /**
     * Get a plugin config option
     *
     * @param string $key  Config Key
     * @param mixed $value Default value if key is not found
     * @return mixed
     */
    public function get(string $key, $value = null)
    {
        // Default
        if (!$this->has($key)) {
            return $value;
        }
        
        // Get Option
        $option = $this->data->getOption($key);
        return $option->value;
    }

    /**
     * Check whether a config key exists and is set.
     *
     * @param string $key
     * @return boolean
     */
    public function has(string $key) : bool
    {
        $option = $this->data->getOption($key);

        return !is_null($option) && property_exists($option, 'value') && isset($option->value) && $option->value != '';
    }
}
