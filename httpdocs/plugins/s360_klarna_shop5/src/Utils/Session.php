<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Utils;

/**
 * Handle Session usage.
 *
 * @package Plugin\s360_klarna_shop5\src\Utils
 */
class Session
{
    /**
     * Defaul session key for klarna.
     */
    public const SESSION_KEY = 's360_klarna';

    /**
     * Get all values for the klarna session.
     *
     * @return null|array
     */
    public function all() : ?array
    {
        return $_SESSION[self::SESSION_KEY] ?? null;
    }

    /**
     * Get the value for a klarna session key.
     *
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        return $_SESSION[self::SESSION_KEY][$key] ?? null;
    }

    /**
     * Set the value for a klarna session key.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set(string $key, $value) : void
    {
        $_SESSION[self::SESSION_KEY][$key] = $value;
    }

    /**
     * Get all values for the klarna session.
     *
     * @return array
     */

    public function allJtl() : array
    {
        return $_SESSION;
    }

    /**
     * Get the value for a JTL session key.
     *
     * @param string $key
     * @return mixed|null
     */
    public function getJtl(string $key)
    {
        return $_SESSION[$key] ?? null;
    }

    /**
     * Set the value for a JTL session key.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setJtl(string $key, $value) : void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Clear a key of the klarna session.
     *
     * @param string $key
     * @return bool
     */
    public function clear($key) : bool
    {
        if (!isset($_SESSION[self::SESSION_KEY])) {
            return false;
        }

        if (array_key_exists($key, $_SESSION[self::SESSION_KEY])) {
            unset($_SESSION[self::SESSION_KEY][$key]);
            return true;
        }

        return false;
    }

    /**
     * Clear all payment data.
     *
     * @return void
     */
    public function clearPaymentdata() : void
    {
        unset($_SESSION[self::SESSION_KEY]);
    }
}
