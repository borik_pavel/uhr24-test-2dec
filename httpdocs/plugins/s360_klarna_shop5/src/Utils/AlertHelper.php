<?php declare(strict_types = 1);

namespace Plugin\s360_klarna_shop5\src\Utils;

use JTL\Alert\Alert;
use JTL\Services\JTL\AlertServiceInterface;
use JTL\Services\JTL\LinkServiceInterface;
use Plugin\s360_klarna_shop5\src\Utils\Translation;

/**
 * Little Helper class for sending alerts.
 *
 * @package Plugin\s360_klarna_shop5\src\Utils
 */
class AlertHelper
{
    /**
     * @var AlertServiceInterface
     */
    protected $alert;

    /**
     * @var LinkServiceInterface
     */
    protected $url;

    /**
     * @var Translation
     */
    protected $translation;

    /**
     * @param AlertServiceInterface $alert
     * @param LinkServiceInterface $url
     * @param Translation $translation
     */
    public function __construct(AlertServiceInterface $alert, LinkServiceInterface $url, Translation $translation)
    {
        $this->alert = $alert;
        $this->translation = $translation;
        $this->url = $url;
    }
    
    /**
     * Send a flash alert with a possible redirect.
     *
     * @SuppressWarnings(PHPMD.ExitExpression)
     * @param string $type           Type of alert
     * @param string $translationKey Translation Key for alert message
     * @param string $context        Alert Context
     * @param string|null $redirect  Redirect filename
     * @return void
     */
    public function flash(string $type, string $translationKey, string $context, string $redirect = null) : void
    {
        $options = ['dismissable' => true, 'saveInSession' => true];
        $this->alert->addAlert($type, $this->translation->get($translationKey), $context, $options);

        if ($redirect) {
            header('Location: ' . $this->url->getStaticRoute($redirect));
            exit;
        }
    }

    /**
     * Shorthand method to flash error messages.
     *
     * @param string $message
     * @param string|null $redirect
     * @return void
     */
    public function error(string $message, ?string $redirect = null) : void
    {
        $this->flash(Alert::TYPE_DANGER, $message, 'klarnaError', $redirect);
    }
}
