{include file="{$s360_klarna_options.admin_snippet_path}header.tpl"}

<div class="klarna-admin-content">
    <div class="row">
        <div class="col-xs-12 col-12">
            <div class="alert alert-info">
                {__('On-Site-Messaging ist eine Lösung, die den Endverbraucher vor dem Kauf darauf hinweist, dass er zum Kauf bei Klarna berechtigt sein kann.')}
                {__('Das Messaging kann dabei von allgemeinen Bannern zur Förderung Ihrer Partnerschaft mit Klarna und der Verfügbarkeit von Finanzmitteln bis zu personalisierten Kreditaktionen auf Produkt- oder Warenkorbseiten reichen.')}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xs-12">
            <h3 class="pull-left">{__('Placements')}</h3>
            <button type="button" class="btn btn-primary pull-right" onclick="window.klarnaOSM.create()">{__('Hinzufügen')}</button>
        </div>

        <div class="col-12 col-xs-12">
            <hr>
        </div>

        <div class="col-12 col-xs-12">
            <div class="table-responsive">
                <table class="list table table-striped">
                    <thead>
                    <tr>
                        <th class="tleft">{__('Sprache / Land')}</th>
                        <th class="tleft">{__('Währung')}</th>
                        <th class="tleft">{__('Aktiv')}</th>
                        <th class="tleft">{__('Typ')}</th>
                        <th class="tleft">{__('Positionen')}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="klarna-placements">
                        <tr class="klarna-on-ajax-loading">
                            <td class="text-center" colspan="5">
                                <i class="fa fas fa-2x fa-spinner fa-pulse"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="klarna-osm-detail-modal" data-backdrop="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{__('Details')}</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="window.klarnaOSM.save()">{__('Speichern')}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{__('Fenster schließen')}</button>
            </div>
        </div>
    </div>
</div>
