<script type="text/javascript">
    window.klarnaAdminAjaxUrl = '{$s360_klarna_options.admin_url}&isAjax=1';
    window._KLARNA_SNIPPETS_ = {$klarna_snippets};
</script>
<script src="{$s360_klarna_options.admin_template_url}js/jquery-confirm.min.js" defer="defer"></script>
<script src="{$s360_klarna_options.admin_template_url}js/admin.js"></script>
<script src="{$s360_klarna_options.admin_template_url}js/klarna.js"></script>
<script src="{$s360_klarna_options.admin_template_url}js/ordermanagement.js"></script>
<script src="{$s360_klarna_options.admin_template_url}js/osm.js"></script>