{strip}
    <tr class="klarna-placement-item" data-id="{$placement->id}">
        <td class="klarna-placement-table-column klarna-shop-placement-country">{$placement->country|strtoupper}</td>
        <td class="klarna-placement-table-column klarna-shop-placement-currency">{$placement->currency}</td>
        <td class="klarna-placement-table-column klarna-shop-placement-active">
            <i class="fa fas fa-{if $placement->active}check{else}ban{/if}"></i>
        </td>
        <td class="klarna-placement-table-column klarna-shop-placement-type">
            {if $placement->placement_type === 'credit-promotions'}{__('Kreditförderung')}{/if}
            {if $placement->placement_type === 'top-strip'}{__('oberer Streifen')}{/if}
            {if $placement->placement_type === 'banner'}{__('Banner')}{/if}
        </td>
        <td class="klarna-placement-table-column klarna-shop-placement-position">{$placement->localizedPositions()}</td>
        <td class="klarna-placement-table-column klarna-placement-actions">
            <div class="input-group">
                <div class="btn-group input-group-btn">
                    <button type="button" class="btn btn-xs btn-default" title="{__('Bearbeiten')}" onclick="window.klarnaOSM.getDetails('{$placement->id}');">
                        <i class="fa fas fa-pen fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" title="{__('Löschen')}" onclick="window.klarnaOSM.delete('{$placement->id}');">
                        <i class="fa fas fa-times fa-ban" aria-hidden="true"></i>
                    </button> 
                </div>
            </div>
        </td>
    </tr>
{/strip}