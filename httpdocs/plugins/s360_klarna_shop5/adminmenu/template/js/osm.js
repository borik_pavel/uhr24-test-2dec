;(function ($, window, document, undefined) {
    /**
     * Display the placement view
     * @this {OSM}
     * @param {Object} data Data Object with placement details
     * @param {Object} data.placement The placement details to display
     * @param {String} data.placement.view The placement detail view
     */
    var _getDetailsCallback = function (data) {
        if (data.placement && data.placement.view) {
            var $body = this.$detailsContainer.find('.modal-body');
            var $container = this.$detailsContainer;
            var show = function () {
                $body.html(data.placement.view);
                $container.modal('show');
            };

            if ($container.hasClass('modal-loaded')) {
                $container.modal('hide');

                if (!$container.hasClass('is_hidden')) {
                    $container.one('hidden.bs.modal', show);
                } else {
                    show();
                }
            } else {
                show();
                $container.addClass('modal-loaded');
            }

            window.KlarnaOnsiteService = window.KlarnaOnsiteService || [];
            window.KlarnaOnsiteService.push({ eventName: 'refresh-placements' });
        }
    };

    /**
     * Add Purchase Amount attribute to placement tag if needed.
     * @param {String} placement
     */
    var addPurchaseAmount = function(placement) {
        var pattern = /<klarna-placement/gim;
        var replace = '<klarna-placement data-purchase_amount="123456"';

        if (placement.indexOf('data-purchase_amount=') > -1) {
            pattern = /data-purchase_amount="(.*)"/gim;
            replace = 'data-purchase_amount="123456"';
        }

        return placement.replace(pattern, replace);
    };

    /**
     * Handles the OSM in backend
     * @class OSM
     * @param {KlarnaAdmin} admin Klarna Admin Class
     */
    var OSM = function (admin) {
        this.admin = admin;
        this.$detailsContainer = $('#klarna-osm-detail-modal');
        this.$entryContainer = $('.klarna-placements');
        this.onDetailPage = false;

        /**
         * Modal instantly sets focus to the bs modal if focus is given to an element that isn't inside it
         * -> disable it
         */
        var self = this;
        this.$detailsContainer.on('shown.bs.modal', function () {
            $(document).off('focusin.modal'); // Bootstrap 3
            $.fn.modal.Constructor.prototype._enforceFocus = function () { }; // Bootstrap 4
            self.onDetailPage = true;
            self.$detailsContainer.removeClass('is_hidden');

            if (!self.$detailsContainer.find('.activate-osm-cb').is(':checked')) {
                self.$detailsContainer.find('.activate-osm-link').hide();
            }
        });

        this.$detailsContainer.on('hidden.bs.modal', function () {
            self.onDetailPage = false;
            self.$detailsContainer.addClass('is_hidden');
        });

        this.$detailsContainer.on('change', '.activate-osm-cb', function(e) {
            if ($(e.target).is(':checked')) {
                self.$detailsContainer.find('.activate-osm-link').fadeIn();
                return;
            }

            self.$detailsContainer.find('.activate-osm-link').fadeOut();
        });

        /**
         * Preview
         */
        this.$detailsContainer.on('change keyup', function(e) {
            var $target = $(e.target);

            if ($target.attr('name') == 'script' || $target.attr('name') == 'placement_tag') {
                try {
                    var placement = addPurchaseAmount(self.$detailsContainer.find('#placement_tag').val().toString());

                    self.$detailsContainer.find('#placement_preview > .preview').html(
                        self.$detailsContainer.find('#script').val() + ' ' + placement
                    );

                    window.KlarnaOnsiteService = window.KlarnaOnsiteService || [];
                    window.KlarnaOnsiteService.push({ eventName: 'refresh-placements' });
                } catch(exc) {
                    console.log(exc);
                }
            }
        });

        /**
         * Load Entries
         */
        this.loadEntries = function() {
            this.admin.doAjaxCall('OnSiteMessaging:loadEntries', {}, function(data) {
                if (data && data.placements) {
                    self.$entryContainer.html('');
                    data.placements.forEach(function (placement) {
                        self.$entryContainer.append(placement.view);
                    });
                }
            });
        };

        /**
         * Create Form
         */
        this.create = function () {
            this.admin.doAjaxCall('OnSiteMessaging:create', {}, _getDetailsCallback.bind(this));
        };

        /**
         * Get Details
         * @param {String} id
         */
        this.getDetails = function(id) {
            this.admin.doAjaxCall('OnSiteMessaging:getDetails', { id: id }, _getDetailsCallback.bind(this));
        };

        /**
         * Save Placement
         */
        this.save = function() {
            this.admin.doAjaxCall('OnSiteMessaging:save', this.$detailsContainer.find('form').serialize(), function(res) {
                self.loadEntries();
                self.$detailsContainer.modal('hide');
                self.admin.renderAlert(res.message, res.result);
            });
        };

        /**
         * Delete a placement if possible.
         * @param {String} id
        */
        this.delete = function (id) {
            var self = this;

            this.admin.doAjaxCall('OnSiteMessaging:delete', { id: id }, function (res) {
                self.admin.renderAlert(res.message, res.result);
                self.loadEntries();
            });
        };
    };

    // Init OSM
    $(document).ready(function () {
        window.klarnaOSM = new OSM(window.klarnaAdmin);
        window.klarnaOSM.loadEntries();
    });
})(jQuery, window, document);