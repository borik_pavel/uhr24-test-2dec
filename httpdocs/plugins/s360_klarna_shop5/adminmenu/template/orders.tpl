{include file="{$s360_klarna_options.admin_snippet_path}styles.tpl"}
{include file="{$s360_klarna_options.admin_snippet_path}header.tpl"}

<div class="klarna-admin-content">
    <div class="row">
        <div class="col-12 col-xs-12">
            <h3>{__('Bestellungen')}</h3>
        </div>
        <div class="col-11 col-xs-11 col-sm-6">
            <div class="klarna-search">
                <form class="form" onsubmit="window.klarnaOrderManagement.search($(this).find('[name=searchValue]').val());return false;">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" autocomplete="off" placeholder="{__('Bestellnummer / Klarna Bestell-ID')}" name="searchValue"/>
                        <div class="input-group-append input-group-btn">
                            <button class="btn btn-success" type="submit">{__('Suchen')}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-1 col-xs-1 col-sm-2">
            <div class="klarna-on-ajax-loading">
                <div class="text-center">
                    <i class="fa fas fa-spinner fa-pulse"></i>
                </div>
            </div>
        </div>
        <div class="col-12 col-xs-12 col-sm-4">
            <div class="klarna-pagination">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" onclick="window.klarnaOrderManagement.firstPage();">
                        <i class="fa fas fa-fast-backward" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-default" onclick="window.klarnaOrderManagement.prevPage();">
                        <i class="fa fas fa-backward" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-default disabled klarna-current-page-indicator" disabled="disabled">1</button>
                    <button type="button" class="btn btn-default" onclick="window.klarnaOrderManagement.nextPage();">
                        <i class="fa fas fa-forward" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-12 col-xs-12">
        <hr>
        </div>

        <div class="col-12 col-xs-12">
            <div class="table-responsive">
                <table class="list table table-striped">
                    <thead>
                    <tr>
                        <th class="tleft">{__('Bestellnummer')}</th>
                        <th class="tleft">{__('JTL Status')}</th>
                        <th class="tleft">{__('Bestell-ID')}</th>
                        <th class="tleft">{__('Klarna Status')}</th>
                        <th class="tleft">{__('Betrugsstatus')}</th>
                        <th class="tleft">{__('Bezahlmethode')}</th>
                        <th class="tleft">{__('Bezahlmethode Typ')}</th>
                        <th class="tleft">{__('Server')}</th>
                        {* <th class="tleft">Aktiviert</th> *}
                        <th class="tright">{__('Bestelldatum')}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="klarna-orders">
                        <tr class="klarna-on-ajax-loading">
                            <td class="text-center" colspan="11">
                                <i class="fa fas fa-2x fa-spinner fa-pulse"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="klarna-order-detail-modal" data-backdrop="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{sprintf(__('Details zur Bestellung <em>%s</em>'), '{orderId}')}</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{__('Fenster schließen')}</button>
            </div>
        </div>
    </div>
</div>

{include file="{$s360_klarna_options.admin_snippet_path}footer.tpl"}