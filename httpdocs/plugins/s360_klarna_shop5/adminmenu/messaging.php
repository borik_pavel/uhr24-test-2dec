<?php declare(strict_types = 1);

use JTL\Helpers\Request;
use JTL\Shop;
use Plugin\s360_klarna_shop5\src\Controllers\OnSiteMessagingController;
use Plugin\s360_klarna_shop5\src\Messaging\Model;

// check for ajax requests for OSM
if (Request::isAjaxRequest() && Request::getVar('controller') == 'OnSiteMessaging') {
    try {
        $controller = Shop::Container()->get(OnSiteMessagingController::class);
        $controller->handleAjax(Shop::Container()->get(Model::class));
        $controller->displayResponse();
    } catch (Exception $ex) {
        $controller->displayErrorResponse($ex->getMessage());
    }
}

// Execute the handle orders action.
try {
    $controller = Shop::Container()->get(OnSiteMessagingController::class);
    $controller->preDispatch();
    $controller->overviewAction();
    $controller->postDispatch();
} catch (Exception | SmartyException $exception) {
    print_r('Exception: ' . $exception->getMessage());
}
