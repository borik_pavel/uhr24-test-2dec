<?php declare(strict_types = 1);

use JTL\Shop;
use Plugin\s360_klarna_shop5\src\Controllers\AdminController;
use Plugin\s360_klarna_shop5\src\Controllers\AjaxOrderManagementController;
use Plugin\s360_klarna_shop5\src\Payments\Model;
use JTL\Helpers\Request;

// check for ajax requests in for order management
if (Request::isAjaxRequest() && Request::getVar('controller') === 'OrderManagement') {
    try {
        $controller = Shop::Container()->get(AjaxOrderManagementController::class);
        $controller->preDispatch();
        $controller->handleAjax(Shop::Container()->get(Model::class));
        $controller->postDispatch();

        $controller->displayResponse();
    } catch (Exception $ex) {
        $controller->displayErrorResponse($ex->getMessage());
    }
}

// Execute the handle orders action.
try {
    // $controller = Shop::Container()->get(\Plugin\s360_klarna_shop5\src\Controllers\SyncController::class);
    // $controller->handleOrderUpdate(new \JTL\Checkout\Bestellung(6), Shop::Container()->get(\JTL\Mail\Mailer::class), new \JTL\Mail\Mail\Mail());

    $controller = Shop::Container()->get(AdminController::class);
    $controller->preDispatch();
    $controller->handleOrders();
    $controller->postDispatch();
} catch (Exception | SmartyException $exception) {
    print_r('Exception: ' . $exception->getMessage());
}
