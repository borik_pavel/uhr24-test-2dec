{if !$shopauskunftIsDown}
    {$xc_label = $shopauskunftId}
    {$seed = "$xc_label|{$Bestellung->cBestellNr}"}
    {$token = base64_encode(openssl_encrypt($seed,'aes128','c2hvcGF1c2t1bmZ0', 0, 'aGFlbmRsZXJidW5k'))}
{literal}
    <script {/literal}{if $settingsGeneral->consentManagerRule != ''} data-cmp-vendor="{$settingsGeneral->consentManagerRule}" class="cmplazyload"{/if}{literal}>
        {/literal}
        var fetched_data = {ldelim}
            'shop_name': '{$shopName}',
            'email': '{$Bestellung->oKunde->cMail}',
            'shop_type': '5',
            'order_value': '{$Bestellung->fWarensumme}',
            'shipping_value': '{$Bestellung->fVersand}',
            'language': '{Shop::Lang()->cISOSprache}',
            'url': '{$smarty.server.SERVER_NAME}',
            'token': '{$token}'
            {rdelim}
    </script>
{literal}
    <script type="text/javascript"
            src="https://rba.shopauskunft.de/js/sa_widget.js" {/literal}{if $settingsGeneral->consentManagerRule != ''} data-cmp-vendor="{$settingsGeneral->consentManagerRule}" class="cmplazyload"{/if}{literal}
            id="sa_widget" async></script>
{/literal}
{/if}