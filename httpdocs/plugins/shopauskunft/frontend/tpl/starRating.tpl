{assign var="ratingArr" value=[1,2,3,4,5]}
{if $rating && $rating > 0}
{foreach from=$ratingArr item=index key=key}
    {if $rating >= $index}
        <span class="starEmpty fa fa-star">
            <span class="fa fa-star starPartiallyFilled" style="width: {1*100}%;"></span>
        </span>
    {elseif $rating >= ($index-1)}
        <span class="starEmpty fa fa-star">
            <span class="fa fa-star starPartiallyFilled" style="width: {100*($rating - $index + 1)}%;"></span>
        </span>
    {else}
        <span class="starEmpty fa fa-star">
            <span class="fa fa-star starPartiallyFilled" style="width: 0%;"></span>
        </span>
    {/if}
{/foreach}
{else}
{foreach from=$ratingArr item=fillStand}
    <span class="starEmpty fa fa-star" style="text-shadow: 0 0 3px">
        <span class="fa fa-star starPartiallyFilled" style="width: 100%; color:#fff;"></span>
    </span>
{/foreach}
{/if}