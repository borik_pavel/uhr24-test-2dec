{literal}
<style>
    #sa_widget_knm {
        margin-{/literal}{$settingsMd->vertical_key}{literal}: {/literal}{$settingsMd->vertical_value}{literal};
        margin-{/literal}{$settingsMd->horizontal_key}{literal}: {/literal}{$settingsMd->horizontal_value}{literal};
    }
    @media (max-width: 992px) {
        #sa_widget_knm {
        margin-{/literal}{$settingsSm->vertical_key}{literal}: {/literal}{$settingsSm->vertical_value}{literal};
        margin-{/literal}{$settingsSm->horizontal_key}{literal}: {/literal}{$settingsSm->horizontal_value}{literal};
        }
    }

</style>
{/literal}
<div class="{$settingsMd->position_class} {$settingsSm->position_class} sa_widget_zIndex" >
    <div class="{$settingsMd->position_class} {$settingsSm->position_class} sa_widget_zIndex" style="position: absolute">
        <div id="sa_widget_knm" class="{$settingsMd->position_class} {$settingsSm->position_class} sa_widget_hidden-xs">
            <div id="ocd" class="ocd sa_box_shadow" style="text-align: center;background-color: #fff;padding: 8px">
                <div style="position: relative; min-height: 65px; display: flex; align-items: center; flex-direction: column;">
                    <i class="fa fa-spinner fa-pulse" style=" position: absolute; margin-top: 15px;"></i>
                    <img style="z-index: 1; max-width: 64px; margin-left: auto; margin-right: auto;"
                         src="{$shopauskunt_logo_path}" alt="Shopauskunft_logo" title="Shopauskunft"/>
                </div>
                <span id="closing_cross_small" class="fa fa-times closeBtn"
                      style="position: absolute; top: 4px;right: 4px;"></span>
                <div>
                    <p>Shopauskunft</p>
                </div>
                {if $reviewCount > 0}
                    <div>
                        {include file="./starRating.tpl" rating=$shopauskunft_overAllRating}
                    </div>
                    <div>
                        <span>{$shopauskunft_overAllRating} / 5,00</span><br/>
                        <span style="font-size: 10px">{$reviewCount} Bewertungen</span>
                    </div>
                {else}
                    <span>keine</span>
                    <br>
                    <span>Bewertungen</span>
                {/if}
            </div>
        </div>
        <div id="sa_widget_bar_extended" class="hide_module sa_box_shadow sa_widget_hidden-xs">
            <div style="width: 100%; max-width: 320px; background-color: #fff;"
                 class="sa_box_shadow {$settingsMd->position_class} {$settingsSm->position_class}">
                <a target="_blank" href="{$shopauskunft_deep_link}" title="{$shopName}"
                   style="color: #000;text-decoration: none;">
                    <div class="col-xs-12"
                         style="display: flex; text-align: center; font-size: 18px; margin-top: 20px;position: relative">
                        <div class="shopauskunftLogoBox">
                            <div class="shopauskunftLogo" style="margin: 0 6px 0 0;">
                                <img src="{$shopauskunt_logo_path}" style=" width: 100%"
                                     alt="{$statisfiedCustomerPercentage}% zufriedene Kunden ({$reviewCount} Bewertungen)"/>
                            </div>
                        </div>
                        <div class="sa-head-text" style="margin: 0 0 0 8px; display: flex; flex-direction: column;">
                            <span style="margin-right: auto; font-size: 18px">Shopauskunft</span>
                            {if $reviewCount > 0}
                                <span style="margin-right: auto; font-size: 14px">{$shopauskunft_overAllRating} / 5,00 ({$reviewCount} Bewertungen)</span>
                            {else}
                                <span style="margin-right: auto; font-size: 14px">keine Bewertungen</span>
                            {/if}
                        </div>
                    </div>
                </a>
                <span id="closing_cross" class="fa fa-times closeBtn"
                      style="position: absolute; top: 15px;right: 15px; font-size: 18px;"></span>
                <div>
                    <div class="col-xs-12 sahrbox">
                        <hr class="sahr"/>
                    </div>
                    {foreach from=$detailedReview item=review key=key name=sahr}
                        {if $key == 0 || ($key + 1)%2 == 0}
                            <div style="padding: 0 15px;">
                        {/if}
                        <div class="col-xs-6{if $key%2  == 0} vr{/if}" style="cursor: default;">
                            {include file='./single_star_evaluation.tpl'
                            iconClass=$review['iconClass']
                            rating=$review['rating']
                            evaluationTitle=$review['name']
                            }
                        </div>
                        {if $key%2 == 0}
                            </div>
                            {if not $smarty.foreach.sahr.last}
                                <div class="col-xs-12 sahrbox">
                                    <hr class="sahr"/>
                                </div>
                            {else}
                                <div class="col-xs-12 sahrbox">
                                    <hr class="sahr"/>
                                </div>
                                {include file="./textRatings.tpl" ratingList=$ratingList}
                                <div id="safoot"></div>
                            {/if}
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>

{if $settingsGeneral->jQuery == "1"}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
{/if}
{literal}
<script {/literal}{if $settingsGeneral->consentManagerRule != ''} data-cmp-vendor="{$settingsGeneral->consentManagerRule}" class="cmplazyload"{/if}{literal}>
    function knmSaWidgetSetCookie() {
        let now = new Date();
        let time = now.getTime();
        let expireTime = time + {/literal}{$sessionLifetime * 1000}{literal};
        now.setTime(expireTime);
        document.cookie = 'hideKnmSaWidget=true;expires=' + now.toGMTString() + ';';
    }


</script>

<script {/literal}{if $settingsGeneral->consentManagerRule != ''}data-cmp-vendor="{$settingsGeneral->consentManagerRule}" class="cmplazyload"{/if}{literal}>
    let specifiedElement = document.getElementById('sa_widget_bar_extended');
    let smallWidget = document.getElementById('sa_widget_knm');
    let bigWidgetClosingCross = document.getElementById('closing_cross');
    let smallWidgetClosingCross = document.getElementById('closing_cross_small');
    let hideCompeletly = false;
    let animationspeed = 750;

    function fadeEleOut(eleId) {
        $(eleId).fadeOut(animationspeed, function () {
            $(eleId).addClass("hide_module");
        });
    }

    function fadeEleIn(eleId) {
        $(eleId).fadeIn(animationspeed, function () {
            $(eleId).removeClass("hide_module");
        });
    }

    document.addEventListener('scroll', function () {
        fadeEleOut("#sa_widget_bar_extended");
        if (!hideCompeletly) {
            fadeEleIn("#sa_widget_knm");
        }
    });

    document.addEventListener('click', function (event) {
        let isClickInside = specifiedElement.contains(event.target);
        let isClickInsideOtherElement = smallWidget.contains(event.target);
        let bigCrossIsClicked = bigWidgetClosingCross.contains(event.target);
        let smallCrossIsClicked = smallWidgetClosingCross.contains(event.target);

        if (!hideCompeletly) {
            if (smallCrossIsClicked) {
                fadeEleOut("#sa_widget_knm");
                $("#sa_widget_bar_extended").addClass("hide_module");
            }
            if (bigCrossIsClicked) {
                fadeEleIn("#sa_widget_knm");
                fadeEleOut("#sa_widget_bar_extended");
            }
            if (!isClickInside && !isClickInsideOtherElement) {
                fadeEleIn("#sa_widget_knm");
                fadeEleOut("#sa_widget_bar_extended");
            }
            if (isClickInsideOtherElement && !smallCrossIsClicked) {
                fadeEleIn("#sa_widget_bar_extended");
                fadeEleOut("#sa_widget_knm");
            }
            if (smallCrossIsClicked) {
                fadeEleOut("#sa_widget_knm");
                hideCompeletly = true;
                if (typeof knmSaWidgetSetCookie !== "undefined") {
                    knmSaWidgetSetCookie();
                }
            }
        }
    });
</script>
{/literal}