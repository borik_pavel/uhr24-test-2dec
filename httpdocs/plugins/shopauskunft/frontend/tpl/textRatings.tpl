<div id="sa-ratings" class="sa-ratings col-xs-12">
    {foreach from=$ratingList item=item key=key name="sAaWidgetRatingText"}
    <div class="sa-rating" aria-hidden="{!$smarty.foreach.sAaWidgetRatingText.first}" style="height:100%; width: 100%;">
        {if isset($item['text'])}
            <div class="col-xs-12" style="display: flex;">
                <i class="fa fa-user" style="margin: auto 8px; margin-left: 0;font-size: 32px"></i>
                <div style="text-align: left;">
                    <span style="font-size: 10px">{$item['evaluator']['username']}</span><br/>
                    <span>{include file="./starRating.tpl" rating=$item['rating_stars']}</span>
                </div>
                <span style="font-size: 10px;margin-left: auto">{$item['date']|date_format: "%d.%m.%Y"}</span>
            </div>
            <div class="carousel-item-txt col-xs-12">
                {$item['text']}
            </div>
            <div class="col-xs-12" style="text-align: right;font-size: 10px;padding: 0; margin-top: auto;">
                <a target="_blank" style="color: #435777" href="{$shopauskunft_deep_link}">zum {$shopName} Bewertungsprofil</a>
            </div>
            </div>
        {/if}
    {/foreach}
</div>
<script type="text/javascript">
    window.addEventListener("DOMContentLoaded", function(e) {
        let stage = document.getElementById("sa-ratings");
        let knm_fadeComplete = function(e) { stage.appendChild(arr[0]); };
        let arr = stage.getElementsByClassName('sa-rating');
        for(let i=0; i < arr.length; i++) {
            arr[i].addEventListener("animationend", knm_fadeComplete, false);
        }
    }, false);
</script>