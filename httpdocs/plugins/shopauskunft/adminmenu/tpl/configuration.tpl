<div id="knm_sa_alert">
    {if isset($tokenIsValid)}
        {if $tokenIsValid == true}
            {knm_alert type="success"}{knmLang key="code is valid"}{/knm_alert}
        {else}
            {knm_alert type="danger"}{knmLang key="Code is invalid"}{/knm_alert}
        {/if}
    {/if}
    {knm_alertSave show={isset($savedSuccessfully)} showSuccess=$savedSuccessfully}{/knm_alertSave}
</div>
<form method="post" action="{$pluginTabBasepath}{$tabId}" class="navbar-form">
    <input type="hidden" name="update_plugin" value="true"/>
    {include $fields->switch id="onOff" name="general[onOff]" label="{knmLang key="plugin status"}" value=1 checked=$settingsGeneral->onOff}

    {knm_setting label="{knmLang key="token"}" description="{knmLang key='where do i find my shopauskunft token'}" class="w-100"}
        <div class="d-flex">
            <input type="text" class="form-control" id="token" name="general[token]" placeholder="{knmLang key="token"}"
                   value="{$settingsGeneral->token}" required/>
            <div class="invalid-tooltip">{knmLang key="you must enter a token"}.</div>
            <div class="col ml-3 p-0">
                <div id="token_loader" class="loading-animation" style="display: none"></div>
                {knm_button id="check_token" icon="fa fa-check-circle" disabled={isset($settingsGeneral->token) && $settingsGeneral->token == ''} }
                {knmLang key="Token"} {knmLang key="check"}
                {/knm_button}
            </div>
        </div>
    {/knm_setting}
    {include $fields->switch id="askForFeedback" name="general[askForFeedback]" value=1 checked="{$settingsGeneral->askForFeedback}" label="{knmLang key="ask customer for feedback"}"}

    {knm_select label="{knmLang key="position"}: {knmLang key="smartphone-view"}" id="position_xs" name="xs[position]"}
        <option value="1" {if $settingsXs->position == 1} selected="" {/if}>{knmLang key="over header"}</option>
        <option value="0" {if $settingsXs->position == 0} selected="" {/if}>{knmLang key="hide"}</option>
    {/knm_select}

    {include file="./margin_settings.tpl" headline="{knmLang key="desktop view"}" layout="md" settings=$settingsMd}
    {include file="./margin_settings.tpl" headline="{knmLang key="tablet view"}" layout="sm" settings=$settingsSm}

    {knm_accordion title="{knmLang key="advanced settings"}" id="show_advanced"}
        {include $fields->switch label="{knmLang key="include jquery"}" name="general[jQuery]" id="jQuery" checked="{if $settingsGeneral->jQuery == "1"}1{/if}" description="{knmLang key="include jquery note"}"}
        {include $fields->text textRight=true label="{knmLang key="consent manager rule"}" placeholder="{knmLang key="consent manager rule"}" id="consentManagerRule" name="general[consentManagerRule]" value="{$settingsGeneral->consentManagerRule}" description="{knmLang key="consent manager rule note"}"}
    {/knm_accordion}
    <div class="d-flex w-100">
        {knm_saveButton class="ml-auto" type="submit"}{/knm_saveButton}
    </div>
</form>
{literal}
<script>
    let knmSa_alert_container = $('#knm_sa_alert');
    let knmSa_button_loader = $('#token_loader');
    let knmSa_token_input = $('#token');
    let knmSa_check_token_button = $("#check_token");

    $(document).ready(function () {
        knmSa_token_input.on('keyup', event => knmSa_check_token_button.prop('disabled', event.target.value === ""));
        knmSa_check_token_button.on('click', () => $.ajax({
            url: "{/literal}{$ajaxUrl}{literal}",
            type: 'POST',
            data: {
                plugin: "{/literal}{$currentPluginId}{literal}",
                token: knmSa_token_input.val(),
            },
            success: data => {
                data = JSON.parse(data);
                let html = data && data.status && data.status === 1 ? `{/literal}{knm_alert type="success"}{knmLang key="code is valid"}{/knm_alert}{literal}` : `{/literal}{knm_alert type="danger"}{knmLang key="Code is invalid"}{/knm_alert}{literal}`;
                knmSa_alert_container.html(html);
            },
            beforeSend: () => knmSa_button_loader.show(),
            complete: () => [knmSa_button_loader.hide(), knmSa_alert_container.find('div').stop().delay(2500).fadeOut(500)],
            error: data => knmSa_alert_container.html(`{/literal}{knm_alert type="danger"}{knmLang key="Code is invalid"}{/knm_alert}{literal}`)
        }))
    })
</script>
{/literal}
