<?php declare(strict_types=1);

namespace Plugin\jtl_widgets;

use JTL\Widgets\AbstractWidget;
use stdClass;

/**
 * Class UnlockRequestNotifier
 * @package Plugin\jtl_widgets
 */
class UnlockRequestNotifier extends AbstractWidget
{
    /**
     * @inheritDoc
     */
    public function init(): void
    {
        require_once \PFAD_ROOT . \PFAD_ADMIN . \PFAD_INCLUDES . 'freischalten_inc.php';

        $sql          = '';
        $searchSQL    = new stdClass();
        $groups       = [];
        $requestCount = 0;

        $searchSQL->cWhere    = '';
        $group                = new stdClass();
        $group->cGroupName    = __('Customer reviews');
        $group->kRequestCount = \count(\gibBewertungFreischalten($sql, $searchSQL, false));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $searchSQL->cOrder    = ' dZuletztGesucht DESC ';
        $group                = new stdClass();
        $group->cGroupName    = __('Search queries');
        $group->kRequestCount = \count(\gibSuchanfrageFreischalten($sql, $searchSQL, false));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $group                = new stdClass();
        $group->cGroupName    = __('News comments');
        $group->kRequestCount = \count(\gibNewskommentarFreischalten($sql, $searchSQL, false));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $searchSQL->cOrder    = ' tnewsletterempfaenger.dEingetragen DESC ';
        $group                = new stdClass();
        $group->cGroupName    = __('Newsletter recipients');
        $group->kRequestCount = \count(\gibNewsletterEmpfaengerFreischalten($sql, $searchSQL, false));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $this->getSmarty()->assign('groups', $groups)
                          ->assign('requestCount', $requestCount);
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->getSmarty()->fetch(__DIR__ . '/templates/widgetUnlockRequestNotifier.tpl');
    }
}
