<?php declare(strict_types=1);

namespace Plugin\jtl_widgets;

use JTL\DB\ReturnType;
use JTL\Widgets\AbstractWidget;

/**
 * Class LastSearch
 * @package Plugin\jtl_widgets
 */
class LastSearch extends AbstractWidget
{
    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $lastQueries = $this->getDB()->query(
            'SELECT * 
                FROM tsuchanfrage 
                ORDER BY dZuletztGesucht DESC 
                LIMIT 10',
            ReturnType::ARRAY_OF_OBJECTS
        );
        $this->getSmarty()->assign('lastQueries', $lastQueries);
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->getSmarty()->fetch(__DIR__ . '/templates/widgetLastSearch.tpl');
    }
}
