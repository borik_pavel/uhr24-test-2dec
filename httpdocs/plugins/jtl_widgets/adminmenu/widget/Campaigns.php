<?php declare(strict_types=1);

namespace Plugin\jtl_widgets;

use JTL\Helpers\Request;
use JTL\Widgets\AbstractWidget;
use stdClass;

/**
 * Class Campaigns
 * @package Plugin\jtl_widgets
 */
class Campaigns extends AbstractWidget
{
    /**
     * @inheritDoc
     */
    public function init(): void
    {
        require_once \PFAD_ROOT . \PFAD_ADMIN . \PFAD_INCLUDES . 'kampagne_inc.php';
        $_SESSION['Kampagne']                 = new stdClass();
        $_SESSION['Kampagne']->nAnsicht       = 2;
        $_SESSION['Kampagne']->nSort          = 0;
        $_SESSION['Kampagne']->cSort          = 'DESC';
        $_SESSION['Kampagne']->nDetailAnsicht = 2;
        $_SESSION['Kampagne']->cFromDate_arr  = ['nJahr' => \date('Y'), 'nMonat' => \date('n'), 'nTag' => '1'];
        $_SESSION['Kampagne']->cToDate_arr    = ['nJahr' => \date('Y'), 'nMonat' => \date('n'), 'nTag' => \date('j')];
        $_SESSION['Kampagne']->cFromDate      = \date('Y-n-1');
        $_SESSION['Kampagne']->cToDate        = \date('Y-n-j');

        $campaigns           = \holeAlleKampagnen(true, false);
        $campaignDefinitions = \holeAlleKampagnenDefinitionen();
        $first               = \array_keys($campaigns);
        $first               = $first[0];
        $campaignID          = (int)$campaigns[$first]->kKampagne;

        if (isset($_SESSION['jtl_widget_kampagnen']['kKampagne'])
            && $_SESSION['jtl_widget_kampagnen']['kKampagne'] > 0
        ) {
            $campaignID = (int)$_SESSION['jtl_widget_kampagnen']['kKampagne'];
        }
        if (Request::getInt('kKampagne') > 0) {
            $campaignID = Request::getInt('kKampagne');
        }
        $_SESSION['jtl_widget_kampagnen']['kKampagne'] = $campaignID;

        $stats = \holeKampagneDetailStats($campaignID, $campaignDefinitions);

        $this->getSmarty()->assign('kKampagne', $campaignID)
                          ->assign('types', \array_keys($stats))
                          ->assign('campaigns', $campaigns)
                          ->assign('campaignDefinitions', $campaignDefinitions)
                          ->assign('campaignStats', $stats);
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->getSmarty()->fetch(__DIR__ . '/templates/widgetCampaigns.tpl');
    }
}
