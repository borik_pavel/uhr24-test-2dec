<?php declare(strict_types = 1);


namespace Plugin\s360_amazonpay_shop5\lib\Utils;

/**
 * Class Constants
 *
 * Provides constants that are used around the plugin.
 *
 * @package Plugin\s360_amazonpay_shop5\lib\Utils
 */
class Constants {
    public const PAYMENT_METHOD_NAME = 'Amazon Pay';
    public const PLUGIN_ID = 's360_amazonpay_shop5';
    public const COOKIE_ACCESS_TOKEN = 'amazon_Login_accessToken';
    public const DEFAULT_SCOPE = 'profile payments:widget payments:shipping_address payments:billing_address';
    public const MAX_TRANSACTION_TIMEOUT = 1440;
    public const ORDER_ATTRIBUTE_REFERENCE_ID = 'AmazonPay-Referenz';
    public const DEVELOPMENT_MODE_CONSTANT = 'AMAZONPAY_DEVELOPMENT_MODE';

    public const CRON_JOB_TYPE_SYNC = 'lpa_sync_cron';

    public const EVENT_AFTER_SET_SHIPPING_ADDRESS = 'amazon_pay.after_set_shipping_address';
    public const EVENT_AFTER_RESET_SESSION = 'amazon_pay.after_reset_session';
    public const EVENT_AFTER_SET_SHIPPING_PAYMENT_METHOD = 'amazon_pay.after_set_shipping_payment_method';
    public const EVENT_PREPARE_PAYMENT_PROCESS = 'amazon_pay.prepare_payment_process';
}