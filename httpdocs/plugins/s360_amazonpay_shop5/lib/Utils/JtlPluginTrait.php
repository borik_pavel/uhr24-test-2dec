<?php declare(strict_types = 1);


namespace Plugin\s360_amazonpay_shop5\lib\Utils;

use JTL\Plugin\Helper;

/**
 * Class JtlPluginTrait
 *
 * Provides access to the plugin object, independend on the location from where it is used.
 *
 * @package Plugin\s360_amazonpay_shop5\lib\Utils
 */
trait JtlPluginTrait {

    public function loadPlugin() {
        return Helper::getPluginById(Constants::PLUGIN_ID);
    }
}