<?php declare(strict_types = 1);


namespace Plugin\s360_amazonpay_shop5\lib\Controllers\Admin;

use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\s360_amazonpay_shop5\lib\Utils\Config;
use Plugin\s360_amazonpay_shop5\lib\Utils\JtlLinkHelper;
use Plugin\s360_amazonpay_shop5\lib\Utils\JtlLoggerTrait;

/**
 * Class AdminController.
 *
 * @package Plugin\s360_amazonpay_shop5\lib\Controllers
 */
abstract class AdminController {
    use JtlLoggerTrait;

    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @var array $request 
     */
    protected $request;

    /**
     * @var PluginInterface $plugin
     */
    protected $plugin;

    /**
     * Errors to show to the user.
     * @var string[] $errors 
     */
    protected $errors;

    /**
     * Warnings to show to the user.
     * @var string[] $warnings
     */
    protected $warnings;

    /**
     * Normal messages to show to the user.
     * @var string[] $messages 
     */
    protected $messages;

    /**
     * Success messages to show to the user.
     * @var string[] $successes 
     */
    protected $successes;

    /**
     * AdminController constructor.
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin) {
        $this->plugin = $plugin;
        $this->config = Config::getInstance();
        $this->request = $_REQUEST;
        $this->errors = [];
        $this->warnings = [];
        $this->messages = [];
        $this->successes = [];
        $this->prepareGlobalVariables();
    }


    /**
     * Prepares variables needed for every tab.
     */
    protected function prepareGlobalVariables(): void {
        $vars = [];
        $vars['adminTemplatePath'] = $this->plugin->getPaths()->getAdminPath() . 'template/';
        $vars['adminTemplateUrl'] = $this->plugin->getPaths()->getAdminURL() . 'template/';
        $vars['adminUrl'] = JtlLinkHelper::getInstance()->getFullAdminUrl(); // notice that this returns the actual URL that displays the plugin admin area, not an url to its own folder
        $vars['widgetEndpointUrl'] = $this->config->getWidgetEndpointUrl();
        $vars['clientId'] = $this->config->getClientId();
        $vars['isSandbox'] = $this->config->getEnvironment() === Config::ENVIRONMENT_SANDBOX ? 'true' : 'false';
        $vars['pluginVersion'] = (string) $this->plugin->getCurrentVersion();
        Shop::Smarty()->assign('lpaAdminGlobal', $vars);
    }

    /**
     * Finalizes the process by assigning the messages acquired while handling and rendering the template file.
     * @param string $templateFilePath
     * @return string
     * @throws \Exception
     */
    protected function finalize(string $templateFilePath): string {
        Shop::Smarty()->assign('lpaErrors', $this->errors);
        Shop::Smarty()->assign('lpaWarnings', $this->warnings);
        Shop::Smarty()->assign('lpaMessages', $this->messages);
        Shop::Smarty()->assign('lpaSuccesses', $this->successes);
        return Shop::Smarty()->fetch($templateFilePath);
    }

    /**
     * Adds an error to display to the user.
     * @param string $error
     * @param string $postfix
     */
    protected function addError(string $error, string $postfix = ''): void {
        $this->errors[] = __($error) . $postfix;
    }

    /**
     * Adds an error to display to the user.
     * @param string $warning
     */
    protected function addWarning(string $warning): void {
        if (!\in_array($warning, $this->warnings, true)) {
            $this->warnings[] = __($warning);
        }
    }

    /**
     * Adds a message to display to the user.
     * @param string $message
     */
    protected function addMessage(string $message): void {
        if (!\in_array($message, $this->messages, true)) {
            $this->messages[] = __($message);
        }
    }

    /**
     * Adds a message to display to the user.
     * @param string $success
     */
    protected function addSuccess(string $success): void {
        if (!\in_array($success, $this->successes, true)) {
            $this->successes[] = __($success);
        }
    }

    /**
     * Expected to fill Smarty Variables and return the rendered template.
     */
    abstract public function handle(): string;

    /**
     * @return Config
     */
    public function getConfig(): Config {
        return $this->config;
    }

    /**
     * @param Config $config
     * @return AdminController
     */
    public function setConfig(Config $config): AdminController {
        $this->config = $config;
        return $this;
    }

    /**
     * @return array
     */
    public function getRequest(): array {
        return $this->request;
    }

    /**
     * @param array $request
     * @return AdminController
     */
    public function setRequest(array $request): AdminController {
        $this->request = $request;
        return $this;
    }

    /**
     * @return PluginInterface
     */
    public function getPlugin(): PluginInterface {
        return $this->plugin;
    }

    /**
     * @param PluginInterface $plugin
     * @return AdminController
     */
    public function setPlugin(PluginInterface $plugin): AdminController {
        $this->plugin = $plugin;
        return $this;
    }

}