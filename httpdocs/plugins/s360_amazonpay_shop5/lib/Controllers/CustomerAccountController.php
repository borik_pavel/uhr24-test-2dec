<?php declare(strict_types = 1);


namespace Plugin\s360_amazonpay_shop5\lib\Controllers;

use JTL\Campaign;
use JTL\CheckBox;
use JTL\Checkout\Kupon;
use JTL\Customer\AccountController;
use JTL\Customer\Customer;
use JTL\Customer\CustomerAttributes;
use JTL\DB\ReturnType;
use JTL\Helpers\Tax;
use JTL\Language\LanguageHelper;
use JTL\Mail\Mail\Mail;
use JTL\Mail\Mailer;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\s360_amazonpay_shop5\lib\Utils\Database;
use Plugin\s360_amazonpay_shop5\lib\Utils\JtlLoggerTrait;

/**
 * Class CustomerAccountController
 *
 * Controls anything related to account actions.
 *
 * - Mapping
 * - Removing mappings
 * - Logins
 *
 * @package Plugin\s360_amazonpay_shop5\lib\Controllers
 */
class CustomerAccountController {

    use JtlLoggerTrait;

    private $database;

    public function __construct() {
        $this->database = Database::getInstance();
    }

    /**
     * When an account gets deleted, we also delete all account mappings.
     * @param int $customerId
     */
    public function handleAccountDeletion(int $customerId): void {
        $this->database->deleteAccountMappingForJtlCustomerId($customerId);
    }

    /**
     * Creates a customer in the shop with the given data.
     *
     * @param Customer $customer
     * @param $data
     * @param bool $isFullCustomer
     * @return Customer|null that was created
     */
    public function createCustomerAccount(Customer $customer, $data, bool $isFullCustomer): ?Customer {

        $oCheckBox = new CheckBox();
        $conf = Shop::getSettings([CONF_GLOBAL, CONF_KUNDENWERBENKUNDEN]);
        $cart = Frontend::getCart();
        $cart->loescheSpezialPos(C_WARENKORBPOS_TYP_VERSANDPOS)->loescheSpezialPos(C_WARENKORBPOS_TYP_ZAHLUNGSART);

        $customerAttributes = getKundenattribute($data);

        $customerGroupId = $customer->kKundengruppe;
        if ($isFullCustomer) {
            // Check if the new customer was refered
            $oNeukunde = Shop::Container()->getDB()->select(
                'tkundenwerbenkunden',
                'cEmail',
                $customer->cMail,
                'nRegistriert',
                0
            );
            // Note: new customers that were refered may have a different initial customer group
            if (isset($oNeukunde, $oNeukunde->kKundenWerbenKunden, $conf['kundenwerbenkunden']['kwk_kundengruppen'])
                && $oNeukunde->kKundenWerbenKunden > 0
                && (int)$conf['kundenwerbenkunden']['kwk_kundengruppen'] > 0
            ) {
                $customerGroupId = (int)$conf['kundenwerbenkunden']['kwk_kundengruppen'];
            }
        }


        // execute checkbox functionality and log checkbox info
        $oCheckBox->triggerSpecialFunction(CHECKBOX_ORT_REGISTRIERUNG, $customerGroupId, true, $data, array('oKunde' => $customer));
        $oCheckBox->checkLogging(CHECKBOX_ORT_REGISTRIERUNG, $customerGroupId, $data, true);

        $customer->kKundengruppe = $customerGroupId; // overwrite customer group id in case it was changed
        $customer->kSprache = Shop::getLanguage();
        $customer->cAbgeholt = 'N';
        $customer->cSperre = 'N';
        $customer->cAktiv = 'Y'; // may be overriden for full customers
        $customer->angezeigtesLand = LanguageHelper::getCountryCodeByCountryName($customer->cLand);
        $customer->nRegistriert = 0; // may be overridden for full customers
        if ($isFullCustomer) {
            // only for new registered customer, a guest is saved to DB AFTER checkout when the order is saved to the database
            $customer->cAktiv = $conf['global']['global_kundenkonto_aktiv'] === 'A' ? 'N' : 'Y';
            $cPasswortKlartext = $customer->cPasswort;
            $customer->cPasswort = Shop::Container()->getPasswordService()->hash($cPasswortKlartext);
            $customer->dErstellt = 'NOW()';
            $customer->nRegistriert = 1;
            $cLand = $customer->cLand;
            $customer->cPasswortKlartext = $cPasswortKlartext;
            $obj = new \stdClass();
            $obj->tkunde = $customer;

            $mailer = Shop::Container()->get(Mailer::class);
            $mail = new Mail();
            $mailer->send($mail->createFromTemplateID(MAILTEMPLATE_NEUKUNDENREGISTRIERUNG, $obj));

            $customer->cLand = $cLand;
            unset($customer->cPasswortKlartext, $customer->Anrede);

            $customer->kKunde = $customer->insertInDB();
            // Kampagne
            if (isset($_SESSION['Kampagnenbesucher'])) {
                Campaign::setCampaignAction(KAMPAGNE_DEF_ANMELDUNG, $customer->kKunde, 1.0); // Anmeldung
            }
            // Insert Kundenattribute, be sure to add the customer id first
            /** @var CustomerAttributes $customerAttributes */
            $customerAttributes->setCustomerID($customer->kKunde);
            $customerAttributes->save();

            // If global customer account activation is disabled - the calling method has to check if this creation also set the user in the session.
            if ($conf['global']['global_kundenkonto_aktiv'] !== 'A') {
                $_SESSION['Kunde'] = new Customer($customer->kKunde);
                $_SESSION['Kunde']->getCustomerAttributes()->load($customer->kKunde);
            }
            // Guthaben des Neukunden aufstocken insofern er geworben wurde
            if (isset($oNeukunde, $oNeukunde->kKundenWerbenKunden) && $oNeukunde->kKundenWerbenKunden > 0) {
                Shop::Container()->getDB()->queryPrepared(
                    'UPDATE tkunde
                        SET fGuthaben = fGuthaben + :amount
                        WHERE kKunde = :cid',
                    [
                        'cid' => (int)$customer->kKunde,
                        'amount' => (float)$conf['kundenwerbenkunden']['kwk_neukundenguthaben']
                    ],
                    ReturnType::AFFECTED_ROWS
                );
                Shop::Container()->getDB()->update('tkundenwerbenkunden', 'cEmail', $customer->cMail, (object)['nRegistriert' => 1]);
            }
        } else {
            // guest login, the customer is not created in the database, additional data is only set on completion of the order
            $_SESSION['Kunde'] = $customer;

            // Save customer attributes for later - this will be read by bestellungInDB for guest orders
            Frontend::set('customerAttributes', $customerAttributes);
        }

        // Perform coupon check
        $this->checkCoupons($this->getCoupons());

        // Refresh tax rates and the basket.
        if (isset($cart->kWarenkorb) && $cart->gibAnzahlArtikelExt([C_WARENKORBPOS_TYP_ARTIKEL]) > 0) {
            Tax::setTaxRates();
            $cart->gibGesamtsummeWarenLocalized();
        }

        return $customer;
    }

    /**
     * Performs a login for an existing customer for the given JTL Customer id.
     *
     * Attention: Do not try to login customers that do not have a verified account mapping!
     *
     * @param $jtlCustomerId
     * @return void
     */
    public function loginCustomer($jtlCustomerId): void {

        // NOTE: Some of the following calls will fail with an error if bestellvorgang_inc.php is not included here (AccountController does not require it by itself, although it indirectly needs it to check the coupons)
        require_once PFAD_ROOT . PFAD_INCLUDES . 'bestellvorgang_inc.php';

        $this->debugLog('Trying to log in customer with id: "' . $jtlCustomerId . '"', 'CustomerAccountController');

        $customer = new Customer($jtlCustomerId);
        if (empty($customer->getID()) || $customer->getID() <= 0) {
            $this->debugLog('Customer with id: "' . $jtlCustomerId . '" was not found. Cancelling login.', 'CustomerAccountController');
            return;
        }
        if ($customer->nRegistriert !== 1) {
            $this->debugLog('Customer with id: "' . $jtlCustomerId . '" is a guest account and cannot be logged in. Cancelling login.', 'CustomerAccountController');
            return;
        }

        /**
         * The alternative/intuitive method $customer->isLoggedIn() does NOT do what you expect - it currently simply returns true iff kKunde > 0 - this is completely useless to us.
         */
        if ($this->isCustomerLoggedIn($jtlCustomerId)) {
            // we do not need to re-login a user that is already logged in.
            $this->debugLog('Customer with id: "' . $jtlCustomerId . '" is already logged in, skipping.', 'CustomerAccountController');
            return;
        }


        // We login the customer by using the JTL account controller
        $jtlCustomer = new Customer($jtlCustomerId);
        if ($jtlCustomer->kKunde > 0) {
            $accountController = new AccountController(Shop::Container()->getDB(), Shop::Container()->getAlertService(), Shop::Container()->getLinkService(), Shop::Smarty());
            $accountController->initCustomer($jtlCustomer);
            return;
        }

    }


    /**
     * @param $customerId - to check if a specific customer is logged in, give his id here, else leave it as null and the method just checks if the current customer is logged in
     * @return bool
     */
    public function isCustomerLoggedIn(int $customerId = null): bool {
        if (null === $customerId) {
            return isset($_SESSION['Kunde'], $_SESSION['Kunde']->kKunde) && (int)$_SESSION['Kunde']->kKunde > 0;
        }

        return isset($_SESSION['Kunde'], $_SESSION['Kunde']->kKunde) && (int)$_SESSION['Kunde']->kKunde > 0 && (int)$_SESSION['Kunde']->kKunde === $customerId;
    }

    /**
     * Checks if a customer exists in the session.
     */
    public function isCustomerPresent(): bool {
        return isset($_SESSION['Kunde']);
    }

    /**
     * Checks if the customer in the session is a guest.
     * Note: This method also returns FALSE if no customer is present in the Session, at all.
     *
     * @return bool
     */
    public function isCustomerGuest(): bool {
        return isset($_SESSION['Kunde']) && (int)$_SESSION['Kunde']->nRegistriert === 0;
    }

    /**
     * Just returns the customer id of the current customer in the session.
     * @return int
     */
    public function getCurrentCustomerId(): int {
        return (int)$_SESSION['Kunde']->kKunde;
    }

    /**
     * 1:1 Copy of a private method in JTLs AccountController.
     * TODOLATER: Maybe use the original method if it becomes public?
     * @return array
     */
    private function getCoupons(): array {
        $coupons = [];
        $coupons[] = !empty($_SESSION['VersandKupon']) ? $_SESSION['VersandKupon'] : null;
        $coupons[] = !empty($_SESSION['oVersandfreiKupon']) ? $_SESSION['oVersandfreiKupon'] : null;
        $coupons[] = !empty($_SESSION['NeukundenKupon']) ? $_SESSION['NeukundenKupon'] : null;
        $coupons[] = !empty($_SESSION['Kupon']) ? $_SESSION['Kupon'] : null;

        return $coupons;
    }

    /**
     * 1:1 Copy of a private method in JTLs AccountController.
     * TODOLATER: Maybe use the original method if it becomes public?
     * @param array $coupons
     */
    private function checkCoupons(array $coupons): void {
        // NOTE: The following will fail with an error if bestellvorgang_inc.php is not included here (Kupon does not require it by itself and we need it for angabenKorrekt)
        require_once PFAD_ROOT . PFAD_INCLUDES . 'bestellvorgang_inc.php';
        foreach ($coupons as $coupon) {
            if (empty($coupon)) {
                continue;
            }
            $error = Kupon::checkCoupon($coupon);
            $returnCode = \angabenKorrekt($error);
            \executeHook(\HOOK_WARENKORB_PAGE_KUPONANNEHMEN_PLAUSI, [
                'error' => &$error,
                'nReturnValue' => &$returnCode
            ]);
            if ($returnCode) {
                if (isset($coupon->kKupon) && $coupon->kKupon > 0 && $coupon->cKuponTyp === Kupon::TYPE_STANDARD) {
                    Kupon::acceptCoupon($coupon);
                    \executeHook(\HOOK_WARENKORB_PAGE_KUPONANNEHMEN);
                } elseif (!empty($coupon->kKupon) && $coupon->cKuponTyp === Kupon::TYPE_SHIPPING) {
                    // Versandfrei Kupon
                    $_SESSION['oVersandfreiKupon'] = $coupon;
                    Shop::Smarty()->assign(
                        'cVersandfreiKuponLieferlaender_arr',
                        \explode(';', $coupon->cLieferlaender)
                    );
                }
            } else {
                Frontend::getCart()->loescheSpezialPos(\C_WARENKORBPOS_TYP_KUPON);
                Kupon::mapCouponErrorMessage($error['ungueltig']);
            }
        }
    }

}