<div class="lpa-button-content">
    <div id="{$lpaButton.id}" class="lpa-button-container lpa-button-login-container" style="width:{$lpaButton.width}px;height:{$lpaButton.height}px;"
         data-merchant-id="{$lpaButton.sellerId}"
         data-ledger-currency="{$lpaButton.ledgerCurrency}"
         data-language="{$lpaButton.language}"
         data-product-type="{$lpaButton.productType}"
         data-placement="{$lpaButton.placement}"
         data-color="{$lpaButton.color}"
         data-sandbox="{if $lpaButton.sandbox}true{else}false{/if}"
         data-publickeyid="{$lpaButton.publicKeyId}"
         data-payload='{$lpaButton.payload|stripcslashes}'
         data-signature="{$lpaButton.signature}"
    ></div>
</div>