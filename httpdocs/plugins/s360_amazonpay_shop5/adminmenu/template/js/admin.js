jQuery(document).ready(function () {

    /* copy to clipboard function */
    jQuery('.s360-copy-to-clipboard').click(function (e) {
        e.preventDefault();
        var $that = jQuery(this).closest('.s360-copy-to-clipboard');
        var $target = jQuery($that.data('target'));
        if ($target.length) {
            $target.select();
            document.execCommand('copy');
        }
    });

    /* show video container */
    jQuery('.s360-video').click(function (e) {
        e.preventDefault();
        var $that = jQuery(this).closest('.s360-video');
        var $iframe = $that.find('iframe:not(.initialized)');
        if ($iframe.length) {
            $iframe.attr('src', $iframe.attr('data-src')).addClass('initialized');
        }
        $that.find('.s360-video-container').slideToggle();
    });

    /* react on check mws data */
    jQuery('#check-access-button').click(function (e) {
        e.preventDefault();
        window.lpaAdmin.checkAccess();
    });

    /* react on create key */
    jQuery('#create-key-button').click(function (e) {
        e.preventDefault();
        window.lpaAdmin.createKey();
    });

    /* react on setting default template values via button */
    jQuery('.lpa-pq-defaults').click(function (e) {
        e.preventDefault();
        var template = $(this).closest('.lpa-pq-defaults').data('template');
        window.lpaAdmin.setTemplateDefaults(template);
    });
});

(function ($) {
    var lpaAdmin = function (options) {
        return {
            checkAccess: function () {
                // This function checks if the currently entered data (not necessarily saved!) is valid for the MWS access.
                var $feedback = $('#check-access-feedback');
                var $form = $('#lpa-account-settings-form');
                $feedback.hide();
                $feedback.html('');

                var errorHtml = '';

                if (errorHtml) {
                    $feedback.html(errorHtml);
                    $feedback.show();
                    return;
                }

                /*
                 * The user entered potentially valid data. Start the check.
                 */
                $feedback.removeClass('success');
                $feedback.removeClass('error');
                $feedback.html(window.lpaLang['pleaseWait']);
                $feedback.show();
                $form.css('cursor', 'wait');

                var ajaxURL = window.lpaAdminAjaxUrl;

                var request = $.ajax({
                    url: ajaxURL + '&action=checkAccess',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        csrf: window.JTL_TOKEN
                    }
                });
                request.done(function (data) {
                    if (data.result === 'success') {
                        if (!$feedback.hasClass('success')) {
                            $feedback.addClass('success');
                        }
                        $feedback.html(window.lpaLang['checkSuccess']);
                    } else {
                        if (!$feedback.hasClass('error')) {
                            $feedback.addClass('error');
                        }
                        $feedback.html(window.lpaLang['checkFail']);
                        if (data.messages && data.messages.length) {
                            data.messages.forEach(function (value) {
                                $feedback.append('<br/>' + value);
                            });
                        }
                    }
                });
                request.fail(function (jqXHR, textStatus, errorThrown) {
                    console.log('Failed: ' + jqXHR + "," + textStatus + "," + errorThrown);
                    if (!$feedback.hasClass('failure')) {
                        $feedback.addClass('failure');
                    }
                    $feedback.html(window.lpaLang['technicalError']);
                });
                request.always(function () {
                    $form.css({'cursor': 'default'});
                });
            },
            createKey: function () {
                var $feedback = $('#create-key-feedback');
                var $publicKeyContainer = $('#create-key-publickey-container');
                $feedback.removeClass('success');
                $feedback.removeClass('error');
                $feedback.html(window.lpaLang['pleaseWait']);
                $feedback.show();

                var ajaxURL = window.lpaAdminAjaxUrl;

                var request = $.ajax({
                    url: ajaxURL + '&action=createKey',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        csrf: window.JTL_TOKEN
                    }
                });
                request.done(function (data) {
                    if (data.result === 'success' && data.data.publickey) {
                        if (!$feedback.hasClass('success')) {
                            $feedback.addClass('success');
                        }
                        $feedback.html(window.lpaLang['keyGenSuccess']);
                        $publicKeyContainer.html('<pre>' + data.data.publickey + '</pre>');
                    } else {
                        if (!$feedback.hasClass('error')) {
                            $feedback.addClass('error');
                        }
                        $feedback.html(window.lpaLang['keyGenFailure']);
                        if (data.messages && data.messages.length) {
                            data.messages.forEach(function (value) {
                                $feedback.append('<br/>' + value);
                            });
                        }
                    }
                });
                request.fail(function (jqXHR, textStatus, errorThrown) {
                    console.log('Failed: ' + jqXHR + "," + textStatus + "," + errorThrown);
                    if (!$feedback.hasClass('failure')) {
                        $feedback.addClass('failure');
                    }
                    $feedback.html(window.lpaLang['technicalError']);
                });
            },
            renderPreviewButton: function (merchantId, id, type, width, height, color) {
                // empty container before rendering
                var $element = $('#' + id);
                if (!$element.length) {
                    return;
                }
                // we need to do this, because amazon tries to add a shadow root which cannot be done twice
                var $parent = $element.parent();
                $parent.children().remove();
                $element = $('<div/>');
                $element.attr('id', id).appendTo($parent);
                amazon.Pay.renderButton('#' + id, {
                    merchantId: merchantId,
                    // Note: any callbacks are missing in these preview buttons - they do not work when clicked!
                    sandbox: true, // dev environment
                    ledgerCurrency: 'EUR', // Amazon Pay account ledger currency
                    checkoutLanguage: 'de_DE', // render language
                    productType: type === 'PwA' ? 'PayAndShip' : 'SignIn', // checkout type
                    buttonColor: color, // color of the button
                    placement: 'Cart' // button placement
                });
                $element.css({
                    width: width + 'px',
                    height: height + 'px'
                });
            },
            setTemplateDefaults: function (template) {
                var templateConfigs = {
                    nova: {
                        loginSelector: '#quick-login, #login_form, fieldset.quick_login',
                        loginMethod: 'append',
                        loginAlignment: 'left',
                        paySelector: '.cart-summary .card > .card-body',
                        payMethod: 'append',
                        payAlignment: 'center',
                        payDetailSelector: '#add-to-cart',
                        payDetailMethod: 'append',
                        payDetailAlignment: 'right',
                        payCategorySelector: '#result-wrapper_buy_form_#kArtikel# .productbox-actions',
                        payCategoryMethod: 'append',
                        payCategoryAlignment: 'center',
                        payWidth: '200',
                        payHeight: '60',
                        payDetailWidth: '150',
                        payDetailHeight: '45',
                        payCategoryWidth: '150',
                        payCategoryHeight: '45'
                    },
                    evo: {
                        loginSelector: '#quick-login, #login_form, fieldset.quick_login',
                        loginMethod: 'append',
                        loginAlignment: 'left',
                        paySelector: '.basket-well .proceed',
                        payMethod: 'append',
                        payAlignment: 'right',
                        payDetailSelector: '#add-to-cart',
                        payDetailMethod: 'append',
                        payDetailAlignment: 'right',
                        payCategorySelector: '#result-wrapper_buy_form_#kArtikel# .expandable',
                        payCategoryMethod: 'append',
                        payCategoryAlignment: 'center',
                        payWidth: '200',
                        payHeight: '60',
                        payDetailWidth: '150',
                        payDetailHeight: '45',
                        payCategoryWidth: '150',
                        payCategoryHeight: '45'
                    },
                    easytemplate360: {
                        loginSelector: '#amapay-login, #amapay-login-dropdown',
                        loginMethod: 'append',
                        loginAlignment: 'left',
                        paySelector: '#amapay-basket, #amapay-basket-dropdown, #amapay-checkout-button-only',
                        payMethod: 'append',
                        payAlignment: 'right',
                        payDetailSelector: '#add-to-cart .basket-form-inline',
                        payDetailMethod: 'append',
                        payDetailAlignment: 'right',
                        payCategorySelector: '#buy_form_#kArtikel#',
                        payCategoryMethod: 'after',
                        payCategoryAlignment: 'center',
                        payWidth: '200',
                        payHeight: '60',
                        payDetailWidth: '200',
                        payDetailHeight: '60',
                        payCategoryWidth: '150',
                        payCategoryHeight: '45'
                    },
                    hypnos: {
                        loginSelector: '#form-login',
                        loginMethod: 'after',
                        loginAlignment: 'left',
                        paySelector: '#paymentbuttons',
                        payMethod: 'append',
                        payAlignment: 'right',
                        payDetailSelector: '#paymentbuttons',
                        payDetailMethod: 'append',
                        payDetailAlignment: 'right',
                        payCategorySelector: '.paymentbuttons_#kArtikel#',
                        payCategoryMethod: 'append',
                        payCategoryAlignment: 'center',
                        payWidth: '200',
                        payHeight: '60',
                        payDetailWidth: '150',
                        payDetailHeight: '45',
                        payCategoryWidth: '150',
                        payCategoryHeight: '45'
                    },
                    snackys: {
                        loginSelector: '#login-popup .form, #login_form, #existing-customer .add-pays .amazon',
                        loginMethod: 'append',
                        loginAlignment: 'center',
                        paySelector: '.c-dp .add-pays .amazon, .cart-sum .add-pays .amazon',
                        payMethod: 'append',
                        payAlignment: 'center',
                        payDetailSelector: '.buy-col .add-pays .amazon',
                        payDetailMethod: 'append',
                        payDetailAlignment: 'center',
                        payCategorySelector: '#result-wrapper_buy_form_#kArtikel# .exp',
                        payCategoryMethod: 'append',
                        payCategoryAlignment: 'center',
                        payWidth: '200',
                        payHeight: '60',
                        payDetailWidth: '150',
                        payDetailHeight: '45',
                        payCategoryWidth: '150',
                        payCategoryHeight: '45'
                    }
                };
                var config = templateConfigs[template];
                $('#lpa-config-button-login-pq-selector').val(config['loginSelector']).trigger('change');
                $('#lpa-config-button-login-pq-method').val(config['loginMethod']).trigger('change');
                $('#lpa-config-button-login-alignment').val(config['loginAlignment']).trigger('change');
                $('#lpa-config-button-pay-pq-selector').val(config['paySelector']).trigger('change');
                $('#lpa-config-button-pay-pq-method').val(config['payMethod']).trigger('change');
                $('#lpa-config-button-pay-alignment').val(config['payAlignment']).trigger('change');
                $('#lpa-config-button-pay-detail-pq-selector').val(config['payDetailSelector']).trigger('change');
                $('#lpa-config-button-pay-detail-pq-method').val(config['payDetailMethod']).trigger('change');
                $('#lpa-config-button-pay-detail-alignment').val(config['payDetailAlignment']).trigger('change');
                $('#lpa-config-button-pay-category-pq-selector').val(config['payCategorySelector']).trigger('change');
                $('#lpa-config-button-pay-category-pq-method').val(config['payCategoryMethod']).trigger('change');
                $('#lpa-config-button-pay-category-alignment').val(config['payCategoryAlignment']).trigger('change');
            },
            reset: function (element) {
                var $this = $(element);
                var $container = $this.closest('.card');
                $container.find('[data-default]').each(function () {
                    var $element = $(this);
                    var defaultValue = $element.attr('data-default');
                    var nodeName = $element.prop('nodeName').toLowerCase();
                    var type = $element.attr('type');
                    if (nodeName === 'input') {
                        switch (type) {
                            case 'radio':
                            case 'checkbox':
                                if (defaultValue === '1') {
                                    $element.prop('checked', true);
                                } else {
                                    $element.prop('checked', false);
                                }
                                break;
                            default:
                                $element.val(defaultValue);
                                break;
                        }
                    } else if (nodeName === 'select' || nodeName === 'textbox') {
                        $element.val(defaultValue);
                    }
                    $element.trigger('input');
                    $element.trigger('change');
                });
            }
        }
    };

    window.lpaAdmin = lpaAdmin();
})(jQuery);