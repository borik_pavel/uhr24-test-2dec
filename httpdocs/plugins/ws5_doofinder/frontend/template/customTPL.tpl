<script type="text/javascript" data-eucid="jst_doofinder_custom_templates">

    let templateResult{$tplID} = "{$dfTPL|htmlentities}";
    let tempDiv{$tplID} = document.createElement("div");
    tempDiv{$tplID}.innerHTML = templateResult{$tplID};

    $("head").append(tempDiv{$tplID}.childNodes[0].nodeValue);
</script>