{include "$knm_dashboard_tpl_path/layout/header.tpl"}
{* number component *}
{assign var="step" value=NULL}
<div class="knm_custom_dashboard" style="position:relative;">
    <div class="bmd-layout-container" style="min-height: 100%">
        {include "$knm_dashboard_tpl_path/layout/navBar.tpl"}
        {* {include "$knm_dashboard_tpl_path/drawer/drawer.tpl"} *}
        <main class="bmd-layout-content">
            <div id="main_content" class="container">
                <div class="card">
                    {if $oSubscription }
                        {if isset($smarty.cookies.knmUpdateNotification) && $smarty.cookies.knmUpdateNotification === 'hidden'}
                            {assign var="notificationCookieIsHidden" value=true}
                        {else}
                            {assign var="notificationCookieIsHidden" value=false}
                        {/if}
                        {if $bUpdateAvailable && !$notificationCookieIsHidden}
                            {include file="$knm_dashboard_tpl_path/updateShopMessage.tpl" version=$strLatestVersion build=$strLatestBuild}
                        {/if}
                    {/if}
                    {if $showLanguagePackages}
                        {include file="$knm_dashboard_tpl_path/languagePackages.tpl" downloadableLanguages=$downloadableLanguages}
                    {else}
                        {include file="$knm_dashboard_tpl_path/display_plugin_tabs.tpl"}
                    {/if}
                </div>
            </div>
        </main>
    </div>
</div>
{include "$knm_dashboard_tpl_path/layout/footer.tpl"}

