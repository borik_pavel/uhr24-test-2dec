<button id="slideButtonWrapper" class="btn" data-toggle="collapse" data-target="#dw-s1">
        <span id="menuIcon" class="slideButtonWrapper--icon">
            <i id="menu_arrow_left" class="fas fa-caret-right"></i>
        </span>
</button>
<div class="bmd-layout-drawer row col-3 p-0 h-100 text-white w-sidebar navbar-collapse collapse d-none d-md-flex sidebar" id="dw-s1">
    <div class="position-fixed h-100 w-sidebar">
        <header>
            <a class="navbar-brand" href="https://www.knowmates.de/shop/Plugins-fuer-JTL-Shop" target="_blank">
                <span>knowmates GmbH</span>
            </a>
        </header>
        <ul class="list-group plugins-list">
            {assign "find" array('ALPHA', 'BETA')}
            {assign "repl" array('<div class="alpha">ALPHA</div>', '<div class="beta">BETA</div>')}
            {assign "text" ""}
            {assign "updateJA" ""}
            {foreach $knowmatsPluginsList as $knowmatesPlugin}
                <a href="{$pluginLink}{$knowmatesPlugin->kPlugin}"
                   class="{if $currentPluginId == $knowmatesPlugin->kPlugin && !$showLanguagePackages}active{/if} list-group-item font-color-primary">
                    {assign "text" utf8_encode($knowmatesPlugin->cName)}
                    {$text|replace:$find:$repl}
                    {if $updateJA === "ja"}
                        <i class="fas fa-sync fa-spin updateInfoIcon"></i>
                    {else}
                        <i class="updateInfoIcon"></i>
                    {/if}
                </a>
            {/foreach}
            {if isset($showAddLanguage) && $showAddLanguage == true}
                <a class="list-group-item {if $showLanguagePackages}active{/if} font-color-primary"
                   href="{$languagePackagesPath}">
                    <i class="fas fa-language"></i>
                    {knmLang key="add language packages" section="dashboard"}
                </a>
            {/if}
        </ul>
    </div>
</div>