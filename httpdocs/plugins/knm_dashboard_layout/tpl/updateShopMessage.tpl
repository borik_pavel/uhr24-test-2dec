<a href="https://shop.knowmates.de/kontakt?betreff=7&nachricht=Ich w&uuml;rde gerne ein Update auf Shop Version {$version}{if $build > 0} build {$build} {/if} beantragen." target="_blank" id="updateShopInfoButton" class="alert alert-info">
    <i class="fas fa-sync fa-spin updateInfoIcon"></i>
    <span class="">
        {knmLang key="jtl-shop version x build x available" version=$version build=$build section="dashboard"}
    </span>
    <i style="right: 8px; top: 8px;z-index: 2;" class="fa fa-times position-absolute" onclick="hideUpdateNotification()"></i>
</a>

