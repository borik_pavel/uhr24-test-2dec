<script>
    $(document).ready(function () {
        knm_handle_endAdornments();
    });
</script>
{if !empty($jsFilesFooter)}
    {foreach item=$customJsFile from=$jsFilesFooter}
        <script type="text/javascript" src="{$customJsFile}"></script>
    {/foreach}
{/if}

{literal}
    <script>
        function setLanguage(lang, prevLang) {
            if (lang !== prevLang) {
                let urlString = document.location.href;
                urlString = urlString.replace(`&lang=${prevLang}`, '');
                window.location.replace(`${urlString}&lang=${lang}`);
            }
        }
    </script>
{/literal}