<div class="bmd-layout-header fixed-top bg-theme-shade-1 bg-faded">
    <div class="bmd-layout-header--inner">
        <a class="navbar-brand" href="https://www.knowmates.de/shop/Plugins-fuer-JTL-Shop" target="_blank">
            <span>knowmates GmbH</span>
        </a>
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <div class="dark-design-toggle" onclick="toggleDarkDesign()">
                    <div class="checkbox checkbox-switch switch-primary">
                        <label>
                            <span class="pr-2">{knmLang key="design" section="dashboard"}:</span>
                            <span style="margin-right: 4px"><i class="fas fa-sun"></i></span>
                            <input id="design-swich" onchange="toggleDarkDesign()" type="checkbox"
                                   {if !isset($smarty.cookies.knmDesign) || $smarty.cookies.knmDesign == 'null' || $smarty.cookies.knmDesign === 'dark'}checked=""{/if}>
                            <span></span>
                        </label>
                    </div>
                    <span><i class="fas fa-moon"></i></span>
                </div>
            </li>
        </ul>
    </div>
</div>