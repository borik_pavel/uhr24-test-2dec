<ul class="nav nav-tabs d-flex d-xl-none customTabBar" id="myTab4" role="tablist">
    {foreach name="pluginadminmenutabs" from=$oPlugin->getAdminMenu()->getItems()->all() item=oPluginAdminMenu key=key}
        {if $smarty.foreach.pluginadminmenutabs.index < 5}
            {include "./singleTab.tpl" tabId="{$oPlugin->getPluginID()}{$oPluginAdminMenu->nSort}" tabName=$oPluginAdminMenu->cName icon=$subPages[$oPluginAdminMenu->nSort]->icon}                {if $smarty.foreach.pluginadminmenutabs.index < 4 && $smarty.foreach.pluginadminmenutabs.last === true}
            {if $licenseType != '(╯°□°）╯︵ ┻━┻)'}
                {include "./singleTab.tpl" tabId='lizenz_tab' tabName='license' icon="fas fa-key"}
            {/if}
            {include "./singleTab.tpl" tabId='about_tab' tabName='about' icon="fas fa-hashtag"}
        {/if}
        {else}
            {if  tabArrayLength === 5}
            {else}
                {if $smarty.foreach.pluginadminmenutabs.index === 5 }
                    <li class="nav-item tabItem dropdown-toggle dropdown-toggle-split tabDropDown p-0"
                        data-toggle="dropdown">
                        <a class="showMoreSmNav w-100 knmPluginTab d-flex nav-link">
                                <span class="m-auto">
                                    {knmLang key="more"} <i style="margin-left:8px" class="fas fa-caret-down"></i>
                                </span>
                        </a>
                    </li>
                    <div class="dropdown-menu tabDropDown customDropDownForTabs tabDropDownSM">
                    <ul id="myTab2" class="nav nav-tabs" role="tablist">
                {/if}
                {include "./singleTab.tpl" isMore=true tabId="{$oPlugin->cPluginID}{$oPluginAdminMenu->nSort}" tabName=$oPluginAdminMenu->cName icon=$subPages[$oPluginAdminMenu->nSort]->icon}
                {if $smarty.foreach.pluginadminmenutabs.last === true}
                    {if $licenseType != '(╯°□°）╯︵ ┻━┻)'}
                        {include "./singleTab.tpl" isMore=true tabId='lizenz_tab' tabName='license' icon="fas fa-key"}
                    {/if}
                    {include "./singleTab.tpl" isMore=true tabId='about_tab' tabName='about' icon="fas fa-hashtag"}
                    </ul>
                    </div>
                {/if}
            {/if}
        {/if}
    {/foreach}
</ul>