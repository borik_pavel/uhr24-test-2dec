<div class="tab-pane fade {if $tabId == $initialTab}active show{/if}"
     id="{$tabId}" role="tabpanel"
     aria-labelledby="{$tabId}-tab">
    <div class="tabbertab">
        <h2 class="d-sm-none">{$tabName}</h2>
        {if isset($index) && isset($subPages[$index]->url) && $subPages[$index]->url != ''}
            {assign var=pluginTemplateFileUrl value=$subPages[$index]->url}
            {include file="$plugin_template_path/$pluginTemplateFileUrl" tabId=$tabId}
        {else}
            {if 'about_tab' == $tabId}
                {include file="$aboutPath" tabId=$tabId}
            {/if}
            {if 'lizenz_tab' == $tabId}
                {include file="$licensePath" tabId=$tabId}
            {/if}
        {/if}
    </div>
</div>