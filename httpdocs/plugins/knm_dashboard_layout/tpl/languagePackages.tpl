<h2>{knmLang key="add language packages" section="dashboard"}</h2>
{if $downloadableLanguages|@count gt 0}
    {foreach name=knmDashboardLang from=$downloadableLanguages item=$downloadableLanguage}
        {if $smarty.foreach.knmDashboardLang.iteration%2 == 1}
            <div class="row d-none d-lg-flex">
        {/if}
        <div class="col-xs-12 col-lg-6 mt-2 mb-2">
            <form method="post" action="{$pluginTabBasepath}&knmLang=1">
                <input type="hidden" name="langToInstall" value="{$downloadableLanguage['langIso']}"/>
                <input type="hidden" name="localeToInstall" value="{$downloadableLanguage['locale']}"/>
                {include "$knm_dashboard_tpl_path/components/singleLanguagePackage.tpl" downloadableLanguage=$downloadableLanguage}
            </form>
        </div>
        {if $smarty.foreach.knmDashboardLang.iteration%2 == 0 || $smarty.foreach.knmDashboardLang.last}
            </div>
        {/if}
    {/foreach}
    {foreach name=knmDashboardLang from=$downloadableLanguages item=$downloadableLanguage}
        <div class="row d-block d-lg-none">
            <div class="col-xs-12 mt-2 mb-2">
                <form method="post" action="{$pluginTabBasepath}&knmLang=1">
                    <input type="hidden" name="langToInstall" value="{$downloadableLanguage['langIso']}"/>
                    <input type="hidden" name="localeToInstall" value="{$downloadableLanguage['locale']}"/>
                    {include "$knm_dashboard_tpl_path/components/singleLanguagePackage.tpl" downloadableLanguage=$downloadableLanguage}
                </form>
            </div>
        </div>
    {/foreach}
{else}
    <div class="alert alert-info mt-4">
        {knmLang key="all available languages already installed" section="dashboard"}
    </div>
{/if}