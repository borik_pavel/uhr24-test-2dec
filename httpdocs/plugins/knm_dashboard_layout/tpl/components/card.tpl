{if !isset($cssClass)}
    {assign cssClass ''}
{/if}
{if !isset($alignActionsRight)}
    {assign alignActionsRight 0}
{/if}
<div class="card-component {$cssClass}">
    {if isset($avatar) or isset($avatarImg) or isset($title) OR isset($subTitle) or isset($headerAction)}
        <div class="card_header">
            {if isset($avatar) or isset($avatarImg)}
                <div class="avatar"
                     {if isset($avatarImg)}style="background: url('{$avatarImg}') no-repeat;background-size: auto 48px;"{/if}>
                    {if isset($avatar)}{$avatar}{/if}
                </div>
            {/if}
            {if isset($title) OR isset($subTitle)}
                <div class="card_header_title">
                    {if isset($title)}
                        <span class="card_header_title_text">{$title}</span>
                    {/if}
                    {if isset($subTitle)}
                        <span class="card_header_subTitle_text">{$subTitle}</span>
                    {/if}
                </div>
            {/if}
            {if isset($headerAction)}
                <div class="card_header_action">
                    {include $components->buttons->iconButton icon="{$headerAction['icon']}" onclick="{$headerAction['onclick']}" buttonType="default"}
                </div>
            {/if}
        </div>
    {/if}
    {if isset($img)}
        <div class="card_header_image">
            <img src="{$img['src']}" alt="{$img['alt']}" {if isset($img['class'])}class="{$img['class']}"{/if} />
        </div>
    {/if}
    {if isset($content)}
        <div class="card_content">
            {$content}
        </div>
    {/if}
    {if isset($actions) or isset($dropwdownContent)}
        <div class="card_actions {if $alignActionsRight == true}justify-content-end{/if}">
            {if isset($actions)}
                {foreach name="cardActions" item=$action from=$actions}
                    {include $components->buttons->iconButton icon="{$action['icon']}" onclick="{$action['onclick']}" buttonType="{if isset($action['buttonType'])}{$action['buttonType']}{else}default{/if}"}
                {/foreach}
            {/if}
            {if isset($dropwdownContent)}
                {include $components->buttons->iconButton cssClass="dropDown-button" icon="dropDown-icon" onclick="knm_handle_card_dropdown('{$dropdownId}')" }
            {/if}
        </div>
    {/if}
    {if isset($dropwdownContent)}
        <div id="{$dropdownId}" class="card_dropwdownContent" style="display: none">
            {$dropwdownContent}
        </div>
    {/if}
</div>
<script>
    if (!knm_handle_card_dropdown) {
        function knm_handle_card_dropdown(id) {
            let ele = $('#' + id);
            if (ele.css('display') === "none") {
                ele.slideDown();
                ele.parent().find('.dropDown-button').addClass('rotated')
            } else {
                ele.parent().find('.dropDown-button').removeClass('rotated')
                ele.slideUp();
            }
        }
    }
</script>