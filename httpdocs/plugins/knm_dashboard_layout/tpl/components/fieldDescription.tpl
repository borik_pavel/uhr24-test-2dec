{if $description != ''}
    <div data-toggle="tooltip" title="{$description}" class="tooltip-icon-wrapper">
        <i class="fa fa-info-circle"></i>
    </div>
{/if}