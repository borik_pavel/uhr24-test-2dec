{assign collapseId "collapse_{$id}"}
<div class="accordion settings-accordion mb-3" id="{$id}">
    <div class="card">
        <div class="card-header">
            <a data-toggle="collapse" data-target="#{$collapseId}" aria-expanded="{if $open == true}true{else}false{/if}"
               aria-controls="{$collapseId}">
                {if isset($title)}{$title}{/if}
            </a>
        </div>
        <div id="{$collapseId}" class="collapse {if $open == true}show{/if}" aria-labelledby="{$collapseId}" data-parent="#{$id}">
            <div class="card-body">
                {$content}
            </div>
        </div>
    </div>
</div>