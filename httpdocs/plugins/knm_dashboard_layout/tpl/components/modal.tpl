{if !isset($closeLabel)}
    {assign var="closeLabel" value={knmLang key="close" section="dashboard"}}
{/if}
{if !isset($submitLabel)}
    {assign var="submitLabel" value={knmLang key="submit" section="dashboard"}}
{/if}
{if !isset($useOpenHandler)}
    {assign var="useOpenHandler" value=0}
{/if}
{if !isset($submitButtonType)}
    {assign var="submitButtonType" value='primary'}
{/if}
<div class="modal fade" id="{$id}" tabindex="-1" role="dialog" aria-labelledby="{$id}Label"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        {if isset($form)}
        <form method="{$form['method']}" action="{$form['action']}">
            {/if}
            <div class="modal-content" style="background-color: var(--themeShade4)">
                <div class="modal-header">
                    {if isset($title)}
                        <h5 class="modal-title" id="{$id}Label">{$title}</h5>
                    {/if}
                    <button type="button" class="close" {if isset($onClose)}onclick="{$onClose}"
                            {else}data-dismiss="modal"{/if} aria-label="Close">
                        <span style="color: var(--primaryFontColor)" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {$content}
                </div>
                <div class="modal-footer">
                    <button
                            type="button" class="btn btn-default mr-2"
                            {if isset($onClose)}onclick="{$onClose}" {else}data-dismiss="modal"{/if}
                    >{$closeLabel}</button>
                    <div style="position: relative">
                        <button
                                {if isset($onSubmit)}type="button" onclick="{$onSubmit}"
                                {elseif isset($form)}
                                type="submit"
                                {else}
                                data-dismiss="modal"
                                {/if}
                                class="btn btn-{$submitButtonType} btn-raised">
                            {$submitLabel}
                        </button>
                    </div>
                </div>
            </div>
            {if isset($form)}
        </form>
        {/if}
    </div>
</div>
{if $useOpenHandler}
{literal}
    <script>
        function open_modal_{/literal}{$id}{literal} () {
            $("#{/literal}{$id}{literal}").modal("show");
            $("#{/literal}{$id}{literal}").appendTo("body");
        }
    </script>
{/literal}
{/if}