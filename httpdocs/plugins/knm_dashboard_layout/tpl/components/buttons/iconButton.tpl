{if !isset($buttonType)}
    {assign "buttonType" "primary"}
{/if}
{if !isset($type)}
    {assign "type" "button"}
{/if}
{if !isset($cssClass)}
    {assign "cssClass" ""}
{/if}
{if !isset($name)}
    {assign "name" ""}
{/if}
{if !isset($id)}
    {assign id $name}
{/if}
<button {if isset($onclick)}onclick="{$onclick}"{/if} class="icon-btn btn btn-{$buttonType} {if isset($variant)}btn-{$variant}{/if}{$cssClass}"
        type="{$type}"  {if isset($name)}name="{$name}"{/if} {if isset($id)}id="{$id}"{/if}>
        <i class="{$icon}"></i>
</button>