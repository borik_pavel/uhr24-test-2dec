{if !isset($iconPosition)}
    {assign "iconPosition" "left"}
{/if}
{if !isset($type)}
    {assign "type" "submit"}
{/if}
{if !isset($hideIcon)}
    {assign "hideIcon" 0}
{/if}
{if !isset($hideText)}
    {assign "hideText" 0}
{/if}
{if !isset($variant)}
    {assign "variant" "raised"}
{/if}
{if !isset($cssClass)}
    {assign "cssClass" "btn-save"}
{/if}

{include
"./button.tpl"
text="{if !$hideText}{if !isset($text)}{knmLang key="save" section="dashboard"}{else}{$text}{/if}{/if}"
buttonType="primary"
icon="{if !$hideIcon}fa fa-save{/if}"
onclick="{if isset($onclick)}{$onclick}{/if}"
cssClass="{$cssClass}"
variant=$variant
iconPosition=$iconPosition
}
