{if !isset($json)}
    {assign json 0}
{/if}
{if $json == true}
    {assign fieldName "json[{$name}]"}
{else}
    {assign fieldName $name}
{/if}
{if !isset($value)}
    {assign value 1}
{/if}
{if !isset($checked)}
    {assign checked false}
{/if}
{if !isset($id)}
    {assign id $name}
{/if}
{if !isset($wrapperCssClass)}
    {assign wrapperCssClass ''}
{/if}

{if !isset($readonly)}
    {assign var="readonly" value=false}
{/if}
<div class="form-group knowmates-form-group form-group-switch {$wrapperCssClass}">
    <div class="checkbox checkbox-switch switch-primary">
        <label>
            <span>
                {$label}
                {if isset($description) && $description|strlen > 0}
                    {include $components->fieldDescription description=$description}
                {/if}
                </span>
            <input id="{$id}" {if isset($cssClass)}class="{$cssClass}"{/if} type="checkbox" name="{$fieldName}"
                   value="{$value}" {if $checked == $value} checked="checked" {/if} {if isset($required)}required{/if} {if isset($onChange)}onChange="{$onChange}"{/if} {if $readonly}readonly{/if}>
            <span>
            </span>
        </label>
    </div>
</div>