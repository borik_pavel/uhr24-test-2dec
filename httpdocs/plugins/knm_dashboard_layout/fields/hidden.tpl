{if !isset($json)}
    {assign json 0}
{/if}
{if $json == true}
    {assign fieldName "json[{$name}]"}
{else}
    {assign fieldName $name}
{/if}
{if !isset($value)}
    {assign value ''}
{/if}
{if !isset($id)}
    {assign id $name}
{/if}
<input id="{$id}" value="{$value}" name="{$fieldName}" type="hidden" class="form-control" />