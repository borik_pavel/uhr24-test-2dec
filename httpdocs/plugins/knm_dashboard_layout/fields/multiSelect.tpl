{if !isset($json)}
    {assign json 0}
{/if}
{if $json == true}
    {assign fieldName "json[{$name}]"}
{else}
    {assign fieldName $name}
{/if}
{if !isset($cssClass)}
    {assign cssClass ''}
{/if}
{if !isset($wrapperCssClass)}
    {assign wrapperCssClass ''}
{/if}
{if !isset($fullWidth)}
    {assign fullWidth 0}
{/if}
{if !isset($id)}
    {assign id $name|replace:'[]':''}
{/if}
{if !isset($value)}
    {assign var="value" value=[] }
{/if}
<div class="form-group knowmates-form-group form-group-select {$wrapperCssClass}">
    <div class="row">
        <div class="{if $fullWidth == true }col-12{else}col-md-6{/if}">
            <label for="{$id}">
                <span>
                    {$label}
                </span>
                {if isset($description)}
                    {include $components->fieldDescription description="$description"}
                {/if}
            </label>
        </div>
        <div class="{if $fullWidth == true }col-12{else}col-md-6{/if} text-right">
            <div class="select-wrapper">
                <select multiple="multiple" class="form-control {$cssClass}" name="{$fieldName}" id="{$id}" {if isset($required) && $required == 'Y'}required{/if}>
                    {foreach from=$options item=option}
                        <option value="{$option['value']}" {if in_array($option['value'],$value)}selected{/if}>
                            {$option['label']|utf8_encode}
                        </option>
                    {/foreach}
                </select>
            </div>
        </div>
    </div>
</div>