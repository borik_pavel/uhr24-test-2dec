{include
$fields->number
name=$field['name']
label="{knmLang key=$field['label']}"
value=$field['value']
required=$field['required']
description="{knmLang key=$field['description']}"
step=$field['step']
max=$field['max']
min=$field['min']
json=1
}