{if isset($field['id'])}
    {assign var="id" value=$field['id']}
{else}
    {assign var="id" value=$field['name']}
{/if}
{knm_select
    id=$id
    name=$field['name']
    label="{knmLang key=$field['label']}"
    required=$field['required']
    disable_search=$field['disable_search']
    multiple=$field['multiple']
    description="{knmLang key=$field['description']}"
    options=$field->options
    json=1
}
{foreach $field->options as $option}
    {if $field['multiple'] == 'true'}
        {assign var="fieldValuesArray" value="|"|explode: $field['value']}

        <option value="{$option['value']}"
                {if in_array($option['value'],$fieldValuesArray)}selected{/if}>{knmLang key=$option['label']}</option>
    {else}
        <option value="{$option['value']}"
                {if $option['value']|trim == $field['value']|trim}selected{/if}>{knmLang key=$option['label']}</option>
    {/if}

{/foreach}
{/knm_select}