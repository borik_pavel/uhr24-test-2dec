{include
$fields->text
name=$field['name']
id=$field['id']
label="{knmLang key=$field['label']}"
value=$field['value']
required=$field['required']
fullwidth=$field['fullwidth']
description="{knmLang key=$field['description']}"
json=1
}