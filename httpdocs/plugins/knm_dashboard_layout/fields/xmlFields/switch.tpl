{include
$fields->switch
name=$field['name']
id=$field['id']
label="{knmLang key=$field['label']}"
checked=intval($field['value'])
required=$field['required']
description="{knmLang key=$field['description']}"
json=1
}