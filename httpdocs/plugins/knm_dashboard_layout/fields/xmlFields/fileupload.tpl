
{include
$fields->fileupload
name=$field['name']
label="{knmLang key=$field['label']}"
value=$field['value']
required=$field['required']
fileAllowedExtensions=$field['fileAllowedExtensions']
multiple=$field['multiple']
fullwidth=$field['fullwidth']
id=$field['id']

description="{knmLang key=$field['description']}"
json=1
}

