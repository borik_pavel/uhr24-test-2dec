{include
$fields->typeahead
name=$field['name']
ajaxName=$field['ajaxName']
ajaxParentName=$field['ajaxParentName']
ajaxKey=$field['ajaxKey']
label="{knmLang key=$field['label']}"
value=$field['value']
required=$field['required']
multiple=$field['multiple']
fullwidth=$field['fullwidth']
id=$field['id']
function=$field['function']
description="{knmLang key=$field['description']}"
json=1
}

