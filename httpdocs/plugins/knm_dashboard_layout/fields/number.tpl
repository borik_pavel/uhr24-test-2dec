{if !isset($json)}
    {assign json 0}
{/if}
{if $json == true}
    {assign fieldName "json[{$name}]"}
{else}
    {assign fieldName $name}
{/if}
{if !isset($value)}
    {assign value 0}
    {if isset($min) && $min > 0}
        {assign var="value" value=$min}
    {/if}
{/if}
{if !isset($fullwidth) OR $fullwidth == false}
    {assign colClass 'col-md-6'}
    {assign formGroupClass ''}
{else}
    {assign colClass 'col-md-12'}
    {assign formGroupClass 'form-group-fullwidth'}
{/if}
{if !isset($id)}
    {assign id $name}
{/if}
{if !isset($cssClass)}
    {assign cssClass ''}
{/if}
{if !isset($wrapperCssClass)}
    {assign wrapperCssClass ''}
{/if}
{if !isset($pattern)}
    {assign pattern '[-]?[0-9]*[.|,]?[0-9]+'}
{else}
    {if $pattern === 'int'}
        {assign pattern '[-]?[0-9]*'}
    {elseif $pattern === 'decimal'}
        {assign pattern '[-]?[0-9]*[.|,]?[0-9]+'}
    {/if}
{/if}
{if !isset($hideErrorMessage)}
    {assign var="hideErrorMessage" value=false}
{/if}
{if !isset($onblur)}
    {assign var="onblur" value="knm_change_number_val"}
{/if}
{if !isset($increaseValue)}
    {assign var="increaseValue" value="knm_add_number_val"}
{/if}
{if !isset($decreaseValue)}
    {assign var="decreaseValue" value="knm_dec_number_val"}
{/if}
{if !isset($hidebuttons)}
    {assign var="hidebuttons" value=false}
{/if}
{if !isset($description)}
    {assign description ''}
{/if}
{if !isset($readonly)}
    {assign var="readonly" value=false}
{/if}
<div class="form-group knowmates-form-group {$formGroupClass} {$wrapperCssClass}">
    <div class="row">
        <div class="{$colClass}">
            <label for="{$name}">
                {$label}
                {if isset($description)}
                    {include $components->fieldDescription description="$description"}
                {/if}
            </label>
        </div>
        <div class="{$colClass} error-field">
            <div class="knm-number-wrapper">
                <input value="{$value}"
                       id="{$name}"
                       name="{$fieldName}"
                       type="text"
                       onblur="{$onblur}(this)"
                       pattern="{$pattern}"
                       {if isset($step)}step="{$step}"{/if}
                        {if isset($max)}max="{$max}"{/if}
                        {if isset($min)}min="{$min}"{/if}
                        {if isset($roundTo)}data-round="{$roundTo}"{/if}
                        {if isset($required) && $required == 'Y'}required{/if}
                        {if $readonly}readonly{/if}
                       class="form-control knm-number-input {$cssClass}"
                        {if $hidebuttons == true}
                            style="padding-right: 4px"
                        {/if}
                >
                {if isset($endAdornment)}
                    <div class="end-adornment">
                        {$endAdornment}
                    </div>
                {/if}
                {if $hidebuttons == false}
                    <div class="d-flex flex-column up-down-btn">
                        <button type="button" onclick="{$increaseValue}(document.getElementById('{$id}'))"
                                class="btn btn-default position-relative mb-0 mt-auto">
                            <i class="dropDown-icon rotate-180-after"></i>
                        </button>
                        <button onclick="{$decreaseValue}(document.getElementById('{$id}'))" type="button"
                                class="btn btn-default position-relative mt-0 mb-auto">
                            <i class="dropDown-icon"></i>
                        </button>
                    </div>
                {/if}
            </div>
            <div class="error-text mt-1 {if $hideErrorMessage == true}d-none{/if}" style="display: none;">
                {if isset($errorMessage)}
                    {$errorMessage}
                {else}
                    {if isset($pattern) && $pattern === 'int'}
                        {knmLang key="number_error_message_int" section="dashboard"}
                    {else}
                        {knmLang key="number_error_message" section="dashboard"}
                    {/if}
                    {if isset($max) && isset($min)}
                        {knmLang key="number_error_message_range" section="dashboard" min=$min max=$max}
                    {elseif isset($min)}
                        {knmLang key="number_error_message_min" section="dashboard" min=$min}
                    {elseif isset($max)}
                        {knmLang key="number_error_message_max" section="dashboard" max=$max}
                    {/if}
                    {if isset($step)}
                        {knmLang key="number_error_message_step" section="dashboard" step=$step}
                    {/if}
                {/if}
            </div>
        </div>
    </div>
</div>