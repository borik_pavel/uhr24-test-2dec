{if !isset($json)}
    {assign json 0}
{/if}
{if $json == true}
    {assign fieldName "json[{$name}]"}
{else}
    {assign fieldName $name}
{/if}
{if !isset($value)}
    {assign value ''}
{/if}
{if !isset($id)}
    {assign id $name}
{/if}
{if !isset($cssClass)}
    {assign cssClass ''}
{/if}
{if !isset($wrapperCssClass)}
    {assign wrapperCssClass ''}
{/if}
{if !isset($rows)}
    {assign rows 2}
{/if}
{if !isset($readonly)}
    {assign var="readonly" value=false}
{/if}
<div class="form-group knowmates-form-group {$wrapperCssClass} form-group-fullwidth">
    <div class="row">
        <div class="col-12">
            <label for="{$id}">
                <span>
                    {$label}
                </span>
                {if isset($description)}
                    {include $components->fieldDescription description="$description"}
                {/if}
            </label>
        </div>
        <div class="col-12 d-flex align-items-center">
            <textarea id="{$id}" name="{$fieldName}" type="text" class="form-control knm_css_area {$cssClass}"
                   {if isset($required) && $required == 'Y'}required{/if}
                    {if $readonly}readonly{/if}>{$value}</textarea>
        </div>
    </div>
</div>
<script>
    if(typeof CodeMirror != "undefined") {
        let cssEditor = CodeMirror.fromTextArea(document.getElementById('{$id}'), {
            mode: "css",
            theme: "default material-darker",
            autoRefresh: true,
        });
    }
</script>