{if !isset($json)}
    {assign json 0}
{/if}
{if $json == true}
    {assign fieldName "json[{$name}]"}
{else}
    {assign fieldName $name}
{/if}
{if !isset($checked)}
    {assign checked 0}
{/if}
{if !isset($wrapperCssClass)}
    {assign wrapperCssClass ''}
{/if}
{if !isset($readonly)}
    {assign var="readonly" value=false}
{/if}
<div class="radio {$wrapperCssClass}">
    <label class="article_checkBox-label" style="cursor: pointer">
        <input {if isset($cssClass)}class="{$cssClass}"{/if} name="{$fieldName}"
               type="radio"
                {if isset($onclick)}onclick="{$onclick}"{/if}
                {if $checked == true}checked=""{/if}
                {if $readonly}readonly{/if}
                {if isset($required) && $required == 'Y'}required{/if}
               value="{$value}"
                {if isset($id)}id="{$id}"{/if}
        >{if isset($label)}{$label}{/if}
    </label>
</div>