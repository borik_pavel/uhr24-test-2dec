1. [Dashboard Components](#dashboard-components)
	1. [Layout Components](#layout-components)
		1. [Button](#button)
		1. [Icon Button](#icon-button)
		1. [Save/Edit/Delete Button](#saveeditdelete-button)
		1. [Stepper](#stepper)
		1. [Accordion](#accordion)
		1. [Collapse](#collapse)
		1. [Setting Component](#setting-component)
		1. [Avatar](#avatar)
		1. [Modal](#modal)
		1. [Card](#card)
		1. [Cropper](#cropper)
		1. [Pagination](#Pagination)
	1. [Form-Fields](#form-fields)
		1. [Select](#select)
		1. [Text Input](#text-input)
		1. [Text Area](#text-area)
		1. [Hidden Input](#hidden-input)
		1. [Switch](#switch)
		1. [Number](#number)
		1. [Checkbox](#checkbox)
		1. [Range Slider](#range-slider)
		1. [Spectrum](#spectrum)
		1. [Divider](#divider)
1. [PluginDashboard Class](#plugindashboard-class)
	1. [Functions](#layout-components)
		1. [addCSS](#addcss)
		1. [addJS](#addjs)
		1. [addJSFooter](#addjsfooter)
		1. [setTitle](#settitle)
		1. [mergeFiles](#mergefiles)
		1. [getXmlFormFields](#getxmlformfields)
		1. [compileSettingsToCssFile](#compileSettingsToCssFile)
		1. [displayDashboard](#displaydashboard)

# Dashboard Components

## Layout Components

### Button

#### Properties

| Prop            | Required      | Values  	| Default Value | Description |
| ----------------|:--------------|:------------|:--------------|:------------|
| buttonType      | optional      | "primary","secondary","danger","info","success","default" | "primary" | defines the layout of the submit button |
| class		  | optional      | string	|	| css class for button 	|
| icon            | optional      | string	|	| font-awesome css icon class |
| iconPosition    | optional      | "left" or "right" | "left" | icon position 	|
| id		  | optional      | string	| "left"| icon position		|
| onclick         | optional      | string	|	| js onclick handler	|
| onMouseOver     | optional      | string	|	| js hover handler	|
| style		  | optional      | string  |  | css styles |
| type            | optional      | string	| "button" | html-button-type	|
| variant         | optional      | "raised", "outlined" | | button variant	|

#### Usage

```smarty
{button icon="fa fa-globe"}button-text{/button}
```
### Icon Button

#### Properties

| Prop            | Required      | Values  | Default Value | Description |
| ----------------|:--------------|:--------|:------------|:-------------|
| icon            | yes		  | string |  | font-awesome css icon class |
| buttonType      | optional      | "primary","secondary","danger","info","success","default" | "primary" | defines the layout of the submit button |
| type            | optional      | string  | "button" | html-button-type |
| class		  | optional      | string  |  | css class for button |
| onclick         | optional      | string  |  | js onclick handler |
| style		  | optional      | string  |  | css styles |
| variant         | optional      | "raised", "outlined" | | button variant |

#### Usage

```smarty
{iconButton icon="fa fa-globe"}{/iconButton}
{iconButton}MM{/iconButton}
```

### Save/Edit/Delete Button

#### Properties

| Prop            | Required      | Values  	| Default Value | Description |
| ----------------|:--------------|:------------|:--------------|:------------|
| buttonType      | optional      | "primary","secondary","danger","info","success","default" | "primary" | defines the layout of the submit button |
| class	          | optional      | string	|	| css class for button 	|
| hideIcon        | optional	  | boolean	| false | hide button icon |
| hideText        | optional	  | boolean	| false | hide button text |
| icon            | optional      | string	|	| font-awesome css icon class |
| iconPosition    | optional      | "left" or "right" | "left" | icon position 	|
| id		  | optional      | string	| "left"| icon position		|
| onclick         | optional      | string	|	| js onclick handler	|
| onMouseOver     | optional      | string	|	| js hover handler	|
| style		  | optional      | string  |  | css styles |
| type            | optional      | string	| "button" | html-button-type	|
| variant         | optional      | "raised", "outlined" | | button variant	|

#### Usage

```smarty
{saveButton}{/saveButton} | {saveButton}custom-text{/saveButton}
{editButton}{/editButton} | {editButton}custom-text{/editButton}
{deleteButton}{/deleteButton} | {deleteButton}custom-text{/deleteButton}
```
### Stepper

#### Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| steps           | yes           | array   |              | array of steps |

#### steps array item 
| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| url             | yes           | string  |             | link url of step |
| icon            | optional      | string  | ""          | fonatwesome icon class |
| title           | optional      | string  |             | step title text |
| active	      | optional      | boolean |             | mark as active |

#### Usage

```smarty
    {include $components->stepper steps=$stepsArray}
```

### Accordion

#### Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      |string   |		  | css class 	|
| id              | yes           | string  |   |accordion id |
| open            | optional      | boolean | false | accordion is expanded on initial page load |
| style		  | optional      | string  |  | css styles |
| title           | optional      | string  | "" | accordion title |

#### Usage

```smarty
{accordion id="id"}content here{/accordion}
```

### Collapse

#### Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      | string  |		  | css class 	|
| id              | yes           | string  |   	  |accordion id |
| open            | optional      | boolean | false 	  | collapse is expanded on initial page load |
| style		  | optional      | string  | 		  | css styles |
| useOpenHandler  | optional      | boolean | false | creates an js function to open the collapse called "knm_collapse_{$id}" |

#### Usage

```smarty
{collapse id="id"}content here{/collapse}
```

### Setting Component

#### Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| alignContent	  | optional      | "left", "right" | "right" | defines align of content. If fullwidth === true and alignContent is not set: default value will be "left" |
| class	          | optional      | string  |		  | css class for content |
| description	  | optional      | string  | 		  | tooltip for label |
| fullWidth	  | optional      | boolean | false	  | adds 100% width to both label and content |
| label		  | optional      | string  | 		  | label for content |
| style		  | optional      | string  | 		  | css styles for content |
| wrapperClass	  | optional      | string  |		  | css class for wrapper |

#### Usage

```smarty
{setting label="content here button"}content here {button}click{/button}{/setting}
```

### Avatar

#### Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      | string  |		  | css class 	|
| backgroundImage | optional      | string  | 	 	  | avatar img	|
| style		  | optional      | string  | 		  | css styles 	|

#### Usage

```smarty
{avatar backgroundImage="https://via.placeholder.com/150"}{/avatar}
{avatar}AA{/avatar}
{avatar backgroundImage="https://via.placeholder.com/150"}AA{/avatar}
```

### Modal

#### Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| centered        | optional      | boolean | false 	  | modal centered vertical and horizontal 	|
| class	          | optional      | string  |		  | css class 	|
| id              | yes           | string  |   	  | modal-id	|
| static	  | optional      | boolean | false 	  | if true, modal do not close on backdrop click 	|
| style		  | optional      | string  |		  | css styles  |
| useOpenHandler  | optional      | boolean | false | creates an js function to open the modal called "open_modal_{$id}" |

#### Modal Header - Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      |string   |		  | css class 	|
| onClose         | optional      | js function |  | js onclick function executet on modal close |
| style		  | optional      | string  |		  | css styles  |
| title           | optional      | string  |		  | modal headline |

#### Modal Body - Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      |string   |		  | css class 	|
| style		  | optional      | string  |		  | css styles  |

#### Modal Actions - Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      |string   |		  | css class 	|
| style		  | optional      | string  |		  | css styles  |

#### Usage

```smarty
{modal}
	{modalHeader title="title"}{/modalHeader}
	{modalBody}content here{/modalBody}
	{modalActions}
		{button onclick="closeModal()"}close Modal{/button}
	{/modalActions}
{/modal}
```


### Card

#### Properties

| Prop            | Required      | Values  	| DefaultValue	| Description 	|
| ----------------|:--------------|:------------|:--------------|:--------------|
| class	          | optional      | string	|		| css class	|
| style		  | optional      | string	|		| css styles  	|

#### Card Header - Properties

| Prop            | Required      | Values  	| DefaultValue	| Description 	|
| ----------------|:--------------|:------------|:--------------|:--------------|
| class	          | optional      | string	|		| css class	|
| style		  | optional      | string	|		| css styles  	|
| avatar          | optional	  | html string |		| content inside the avatar of the card in the top left |
| title           | optional      | string 	| 		| card title 	|
| subTitle        | optional	  | string	|		| card subtitle |
| action	  | optional      | html string |		| defines an action button for the top right of the card element |

#### Card Media - Properties

| Prop            | Required      | Values  	| DefaultValue	| Description 	|
| ----------------|:--------------|:------------|:--------------|:--------------|
| class	          | optional      | string	|		| css class	|
| style		  | optional      | string	|		| css styles  	|
| image           | required      | string 	| 		| image source  |
| alt             | optional      | string 	| 		| alt description |
| title           | optional      | string 	| 		| image title	|

#### Card Content - Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      | string  |		  | css class 	|
| style		  | optional      | string  |		  | css styles  |

#### Card Actions - Properties

| Prop            | Required      | Values  | DefaultValue| Description |
| ----------------|:--------------|:--------|:------------|:------------|
| class	          | optional      | string  |		  | css class 	|
| style		  | optional      | string  |		  | css styles  |

#### Usage

```smarty
{card}
	{cardHeader title="title" avatar="<div class='avatar'>AA</div>"}{/cardHeader}
	{cardMedia image="https://via.placeholder.com/300"}{/cardMedia}
	{cardContent}
		content here
	{/cardContent}
	{cardActions}
		{button}button1{/button}
	{/cardActions}
{/card}
```
### Cropper

#### Properties

| Prop            | Required      | Values  	| DefaultValue	| Description 	|
| ----------------|:--------------|:------------|:--------------|:--------------|
| aspectRatio	  | optional      | number  	| 1		| initial aspect ration of cropper |
| navPos	  | optional      | "left", "top", "right", "botom" | "right"	| defines the position of the toolbar |
| class	          | optional      | string  	|		| css class 	|
| imageId	  | yes		  | string	|		| equals Content Image ID, required to create cropper instance |
| preview         | optional	  | string-selector, NodeList, Element, Element-Array	|		| define preview container for cropped image |
| style		  | optional      | string	|		| css styles  	|

#### Usage

```smarty
{cropper imageId="uploadPreview" preview=".preview"}
    <img id="uploadPreview" src="patho/to/image">
{/cropper}
```

### Pagination

#### Properties

| Prop            | Required      | Values  	| DefaultValue	| Description 	|
| ----------------|:--------------|:------------|:--------------|:--------------|
| class	          | optional      | string  	|		| css class 	|
| currentPage	  | optional      | number  	| 1		| initial page  |
| displayCount	  | optional      | number	| 5		| defines, how many pages are displayed (excluding first and last page) |
| displayFirstPage| optional      | boolean	| true		| allways display first page in pagination |
| displayLastPage | optional      | boolean	| true		| allways display last page in pagination |
| displayNextPage | optional      | boolean	| true		| display next page button |
| displayPrevPage | optional      | boolean	| true		| display previous page button |
| onclick	  | yes		  | string	| 		| name of js function for onclick-handler for pagination element, recieves the clicked page as a param |
| pageCount	  | yes		  | number	|		| amount of pages of pagination |
| style		  | optional      | string	|		| css styles  	|
| prevTabs        | optional      | number 	| calculated	| calcuclated by looking how many pages are displayed and how many can be displayed before the current page. Default Behavior: if its les than 50% of displayCount, additional tabs will be added after the current page 	|
| nextTabs        | optional      | number 	| calculated	| calcuclated by looking how many pages are displayed and how many can be displayed after the current page. Default Behavior: if its les than 50% of displayCount, additional tabs will be added before the current page 	|

#### Usage

```smarty
{pagination pageCount=10 currentPage=1 displayCount=3 onclick="handle_pagination_link"}{/pagination}
<script>
    const handle_pagination_link = (page) => window.location.href = "url?page="+page;
</script>
```

## Form-Fields

### Select

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:------------|:------------|
| class		| optional      | string  |  | css class for input |
| description   | optional      | string  |  | description for the input |
| fullWidth     | optional      | boolean | 0 | displays the inputField and the label with 100% width each |
| id	        | optional      | string  |  | id for checkbox |
| json	        | optional      | boolean | adds the value to the "json" array with prop "name" as key (json["name"]) |
| label         | yes           | string  |  | description for the select |
| multiple      | optional      | boolean | false | define select as multiselect |
| name		| yes           | string  | | input name, has to be an array(name[]) when used as multiselect |
| choosenOptions| yes           | array[optName => optValue]| ['width' => '100%','disable_search' => true, 'no_results_text' => knmLang(array('key' => 'no results for key', 'section'=> 'dashboard'))]  | array with choosen options (https://harvesthq.github.io/chosen/options.html) |
| placeholder   | optional      | string  | "" | placholder for select |
| required      | optional      | boolean | false | set input as required |
| value		| optional      | number/string |  | selected value |
| wrapperClass  | optional   	| string  |  		  | css class for container |

#### Usage

```smarty
    {select label="<em>Select</em>" name="se[]" id="se" multiple=true}
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    {/select}
    
    {select label="<em>Multiple Select</em>" name="se2" choosenOptions=['disable_search' => false]}
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    {/select}
```

Can be used in 2 ways:
via $fields: Each property passed by their own
via $xmlFields: All Properties passed as an associative Array

### Text Input

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:--------------|:------------|
| cssClass      | optional      | string  |  		  | css class for input |
| description   | optional      | string  |  		  | description for the input |
| fullWidth     | optional      | boolean | 0 		  | displays the inputField and the label with 100% width each |
| endAdorment   | optional      | html string |		  | adds Element after the input value |
| id	        | optional      | string  | = prop "name" | id of the input |
| json	        | optional      | "true"/1| 0 		  | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | yes           | string  |  		  | description for the input |
| name		| yes           | string  | 		  | input name |
| onkeyup       | optional      | js-function | 	  | js function on key up handler |
| placeholder   | optional      | string |		  | placeholder for input |
| required      | optional      | "Y"     | 0 		  | set input as required |
| value		| optional      | string  |		  | input value |
| wrapperCssClass | optional      | string  |  		  | css class for container |

#### Usage

```smarty
{include $fields->text name="fieldName"}
// $field['name'] = "fieldName"
{include $xmlFields->text field=$field}
```
### Text Area

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:--------------|:------------|
| cols 		| optional      | string/number | 	  | define initial cols for text area |
| cssClass      | optional      | string  |  		  | css class for input |
| description   | optional      | string  |  		  | description for the input |
| id	        | optional      | string  | = prop "name" | id of the input |
| json	        | optional      | "true"/1| 0 		  | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | yes           | string  |  		  | description for the input |
| name		| yes           | string  | 		  | input name |
| placeholder   | optional      | string |		  | placeholder for input |
| required      | optional      | "Y"     | 0 		  | set input as required |
| rows 		| optional      | string/number | 2	  | define initial rows for text area |
| value		| optional      | string  |		  | input value |
| wrapperCssClass | optional      | string  |  		  | css class for container |

#### Usage

```smarty
{include $fields->textArea name="fieldName"}
// $field['name'] = "fieldName"
{include $xmlFields->textArea field=$field}
```

### Hidden Input

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:------------|:------------|
| id	        | optional      | string  | = prop "name"  | id of the input |
| json	        | optional      | "true"/1| 0 | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| name		| yes           | string  | | input name |
| value		| optional      | string |  | input value |

#### Usage

```smarty
{include $fields->hidden name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->hidden field=$field}
```

### Switch

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:--------------|:------------|
| cssClass      | optional      | string  |  		  | css class for input |
| description   | optional      | string  |  | description for the input |
| id	        | optional      | string  | = prop "name"  | id of the input |
| json	        | optional      | "true"/1| 0 | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | yes           | string  |  | description for the input |
| name		| yes           | string  | | input name |
| required      | optional      | "Y"     | 0 | set input as required |
| value		| optional      | boolean | ""  | input value |
| wrapperCssClass | optional    | string  |  		  | css class for container |

#### Usage

```smarty
{include $fields->switch name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->switch field=$field}
```

### Number

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:------------|:------------|
| cssClass      | optional      | string  | | css class for input |
| description   | optional      | string  | | description for the input |
| endAdorment   | optional      | html string |		  | adds Element after the input value |
| fullWidth     | optional      | boolean | 0 | displays the inputField and the label with 100% width each |
| hideButtons   | optional      | boolean | false | hide the increase/decrease buttons of the input |
| hideErrorMessage | optional   | boolean | false | hide the erro message below the input |
| id	        | optional/required | string  | = prop "name"  | id of the input (has to be unique) |
| json	        | optional      | "true"/1| 0 | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | yes           | string  | | description for the input |
| max           | optional      | string  | | max value for input |
| min           | optional      | string  | | min value for input |
| name		| yes           | string  | | input name |
| pattern	| optional	| html-pattern/decimal/int | [-]?[0-9]*[.\|\,]?[0-9]+ | defines the input pattern: decimal=default value for decimal with either "." or "," and int = [-]?[0-9]*: any Integer Value |
| required      | optional      | "Y"     | | set input as required |
| step          | optional      | string  | | step value |
| value		| optional      | number  | | input value |
| wrapperCssClass | optional    | string  |  		  | css class for container |
| onblur        | optional      | js function name | | (event, inputNode) => onblur(event, inputNode) |
| increaseValue	| optional      | js function name | | (inputNode) => increaseValue(inputNode) |
| decreaseValue	| optional      | js function name | | (inputNode) => decreaseValue(inputNode) |

#### Usage

```smarty
{include $fields->number name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->number field=$field}
```
### Checkbox

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:------------|:------------|
| cssClass      | optional      | string  | | css class for input |
| json	        | optional      | "true"/1| 0 | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | optional      | string  | | checkbox label |
| name		| yes           | string  | | checkbox name |
| onclick       | optional      | js-function-string |  | js function |
| required      | optional      | "Y"     | | set input as required |
| value		| optional      | string  | | checkbox value |
| wrapperCssClass | optional    | string  |  		  | css class for container |

#### Usage

```smarty
{include $fields->checkbox name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->checkbox field=$field}
```

### Range Slider

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:------------|:------------|
| cssClass      | optional      | string  |  | css class for input |
| description   | optional      | string  |  | description for the input |
| fullWidth     | optional      | boolean | 0 | displays the inputField and the label with 100% width each |
| json	        | optional      | "true"/1| 0 | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | yes           | string  |  | description for the select |
| max           | optional      | string  | | max value for input |
| min           | optional      | string  | | min value for input |
| name		| yes           | string  | | input name |
| options       | yes           | options array[[label => string, value => string,number]]|  | array with select options |
| required      | optional      | "Y"     | | set input as required |
| step          | optional      | string  | | step value |
| value		| optional      | string/array |  | initial value |
| wrapperCssClass | optional    | string  |  		  | css class for container |

#### Usage

```smarty
{include $fields->multiSelect name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->multiSelect field=$field}
```

### Spectrum

Requires [$dashboardOptions->useSpectrum = true](#dashboard-options)

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:--------------|:------------|
| cssClass      | optional      | string  |  | css class for input 	|
| description   | optional      | string  |  | description for the input |
| id            | yes           | string  |  | html id |
| json	        | optional      | "true"/1| 0 | adds the value to teh "json" array with prop "name" as key (json["name"]) |
| label         | yes		| string  |  | description for the spectrum field |
| name		| yes           | string  | | input name |
| resetValue	| optional	| string  | = value | reset Value of input |
| showReset	| optional      | string  | | shows reset button of input |
| value		| optional      | number/string |  | input value |
| wrapperCssClass | optional    | string  |  		  | css class for container |

#### Usage

```smarty
{include $fields->spectrum name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->spectrum field=$field}
```

### Divider

Requires [$dashboardOptions->useSpectrum = true](#dashboard-options)

#### Properties

| Prop          | Required      | Values  | Default Value | Description |
| --------------|:--------------|:--------|:------------|:------------|
| label         | optional      | string  |  | divider label |

#### Usage

```smarty
{include $fields->spectrum name="fieldName"}

// $field['name'] = "fieldName"
{include $xmlFields->spectrum field=$field}
```

# PluginDashboard Class

## Usage

```php
$pluginDashboard = new Plugindasboard($oPlugin)
```

## Functions

### addCSS

params: any amount of strings with CSS-File Paths

```php
Plugindasboard::addCSS(string $pathToCss1,string $pathToCss2, ...)
```

### addCSS

Adds CSS-files to the header of the Plugin Dashboard.
> params: any amount of strings with CSS-file paths

```php
Plugindasboard::addCSS(string $pathToCss1,string $pathToCss2, ...)
```

### addJS

Adds JS-files to the header of the Plugin Dashboard.
> params: any amount of strings with JS-file paths

```php
Plugindasboard::addJS(string $pathToJs1, string $pathToJs2, ...)
```
### addJSFooter

Adds JS-files to the Footer of the Plugin Dashboard.

> params: any amount of strings with JS-file paths

```php
Plugindasboard::addJSFooter(string $pathToJs1,string $pathToJs2, ...)
```
### setTitle

Does set a HTML title for the Plugin.

> params: string

```php
Plugindasboard::setTitle(string $title)
```
### mergeFiles

Combines the Content of an array of files. If no new File Path is set, the Files will be saved to the first Path given in the Array.

> params: file path array, (newFilePath)

```php
Plugindasboard::mergeFiles(array $filePathsToCombine, string $newFilePath = '')
```

### getXmlFormFields

Get Formfields from an XML-file. Also adds the initial Values from the Initial-Values object, if the key of the Initial-Values objects and the name of the form field are equal.

> params: XML file path, (initialValues)

```php
Plugindasboard::getXmlFormFields(string $xmlFile, object $initialValues = null)
```

### compileSettingsToCssFile

Compile css Settings to a CSS File: Given Values will be assign to a smarty template file and transformed into a css file.

> params: values, compilerFTplFile, (cssFilePath)

```php
$pluginDashboard = new PluginDashboard($oPlugin);
$pluginDashboard->compileSettingsToCssFile(object/assoziativeArray $values, smartyTemplate $compilerTplFile, $cssFilePath = $pluginFrontendPfad . 'css/config.css)
```

### displayDashboard

Displays the Dashboard.

#### Dashboard Options

| option          | Values   	 | Default Value | Description 		   |
| ----------------|:-------------|:--------------|:------------------------|
| useSpectrum     | boolean      | false   	 | includes color picker js|
| useDatePicker   | boolean      | false   	 | includes datePicker js  |

#### Available Smarty Params

| param				| Value		| Description		|
| ------------------------------|:--------------|:----------------------|
| plugin_admin_path		| string		| path to plugin adminmenu 		|
| plugin_admin_path		| string		| path to plugin adminmenu tpl Folder 	|
| fields			| object		| [Form-Fields Object](#form-fields)	|
| xmlFields			| object		| [XML-Form-Fields Components Object](#form-fields)	|
| components			| object		| [Layout Components Object](#layout-components)	|
| currentlySelectedLanguage	| assoziative array	| example: array('name' => 'Deutsch', 'country' => 'DE','flag' => 'de','locale' => 'de_DE','langIso' => 'de')	|
| pluginTabBasepath		| string		| pluginUrl			|
| pluginLink			| string		| pluginUrl . '&cPluginTab='	|
| currentPluginId		| string		| plugin id			|

> params: (dashboardOptions)

```php
$pluginDashboard = new PluginDasboard($oPlugin)
$pluginTemplateFile = $pluginDashboard->displayDashboard(object $dashboardOptions = null);
```
