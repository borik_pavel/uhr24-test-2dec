<div class="col-xs-12 alert alert-warning">
	<div class="errortxt" style="color: #ffffff;">
		<i class="fas fa-exclamation-triangle"></i> {$message}
	</div>
</div>