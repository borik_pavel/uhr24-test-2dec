{if isset($jtl_license_tab_content) && $jtl_license_tab_content != ""}
    <div class="knm-jtl-license" style="margin-bottom: 64px;">
        <div class="col-xs-12 mt-4">
            <h2 style="display: block">JTL - Lizenz</h2>
        </div>
        <hr>
        {$jtl_license_tab_content}
    </div>
{/if}
<div class="col-xs-12 mt-4"">
    <h2 style="display: block">Knowmates - Lizenz</h2>
</div>
<hr>
<div class="d-flex flex-column form-row text-center">

    {if isset($status->code)}
        {if $status->type == 'danger'}
            {include file='./alert_danger.tpl' message=$status->message}
        {elseif $status->type == 'success'}
            {include file='./alert_success.tpl' message=$status->message}
        {elseif $status->type == 'warning'}
            {include file='./alert_warning.tpl' message=$status->message}
        {/if}
    {else}
    {/if}
</div>
<div id="license_btnGrp" class="d-flex justify-content-center">
    <div class="license_btnGrp_duo">
        <a href="{$pluginTabBasepath}{$tabId}&action=check" class="btn btn-outline-primary mx-2 license_btn"><i
                    class="fas fa-sync"></i> {knmLang key="request license" section="dashboard"}</a>
        {if !in_array($status->code, ['000', '120', '200', '250'])}
            <a href="{$pluginTabBasepath}{$tabId}&action=getDemo" class="btn btn-outline-primary mx-2 license_btn"><i
                        class="fa fa-laptop"></i> {knmLang key="try now for x days" days=$demoDuration section="dashboard"}
            </a>
        {/if}
    </div>
    <div class="license_btnGrp_duo">
        {if  $licenseType === '^(*(oo)*)^'}
            <a href="{$knowmatesShopUrl}" class="btn btn-outline-primary mx-2 license_btn"><i
                        class="fa fa-external-link-alt"></i>
                {if $status->code == "250"}
                    {knmLang key="renew license" section="dashboard"}
                {else}
                    {knmLang key="buy a license" section="dashboard"}
                {/if}
            </a>
        {/if}
        {if $licenseType === '(╯°□°）╯︵ ┻━┻)'}
            {if  $status->code !== "000"}
                <a href="{$pluginTabBasepath}{$tabId}&action=getFreeLicense"
                   class="btn btn-outline-primary mx-2 license_btn">
                    <i class="fas fa-key"></i> {knmLang key="activate your lifelong license" section="dashboard"}
                </a>
            {/if}
        {/if}
        {if $licenseType === '=^..^='}
            {if  $status->code !== "000"}
                <a href="{$knowmatesShopUrl}" target="_blank" class="btn btn-outline-primary mx-2 license_btn">
                    <i class="fas fa-key"></i> {knmLang key="get a license" section="dashboard"}
                </a>
            {/if}
        {/if}
    </div>
</div>