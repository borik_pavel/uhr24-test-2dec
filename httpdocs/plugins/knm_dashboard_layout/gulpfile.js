const {series, parallel, src, dest, watch} = require('gulp');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const csso = require('gulp-csso');
const gulpClone = require("gulp-clone");
const touch = require('gulp-touch-fd');
const eventStream = require('event-stream');

const config = {

    /** Browser Sync URL */
    proxyUrl: 'http://danmoi.sr',

    /** Changes to those files trigger browser sync to refresh page */
    browserSyncWatchedFiles: [
        './css/template.css'
    ],

    /** Changes to those files trigger gulp task */
    gulpWatchedFiles: [
        './**/custom-css.css',
        './**/dark-design.css',
        './**/*.scss',
    ],

    /** The path of the less file to compile via gulp */
    scssRootFilePath: './sass/template.scss',

    /** Production CSS file props */
    prodCss: {
        filename: 'template.css',
        basePath: './css'
    },

    /** Debug CSS file props */
    //debugCss: {
    //    filename: 'template.css',
    //    basePath: './'
    //}
};

config.default = config;



// DON'T CHANGE ANYTHING FROM HERE UNLESS NECESSARY
let initBrowserSync = function (done) {
    browserSync.init(config.default.browserSyncWatchedFiles, {
        proxy: config.default.proxyUrl,
        notify: false,
        online: false,
        ghostMode: {
            clicks: false,
            forms: false,
            scroll: false
        },
        open: false,
        reloadOnRestart: true
    });
    done();
};

let compile = function (done) {
    const css = src(config.default.scssRootFilePath)
        .pipe(sourcemaps.init())
        .pipe(sass({
            onError: browserSync.notify
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(csso().on('error', function(err) {
            console.log(err);
        }))
    ;

    const prod = css
        .pipe(gulpClone())
        .pipe(rename(config.default.prodCss.filename))
        .pipe(dest(config.default.prodCss.basePath))
        .pipe(touch())
    ;

    // const debug = css
    //     .pipe(gulpClone())
    //     .pipe(rename(config.default.debugCss.filename))
    //     .pipe(sourcemaps.write('./'))
    //     .pipe(dest(config.default.debugCss.basePath))
    //     .pipe(touch())
    // ;

    // eventStream.merge(prod, debug);
    eventStream.merge(prod);
    done();
};

let initWatch = function (done) {
    watch(config.default.gulpWatchedFiles, compile);
    done();
};

exports.default = series(compile, parallel(initBrowserSync, initWatch));
