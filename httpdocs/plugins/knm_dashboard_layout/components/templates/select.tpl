
<div class="form-group knowmates-form-group form-group-select {$params.wrapperClass->getValue()}">
    <div class="row">
        <div class="{if $params.fullWidth->getValue()}col-12{else}col-md-6{/if}">
            <label for="{$params.id->getValue()}">
                {$params.label->getValue()}
                {if $params.description->hasValue()}
                    {include $components->fieldDescription description=$params.description->getValue()}
                {/if}
            </label>
        </div>
        <div class="{if $params.fullWidth->getValue()}col-12{else}col-md-6{/if} text-right">
            <div class="select-wrapper">
                <select name="{$params.fieldName->getValue()}"
                        id="{$params.id->getValue()}"
                        {if $params.required->getValue()}required{/if}
                        {if $params.placeholder->hasValue()}data-placeholder="{$params.placeholder->getValue()}"{/if}
                        {if $params.multiple->getValue()}multiple{/if}
                        class="chosen-select-width form-control{$params.class->getValue()}"
                        tabindex="16">
                    {$content}
                </select>
            </div>
        </div>
    </div>
</div>
{literal}
<script>
    $("#{/literal}{$params.id->getValue()}{literal}").chosen({/literal}{$params.choosenOptions->getValue()}{literal});
</script>
{/literal}