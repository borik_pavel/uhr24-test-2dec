<div {if $params.id->hasValue()}id="{$params.id->getValue()}"{/if} class="knm_stepper_component{$params.class->getValue()}" {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if}>
    {$content}
</div>
{literal}
<script>
    $('.knm_stepper_component').knm_stepper({/literal}{$params.options->getValue()}{literal});
</script>
{/literal}