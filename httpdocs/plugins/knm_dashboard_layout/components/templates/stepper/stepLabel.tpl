<div class="knm_step-label-container{$params.class->getValue()}"
     {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if}>
    <span class="knm_step-icon">
        {if $parentParams.knm_step.step < $parentParams.knm_stepperComponent.activeStep}
            <i class="fas fa-check"></i>
        {elseif $params.stepIcon->hasValue()}
            {$params.stepIcon->getValue()}
        {else}
            {$parentParams.knm_step.step}
        {/if}
    </span>
    <span class="knm_step-label">{$content}</span>
</div>