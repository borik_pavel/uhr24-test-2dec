<div id="{$params.id->getValue()}" class="{$params.class->getValue()}" {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if}>
    {$content}
</div>
{if $params.useOpenHandler->getValue()}
{literal}
    <script>
        function knm_collapse_{/literal}{$params.id->getValue()}{literal}() {
            let ele = $("#{/literal}{$params.id->getValue()}{literal}");
            if (ele.css('display') === "none") {
                ele.slideDown();
            } else {
                ele.slideUp();
            }
        }
    </script>
{/literal}
{/if}