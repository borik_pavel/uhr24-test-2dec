<div class="pagination{$params.class->getValue()}" {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if}>
    {if $params.displayNextPage->getValue()}
    {knm_iconButton onclick="{$params.onclick->getValue()}({$params.currentPage->getValue()-1})" class="next-page" icon="fa fa-chevron-left" style="font-size:12px;" disabled={$params.currentPage->getValue() == 1}}prev page{/knm_iconButton}
    {/if}
    {if $params.displayFirstPage->getValue()}
        {knm_iconButton onclick="{$params.onclick->getValue()}(1)"}1{/knm_iconButton}
        {if ($params.currentPage->getValue() - $params.prevTabs->getValue()) > 2}
            <span class="dots">...</span>
        {/if}
    {/if}
    {for $index=1 to $params.prevTabs->getValue()}
        {assign page "{$params.currentPage->getValue() - 1 -$params.prevTabs->getValue() + $index}"}
        {knm_iconButton onclick="{$params.onclick->getValue()}({$page})"}{$page}{/knm_iconButton}
    {/for}
    {knm_iconButton onclick="{$params.onclick->getValue()}({$params.currentPage->getValue()})" variant="raised" buttonType="primary"}{$params.currentPage->getValue()}{/knm_iconButton}

    {for $index=1 to {$params.nextTabs->getValue()}}
        {assign page "{$params.currentPage->getValue() + $index}"}
        {knm_iconButton onclick="{$params.onclick->getValue()}({$page})"}{$page}{/knm_iconButton}
    {/for}
    {if $params.displayLastPage->getValue()}
        {if ($params.currentPage->getValue() + $params.nextTabs->getValue()) < ($params.pageCount->getValue() - 1)}
            <span class="dots">...</span>
        {/if}
        {knm_iconButton onclick="{$params.onclick->getValue()}({$params.pageCount->getValue()})"}{$params.pageCount->getValue()}{/knm_iconButton}
    {/if}
    {if $params.displayPrevPage->getValue()}
    {knm_iconButton onclick="{$params.onclick->getValue()}({$params.currentPage->getValue()+1})" class="prev-page" icon="fa fa-chevron-right" style="font-size:12px;" disabled={$params.currentPage->getValue() == $params.pageCount->getValue()}}{/knm_iconButton}
    {/if}
</div>