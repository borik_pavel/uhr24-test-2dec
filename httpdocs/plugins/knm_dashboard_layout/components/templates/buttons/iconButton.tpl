<button {if $params.onclick->hasValue()}onclick="{$params.onclick->getValue()}"{/if}
        class="icon-btn btn btn-{$params.buttonType->getValue()} {if $params.variant->hasValue()}btn-{$params.variant->getValue()}{/if}{$params.class->getValue()}"
        {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if}
        {if $params.onMouseOver->hasValue()}onMouseOver="{$params.onMouseOver->getValue()}"{/if}
        type="{$params.type->getValue()}"
        {if $params.disabled->getValue()}disabled=""{/if}
>
    {if $params.icon->hasValue() == true}
        <i class="{$params.icon->getValue()}"></i>
    {else}
        <span>
        {$content}
        </span>
    {/if}
</button>