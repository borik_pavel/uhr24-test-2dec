<div class="accordion settings-accordion mb-3{$params.class->getValue()}" {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if} id="{$params.id->getValue()}">
    <div class="card">
        <div class="card-header">
            <a data-toggle="collapse" data-target="#{$params.collapseId->getValue()}" aria-expanded="{$params.open->getValue(true)}"
               aria-controls="{$params.collapseId->getValue()}">{$params.title->getValue()}
            </a>
        </div>
        <div id="{$params.collapseId->getValue()}" class="collapse {if $params.open->getValue()}show{/if}" aria-labelledby="{$params.collapseId->getValue()}" data-parent="#{$params.id->getValue()}">
            <div class="card-body{if $params.contentClass->hasValue()} {$params.contentClass->getValue()}{/if}">
                {$content}
            </div>
        </div>
    </div>
</div>