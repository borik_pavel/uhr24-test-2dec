<div class="form-group knowmates-form-group set-comp {$params.wrapperClass->getValue()}" {if $params.id->hasValue()}id="{$params.id->getValue()}"{/if}>
    <div class="row">
        {if $params.label->hasValue()}
            <div class="{if !$params.fullWidth->getValue()}col-md-6{/if} col-12">
                <label>{$params.label->getValue()}
                    {if $params.description->hasValue()}
                        {include $components->fieldDescription description=$params.description->getValue()}
                    {/if}
                </label>
            </div>
        {/if}
        <div class="{if !$params.fullWidth->getValue()}col-md-6{/if} col-12 d-flex">
            <div class="{if $params.alignContent->getValue() == "left"}mr-auto{else}ml-auto{/if}{$params.class->getValue()} set-comp__content" {if $params.style->hasValue()}style="{$params.style->getValue()}"{/if}>
                {$content}
            </div>
        </div>
    </div>
</div>