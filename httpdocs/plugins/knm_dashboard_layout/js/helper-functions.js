function getCookie(name) {
    let v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

const knm_handle_endAdornments = () => $('.knm_custom_dashboard .end-adornment:not(.aligned):visible').each((index, ele) => alignEndAdornment(ele));


let currentDate = new Date();
let nextYear = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate());
let inTwoWeeks = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate() + 14);
document.cookie = "knmDesign=" + getCookie('knmDesign') + ";expires=" + nextYear.toUTCString() + ";path=/";


function toggleDarkDesign() {
    let darkDesignEnabled = document.getElementById("dark-css-file").disabled;
    document.getElementById("dark-css-file").disabled = !darkDesignEnabled;
    if (document.getElementById("dark-css-file-code-field")) {
        document.getElementById("dark-css-file-code-field").disabled = !darkDesignEnabled;
    }
    document.getElementById("design-swich").checked = darkDesignEnabled;
    if (darkDesignEnabled) {
        document.cookie = "knmDesign=dark;expires=" + nextYear.toUTCString() + ";path=/";
    } else {
        document.cookie = "knmDesign=light;expires=" + nextYear.toUTCString() + ";path=/";
    }
}

function hideUpdateNotification() {
    document.cookie = "knmUpdateNotification=hidden;expires=" + inTwoWeeks.toUTCString() + ";path=/";
    document.getElementById('updateShopInfoButton').style.display = 'none';
}

const knm_show_hide_ele = (ele, hide = true, display = "block") => {
    hide ? ele.style.display = 'none' : ele.style.display = display;
}

const knm_set_number_change_disable = (input, disable = true) => {
    input.parentElement.querySelector(".up-down-btn").querySelectorAll("button").forEach(
        button => {
            button.disabled = disable
        }
    );
}
const knm_isset_or_zero = number => number || number === 0;

const convert_decimal_to_lang = (dec) => {
    if (knm_dashboard_current_lang && knm_dashboard_current_lang === 'de') {
        return dec.toString().replace(/\./g, ',')
    } else {
        return dec.toString().replace(/,/g, '.')
    }
}

function knm_change_number_val(input, operation = "") {
    let errorComponent = input.closest('.error-field').querySelector(".error-text");
    let step = Number(input.getAttribute("step")) || 1;
    let min = input.getAttribute("min") && Number(input.getAttribute("min"));
    let max = input.getAttribute("max") && Number(input.getAttribute("max"));
    let roundCount = 0;
    if (input.dataset.round) {
        roundCount = input.dataset.round;
    } else if (input.getAttribute("step") && input.getAttribute("step").indexOf(".") !== -1) {
        roundCount = input.getAttribute("step").substring(input.getAttribute("step").indexOf(".") + 1).length
    }
    let value = Number(input.value.replace(/,/g, '.'));
    if (!input.validity.valid || isNaN(value)) {
        value = input.value;
        knm_set_number_change_disable(input);
        knm_show_hide_ele(errorComponent, false);
    } else {
        if (operation === 'add') {
            value = value + step;
        }
        if (operation === 'dec') {
            value = value - step;
        }
        if (knm_isset_or_zero(min) && value < min) {
            value = min;
            knm_show_hide_ele(errorComponent, false);
        } else {
            if (knm_isset_or_zero(max) && value > max) {
                value = max;
                knm_show_hide_ele(errorComponent, false);
            } else {
                knm_show_hide_ele(errorComponent);
            }
        }
    }
    value = typeof value === "number" ? value.toFixed(roundCount) : value;
    input.value = convert_decimal_to_lang(value);
}

const knm_add_number_val = (input) => {
    knm_change_number_val(input, "add");
}
const knm_dec_number_val = (input) => {
    knm_change_number_val(input, "dec");
}

$.fn.elementRealWidth = function () {
    $clone = this.clone()
        .css("visibility", "hidden")
        .appendTo($('body'));
    let $width = $clone.outerWidth();
    $clone.remove();
    return $width;
};
const alignEndAdornment = (adornment) => {
    adornment = $(adornment)
    let input = adornment.parent().find("input");
    let padding = parseFloat(input.css('padding-right')) + parseFloat(adornment.width()) + "px";
    input.css('padding-right', padding);
    adornment.addClass("aligned");
}

const reset_color_value = (eleId, value) => {
    document.getElementById(eleId + "_presentation").value = value;
    $("#" + eleId).spectrum("set", value);
}

const knm_add_to_array_if_not_included = (array, item) => {
    if (!array.includes(item)) {
        array.push(item);
    }
    return array
}

/* Stepper */
(function ($) {
    let defaults = {
        currentStep: 1,
        steps: 0,
        keepCompleted: true,
        goNext: null,
        completed: [],
        prevButtonSelector: '.knm_previous',
        nextButtonSelector: '.knm_next',
        clickCallback: null,
    };

    const toggle_completeState = (child, step) => {
        if ((child.dataset.step < step) || (defaults.completed.includes(Number(step)))) {
            child.classList.add('done')
            child.classList.remove('disable-click')
            $(child).find('.knm_step-icon').html('<i class="fas fa-check"></i>');
        } else {
            child.classList.remove('done')
            child.classList.add('disable-click')
            $(child).find('.knm_step-icon').html(child.dataset.step);
        }
    };

    const setActiveStep = (child, step) => {
        if (Number(child.dataset.step) === Number(step)) {
            child.classList.add('active')
            defaults.currentStep = Number(step);
            $(child).find('.step-icon').html(child.dataset.step);
            if (child.classList.contains('knm_step-connector') && defaults.completed.includes(Number(step) + 1)) {
                child.classList.add('done')
            }
        } else {
            child.classList.remove('active')
        }
    }

    const knm_handleChild = (child, step) => {
        toggle_completeState(child, step);
        setActiveStep(child, step);
    }

    const knm_clickEle = (stepperComponent, stepper, step, callback = null) => {
        stepperComponent.children().each(
            (index, child) => {
                if (child.classList.contains('knm_step_content')) {
                    if (Number(child.dataset.step) === Number(step)) {
                        child.classList.add('active')
                    } else {
                        child.classList.remove('active')
                    }
                }
            }
        );
        stepper.children().each((index, child) => knm_handleChild(child, step));
        if (callback) {
            callback();
        }
        if (defaults.clickCallback) {
            defaults.clickCallback();
        }
    }

    const disableButton = (button, disable = true) => {
        button.prop('disable', disable);
        if (disable === true) {
            button.addClass('disabled disable-click');
        } else {
            button.removeClass('disabled disable-click');
        }
    }

    const knm_set_nav_buttons = () => {
        let prevBtn = $(defaults.prevButtonSelector);
        let nextBtn = $(defaults.nextButtonSelector);
        disableButton(prevBtn, defaults.currentStep <= 1);
        disableButton(nextBtn, defaults.currentStep >= defaults.steps || !defaults.completed.includes(defaults.currentStep))
    }

    let methods = {
        init: (stepperComponent, stepper, options = {}) => {
            if (options.clickCallback) {
                options.clickCallback = eval(options.clickCallback);
            }
            defaults = {...defaults, ...options};
            stepper.children().each((index, child) => {
                $(child).off().click(() => knm_clickEle(stepperComponent, stepper, child.dataset.step))
            })
            defaults.steps = stepper.find('.knm_step-icon').length;
            knm_set_nav_buttons();
        },
        changeStep: (stepperComponent, stepper, callback = null) => {
            stepper.children().each((index, child) => {
                $(child).off().click(() => knm_clickEle(stepperComponent, stepper, child.dataset.step, callback))
            })
        },
        setStep: (stepperComponent, stepper, step, callback = null) => {
            knm_clickEle(stepperComponent, stepper, step, callback);
            knm_set_nav_buttons();
        },
        goNext: (stepperComponent, stepper, callback = null) => {
            if ((defaults.currentStep > 0) && (defaults.currentStep < defaults.steps)) {
                defaults.completed = knm_add_to_array_if_not_included(defaults.completed, defaults.currentStep - 1);
                defaults.currentStep++;
                knm_clickEle(stepperComponent, stepper, defaults.currentStep, callback)
            }
            knm_set_nav_buttons();
        },
        goPrevious: (stepperComponent, stepper, callback = null) => {
            if (defaults.currentStep > 1) {
                defaults.currentStep--;
                knm_clickEle(stepperComponent, stepper, defaults.currentStep, callback)
            }
            knm_set_nav_buttons();
        },
        addCompleted: (stepperComponent, stepper, step = null) => {
            defaults.completed = knm_add_to_array_if_not_included(defaults.completed, step || defaults.currentStep);
            stepper.children().each(
                (index, child) => toggle_completeState(child, child.dataset.step)
            );
        },
        removeCompleted: (stepperComponent, stepper, step) => {
            const index = defaults.completed.indexOf(step);
            if (index > -1) {
                defaults.completed.splice(index, 1);
            }
        },
        setCompleted: (stepperComponent, stepper, completed) => {
            defaults.completed = completed;
            stepper.children().each((index, child) => [
                toggle_completeState(child, child.dataset.step),
                Number(child.dataset.step) === defaults.currentStep && setActiveStep(child, child.dataset.step)
            ]);
        },
        reset: (stepperComponent, stepper) => {
            defaults.currentStep = 1;
            defaults.completed = [];
            methods.setStep(stepperComponent, stepper, defaults.currentStep);
        },
        disablePrevButton: (stepperComponent, stepper, disable = true) => disableButton($(defaults.prevButtonSelector), disable),
        disableNextButton: (stepperComponent, stepper, disable = true) => disableButton($(defaults.nextButtonSelector), disable),
    };
    $.fn.knm_stepper = function (methodOrOptions) {
        let stepperComponent = $(this);
        if (!stepperComponent.hasClass('knm_stepper_component')) {
            stepperComponent = stepperComponent.closest('.knm_stepper_component');
        }
        let stepper = stepperComponent.find('.knm_stepper');

        if (methods[methodOrOptions] || defaults[methodOrOptions]) {
            if (methods[methodOrOptions]) {
                return methods[methodOrOptions](stepperComponent, stepper, ...Array.prototype.slice.call(arguments, 1));
            } else {
                return defaults[methodOrOptions];
            }
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            return methods.init(stepperComponent, stepper, methodOrOptions);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on ele');
        }
    }
})(jQuery);
