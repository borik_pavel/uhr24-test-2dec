<?php
/* Smarty version 3.1.39, created on 2021-10-07 08:29:16
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_shopping/adminmenu/templates/custom_attributes.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615e93bc1e9db8_72161474',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a45106f4e604eaa7d6bd4bbef3e3a68d66e69c9' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_shopping/adminmenu/templates/custom_attributes.tpl',
      1 => 1615108376,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615e93bc1e9db8_72161474 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="alert alert-info"><?php echo __('Normally NOTHING will be changed here.');?>
</div>
<form method="post" enctype="multipart/form-data" name="export">
    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

    <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
    <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
    <input type="hidden" name="stepPlugin" value="alteAttr" />
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo __('ID');?>
</th>
                    <th><?php echo __('Google name');?>
</th>
                    <th class="min-w-sm"><?php echo __('Value name');?>
</th>
                    <th class="min-w-sm"><?php echo __('Value type');?>
</th>
                    <th class="text-center"><?php echo __('Active');?>
</th>
                    <th><?php echo __('Actions');?>
</th>
                </tr>
            </thead>
            <tbody>
            <?php if ($_smarty_tpl->tpl_vars['attribute_arr']->value) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['attribute_arr']->value, 'oAttribut');
$_smarty_tpl->tpl_vars['oAttribut']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oAttribut']->value) {
$_smarty_tpl->tpl_vars['oAttribut']->do_else = false;
?>
                    <?php $_smarty_tpl->_assignInScope('kAttribut', $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut);?>

                    <?php if ((isset($_smarty_tpl->tpl_vars['kindAttribute_arr']->value[$_smarty_tpl->tpl_vars['kAttribut']->value]))) {?>
                        <?php $_smarty_tpl->_assignInScope('cBackground', ' style="background-color: #f1f1f1; border:0px"');?>
                    <?php } else { ?>
                        <?php $_smarty_tpl->_assignInScope('cBackground', '');?>
                    <?php }?>

                    <tr>
                        <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
><?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
</td>
                        <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
>
                            <?php if ($_smarty_tpl->tpl_vars['oAttribut']->value->bStandard == 1) {?>
                                <?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->cGoogleName;?>

                            <?php } else { ?>
                                <input class="form-control" type="text" name="cGoogleName[<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->cGoogleName;?>
" />
                            <?php }?>
                        </td>
                        <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
>
                            <input class="form-control" type="text" name="cWertName[<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->cWertName;?>
" <?php if ((isset($_smarty_tpl->tpl_vars['kindAttribute_arr']->value[$_smarty_tpl->tpl_vars['kAttribut']->value]))) {?>style="display: none;"<?php }?> />
                        </td>
                        <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
>
                            <?php if ((isset($_smarty_tpl->tpl_vars['kindAttribute_arr']->value[$_smarty_tpl->tpl_vars['kAttribut']->value]))) {?>
                                <?php echo $_smarty_tpl->tpl_vars['eWertHerkunft']->value;?>

                            <?php } else { ?>
                                <select class="custom-select" name="eWertHerkunft[<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
]">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['eWertHerkunft_arr']->value, 'eWertHerkunft');
$_smarty_tpl->tpl_vars['eWertHerkunft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['eWertHerkunft']->value) {
$_smarty_tpl->tpl_vars['eWertHerkunft']->do_else = false;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['eWertHerkunft']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['eWertHerkunft']->value === $_smarty_tpl->tpl_vars['oAttribut']->value->eWertHerkunft) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['eWertHerkunft']->value;?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            <?php }?>
                        <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
 class="text-center">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="active-<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
" name="bAktiv[<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['oAttribut']->value->bAktiv == 1) {?>checked="true"<?php }?> />
                                <label class="custom-control-label" for="active-<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
"></label>
                            </div>
                        </td>
                        <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oAttribut']->value->bStandard)) && $_smarty_tpl->tpl_vars['oAttribut']->value->bStandard == 1) {?>
                                <button type="submit"
                                        name="btn_standard[<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
]"
                                        value="<?php echo __('Reset');?>
"
                                        class="btn btn-link" <?php if ((isset($_smarty_tpl->tpl_vars['kindAttribute_arr']->value[$_smarty_tpl->tpl_vars['kAttribut']->value]))) {?>style="display: none;"<?php }?>
                                        title="<?php echo __('Reset');?>
"
                                        data-toggle="tooltip">
                                    <span class="icon-hover">
                                        <span class="fal fa-undo"></span>
                                        <span class="fas fa-undo"></span>
                                    </span>
                                </button>
                            <?php } else { ?>
                                <button type="submit"
                                        name="btn_delete[<?php echo $_smarty_tpl->tpl_vars['oAttribut']->value->kAttribut;?>
]"
                                        value="<?php echo __('Delete');?>
"
                                        class="btn btn-link"
                                        title="<?php echo __('Delete');?>
"
                                        data-toggle="tooltip">
                                     <span class="icon-hover">
                                        <span class="fal fa-trash-alt"></span>
                                        <span class="fas fa-trash-alt"></span>
                                    </span>
                                </button>
                            <?php }?>
                        </td>
                    </tr>
                    <?php if ((isset($_smarty_tpl->tpl_vars['kindAttribute_arr']->value[$_smarty_tpl->tpl_vars['kAttribut']->value]))) {?>
                        <tr>
                            <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
>&Gt;</td>
                            <td<?php echo $_smarty_tpl->tpl_vars['cBackground']->value;?>
 colspan="5">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><?php echo __('ID');?>
</th>
                                                <th><?php echo __('V-ID');?>
</th>
                                                <th><?php echo __('Google name');?>
</th>
                                                <th><?php echo __('Value name');?>
</th>
                                                <th><?php echo __('Value type');?>
</th>
                                                <th class="text-center"><?php echo __('Active');?>
</th>
                                                <th><?php echo __('Actions');?>
</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kindAttribute_arr']->value[$_smarty_tpl->tpl_vars['kAttribut']->value], 'oKindAttribut');
$_smarty_tpl->tpl_vars['oKindAttribut']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oKindAttribut']->value) {
$_smarty_tpl->tpl_vars['oKindAttribut']->do_else = false;
?>
                                            <tr>
                                                <td><?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
</td>
                                                <td><?php if ($_smarty_tpl->tpl_vars['oKindAttribut']->value->bStandard == 1) {
echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kVaterAttribut;
} else { ?><input class="form-control" type="text" name="kVaterAttribut[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kVaterAttribut;?>
" /><?php }?></td>
                                                <td><?php if ($_smarty_tpl->tpl_vars['oKindAttribut']->value->bStandard == 1) {
echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->cGoogleName;
} else { ?><input class="form-control" type="text" name="cGoogleName[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->cGoogleName;?>
" /><?php }?></td>
                                                <td><input class="form-control" type="text" name="cWertName[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->cWertName;?>
" /></td>
                                                <td>
                                                    <select class="custom-select" name="eWertHerkunft[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]">
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['eWertHerkunft_arr']->value, 'eWertHerkunft', false, 'cWertHerkunft');
$_smarty_tpl->tpl_vars['eWertHerkunft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cWertHerkunft']->value => $_smarty_tpl->tpl_vars['eWertHerkunft']->value) {
$_smarty_tpl->tpl_vars['eWertHerkunft']->do_else = false;
?>
                                                            <option value="<?php echo $_smarty_tpl->tpl_vars['eWertHerkunft']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['eWertHerkunft']->value === $_smarty_tpl->tpl_vars['oKindAttribut']->value->eWertHerkunft) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['cWertHerkunft']->value;?>
</option>
                                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    </select>
                                                <td class="text-center">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" id="active-child-<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
"  name="bAktiv[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['oKindAttribut']->value->bAktiv == 1) {?>checked="true"<?php }?> />
                                                        <label class="custom-control-label" for="active-child-<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php if ((isset($_smarty_tpl->tpl_vars['oKindAttribut']->value->bStandard)) && $_smarty_tpl->tpl_vars['oKindAttribut']->value->bStandard == 1) {?>
                                                        <button type="submit"
                                                                name="btn_standard[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]"
                                                                value="<?php echo __('Reset');?>
"
                                                                class="btn btn-link"
                                                                title="<?php echo __('Reset');?>
"
                                                                data-toggle="tooltip">
                                                            <span class="icon-hover">
                                                                <span class="fal fa-undo"></span>
                                                                <span class="fas fa-undo"></span>
                                                            </span>
                                                        </button>
                                                    <?php } else { ?>
                                                        <button type="submit"
                                                                name="btn_delete[<?php echo $_smarty_tpl->tpl_vars['oKindAttribut']->value->kAttribut;?>
]"
                                                                value="<?php echo __('Delete');?>
"
                                                                class="btn btn-link"
                                                                title="<?php echo __('Delete');?>
"
                                                                data-toggle="tooltip">
                                                            <span class="icon-hover">
                                                                <span class="fal fa-trash-alt"></span>
                                                                <span class="fas fa-trash-alt"></span>
                                                            </span>
                                                        </button>
                                                    <?php }?>
                                                </td>
                                            </tr>
                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        <tr>
                    <?php }?>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php } else { ?>
                <tr>
                    <td colspan="5"><?php echo __('Currently no attributes exists');?>
</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
    <div class="row mr-0 mt-5">
        <div class="ml-auto col-sm-6 col-lg-auto">
            <button id="btn_reset_all" type="submit" name="btn_reset_all" value="<?php echo __('Reset all');?>
" class="btn btn-outline-primary">
                <i class="far fa-remove"></i> <?php echo __('Reset all');?>

            </button>
        </div>
        <div class="col-sm-6 col-lg-auto">
            <button type="submit" name="btn_save_old" value="<?php echo __('Save changes');?>
" class="btn btn-primary">
                <i class="far fa-save"></i> <?php echo __('Save changes');?>

            </button>
        </div>
    </div>
</form>
<div class="mt-7">
    <div class="subheading1 mb-3">
        <?php echo __('Create new attribute');?>

    </div>
    <form method="post" enctype="multipart/form-data" name="export">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
        <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
        <input type="hidden" name="stepPlugin" value="neuesAttr" />
        <table class="table">
            <tr>
                <td class="text-right"><label for="cGoogleName"><?php echo __('Google name');?>
:</label></td>
                <td><input class="form-control" id="cGoogleName" type="text" name="cGoogleName" value="<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cGoogleName']))) {
echo $_smarty_tpl->tpl_vars['requestData']->value['cGoogleName'];
}?>" required /></td>
                <td><?php echo __('How is the name of the attribute in Google export export file');?>
</td>
            </tr>
            <tr>
                <td class="text-right"><label for="eWertHerkunft"><?php echo __('Value type');?>
:</label></td>
                <td>
                    <select class="custom-select" name="eWertHerkunft" id="eWertHerkunft" required>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['eWertHerkunft_arr']->value, 'eWertHerkunft', false, 'cWertHerkunft');
$_smarty_tpl->tpl_vars['eWertHerkunft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cWertHerkunft']->value => $_smarty_tpl->tpl_vars['eWertHerkunft']->value) {
$_smarty_tpl->tpl_vars['eWertHerkunft']->do_else = false;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['eWertHerkunft']->value;?>
" <?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['eWertHerkunft'])) && $_smarty_tpl->tpl_vars['requestData']->value['eWertHerkunft'] === $_smarty_tpl->tpl_vars['eWertHerkunft']->value) {?>selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['cWertHerkunft']->value;?>
</option>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </select>
                </td>
                <td><?php echo __('Which field type should be used to export the value');?>
</td>
            </tr>
            <tr>
                <td class="text-right"><label for="cWertName"><?php echo __('Value name');?>
:</label></td>
                <td><input class="form-control" id="cWertName" type="text" name="cWertName" value="<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cWertName']))) {
echo $_smarty_tpl->tpl_vars['requestData']->value['cWertName'];
}?>" /></td>
                <td>
                    <?php echo __('Depending on the value type');?>
:<br/>
                    <b><?php echo __('Article property');?>
:</b> <?php echo __('Which property of article object contains the value');?>
<br />
                    <b><?php echo __('Function attribute');?>
:</b> <?php echo sprintf(__('Which type contains the value'),__('Function attribute'));?>
<br />
                    <b><?php echo __('Attribute');?>
:</b> <?php echo sprintf(__('Which type contains the value'),__('Attribute'));?>
<br />
                    <b><?php echo __('Feature');?>
:</b> <?php echo sprintf(__('Which type contains the value'),__('Feature'));?>
<br />
                    <b><?php echo __('Static value');?>
:</b> <?php echo __('This value will be exported as it is');?>
<br />
                    <b><?php echo __('Parent attribute');?>
:</b> <?php echo __('Leave this blank if attribute is a parent attribute');?>
<br />
                </td>
            </tr>
            <tr>
                <td class="text-right"><label for="bAktiv"><?php echo __('Active');?>
:</label></td>
                <td class="text-center">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" id="bAktiv" type="checkbox" name="bAktiv" value="1" <?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['bAktiv'])) && $_smarty_tpl->tpl_vars['requestData']->value['bAktiv'] == 1) {?>checked="true" <?php }?>/>
                        <label class="custom-control-label" for="bAktiv"></label>
                    </div>

                </td>
                <td><?php echo __('Should this attribute be exported');?>
</td>
            </tr>
            <tr>
                <td class="text-right"><label for="kVaterAttribut"><?php echo __('V-ID');?>
 (<?php echo __('Parent-ID');?>
:</label></td>
                <td><input class="form-control" id="kVaterAttribut" type="text" name="kVaterAttribut" value="<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['kVaterAttribut']))) {
echo $_smarty_tpl->tpl_vars['requestData']->value['kVaterAttribut'];
}?>" /></td>
                <td>
                    <?php echo __('Enter the ID of the parent attribute');?>

                    (<b class="text-uppercase"><?php echo __('Attention');?>
:</b> <?php echo __('Only IDs are allowed for which the value type Parent attribute is selected');?>
<br />
                    <?php echo __('Leave this blank, if this attribute is not a child attribute');?>

                </td>
            </tr>
        </table>
        <div class="row ml-0">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button type="submit" name="btn_save_new" value="Neues Attribut speichern" class="btn btn-primary btn-block">
                    <i class="fa fa-save"></i> <?php echo __('Save new attribute');?>

                </button>
            </div>
        </div>
    </form>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $('#btn_reset_all').on('click', function (e) {
        if (!window.confirm('<?php echo __('All attributes will be reseted');?>
')) {
            e.preventDefault();
        }
    });
<?php echo '</script'; ?>
>
<?php }
}
