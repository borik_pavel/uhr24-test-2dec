<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:08:02
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/snippets/einstellungen_log_icon.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e3c282f4d4_66597910',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '86f81c9d8ba70e76b77383d0cbef535306f178e7' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/snippets/einstellungen_log_icon.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e3c282f4d4_66597910 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['account']->value->oGroup->kAdminlogingruppe === 1) {?>
    <button class="btn btn-link px-1 py-0 setting-changelog"
        title="<?php echo __('settingLogTitle');?>
"
        data-toggle="tooltip"
        data-placement="top"
        data-setting-name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
"
        data-name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cName;?>
"
        data-id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->kEinstellungenConf;?>
"
        type="button"
    >
        <span class="icon-hover">
            <span class="fal fa-history"></span>
            <span class="fas fa-history"></span>
        </span>
    </button>
<?php }
}
}
