<?php
/* Smarty version 3.1.39, created on 2021-11-01 09:41:13
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/pluginverwaltung_lizenzkey.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_617fa829c9e3e6_43434596',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2946c15cf84aefa6ff0a8314b3520f03d440008' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/pluginverwaltung_lizenzkey.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/seite_header.tpl' => 1,
  ),
),false)) {
function content_617fa829c9e3e6_43434596 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>((__('pluginverwaltungLicenceKeyInput')).(': ')).($_smarty_tpl->tpl_vars['oPlugin']->value->getMeta()->getName()),'cBeschreibung'=>__('pluginverwaltungDesc')), 0, false);
?>
<div id="content">
    <form name="pluginverwaltung" method="post" action="pluginverwaltung.php">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="pluginverwaltung_uebersicht" value="1" />
        <input type="hidden" name="lizenzkeyadd" value="1" />
        <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />

        <div class="card">
            <div class="card-header">
                <div class="subheading1"><?php echo __('pluginverwaltungLicenceKeyInput');?>
</div>
                <hr class="mb-n3">
            </div>
            <div class="card-body">
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="cKey"><?php echo __('pluginverwaltungLicenceKey');?>
:</label>
                    <span class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <input id="cKey" placeholder="<?php echo __('pluginverwaltungLicenceKey');?>
" class="form-control" name="cKey" type="text" value="<?php if ((isset($_smarty_tpl->tpl_vars['oPlugin']->value->cLizenz))) {
echo $_smarty_tpl->tpl_vars['oPlugin']->value->cLizenz;
}?>" />
                    </span>
                </div>
            </div>
            <div class="card-footer save-wrapper">
                <div class="row">
                    <div class="ml-auto col-sm-6 col-xl-auto">
                        <button name="speichern" type="submit" value="<?php echo __('save');?>
" class="btn btn-primary btn-block">
                            <?php echo __('saveWithIcon');?>

                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php }
}
