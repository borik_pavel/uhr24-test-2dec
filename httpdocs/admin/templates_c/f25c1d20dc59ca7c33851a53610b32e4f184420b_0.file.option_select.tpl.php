<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:22:08
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/option_select.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e7104e4160_52189470',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f25c1d20dc59ca7c33851a53610b32e4f184420b' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/option_select.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e7104e4160_52189470 (Smarty_Internal_Template $_smarty_tpl) {
?><select class="custom-select" name="<?php echo $_smarty_tpl->tpl_vars['setting']->value->elementID;?>
" id="<?php echo $_smarty_tpl->tpl_vars['setting']->value->elementID;?>
">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['setting']->value->options, 'option');
$_smarty_tpl->tpl_vars['option']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->do_else = false;
?>
        <option value="<?php echo $_smarty_tpl->tpl_vars['option']->value->value;?>
" <?php if ($_smarty_tpl->tpl_vars['option']->value->value == $_smarty_tpl->tpl_vars['setting']->value->value) {?>selected="selected"<?php }?>><?php echo __($_smarty_tpl->tpl_vars['option']->value->name);?>
</option>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</select>
<?php }
}
