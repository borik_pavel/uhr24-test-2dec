<?php
/* Smarty version 3.1.39, created on 2021-11-04 19:45:31
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/opc/tpl/modals/error.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61842a4bd1c604_88185609',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3a5d667eb274a1c5f3555d3d2949afb7924dd94b' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/opc/tpl/modals/error.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61842a4bd1c604_88185609 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="errorModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="errorTitle"><?php echo __('error');?>
</h5>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="errorAlert">
                    <?php echo __('somethingHappend');?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
