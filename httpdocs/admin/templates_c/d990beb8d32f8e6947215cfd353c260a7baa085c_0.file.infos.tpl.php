<?php
/* Smarty version 3.1.39, created on 2021-11-18 11:07:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_paypal/adminmenu/templates/infos.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619625dfd675a4_61376593',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd990beb8d32f8e6947215cfd353c260a7baa085c' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_paypal/adminmenu/templates/infos.tpl',
      1 => 1632907929,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619625dfd675a4_61376593 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container-fluid">
    <?php if ((isset($_smarty_tpl->tpl_vars['errorMessage']->value)) && strlen($_smarty_tpl->tpl_vars['errorMessage']->value) > 0) {?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> <?php echo $_smarty_tpl->tpl_vars['errorMessage']->value;?>

        </div>
    <?php }?>
    <h2><?php echo __('Configuration');?>
</h2>
    <table class="table" id="paypal-test-credentials">
        <thead>
            <tr>
                <th><?php echo __('Payment method');?>
</th>
                <th class="text-center"><?php echo __('Mode');?>
</th>
                <th class="text-center"><?php echo __('Credentials');?>
</th>
                <th class="text-center"><?php echo __('Configuration');?>
<br><small class="text-muted"><?php echo __('with shipping method');?>
</small></th>
            </tr>
        </thead>
        <tbody>
            <tr class="basic">
                <td><?php echo __('PayPal Basic');?>
</td>
                <td class="payment-modus text-center"><i class="fa fa-spinner fa-spin"></i></td>
                <td class="payment-state text-center">
                    <i class="fa fa-spinner fa-spin"></i>
                </td>
                <td class="payment-linked text-center"><i class="fa fa-spinner fa-spin"></i></td>
            </tr>
            <tr class="express">
                <td><?php echo __('PayPal Express');?>
</td>
                <td class="payment-modus text-center"><i class="fa fa-spinner fa-spin"></i></td>
                <td class="payment-state text-center">
                    <i class="fa fa-spinner fa-spin"></i>
                </td>
                <td class="payment-linked text-center"><i class="fa fa-spinner fa-spin"></i></td>
            </tr>
            <tr class="plus">
                <td><?php echo __('PayPal PLUS');?>
</td>
                <td class="payment-modus text-center"><i class="fa fa-spinner fa-spin"></i></td>
                <td class="payment-state text-center"><i class="fa fa-spinner fa-spin"></i></td>
                <td class="payment-linked text-center"><i class="fa fa-spinner fa-spin"></i></td>
            </tr>
        </tbody>
    </table>
    <br />
    <p>
        <a href="http://jtl-url.de/paypaldocs" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o"></i> <?php echo __('Read the integration hand book');?>
</a>
    </p>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    var url = '<?php echo $_smarty_tpl->tpl_vars['post_url']->value;?>
';
    $(['basic', 'express', 'plus']).each(function(i, item) {
        check_payment(item);
    });

    function check_payment(type) {
        $.ajax({
            dataType: 'json',
            url: url+'&validate=' + type,
            success: function(data) {
                var error = data.msg || "<?php echo __('Invalid');?>
",
                    label_type = data['status'] === 'success' ? "<?php echo __('Valid');?>
" : error,
                    class_state = data['status'] === 'success' ? 'success' : 'danger',
                    state = '<small class="label label-'+class_state+'">'+label_type+'</small>',
                    label_linked = data['linked'] ? "<?php echo __('Yes');?>
" : "<?php echo __('No');?>
",
                    class_linked = data['linked'] ? 'success' : 'danger',
                    linked = '<small class="label label-'+class_linked+'">'+label_linked+'</small>',
                    mode = '<small class="label label-info">'+data['modus'].toUpperCase()+'</small>';

                $('tr.' + data['type'] + ' td.payment-state').html(state);
                $('tr.' + data['type'] + ' td.payment-linked').html(linked);
                $('tr.' + data['type'] + ' td.payment-modus').html(mode);
            }
        });
    }
<?php echo '</script'; ?>
>
<?php }
}
