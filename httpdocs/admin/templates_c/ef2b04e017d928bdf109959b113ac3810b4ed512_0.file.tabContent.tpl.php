<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/tabBar/tabContent.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fbfb616_92705771',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef2b04e017d928bdf109959b113ac3810b4ed512' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/tabBar/tabContent.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fbfb616_92705771 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['tabId']->value == $_smarty_tpl->tpl_vars['initialTab']->value) {?>active show<?php }?>"
     id="<?php echo $_smarty_tpl->tpl_vars['tabId']->value;?>
" role="tabpanel"
     aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['tabId']->value;?>
-tab">
    <div class="tabbertab">
        <h2 class="d-sm-none"><?php echo $_smarty_tpl->tpl_vars['tabName']->value;?>
</h2>
        <?php if ((isset($_smarty_tpl->tpl_vars['index']->value)) && (isset($_smarty_tpl->tpl_vars['subPages']->value[$_smarty_tpl->tpl_vars['index']->value]->url)) && $_smarty_tpl->tpl_vars['subPages']->value[$_smarty_tpl->tpl_vars['index']->value]->url != '') {?>
            <?php $_smarty_tpl->_assignInScope('pluginTemplateFileUrl', $_smarty_tpl->tpl_vars['subPages']->value[$_smarty_tpl->tpl_vars['index']->value]->url);?>
            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['plugin_template_path']->value)."/".((string)$_smarty_tpl->tpl_vars['pluginTemplateFileUrl']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tabId'=>$_smarty_tpl->tpl_vars['tabId']->value), 0, true);
?>
        <?php } else { ?>
            <?php if ('about_tab' == $_smarty_tpl->tpl_vars['tabId']->value) {?>
                <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['aboutPath']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tabId'=>$_smarty_tpl->tpl_vars['tabId']->value), 0, true);
?>
            <?php }?>
            <?php if ('lizenz_tab' == $_smarty_tpl->tpl_vars['tabId']->value) {?>
                <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['licensePath']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tabId'=>$_smarty_tpl->tpl_vars['tabId']->value), 0, true);
?>
            <?php }?>
        <?php }?>
    </div>
</div><?php }
}
