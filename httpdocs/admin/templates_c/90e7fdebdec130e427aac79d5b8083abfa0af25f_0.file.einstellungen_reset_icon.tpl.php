<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:44
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/snippets/einstellungen_reset_icon.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61652630efed00_27521376',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '90e7fdebdec130e427aac79d5b8083abfa0af25f' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/snippets/einstellungen_reset_icon.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61652630efed00_27521376 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['account']->value->oGroup->kAdminlogingruppe === 1) {?>
    <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'selectbox') {?>
        <?php $_smarty_tpl->_assignInScope('defaultValue', __(((string)$_smarty_tpl->tpl_vars['cnf']->value->cWertName)."_value(".((string)$_smarty_tpl->tpl_vars['cnf']->value->defaultValue).")"));?>
    <?php } else { ?>
        <?php $_smarty_tpl->_assignInScope('defaultValue', $_smarty_tpl->tpl_vars['cnf']->value->defaultValue);?>
    <?php }?>
    <button type="button"
            name="resetSetting"
            value="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
"
            class="btn btn-link p-0 <?php if ($_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert === $_smarty_tpl->tpl_vars['cnf']->value->defaultValue) {?>hidden<?php }?> delete-confirm btn-submit"
            title="<?php echo sprintf(__('settingReset'),$_smarty_tpl->tpl_vars['defaultValue']->value);?>
"
            data-toggle="tooltip"
            data-placement="top"
            data-modal-body="<?php echo sprintf(__('confirmResetLog'),__(((string)$_smarty_tpl->tpl_vars['cnf']->value->cWertName)."_name"),$_smarty_tpl->tpl_vars['defaultValue']->value);?>
"
            data-modal-title="<?php echo __('confirmResetLogTitle');?>
"
            data-modal-submit="<?php echo __('reset');?>
"
    >
        <span class="icon-hover">
            <span class="fal fa-refresh"></span>
            <span class="fas fa-refresh"></span>
        </span>
    </button>
<?php }
}
}
