<?php
/* Smarty version 3.1.39, created on 2021-10-07 08:29:16
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_shopping/adminmenu/templates/custom_exports.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615e93bc25c4c4_78315874',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '84dfc42f4b392480b7c9a1d290e9e7b49c5e2227' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_shopping/adminmenu/templates/custom_exports.tpl',
      1 => 1615108376,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615e93bc25c4c4_78315874 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="alert alert-info">
    <?php echo __('If you want to export more languages...');?>

</div>

<div>
    <form method="post" enctype="multipart/form-data" name="export">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
        <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
        <input type="hidden" name="stepPlugin" value="<?php echo $_smarty_tpl->tpl_vars['stepPlugin']->value;?>
" />

        <div class="subheading1"><?php echo __('Create new export format');?>
:</div>
        <hr>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="cName"><?php echo __('Export name');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <input class="form-control" type="text" id="cName" name="cName" value="<?php if ((isset($_POST['cName']))) {
echo $_POST['cName'];
}?>" required />
            </div>
        </div>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="cDateiname"><?php echo __('File name');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <input class="form-control" type="text" id="cDateiname" name="cDateiname" value="<?php if ((isset($_POST['cDateiname']))) {
echo $_POST['cDateiname'];
}?>" required />
            </div>
        </div>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="kSprache"><?php echo __('Language');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <select class="custom-select" id="kSprache" name="kSprache" required>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['gs_languages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->kSprache;?>
"><?php echo __($_smarty_tpl->tpl_vars['language']->value->cNameDeutsch);?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </select>
            </div>
        </div>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="kKundengruppe"><?php echo __('Customer group');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <select class="custom-select" name="kKundengruppe" id="kKundengruppe" required>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['gs_customerGroups']->value, 'customerGroup');
$_smarty_tpl->tpl_vars['customerGroup']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['customerGroup']->value) {
$_smarty_tpl->tpl_vars['customerGroup']->do_else = false;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['customerGroup']->value->kKundengruppe;?>
"><?php echo __($_smarty_tpl->tpl_vars['customerGroup']->value->cName);?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </select>
            </div>
        </div>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="kWaehrung"><?php echo __('Currency');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <select class="custom-select" name="kWaehrung" id="kWaehrung" required>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['gs_currencies']->value, 'currency');
$_smarty_tpl->tpl_vars['currency']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['currency']->value) {
$_smarty_tpl->tpl_vars['currency']->do_else = false;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['currency']->value->kWaehrung;?>
"><?php echo __($_smarty_tpl->tpl_vars['currency']->value->cName);?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </select>
            </div>
        </div>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="cLieferlandIso"><?php echo __('Shipping country');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <select class="custom-select" name="cLieferlandIso" id="cLieferlandIso" required>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['gs_shippingCountries']->value, 'country');
$_smarty_tpl->tpl_vars['country']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->do_else = false;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['country']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['country']->value;?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </select>
            </div>
        </div>
        <div class="row mr-0">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button type="submit" name="btn_save_new" value="Neuen Export anlegen" class="btn btn-primary btn-block">
                    <i class="fa fa-save"></i> <?php echo __('Create new export format');?>

                </button>
            </div>
        </div>
    </form>
</div>

<div class="mt-7">
    <div class="subheading1">
        <?php echo __('Exportformats');?>

    </div>
    <hr class="mb-4">
    <form method="post" enctype="multipart/form-data" name="export_delete">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
        <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
        <input type="hidden" name="stepPlugin" value="<?php echo $_smarty_tpl->tpl_vars['stepPlugin']->value;?>
" />
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?php echo __('Export name');?>
</th>
                        <th><?php echo __('File name');?>
</th>
                        <th class="text-center"><?php echo __('Language');?>
</th>
                        <th class="text-center"><?php echo __('Currency');?>
</th>
                        <th><?php echo __('Customer group');?>
</th>
                        <th class="text-center"><?php echo __('Shipping country');?>
</th>
                        <th class="text-center"><?php echo __('Actions');?>
</th>
                    </tr>
                </thead>
                <tbody>
                <?php if ($_smarty_tpl->tpl_vars['oExportformate']->value) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oExportformate']->value, 'oExportformat');
$_smarty_tpl->tpl_vars['oExportformat']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oExportformat']->value) {
$_smarty_tpl->tpl_vars['oExportformat']->do_else = false;
?>
                        <tr>
                            <td><?php echo $_smarty_tpl->tpl_vars['oExportformat']->value->cName;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['oExportformat']->value->cDateiname;?>
</td>
                            <td class="text-center"><?php echo __($_smarty_tpl->tpl_vars['oExportformat']->value->cSprache);?>
</td>
                            <td class="text-center"><?php echo __($_smarty_tpl->tpl_vars['oExportformat']->value->cWaehrung);?>
</td>
                            <td><?php echo __($_smarty_tpl->tpl_vars['oExportformat']->value->cKundengruppe);?>
</td>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['oExportformat']->value->cLieferlandIso;?>
</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="submit" name="btn_delete" value="<?php echo $_smarty_tpl->tpl_vars['oExportformat']->value->kExportformat;?>
" class="btn btn-link px-2">
                                        <span class="icon-hover">
                                            <span class="fal fa-trash-alt"></span>
                                            <span class="fas fa-trash-alt"></span>
                                        </span>
                                    </button>
                                    <a href="exportformate.php?action=edit&kExportformat=<?php echo $_smarty_tpl->tpl_vars['oExportformat']->value->kExportformat;?>
" class="btn btn-link px-2">
                                        <span class="icon-hover">
                                            <span class="fal fa-edit"></span>
                                            <span class="fas fa-edit"></span>
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <?php }?>
                </tbody>
            </table>
        </div>
    </form>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $('form button[name="btn_delete').on('click', function (e) {
        if (!window.confirm('<?php echo __('Are you really sure to delete this export format');?>
')) {
            e.preventDefault();
        }
    });
<?php echo '</script'; ?>
>
<?php }
}
