<?php
/* Smarty version 3.1.39, created on 2021-10-12 13:41:41
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/checkbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61657475110006_49797165',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '078711c0aaa716a571f72ede59febf322506581e' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/checkbox.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/pagination.tpl' => 2,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_61657475110006_49797165 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
 type='text/javascript'>
    
    function aenderAnzeigeLinks(bShow) {
        if (bShow) {
            document.getElementById('InterneLinks').style.display = 'block';
            document.getElementById('InterneLinks').disabled = false;
        } else {
            document.getElementById('InterneLinks').style.display = 'none';
            document.getElementById('InterneLinks').disabled = true;
        }
    }

    function checkFunctionDependency() {
        var elemOrt = document.getElementById('cAnzeigeOrt'),
            elemSF = document.getElementById('kCheckBoxFunktion');

        if (elemSF.options[elemSF.selectedIndex].value == 1) {
            elemOrt.options[2].disabled = true;
        } else if (elemSF.options[elemSF.selectedIndex].value != 1) {
            elemOrt.options[2].disabled = false;
        }
        if (elemOrt.options[elemOrt.selectedIndex].value == 3) {
            elemSF.options[2].disabled = true;
        } else if (elemOrt.options[elemOrt.selectedIndex].value != 3) {
            elemSF.options[2].disabled = false;
        }
    }
    
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('checkbox'),'cBeschreibung'=>__('checkboxDesc'),'cDokuURL'=>__('checkboxURL')), 0, false);
?>
<div id="content">
    <div class="tabs">
        <nav class="tabs-nav">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cTab']->value === '' || $_smarty_tpl->tpl_vars['cTab']->value === 'uebersicht') {?> active<?php }?>" data-toggle="tab" role="tab" href="#uebersicht">
                        <?php echo __('overview');?>

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'erstellen') {?> active<?php }?>" data-toggle="tab" role="tab" href="#erstellen">
                        <?php echo __('create');?>

                    </a>
                </li>
            </ul>
        </nav>
        <div class="tab-content">
            <div id="uebersicht" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['cTab']->value === '' || $_smarty_tpl->tpl_vars['cTab']->value === 'uebersicht') {?> active show<?php }?>">
                <?php if ((isset($_smarty_tpl->tpl_vars['oCheckBox_arr']->value)) && count($_smarty_tpl->tpl_vars['oCheckBox_arr']->value) > 0) {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'cAnchor'=>'uebersicht'), 0, false);
?>
                    <div id="tabellenLivesuche">
                        <form name="uebersichtForm" method="post" action="checkbox.php">
                            <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                            <input type="hidden" name="uebersicht" value="1" />
                            <input type="hidden" name="tab" value="uebersicht" />
                            <div>
                                <div class="subheading1"><?php echo __('availableCheckboxes');?>
</div>
                                <hr class="mb-3">
                                <div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-align-top">
                                            <thead>
                                                <tr>
                                                    <th class="th-1">&nbsp;</th>
                                                    <th class="th-1"><?php echo __('name');?>
</th>
                                                    <th class="th-2"><?php echo __('checkboxLink');?>
</th>
                                                    <th class="th-3"><?php echo __('checkboxLocation');?>
</th>
                                                    <th class="th-4"><?php echo __('checkboxFunction');?>
</th>
                                                    <th class="th-4 text-center"><?php echo __('requiredEntry');?>
</th>
                                                    <th class="th-5 text-center"><?php echo __('active');?>
</th>
                                                    <th class="th-5 text-center"><?php echo __('checkboxLogging');?>
</th>
                                                    <th class="th-6 text-center"><?php echo __('sorting');?>
</th>
                                                    <th class="th-7"><?php echo __('customerGroup');?>
</th>
                                                    <th class="th-8" colspan="2"><?php echo __('checkboxDate');?>
</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oCheckBox_arr']->value, 'oCheckBoxUebersicht');
$_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->index = -1;
$_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value) {
$_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->do_else = false;
$_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->index++;
$__foreach_oCheckBoxUebersicht_0_saved = $_smarty_tpl->tpl_vars['oCheckBoxUebersicht'];
?>
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input class="custom-control-input" name="kCheckBox[]" id="cb-check-<?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->index;?>
" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->kCheckBox;?>
" />
                                                            <label class="custom-control-label" for="cb-check-<?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->index;?>
"></label>
                                                        </div>
                                                    </td>
                                                    <td><label for="cb-check-<?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->index;?>
"><?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->cName;?>
</label></td>
                                                    <td><?php if ($_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->oLink !== null) {
echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->oLink->getName();
}?></td>
                                                    <td>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->kAnzeigeOrt_arr, 'kAnzeigeOrt', true);
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->iteration = 0;
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['kAnzeigeOrt']->value) {
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->do_else = false;
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->iteration++;
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->last = $_smarty_tpl->tpl_vars['kAnzeigeOrt']->iteration === $_smarty_tpl->tpl_vars['kAnzeigeOrt']->total;
$__foreach_kAnzeigeOrt_1_saved = $_smarty_tpl->tpl_vars['kAnzeigeOrt'];
?>
                                                            <?php echo $_smarty_tpl->tpl_vars['cAnzeigeOrt_arr']->value[$_smarty_tpl->tpl_vars['kAnzeigeOrt']->value];
if (!$_smarty_tpl->tpl_vars['kAnzeigeOrt']->last) {?>, <?php }?>
                                                        <?php
$_smarty_tpl->tpl_vars['kAnzeigeOrt'] = $__foreach_kAnzeigeOrt_1_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    </td>
                                                    <td><?php if ((isset($_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->oCheckBoxFunktion->cName))) {
echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->oCheckBoxFunktion->cName;
}?></td>

                                                    <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->nPflicht) {
echo __('yes');
} else {
echo __('no');
}?></td>
                                                    <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->nAktiv) {?><i class="fal fa-check text-success"></i><?php } else { ?><i class="fal fa-times text-danger"></i><?php }?></td>
                                                    <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->nLogging) {
echo __('yes');
} else {
echo __('no');
}?></td>
                                                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->nSort;?>
</td>
                                                    <td>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->kKundengruppe_arr, 'id', true);
$_smarty_tpl->tpl_vars['id']->iteration = 0;
$_smarty_tpl->tpl_vars['id']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['id']->value) {
$_smarty_tpl->tpl_vars['id']->do_else = false;
$_smarty_tpl->tpl_vars['id']->iteration++;
$_smarty_tpl->tpl_vars['id']->last = $_smarty_tpl->tpl_vars['id']->iteration === $_smarty_tpl->tpl_vars['id']->total;
$__foreach_id_2_saved = $_smarty_tpl->tpl_vars['id'];
?>
                                                            <?php echo Kundengruppe::getNameByID($_smarty_tpl->tpl_vars['id']->value);
if (!$_smarty_tpl->tpl_vars['id']->last) {?>, <?php }?>
                                                        <?php
$_smarty_tpl->tpl_vars['id'] = $__foreach_id_2_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    </td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->dErstellt_DE;?>
</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a href="checkbox.php?edit=<?php echo $_smarty_tpl->tpl_vars['oCheckBoxUebersicht']->value->kCheckBox;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"
                                                               class="btn btn-link px-2" title="<?php echo __('modify');?>
" data-toggle="tooltip">
                                                                <span class="icon-hover">
                                                                    <span class="fal fa-edit"></span>
                                                                    <span class="fas fa-edit"></span>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
$_smarty_tpl->tpl_vars['oCheckBoxUebersicht'] = $__foreach_oCheckBoxUebersicht_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer save-wrapper">
                                    <div class="row">
                                        <div class="col-sm-6 col-xl-auto text-left">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" name="ALLMSGS" id="ALLMSGS" type="checkbox" onclick="AllMessages(this.form);">
                                                <label class="custom-control-label" for="ALLMSGS"><?php echo __('globalSelectAll');?>
</label>
                                            </div>
                                        </div>
                                        <div class="ml-auto col-sm-6 col-xl-auto">
                                            <button name="checkboxLoeschenSubmit" class="btn btn-danger btn-block" type="submit" value="<?php echo __('delete');?>
">
                                                <i class="fas fa-trash-alt"></i> <?php echo __('delete');?>

                                            </button>
                                        </div>
                                        <div class="col-sm-6 col-xl-auto">
                                            <button name="checkboxDeaktivierenSubmit" class="btn btn-outline-primary btn-block" type="submit" value="<?php echo __('deactivate');?>
">
                                                <i class="fal fa-times text-danger"></i> <?php echo __('deactivate');?>

                                            </button>
                                        </div>
                                        <div class="col-sm-6 col-xl-auto">
                                            <button name="checkboxAktivierenSubmit" type="submit" class="btn btn-outline-primary btn-block" value="<?php echo __('activate');?>
">
                                                <i class="fal fa-check text-success"></i> <?php echo __('activate');?>

                                            </button>
                                        </div>
                                        <div class="col-sm-6 col-xl-auto">
                                            <button name="erstellenShowButton" type="submit" class="btn btn-primary btn-block" value="neue Checkbox erstellen">
                                                <i class="fa fa-share"></i> <?php echo __('checkboxCreate');?>

                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'cAnchor'=>'uebersicht','isBottom'=>true), 0, true);
?>
                <?php } else { ?>
                    <div class="alert alert-info" role="alert"><?php echo __('noDataAvailable');?>
</div>
                    <form method="post" action="checkbox.php">
                        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                        <input name="tab" type="hidden" value="erstellen" />
                        <button name="erstellenShowButton" type="submit" class="btn btn-primary" value="neue Checkbox erstellen"><i class="fa fa-share"></i> <?php echo __('checkboxCreate');?>
</button>
                    </form>
                <?php }?>
            </div>
            <div id="erstellen" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'erstellen') {?> active show<?php }?>">
                <div>
                    <div class="subheading1"><?php if ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBox)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBox > 0) {
echo __('edit');
} else {
echo __('create');
}?></div>
                        <hr class="mb-3">
                    <div>
                        <form method="post" action="checkbox.php" >
                            <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                            <input name="erstellen" type="hidden" value="1" />
                            <input name="tab" type="hidden" value="erstellen" />
                            <?php if ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBox)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBox > 0) {?>
                                <input name="kCheckBox" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBox;?>
" />
                            <?php } elseif ((isset($_smarty_tpl->tpl_vars['kCheckBox']->value)) && $_smarty_tpl->tpl_vars['kCheckBox']->value > 0) {?>
                                <input name="kCheckBox" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['kCheckBox']->value;?>
" />
                            <?php }?>

                            <div class="settings">
                                <div class="form-group form-row align-items-center<?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['cName']))) {?> form-error<?php }?>">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="cName"><?php echo __('name');?>
</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <input id="cName" name="cName" type="text" placeholder="Name" class="form-control" value="<?php if ((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['cName']))) {
echo $_smarty_tpl->tpl_vars['cPost_arr']->value['cName'];
} elseif ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->cName))) {
echo $_smarty_tpl->tpl_vars['oCheckBox']->value->cName;
}?>">
                                    </div>
                                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxName')),$_smarty_tpl ) );?>
</div>
                                </div>
                                <?php if (count($_smarty_tpl->tpl_vars['availableLanguages']->value) > 0) {?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                                        <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getCode());?>
                                        <?php $_smarty_tpl->_assignInScope('kSprache', $_smarty_tpl->tpl_vars['language']->value->getId());?>
                                        <?php $_smarty_tpl->_assignInScope('cISOText', "cText_".((string)$_smarty_tpl->tpl_vars['cISO']->value));?>
                                        <div class="form-group form-row align-items-center<?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['cText']))) {?> form-error<?php }?>">
                                            <label class="col col-sm-4 col-form-label text-sm-right" for="cText_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('text');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                                <textarea id="cText_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" placeholder="Text (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
)" class="form-control " name="cText_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php if ((isset($_smarty_tpl->tpl_vars['cPost_arr']->value[$_smarty_tpl->tpl_vars['cISOText']->value]))) {
echo $_smarty_tpl->tpl_vars['cPost_arr']->value[$_smarty_tpl->tpl_vars['cISOText']->value];
} elseif ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->oCheckBoxSprache_arr[$_smarty_tpl->tpl_vars['kSprache']->value]->cText))) {
echo $_smarty_tpl->tpl_vars['oCheckBox']->value->oCheckBoxSprache_arr[$_smarty_tpl->tpl_vars['kSprache']->value]->cText;
}?></textarea>
                                            </div>
                                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxText')),$_smarty_tpl ) );?>
</div>
                                        </div>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                                        <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getCode());?>
                                        <?php $_smarty_tpl->_assignInScope('kSprache', $_smarty_tpl->tpl_vars['language']->value->getId());?>
                                        <?php $_smarty_tpl->_assignInScope('cISOBeschreibung', "cBeschreibung_".((string)$_smarty_tpl->tpl_vars['cISO']->value));?>
                                        <div class="form-group form-row align-items-center <?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['cBeschreibung']))) {?> form-error<?php }?>">
                                            <label class="col col-sm-4 col-form-label text-sm-right" for="cBeschreibung_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('description');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                                <textarea id="cBeschreibung_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" class="form-control" name="cBeschreibung_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php if ((isset($_smarty_tpl->tpl_vars['cPost_arr']->value[$_smarty_tpl->tpl_vars['cISOBeschreibung']->value]))) {
echo $_smarty_tpl->tpl_vars['cPost_arr']->value[$_smarty_tpl->tpl_vars['cISOBeschreibung']->value];
} elseif ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->oCheckBoxSprache_arr[$_smarty_tpl->tpl_vars['kSprache']->value]->cBeschreibung))) {
echo $_smarty_tpl->tpl_vars['oCheckBox']->value->oCheckBoxSprache_arr[$_smarty_tpl->tpl_vars['kSprache']->value]->cBeschreibung;
}?></textarea>
                                            </div>
                                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxDescription')),$_smarty_tpl ) );?>
</div>
                                        </div>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                <?php }?>

                                <?php if ((isset($_smarty_tpl->tpl_vars['oLink_arr']->value)) && count($_smarty_tpl->tpl_vars['oLink_arr']->value) > 0) {?>
                                    <div class="form-group form-row align-items-center<?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['kLink']))) {?> form-error<?php }?>">
                                        <label class="col col-sm-4 col-form-label text-sm-right" for="nLink"><?php echo __('internalLinkTitle');?>
:</label>
                                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                            <div class="form-row align-items-center">
                                                <div class="col-xs-3 group-radio">
                                                    <label>
                                                    <input id="nLink" name="nLink" type="radio" class="" value="-1" onClick="aenderAnzeigeLinks(false);"<?php if ((!(isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['kLink'])) && (!(isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kLink)) || !$_smarty_tpl->tpl_vars['oCheckBox']->value->kLink)) || (isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['kLink'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nLink'] == -1) {?> checked="checked"<?php }?> />
                                                        <?php echo __('noLink');?>

                                                    </label>
                                                </div>
                                                <div class="col-xs-3 group-radio">
                                                    <label>
                                                        <input id="nLink2" name="nLink" type="radio" class="form-control2" value="1" onClick="aenderAnzeigeLinks(true);"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nLink'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nLink'] == 1) || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kLink)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->kLink > 0)) {?> checked="checked"<?php }?> />
                                                        <?php echo __('internalLink');?>

                                                    </label>
                                                </div>
                                                <div id="InterneLinks" style="display: none;">
                                                    <select name="kLink" class="custom-select">
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oLink_arr']->value, 'oLink');
$_smarty_tpl->tpl_vars['oLink']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oLink']->value) {
$_smarty_tpl->tpl_vars['oLink']->do_else = false;
?>
                                                            <option value="<?php echo $_smarty_tpl->tpl_vars['oLink']->value->kLink;?>
"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['kLink'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['kLink'] == $_smarty_tpl->tpl_vars['oLink']->value->kLink) || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kLink)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->kLink == $_smarty_tpl->tpl_vars['oLink']->value->kLink)) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['oLink']->value->cName;?>
</option>
                                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintInternalPage')),$_smarty_tpl ) );?>
</div>
                                    </div>
                                <?php }?>

                                <div class="form-group form-row align-items-center<?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['cAnzeigeOrt']))) {?> form-error<?php }?>">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="cAnzeigeOrt"><?php echo __('checkboxLocation');?>
:</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <select id="cAnzeigeOrt"
                                                name="cAnzeigeOrt[]"
                                                class="selectpicker custom-select"
                                                multiple="multiple"
                                                onClick="checkFunctionDependency();"
                                                data-selected-text-format="count > 2"
                                                data-size="7">
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cAnzeigeOrt_arr']->value, 'cAnzeigeOrt', false, 'key', 'anzeigeortarr', array (
));
$_smarty_tpl->tpl_vars['cAnzeigeOrt']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['cAnzeigeOrt']->value) {
$_smarty_tpl->tpl_vars['cAnzeigeOrt']->do_else = false;
?>
                                                <?php $_smarty_tpl->_assignInScope('bAOSelect', false);?>
                                                <?php if (!(isset($_smarty_tpl->tpl_vars['cPost_arr']->value['cAnzeigeOrt'])) && !(isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['cAnzeigeOrt'])) && !(isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kAnzeigeOrt_arr)) && $_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['CHECKBOX_ORT_REGISTRIERUNG']->value) {?>
                                                    <?php $_smarty_tpl->_assignInScope('bAOSelect', true);?>
                                                <?php } elseif ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kAnzeigeOrt_arr)) && count($_smarty_tpl->tpl_vars['oCheckBox']->value->kAnzeigeOrt_arr) > 0) {?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oCheckBox']->value->kAnzeigeOrt_arr, 'kAnzeigeOrt', true);
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->iteration = 0;
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['kAnzeigeOrt']->value) {
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->do_else = false;
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->iteration++;
$_smarty_tpl->tpl_vars['kAnzeigeOrt']->last = $_smarty_tpl->tpl_vars['kAnzeigeOrt']->iteration === $_smarty_tpl->tpl_vars['kAnzeigeOrt']->total;
$__foreach_kAnzeigeOrt_7_saved = $_smarty_tpl->tpl_vars['kAnzeigeOrt'];
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['kAnzeigeOrt']->value) {?>
                                                            <?php $_smarty_tpl->_assignInScope('bAOSelect', true);?>
                                                        <?php }?>
                                                    <?php
$_smarty_tpl->tpl_vars['kAnzeigeOrt'] = $__foreach_kAnzeigeOrt_7_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                <?php } elseif ((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['cAnzeigeOrt'])) && count($_smarty_tpl->tpl_vars['cPost_arr']->value['cAnzeigeOrt']) > 0) {?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cPost_arr']->value['cAnzeigeOrt'], 'cBoxAnzeigeOrt');
$_smarty_tpl->tpl_vars['cBoxAnzeigeOrt']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cBoxAnzeigeOrt']->value) {
$_smarty_tpl->tpl_vars['cBoxAnzeigeOrt']->do_else = false;
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['cBoxAnzeigeOrt']->value == $_smarty_tpl->tpl_vars['key']->value) {?>
                                                            <?php $_smarty_tpl->_assignInScope('bAOSelect', true);?>
                                                        <?php }?>
                                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                <?php }?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['bAOSelect']->value) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['cAnzeigeOrt']->value;?>
</option>
                                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </select>
                                    </div>
                                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintPlaceToShowCheckbox')),$_smarty_tpl ) );?>
</div>
                                </div>

                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="nPflicht"><?php echo __('requiredEntry');?>
:</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <select id="nPflicht" name="nPflicht" class="custom-select">
                                            <option value="Y"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nPflicht'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nPflicht'] === 'Y') || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nPflicht)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->nPflicht == 1)) {?> selected<?php }?>>
                                                <?php echo __('yes');?>

                                            </option>
                                            <option value="N"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nPflicht'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nPflicht'] === 'N') || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nPflicht)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->nPflicht == 0)) {?> selected<?php }?>>
                                                <?php echo __('no');?>

                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckCheckboxActivation')),$_smarty_tpl ) );?>
</div>
                                </div>

                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="nAktiv"><?php echo __('active');?>
:</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <select id="nAktiv" name="nAktiv" class="custom-select">
                                            <option value="Y"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nAktiv'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nAktiv'] === 'Y') || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nAktiv)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->nAktiv == 1)) {?> selected<?php }?>>
                                                <?php echo __('yes');?>

                                            </option>
                                            <option value="N"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nAktiv'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nAktiv'] === 'N') || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nAktiv)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->nAktiv == 0)) {?> selected<?php }?>>
                                                <?php echo __('no');?>

                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxActive')),$_smarty_tpl ) );?>
</div>
                                </div>

                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="nLogging"><?php echo __('checkboxLogging');?>
:</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <select id="nLogging" name="nLogging" class="custom-select">
                                            <option value="Y"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nLogging'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nLogging'] === 'Y') || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nLogging)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->nLogging == 1)) {?> selected<?php }?>>
                                                <?php echo __('yes');?>

                                            </option>
                                            <option value="N"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nLogging'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nLogging'] === 'N') || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nLogging)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->nLogging == 0)) {?> selected<?php }?>>
                                                <?php echo __('no');?>

                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxLogActivate')),$_smarty_tpl ) );?>
</div>
                                </div>

                                <div class="form-group form-row align-items-center<?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['nSort']))) {?> form-error<?php }?>">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="nSort"><?php echo __('sortHigherBottom');?>
:</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <input id="nSort" name="nSort" type="text" class="form-control" value="<?php if ((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nSort']))) {
echo $_smarty_tpl->tpl_vars['cPost_arr']->value['nSort'];
} elseif ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->nSort))) {
echo $_smarty_tpl->tpl_vars['oCheckBox']->value->nSort;
}?>" />
                                    </div>
                                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxOrder')),$_smarty_tpl ) );?>
</div>
                                </div>

                                <?php if ((isset($_smarty_tpl->tpl_vars['oCheckBoxFunktion_arr']->value)) && count($_smarty_tpl->tpl_vars['oCheckBoxFunktion_arr']->value) > 0) {?>
                                    <div class="form-group form-row align-items-center">
                                        <label class="col col-sm-4 col-form-label text-sm-right" for="kCheckBoxFunktion"><?php echo __('specialShopFunction');?>
:</label>
                                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                            <select class="custom-select" id="kCheckBoxFunktion" name="kCheckBoxFunktion" onclick="checkFunctionDependency();">
                                                <option value="0"></option>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oCheckBoxFunktion_arr']->value, 'oCheckBoxFunktion');
$_smarty_tpl->tpl_vars['oCheckBoxFunktion']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oCheckBoxFunktion']->value) {
$_smarty_tpl->tpl_vars['oCheckBoxFunktion']->do_else = false;
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['oCheckBoxFunktion']->value->kCheckBoxFunktion;?>
"<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['kCheckBoxFunktion'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['kCheckBoxFunktion'] == $_smarty_tpl->tpl_vars['oCheckBoxFunktion']->value->kCheckBoxFunktion) || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBoxFunktion)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->kCheckBoxFunktion == $_smarty_tpl->tpl_vars['oCheckBoxFunktion']->value->kCheckBoxFunktion)) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['oCheckBoxFunktion']->value->cName;?>
</option>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </select>
                                        </div>
                                        <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxFunction')),$_smarty_tpl ) );?>
</div>
                                    </div>
                                <?php }?>

                                <?php if (count($_smarty_tpl->tpl_vars['customerGroups']->value) > 0) {?>
                                    <div class="form-group form-row align-items-center<?php if ((isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['kKundengruppe']))) {?> form-error<?php }?>">
                                        <label class="col col-sm-4 col-form-label text-sm-right" for="kKundengruppe"><?php echo __('customerGroup');?>
:</label>
                                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                            <select id="kKundengruppe"
                                                    name="kKundengruppe[]"
                                                    class="selectpicker custom-select"
                                                    multiple="multiple"
                                                    data-selected-text-format="count > 2"
                                                    data-size="7"
                                                    data-actions-box="true">
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['customerGroups']->value, 'customerGroup', false, 'key', 'kundengruppen', array (
));
$_smarty_tpl->tpl_vars['customerGroup']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['customerGroup']->value) {
$_smarty_tpl->tpl_vars['customerGroup']->do_else = false;
?>
                                                <?php $_smarty_tpl->_assignInScope('bKGSelect', false);?>
                                                <?php if (!(isset($_smarty_tpl->tpl_vars['cPost_arr']->value['kKundengruppe'])) && !(isset($_smarty_tpl->tpl_vars['cPlausi_arr']->value['kKundengruppe'])) && !(isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kKundengruppe_arr)) && $_smarty_tpl->tpl_vars['customerGroup']->value->isDefault()) {?>
                                                    <?php $_smarty_tpl->_assignInScope('bKGSelect', true);?>
                                                <?php } elseif ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kKundengruppe_arr)) && count($_smarty_tpl->tpl_vars['oCheckBox']->value->kKundengruppe_arr) > 0) {?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oCheckBox']->value->kKundengruppe_arr, 'kKundengruppe');
$_smarty_tpl->tpl_vars['kKundengruppe']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['kKundengruppe']->value) {
$_smarty_tpl->tpl_vars['kKundengruppe']->do_else = false;
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['kKundengruppe']->value == $_smarty_tpl->tpl_vars['customerGroup']->value->getID()) {?>
                                                            <?php $_smarty_tpl->_assignInScope('bKGSelect', true);?>
                                                        <?php }?>
                                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                <?php } elseif ((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['kKundengruppe'])) && count($_smarty_tpl->tpl_vars['cPost_arr']->value['kKundengruppe']) > 0) {?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cPost_arr']->value['kKundengruppe'], 'kKundengruppe');
$_smarty_tpl->tpl_vars['kKundengruppe']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['kKundengruppe']->value) {
$_smarty_tpl->tpl_vars['kKundengruppe']->do_else = false;
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['kKundengruppe']->value == $_smarty_tpl->tpl_vars['customerGroup']->value->getID()) {?>
                                                            <?php $_smarty_tpl->_assignInScope('bKGSelect', true);?>
                                                        <?php }?>
                                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                <?php }?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['customerGroup']->value->getID();?>
"<?php if ($_smarty_tpl->tpl_vars['bKGSelect']->value) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['customerGroup']->value->getName();?>
</option>
                                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </select>
                                        </div>
                                        <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('hintCheckboxCustomerGroup')),$_smarty_tpl ) );?>
</div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="card-footer save-wrapper">
                            <div class="row">
                                <div class="ml-auto col-sm-6 col-xl-auto">
                                    <a class="btn btn-outline-primary btn-block" href="checkbox.php">
                                        <?php echo __('cancelWithIcon');?>

                                    </a>
                                </div>
                                <div class="col-sm-6 col-xl-auto">
                                    <button name="speichern" type="submit" value="<?php echo __('save');?>
" class="btn btn-primary btn-block">
                                        <?php echo __('saveWithIcon');?>

                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (((isset($_smarty_tpl->tpl_vars['cPost_arr']->value['nLink'])) && $_smarty_tpl->tpl_vars['cPost_arr']->value['nLink'] == 1) || ((isset($_smarty_tpl->tpl_vars['oCheckBox']->value->kLink)) && $_smarty_tpl->tpl_vars['oCheckBox']->value->kLink > 0)) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        aenderAnzeigeLinks(true);
    <?php echo '</script'; ?>
>
<?php }
$_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
