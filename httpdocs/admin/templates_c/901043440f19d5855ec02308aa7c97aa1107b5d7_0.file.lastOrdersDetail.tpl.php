<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/lastOrdersDetail.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165261fb3aee1_03890196',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '901043440f19d5855ec02308aa7c97aa1107b5d7' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/lastOrdersDetail.tpl',
      1 => 1611753980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165261fb3aee1_03890196 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h2 class="modal-title"> <?php echo __('Order details');?>
</h2>
            <button type="button" class="close" data-dismiss="modal">
                <i class="fal fa-times"></i>
            </button>
        </div>
        <div class="modal-body">
            <hr>
            <div class="last_order">
                <div class="row">
                    <div class="col-4"><?php echo __('Order number');?>
:</div>
                    <div class="col-8"><strong><?php echo $_smarty_tpl->tpl_vars['oBestellung']->value->cBestellNr;?>
</strong></div>
                </div>
                <div class="row">
                    <div class="col-4"><?php echo __('Order date');?>
:</div>
                    <div class="col-8"><strong><?php echo $_smarty_tpl->tpl_vars['oBestellung']->value->dErstelldatum_de;?>
</strong></div>
                </div>
            </div>
            <?php if (count($_smarty_tpl->tpl_vars['oBestellung']->value->Positionen) > 0) {?>
                <div class="mt-5">
                    <div class="table-responsive">
                        <?php if ((isset($_smarty_tpl->tpl_vars['oBestellung']->value->Positionen)) && count($_smarty_tpl->tpl_vars['oBestellung']->value->Positionen) > 0) {?>
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th><?php echo __('Item');?>
</th>
                                        <th><?php echo __('price');?>
</th>
                                    </tr>
                                </thead>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oBestellung']->value->Positionen, 'position');
$_smarty_tpl->tpl_vars['position']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['position']->value) {
$_smarty_tpl->tpl_vars['position']->do_else = false;
?>
                                    <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['cDetailPosition']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Position'=>$_smarty_tpl->tpl_vars['position']->value,'Bestellung'=>$_smarty_tpl->tpl_vars['oBestellung']->value), 0, true);
?>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </table>
                        <?php }?>
                    </div>
                </div>
            <?php }?>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="ml-auto col-sm-6 col-xl-auto">
                    <button type="button" class="btn btn-outline-primary btn-block" data-dismiss="modal"><?php echo __('Close');?>
</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
