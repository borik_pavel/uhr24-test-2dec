<?php
/* Smarty version 3.1.39, created on 2021-10-12 12:14:22
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/language_switcher.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61655ffe917024_28405628',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'de785015eef220d74391a8b9cb06e5d720ac9725' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/language_switcher.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61655ffe917024_28405628 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('onchange', (($tmp = $_smarty_tpl->tpl_vars['onchange']->value ?? null)===null||$tmp==='' ? true : $tmp));
$_smarty_tpl->_assignInScope('id', (($tmp = $_smarty_tpl->tpl_vars['id']->value ?? null)===null||$tmp==='' ? 'lang-switcher' : $tmp));?>
<form name="sprache" method="post" action="<?php echo (($tmp = $_smarty_tpl->tpl_vars['action']->value ?? null)===null||$tmp==='' ? '' : $tmp);?>
" class="inline_block">
    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

    <input type="hidden" name="sprachwechsel" value="1" />
    <div class="form-row">
        <label class="col-sm-auto col-form-label" for="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo __('changeLanguage');?>
:</label>
        <span class="col-sm-auto">
            <select id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" name="kSprache" class="custom-select selectBox"<?php if ($_smarty_tpl->tpl_vars['onchange']->value) {?> onchange="document.sprache.submit();"<?php }?>>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->getId();?>
" <?php if ($_smarty_tpl->tpl_vars['language']->value->getId() === $_SESSION['editLanguageID']) {
$_smarty_tpl->_assignInScope('currentLanguage', $_smarty_tpl->tpl_vars['language']->value->getLocalizedName());?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
</option>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
        </span>
    </div>
</form>
<?php }
}
