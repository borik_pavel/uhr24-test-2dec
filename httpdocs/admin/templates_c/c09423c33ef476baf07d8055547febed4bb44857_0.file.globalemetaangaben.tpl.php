<?php
/* Smarty version 3.1.39, created on 2021-10-07 08:28:50
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/globalemetaangaben.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615e93a2795096_96787605',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c09423c33ef476baf07d8055547febed4bb44857' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/globalemetaangaben.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/language_switcher.tpl' => 1,
    'file:snippets/einstellungen_icons.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_615e93a2795096_96787605 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('globalemetaangaben'),'cBeschreibung'=>__('globalemetaangabenDesc'),'cDokuURL'=>__('globalemetaangabenUrl')), 0, false);
$_smarty_tpl->_assignInScope('currentLanguage', '');?>
<div id="content">
    <div class="card">
        <div class="card-body">
            <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/language_switcher.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('action'=>'globalemetaangaben.php"'), 0, false);
?>
        </div>
    </div>
    <form method="post" action="globalemetaangaben.php">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="einstellungen" value="1" />
        <div class="settings">
            <div class="card">
                <div class="card-header">
                    <div class="subheading1"><?php echo $_smarty_tpl->tpl_vars['currentLanguage']->value;?>
</div>
                </div>
                <div class="card-body">
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 text-sm-right" for="Title"><?php echo __('title');?>
:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <input type="text" class="form-control" id="Title" name="Title" value="<?php if ((isset($_smarty_tpl->tpl_vars['oMetaangaben_arr']->value['Title']))) {
echo $_smarty_tpl->tpl_vars['oMetaangaben_arr']->value['Title'];
}?>" tabindex="1" />
                        </div>
                    </div>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 text-sm-right" for="Meta_Description"><?php echo __('globalemetaangabenMetaDesc');?>
:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <input type="text" class="form-control" id="Meta_Description" name="Meta_Description" value="<?php if ((isset($_smarty_tpl->tpl_vars['oMetaangaben_arr']->value['Meta_Description']))) {
echo $_smarty_tpl->tpl_vars['oMetaangaben_arr']->value['Meta_Description'];
}?>" tabindex="1" />
                        </div>
                    </div>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 text-sm-right" for="Meta_Description_Praefix"><?php echo __('globalemetaangabenMetaDescPraefix');?>
:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <input type="text" class="form-control" id="Meta_Description_Praefix" name="Meta_Description_Praefix" value="<?php if ((isset($_smarty_tpl->tpl_vars['oMetaangaben_arr']->value['Meta_Description_Praefix']))) {
echo $_smarty_tpl->tpl_vars['oMetaangaben_arr']->value['Meta_Description_Praefix'];
}?>" tabindex="1" />
                        </div>
                    </div>
                </div>
            </div>

            <?php $_smarty_tpl->_assignInScope('open', false);?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oConfig_arr']->value, 'oConfig');
$_smarty_tpl->tpl_vars['oConfig']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oConfig']->value) {
$_smarty_tpl->tpl_vars['oConfig']->do_else = false;
?>
                <?php if ($_smarty_tpl->tpl_vars['oConfig']->value->cConf === 'Y') {?>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 text-sm-right" for="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
"><?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cName;?>
:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 <?php if ($_smarty_tpl->tpl_vars['oConfig']->value->cInputTyp === 'number') {?>config-type-number<?php }?>">
                            <?php if ($_smarty_tpl->tpl_vars['oConfig']->value->cInputTyp === 'selectbox') {?>
                                <select name="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
" class="custom-select combo">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oConfig']->value->ConfWerte, 'wert');
$_smarty_tpl->tpl_vars['wert']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['wert']->value) {
$_smarty_tpl->tpl_vars['wert']->do_else = false;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['wert']->value->cWert;?>
" <?php if ($_smarty_tpl->tpl_vars['oConfig']->value->gesetzterWert == $_smarty_tpl->tpl_vars['wert']->value->cWert) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['wert']->value->cName;?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            <?php } elseif ($_smarty_tpl->tpl_vars['oConfig']->value->cInputTyp === 'number') {?>
                                <div class="input-group form-counter">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-outline-secondary border-0" data-count-down>
                                            <span class="fas fa-minus"></span>
                                        </button>
                                    </div>
                                    <input class="form-control" type="number" name="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['oConfig']->value->gesetzterWert))) {
echo $_smarty_tpl->tpl_vars['oConfig']->value->gesetzterWert;
}?>" tabindex="1" />
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-secondary border-0" data-count-up>
                                            <span class="fas fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['oConfig']->value->cWertName;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['oConfig']->value->gesetzterWert))) {
echo $_smarty_tpl->tpl_vars['oConfig']->value->gesetzterWert;
}?>" tabindex="1" />
                            <?php }?>
                        </div>
                        <?php $_smarty_tpl->_subTemplateRender('file:snippets/einstellungen_icons.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cnf'=>$_smarty_tpl->tpl_vars['oConfig']->value), 0, true);
?>
                    </div>
                <?php } else { ?>
                    <?php if ($_smarty_tpl->tpl_vars['open']->value) {?></div></div><?php }?>
                    <div class="card">
                        <?php if ($_smarty_tpl->tpl_vars['oConfig']->value->cName) {?>
                            <div class="card-header">
                                <div class="subheading1"><?php echo __('settings');?>
</div>
                                <hr class="mb-n3">
                            </div>
                        <?php }?>
                        <div class="card-body">
                        <?php $_smarty_tpl->_assignInScope('open', true);?>
                <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php if ($_smarty_tpl->tpl_vars['open']->value) {?>
                </div>
            </div>
            <?php }?>
        </div>

        <div class="card-footer save-wrapper submit">
            <div class="row">
                <div class="ml-auto col-sm-6 col-xl-auto">
                    <button name="speichern" type="submit" value="<?php echo __('save');?>
" class="btn btn-primary btn-block">
                        <?php echo __('saveWithIcon');?>

                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
