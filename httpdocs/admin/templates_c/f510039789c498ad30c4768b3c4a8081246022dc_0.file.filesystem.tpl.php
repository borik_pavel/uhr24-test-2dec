<?php
/* Smarty version 3.1.39, created on 2021-10-14 08:54:38
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/filesystem.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6167d42e33ffb5_21523651',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f510039789c498ad30c4768b3c4a8081246022dc' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/filesystem.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:snippets/colorpicker.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_6167d42e33ffb5_21523651 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('fs'),'cBeschreibung'=>__('fsDesc'),'cDokuURL'=>__('fsUrl')), 0, false);
?>

<div id="content">
    <div id="settings">
        <form method="post" action="filesystem.php">
            <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

            <?php $_smarty_tpl->_assignInScope('open', false);?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oConfig_arr']->value, 'cnf', false, NULL, 'conf', array (
));
$_smarty_tpl->tpl_vars['cnf']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cnf']->value) {
$_smarty_tpl->tpl_vars['cnf']->do_else = false;
?>
                <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cConf === 'Y') {?>
                    <div class="form-group form-row align-items-center item<?php if ((isset($_smarty_tpl->tpl_vars['cSuche']->value)) && $_smarty_tpl->tpl_vars['cnf']->value->kEinstellungenConf == $_smarty_tpl->tpl_vars['cSuche']->value) {?> highlight<?php }?>">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
"><?php echo $_smarty_tpl->tpl_vars['cnf']->value->cName;?>
</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'number') {?>config-type-number<?php }?>">
                            <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'selectbox') {?>
                                <select class="custom-select" name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cnf']->value->ConfWerte, 'wert', false, NULL, 'selectfor', array (
));
$_smarty_tpl->tpl_vars['wert']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['wert']->value) {
$_smarty_tpl->tpl_vars['wert']->do_else = false;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['wert']->value->cWert;?>
" <?php if ($_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert == $_smarty_tpl->tpl_vars['wert']->value->cWert) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['wert']->value->cName;?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            <?php } elseif ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'pass') {?>
                                <input class="form-control" type="password" name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" value="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert;?>
" tabindex="1" />
                            <?php } elseif ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'number') {?>
                                <div class="input-group form-counter">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-outline-secondary border-0" data-count-down>
                                            <span class="fas fa-minus"></span>
                                        </button>
                                    </div>
                                    <input class="form-control" type="number" name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" value="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert;?>
" tabindex="1" />
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-secondary border-0" data-count-up>
                                            <span class="fas fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                            <?php } elseif ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'color') {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:snippets/colorpicker.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cpID'=>$_smarty_tpl->tpl_vars['cnf']->value->cWertName,'cpName'=>$_smarty_tpl->tpl_vars['cnf']->value->cWertName,'cpValue'=>$_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert), 0, true);
?>
                            <?php } else { ?>
                                <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" value="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert;?>
" tabindex="1" />
                            <?php }?>
                        </div>
                        <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cBeschreibung) {?>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>$_smarty_tpl->tpl_vars['cnf']->value->cBeschreibung,'cID'=>$_smarty_tpl->tpl_vars['cnf']->value->kEinstellungenConf),$_smarty_tpl ) );?>

                            </div>
                        <?php }?>
                    </div>
                <?php } else { ?>
                    <?php if ($_smarty_tpl->tpl_vars['open']->value) {?></div></div><?php }?>
                    <div class="card">
                        <div class="card-header">
                            <div class="subheading1"><?php echo $_smarty_tpl->tpl_vars['cnf']->value->cName;?>

                            <?php if ((isset($_smarty_tpl->tpl_vars['cnf']->value->cSektionsPfad)) && strlen($_smarty_tpl->tpl_vars['cnf']->value->cSektionsPfad) > 0) {?>
                                <span class="path"><strong><?php echo __('settingspath');?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['cnf']->value->cSektionsPfad;?>
</span>
                            <?php }?>
                            </div>
                            <hr class="mb-n3">
                        </div>
                        <div class="card-body">
                        <?php $_smarty_tpl->_assignInScope('open', true);?>
                <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <?php if ($_smarty_tpl->tpl_vars['open']->value) {?>
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->
                <?php }?>
                <div class="card-footer save-wrapper">
                    <div class="row">
                        <div class="ml-auto col-sm-6 col-xl-auto">
                            <button name="test" type="submit" value="1" class="btn btn-default btn-block">
                                <i class="fal fa-play-circle"></i> <?php echo __('methodTest');?>

                            </button>
                        </div>
                        <div class="col-sm-6 col-xl-auto">
                            <button name="save" type="submit" value="1" class="btn btn-primary btn-block add">
                            <?php echo __('saveWithIcon');?>

                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
