<?php
/* Smarty version 3.1.39, created on 2021-10-31 16:56:07
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/redirect.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_617ebc974e3cf4_21626885',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '328646c68acc46dbf23ecf1ec76de9f4cb96646b' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/redirect.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/sortcontrols.tpl' => 1,
    'file:tpl_inc/csv_export_btn.tpl' => 1,
    'file:tpl_inc/csv_import_btn.tpl' => 1,
    'file:tpl_inc/filtertools.tpl' => 1,
    'file:tpl_inc/pagination.tpl' => 2,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_617ebc974e3cf4_21626885 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('redirect'),'cBeschreibung'=>__('redirectDesc'),'cDokuURL'=>__('redirectURL')), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/sortcontrols.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_assignInScope('cTab', (($tmp = $_smarty_tpl->tpl_vars['cTab']->value ?? null)===null||$tmp==='' ? 'redirects' : $tmp));?>

<?php echo '<script'; ?>
>
    $(function () {
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oRedirect_arr']->value, 'oRedirect');
$_smarty_tpl->tpl_vars['oRedirect']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oRedirect']->value) {
$_smarty_tpl->tpl_vars['oRedirect']->do_else = false;
?>
            var $stateChecking    = $('#input-group-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
 .state-checking');
            var $stateAvailable   = $('#input-group-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
 .state-available');
            var $stateUnavailable = $('#input-group-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
 .state-unavailable');

            <?php if ($_smarty_tpl->tpl_vars['oRedirect']->value->cAvailable === 'y') {?>
                $stateChecking.hide();
                $stateAvailable.show();
            <?php } elseif ($_smarty_tpl->tpl_vars['oRedirect']->value->cAvailable === 'n') {?>
                $stateChecking.hide();
                $stateUnavailable.show();
            <?php } else { ?>
                checkUrl(<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
, true);
            <?php }?>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    });

    function checkUrl(kRedirect, doUpdate)
    {
        doUpdate = doUpdate || false;

        var $stateChecking    = $('#input-group-' + kRedirect + ' .state-checking');
        var $stateAvailable   = $('#input-group-' + kRedirect + ' .state-available');
        var $stateUnavailable = $('#input-group-' + kRedirect + ' .state-unavailable');

        $stateChecking.show();
        $stateAvailable.hide();
        $stateUnavailable.hide();

        function checkUrlCallback(result)
        {
            $stateChecking.hide();
            $stateAvailable.hide();
            $stateUnavailable.hide();

            if (result === true) {
                $stateAvailable.show();
            } else {
                $stateUnavailable.show();
            }
        }

        if(doUpdate) {
            ioCall('updateRedirectState', [kRedirect], checkUrlCallback);
        } else {
            ioCall('redirectCheckAvailability', [$('#cToUrl-' + kRedirect).val()], checkUrlCallback);
        }
    }

    function redirectTypeahedDisplay(item)
    {
        return '/' + item.cSeo;
    }

    function redirectTypeahedSuggestion(item)
    {
        var type = '';
        switch(item.cKey) {
            case 'kLink': type = 'Seite'; break;
            case 'kNews': type = 'News'; break;
            case 'kNewsKategorie': type = 'News-Kategorie'; break;
            case 'kNewsMonatsUebersicht': type = 'News-Montasübersicht'; break;
            case 'kArtikel': type = 'Artikel'; break;
            case 'kKategorie': type = 'Kategorie'; break;
            case 'kHersteller': type = 'Hersteller'; break;
            case 'kMerkmalWert': type = 'Merkmal-Wert'; break;
            case 'suchspecial': type = 'Suchspecial'; break;
            default: type = 'Anderes'; break;
        }
        return '<span>/' + item.cSeo +
            ' <small class="text-muted">- ' + type + '</small></span>';
    }

    function toggleReferer(kRedirect)
    {
        var $refTr  = $('#referer-tr-' + kRedirect);
        var $refDiv = $('#referer-div-' + kRedirect);

        if(!$refTr.is(':visible')) {
            $refTr.show();
            $refDiv.slideDown();
        } else {
            $refDiv.slideUp(500, $refTr.hide.bind($refTr));
        }
    }
<?php echo '</script'; ?>
>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6 col-xl-auto">
                <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/csv_export_btn.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('exporterId'=>'redirects'), 0, false);
?>
            </div>
            <div class="<?php if (!count($_smarty_tpl->tpl_vars['oRedirect_arr']->value) > 0) {?>ml-auto<?php }?> col-sm-6 col-xl-auto">
                <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/csv_import_btn.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('importerId'=>'redirects'), 0, false);
?>
            </div>
        </div>
    </div>
</div>

<div class="tabs">
    <nav class="tabs-nav">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'redirects') {?> active<?php }?>" data-toggle="tab" role="tab" href="#redirects">
                    <?php echo __('overview');?>

                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'new_redirect') {?> active<?php }?>" data-toggle="tab" role="tab" href="#new_redirect">
                    <?php echo __('create');?>

                </a>
            </li>
        </ul>
    </nav>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade<?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'redirects') {?> active show<?php }?>" id="redirects">
            <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/filtertools.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('oFilter'=>$_smarty_tpl->tpl_vars['oFilter']->value), 0, false);
?>
            <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'cAnchor'=>'redirects'), 0, false);
?>
            <div>
                <form method="post">
                    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                    <?php if (count($_smarty_tpl->tpl_vars['oRedirect_arr']->value) > 0) {?>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th><?php echo __('redirectFrom');?>
 <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'sortControls', array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'nSortBy'=>0), true);?>
</th>
                                        <th class="min-w"><?php echo __('redirectTo');?>
 <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'sortControls', array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'nSortBy'=>1), true);?>
</th>
                                        <th class="text-center"><?php echo __('redirectRefererCount');?>
 <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'sortControls', array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'nSortBy'=>2), true);?>
</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oRedirect_arr']->value, 'oRedirect');
$_smarty_tpl->tpl_vars['oRedirect']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oRedirect']->value) {
$_smarty_tpl->tpl_vars['oRedirect']->do_else = false;
?>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="redirects[<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
][enabled]" value="1"
                                                       id="check-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
">
                                                    <label class="custom-control-label" for="check-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <label for="check-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
">
                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->cFromUrl;?>
" target="_blank"
                                                       <?php if (strlen($_smarty_tpl->tpl_vars['oRedirect']->value->cFromUrl) > 50) {?>data-toggle="tooltip"
                                                       data-placement="bottom" title="<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->cFromUrl;?>
"<?php }?>>
                                                        <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['oRedirect']->value->cFromUrl,50 ));?>

                                                    </a>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="form-group form-row align-items-center" id="input-group-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
">
                                                    <span class="col col-lg-auto col-form-label text-info state-checking">
                                                        <i class="fa fa-spinner fa-pulse"></i>
                                                    </span>
                                                    <span class="col col-lg-auto col-form-label text-success state-available" style="display:none;">
                                                        <i class="fal fa-check"></i>
                                                    </span>
                                                    <span class="col col-lg-auto col-form-label text-danger state-unavailable" style="display:none;">
                                                        <i class="fal fa-exclamation-triangle"></i>
                                                    </span>
                                                    <div class="col col-md-10">
                                                        <input class="form-control min-w-sm" name="redirects[<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
][cToUrl]"
                                                               value="<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->cToUrl;?>
" id="cToUrl-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
"
                                                               onblur="checkUrl(<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
)">
                                                    </div>
                                                    <?php echo '<script'; ?>
>
                                                        enableTypeahead(
                                                            '#cToUrl-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
', 'getSeos',
                                                            redirectTypeahedDisplay, redirectTypeahedSuggestion,
                                                            checkUrl.bind(null, <?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
, false)
                                                        );
                                                    <?php echo '</script'; ?>
>

                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($_smarty_tpl->tpl_vars['oRedirect']->value->nCount > 0) {?>
                                                    <span class="badge badge-primary font-weight-bold font-size-sm"><?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->nCount;?>
</span>
                                                <?php }?>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($_smarty_tpl->tpl_vars['oRedirect']->value->nCount > 0) {?>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-link px-2" title="<?php echo __('details');?>
"
                                                                onclick="toggleReferer(<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
);"
                                                                data-toggle="tooltip">
                                                            <span class="fal fa-chevron-circle-down rotate-180 font-size-lg"></span>
                                                        </button>
                                                    </div>
                                                <?php }?>
                                            </td>
                                        </tr>
                                        <?php if ($_smarty_tpl->tpl_vars['oRedirect']->value->nCount > 0) {?>
                                            <tr id="referer-tr-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
" style="display:none;">
                                                <td></td>
                                                <td colspan="5">
                                                    <div id="referer-div-<?php echo $_smarty_tpl->tpl_vars['oRedirect']->value->kRedirect;?>
" style="display:none;">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th><?php echo __('redirectReferer');?>
</th>
                                                                    <th><?php echo __('date');?>
</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oRedirect']->value->oRedirectReferer_arr, 'oRedirectReferer');
$_smarty_tpl->tpl_vars['oRedirectReferer']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oRedirectReferer']->value) {
$_smarty_tpl->tpl_vars['oRedirectReferer']->do_else = false;
?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php if ($_smarty_tpl->tpl_vars['oRedirectReferer']->value->kBesucherBot > 0) {?>
                                                                                <?php if (strlen($_smarty_tpl->tpl_vars['oRedirectReferer']->value->cBesucherBotName) > 0) {?>
                                                                                    <?php echo $_smarty_tpl->tpl_vars['oRedirectReferer']->value->cBesucherBotName;?>

                                                                                <?php } else { ?>
                                                                                    <?php echo $_smarty_tpl->tpl_vars['oRedirectReferer']->value->cBesucherBotAgent;?>

                                                                                <?php }?>
                                                                                (Bot)
                                                                            <?php } elseif (strlen($_smarty_tpl->tpl_vars['oRedirectReferer']->value->cRefererUrl) > 0) {?>
                                                                                <a href="<?php echo $_smarty_tpl->tpl_vars['oRedirectReferer']->value->cRefererUrl;?>
" target="_blank">
                                                                                    <?php echo $_smarty_tpl->tpl_vars['oRedirectReferer']->value->cRefererUrl;?>

                                                                                </a>
                                                                            <?php } else { ?>
                                                                                <i><?php echo __('redirectRefererDirect');?>
</i>
                                                                            <?php }?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['oRedirectReferer']->value->dDate,'%d.%m.%Y - %H:%M:%S');?>

                                                                        </td>
                                                                    </tr>
                                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php }?>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tbody>
                            </table>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['nTotalRedirectCount']->value > 0) {?>
                        <div class="alert alert-info" role="alert"><?php echo __('noFilterResults');?>
</div>
                    <?php } else { ?>
                        <div class="alert alert-info" role="alert"><?php echo __('noDataAvailable');?>
</div>
                    <?php }?>
                    <div class="save-wrapper">
                        <div class="row">
                            <div class="col-sm-6 col-xl-auto text-left">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="ALLMSGS" id="ALLMSGS" onclick="AllMessages(this.form);">
                                    <label class="custom-control-label" for="ALLMSGS"><?php echo __('globalSelectAll');?>
</label>
                                </div>
                            </div>
                            <?php if (count($_smarty_tpl->tpl_vars['oRedirect_arr']->value) > 0) {?>
                                <div class="ml-auto col-sm-6 col-xl-auto">
                                    <button name="action" value="delete" class="btn btn-danger btn-block">
                                        <i class="fas fa-trash-alt"></i> <?php echo __('deleteSelected');?>

                                    </button>
                                </div>
                                <div class="col-sm-6 col-xl-auto">
                                    <button name="action" value="delete_all" class="btn btn-warning btn-block">
                                        <?php echo __('redirectDelUnassigned');?>

                                    </button>
                                </div>
                            <?php }?>
                            <div class="ol-sm-6 col-xl-auto">
                                <button name="action" value="save" class="btn btn-primary btn-block">
                                    <?php echo __('saveWithIcon');?>

                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'cAnchor'=>'redirects','isBottom'=>true), 0, true);
?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade<?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'new_redirect') {?> active show<?php }?>" id="new_redirect">
            <form method="post">
                <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                <div class="settings">
                    <div class="subheading1"><?php echo __('redirectNew');?>
</div>
                    <hr class="mb-3">
                    <div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cFromUrl"><?php echo __('redirectFrom');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" id="cFromUrl" name="cFromUrl" required
                                       <?php if (!empty($_smarty_tpl->tpl_vars['cFromUrl']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['cFromUrl']->value;?>
"<?php }?>>
                            </div>
                        </div>
                        <div class="form-group form-row align-items-center" id="input-group-0">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cToUrl-0"><?php echo __('redirectTo');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" id="cToUrl-0" name="cToUrl" required
                                       onblur="checkUrl(0)" <?php if (!empty($_smarty_tpl->tpl_vars['cToUrl']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['cToUrl']->value;?>
"<?php }?>>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3" style="display:none;">
                                <i class="fa fa-spinner fa-pulse text-info state-checking"></i>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3" style="display:none;">
                                <i class="fal fa-check text-success text-success state-available"></i>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                                <i class="fal fa-exclamation-triangle text-danger state-unavailable"></i>
                            </div>
                            <?php echo '<script'; ?>
>
                                enableTypeahead(
                                    '#cToUrl-0', 'getSeos', redirectTypeahedDisplay, redirectTypeahedSuggestion,
                                    checkUrl.bind(null, 0, false)
                                )
                            <?php echo '</script'; ?>
>
                        </div>
                    </div>
                    <div class="save-wrapper">
                        <div class="row">
                            <div class="ml-auto col-sm-6 col-xl-auto">
                                <button name="action" value="new" class="btn btn-primary btn-block">
                                    <i class="fa fa-save"></i> <?php echo __('create');?>

                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
