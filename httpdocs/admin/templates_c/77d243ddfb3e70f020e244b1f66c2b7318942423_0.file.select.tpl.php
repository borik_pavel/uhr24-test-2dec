<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/select.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fcbd982_88647267',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77d243ddfb3e70f020e244b1f66c2b7318942423' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/select.tpl',
      1 => 1623869889,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fcbd982_88647267 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="form-group knowmates-form-group form-group-select <?php echo $_smarty_tpl->tpl_vars['params']->value['wrapperClass']->getValue();?>
">
    <div class="row">
        <div class="<?php if ($_smarty_tpl->tpl_vars['params']->value['fullWidth']->getValue()) {?>col-12<?php } else { ?>col-md-6<?php }?>">
            <label for="<?php echo $_smarty_tpl->tpl_vars['params']->value['id']->getValue();?>
">
                <?php echo $_smarty_tpl->tpl_vars['params']->value['label']->getValue();?>

                <?php if ($_smarty_tpl->tpl_vars['params']->value['description']->hasValue()) {?>
                    <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['components']->value->fieldDescription, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('description'=>$_smarty_tpl->tpl_vars['params']->value['description']->getValue()), 0, true);
?>
                <?php }?>
            </label>
        </div>
        <div class="<?php if ($_smarty_tpl->tpl_vars['params']->value['fullWidth']->getValue()) {?>col-12<?php } else { ?>col-md-6<?php }?> text-right">
            <div class="select-wrapper">
                <select name="<?php echo $_smarty_tpl->tpl_vars['params']->value['fieldName']->getValue();?>
"
                        id="<?php echo $_smarty_tpl->tpl_vars['params']->value['id']->getValue();?>
"
                        <?php if ($_smarty_tpl->tpl_vars['params']->value['required']->getValue()) {?>required<?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['params']->value['placeholder']->hasValue()) {?>data-placeholder="<?php echo $_smarty_tpl->tpl_vars['params']->value['placeholder']->getValue();?>
"<?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['params']->value['multiple']->getValue()) {?>multiple<?php }?>
                        class="chosen-select-width form-control<?php echo $_smarty_tpl->tpl_vars['params']->value['class']->getValue();?>
"
                        tabindex="16">
                    <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

                </select>
            </div>
        </div>
    </div>
</div>

<?php echo '<script'; ?>
>
    $("#<?php echo $_smarty_tpl->tpl_vars['params']->value['id']->getValue();?>
").chosen(<?php echo $_smarty_tpl->tpl_vars['params']->value['choosenOptions']->getValue();?>
);
<?php echo '</script'; ?>
>
<?php }
}
