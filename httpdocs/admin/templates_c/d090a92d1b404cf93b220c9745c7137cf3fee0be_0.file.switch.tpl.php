<?php
/* Smarty version 3.1.39, created on 2021-10-07 13:32:13
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/fields/switch.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615edabdd3fdf8_42434799',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd090a92d1b404cf93b220c9745c7137cf3fee0be' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/fields/switch.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615edabdd3fdf8_42434799 (Smarty_Internal_Template $_smarty_tpl) {
if (!(isset($_smarty_tpl->tpl_vars['json']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('json', 0);
}
if ($_smarty_tpl->tpl_vars['json']->value == true) {?>
    <?php $_smarty_tpl->_assignInScope('fieldName', "json[".((string)$_smarty_tpl->tpl_vars['name']->value)."]");
} else { ?>
    <?php $_smarty_tpl->_assignInScope('fieldName', $_smarty_tpl->tpl_vars['name']->value);
}
if (!(isset($_smarty_tpl->tpl_vars['value']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('value', 1);
}
if (!(isset($_smarty_tpl->tpl_vars['checked']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('checked', 'false');
}
if (!(isset($_smarty_tpl->tpl_vars['id']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('id', $_smarty_tpl->tpl_vars['name']->value);
}
if (!(isset($_smarty_tpl->tpl_vars['wrapperCssClass']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('wrapperCssClass', '');
}?>

<?php if (!(isset($_smarty_tpl->tpl_vars['readonly']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('readonly', false);
}?>
<div class="form-group knowmates-form-group form-group-switch <?php echo $_smarty_tpl->tpl_vars['wrapperCssClass']->value;?>
">
    <div class="checkbox checkbox-switch switch-primary">
        <label>
            <span>
                <?php echo $_smarty_tpl->tpl_vars['label']->value;?>

                <?php if ((isset($_smarty_tpl->tpl_vars['description']->value)) && strlen($_smarty_tpl->tpl_vars['description']->value) > 0) {?>
                    <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['components']->value->fieldDescription, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('description'=>$_smarty_tpl->tpl_vars['description']->value), 0, true);
?>
                <?php }?>
                </span>
            <input id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" <?php if ((isset($_smarty_tpl->tpl_vars['cssClass']->value))) {?>class="<?php echo $_smarty_tpl->tpl_vars['cssClass']->value;?>
"<?php }?> type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['fieldName']->value;?>
"
                   value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['checked']->value == $_smarty_tpl->tpl_vars['value']->value) {?> checked="checked" <?php }?> <?php if ((isset($_smarty_tpl->tpl_vars['required']->value))) {?>required<?php }?> <?php if ((isset($_smarty_tpl->tpl_vars['onChange']->value))) {?>onChange="<?php echo $_smarty_tpl->tpl_vars['onChange']->value;?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['readonly']->value) {?>readonly<?php }?>>
            <span>
            </span>
        </label>
    </div>
</div><?php }
}
