<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/setting.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fc9c885_07823336',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8926645e88351b79dbfb3b2e916c46bf044bd65d' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/setting.tpl',
      1 => 1623869889,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fc9c885_07823336 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="form-group knowmates-form-group set-comp <?php echo $_smarty_tpl->tpl_vars['params']->value['wrapperClass']->getValue();?>
" <?php if ($_smarty_tpl->tpl_vars['params']->value['id']->hasValue()) {?>id="<?php echo $_smarty_tpl->tpl_vars['params']->value['id']->getValue();?>
"<?php }?>>
    <div class="row">
        <?php if ($_smarty_tpl->tpl_vars['params']->value['label']->hasValue()) {?>
            <div class="<?php if (!$_smarty_tpl->tpl_vars['params']->value['fullWidth']->getValue()) {?>col-md-6<?php }?> col-12">
                <label><?php echo $_smarty_tpl->tpl_vars['params']->value['label']->getValue();?>

                    <?php if ($_smarty_tpl->tpl_vars['params']->value['description']->hasValue()) {?>
                        <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['components']->value->fieldDescription, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('description'=>$_smarty_tpl->tpl_vars['params']->value['description']->getValue()), 0, true);
?>
                    <?php }?>
                </label>
            </div>
        <?php }?>
        <div class="<?php if (!$_smarty_tpl->tpl_vars['params']->value['fullWidth']->getValue()) {?>col-md-6<?php }?> col-12 d-flex">
            <div class="<?php if ($_smarty_tpl->tpl_vars['params']->value['alignContent']->getValue() == "left") {?>mr-auto<?php } else { ?>ml-auto<?php }
echo $_smarty_tpl->tpl_vars['params']->value['class']->getValue();?>
 set-comp__content" <?php if ($_smarty_tpl->tpl_vars['params']->value['style']->hasValue()) {?>style="<?php echo $_smarty_tpl->tpl_vars['params']->value['style']->getValue();?>
"<?php }?>>
                <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

            </div>
        </div>
    </div>
</div><?php }
}
