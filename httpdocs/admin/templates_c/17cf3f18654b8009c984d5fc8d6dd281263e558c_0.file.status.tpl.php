<?php
/* Smarty version 3.1.39, created on 2021-10-07 13:34:11
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/hb_legaltext/adminmenu/templates/status.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615edb33cf4075_11133973',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '17cf3f18654b8009c984d5fc8d6dd281263e558c' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/hb_legaltext/adminmenu/templates/status.tpl',
      1 => 1607704508,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615edb33cf4075_11133973 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
">
    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>


    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>

                <th class="py-3">Rechtstext</th>

                <th class="py-3">
                    Automatische Aktualiserung
                    <i class="fas fa-info-circle fa-fw p-1"
                       data-toggle="tooltip"
                       data-placement="right"
                       title=""
                       data-original-title="Rechtstext für die automatische Aktualisierung aktiviert/deaktiviert">
                    </i>
                </th>

                <th class="py-3">Format</th>
                <th class="py-3">Aktualisiert</th>
                <th class="py-3 text-right">Status letzte Aktualisierung</th>
            </tr>
            </thead>

            <tbody>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['legalTexts']->value, 'legalText');
$_smarty_tpl->tpl_vars['legalText']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['legalText']->value) {
$_smarty_tpl->tpl_vars['legalText']->do_else = false;
?>
                <tr>
                    <td class="check">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input legal-text-input" type="checkbox" name="selected[]" id="legal-text-action-<?php echo $_smarty_tpl->tpl_vars['legalText']->value->id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['legalText']->value->id;?>
">
                            <label class="custom-control-label" for="legal-text-action-<?php echo $_smarty_tpl->tpl_vars['legalText']->value->id;?>
"></label>
                        </div>
                    </td>

                    <td>
                        <label class="p-1"
                               for="legal-text-action-<?php echo $_smarty_tpl->tpl_vars['legalText']->value->id;?>
"
                               data-toggle="tooltip"
                               data-placement="right"
                               title=""
                               data-original-title="ID: <?php echo $_smarty_tpl->tpl_vars['legalText']->value->id;?>
">
                            <?php echo $_smarty_tpl->tpl_vars['legalText']->value->name;?>

                        </label>
                    </td>

                    <td>
                        <?php if ($_smarty_tpl->tpl_vars['legalText']->value->active != 1) {?>
                            <span class="text-warning">Deaktiviert</span>
                        <?php } else { ?>
                            <span class="text-success">Aktiviert</span>
                        <?php }?>
                    </td>

                    <td><?php echo $_smarty_tpl->tpl_vars['legalText']->value->mode;?>
</td>

                    <td>
                        <?php if ($_smarty_tpl->tpl_vars['legalText']->value->updated === NULL) {?>
                            -
                        <?php } else { ?>
                            <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['legalText']->value->updated,'%d.%m.%Y %H:%M:%S');?>

                        <?php }?>
                    </td>

                    <td class="text-right">
                        <?php if ($_smarty_tpl->tpl_vars['legalText']->value->updated_state === NULL) {?>
                            -
                        <?php } elseif ($_smarty_tpl->tpl_vars['legalText']->value->updated_state === '0') {?>
                            <span class="badge badge-danger badge-pill ml-2">Fehlgeschlagen</span>
                        <?php } else { ?>
                            <span class="badge badge-success badge-pill ml-2">Erfolgreich</span>
                        <?php }?>
                    </td>
                </tr>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </tbody>
        </table>
    </div>

    <div class="card-footer save-wrapper save">
        <div class="row">
            <div class="col-md-12 col-xl-auto text-left mb-4 mb-lg-0">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" name="all_legal_texts" id="all-legal-texts" type="checkbox">
                    <label class="custom-control-label" for="all-legal-texts">Alle auswählen</label>

                    
                        <?php echo '<script'; ?>
>
                            document.addEventListener('DOMContentLoaded', () => {
                                const legalTexts = document.querySelectorAll('input.legal-text-input');
                                const inputSelectAll = document.getElementById('all-legal-texts');

                                inputSelectAll.addEventListener('change', () => {
                                    const state = inputSelectAll.checked

                                    for (const legalText of legalTexts) {
                                        legalText.checked = state;
                                    }
                                })
                            })
                        <?php echo '</script'; ?>
>
                    
                </div>
            </div>

            <div class="ml-auto col-md-12 col-xl-auto">
                <button name="action" value="update" type="submit" class="btn btn-secondary mb-2 mb-md-0 d-block d-sm-inline-block">
                    <i class="fas fa-refresh mr-2"></i> Aktualisieren
                </button>

                <button name="action" value="enable" type="submit" class="btn btn-success mb-2 mb-md-0 d-block d-sm-inline-block">
                    <i class="fas fa-check mr-2"></i> Aktivieren
                </button>

                <button name="action" value="disable" type="submit" class="btn btn-warning mb-2 mb-md-0 d-block d-sm-inline-block">
                    <i class="fas fa-close mr-2"></i> Deaktivieren
                </button>
            </div>
        </div>
    </div>
</form>
<?php }
}
