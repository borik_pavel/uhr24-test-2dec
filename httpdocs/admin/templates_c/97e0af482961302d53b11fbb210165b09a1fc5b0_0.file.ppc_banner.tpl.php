<?php
/* Smarty version 3.1.39, created on 2021-11-18 11:07:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_paypal/adminmenu/templates/ppc_banner.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619625dfe43252_81602696',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '97e0af482961302d53b11fbb210165b09a1fc5b0' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_paypal/adminmenu/templates/ppc_banner.tpl',
      1 => 1632907929,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619625dfe43252_81602696 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="ppcBanner">
    <form method="post" action="plugin.php?kPlugin=<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" enctype="multipart/form-data" name="wizard" class="settings navbar-form">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
        <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
        <div class="card">
            <div class="card-header">
                <span class="subheading1"><?php echo __('PayPal Ratenzahlung Banner');?>
</span>
                <hr class="mb-n3">
            </div>
            <div class="card-body">
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingPPCActive"><?php echo __('PayPal Ratenzahlung Banner aktivieren');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[active]" id="settingPPCActive" data-target=".settingbanner_position_activate_details" data-active="Y">
                            <option value="Y"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('active','N') === 'Y') {?> selected="selected"<?php }?>><?php echo __('Ja');?>
</option>
                            <option value="N"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('active','N') === 'N') {?> selected="selected"<?php }?>><?php echo __('Nein');?>
</option>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo __('Aktivieren Sie hier die Nutzung des PayPal Ratenzahlung Banner.');?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group form-row align-items-center settingbanner_position_activate_details collapse<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('active','N') === 'Y') {?> show<?php }?>">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingClientID"><?php echo __('PayPal-API-Client-ID');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <input type="text" class="form-control" name="ppcSetting[api_client_id]" id="settingClientID" value="<?php echo $_smarty_tpl->tpl_vars['ppcSetting']->value->get('api_client_id',$_smarty_tpl->tpl_vars['apiClientID']->value);?>
" placeholder="xxx" />
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo __('Tragen Sie hier die Client-ID ein.');?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['consentAvail']->value === 'Y') {?>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingPPCUseConsent"><?php echo __('Consent Manager verwenden');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[use_consent]" id="settingPPCUseConsent">
                            <option value="Y"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('use_consent','N') === 'Y') {?> selected="selected"<?php }?>><?php echo __('Ja');?>
</option>
                            <option value="N"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('use_consent','N') === 'N') {?> selected="selected"<?php }?>><?php echo __('Nein');?>
</option>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo __('Consent Manager verwenden ja/nein');?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <?php } else { ?>
                <input type="hidden" name="ppcSetting[use_consent]" id="settingPPCUseConsent" value="N">
                <?php }?>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingPPCTplSupport"><?php echo __('Template-Unterstützung');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[tpl_support]" id="settingPPCTplSupport">
                            <option value="NOVA"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('tpl_support','NOVA') === 'NOVA') {?> selected="selected"<?php }?>><?php echo __('NOVA');?>
</option>
                            <option value="Evo"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('tpl_support','NOVA') === 'Evo') {?> selected="selected"<?php }?>><?php echo __('Evo');?>
</option>
                            <option value=""<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('tpl_support','NOVA') === '') {?> selected="selected"<?php }?>><?php echo __('Individuell');?>
</option>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo __('Template-Unterstützung NOVA/Evo');?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ppcPositions']->value, 'ppcPosition', false, 'ppcPositionKey');
$_smarty_tpl->tpl_vars['ppcPosition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ppcPositionKey']->value => $_smarty_tpl->tpl_vars['ppcPosition']->value) {
$_smarty_tpl->tpl_vars['ppcPosition']->do_else = false;
?>
                <div class="form-group form-row align-items-center settingbanner_position_activate_details collapse<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('active','N') === 'Y') {?> show<?php }?>">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_active"><?php echo $_smarty_tpl->tpl_vars['ppcPosition']->value;?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select settingbanner_position_activate" data-target="#settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
" data-active="Y" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_active]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_active">
                            <option value="Y"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_active'),'N') === 'Y') {?> selected="selected"<?php }?>><?php echo __('Ja');?>
</option>
                            <option value="N"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_active'),'N') === 'N') {?> selected="selected"<?php }?>><?php echo __('Nein');?>
</option>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php ob_start();
echo $_smarty_tpl->tpl_vars['ppcPosition']->value;
$_prefixVariable1 = ob_get_clean();
echo __($_prefixVariable1);?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        </div>
        <div class="settingbanner_position_activate_details collapse<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get('active','N') === 'Y') {?> show<?php }?>">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ppcPositions']->value, 'ppcPosition', false, 'ppcPositionKey');
$_smarty_tpl->tpl_vars['ppcPosition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ppcPositionKey']->value => $_smarty_tpl->tpl_vars['ppcPosition']->value) {
$_smarty_tpl->tpl_vars['ppcPosition']->do_else = false;
?>
        <div class="card collapse<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_active'),'N') === 'Y') {?> show<?php }?>" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
" >
            <div class="card-header">
                <span class="subheading1"><?php echo __($_smarty_tpl->tpl_vars['ppcPosition']->value);?>
</span>
                <hr class="mb-n3">
            </div>
            <div class="card-body">
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_minprice"><?php echo __('Mindestpreis');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <input type="number" step="1" class="form-control" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_minprice]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_minprice" value="<?php echo number_format(round($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_minprice'),100)),2);?>
" />
                    </div>
                </div>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_layout"><?php echo __('Layout');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select settingbanner_layout_select" data-target=".banner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_flex_style" data-active="flex" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_layout]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_layout">
                            <option value="text"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_layout'),'text') === 'text') {?> selected="selected"<?php }?>><?php echo __('Contextual message');?>
</option>
                            <option value="flex"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_layout'),'text') === 'flex') {?> selected="selected"<?php }?>><?php echo __('Responsive display banner');?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_logo_type"><?php echo __('Logo Type');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_logo_type]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_logo_type">
                            <option value="primary"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_logo_type'),'primary') === 'primary') {?> selected="selected"<?php }?>><?php echo __('Full Logo');?>
</option>
                            <option value="alternative"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_logo_type'),'primary') === 'alternative') {?> selected="selected"<?php }?>><?php echo __('Monogram Logo');?>
</option>
                            <option value="inline"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_logo_type'),'primary') === 'inline') {?> selected="selected"<?php }?>><?php echo __('Inline Logo');?>
</option>
                            <option value="none"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_logo_type'),'primary') === 'none') {?> selected="selected"<?php }?>><?php echo __('Text only');?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_text_size"><?php echo __('Text Size');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <input type="number" step="1" class="form-control" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_text_size]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_text_size" value="<?php echo number_format($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_text_size'),12));?>
" />
                    </div>
                </div>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_text_color"><?php echo __('Text Color');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_text_color]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_text_color">
                            <option value="black"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_text_color'),'black') === 'black') {?> selected="selected"<?php }?>><?php echo __('Black text, colored logo');?>
</option>
                            <option value="white"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_text_color'),'black') === 'white') {?> selected="selected"<?php }?>><?php echo __('White text, white logo');?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-row align-items-center banner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_flex_style collapse<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_layout'),'text') === 'flex') {?> show<?php }?>">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_style_color"><?php echo __('Flex Layout Type');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_style_color]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_style_color">
                            <option value="blue"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_color'),'white') === 'blue') {?> selected="selected"<?php }?>><?php echo __('Blue background, white text');?>
</option>
                            <option value="black"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_color'),'white') === 'black') {?> selected="selected"<?php }?>><?php echo __('Black background, white text');?>
</option>
                            <option value="white"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_color'),'white') === 'white') {?> selected="selected"<?php }?>><?php echo __('White background, blue text');?>
</option>
                            <option value="gray"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_color'),'white') === 'gray') {?> selected="selected"<?php }?>><?php echo __('Light gray background, blue text');?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-row align-items-center banner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_flex_style collapse<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_layout'),'text') === 'flex') {?> show<?php }?>">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_style_ratio"><?php echo __('Flex Layout Ratio');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_style_ratio]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_style_ratio">
                            <option value="1x1"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_ratio'),'1x1') === '1x1') {?> selected="selected"<?php }?>><?php echo __('Ratio of 1x1');?>
</option>
                            <option value="1x4"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_ratio'),'1x1') === '1x4') {?> selected="selected"<?php }?>><?php echo __('Ratio of 1x4');?>
</option>
                            <option value="8x1"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_ratio'),'1x1') === '8x1') {?> selected="selected"<?php }?>><?php echo __('Ratio of 8x1');?>
</option>
                            <option value="20x1"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_style_ratio'),'1x1') === '20x1') {?> selected="selected"<?php }?>><?php echo __('Ratio of 20x1');?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_query_selector"><?php echo __('PHPQuery Selektor');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <input type="text" class="form-control" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_query_selector]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_query_selector" value="<?php echo $_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_query_selector'));?>
" />
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo __('PHPQuery Selektor Beschreibung');?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group form-row align-items-center ">
                    <label class="col col-sm-4 col-form-label text-sm-right order-1" for="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_query_method"><?php echo __('PHPQuery Methode');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <select class="custom-select" name="ppcSetting[<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_query_method]" id="settingbanner_<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
_query_method">
                            <option value="after"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_query_method'),'after') === 'after') {?> selected="selected"<?php }?>><?php echo __('after');?>
</option>
                            <option value="append"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_query_method'),'after') === 'append') {?> selected="selected"<?php }?>><?php echo __('append');?>
</option>
                            <option value="before"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_query_method'),'after') === 'before') {?> selected="selected"<?php }?>><?php echo __('before');?>
</option>
                            <option value="prepend"<?php if ($_smarty_tpl->tpl_vars['ppcSetting']->value->get(($_smarty_tpl->tpl_vars['ppcPositionKey']->value).('_query_method'),'after') === 'prepend') {?> selected="selected"<?php }?>><?php echo __('prepend');?>
</option>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                        <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo __('PHPQuery Methode Beschreibung');?>
">
                            <span class="fas fa-info-circle fa-fw"></span>
                        </span>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['apiConficured']->value) {?>
                <div class="form-group form-row align-items-right">
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-right">
                        <a href="#" class="btn btn-default btn-banner-preview" data-target="<?php echo $_smarty_tpl->tpl_vars['ppcPositionKey']->value;?>
"><?php echo __('Vorschau');?>
</a>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <div class="card-footer save-wrapper">
            <div class="row">
                <div class="col-sm-6 col-xl-auto">
                    <button type="submit" class="btn btn-primary btn-block" name="task" value="savePPCSettings">
                        <i class="fal fa-save"></i> <?php echo __('Save');?>

                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php if ($_smarty_tpl->tpl_vars['apiConficured']->value && $_smarty_tpl->tpl_vars['apiURL']->value) {?>
    <div class="modal fade" id="ppcBannerPreviewModal" tabindex="-1" role="dialog" aria-labelledby="ppcBannerPreviewLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ppcBannerPreviewLabel"><?php echo __('PPC Banner Preview');?>
</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="ppc-message"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('close');?>
</button>
                </div>
            </div>
        </div>
    </div>
<?php echo '<script'; ?>
 data-namespace="ppcSetting" src="<?php echo $_smarty_tpl->tpl_vars['apiURL']->value;?>
" ><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
>
    
    function ppcBannerPreview(banner) {
        let $form   = $('form.settings'),
            apiID   = '<?php echo $_smarty_tpl->tpl_vars['ppcSetting']->value->get('api_client_id',$_smarty_tpl->tpl_vars['apiClientID']->value);?>
';

        if (apiID !== $('[name="ppcSetting[api_client_id]"]', $form).val()) {
            window.alert('<?php echo __('Please save your general changes before trying preview.');?>
');

            return;
        }
        let $modal = $('#ppcBannerPreviewModal'),
            layout = $('[name="ppcSetting[' + banner + '_layout]"]', $form).val(),
            data   = {
                amount: Math.floor(Math.random() * 500) + 100,
                style: {
                    layout: layout,
                    logo: {
                        type: $('[name="ppcSetting[' + banner + '_logo_type]"]', $form).val()
                    },
                    text: {
                        size: $('[name="ppcSetting[' + banner + '_text_size]"]', $form).val(),
                        color: $('[name="ppcSetting[' + banner + '_text_color]"]', $form).val()
                    }
                },
                onRender: function () {
                    stopSpinner();
                    $modal.modal('show');
                }
            };

        if (layout === 'flex') {
            data.style.color = $('[name="ppcSetting[' + banner + '_style_color]"]', $form).val();
            data.style.ratio = $('[name="ppcSetting[' + banner + '_style_ratio]"]', $form).val();
        }

        startSpinner();
        let ppc = ppcSetting.Messages(data);
        ppc.render('#ppcBannerPreviewModal .ppc-message');
    }
    $('#settingPPCActive, .settingbanner_position_activate, .settingbanner_layout_select').on('change', function (e) {
        let $this = $(this);
        if ($this.val() === $this.data('active')) {
            $($this.data('target')).collapse('show');
        } else {
            $($this.data('target')).collapse('hide');
        }
    });
    $('#settingPPCTplSupport').on('change', function (e) {
        let $this = $(this),
            tpls  = <?php echo $_smarty_tpl->tpl_vars['tplDefaults']->value;?>
;
        if ($this.val() !== '' && tpls[$this.val()]['selector']) {
            for (let pos in tpls[$this.val()]['selector']) {
                if (tpls[$this.val()]['selector'].hasOwnProperty(pos)) {
                    $('#settingbanner_' + pos + '_query_selector').val(tpls[$this.val()]['selector'][pos]);
                    $('#settingbanner_' + pos + '_query_method').val(tpls[$this.val()]['method'][pos]);
                }
            }
        }
    });
    $('.btn-banner-preview').on('click', function (e) {
        e.preventDefault();
        ppcBannerPreview($(this).data('target'))
    });
    
<?php echo '</script'; ?>
><?php }
}
