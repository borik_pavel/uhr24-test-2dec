<?php
/* Smarty version 3.1.39, created on 2021-10-07 15:58:46
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/zahlungsarten_einstellen.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615efd166ad077_00800675',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8c716bb6c2b901db7b12a99ce962fe84c78bf216' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/zahlungsarten_einstellen.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/seite_header.tpl' => 1,
  ),
),false)) {
function content_615efd166ad077_00800675 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cBeschreibung'=>__('configurePaymentmethod'),'cTitel'=>$_smarty_tpl->tpl_vars['zahlungsart']->value->cName), 0, false);
?>
<div id="content">
    <form name="einstellen" method="post" action="zahlungsarten.php" class="settings">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="einstellungen_bearbeiten" value="1" />
        <input type="hidden" name="kZahlungsart" value="<?php if ((isset($_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart))) {
echo $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart;
}?>" />

        <div class="card">
            <div class="card-header">
                <div class="subheading1"><?php echo __('settings');?>
: <?php echo __('general');?>
</div>
                <hr class="mb-n3">
            </div>
            <div class="card-body">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                    <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="cName_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('showedName');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <input class="form-control" type="text" name="cName_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" id="cName_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['Zahlungsartname']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {
echo $_smarty_tpl->tpl_vars['Zahlungsartname']->value[$_smarty_tpl->tpl_vars['cISO']->value];
}?>" tabindex="1" />
                        </div>
                    </div>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="cBild"><?php echo __('pictureURL');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <input class="form-control" type="text" name="cBild" id="cBild" value="<?php if ((isset($_smarty_tpl->tpl_vars['zahlungsart']->value->cBild))) {
echo $_smarty_tpl->tpl_vars['zahlungsart']->value->cBild;
}?>" tabindex="1" />
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('pictureDesc')),$_smarty_tpl ) );?>
</div>
                </div>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                    <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="cGebuehrname_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('feeName');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <input class="form-control" type="text" name="cGebuehrname_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" id="cGebuehrname_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['Gebuehrname']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {
echo $_smarty_tpl->tpl_vars['Gebuehrname']->value[$_smarty_tpl->tpl_vars['cISO']->value];
}?>" tabindex="2" />
                        </div>
                        <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('feeNameHint')),$_smarty_tpl ) );?>
</div>
                    </div>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="kKundengruppe"><?php echo __('restrictedToCustomerGroups');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <select name="kKundengruppe[]"
                                multiple="multiple"
                                size="6"
                                id="kKundengruppe"
                                class="selectpicker custom-select"
                                data-selected-text-format="count > 2"
                                data-size="7"
                                data-actions-box="true">
                            <option value="0" <?php if ((isset($_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value[0])) && $_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value[0]) {?>selected<?php }?>>
                                <?php echo __('all');?>

                            </option>
                            <option data-divider="true"></option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['kundengruppen']->value, 'kundengruppe');
$_smarty_tpl->tpl_vars['kundengruppe']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['kundengruppe']->value) {
$_smarty_tpl->tpl_vars['kundengruppe']->do_else = false;
?>
                                <?php $_smarty_tpl->_assignInScope('kKundengruppe', $_smarty_tpl->tpl_vars['kundengruppe']->value->kKundengruppe);?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['kundengruppe']->value->kKundengruppe;?>
" <?php if ((isset($_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value[$_smarty_tpl->tpl_vars['kKundengruppe']->value])) && $_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value[$_smarty_tpl->tpl_vars['kKundengruppe']->value]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['kundengruppe']->value->cName;?>
</option>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </select>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('multipleChoice')),$_smarty_tpl ) );?>
</div>
                </div>
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="nSort"><?php echo __('sortNo');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <input class="form-control" type="text" name="nSort" id="nSort" value="<?php if ((isset($_smarty_tpl->tpl_vars['zahlungsart']->value->nSort))) {
echo $_smarty_tpl->tpl_vars['zahlungsart']->value->nSort;
}?>" tabindex="3" />
                    </div>
                </div>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                    <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="cHinweisTextShop_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('noticeTextShop');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <textarea class="form-control" id="cHinweisTextShop_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" name="cHinweisTextShop_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php if ((isset($_smarty_tpl->tpl_vars['cHinweisTexteShop_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {
echo $_smarty_tpl->tpl_vars['cHinweisTexteShop_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value];
}?></textarea>
                        </div>
                    </div>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                    <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="cHinweisText_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('noticeTextEmail');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <textarea class="form-control" id="cHinweisText_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" name="cHinweisText_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php if ((isset($_smarty_tpl->tpl_vars['cHinweisTexte_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {
echo $_smarty_tpl->tpl_vars['cHinweisTexte_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value];
}?></textarea>
                        </div>
                        <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('noticeTextEmailDesc')),$_smarty_tpl ) );?>
</div>
                    </div>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="nMailSenden"><?php echo __('paymentAckMail');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <select id="nMailSenden" name="nMailSenden" class="custom-select combo">
                            <option value="1"<?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nMailSenden&$_smarty_tpl->tpl_vars['ZAHLUNGSART_MAIL_EINGANG']->value) {?> selected="selected"<?php }?>>
                                <?php echo __('yes');?>

                            </option>
                            <option value="0"<?php if (!($_smarty_tpl->tpl_vars['zahlungsart']->value->nMailSenden&$_smarty_tpl->tpl_vars['ZAHLUNGSART_MAIL_EINGANG']->value)) {?> selected="selected"<?php }?>>
                                <?php echo __('no');?>

                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="nMailSendenStorno"><?php echo __('paymentCancelMail');?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                        <select id="nMailSendenStorno" name="nMailSendenStorno" class="custom-select combo">
                            <option value="1"<?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nMailSenden&$_smarty_tpl->tpl_vars['ZAHLUNGSART_MAIL_STORNO']->value) {?> selected="selected"<?php }?>>
                                <?php echo __('yes');?>

                            </option>
                            <option value="0"<?php if (!($_smarty_tpl->tpl_vars['zahlungsart']->value->nMailSenden&$_smarty_tpl->tpl_vars['ZAHLUNGSART_MAIL_STORNO']->value)) {?> selected="selected"<?php }?>>
                                <?php echo __('no');?>

                            </option>
                        </select>
                    </div>
                </div>

                <?php $_smarty_tpl->_assignInScope('filters', array('za_nachnahme_jtl','za_ueberweisung_jtl','za_rechnung_jtl','za_barzahlung_jtl','za_lastschrift_jtl','za_kreditkarte_jtl'));?>

                <?php if (!in_array($_smarty_tpl->tpl_vars['zahlungsart']->value->cModulId,$_smarty_tpl->tpl_vars['filters']->value)) {?>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="nWaehrendBestellung"><?php echo __('duringOrder');?>
:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <select id="nWaehrendBestellung" name="nWaehrendBestellung" class="custom-select combo">
                                <option value="1"<?php if ((isset($_smarty_tpl->tpl_vars['zahlungsart']->value->nWaehrendBestellung)) && $_smarty_tpl->tpl_vars['zahlungsart']->value->nWaehrendBestellung == 1) {?> selected<?php }?>>
                                    <?php echo __('yes');?>

                                </option>
                                <option value="0"<?php if ((isset($_smarty_tpl->tpl_vars['zahlungsart']->value->nWaehrendBestellung)) && $_smarty_tpl->tpl_vars['zahlungsart']->value->nWaehrendBestellung == 0) {?> selected<?php }?>>
                                    <?php echo __('no');?>

                                </option>
                            </select>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
        <div class="card">
            <?php $_smarty_tpl->_assignInScope('hasBody', false);?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Conf']->value, 'cnf');
$_smarty_tpl->tpl_vars['cnf']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cnf']->value) {
$_smarty_tpl->tpl_vars['cnf']->do_else = false;
?>
            <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cConf === 'Y') {?>
            <?php if ($_smarty_tpl->tpl_vars['hasBody']->value === false) {?><div class="card-body"><?php $_smarty_tpl->_assignInScope('hasBody', true);
}?>
                <div class="form-group form-row align-items-center">
                    <label class="col col-sm-4 col-form-label text-sm-right" for="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
"><?php echo $_smarty_tpl->tpl_vars['cnf']->value->cName;?>
:</label>
                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                    <?php if ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'selectbox') {?>
                        <select name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" class="custom-select combo">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cnf']->value->ConfWerte, 'wert');
$_smarty_tpl->tpl_vars['wert']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['wert']->value) {
$_smarty_tpl->tpl_vars['wert']->do_else = false;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['wert']->value->cWert;?>
" <?php if ((isset($_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert)) && $_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert == $_smarty_tpl->tpl_vars['wert']->value->cWert) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['wert']->value->cName;?>
</option>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </select>
                    <?php } elseif ($_smarty_tpl->tpl_vars['cnf']->value->cInputTyp === 'password') {?>
                        <input class="form-control" autocomplete="off" type="password" name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert))) {
echo $_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert;
}?>" />
                    <?php } else { ?>
                        <input class="form-control" type="text" name="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" id="<?php echo $_smarty_tpl->tpl_vars['cnf']->value->cWertName;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert))) {
echo $_smarty_tpl->tpl_vars['cnf']->value->gesetzterWert;
}?>" />
                        <?php if ((isset($_smarty_tpl->tpl_vars['cnf']->value->kEinstellungenConf))) {?>
                            <span id="EinstellungAjax_<?php echo $_smarty_tpl->tpl_vars['cnf']->value->kEinstellungenConf;?>
"></span>
                        <?php }?>
                    <?php }?>
                    </div>
                    <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>$_smarty_tpl->tpl_vars['cnf']->value->cBeschreibung),$_smarty_tpl ) );?>
</div>
                </div>
                <?php } else { ?>
                    <div class="card-header">
                        <div class="subheading1"><?php echo __('settings');?>
: <?php echo $_smarty_tpl->tpl_vars['cnf']->value->cName;?>
</div>
                        <hr class="mb-n3">
                    </div>
                    <div class="card-body">
                        <?php $_smarty_tpl->_assignInScope('hasBody', true);?>
            <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        <div class="save-wrapper">
            <div class="row">
                <div class="ml-auto col-sm-6 col-xl-auto">
                    <a href="zahlungsarten.php" title="<?php echo __('cancel');?>
" class="btn btn-outline-primary btn-block">
                        <?php echo __('cancelWithIcon');?>

                    </a>
                </div>
                <div class="col-sm-6 col-xl-auto">
                    <button type="submit" value="<?php echo __('save');?>
" class="btn btn-primary btn-block">
                        <?php echo __('saveWithIcon');?>

                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php }
}
