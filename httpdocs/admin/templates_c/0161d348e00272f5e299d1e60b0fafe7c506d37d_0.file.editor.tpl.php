<?php
/* Smarty version 3.1.39, created on 2021-09-30 17:40:04
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_theme_editor/adminmenu/templates/editor.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6155da542af0c5_14258779',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0161d348e00272f5e299d1e60b0fafe7c506d37d' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_theme_editor/adminmenu/templates/editor.tpl',
      1 => 1603789825,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6155da542af0c5_14258779 (Smarty_Internal_Template $_smarty_tpl) {
?><link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
templates/style.css" />
<div id="jtl-template-editor">
    <div id="main-actions">
        <select id="theme" aria-label="<?php echo __('--- select theme ---');?>
">
            <option value=""><?php echo __('--- select theme ---');?>
</option>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['themes']->value, 'theme');
$_smarty_tpl->tpl_vars['theme']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['theme']->value) {
$_smarty_tpl->tpl_vars['theme']->do_else = false;
?>
                <?php if ((isset($_smarty_tpl->tpl_vars['theme']->value['theme']))) {?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['theme']->value['theme'];?>
" data-template="<?php echo $_smarty_tpl->tpl_vars['theme']->value['template'];?>
"><?php echo ucfirst($_smarty_tpl->tpl_vars['theme']->value['theme']);?>
</option>
                <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </select>
        <a href="#" id="refresh"><span class="glyphicon glyphicon-refresh"></span></a>
                <div class="pull-right">
            <button id="compile" class="btn btn-success">
                <i id="loader" class="fa fa-spin fa-spinner"></i>
                <?php echo __('compile theme');?>

            </button>
                    </div>
    </div>

    <div id="editor-sidebar">
        <ul id="files"></ul>
    </div>

    <div id="editor"></div>

    <div id="actions">
        <button id="save" class="btn btn-primary"><?php echo __('save file');?>
</button>
        <button id="compilefile" class="btn btn-secondary"><?php echo __('compile file');?>
</button>
        <button id="reset" class="btn btn-danger pull-right"><?php echo __('reset file to default');?>
</button>
    </div>

    <div id="messages" class="text-center">
        <div id="msg" class="alert"></div>
    </div>
</div>

<div id="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="dialog-content" class="modal-body"></div>
            <div class="modal-footer">
                <span class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('cancel');?>
</button>
                    <button id="dialog-action" type="button" class="btn btn-danger"></button>
                </span>
            </div>
        </div>
    </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/theme-monokai.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/mode-less.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ext-searchbox.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    var URL = '<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
api.php',
        token = '<?php echo $_SESSION['jtl_token'];?>
',
        warningCompile = '<?php echo __('warningCompile');?>
',
        warningReset = '<?php echo __('warningReset');?>
',
        actionCompile = '<?php echo __('actionCompile');?>
',
        actionReset = '<?php echo __('actionReset');?>
';
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
script.js"><?php echo '</script'; ?>
>
<?php }
}
