<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:08:44
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/widgets/bots.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e3ec37eb24_42068176',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ecb412cb7ea94012c90493919a27f8c352bdab37' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/widgets/bots.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e3ec37eb24_42068176 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="widget-custom-data widget-bots">
    <?php if (is_array($_smarty_tpl->tpl_vars['oBots_arr']->value) && count($_smarty_tpl->tpl_vars['oBots_arr']->value) > 0) {?>
        <table class="table table-condensed table-hover table-blank">
            <thead>
                <tr>
                    <th><?php echo __('name');?>
 / <?php echo __('userAgent');?>
</th>
                    <th class="text-right"><?php echo __('count');?>
</th>
                </tr>
            </thead>
            <tbody>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oBots_arr']->value, 'oBots');
$_smarty_tpl->tpl_vars['oBots']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oBots']->value) {
$_smarty_tpl->tpl_vars['oBots']->do_else = false;
?>
                    <tr>
                        <td>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oBots']->value->cName)) && strlen($_smarty_tpl->tpl_vars['oBots']->value->cName) > 0) {?>
                                <?php echo $_smarty_tpl->tpl_vars['oBots']->value->cName;?>

                            <?php } elseif ((isset($_smarty_tpl->tpl_vars['oBots']->value->cUserAgent)) && strlen($_smarty_tpl->tpl_vars['oBots']->value->cUserAgent) > 0) {?>
                                <?php echo $_smarty_tpl->tpl_vars['oBots']->value->cUserAgent;?>

                            <?php } else { ?>
                                <?php echo __('unknown');?>

                            <?php }?>
                        </td>
                        <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['oBots']->value->nCount;?>
</td>
                    </tr>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </tbody>
        </table>
        <?php echo __('moreDetailsStatistics');?>

    <?php } else { ?>
        <div class="alert alert-info"><?php echo __('noStatisticsFound');?>
</div>
    <?php }?>
</div>
<?php }
}
