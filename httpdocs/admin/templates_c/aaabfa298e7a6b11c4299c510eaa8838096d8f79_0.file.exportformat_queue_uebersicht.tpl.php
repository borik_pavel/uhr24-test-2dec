<?php
/* Smarty version 3.1.39, created on 2021-09-30 18:07:32
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/exportformat_queue_uebersicht.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6155e0c4258811_90824024',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aaabfa298e7a6b11c4299c510eaa8838096d8f79' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/exportformat_queue_uebersicht.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/seite_header.tpl' => 1,
  ),
),false)) {
function content_6155e0c4258811_90824024 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('exportformat'),'cBeschreibung'=>__('exportformatDesc'),'cDokuURL'=>__('exportformatUrl')), 0, false);
?>
<div id="content">
    <div class="tabs">
        <nav class="tabs-nav">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cTab']->value === '' || empty($_smarty_tpl->tpl_vars['cTab']->value) || $_smarty_tpl->tpl_vars['cTab']->value === 'aktiv') {?> active<?php }?>" data-toggle="tab" role="tab" href="#aktiv">
                        <?php echo __('exportformatQueue');?>

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'fertig') {?> active<?php }?>" data-toggle="tab" role="tab" href="#fertig">
                        <?php echo __('exportformatTodaysWork');?>

                    </a>
                </li>
            </ul>
        </nav>
        <div class="tab-content">
            <div id="aktiv" class="tab-pane fade<?php if ($_smarty_tpl->tpl_vars['cTab']->value === '' || $_smarty_tpl->tpl_vars['cTab']->value === 'aktiv') {?> active show<?php }?>">
                <form method="post" action="exportformat_queue.php">
                    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                    <div>
                        <div class="subheading1"><?php echo __('exportformatQueue');?>
</div>
                        <hr class="mb-3">
                        <?php if ($_smarty_tpl->tpl_vars['oExportformatCron_arr']->value && count($_smarty_tpl->tpl_vars['oExportformatCron_arr']->value) > 0) {?>
                            <div id="tabellenLivesuche" class="table-responsive">
                                <table class="table table-striped table-align-top">
                                    <thead>
                                        <tr>
                                            <th class="text-left" style="width: 10px;">&nbsp;</th>
                                            <th class="text-left"><?php echo __('exportformat');?>
</th>
                                            <th class="text-left"><?php echo __('exportformatOptions');?>
</th>
                                            <th class="text-center"><?php echo __('exportformatStart');?>
</th>
                                            <th class="text-center"><?php echo __('repetition');?>
</th>
                                            <th class="text-center"><?php echo __('exportformatExported');?>
</th>
                                            <th class="text-center"><?php echo __('exportformatLastStart');?>
</th>
                                            <th class="text-center"><?php echo __('exportformatNextStart');?>
</th>
                                            <th class="text-center">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oExportformatCron_arr']->value, 'oExportformatCron');
$_smarty_tpl->tpl_vars['oExportformatCron']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oExportformatCron']->value) {
$_smarty_tpl->tpl_vars['oExportformatCron']->do_else = false;
?>
                                        <tr>
                                            <td class="text-left">
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" name="kCron[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cronID;?>
" id="kCron-<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cronID;?>
" />
                                                    <label class="custom-control-label" for="kCron-<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cronID;?>
"></label>
                                                </div>
                                            </td>
                                            <td class="text-left"><label for="kCron-<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cronID;?>
"><?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cName;?>
</label></td>
                                            <td class="text-left"><?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->Sprache->getLocalizedName();?>
/<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->Waehrung->cName;?>
/<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->Kundengruppe->cName;?>
</td>
                                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->dStart_de;?>
</td>
                                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cAlleXStdToDays;?>
</td>
                                            <td class="text-center">
                                                <?php echo (($tmp = $_smarty_tpl->tpl_vars['oExportformatCron']->value->oJobQueue->tasksExecuted ?? null)===null||$tmp==='' ? 0 : $tmp);?>
/<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->nAnzahlArtikel->nAnzahl;?>

                                            </td>
                                            <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['oExportformatCron']->value->dLetzterStart_de === '00.00.0000 00:00') {?>-<?php } else {
echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->dLetzterStart_de;
}?></td>
                                            <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['oExportformatCron']->value->dNaechsterStart_de === null) {
echo __('immediately');
} else {
echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->dNaechsterStart_de;
}?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="exportformat_queue.php?action=editieren&kCron=<?php echo $_smarty_tpl->tpl_vars['oExportformatCron']->value->cronID;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"
                                                       class="btn btn-link px-2"
                                                       title="<?php echo __('modify');?>
"
                                                       data-toggle="tooltip">
                                                        <span class="icon-hover">
                                                            <span class="fal fa-edit"></span>
                                                            <span class="fas fa-edit"></span>
                                                        </span>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer save-wrapper">
                                <div class="row">
                                    <div class="col-sm-6 col-xl-auto text-left">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" name="ALLMSGS" id="ALLMSGS" type="checkbox" onclick="AllMessages(this.form);">
                                            <label class="custom-control-label" for="ALLMSGS"><?php echo __('globalSelectAll');?>
</label>
                                        </div>
                                    </div>
                                    <div class="ml-auto col-sm-6 col-xl-auto">
                                        <button name="action[loeschen]" type="submit" value="1" class="btn btn-danger btn-block">
                                            <i class="fas fa-trash-alt"></i> <?php echo __('exportformatDelete');?>

                                        </button>
                                    </div>
                                    <div class="col-sm-6 col-xl-auto">
                                        <button name="action[triggern]" type="submit" value="1" class="btn btn-outline-primary btn-block">
                                            <i class="fal fa-play-circle"></i> <?php echo __('exportformatTriggerCron');?>

                                        </button>
                                    </div>
                                    <div class="col-sm-6 col-xl-auto">
                                        <button name="action[uebersicht]" type="submit" value="1" class="btn btn-outline-primary btn-block">
                                            <i class="fa fa-refresh"></i> <?php echo __('refresh');?>

                                        </button>
                                    </div>
                                    <div class="col-sm-6 col-xl-auto">
                                        <button name="action[erstellen]" type="submit" value="1" class="btn btn-primary btn-block add">
                                            <i class="fa fa-share"></i> <?php echo __('exportformatAdd');?>

                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-info" role="alert"><?php echo __('noDataAvailable');?>
</div>
                            <div class="card-footer save-wrapper">
                                <div class="row">
                                    <div class="ml-auto col-sm-6 col-xl-auto">
                                        <button name="action[triggern]" type="submit" value="1" class="btn btn-outline-primary btn-block">
                                            <i class="fal fa-play-circle"></i> <?php echo __('exportformatTriggerCron');?>

                                        </button>
                                    </div>
                                    <div class="col-sm-6 col-xl-auto">
                                        <button name="action[erstellen]" type="submit" value="1" class="btn btn-primary btn-block add">
                                            <i class="fa fa-share"></i> <?php echo __('exportformatAdd');?>

                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </form>
            </div>
            <div id="fertig" class="tab-pane fade<?php if ($_smarty_tpl->tpl_vars['cTab']->value === 'fertig') {?> active show<?php }?>">
                <div class="toolbar">
                    <form method="post" action="exportformat_queue.php">
                        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                        <div class="form-row">
                            <label class="col-sm-auto col-form-label" for="nStunden"><?php echo __('exportformatLastXHourPre');?>
 <?php echo __('hours');?>
:</label>
                            <div class="col-sm-auto mb-3">
                                <input size="2" class="form-control w-100" id="nStunden" name="nStunden" type="text" value="<?php echo $_smarty_tpl->tpl_vars['nStunden']->value;?>
" />
                            </div>
                            <span class="col-sm-auto">
                                <button name="action[fertiggestellt]" type="submit" value="1" class="btn btn-primary btn-block">
                                    <i class="fal fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <div>
                    <div class="subheading1"><?php echo __('exportformatTodaysWork');?>
</div>
                    <hr class="mb-3">
                    <div>
                    <?php if ($_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet_arr']->value && count($_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet_arr']->value) > 0) {?>
                        <div id="tabellenLivesuche" class="table-responsive">
                            <table class="table table-striped table-align-top">
                                <thead>
                                    <tr>
                                        <th class="th-1"><?php echo __('exportformat');?>
</th>
                                        <th class="th-2"><?php echo __('filename');?>
</th>
                                        <th class="th-3"><?php echo __('exportformatOptions');?>
</th>
                                        <th class="th-4"><?php echo __('exportformatExported');?>
</th>
                                        <th class="th-5"><?php echo __('exportformatLastStart');?>
</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet_arr']->value, 'oExportformatQueueBearbeitet');
$_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value) {
$_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->do_else = false;
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->cName;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->cDateiname;?>
</td>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->name;?>
/<?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->cNameWaehrung;?>
/<?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->cNameKundengruppe;?>

                                        </td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->nLimitN;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['oExportformatQueueBearbeitet']->value->dZuletztGelaufen_DE;?>
</td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info"><?php echo __('exportformatNoTodaysWork');?>
</div>
                    <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
