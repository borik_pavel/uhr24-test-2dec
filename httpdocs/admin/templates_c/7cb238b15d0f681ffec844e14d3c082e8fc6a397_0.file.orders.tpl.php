<?php
/* Smarty version 3.1.39, created on 2021-11-18 11:07:28
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_paypal/adminmenu/templates/orders.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619625e073d288_19515566',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7cb238b15d0f681ffec844e14d3c082e8fc6a397' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_paypal/adminmenu/templates/orders.tpl',
      1 => 1632907929,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/pagination.tpl' => 1,
  ),
),false)) {
function content_619625e073d288_19515566 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['lang']->value).".conf", "bestellungen", 0);
?>

<div class="container-full">
    <?php if ($_smarty_tpl->tpl_vars['message']->value) {?>
        <div class="alert alert-<?php echo key($_smarty_tpl->tpl_vars['message']->value);?>
" role="alert">
            <?php echo reset($_smarty_tpl->tpl_vars['message']->value);?>

        </div>
    <?php }?>

    <?php if (count($_smarty_tpl->tpl_vars['orders']->value) > 0 && $_smarty_tpl->tpl_vars['orders']->value) {?>
        <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('pagination'=>$_smarty_tpl->tpl_vars['pagination']->value,'cParam_arr'=>array('kPlugin'=>$_smarty_tpl->tpl_vars['oPlugin']->value->getID()),'cAnchor'=>$_smarty_tpl->tpl_vars['hash']->value), 0, false);
?>
        <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['post_url']->value;?>
">
            <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

            <div class="panel panel-default">
                <table class="list table table-hover">
                    <thead>
                    <tr>
                        <th class="tleft"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'orderNumber');?>
</th>
                        <th class="tleft"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'orderCostumer');?>
</th>
                        <th class="tleft"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'orderPaymentName');?>
</th>
                        <th class="text-center"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'orderSum');?>
</th>
                        <th class="text-center">Status</th>
                        <th class="text-right"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders']->value, 'order');
$_smarty_tpl->tpl_vars['order']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->do_else = false;
?>
                        <?php $_smarty_tpl->_assignInScope('payment', null);?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['payments']->value[$_smarty_tpl->tpl_vars['order']->value->kBestellung]))) {?>
                            <?php $_smarty_tpl->_assignInScope('payment', $_smarty_tpl->tpl_vars['payments']->value[$_smarty_tpl->tpl_vars['order']->value->kBestellung]);?>
                        <?php }?>
                        <tr class="text-vcenter">
                            <td>
                                <div><?php echo $_smarty_tpl->tpl_vars['order']->value->cBestellNr;?>
</div>
                                <small class="text-muted" title="<?php echo $_smarty_tpl->tpl_vars['order']->value->dErstelldatum_de;?>
" data-toggle="tooltip" data-placement="left"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['order']->value->dErstelldatum_de,'%d.%m.%Y');?>
</small>
                            </td>
                            <td>
                                <?php if ((isset($_smarty_tpl->tpl_vars['order']->value->oKunde->cVorname)) || (isset($_smarty_tpl->tpl_vars['order']->value->oKunde->cNachname)) || (isset($_smarty_tpl->tpl_vars['order']->value->oKunde->cFirma))) {?>
                                    <div>
                                        <?php echo $_smarty_tpl->tpl_vars['order']->value->oKunde->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['order']->value->oKunde->cNachname;?>

                                        <?php if ((isset($_smarty_tpl->tpl_vars['order']->value->oKunde->cFirma)) && mb_strlen($_smarty_tpl->tpl_vars['order']->value->oKunde->cFirma) > 0) {?> (<?php echo $_smarty_tpl->tpl_vars['order']->value->oKunde->cFirma;?>
)<?php }?>
                                    </div>
                                    <small class="text-muted"><i class="fa fa-user" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['order']->value->oKunde->cMail;?>
</small>
                                <?php } else { ?>
                                    <i class="fa fa-user-secret" aria-hidden="true"></i> <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'noAccount');?>

                                <?php }?>
                            </td>
                            <td>
                                <div><?php echo $_smarty_tpl->tpl_vars['order']->value->cZahlungsartName;?>
</div>
                                <?php if ($_smarty_tpl->tpl_vars['payment']->value) {?>
                                    <small class="text-muted"><i class="fa fa-paypal text-info" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['payment']->value->cHinweis;?>
</small>
                                <?php }?>
                            </td>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['order']->value->WarensummeLocalized[0];?>
</td>
                            <td class="text-center">
                                <small class="<?php if ($_smarty_tpl->tpl_vars['order']->value->cStatus < 0) {?>label label-danger<?php } elseif ($_smarty_tpl->tpl_vars['order']->value->cStatus > 0 && $_smarty_tpl->tpl_vars['order']->value->cStatus < 3) {?>text-muted<?php } else { ?>label label-success<?php }?>"><?php echo $_smarty_tpl->tpl_vars['order']->value->Status;?>
</small>
                            </td>
                            <th class="text-right no-flow">
                                <?php if ($_smarty_tpl->tpl_vars['payment']->value) {?>
                                    <div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group">
                                        <a href="https://www.sandbox.paypal.com/activity/payment/<?php echo $_smarty_tpl->tpl_vars['payment']->value->cHinweis;?>
" target="_blank" class="btn btn-default">Sandbox</a>
                                        <a href="https://www.paypal.com/activity/payment/<?php echo $_smarty_tpl->tpl_vars['payment']->value->cHinweis;?>
" target="_blank" class="btn btn-default active">Live</a>
                                    </div>
                                <?php }?>
                            </th>
                        </tr>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </tbody>
                </table>
            </div>
        </form>
    <?php } else { ?>
        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo __('noData');?>
</div>
    <?php }?>
</div>
<?php }
}
