<?php
/* Smarty version 3.1.39, created on 2021-10-11 17:08:07
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/netzdingeDE_google_codes/adminmenu/template/doc.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61645357223fe8_05138589',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a1d5fee0682f1f405b44d3c5b25b4d1a521fd152' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/netzdingeDE_google_codes/adminmenu/template/doc.tpl',
      1 => 1632586021,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61645357223fe8_05138589 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['aDoc']->value) {?>
    
    <style>
    .settings-content .subheading1.text-sm-right {padding-right: 0;}
    </style>
    
    <div class="settings-content">
        <div class="panel-idx-0 first mb-3 doku">
    	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['aDoc']->value, 'Doc', false, NULL, 'documentation', array (
  'iteration' => true,
));
$_smarty_tpl->tpl_vars['Doc']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Doc']->value) {
$_smarty_tpl->tpl_vars['Doc']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_documentation']->value['iteration']++;
?>
            <?php if ($_smarty_tpl->tpl_vars['Doc']->value['heading'] != '') {?>
                <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_documentation']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_documentation']->value['iteration'] : null) > 1) {?>
                    </div>
                    <div class="panel-idx-0 first mb-3 doku">
                <?php }?>
                <div class="subheading1"><?php echo $_smarty_tpl->tpl_vars['Doc']->value['heading'];?>
</div>
                <hr>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['Doc']->value['description'] == '') {?>
                <div class="subheading1 col col-sm-4 col-form-label text-sm-right"><?php echo $_smarty_tpl->tpl_vars['Doc']->value['einstellung'];?>
</div>
            <?php } else { ?>
                <div class="">
                    <div class="form-group form-row">
                        <label class="col col-sm-4 col-form-label text-sm-right"><?php if ($_smarty_tpl->tpl_vars['Doc']->value['einstellung'] != '') {
echo $_smarty_tpl->tpl_vars['Doc']->value['einstellung'];?>
:<?php }?></label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2"><div><?php echo $_smarty_tpl->tpl_vars['Doc']->value['description'];?>
</div></div>
                        <div class="col-auto ml-sm-n4 order-2 order-sm-3">
                            <span data-html="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="Standardwert: <?php echo $_smarty_tpl->tpl_vars['Doc']->value['standard'];?>
">
                                <span class="fas fa-info-circle fa-fw"></span>
                            </span>
                        </div>
                    </div>
                </div>
            <?php }?>
    	<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    	</div>
    </div>
<?php }
}
}
