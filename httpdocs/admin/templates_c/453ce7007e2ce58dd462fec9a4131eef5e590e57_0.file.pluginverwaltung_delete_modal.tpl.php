<?php
/* Smarty version 3.1.39, created on 2021-10-12 12:13:28
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/pluginverwaltung_delete_modal.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61655fc88911d7_71954544',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '453ce7007e2ce58dd462fec9a4131eef5e590e57' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/pluginverwaltung_delete_modal.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61655fc88911d7_71954544 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="uninstall-<?php echo $_smarty_tpl->tpl_vars['context']->value;?>
-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title"><?php echo __('deletePluginFilesHeading');?>
</h2>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="ml-auto col-sm-6 col-xl-auto submit">
                        <button type="button" class="delete-plugindata-yes btn btn-danger btn-bock">
                            <i class="fa fa-close"></i>&nbsp;<?php echo __('deletePluginDataYes');?>

                        </button>
                    </div>
                    <div class="col-sm-6 col-xl-auto submit">
                        <button type="button" class="btn btn-primary" name="cancel" data-dismiss="modal">
                            <?php echo __('cancelWithIcon');?>

                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    $(document).ready(function() {
        var disModal = $('#uninstall-<?php echo $_smarty_tpl->tpl_vars['context']->value;?>
-modal');
        $('<?php echo $_smarty_tpl->tpl_vars['button']->value;?>
').on('click', function(event) {
            disModal.modal('show');
            return false;
        });
        $('#uninstall-<?php echo $_smarty_tpl->tpl_vars['context']->value;?>
-modal .delete-plugindata-yes').on('click', function (event) {
            disModal.modal('hide');
            uninstall(true);
        });
        $('#uninstall-<?php echo $_smarty_tpl->tpl_vars['context']->value;?>
-modal .delete-plugindata-no').on('click', function (event) {
            disModal.modal('hide');
            uninstall(false);
        });
        function uninstall(deleteData) {
            var data = $('<?php echo $_smarty_tpl->tpl_vars['selector']->value;?>
').serialize();
            data += '&delete=1&delete-data=1&delete-files=1';
            $('<?php echo $_smarty_tpl->tpl_vars['selector']->value;?>
 input[type=checkbox]:checked').each(function (i, ele) {
                var name = $(ele).attr('value');
                data += '&ext[' + name + ']=' + $('#plugin-ext-' + name).val();
            });
            simpleAjaxCall('pluginverwaltung.php', data, function (res) {
                location.reload();
            });
            return false;
        }
    });
<?php echo '</script'; ?>
>
<?php }
}
