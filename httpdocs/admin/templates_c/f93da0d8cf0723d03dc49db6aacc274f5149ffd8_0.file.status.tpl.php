<?php
/* Smarty version 3.1.39, created on 2021-10-11 17:08:07
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/netzdingeDE_google_codes/adminmenu/template/status.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_616453573c7138_06992753',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f93da0d8cf0723d03dc49db6aacc274f5149ffd8' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/netzdingeDE_google_codes/adminmenu/template/status.tpl',
      1 => 1632586021,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_616453573c7138_06992753 (Smarty_Internal_Template $_smarty_tpl) {
?><style>
	
	#content_wrapper.backend-content {
		background: #454545;
		background: -ms-radial-gradient(center, circle cover, #b6b6b6 1.82%, #4b4b4b 100%);
		background: -moz-radial-gradient(center, circle cover, #b6b6b6 1.82%, #4b4b4b 100%);
		background: -o-radial-gradient(center, circle cover, #b6b6b6 1.82%, #4b4b4b 100%);
		background: -webkit-gradient(radial,left top,left bottom,color-stop(1.82%,#b6b6b6),color-stop(100%,#4b4b4b));
		background: -webkit-radial-gradient(center, circle cover, #b6b6b6 1.82%, #4b4b4b 100%);
		background: radial-gradient(circle farthest-corner at center, #b6b6b6 1.82%, #4b4b4b 100%);
		filter: progid:DXImageTransform.Microsoft.Gradient(startColorstr='#ffffff', endColorstr='#4b4b4b',GradientType=0);
		display: block;
	}

	#content_wrapper.backend-content .content-header {
		background-color: rgba(255, 255, 255, 0.9);
		border: #ccc;
		padding:15px;

	}

	@media (min-width: 655px) {
		#content_wrapper.backend-content .content-header .content-header-headline {
			background: url("<?php echo $_smarty_tpl->tpl_vars['PluginUrl']->value;?>
adminmenu/common/images/NETZdingeDE.png") no-repeat 99% 7px rgba(70, 70, 70, 0.9);
			padding: 8px 15px;
			color:#fff;
		}
		#content_wrapper.backend-content .content-header p {
			padding-left: 15px;
		}
	}

	@media (max-width: 654px) {
		#content_wrapper.backend-content {
			background: url("<?php echo $_smarty_tpl->tpl_vars['PluginUrl']->value;?>
adminmenu/common/images/NETZdingeDE.png") no-repeat 15px 7px rgba(70, 70, 70, 0.9);
		}

		#content_wrapper.backend-content .content-header {
			margin-top:35px;
		}
	}

	#content > div > div.tabs > nav {
		background-color: rgba(70, 70, 70, 0.9);
		padding: 4px;
		border-radius: 4px;
		border-bottom: 1px solid rgba(70, 70, 70, 0.9);
	}

	#content > div > div.tabs > nav > ul > li > a {
		background: #52c6d8 none repeat scroll 0 0;
		border: 1px solid #52c6d8;
		color: #fff;
		border-radius: 4px;
	}

	#content > div > div.tabs > nav > ul > li > a.active {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		color: #555;
	}

	.backend-wrapper > .backend-main > .backend-navbar { box-shadow: none;}

	#content > div > div.tabs > div { background:none; padding:0; padding-top:15px;4 }
	#content .settings-content div[class^="panel-idx-"] { background-color:#fff; margin-bottom: 20px;padding:15px;}
	#content .settings-content div[class^="panel-idx-"] .form-group { position: relative; border-collapse: separate; }
	#content .settings-content div[class^="panel-idx-"] .form-group.align-items-center {align-items: inherit!important;}
	#content .settings-content div[class^="panel-idx-"] .form-group.align-items-center .col-form-label {
	    padding-top: calc(0.375rem + 1px);
	    padding-bottom: calc(0.375rem + 1px);
	    margin-bottom: 0;
	    font-size: inherit;
	    line-height: 1.625;
	}
	#content .settings-content div[class^="panel-idx-"] .form-group > label {background-color: #eee; border: 1px solid #ccc; padding-right: 15px; min-height:40px; }
	#content .settings-content div[class^="panel-idx-"] .form-group label + div > div {border: 1px solid #ccc; padding: 3px 0; min-height:40px;padding-left:10px;padding-right:10px;}
	#content .settings-content div[class^="panel-idx-"] .form-group label + div > div a {text-decoration: underline;}
	#content .settings-content div[class^="panel-idx-"] .form-group label + div .form-control {border-radius: 0; height:40px;border: 1px solid #ccc;}
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group-checkbox-wrap {display:block; width:100%; border-radius: 0;}
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group-checkbox-wrap .custom-checkbox {margin-top:5px;}
	#content .settings-content div[class^="panel-idx-"] .form-group .custom-select {height: calc(1.625em + 0.75rem + 6px);border: 1px solid #ccc;border-radius: 0;}
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group > .input-group-prepend > .btn .fas,
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group > .input-group-append > .btn .fas {border: 1px solid #ccc; padding:5px;}
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group > input[type="number"] {border: none;flex: inherit;width: auto;}
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group-checkbox-wrap > input[type="radio"] {vertical-align: top;display: inline-block;margin: 2px 0 15px 5px;}
	#content .settings-content div[class^="panel-idx-"] .form-group .input-group-checkbox-wrap > input[type="radio"] + label {width: 95%;margin-bottom: 15px;margin-left: 10px;}
 

	.save-wrapper .col-xl-auto {width: 100%;}
	@media (min-width: 576px){
		.save-wrapper .col-sm-6 {
		    flex: 0 0 100%;
		    max-width: 100%;
		}
	}
	.save-wrapper .col-xl-auto .btn {background-color: #52c6d8;padding: 8px 15px;border-radius: 4px;}

	.subheading1, .table thead th, .table-sm thead th {color: #666;}
	
	
</style>
<?php if ($_smarty_tpl->tpl_vars['cLizenzfehler']->value && $_smarty_tpl->tpl_vars['cLizenzfehlerFestgestellt']->value) {?>
	<p class="box_error">Es gibt ein Problem mit der Lizensierung dieses Plugins, das Plugin wird nicht korrekt funktionieren.<br /><br />Fehlermeldung: <?php echo $_smarty_tpl->tpl_vars['cLizenzfehler']->value;?>
<br />Festgestellt am:  <?php echo $_smarty_tpl->tpl_vars['cLizenzfehlerFestgestellt']->value;?>
<br /><br />Installieren Sie die korrekte Version bitte neu, bei weiteren Problemen nehmen Sie bitte Kontakt mit uns auf!</p>
	<div class="well text-center tcenter">
		<a class="btn btn-primary button" href="plugin.php?kPlugin=<?php echo $_smarty_tpl->tpl_vars['oPlugin']->value->kPlugin;?>
&ndAction=getLicence">Lizenz neu anfragen</a>
	</div>
<?php }?>
<div class="settings-content">
    <div class="doku">
		<?php echo $_smarty_tpl->tpl_vars['LicContent']->value;?>

	</div>
</div><?php }
}
