<?php
/* Smarty version 3.1.39, created on 2021-11-06 17:18:05
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/trennzeichen.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6186aabde4c2f8_01746202',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35192fbd99cb6dd2411cf9f84f8db24db4b1365f' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/trennzeichen.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/language_switcher.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_6186aabde4c2f8_01746202 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('Trennzeichen'),'cBeschreibung'=>__('trennzeichenDesc'),'cDokuURL'=>__('trennzeichenURL')), 0, false);
?>
<div id="content">
    <div class="card">
        <div class="card-body">
            <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/language_switcher.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('action'=>'trennzeichen.php'), 0, false);
?>
        </div>
    </div>
    <form method="post" action="trennzeichen.php">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="save" value="1" />
        <div id="settings">
            <div class="card">
                <div class="card-header">
                    <div class="subheading1"><?php echo __('divider');?>
</div>
                    <hr class="mb-n3">
                </div>
                <div class="card-body table-responsive">
                    <table class="list table">
                    <thead>
                    <tr>
                        <th class="text-left"><?php echo __('unit');?>
</th>
                        <th class="text-center"><?php echo __('countDecimals');?>
</th>
                        <th class="text-center"><?php echo __('decimalsDivider');?>
</th>
                        <th class="text-center"><?php echo __('thousandDivider');?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php $_smarty_tpl->_assignInScope('nDezimal_weight', ("nDezimal_").((defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)));?>
                        <?php $_smarty_tpl->_assignInScope('cDezZeichen_weight', ("cDezZeichen_").((defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)));?>
                        <?php $_smarty_tpl->_assignInScope('cTausenderZeichen_weight', ("cTausenderZeichen_").((defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)));?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]))) {?>
                            <input type="hidden" name="kTrennzeichen_<?php echo (defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null);?>
" value="<?php echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]->getTrennzeichen();?>
" />
                        <?php }?>
                        <td class="text-left"><?php echo __('weight');?>
</td>
                        <td class="widthheight text-center">
                            <div class="input-group form-counter<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_weight']->value]))) {?> form-error<?php }?>">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-outline-secondary border-0" data-count-down>
                                        <span class="fas fa-minus"></span>
                                    </button>
                                </div>
                                <input size="2" type="number" name="nDezimal_<?php echo (defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null);?>
" class="form-control" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_weight']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_weight']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]->getDezimalstellen();
}
}?>" />
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary border-0" data-count-up>
                                        <span class="fas fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td class="widthheight text-center">
                            <input size="2" type="text" name="cDezZeichen_<?php echo (defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null);?>
" class="m-auto form-control<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_weight']->value]))) {?> fieldfillout<?php }?>" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_weight']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_weight']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]->getDezimalZeichen();
}
}?>" />
                        </td>
                        <td class="widthheight text-center">
                            <input size="2" type="text" name="cTausenderZeichen_<?php echo (defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null);?>
" class="m-auto form-control<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_weight']->value]))) {?> fieldfillout<?php }?>" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_weight']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_weight']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_WEIGHT') ? constant('JTL_SEPARATOR_WEIGHT') : null)]->getTausenderZeichen();
}
}?>" />
                        </td>
                    </tr>
                    <tr>
                        <?php $_smarty_tpl->_assignInScope('nDezimal_amount', ("nDezimal_").((defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)));?>
                        <?php $_smarty_tpl->_assignInScope('cDezZeichen_amount', ("cDezZeichen_").((defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)));?>
                        <?php $_smarty_tpl->_assignInScope('cTausenderZeichen_amount', ("cTausenderZeichen_").((defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)));?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]))) {?>
                            <input type="hidden" name="kTrennzeichen_<?php echo (defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null);?>
" value="<?php echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]->getTrennzeichen();?>
" />
                        <?php }?>
                        <td class="text-left"><?php echo __('quantity');?>
</td>
                        <td class="widthheight text-center">
                            <div class="input-group form-counter<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_amount']->value]))) {?> form-error<?php }?>">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-outline-secondary border-0" data-count-down>
                                        <span class="fas fa-minus"></span>
                                    </button>
                                </div>
                                <input size="2" type="number" name="nDezimal_<?php echo (defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null);?>
" class="form-control" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_amount']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_amount']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]->getDezimalstellen();
}
}?>" />
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary border-0" data-count-up>
                                        <span class="fas fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td class="widthheight text-center">
                            <input size="2" type="text" name="cDezZeichen_<?php echo (defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null);?>
" class="m-auto form-control<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_amount']->value]))) {?> fieldfillout<?php }?>" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_amount']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_amount']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]->getDezimalZeichen();
}
}?>" />
                        </td>
                        <td class="widthheight text-center">
                            <input size="2" type="text" name="cTausenderZeichen_<?php echo (defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null);?>
" class="m-auto form-control<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_amount']->value]))) {?> fieldfillout<?php }?>" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_amount']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_amount']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_AMOUNT') ? constant('JTL_SEPARATOR_AMOUNT') : null)]->getTausenderZeichen();
}
}?>" />
                        </td>
                    </tr>
                    <tr>
                        <?php $_smarty_tpl->_assignInScope('nDezimal_length', ("nDezimal_").((defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)));?>
                        <?php $_smarty_tpl->_assignInScope('cDezZeichen_length', ("cDezZeichen_").((defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)));?>
                        <?php $_smarty_tpl->_assignInScope('cTausenderZeichen_length', ("cTausenderZeichen_").((defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)));?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]))) {?>
                            <input type="hidden" name="kTrennzeichen_<?php echo (defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null);?>
" value="<?php echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]->getTrennzeichen();?>
" />
                        <?php }?>
                        <td class="text-left"><?php echo __('length');?>
</td>
                        <td class="widthheight text-center">
                            <div class="input-group form-counter<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_length']->value]))) {?> form-error<?php }?>">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-outline-secondary border-0" data-count-down>
                                        <span class="fas fa-minus"></span>
                                    </button>
                                </div>
                                <input size="2" type="number" name="nDezimal_<?php echo (defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null);?>
" class="form-control" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_length']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['nDezimal_length']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]->getDezimalstellen();
}
}?>" />
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary border-0" data-count-up>
                                        <span class="fas fa-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td class="widthheight text-center">
                            <input size="2" type="text" name="cDezZeichen_<?php echo (defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null);?>
" class="m-auto form-control<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_length']->value]))) {?> fieldfillout<?php }?>" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_length']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cDezZeichen_length']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]->getDezimalZeichen();
}
}?>" />
                        </td>
                        <td class="widthheight text-center">
                            <input size="2" type="text" name="cTausenderZeichen_<?php echo (defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null);?>
" class="m-auto form-control<?php if ((isset($_smarty_tpl->tpl_vars['xPlausiVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_length']->value]))) {?> fieldfillout<?php }?>" value="<?php if ((isset($_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_length']->value]))) {
echo $_smarty_tpl->tpl_vars['xPostVar_arr']->value[$_smarty_tpl->tpl_vars['cTausenderZeichen_length']->value];
} else {
if ((isset($_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]))) {
echo $_smarty_tpl->tpl_vars['oTrennzeichenAssoc_arr']->value[(defined('JTL_SEPARATOR_LENGTH') ? constant('JTL_SEPARATOR_LENGTH') : null)]->getTausenderZeichen();
}
}?>" />
                        </td>
                    </tr>

                    </tbody>
                </table>
                </div>
                <div class="card-footer save-wrapper">
                    <div class="row">
                        <div class="ml-auto col-sm-6 col-xl-auto">
                            <button name="speichern" type="submit" value="<?php echo __('save');?>
" class="btn btn-primary btn-block">
                                <?php echo __('saveWithIcon');?>

                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
