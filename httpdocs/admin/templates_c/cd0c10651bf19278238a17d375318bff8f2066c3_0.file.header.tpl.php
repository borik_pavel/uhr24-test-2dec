<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fb32062_80230191',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd0c10651bf19278238a17d375318bff8f2066c3' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/header.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fb32062_80230191 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
>
    var knm_dashboard_current_lang = "<?php echo $_smarty_tpl->tpl_vars['currentlySelectedLanguage']->value['langIso'];?>
";
<?php echo '</script'; ?>
>



<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
chosen/chosen.jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
arriveJs/arrive.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['jsPath']->value;?>
helper-functions.js" crossorigin="anonymous"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
chosen/chosen.min.css">
<?php if ((isset($_smarty_tpl->tpl_vars['dashboardOptions']->value->useSpectrum)) && $_smarty_tpl->tpl_vars['dashboardOptions']->value->useSpectrum === true) {?>
    <?php echo '<script'; ?>
 src='<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
spectrum_color_picker/spectrum.js'><?php echo '</script'; ?>
>
    <link rel='stylesheet' href='<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
spectrum_color_picker/spectrum.css'/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssPath']->value;?>
spectrum.css">
<?php }
if ((isset($_smarty_tpl->tpl_vars['dashboardOptions']->value->useCropper)) && $_smarty_tpl->tpl_vars['dashboardOptions']->value->useCropper === true) {?>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
cropper_js/cropper.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
cropper_js/cropper.min.js"><?php echo '</script'; ?>
>
<?php }
if ((isset($_smarty_tpl->tpl_vars['dashboardOptions']->value->useDatePicker)) && $_smarty_tpl->tpl_vars['dashboardOptions']->value->useDatePicker === true) {?>

    <?php echo '<script'; ?>
>
        function knm_dashboard_getTableHead() {
            if (knm_dashboard_current_lang && knm_dashboard_current_lang === 'de') {
                return "<th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'mon','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'tue','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'wed','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'thu','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'fri','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'sat','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'sun','section'=>'dashboard'),$_smarty_tpl ) );?>
</th>"
            } else {
                return "<th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'sun','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'mon','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'tue','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'wed','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'thu','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'fri','section'=>'dashboard'),$_smarty_tpl ) );?>
</th><th>\<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'sat','section'=>'dashboard'),$_smarty_tpl ) );?>
</th>"
            }
        }

        function knm_dashboard_getMonthArray() {
            return ["<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'january','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'february','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'march','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'april','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'may','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'june','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'july','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'august','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'september','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'october','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'november','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'december','section'=>'dashboard'),$_smarty_tpl ) );?>
"];
        }

        function knm_dashboard_getDayArray() {
            return [
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'monday','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'tuesday','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'wednesday','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'thursday','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'friday','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'saturday','section'=>'dashboard'),$_smarty_tpl ) );?>
",
                "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>'sunday','section'=>'dashboard'),$_smarty_tpl ) );?>
"
            ];
        }

        function knm_dashboard_getDefaultTime() {
            return '';
        }
    <?php echo '</script'; ?>
>

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
simplepicker-master/dist/simplepicker.css">
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['libPath']->value;?>
simplepicker-master/dist/simplepicker.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssPath']->value;?>
simplePicker.css">
<?php }?>


<link rel="shortcut icon" type="image/x-icon" href="<?php echo $_smarty_tpl->tpl_vars['assetsUrl']->value;?>
favicon.ico">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssPath']->value;?>
light-design.css">

<link id="dark-css-file" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssPath']->value;?>
dark-design.css"
      <?php if ((isset($_COOKIE['knmDesign'])) && $_COOKIE['knmDesign'] !== 'null' && $_COOKIE['knmDesign'] !== 'dark') {?>disabled<?php }?>>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssPath']->value;?>
template.css">

<?php if (!empty($_smarty_tpl->tpl_vars['cssFiles']->value)) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cssFiles']->value, 'customCssFile');
$_smarty_tpl->tpl_vars['customCssFile']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['customCssFile']->value) {
$_smarty_tpl->tpl_vars['customCssFile']->do_else = false;
?>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['customCssFile']->value;?>
">
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
if (!empty($_smarty_tpl->tpl_vars['codeModes']->value)) {?>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['PFAD_CODEMIRROR']->value;?>
lib/codemirror.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['PFAD_CODEMIRROR']->value;?>
addon/display/autorefresh.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['PFAD_CODEMIRROR']->value;?>
lib/codemirror.css">
    <link id="dark-css-file-code-field" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cssPath']->value;?>
material-darker.css"
          <?php if ((isset($_COOKIE['knmDesign'])) && $_COOKIE['knmDesign'] !== 'null' && $_COOKIE['knmDesign'] !== 'dark') {?>disabled<?php }?>
    >
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['codeModes']->value, 'codeMode');
$_smarty_tpl->tpl_vars['codeMode']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['codeMode']->value) {
$_smarty_tpl->tpl_vars['codeMode']->do_else = false;
?>
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['PFAD_CODEMIRROR']->value;?>
mode/<?php echo $_smarty_tpl->tpl_vars['codeMode']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['codeMode']->value;?>
.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['customCssFile']->value;?>
">
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>


<?php if (!empty($_smarty_tpl->tpl_vars['jsFiles']->value)) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jsFiles']->value, 'customJsFile');
$_smarty_tpl->tpl_vars['customJsFile']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['customJsFile']->value) {
$_smarty_tpl->tpl_vars['customJsFile']->do_else = false;
?>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['customJsFile']->value;?>
"><?php echo '</script'; ?>
>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
}
