<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:25
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/widgets/patch.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165261dae4e81_46046993',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8c88b4751af529d28ad2aad1559f34abacec5caf' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/widgets/patch.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165261dae4e81_46046993 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function () {
        ioCall(
            'getRemoteData',
            ['<?php echo (defined('JTLURL_GET_SHOPPATCH') ? constant('JTLURL_GET_SHOPPATCH') : null);?>
?vf=<?php echo $_smarty_tpl->tpl_vars['version']->value;?>
',
                'oPatch_arr',
                'widgets/patch_data.tpl',
                'patch_data_wrapper'],
            undefined,
            undefined,
            undefined,
            true
        );
    });
<?php echo '</script'; ?>
>

<div class="widget-custom-data widget-patch">
    <div id="patch_data_wrapper">
        <p class="ajax_preloader"><?php echo __('loading');?>
</p>
    </div>
</div><?php }
}
