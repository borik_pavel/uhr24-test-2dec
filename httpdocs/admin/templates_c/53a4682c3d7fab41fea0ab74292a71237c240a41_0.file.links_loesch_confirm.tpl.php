<?php
/* Smarty version 3.1.39, created on 2021-11-06 09:13:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/links_loesch_confirm.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_618639275b2e92_06177468',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53a4682c3d7fab41fea0ab74292a71237c240a41' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/links_loesch_confirm.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/seite_header.tpl' => 1,
  ),
),false)) {
function content_618639275b2e92_06177468 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('deleteLinkGroup')), 0, false);
?>
<div id="content">
    <div class="card">
        <div class="card-body">
            <form method="post" action="links.php">
                <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                <input type="hidden" name="action" value="confirm-delete" />
                <input type="hidden" name="kLinkgruppe" value="<?php echo $_smarty_tpl->tpl_vars['linkGroup']->value->getID();?>
" />

                <div class="alert alert-danger">
                    <p><strong><?php echo __('danger');?>
</strong></p>
                    <?php if (count($_smarty_tpl->tpl_vars['affectedLinkNames']->value) > 0) {?>
                        <p><?php echo __('dangerDeleteLinksAlso');?>
:</p>
                        <ul class="list">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['affectedLinkNames']->value, 'link');
$_smarty_tpl->tpl_vars['link']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->do_else = false;
?>
                                <li><?php echo $_smarty_tpl->tpl_vars['link']->value;?>
</li>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>
                    <?php }?>
                    <p><?php ob_start();
echo __('sureDeleteLinkGroup');
$_prefixVariable1 = ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['linkGroup']->value->getName();
$_prefixVariable2 = ob_get_clean();
echo sprintf($_prefixVariable1,$_prefixVariable2);?>
</p>
                </div>
                <div class="row">
                    <div class="ml-auto col-sm-6 col-xl-auto mb-2">
                        <button type="submit" name="confirmation" value="1" value="<?php echo __('yes');?>
" class="btn btn-danger btn-block min-w-sm">
                            <i class="fal fa-check"></i> <?php echo __('yes');?>

                        </button>
                    </div>
                    <div class="col-sm-6 col-xl-auto">
                        <button type="submit" name="confirmation" value="0" value="<?php echo __('no');?>
" class="btn btn-outline-primary btn-block min-w-sm">
                            <i class="fa fa-close"></i> <?php echo __('no');?>

                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php }
}
