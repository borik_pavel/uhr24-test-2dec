<?php
/* Smarty version 3.1.39, created on 2021-10-08 22:24:36
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_recaptcha/adminmenu/template/testing.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6160a904e911d3_23136314',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c7bbd15e4107cd34f6cd7b80f47b557469fcaf2' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_recaptcha/adminmenu/template/testing.tpl',
      1 => 1633724584,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6160a904e911d3_23136314 (Smarty_Internal_Template $_smarty_tpl) {
?><h2><?php echo __('Testing');?>
</h2>
<?php if ($_smarty_tpl->tpl_vars['reCaptchaConfig']->value) {?>
    <p><?php echo __('This will testing the captcha');?>
</p>
    <?php if ((isset($_smarty_tpl->tpl_vars['reCaptchaValid']->value))) {?>
        <?php if ($_smarty_tpl->tpl_vars['reCaptchaValid']->value) {?>
            <div class="alert alert-success">
                <?php echo __('Captcha is valid');?>
<br>
                <?php echo __('Some number');?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['reCaptchaTest']->value;?>

            </div>
        <?php } else { ?>
            <div class="alert alert-warning">
                <?php echo __('Captcha is invalid');?>

            </div>
        <?php }?>
    <?php }?>
    <form method="post" action="plugin.php?kPlugin=<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" name="captcha_testing">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
        <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
        <div class="form-group form-row align-items-center">
            <label class="col-form-label" for="test_var"><?php echo __('Some number');?>
</label>
            <div class="col col-sm-4">
                <input class="form-control" id="test_var" name="reCaptchaTest[test_var]" type="text" maxlength="32" required aria-required="true" />
            </div>
            <input class="btn btn-default" type="submit" value="<?php echo __('Send');?>
">
        </div>
        <?php echo $_smarty_tpl->tpl_vars['reCaptcha']->value;?>

    </form>
<?php } else { ?>
    <?php echo __('Not configured');?>

<?php }
}
}
