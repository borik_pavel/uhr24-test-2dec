<?php
/* Smarty version 3.1.39, created on 2021-09-30 08:39:13
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/sortcontrols.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61555b912edb09_89756277',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '258e89a789459bcedc0bc28c2d20c39c903b705a' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/sortcontrols.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61555b912edb09_89756277 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'sortControls' => 
  array (
    'compiled_filepath' => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates_c/258e89a789459bcedc0bc28c2d20c39c903b705a_0.file.sortcontrols.tpl.php',
    'uid' => '258e89a789459bcedc0bc28c2d20c39c903b705a',
    'call_name' => 'smarty_template_function_sortControls_29710898261555b912df1e5_13008522',
  ),
));
echo '<script'; ?>
>
    function pagiResort (pagiId, nSortBy, nSortDir)
    {
        $('#' + pagiId + '_nSortByDir').val(nSortBy * 2 + nSortDir);
        $('form#' + pagiId).submit();
        return false;
    }
<?php echo '</script'; ?>
>


<?php }
/* smarty_template_function_sortControls_29710898261555b912df1e5_13008522 */
if (!function_exists('smarty_template_function_sortControls_29710898261555b912df1e5_13008522')) {
function smarty_template_function_sortControls_29710898261555b912df1e5_13008522(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

    <?php if ($_smarty_tpl->tpl_vars['pagination']->value->getSortBy() !== $_smarty_tpl->tpl_vars['nSortBy']->value) {?>
        <a href="#" onclick="return pagiResort('<?php echo $_smarty_tpl->tpl_vars['pagination']->value->getId();?>
', <?php echo $_smarty_tpl->tpl_vars['nSortBy']->value;?>
, 0);"><i class="fa fa-unsorted"></i></a>
    <?php } elseif ($_smarty_tpl->tpl_vars['pagination']->value->getSortDirSpecifier() === 'DESC') {?>
        <a href="#" onclick="return pagiResort('<?php echo $_smarty_tpl->tpl_vars['pagination']->value->getId();?>
', <?php echo $_smarty_tpl->tpl_vars['nSortBy']->value;?>
, 0);"><i class="fa fa-sort-desc"></i></a>
    <?php } elseif ($_smarty_tpl->tpl_vars['pagination']->value->getSortDirSpecifier() === 'ASC') {?>
        <a href="#" onclick="return pagiResort('<?php echo $_smarty_tpl->tpl_vars['pagination']->value->getId();?>
', <?php echo $_smarty_tpl->tpl_vars['nSortBy']->value;?>
, 1);"><i class="fa fa-sort-asc"></i></a>
    <?php }
}}
/*/ smarty_template_function_sortControls_29710898261555b912df1e5_13008522 */
}
