<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/tabBar/singleTab.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fba2cf5_06553395',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '586ef643f40997955bfd246628c7e579c1cc8f9a' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/tabBar/singleTab.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fba2cf5_06553395 (Smarty_Internal_Template $_smarty_tpl) {
if (!(isset($_smarty_tpl->tpl_vars['isMore']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('isMore', 'false');
}?>
<li class="nav-item tabItem <?php echo $_smarty_tpl->tpl_vars['tabId']->value;?>
-tab">
    <a onclick='setTab("<?php echo $_smarty_tpl->tpl_vars['tabId']->value;?>
", <?php echo $_smarty_tpl->tpl_vars['isMore']->value;?>
)'
       class="knmPluginTab d-flex nav-link <?php if ($_smarty_tpl->tpl_vars['tabId']->value === $_smarty_tpl->tpl_vars['initialTab']->value) {?>active show<?php }?>"
       data-toggle="tab"
       href="#<?php echo $_smarty_tpl->tpl_vars['tabId']->value;?>
" role="tab"
       aria-controls="<?php echo $_smarty_tpl->tpl_vars['tabId']->value;?>
" aria-selected="true">
        <span class="d-flex flex-column" style="margin: 0 auto">
            <i class="<?php echo $_smarty_tpl->tpl_vars['icon']->value;?>
 m-auto tabIcon"></i>
            <span style="margin: 4px auto;" class="tabText">
                <?php ob_start();
if ($_smarty_tpl->tpl_vars['tabName']->value == 'license' || $_smarty_tpl->tpl_vars['tabName']->value == 'about') {
echo "dashboard";
}
$_prefixVariable1=ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>$_smarty_tpl->tpl_vars['tabName']->value,'section'=>$_prefixVariable1),$_smarty_tpl ) );?>

            </span>
        </span>
    </a>
</li><?php }
}
