<?php
/* Smarty version 3.1.39, created on 2021-11-18 16:20:38
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/versandarten_neue_Versandart.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61966f46b6f8f6_19327740',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2c5c5a56fb4d7e486f4a9824a1189ff13dd21f17' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/versandarten_neue_Versandart.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/seite_header.tpl' => 1,
  ),
),false)) {
function content_61966f46b6f8f6_19327740 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.replace.php','function'=>'smarty_modifier_replace',),));
echo '<script'; ?>
 type="text/javascript">
    <?php $_smarty_tpl->_assignInScope('addOne', 1);?>
    var i = <?php if ((isset($_smarty_tpl->tpl_vars['VersandartStaffeln']->value)) && count($_smarty_tpl->tpl_vars['VersandartStaffeln']->value) > 0) {?>Number(<?php echo count($_smarty_tpl->tpl_vars['VersandartStaffeln']->value);?>
) + 1<?php } else { ?>2<?php }?>;
    function addInputRow() {
        $('#price_range tbody').append('<tr><td><div class="input-group"><span class="input-group-addon"><label><?php echo __('upTo');?>
</label></span><input type="text" name="bis[]"  id="bis' + i + '" class="form-control kilogram"><span class="input-group-addon"><label><?php if ((isset($_smarty_tpl->tpl_vars['einheit']->value))) {
echo $_smarty_tpl->tpl_vars['einheit']->value;
}?></label></span></div></td><td class="text-center"><div class="input-group"><span class="input-group-addon"><label><?php echo __('amount');?>
</label></span><input type="text" name="preis[]"  id="preis' + i + '" class="form-control price_large"></div></td></tr>');
        i += 1;
        }

    function confirmAllCombi() {
        return confirm('<?php echo __('shippingConfirm');?>
');
        }

    
    function delInputRow() {
        i -= 1;
        $('#price_range tbody tr').last().remove();
    }

    function addShippingCombination() {
        var newCombi = '<div class=\'input-group align-baseline mt-2\'>'+$('#ulVK #liVKneu').html()+'</div>';
        newCombi = newCombi.replace(/selectX/gi,'select');
        if ($("select[name='Versandklassen']").length >= 1) {
            newCombi = newCombi.replace(/<option value="-1">/gi, '<option value="-1" disabled="disabled">');
        }

        $('#ulVK').append(newCombi);
    }

    function updateVK() {
        var val = '';
        $("select[name='Versandklassen']").each( function(index) {
            if ($(this).val()!= null) {
                val += ((val.length > 0)?' ':'') + $(this).val().toString().replace(/,/gi,'-');
            }
        });
        $("input[name='kVersandklasse']").val(val);
    }

    function checkCombination() {
        var remove = false;
        $("select[name='Versandklassen']").each(function (index) {
            if (index === 0) {
                if ($.inArray("-1", $(this).val()) != -1) {
                    if (!confirmAllCombi()) {
                        var valSelected = $(this).val();
                        valSelected.shift();
                        $(this).val(valSelected);
                        $('.select2').select2();
                        return false;
                    }
                    if ($("select[name='Versandklassen']").length >= 1) {
                        $(this).val("-1");
                        $('#addNewShippingClassCombi').prop('disabled', true);
                        remove = true;
                    }
                    $(this).val("-1");
                    $('#addNewShippingClassCombi').prop('disabled', true);
                    $('.select2').select2();
                } else {
                    $('#addNewShippingClassCombi').prop('disabled', false);
                }
            } else {
                if (remove) {
                    $(this).parent().parent().detach();
                }
            }
        });
    }
    
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_assignInScope('cTitel', __('createShippingMethodTitle'));
$_smarty_tpl->_assignInScope('cBeschreibung', __('createShippingMethodDesc'));?>

<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->kVersandart)) && $_smarty_tpl->tpl_vars['Versandart']->value->kVersandart > 0) {?>
    <?php $_smarty_tpl->_assignInScope('cTitel', sprintf(__('modifyedShippingTypeTitle'),$_smarty_tpl->tpl_vars['Versandart']->value->cName));?>
    <?php $_smarty_tpl->_assignInScope('cBeschreibung', '');
}?>

<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>$_smarty_tpl->tpl_vars['cTitel']->value,'cBeschreibung'=>$_smarty_tpl->tpl_vars['cBeschreibung']->value), 0, false);
?>
<div id="content">
    <form name="versandart_neu" method="post" action="versandarten.php">
        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

        <input type="hidden" name="neueVersandart" value="1" />
        <input type="hidden" name="kVersandberechnung" value="<?php echo $_smarty_tpl->tpl_vars['versandberechnung']->value->kVersandberechnung;?>
" />
        <input type="hidden" name="kVersandart" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->kVersandart))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->kVersandart;
}?>" />
        <input type="hidden" name="cModulId" value="<?php echo $_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId;?>
" />
        <div class="row">
            <div class="col-12 col-xl-6 settings">
                <div class="card">
                    <div class="card-header">
                        <div class="subheading1"><?php echo __('general');?>
</div>
                        <hr class="mb-n3">
                    </div>
                    <div class="card-body">
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cName"><?php echo __('shippingMethodName');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" type="text" id="cName" name="cName" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cName))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->cName;
}?>" />
                            </div>
                        </div>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                            <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {?>
                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="cName_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('showedName');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <input class="form-control" type="text" id="cName_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" name="cName_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cName))) {
echo $_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cName;
}?>" />
                                    </div>
                                </div>
                            <?php }?>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="nSort"><?php echo __('sortnr');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" type="text" id="nSort" name="nSort" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->nSort))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->nSort;
}?>" />
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('versandartenSortDesc')),$_smarty_tpl ) );?>
</div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cBild"><?php echo __('pictureURL');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" type="text" id="cBild" name="cBild" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cBild))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->cBild;
}?>" />
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('pictureDesc')),$_smarty_tpl ) );?>
</div>
                        </div>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                            <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {?>
                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="cHinweistextShop_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('shippingNoteShop');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <textarea id="cHinweistextShop_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" class="form-control combo" name="cHinweistextShop_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cHinweistextShop))) {
echo $_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cHinweistextShop;
}?></textarea>
                                    </div>
                                </div>
                            <?php }?>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                            <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {?>
                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="cHinweistext_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('shippingNoteEmail');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <textarea id="cHinweistext_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" class="form-control combo" name="cHinweistext_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cHinweistext))) {
echo $_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cHinweistext;
}?></textarea>
                                    </div>
                                </div>
                            <?php }?>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <div class="form-group form-row align-items-center mt-7">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="nMinLiefertage"><?php echo __('minLiefertage');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" type="text" id="nMinLiefertage" name="nMinLiefertage" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->nMinLiefertage))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->nMinLiefertage;
}?>" />
                            </div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="nMaxLiefertage"><?php echo __('maxLiefertage');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input class="form-control" type="text" id="nMaxLiefertage" name="nMaxLiefertage" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->nMaxLiefertage))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->nMaxLiefertage;
}?>" />
                            </div>
                        </div>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['availableLanguages']->value, 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                            <?php $_smarty_tpl->_assignInScope('cISO', $_smarty_tpl->tpl_vars['language']->value->getIso());?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]))) {?>
                                <div class="form-group form-row align-items-center">
                                    <label class="col col-sm-4 col-form-label text-sm-right" for="cLieferdauer_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
"><?php echo __('shippingTime');?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value->getLocalizedName();?>
):</label>
                                    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                        <input class="form-control" type="text" id="cLieferdauer_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" name="cLieferdauer_<?php echo $_smarty_tpl->tpl_vars['cISO']->value;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cLieferdauer))) {
echo $_smarty_tpl->tpl_vars['oVersandartSpracheAssoc_arr']->value[$_smarty_tpl->tpl_vars['cISO']->value]->cLieferdauer;
}?>" />
                                    </div>
                                </div>
                            <?php }?>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <div class="form-group form-row align-items-center mt-7">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cAnzeigen"><?php echo __('showShippingMethod');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="cAnzeigen" id="cAnzeigen" class="custom-select combo">
                                    <option value="immer" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cAnzeigen)) && $_smarty_tpl->tpl_vars['Versandart']->value->cAnzeigen === 'immer') {?>selected<?php }?>><?php echo __('always');?>
</option>
                                    <option value="guenstigste" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cAnzeigen)) && $_smarty_tpl->tpl_vars['Versandart']->value->cAnzeigen === 'guenstigste') {?>selected<?php }?>><?php echo __('lowest');?>
</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cIgnoreShippingProposal"><?php echo __('excludeShippingProposal');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="cIgnoreShippingProposal" id="cIgnoreShippingProposal" class="custom-select combo">
                                    <option value="N" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cIgnoreShippingProposal)) && $_smarty_tpl->tpl_vars['Versandart']->value->cIgnoreShippingProposal === 'N') {?>selected<?php }?>><?php echo __('no');?>
</option>
                                    <option value="Y" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cIgnoreShippingProposal)) && $_smarty_tpl->tpl_vars['Versandart']->value->cIgnoreShippingProposal === 'Y') {?>selected<?php }?>><?php echo __('yes');?>
</option>
                                </select>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('excludeShippingProposalDesc')),$_smarty_tpl ) );?>
</div>
                        </div>

                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cNurAbhaengigeVersandart"><?php echo __('onlyForOwnShippingPrices');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="cNurAbhaengigeVersandart" id="cNurAbhaengigeVersandart" class="combo custom-select">
                                    <option value="N" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cNurAbhaengigeVersandart)) && $_smarty_tpl->tpl_vars['Versandart']->value->cNurAbhaengigeVersandart === 'N') {?>selected<?php }?>><?php echo __('no');?>
</option>
                                    <option value="Y" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cNurAbhaengigeVersandart)) && $_smarty_tpl->tpl_vars['Versandart']->value->cNurAbhaengigeVersandart === 'Y') {?>selected<?php }?>><?php echo __('yes');?>
</option>
                                </select>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('ownShippingPricesDesc')),$_smarty_tpl ) );?>
</div>
                        </div>

                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="eSteuer"><?php echo __('taxshippingcosts');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="eSteuer" id="eSteuer" class="combo custom-select">
                                    <option value="brutto" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->eSteuer)) && $_smarty_tpl->tpl_vars['Versandart']->value->eSteuer === 'brutto') {?>selected<?php }?>><?php echo __('gross');?>
</option>
                                    <option value="netto" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->eSteuer)) && $_smarty_tpl->tpl_vars['Versandart']->value->eSteuer === 'netto') {?>selected<?php }?>><?php echo __('net');?>
</option>
                                </select>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('taxshippingcostsDesc')),$_smarty_tpl ) );?>
</div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cSendConfirmationMail"><?php echo __('sendShippingNotification');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="cSendConfirmationMail" id="cSendConfirmationMail" class="combo custom-select">
                                    <option value="Y" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cSendConfirmationMail)) && $_smarty_tpl->tpl_vars['Versandart']->value->cSendConfirmationMail === 'Y') {?>selected<?php }?>><?php echo __('yes');?>
</option>
                                    <option value="N" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->cSendConfirmationMail)) && $_smarty_tpl->tpl_vars['Versandart']->value->cSendConfirmationMail === 'N') {?>selected<?php }?>><?php echo __('no');?>
</option>
                                </select>
                            </div>
                                                    </div>
                        <div class="form-group form-row align-items-center mt-7">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="kKundengruppe"><?php echo __('customerclass');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="kKundengruppe[]"
                                        id="kKundengruppe"
                                        multiple="multiple"
                                        class="selectpicker custom-select"
                                        data-selected-text-format="count > 2"
                                        data-size="7"
                                        data-actions-box="true">
                                    <option value="-1" <?php if (empty($_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value) || (isset($_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value['alle'])) && $_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value['alle']) {?>selected<?php }?>><?php echo __('all');?>
</option>
                                    <option data-divider="true"></option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['customerGroups']->value, 'customerGroup');
$_smarty_tpl->tpl_vars['customerGroup']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['customerGroup']->value) {
$_smarty_tpl->tpl_vars['customerGroup']->do_else = false;
?>
                                        <?php $_smarty_tpl->_assignInScope('classID', $_smarty_tpl->tpl_vars['customerGroup']->value->getID());?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['classID']->value;?>
" <?php if ((isset($_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value[$_smarty_tpl->tpl_vars['classID']->value])) && $_smarty_tpl->tpl_vars['gesetzteKundengruppen']->value[$_smarty_tpl->tpl_vars['classID']->value]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['customerGroup']->value->getName();?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                            <div class="col-auto ml-sm-n4 order-2 order-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('customerclassDesc')),$_smarty_tpl ) );?>
</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-6">
                <div class="card">
                    <div class="card-body">
                    <?php if ($_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId === 'vm_versandberechnung_gewicht_jtl' || $_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId === 'vm_versandberechnung_warenwert_jtl' || $_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId === 'vm_versandberechnung_artikelanzahl_jtl') {?>
                        <div class="subheading1"><?php echo __('priceScale');?>
</div>
                        <hr class="mb-3">
                        <ul class="jtl-list-group">
                            <li class="input-group">
                                <table id="price_range" class="table">
                                    <thead></thead>
                                    <tbody>
                                    <?php if ((isset($_smarty_tpl->tpl_vars['VersandartStaffeln']->value)) && count($_smarty_tpl->tpl_vars['VersandartStaffeln']->value) > 0) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['VersandartStaffeln']->value, 'oPreisstaffel');
$_smarty_tpl->tpl_vars['oPreisstaffel']->index = -1;
$_smarty_tpl->tpl_vars['oPreisstaffel']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oPreisstaffel']->value) {
$_smarty_tpl->tpl_vars['oPreisstaffel']->do_else = false;
$_smarty_tpl->tpl_vars['oPreisstaffel']->index++;
$__foreach_oPreisstaffel_5_saved = $_smarty_tpl->tpl_vars['oPreisstaffel'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['oPreisstaffel']->value->fBis != 999999999) {?>
                                                <tr>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><label><?php echo __('upTo');?>
</label></span>
                                                            <input type="text" id="bis<?php echo $_smarty_tpl->tpl_vars['oPreisstaffel']->index;?>
" name="bis[]" value="<?php if ((isset($_smarty_tpl->tpl_vars['VersandartStaffeln']->value[$_smarty_tpl->tpl_vars['oPreisstaffel']->index]->fBis))) {
echo $_smarty_tpl->tpl_vars['VersandartStaffeln']->value[$_smarty_tpl->tpl_vars['oPreisstaffel']->index]->fBis;
}?>" class="form-control kilogram" />
                                                            <span class="input-group-addon"><label><?php echo $_smarty_tpl->tpl_vars['einheit']->value;?>
</label></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><label><?php echo __('amount');?>
:</label></span>
                                                            <input type="text" id="preis<?php echo $_smarty_tpl->tpl_vars['oPreisstaffel']->index;?>
" name="preis[]" value="<?php if ((isset($_smarty_tpl->tpl_vars['VersandartStaffeln']->value[$_smarty_tpl->tpl_vars['oPreisstaffel']->index]->fPreis))) {
echo $_smarty_tpl->tpl_vars['VersandartStaffeln']->value[$_smarty_tpl->tpl_vars['oPreisstaffel']->index]->fPreis;
}?>" class="form-control price_large">                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['oPreisstaffel'] = $__foreach_oPreisstaffel_5_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    <?php } else { ?>
                                        <tr>
                                            <td>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><label><?php echo __('upTo');?>
</label></span>
                                                    <input type="text" id="bis1" name="bis[]" value="" class="form-control kilogram" />
                                                    <span class="input-group-addon"><label><?php echo $_smarty_tpl->tpl_vars['einheit']->value;?>
</label></span>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><label><?php echo __('amount');?>
:</label></span>
                                                    <input type="text" id="preis1" name="preis[]" value="" class="form-control price_large">                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="ml-auto col-sm-6 mb-2">
                                <button name="delRow" type="button" value="<?php echo __('delPriceScale');?>
" onclick="delInputRow();" class="btn btn-outline-primary btn-block">
                                    <i class="fas fa-trash-alt"></i> <?php echo __('delPriceScale');?>

                                </button>
                            </div>
                            <div class="col-sm-6">
                                <button name="addRow" type="button" value="<?php echo __('addPriceScale');?>
" onclick="addInputRow();" class="btn btn-primary btn-block">
                                    <i class="fas fa-share"></i> <?php echo __('addPriceScale');?>

                                </button>
                            </div>
                        </div>
                        <hr class="mb-3">
                    <?php } elseif ($_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId === 'vm_versandkosten_pauschale_jtl') {?>
                        <div class="subheading1"><?php echo __('shippingPrice');?>
</div>
                        <hr class="mb-3">
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right">
                                <?php echo __('amount');?>
:
                            </label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input type="text" id="fPreisNetto" name="fPreis" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->fPreis))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->fPreis;
}?>" class="form-control price_large">                            </div>
                        </div>
                    <?php }?>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right">
                                <?php if ($_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId === 'vm_versandberechnung_warenwert_jtl') {?>
                                    <?php echo __('freeShippingBasketValue');?>
:
                                <?php } else { ?>
                                    <?php echo __('freeShipping');?>
:
                                <?php }?>
                            </label>
                            <div class="col-sm-4 pl-sm-3 order-last order-sm-2">
                                <select id="versandkostenfreiAktiv" name="versandkostenfreiAktiv" class="custom-select">
                                    <option value="0"><?php echo __('no');?>
</option>
                                    <option value="1" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->fVersandkostenfreiAbX)) && $_smarty_tpl->tpl_vars['Versandart']->value->fVersandkostenfreiAbX > 0) {?>selected<?php }?>><?php echo __('yes');?>
</option>
                                </select>
                            </div>
                            <div class="col-sm pr-sm-5 order-last order-sm-2 <?php if ($_smarty_tpl->tpl_vars['versandberechnung']->value->cModulId === 'vm_versandberechnung_warenwert_jtl') {?>d-none<?php }?>">
                                <input type="text" id="fVersandkostenfreiAbX" name="fVersandkostenfreiAbX" class="form-control price_large" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->fVersandkostenfreiAbX))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->fVersandkostenfreiAbX;
}?>">                            </div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right">
                                <?php echo __('maxCosts');?>
:
                            </label>
                            <div class="col-sm-4 pl-sm-3 order-last order-sm-2">
                                <select id="versanddeckelungAktiv" name="versanddeckelungAktiv" class="combo custom-select">
                                    <option value="0"><?php echo __('no');?>
</option>
                                    <option value="1" <?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->fDeckelung)) && $_smarty_tpl->tpl_vars['Versandart']->value->fDeckelung > 0) {?>selected<?php }?>><?php echo __('yes');?>
</option>
                                </select>
                            </div>
                            <div class="col-sm pr-sm-5 order-last order-sm-2">
                                <input type="text" id="fDeckelung" name="fDeckelung" value="<?php if ((isset($_smarty_tpl->tpl_vars['Versandart']->value->fDeckelung))) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->fDeckelung;
}?>" class="form-control price_large">                            </div>
                        </div>
                        <div class="mt-7">
                            <div class="subheading1"><?php echo __('validOnShippingClasses');?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('shippingclassDesc')),$_smarty_tpl ) );?>
</div>
                            <hr class="mb-3">
                            <input name="kVersandklasse" type="hidden" value="<?php if (!empty($_smarty_tpl->tpl_vars['Versandart']->value->cVersandklassen)) {
echo $_smarty_tpl->tpl_vars['Versandart']->value->cVersandklassen;
} else { ?>-1<?php }?>">
                            <div id="ulVK" class="jtl-list-group">
                                <div id='liVKneu' class="input-group al" style="display:none;">
                                    <div class="col-sm">
                                        <selectX class="selectX2 custom-select" name="Versandklassen"
                                                 onchange="checkCombination();updateVK();"
                                                 multiple>
                                            <option value="-1"><?php echo __('allCombinations');?>
</option>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['versandKlassen']->value, 'vk');
$_smarty_tpl->tpl_vars['vk']->iteration = 0;
$_smarty_tpl->tpl_vars['vk']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['vk']->value) {
$_smarty_tpl->tpl_vars['vk']->do_else = false;
$_smarty_tpl->tpl_vars['vk']->iteration++;
$__foreach_vk_6_saved = $_smarty_tpl->tpl_vars['vk'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['vk']->value->kVersandklasse;?>
"><?php echo $_smarty_tpl->tpl_vars['vk']->value->cName;?>
</option>
                                            <?php
$_smarty_tpl->tpl_vars['vk'] = $__foreach_vk_6_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </selectX>
                                    </div>
                                    <div>
                                        <button class="btn btn-link pl-0" type="button"
                                                onclick="$(this).parent().parent().detach(); updateVK();">
                                            <span class="far fa-trash-alt"></span>
                                        </button>
                                    </div>
                                </div>
                                <?php if (!empty($_smarty_tpl->tpl_vars['Versandart']->value->cVersandklassen)) {?>
                                    <?php $_smarty_tpl->_assignInScope('aVK', explode(' ',$_smarty_tpl->tpl_vars['Versandart']->value->cVersandklassen));?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['aVK']->value, 'VK');
$_smarty_tpl->tpl_vars['VK']->iteration = 0;
$_smarty_tpl->tpl_vars['VK']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['VK']->value) {
$_smarty_tpl->tpl_vars['VK']->do_else = false;
$_smarty_tpl->tpl_vars['VK']->iteration++;
$__foreach_VK_7_saved = $_smarty_tpl->tpl_vars['VK'];
?>
                                        <div class="input-group align-baseline mt-2">
                                            <div class="col-sm">
                                                <select class="select2 custom-select" name="Versandklassen"
                                                        onchange="checkCombination();updateVK();" multiple="multiple">
                                                    <option value="-1"<?php if ($_smarty_tpl->tpl_vars['VK']->iteration > 1) {?> disabled="disabled"<?php }
if ($_smarty_tpl->tpl_vars['VK']->value === '-1') {?> selected<?php }?>><?php echo __('allCombinations');?>
</option>
                                                    <?php if ($_smarty_tpl->tpl_vars['VK']->value === '-1') {?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['versandKlassen']->value, 'vclass');
$_smarty_tpl->tpl_vars['vclass']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['vclass']->value) {
$_smarty_tpl->tpl_vars['vclass']->do_else = false;
?>
                                                            <option value="<?php echo $_smarty_tpl->tpl_vars['vclass']->value->kVersandklasse;?>
"><?php echo $_smarty_tpl->tpl_vars['vclass']->value->cName;?>
</option>
                                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    <?php } else { ?>
                                                        <?php $_smarty_tpl->_assignInScope('vkID', explode('-',$_smarty_tpl->tpl_vars['VK']->value));?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['versandKlassen']->value, 'vclass');
$_smarty_tpl->tpl_vars['vclass']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['vclass']->value) {
$_smarty_tpl->tpl_vars['vclass']->do_else = false;
?>
                                                        <option value="<?php echo $_smarty_tpl->tpl_vars['vclass']->value->kVersandklasse;?>
"<?php if (in_array($_smarty_tpl->tpl_vars['vclass']->value->kVersandklasse,$_smarty_tpl->tpl_vars['vkID']->value)) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['vclass']->value->cName;?>
</option>
                                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <?php if ($_smarty_tpl->tpl_vars['VK']->iteration != 1) {?>
                                                <div>
                                                    <button class="btn btn-link pl-0" type="button"
                                                            onclick="$(this).parent().parent().detach(); updateVK();">
                                                        <span class="far fa-trash-alt"></span>
                                                    </button>
                                                </div>
                                            <?php }?>
                                        </div>
                                    <?php
$_smarty_tpl->tpl_vars['VK'] = $__foreach_VK_7_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                <?php } else { ?>
                                    <div class="input-group">
                                        <select class="select2 custom-select" name="Versandklassen"
                                                onchange="checkCombination();updateVK();" multiple="multiple">
                                            <option value="-1"><?php echo __('allCombinations');?>
</option>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['versandKlassen']->value, 'vclass');
$_smarty_tpl->tpl_vars['vclass']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['vclass']->value) {
$_smarty_tpl->tpl_vars['vclass']->do_else = false;
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['vclass']->value->kVersandklasse;?>
"><?php echo $_smarty_tpl->tpl_vars['vclass']->value->cName;?>
</option>
                                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </select>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="row mt-4">
                                <?php if (!empty($_smarty_tpl->tpl_vars['missingShippingClassCombis']->value)) {?>
                                    <div class="ml-auto col-sm-6 col-lg-auto">
                                        <button class="btn btn-warning btn-block" type="button" data-toggle="collapse" data-target="#collapseShippingClasses" aria-expanded="false" aria-controls="collapseShippingClasses">
                                            <?php echo __('showMissingCombinations');?>

                                        </button>
                                    </div>
                                <?php }?>
                                <div class="<?php if (empty($_smarty_tpl->tpl_vars['missingShippingClassCombis']->value)) {?>ml-auto<?php }?> col-sm-6 col-lg-auto mb-2">
                                    <button id="addNewShippingClassCombi" class="btn btn-primary btn-block" type="button"
                                            onclick="addShippingCombination();$('.select2').select2();">
                                        <span class="far fa-plus"></span> <?php echo __('addShippingClass');?>

                                    </button>
                                </div>
                            </div>
                            <?php if (!empty($_smarty_tpl->tpl_vars['missingShippingClassCombis']->value)) {?>
                                <div class="collapse row" id="collapseShippingClasses">
                                    <?php if ($_smarty_tpl->tpl_vars['missingShippingClassCombis']->value === -1) {?>
                                        <div class="col-xs-12">
                                            <?php echo __('coverageShippingClassCombination');?>

                                            <?php echo smarty_modifier_replace(__('noShipClassCombiValidation'),'%s',(defined('SHIPPING_CLASS_MAX_VALIDATION_COUNT') ? constant('SHIPPING_CLASS_MAX_VALIDATION_COUNT') : null));?>

                                        </div>
                                    <?php } else { ?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['missingShippingClassCombis']->value, 'mscc');
$_smarty_tpl->tpl_vars['mscc']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['mscc']->value) {
$_smarty_tpl->tpl_vars['mscc']->do_else = false;
?>
                                            <div class="col-auto">
                                                <span class="badge badge-info"><?php echo $_smarty_tpl->tpl_vars['mscc']->value;?>
</span>
                                            </div>
                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    <?php }?>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="subheading1"><?php echo __('acceptedPaymentMethods');?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>__('acceptedPaymentMethodsDesc')),$_smarty_tpl ) );?>
</div>
                        <hr class="mb-n3">
                    </div>
                    <div class="card-body">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['zahlungsarten']->value, 'zahlungsart');
$_smarty_tpl->tpl_vars['zahlungsart']->index = -1;
$_smarty_tpl->tpl_vars['zahlungsart']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['zahlungsart']->value) {
$_smarty_tpl->tpl_vars['zahlungsart']->do_else = false;
$_smarty_tpl->tpl_vars['zahlungsart']->index++;
$__foreach_zahlungsart_12_saved = $_smarty_tpl->tpl_vars['zahlungsart'];
?>
                            <?php $_smarty_tpl->_assignInScope('kZahlungsart', $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart);?>
                            <div class="form-group form-row align-items-center mb-5 mb-md-3">
                                <div class="col-12 col-md mb-1 mb-md-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"
                                               id="kZahlungsart<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->index;?>
"
                                               name="kZahlungsart[]"
                                               class="custom-control-input"
                                               value="<?php echo $_smarty_tpl->tpl_vars['kZahlungsart']->value;?>
"
                                                <?php if ((isset($_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->checked))) {
echo $_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->checked;
}?> />
                                        <label class="custom-control-label" for="kZahlungsart<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->index;?>
">
                                            <?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->cName;
if ((isset($_smarty_tpl->tpl_vars['zahlungsart']->value->cAnbieter)) && strlen($_smarty_tpl->tpl_vars['zahlungsart']->value->cAnbieter) > 0) {?> (<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->cAnbieter;?>
)<?php }?>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto ml-md-3 text-md-right"><?php echo __('discount');?>
:</div>
                                <div class="col ml-md-3">
                                    <input type="text" id="Netto_<?php echo $_smarty_tpl->tpl_vars['kZahlungsart']->value;?>
" name="fAufpreis_<?php echo $_smarty_tpl->tpl_vars['kZahlungsart']->value;?>
" value="<?php if ((isset($_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->fAufpreis))) {
echo $_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->fAufpreis;
}?>" class="form-control price_large" />
                                </div>
                                <div class="col-auto ml-md-3">
                                    <select name="cAufpreisTyp_<?php echo $_smarty_tpl->tpl_vars['kZahlungsart']->value;?>
" id="cAufpreisTyp_<?php echo $_smarty_tpl->tpl_vars['kZahlungsart']->value;?>
" class="custom-select">
                                        <option value="festpreis"<?php if ((isset($_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->cAufpreisTyp)) && $_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->cAufpreisTyp === 'festpreis') {?> selected<?php }?>>
                                            <?php echo __('amount');?>

                                        </option>
                                        <option value="prozent"<?php if ((isset($_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->cAufpreisTyp)) && $_smarty_tpl->tpl_vars['VersandartZahlungsarten']->value[$_smarty_tpl->tpl_vars['kZahlungsart']->value]->cAufpreisTyp === 'prozent') {?> selected<?php }?>>
                                            %
                                        </option>
                                    </select>
                                    <span id="ZahlungsartAufpreis_<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart;?>
" class="ZahlungsartAufpreis"></span>
                                </div>
                            </div>
                        <?php
$_smarty_tpl->tpl_vars['zahlungsart'] = $__foreach_zahlungsart_12_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="subheading1"><?php echo __('shipToCountries');?>
</div>
                <hr class="mb-n3">
            </div>
            <div class="card-body">
                <div class="accordion" id="shippingTo">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['continents']->value, 'continent', false, 'continentKey');
$_smarty_tpl->tpl_vars['continent']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['continentKey']->value => $_smarty_tpl->tpl_vars['continent']->value) {
$_smarty_tpl->tpl_vars['continent']->do_else = false;
?>
                    <div class="accordion-row">
                        <div class="accordion-header" id="continent-heading-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
">
                            <div class="row align-items-center">
                                <div class="col mr-auto accordion-title cursor-pointer"
                                     data-toggle="collapse"
                                     data-target="#collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"
                                     aria-expanded="false"
                                     aria-controls="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
">
                                    <?php echo $_smarty_tpl->tpl_vars['continent']->value->name;?>

                                </div>
                                <div class="col-auto ml-auto">
                                    <button class="btn btn-link text-decoration-none btn-sm text-muted font-size-base dropdown-toggle"
                                            type="button"
                                            data-toggle="collapse"
                                            data-target="#collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"
                                            aria-expanded="false"
                                            aria-controls="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
">
                                        <?php echo $_smarty_tpl->tpl_vars['continent']->value->countriesCount;?>
 <?php echo __('countries');?>

                                        <i class="far fa-chevron-down rotate-180 ml-2"></i>
                                    </button>
                                </div>
                                <div class="w-100 d-md-none"></div>
                                <div class="col-auto cursor-pointer"
                                     data-toggle="collapse"
                                     data-target="#collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"
                                     aria-expanded="false"
                                     aria-controls="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
">
                                    <span data-select-all-count="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['continent']->value->countriesSelectedCount;?>
</span> <?php echo __('countriesSelected');?>

                                </div>
                                <div class="col-auto ml-auto">
                                    <button class="btn btn-link btn-sm font-size-base pr-2"
                                            type="button"
                                            data-select-all="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"
                                            aria-controls="countryCollapse-<?php echo '<?php ';?>
echo $i; <?php echo '?>';?>
">
                                        <span class="fal fa-check-square mr-2"></span> <?php echo __('all');?>

                                    </button>
                                    <button class="btn btn-link btn-sm font-size-base pl-2"
                                            type="button"
                                            data-deselect-all="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"
                                            aria-controls="countryCollapse-<?php echo '<?php ';?>
echo $i; <?php echo '?>';?>
">
                                        <span class="fal fa-square mr-2"></span> <?php echo __('none');?>

                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-body">
                            <div id="collapse-continent-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
" class="collapse" aria-labelledby="continent-heading-<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
" data-parent="#shippingTo">
                                <div class="row">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['continent']->value->countries, 'country');
$_smarty_tpl->tpl_vars['country']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->do_else = false;
?>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input"
                                                   type="checkbox" name="land[]"
                                                   data-id="country_<?php echo $_smarty_tpl->tpl_vars['country']->value->getISO();?>
"
                                                   value="<?php echo $_smarty_tpl->tpl_vars['country']->value->getISO();?>
"
                                                   id="country_<?php echo $_smarty_tpl->tpl_vars['country']->value->getISO();?>
_<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"
                                                    <?php if ((isset($_smarty_tpl->tpl_vars['gewaehlteLaender']->value)) && is_array($_smarty_tpl->tpl_vars['gewaehlteLaender']->value) && in_array($_smarty_tpl->tpl_vars['country']->value->getISO(),$_smarty_tpl->tpl_vars['gewaehlteLaender']->value)) {?> checked="checked"<?php }?> />
                                            <label class="custom-control-label" for="country_<?php echo $_smarty_tpl->tpl_vars['country']->value->getISO();?>
_<?php echo $_smarty_tpl->tpl_vars['continentKey']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['country']->value->getName();?>
</label>
                                        </div>
                                    </div>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
        <div class="save-wrapper">
            <div class="row">
                <div class="ml-auto col-sm-6 col-lg-auto">
                    <a href="versandarten.php" title="<?php echo __('cancel');?>
" class="btn btn-outline-primary btn-block">
                        <?php echo __('cancelWithIcon');?>

                    </a>
                </div>
                <div class="col-sm-6 col-lg-auto">
                    <button type="submit"
                            value="<?php if (!(isset($_smarty_tpl->tpl_vars['Versandart']->value->kVersandart)) || !$_smarty_tpl->tpl_vars['Versandart']->value->kVersandart) {
echo __('createShippingType');
} else {
echo __('modifyedShippingType');
}?>"
                            class="btn btn-primary btn-block">
                        <?php if (!(isset($_smarty_tpl->tpl_vars['Versandart']->value->kVersandart)) || !$_smarty_tpl->tpl_vars['Versandart']->value->kVersandart) {?>
                            <i class="fa fa-share"></i> <?php echo __('createShippingType');?>

                        <?php } else { ?>
                            <?php echo __('saveWithIcon');?>

                        <?php }?>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

    <?php echo '<script'; ?>
 type="text/javascript">
        $('input[name="land[]"]').on('change', function () {
            $('input[data-id="' + $(this).data('id') + '"]').prop('checked', $(this).prop('checked'));
        });
    <?php echo '</script'; ?>
>

<?php }
}
