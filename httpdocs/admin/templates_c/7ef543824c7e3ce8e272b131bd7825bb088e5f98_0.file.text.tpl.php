<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/fields/text.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fcfa352_90998941',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ef543824c7e3ce8e272b131bd7825bb088e5f98' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/fields/text.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fcfa352_90998941 (Smarty_Internal_Template $_smarty_tpl) {
if (!(isset($_smarty_tpl->tpl_vars['json']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('json', 0);
}
if ($_smarty_tpl->tpl_vars['json']->value == true) {?>
    <?php $_smarty_tpl->_assignInScope('fieldName', "json[".((string)$_smarty_tpl->tpl_vars['name']->value)."]");
} else { ?>
    <?php $_smarty_tpl->_assignInScope('fieldName', $_smarty_tpl->tpl_vars['name']->value);
}
if (!(isset($_smarty_tpl->tpl_vars['value']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('value', '');
}
if (!(isset($_smarty_tpl->tpl_vars['fullWidth']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('colClass', 'col-md-6');?>
    <?php $_smarty_tpl->_assignInScope('formGroupClass', '');
} else { ?>
    <?php $_smarty_tpl->_assignInScope('colClass', 'col-md-12');?>
    <?php $_smarty_tpl->_assignInScope('formGroupClass', 'form-group-fullwidth');
}
if (!(isset($_smarty_tpl->tpl_vars['id']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('id', $_smarty_tpl->tpl_vars['name']->value);
}
if (!(isset($_smarty_tpl->tpl_vars['cssClass']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('cssClass', '');
}
if (!(isset($_smarty_tpl->tpl_vars['wrapperCssClass']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('wrapperCssClass', '');
}
if (!(isset($_smarty_tpl->tpl_vars['textRight']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('textRight', false);
}
if (!(isset($_smarty_tpl->tpl_vars['readonly']->value))) {?>
    <?php $_smarty_tpl->_assignInScope('readonly', false);
}?>
<div class="form-group knowmates-form-group <?php echo $_smarty_tpl->tpl_vars['wrapperCssClass']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['formGroupClass']->value;?>
">
    <div class="row">
        <div class="<?php echo $_smarty_tpl->tpl_vars['colClass']->value;?>
">
            <label for="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
                <span>
                    <?php echo $_smarty_tpl->tpl_vars['label']->value;?>

                </span>
                <?php if ((isset($_smarty_tpl->tpl_vars['description']->value))) {?>
                    <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['components']->value->fieldDescription, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('description'=>((string)$_smarty_tpl->tpl_vars['description']->value)), 0, true);
?>
                <?php }?>
            </label>

        </div>
        <div class="<?php echo $_smarty_tpl->tpl_vars['colClass']->value;?>
 d-flex align-items-center">
            <input id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" name="<?php echo $_smarty_tpl->tpl_vars['fieldName']->value;?>
" type="text" class="form-control <?php echo $_smarty_tpl->tpl_vars['cssClass']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['textRight']->value == true) {?> style="text-align: right;"<?php }?>
                   <?php if ((isset($_smarty_tpl->tpl_vars['required']->value)) && $_smarty_tpl->tpl_vars['required']->value == 'Y') {?>required<?php }?>
                    <?php if ((isset($_smarty_tpl->tpl_vars['onkeyup']->value))) {?>onkeyup="<?php echo $_smarty_tpl->tpl_vars['onkeyup']->value;?>
"<?php }?>
                    <?php if ((isset($_smarty_tpl->tpl_vars['oninput']->value))) {?>oninput="<?php echo $_smarty_tpl->tpl_vars['oninput']->value;?>
"<?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['readonly']->value) {?>readonly<?php }?>
                    <?php if ((isset($_smarty_tpl->tpl_vars['placeholder']->value))) {?>placeholder="<?php echo $_smarty_tpl->tpl_vars['placeholder']->value;?>
"<?php }?>
            >
            <?php if ((isset($_smarty_tpl->tpl_vars['endAdorment']->value))) {?>
                <div class="end-adornment">
                    <?php echo $_smarty_tpl->tpl_vars['endAdorment']->value;?>

                </div>
            <?php }?>
        </div>
    </div>
</div><?php }
}
