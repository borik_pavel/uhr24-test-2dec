<?php
/* Smarty version 3.1.39, created on 2021-10-07 13:32:13
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/buttons/button.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615edabdd5f438_76625863',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0447cd5f5a507177b3a7f43fdce8717dac42e306' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/buttons/button.tpl',
      1 => 1623869889,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615edabdd5f438_76625863 (Smarty_Internal_Template $_smarty_tpl) {
?>
<button
        <?php if ($_smarty_tpl->tpl_vars['params']->value['onclick']->hasValue()) {?>onclick="<?php echo $_smarty_tpl->tpl_vars['params']->value['onclick']->getValue();?>
"<?php }?>
        class="btn btn-<?php echo $_smarty_tpl->tpl_vars['params']->value['buttonType']->getValue();?>
 <?php if ($_smarty_tpl->tpl_vars['params']->value['variant']->hasValue()) {?>btn-<?php echo $_smarty_tpl->tpl_vars['params']->value['variant']->getValue();
}?> <?php echo $_smarty_tpl->tpl_vars['params']->value['class']->getValue();?>
"
        <?php if ($_smarty_tpl->tpl_vars['params']->value['style']->hasValue()) {?>style="<?php echo $_smarty_tpl->tpl_vars['params']->value['style']->getValue();?>
"<?php }?>
        type="<?php echo $_smarty_tpl->tpl_vars['params']->value['type']->getValue();?>
"
        <?php if ($_smarty_tpl->tpl_vars['params']->value['id']->hasValue()) {?>id="<?php echo $_smarty_tpl->tpl_vars['params']->value['id']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['onMouseOver']->hasValue()) {?>onMouseOver="<?php echo $_smarty_tpl->tpl_vars['params']->value['onMouseOver']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['data-dismiss']->hasValue()) {?>data-dismiss="<?php echo $_smarty_tpl->tpl_vars['params']->value['data-dismiss']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['disabled']->getValue()) {?>disabled=""<?php }?>

        <?php if ($_smarty_tpl->tpl_vars['params']->value['name']->hasValue()) {?>name="<?php echo $_smarty_tpl->tpl_vars['params']->value['name']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['value']->hasValue()) {?>value="<?php echo $_smarty_tpl->tpl_vars['params']->value['value']->getValue();?>
"<?php }?>
>
    <?php if ($_smarty_tpl->tpl_vars['params']->value['iconPosition']->hasValue() && $_smarty_tpl->tpl_vars['params']->value['iconPosition']->getValue() === "left" && $_smarty_tpl->tpl_vars['params']->value['icon']->hasValue()) {?>
        <i class="btn-loader-svg<?php if ((isset($_smarty_tpl->tpl_vars['content']->value)) && $_smarty_tpl->tpl_vars['content']->value != '') {?> mr-1<?php }?>" style="display: none"></i>
        <i class="<?php echo $_smarty_tpl->tpl_vars['params']->value['icon']->getValue();
if ((isset($_smarty_tpl->tpl_vars['content']->value)) && $_smarty_tpl->tpl_vars['content']->value != '') {?> mr-1<?php }?>"></i>
    <?php }?>
        <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

        <?php if ($_smarty_tpl->tpl_vars['params']->value['iconPosition']->hasValue() && $_smarty_tpl->tpl_vars['params']->value['iconPosition']->getValue() === "right" && $_smarty_tpl->tpl_vars['params']->value['icon']->hasValue()) {?>
        <i class="btn-loader-svg<?php if ((isset($_smarty_tpl->tpl_vars['content']->value)) && $_smarty_tpl->tpl_vars['content']->value != '') {?> ml-1<?php }?>" style="display: none"></i>
        <i class="<?php echo $_smarty_tpl->tpl_vars['params']->value['icon']->getValue();
if ((isset($_smarty_tpl->tpl_vars['content']->value)) && $_smarty_tpl->tpl_vars['content']->value != '') {?> ml-1<?php }?>"></i>
    <?php }?>
</button><?php }
}
