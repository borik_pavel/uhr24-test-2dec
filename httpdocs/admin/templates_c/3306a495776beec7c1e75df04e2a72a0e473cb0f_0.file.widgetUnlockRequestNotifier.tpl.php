<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:08:41
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/widgetUnlockRequestNotifier.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e3e9ace5e6_09214754',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3306a495776beec7c1e75df04e2a72a0e473cb0f' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/widgetUnlockRequestNotifier.tpl',
      1 => 1611753980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e3e9ace5e6_09214754 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="widget-custom-data">
    <?php if ($_smarty_tpl->tpl_vars['requestCount']->value > 0) {?>
        <div class="mb-3">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'group');
$_smarty_tpl->tpl_vars['group']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->do_else = false;
?>
                <?php if ($_smarty_tpl->tpl_vars['group']->value->kRequestCount > 0) {?>
                    <div class="row">
                        <div class="col-6"><strong><?php echo $_smarty_tpl->tpl_vars['group']->value->cGroupName;?>
:</strong></div>
                        <div class="col-auto"><?php echo $_smarty_tpl->tpl_vars['group']->value->kRequestCount;?>
</div>
                    </div>
                <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <p><?php echo __('See Approvals to manage your open requests.');?>
</p>
    <?php } else { ?>
        <div class="alert alert-info">
            <?php echo __('At the moment there are no open requests.');?>

        </div>
    <?php }?>
</div>
<?php }
}
