<?php
/* Smarty version 3.1.39, created on 2021-10-07 13:32:13
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/alert.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615edabde20001_73407017',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '435b3b1fa44d7d646ed8e09de5597d5b570c527a' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/alert.tpl',
      1 => 1623869889,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615edabde20001_73407017 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="alert alert-<?php echo $_smarty_tpl->tpl_vars['params']->value['type']->getValue();
echo $_smarty_tpl->tpl_vars['params']->value['class']->getValue();?>
"
     <?php if ($_smarty_tpl->tpl_vars['params']->value['style']->hasValue()) {?>style="<?php echo $_smarty_tpl->tpl_vars['params']->value['style']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['id']->hasValue()) {?>id="<?php echo $_smarty_tpl->tpl_vars['params']->value['id']->getValue();?>
"<?php }?>>
    <?php if ($_smarty_tpl->tpl_vars['params']->value['hideIcon']->getValue() === false) {?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['icon']->hasValue()) {
echo $_smarty_tpl->tpl_vars['params']->value['icon']->getValue();
} else { ?><i class="<?php echo $_smarty_tpl->tpl_vars['params']->value['iconClass']->getValue();?>
"></i><?php }?>
    <?php }?> <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

</div><?php }
}
