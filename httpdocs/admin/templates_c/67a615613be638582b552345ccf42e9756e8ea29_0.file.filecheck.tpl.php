<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:08:09
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/filecheck.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61652649b53996_68346327',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '67a615613be638582b552345ccf42e9756e8ea29' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/filecheck.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_61652649b53996_68346327 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('filecheck'),'cBeschreibung'=>__('filecheckDesc'),'cDokuURL'=>__('filecheckURL')), 0, false);
?>

<?php echo $_smarty_tpl->tpl_vars['alertList']->value->displayAlertByKey('orphanedFilesError');?>

<?php echo $_smarty_tpl->tpl_vars['alertList']->value->displayAlertByKey('modifiedFilesError');?>

<?php echo $_smarty_tpl->tpl_vars['alertList']->value->displayAlertByKey('backupMessage');?>

<?php echo $_smarty_tpl->tpl_vars['alertList']->value->displayAlertByKey('zipArchiveError');?>


<div class="card collapsed">
    <div class="card-header<?php if (count($_smarty_tpl->tpl_vars['modifiedFiles']->value) > 0) {?> accordion-toggle" data-toggle="collapse" data-target="#pageCheckModifiedFiles" style="cursor:pointer"<?php } else { ?>"<?php }?>>
        <div class="card-title">
            <?php if (count($_smarty_tpl->tpl_vars['modifiedFiles']->value) > 0) {?><i class="fa fas fa-plus"></i> <?php }?>
            <?php echo __('fileCheckNumberModifiedFiles');?>
: <?php echo count($_smarty_tpl->tpl_vars['modifiedFiles']->value);?>

        </div>
    </div>
    <?php if (count($_smarty_tpl->tpl_vars['modifiedFiles']->value) > 0) {?>
        <div class="card-body  collapse" id="pageCheckModifiedFiles">
            <p class="small text-muted"><?php echo __('fileCheckModifiedFilesNote');?>
</p>
            <div id="contentModifiedFilesCheck">
                <table class="table table-sm table-borderless req">
                    <thead>
                    <tr>
                        <th class="text-left"><?php echo __('file');?>
</th>
                        <th class="text-right"><?php echo __('lastModified');?>
</th>
                    </tr>
                    </thead>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['modifiedFiles']->value, 'file');
$_smarty_tpl->tpl_vars['file']->iteration = 0;
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
$_smarty_tpl->tpl_vars['file']->iteration++;
$__foreach_file_0_saved = $_smarty_tpl->tpl_vars['file'];
?>
                        <tr class="filestate mod<?php echo $_smarty_tpl->tpl_vars['file']->iteration%2;?>
 modified">
                            <td class="text-left"><?php echo $_smarty_tpl->tpl_vars['file']->value->name;?>
</td>
                            <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['file']->value->lastModified;?>
</td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['file'] = $__foreach_file_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </table>
            </div>
        </div>
    <?php }?>
</div>
<div class="card collapsed">
    <div class="card-header<?php if (count($_smarty_tpl->tpl_vars['orphanedFiles']->value) > 0) {?> accordion-toggle" data-toggle="collapse" data-target="#pageCheckOrphanedFiles" style="cursor:pointer"<?php } else { ?>"<?php }?>>
        <div class="card-title">
            <?php if (count($_smarty_tpl->tpl_vars['orphanedFiles']->value) > 0) {?><i class="fa fas fa-plus"></i> <?php }?>
            <?php echo __('fileCheckNumberOrphanedFiles');?>
: <?php echo count($_smarty_tpl->tpl_vars['orphanedFiles']->value);?>

        </div>
    </div>
    <?php if (count($_smarty_tpl->tpl_vars['orphanedFiles']->value) > 0) {?>
        <div class="card-body  collapse" id="pageCheckOrphanedFiles">
            <p class="alert alert-info"><?php echo __('fileCheckOrphanedFilesNote');?>
</p>
            <div id="contentOrphanedFilesCheck">
                <table class="table table-sm table-borderless req">
                    <thead>
                        <tr>
                            <th class="text-left"><?php echo __('file');?>
</th>
                            <th class="text-right"><?php echo __('lastModified');?>
</th>
                        </tr>
                    </thead>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orphanedFiles']->value, 'file');
$_smarty_tpl->tpl_vars['file']->iteration = 0;
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
$_smarty_tpl->tpl_vars['file']->iteration++;
$__foreach_file_1_saved = $_smarty_tpl->tpl_vars['file'];
?>
                        <tr class="filestate mod<?php echo $_smarty_tpl->tpl_vars['file']->iteration%2;?>
 orphaned">
                            <td class="text-left"><?php echo $_smarty_tpl->tpl_vars['file']->value->name;?>
</td>
                            <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['file']->value->lastModified;?>
</td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['file'] = $__foreach_file_1_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </table>
                <div class="save-wrapper">
                    <form method="post">
                        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                        <div class="row">
                            <div class="ml-auto col-sm-6 col-xl-auto">
                                <button type="submit" class="btn btn-danger btn-block delete-confirm" name="delete-orphans" value="1" data-modal-body="<?php echo __('confirmDeleteText');?>
">
                                    <i class="fa fas fa-trash"></i> <?php echo __('delete');?>

                                </button>
                            </div>
                            <div class="col-sm-6 col-xl-auto">
                                <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#show-script" aria-expanded="false" aria-controls="show-script">
                                    <i class="fa fas fa-terminal"></i> <?php echo __('showDeleteScript');?>

                                </button>
                            </div>
                        </div>
                        <div class="collapse" id="show-script">
                            <div class="card card-body">
                                <pre style="margin-top:1em;"><?php echo $_smarty_tpl->tpl_vars['deleteScript']->value;?>
</pre>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php }?>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
