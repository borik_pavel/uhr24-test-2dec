<?php
/* Smarty version 3.1.39, created on 2021-10-07 13:32:13
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615edabde76531_60452154',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5056ebf80bd00c655b56df80d3e4595498121814' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/footer.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615edabde76531_60452154 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
    $(document).ready(function () {
        knm_handle_endAdornments();
    });
<?php echo '</script'; ?>
>
<?php if (!empty($_smarty_tpl->tpl_vars['jsFilesFooter']->value)) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jsFilesFooter']->value, 'customJsFile');
$_smarty_tpl->tpl_vars['customJsFile']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['customJsFile']->value) {
$_smarty_tpl->tpl_vars['customJsFile']->do_else = false;
?>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['customJsFile']->value;?>
"><?php echo '</script'; ?>
>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>


    <?php echo '<script'; ?>
>
        function setLanguage(lang, prevLang) {
            if (lang !== prevLang) {
                let urlString = document.location.href;
                urlString = urlString.replace(`&lang=${prevLang}`, '');
                window.location.replace(`${urlString}&lang=${lang}`);
            }
        }
    <?php echo '</script'; ?>
>
<?php }
}
