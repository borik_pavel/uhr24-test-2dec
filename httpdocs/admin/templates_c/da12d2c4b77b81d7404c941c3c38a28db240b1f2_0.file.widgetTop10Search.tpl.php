<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/widgetTop10Search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165261fc194b6_85216060',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'da12d2c4b77b81d7404c941c3c38a28db240b1f2' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/widgetTop10Search.tpl',
      1 => 1611753980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165261fc194b6_85216060 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="widget-custom-data">
	<?php if (count($_smarty_tpl->tpl_vars['searchQueries']->value) > 0) {?>
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><?php echo __('Search queries');?>
</th>
						<th class="text-center"><?php echo __('Count');?>
</th>
					</tr>
				</thead>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['searchQueries']->value, 'query');
$_smarty_tpl->tpl_vars['query']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['query']->value) {
$_smarty_tpl->tpl_vars['query']->do_else = false;
?>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['query']->value->cSuche;?>
</td>
						<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['query']->value->nAnzahlGesuche;?>
</td>
					</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</table>
		</div>
	<?php } else { ?>
		<div class="alert alert-info" role="alert">
            <?php echo __('No search queries found.');?>

		</div>
	<?php }?>
</div>
<?php }
}
