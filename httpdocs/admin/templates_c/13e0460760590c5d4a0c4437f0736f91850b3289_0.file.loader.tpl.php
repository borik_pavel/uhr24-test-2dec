<?php
/* Smarty version 3.1.39, created on 2021-11-04 19:45:31
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/opc/tpl/modals/loader.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61842a4bd153d6_31937513',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '13e0460760590c5d4a0c4437f0736f91850b3289' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/opc/tpl/modals/loader.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61842a4bd153d6_31937513 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="loaderModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo __('Please wait...');?>
</h5>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-info progress-bar-animated"
                         style="width:100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
