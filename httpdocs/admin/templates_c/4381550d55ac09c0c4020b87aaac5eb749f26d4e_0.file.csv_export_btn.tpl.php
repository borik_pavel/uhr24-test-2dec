<?php
/* Smarty version 3.1.39, created on 2021-10-12 12:50:51
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/csv_export_btn.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165688b4fa673_16221132',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4381550d55ac09c0c4020b87aaac5eb749f26d4e' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/csv_export_btn.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165688b4fa673_16221132 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
    function onClickCsvExport_<?php echo $_smarty_tpl->tpl_vars['exporterId']->value;?>
 ()
    {
        window.location = window.location.pathname + '?exportcsv=<?php echo $_smarty_tpl->tpl_vars['exporterId']->value;?>
&token=<?php echo $_SESSION['jtl_token'];?>
';
    }
<?php echo '</script'; ?>
>
<button type="button" class="btn btn-outline-primary btn-block" onclick="onClickCsvExport_<?php echo $_smarty_tpl->tpl_vars['exporterId']->value;?>
()">
    <i class="fa fa-download"></i> <?php echo __('exportCsv');?>

</button><?php }
}
