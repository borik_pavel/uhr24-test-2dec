<?php
/* Smarty version 3.1.39, created on 2021-10-07 08:29:16
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_shopping/adminmenu/templates/custom_mapping.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615e93bc224432_25069107',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f9e12df9ef99ff73430a5047cf4f997a0628c396' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_google_shopping/adminmenu/templates/custom_mapping.tpl',
      1 => 1615108376,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615e93bc224432_25069107 (Smarty_Internal_Template $_smarty_tpl) {
?><form method="post" enctype="multipart/form-data" name="mapping">
    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

    <input type="hidden" name="kPlugin" value="<?php echo $_smarty_tpl->tpl_vars['kPlugin']->value;?>
" />
    <input type="hidden" name="kPluginAdminMenu" value="<?php echo $_smarty_tpl->tpl_vars['kPluginAdminMenu']->value;?>
" />
    <input type="hidden" name="stepPlugin" value="<?php echo $_smarty_tpl->tpl_vars['stepPlugin']->value;?>
" />

    <div>
        <div class="subheading1">
            <?php echo __('Create new mapping');?>
:
        </div>
        <hr>
        <div class="form-group form-row align-items-center">
            <label class="col col-sm-4 col-form-label text-sm-right" for="cType"><?php echo __('type');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <select class="custom-select" id="cType" name="cType" >
                    <option value="0">-<?php echo __('please select');?>
-</option>
                    <option value="Merkmal"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cType'] === 'Merkmal') {?> selected="selected"<?php }?>><?php echo __('Merkmal');?>
</option>
                    <option value="Merkmalwert"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cType'] === 'Merkmalwert') {?> selected="selected"<?php }?>><?php echo __('Merkmalwert');?>
</option>
                </select>
            </div>
        </div>
        <div class="form-group form-row align-items-center zeile">
            <label class="col col-sm-4 col-form-label text-sm-right" for="cVon"><?php echo __('From');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <input class="form-control" type="text" name="cVon" id="cVon"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cVon']))) {?> value="<?php echo $_smarty_tpl->tpl_vars['requestData']->value['cVon'];?>
"<?php }?> />
                <span class="cZu Merkmalwert">
                    <small>(<?php echo __('e.g.');?>
 <?php echo __('very large');?>
)</small>
                </span>
                <span class="cZu Merkmal">
                    <small>(<?php echo __('e.g.');?>
 <?php echo __('Clothing size');?>
)</small>
                </span>
            </div>
        </div>
        <div class="form-group form-row align-items-center zeile">
            <label class="col col-sm-4 col-form-label text-sm-right" for="cZuMerkmal"><?php echo __('To');?>
:</label>
            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                <span class="cZu Merkmalwert">
                    <input class="form-control" id="cZu" type="text" name="cZuMerkmalwert"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmalwert']))) {?> value="<?php echo $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmalwert'];?>
"<?php }?> />
                    <small>(<?php echo __('e.g.');?>
 <?php echo __('XL');?>
)</small>
                </span>
                <span class="cZu Merkmal">
                    <select class="custom-select" name="cZuMerkmal" id="cZuMerkmal">
                        <option value="farbe"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmal'] === 'farbe') {?> selected="selected"<?php }?>><?php echo __('farbe');?>
</option>
                        <option value="groesse"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmal'] === 'groesse') {?> selected="selected"<?php }?>><?php echo __('groesse');?>
</option>
                        <option value="geschlecht"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmal'] === 'geschlecht') {?> selected="selected"<?php }?>><?php echo __('geschlecht');?>
</option>
                        <option value="altersgruppe"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmal'] === 'altersgruppe') {?> selected="selected"<?php }?>><?php echo __('altersgruppe');?>
</option>
                        <option value="muster"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmal'] === 'muster') {?> selected="selected"<?php }?>><?php echo __('muster');?>
</option>
                        <option value="material"<?php if ((isset($_smarty_tpl->tpl_vars['requestData']->value['cType'])) && $_smarty_tpl->tpl_vars['requestData']->value['cZuMerkmal'] === 'material') {?> selected="selected"<?php }?>><?php echo __('material');?>
</option>
                    </select>
                    <small>(<?php echo __('e.g.');?>
 <?php echo __('Size');?>
)</small>
                </span>
            </div>
        </div>
        <div class="row mr-0 mb-4">
            <div class="ml-auto col-sm-6 col-lg-auto">
                <button type="submit" class="btn btn-primary btn-block zeile" name="btn_save_new" value="Neues Attribut speichern">
                    <i class="far fa-save"></i> <?php echo __('Save new mapping');?>

                </button>
            </div>
        </div>
    </div>
    <div class="mt-7">
        <div class="subheading1">
            <?php echo __('Mappings');?>

        </div>
        <hr class="mb-4">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('ID');?>
</th>
                        <th class="TD2"><?php echo __('Type');?>
</th>
                        <th class="TD3"><?php echo __('To');?>
</th>
                        <th class="TD4"><?php echo __('From');?>
</th>
                        <th class="text-center"><?php echo __('Delete');?>
</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($_smarty_tpl->tpl_vars['mappings']->value) > 0) {?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mappings']->value, 'mapping');
$_smarty_tpl->tpl_vars['mapping']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['mapping']->value) {
$_smarty_tpl->tpl_vars['mapping']->do_else = false;
?>
                            <tr>
                                <td class="text-center" style="width: 110px;"><?php echo $_smarty_tpl->tpl_vars['mapping']->value['kMapping'];?>
</td>
                                <td class="TD2"><?php echo __($_smarty_tpl->tpl_vars['mapping']->value['cType']);?>
</td>
                                <td class="TD3"><?php echo __($_smarty_tpl->tpl_vars['mapping']->value['cZu']);?>
</td>
                                <td class="TD4"><?php echo $_smarty_tpl->tpl_vars['mapping']->value['cVon'];?>
</td>
                                <td class="text-center">
                                    <button type="submit" name="btn_delete[<?php echo $_smarty_tpl->tpl_vars['mapping']->value['kMapping'];?>
]" value="<?php echo __('delete');?>
" class="btn btn-link">
                                        <span class="icon-hover">
                                            <span class="fal fa-trash-alt"></span>
                                            <span class="fas fa-trash-alt"></span>
                                        </span>
                                    </button>
                                </td>
                            </tr>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="5">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i> <?php echo __('Currently no optional mappings exists');?>

                                </div>
                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</form>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function() {
        toogleZu();
        $('#cType').change(function() {
            toogleZu();
        });
    });

    function toogleZu() {
        var cValue = $('#cType').val();
        if(cValue == 0) {
            $('.zeile').hide();
        } else {
            $('.zeile').show();
            $('.cZu').hide();
            $('.'+cValue).show();
        }
    }
<?php echo '</script'; ?>
>

<?php }
}
