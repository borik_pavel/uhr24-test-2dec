<?php
/* Smarty version 3.1.39, created on 2021-09-30 18:56:43
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/status.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6155ec4b977108_40356755',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7d6b4082b47cd95fb9ff60af914ec50aa56b564e' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/status.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/systemcheck.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_6155ec4b977108_40356755 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'render_item' => 
  array (
    'compiled_filepath' => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates_c/7d6b4082b47cd95fb9ff60af914ec50aa56b564e_0.file.status.tpl.php',
    'uid' => '7d6b4082b47cd95fb9ff60af914ec50aa56b564e',
    'call_name' => 'smarty_template_function_render_item_21209931626155ec4b8da1c3_81144390',
  ),
));
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
>
    $(function() {
        $('.table tr[data-href]').each(function(){
            $(this).css('cursor','pointer').hover(
                function(){
                    $(this).addClass('active');
                },
                function(){
                    $(this).removeClass('active');
                }).on('click', function(){
                    document.location = $(this).attr('data-href');
                }
            );
        });

        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-item',
            percentPosition: true
        });

    });
<?php echo '</script'; ?>
>



<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/systemcheck.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div id="content">
    <div class="grid">
        <div class="grid-item">
            <div class="card">
                <div class="card-header">
                    <div class="subheading1"><?php echo __('general');?>
</div>
                    <hr class="mb-n3">
                </div>
                <div class="card-body">
                    <table class="table table-striped text-x1 last-child">
                        <tbody>
                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('databaseStructure'),'val'=>$_smarty_tpl->tpl_vars['status']->value->validDatabaseStruct(),'more'=>'dbcheck.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('fileStructure'),'val'=>($_smarty_tpl->tpl_vars['status']->value->validModifiedFileStruct() && $_smarty_tpl->tpl_vars['status']->value->validOrphanedFilesStruct()),'more'=>'filecheck.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('directoryPermissions'),'val'=>$_smarty_tpl->tpl_vars['status']->value->validFolderPermissions(),'more'=>'permissioncheck.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('openUpdates'),'val'=>!$_smarty_tpl->tpl_vars['status']->value->hasPendingUpdates(),'more'=>'dbupdater.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('installDirectory'),'val'=>!$_smarty_tpl->tpl_vars['status']->value->hasInstallDir()), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('profilerActive'),'val'=>!$_smarty_tpl->tpl_vars['status']->value->hasActiveProfiler(),'more'=>'profiler.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('server'),'val'=>$_smarty_tpl->tpl_vars['status']->value->hasValidEnvironment(),'more'=>'systemcheck.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('orphanedCategories'),'val'=>$_smarty_tpl->tpl_vars['status']->value->getOrphanedCategories(),'more'=>'categorycheck.php'), true);?>

                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'render_item', array('title'=>__('newPluginVersions'),'val'=>!$_smarty_tpl->tpl_vars['status']->value->hasNewPluginVersions(),'more'=>'pluginverwaltung.php'), true);?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="grid-item">
            <div class="card">
                <div class="card-header">
                    <div class="heading-body">
                        <div class="subheading1"><?php echo __('cache');?>
</div>
                    </div>
                    <div class="heading-right">
                        <div class="btn-group btn-group-xs">
                            <button class="btn btn-primary dropdown-toggle text-uppercase" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo __('details');?>
 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="cache.php"><?php echo __('systemCache');?>
</a></li>
                                <li class="dropdown-item"><a href="bilderverwaltung.php"><?php echo __('imageCache');?>
</a></li>
                            </ul>
                        </div>
                    </div>
                    <hr class="mb-n3">
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 border-right">
                            <div class="text-center">
                                <?php if ($_smarty_tpl->tpl_vars['status']->value->getObjectCache()->getResultCode() === 1) {?>
                                    <?php $_smarty_tpl->_assignInScope('cacheOptions', $_smarty_tpl->tpl_vars['status']->value->getObjectCache()->getOptions());?>
                                    <i class="fal fa-check-circle text-four-times text-success"></i>
                                    <h3 style="margin-top:10px;margin-bottom:0"><?php echo __('activated');?>
</h3>
                                    <span style="color:#c7c7c7"><?php echo ucfirst($_smarty_tpl->tpl_vars['cacheOptions']->value['method']);?>
</span>
                                <?php } else { ?>
                                    <i class="fa fa-exclamation-circle text-four-times text-info"></i>
                                    <h3 style="margin-top:10px;margin-bottom:0"><?php echo __('deactivated');?>
</h3>
                                    <span style="color:#c7c7c7"><?php echo __('requirementsMet');?>
</span>
                                <?php }?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center">
                                <?php $_smarty_tpl->_assignInScope('imageCache', $_smarty_tpl->tpl_vars['status']->value->getImageCache());?>
                                <i class="fa fa-file-image-o text-four-times text-success"></i>
                                <h3 style="margin-top:10px;margin-bottom:0">
                                    <?php echo round((($_smarty_tpl->tpl_vars['imageCache']->value->getGeneratedBySize(\JTL\Media\Image::SIZE_XS)+$_smarty_tpl->tpl_vars['imageCache']->value->getGeneratedBySize(\JTL\Media\Image::SIZE_SM)+$_smarty_tpl->tpl_vars['imageCache']->value->getGeneratedBySize(\JTL\Media\Image::SIZE_MD)+$_smarty_tpl->tpl_vars['imageCache']->value->getGeneratedBySize(\JTL\Media\Image::SIZE_LG)+$_smarty_tpl->tpl_vars['imageCache']->value->getGeneratedBySize(\JTL\Media\Image::SIZE_XL))/5),0);?>

                                </h3>
                                <span style="color:#c7c7c7"><?php echo __('imagesInCache');?>
</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-item">
            <div class="card">
                <div class="card-header">
                    <div class="subheading1"><?php echo __('subscription');?>
</div>
                    <hr class="mb-n3">
                </div>
                <div class="card-body">
                    <?php if ($_smarty_tpl->tpl_vars['sub']->value === null) {?>
                        <div class="alert alert-danger alert-sm">
                            <p><i class="fa fa-exclamation-circle"></i> <?php echo __('atmNoInfo');?>
</p>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col <?php if (intval($_smarty_tpl->tpl_vars['sub']->value->bUpdate) === 0) {?>col-md-3<?php }?> text-center">
                                <?php if (intval($_smarty_tpl->tpl_vars['sub']->value->bUpdate) === 0) {?>
                                    <i class="fal fa-check-circle text-four-times text-success"></i>
                                    <h3 style="margin-top:10px;margin-bottom:0"><?php echo __('valid');?>
</h3>
                                <?php } else { ?>
                                    <?php if ($_smarty_tpl->tpl_vars['sub']->value->nDayDiff <= 0) {?>
                                        <i class="fa fa-exclamation-circle text-four-times text-danger"></i>
                                        <h3 style="margin-top:10px;margin-bottom:0"><?php echo __('expired');?>
</h3>
                                    <?php } else { ?>
                                        <i class="fa fa-exclamation-circle text-four-times text-info"></i>
                                        <h3 style="margin-top:10px;margin-bottom:0"><?php ob_start();
echo __('expiresInXDays');
$_prefixVariable1 = ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['sub']->value->nDayDiff;
$_prefixVariable2 = ob_get_clean();
echo sprintf($_prefixVariable1,$_prefixVariable2);?>
</h3>
                                    <?php }?>
                                <?php }?>
                            </div>
                            <?php if (intval($_smarty_tpl->tpl_vars['sub']->value->bUpdate) === 0) {?>
                                <div class="col-md-9">
                                    <table class="table table-blank text-x1 last-child">
                                        <tbody>
                                            <tr>
                                                <td class="text-muted text-right"><strong><?php echo __('version');?>
</strong></td>
                                                <td><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['formatVersion'][0], array( array('value'=>$_smarty_tpl->tpl_vars['sub']->value->oShopversion->nVersion),$_smarty_tpl ) );?>
 <span class="label label-default"><?php echo $_smarty_tpl->tpl_vars['sub']->value->eTyp;?>
</span></td>
                                            </tr>
                                            <tr>
                                                <td class="text-muted text-right"><strong><?php echo __('domain');?>
</strong></td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['sub']->value->cDomain;?>
</td>
                                            </tr>
                                            <tr>
                                                <td class="text-muted text-right"><strong><?php echo __('validUntil');?>
</strong></td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['sub']->value->dDownloadBis_DE;?>
 <span class="text-muted">(<?php echo $_smarty_tpl->tpl_vars['sub']->value->nDayDiff;?>
 <?php echo __('days');?>
)</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php }?>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>

        <div class="grid-item">
            <div class="card">
                <div class="card-header">
                    <div class="subheading1"><?php echo __('extensions');?>
</div>
                    <hr class="mb-n3">
                </div>
                <div class="card-body">
                    <ul class="infolist list-group list-group-flush">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['status']->value->getExtensions(), 'extension', true);
$_smarty_tpl->tpl_vars['extension']->iteration = 0;
$_smarty_tpl->tpl_vars['extension']->index = -1;
$_smarty_tpl->tpl_vars['extension']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['extension']->value) {
$_smarty_tpl->tpl_vars['extension']->do_else = false;
$_smarty_tpl->tpl_vars['extension']->iteration++;
$_smarty_tpl->tpl_vars['extension']->index++;
$_smarty_tpl->tpl_vars['extension']->first = !$_smarty_tpl->tpl_vars['extension']->index;
$_smarty_tpl->tpl_vars['extension']->last = $_smarty_tpl->tpl_vars['extension']->iteration === $_smarty_tpl->tpl_vars['extension']->total;
$__foreach_extension_0_saved = $_smarty_tpl->tpl_vars['extension'];
?>
                            <li class="list-group-item <?php if ($_smarty_tpl->tpl_vars['extension']->first) {?>first<?php } elseif ($_smarty_tpl->tpl_vars['extension']->last) {?>last<?php }?>">
                                <p class="key">
                                    <?php if ($_smarty_tpl->tpl_vars['extension']->value->bActive) {?>
                                        <i class="fal fa-check-circle text-success fa-fw" aria-hidden="true"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-times-circle text-warning fa-fw" aria-hidden="true"></i>
                                    <?php }?>
                                    <span><?php echo $_smarty_tpl->tpl_vars['extension']->value->cName;?>
</span>
                                    <span class="float-right">
                                        <?php if ($_smarty_tpl->tpl_vars['extension']->value->bActive) {?>
                                            <span class="text-success"><?php echo __('active');?>
</span>
                                        <?php } else { ?>
                                           <a href="<?php echo $_smarty_tpl->tpl_vars['extension']->value->cURL;?>
" target="_blank" rel="noopener"><?php echo __('buyNow');?>
</a>
                                        <?php }?>
                                    </span>
                                </p>
                            </li>
                        <?php
$_smarty_tpl->tpl_vars['extension'] = $__foreach_extension_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </ul>
                </div>
            </div>
        </div>

        <?php $_smarty_tpl->_assignInScope('incorrectPaymentMethods', $_smarty_tpl->tpl_vars['status']->value->getPaymentMethodsWithError());?>
        <?php if (count($_smarty_tpl->tpl_vars['incorrectPaymentMethods']->value) > 0) {?>
            <div class="grid-item">
                <div class="card">
                    <div class="card-header">
                        <div class="subheading1"><?php echo __('paymentTypes');?>
</div>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-info">
                            <?php echo __('paymentTypesWithError');?>

                        </div>

                        <table class="table table-condensed table-striped table-blank last-child">
                            <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['incorrectPaymentMethods']->value, 's');
$_smarty_tpl->tpl_vars['s']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['s']->key => $_smarty_tpl->tpl_vars['s']->value) {
$_smarty_tpl->tpl_vars['s']->do_else = false;
$__foreach_s_1_saved = $_smarty_tpl->tpl_vars['s'];
?>
                                <tr class="text-vcenter">
                                    <td class="text-left" width="55">
                                        <h4 class="label-wrap"><span class="label label-danger" style="display:inline-block;width:3em"><?php echo $_smarty_tpl->tpl_vars['s']->value->logCount;?>
</span></h4>
                                    </td>
                                    <td class="text-muted"><strong><?php echo $_smarty_tpl->tpl_vars['s']->value->cName;?>
</strong></td>
                                    <td class="text-right">
                                        <a class="btn btn-default text-uppercase" href="zahlungsarten.php?a=log&kZahlungsart=<?php echo $_smarty_tpl->tpl_vars['s']->value->kZahlungsart;?>
"><?php echo __('details');?>
</a>
                                    </td>
                                </tr>
                            <?php
$_smarty_tpl->tpl_vars['s'] = $__foreach_s_1_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        <?php }?>

        <?php $_smarty_tpl->_assignInScope('shared', $_smarty_tpl->tpl_vars['status']->value->getPluginSharedHooks());?>
        <?php if (count($_smarty_tpl->tpl_vars['shared']->value) > 0) {?>
            <div class="grid-item">
                <div class="card">
                    <div class="card-header">
                        <div class="subheading1"><?php echo __('plugin');?>
</div>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-info">
                            <?php echo __('pluginsWithSameHook');?>

                        </div>

                        <table class="table table-condensed table-striped table-blank last-child">
                            <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shared']->value, 's');
$_smarty_tpl->tpl_vars['s']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['s']->key => $_smarty_tpl->tpl_vars['s']->value) {
$_smarty_tpl->tpl_vars['s']->do_else = false;
$__foreach_s_2_saved = $_smarty_tpl->tpl_vars['s'];
?>
                                <?php if (count($_smarty_tpl->tpl_vars['s']->value) > 1) {?>
                                    <tr>
                                        <td class="text-muted text-right" width="33%"><strong><?php echo $_smarty_tpl->tpl_vars['s']->key;?>
</strong></td>
                                        <td width="66%">
                                            <ul class="list-unstyled">
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['s']->value, 'p');
$_smarty_tpl->tpl_vars['p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->do_else = false;
?>
                                                    <li><?php echo $_smarty_tpl->tpl_vars['p']->value->cName;?>
</li>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </ul>
                                        </td>
                                    </tr>
                                <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['s'] = $__foreach_s_2_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        <?php }?>

        <div class="grid-item">
            <?php $_smarty_tpl->_assignInScope('tests', $_smarty_tpl->tpl_vars['status']->value->getEnvironmentTests());?>

            <div class="card">
                <div class="card-header">
                    <div class="heading-body">
                        <div class="subheading1"><?php echo __('server');?>
</div>
                    </div>
                    <div class="heading-right">
                        <a href="systemcheck.php" class="btn btn-primary text-uppercase"><?php echo __('details');?>
</a>
                    </div>
                    <hr class="mb-n3">
                </div>
                <div class="card-body">
                    <?php if (count($_smarty_tpl->tpl_vars['tests']->value['recommendations']) > 0) {?>
                        <table class="table table-condensed table-striped table-blank">
                            <thead>
                            <tr>
                                <th class="col-xs-7">&nbsp;</th>
                                <th class="col-xs-3 text-center"><?php echo __('recommendedValue');?>
</th>
                                <th class="col-xs-2 text-center"><?php echo __('yourSystem');?>
</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tests']->value['recommendations'], 'test');
$_smarty_tpl->tpl_vars['test']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['test']->value) {
$_smarty_tpl->tpl_vars['test']->do_else = false;
?>
                                <tr class="text-vcenter">
                                    <td>
                                        <div class="test-name">
                                            <?php if (call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'count_characters' ][ 0 ], array( $_smarty_tpl->tpl_vars['test']->value->getDescription() )) > 0) {?>
                                                <abbr title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['test']->value->getDescription(), ENT_QUOTES, 'utf-8', true);?>
"><?php echo $_smarty_tpl->tpl_vars['test']->value->getName();?>
</abbr>
                                            <?php } else { ?>
                                                <?php echo $_smarty_tpl->tpl_vars['test']->value->getName();?>

                                            <?php }?>
                                        </div>
                                    </td>
                                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['test']->value->getRequiredState();?>
</td>
                                    <td class="text-center"><?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'test_result', array('test'=>$_smarty_tpl->tpl_vars['test']->value), true);?>
</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="alert alert-success">
                            <?php echo __('requirementsMet');?>

                        </div>
                    <?php }?>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
/* smarty_template_function_render_item_21209931626155ec4b8da1c3_81144390 */
if (!function_exists('smarty_template_function_render_item_21209931626155ec4b8da1c3_81144390')) {
function smarty_template_function_render_item_21209931626155ec4b8da1c3_81144390(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('title'=>null,'desc'=>null,'val'=>null,'more'=>null), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

    <tr class="text-vcenter"<?php if ($_smarty_tpl->tpl_vars['more']->value) {?> data-href="<?php echo $_smarty_tpl->tpl_vars['more']->value;?>
"<?php }?>>
        <td <?php if (!$_smarty_tpl->tpl_vars['more']->value) {?>colspan="2"<?php }?>>
            <?php if ($_smarty_tpl->tpl_vars['val']->value) {?>
                <i class="fal fa-check-circle text-success fa-fw" aria-hidden="true"></i>
            <?php } else { ?>
                <i class="fa fa-exclamation-circle text-danger fa-fw" aria-hidden="true"></i>
            <?php }?>
            <span><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</span>
            <?php if ($_smarty_tpl->tpl_vars['desc']->value) {?><p class="text-muted"></p><?php }?>
        </td>
        <?php if ($_smarty_tpl->tpl_vars['more']->value) {?>
            <td class="text-right">
                <a href="<?php echo $_smarty_tpl->tpl_vars['more']->value;?>
" class="btn btn-default btn-sm text-uppercase"><?php echo __('details');?>
</a>
            </td>
        <?php }?>
    </tr>
<?php
}}
/*/ smarty_template_function_render_item_21209931626155ec4b8da1c3_81144390 */
}
