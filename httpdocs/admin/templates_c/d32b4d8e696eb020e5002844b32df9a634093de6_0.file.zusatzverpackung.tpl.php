<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:50:02
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/zusatzverpackung.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619deeaa94be64_43889447',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd32b4d8e696eb020e5002844b32df9a634093de6' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/zusatzverpackung.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/zusatzverpackung_bearbeiten.tpl' => 1,
    'file:tpl_inc/zusatzverpackung_uebersicht.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_619deeaa94be64_43889447 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('zusatzverpackung'),'cBeschreibung'=>__('zusatzverpackungDesc'),'cDokuURL'=>__('zusatzverpackungURL')), 0, false);
?>
<div id="content">
    <?php if ($_smarty_tpl->tpl_vars['action']->value === 'edit') {?>
        <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/zusatzverpackung_bearbeiten.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php } else { ?>
        <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/zusatzverpackung_uebersicht.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php }?>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
