<?php
/* Smarty version 3.1.39, created on 2021-10-06 10:20:17
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615d5c417da2d9_84941320',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '59e21967b3994459a629e0dffb6b7c8dd05e6be3' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/news.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/news_erstellen.tpl' => 1,
    'file:tpl_inc/news_kategorie_erstellen.tpl' => 1,
    'file:tpl_inc/news_uebersicht.tpl' => 1,
    'file:tpl_inc/news_vorschau.tpl' => 1,
    'file:tpl_inc/news_kommentar_editieren.tpl' => 1,
    'file:tpl_inc/news_kommentar_antwort_editieren.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_615d5c417da2d9_84941320 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_smarty_tpl->tpl_vars['step']->value === 'news_erstellen' || $_smarty_tpl->tpl_vars['step']->value === 'news_editieren') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/news_erstellen.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'news_kategorie_erstellen') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/news_kategorie_erstellen.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'news_uebersicht') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/news_uebersicht.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'news_vorschau') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/news_vorschau.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'news_kommentar_editieren') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/news_kommentar_editieren.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'news_kommentar_antwort_editieren') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/news_kommentar_antwort_editieren.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
$_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
