<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:08:45
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/widgetCampaigns.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e3ede8b464_13666925',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac7a6538ba4fee2396faadc0d9f2eaa70ee8390d' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/widgetCampaigns.tpl',
      1 => 1611753980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e3ede8b464_13666925 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="widget-custom-data">
    <?php echo '<script'; ?>
 type="text/javascript">
        
        $(function() {
            $('#select_kampagne').change(function() {
                var kKampagne = $('#select_kampagne option:selected').val();
                window.location = 'index.php?kKampagne=' + kKampagne;
            });
        });
        
    <?php echo '</script'; ?>
>
    <select name="kKampagne" id="select_kampagne" class="custom-select mb-3" >
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['campaigns']->value, 'campaign');
$_smarty_tpl->tpl_vars['campaign']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['campaign']->value) {
$_smarty_tpl->tpl_vars['campaign']->do_else = false;
?>
        <option value="<?php echo $_smarty_tpl->tpl_vars['campaign']->value->kKampagne;?>
" <?php if ($_smarty_tpl->tpl_vars['campaign']->value->kKampagne == $_smarty_tpl->tpl_vars['kKampagne']->value) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['campaign']->value->cName;?>
</option>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
    <table class="table">
        <thead>
            <tr>
            <th><?php echo __('Statistics');?>
</th>
            <th class="text-center"><?php echo $_smarty_tpl->tpl_vars['campaignStats']->value[$_smarty_tpl->tpl_vars['types']->value[0]]['cDatum'];?>
</th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['campaignDefinitions']->value, 'campaignDefinition');
$_smarty_tpl->tpl_vars['campaignDefinition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['campaignDefinition']->value) {
$_smarty_tpl->tpl_vars['campaignDefinition']->do_else = false;
?>
                <?php $_smarty_tpl->_assignInScope('kKampagneDef', $_smarty_tpl->tpl_vars['campaignDefinition']->value->kKampagneDef);?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['campaignDefinition']->value->cName;?>
</td>
                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['campaignStats']->value[$_smarty_tpl->tpl_vars['types']->value[0]][$_smarty_tpl->tpl_vars['kKampagneDef']->value];?>
</td>
                </tr>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
    </table>
</div>
<?php }
}
