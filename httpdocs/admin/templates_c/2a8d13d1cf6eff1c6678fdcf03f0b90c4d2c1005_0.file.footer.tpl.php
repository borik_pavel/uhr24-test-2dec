<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fdbeb47_04026576',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2a8d13d1cf6eff1c6678fdcf03f0b90c4d2c1005' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/footer.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fdbeb47_04026576 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
    $(document).ready(function () {
        knm_handle_endAdornments();
    });
<?php echo '</script'; ?>
>
<?php if (!empty($_smarty_tpl->tpl_vars['jsFilesFooter']->value)) {?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jsFilesFooter']->value, 'customJsFile');
$_smarty_tpl->tpl_vars['customJsFile']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['customJsFile']->value) {
$_smarty_tpl->tpl_vars['customJsFile']->do_else = false;
?>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['customJsFile']->value;?>
"><?php echo '</script'; ?>
>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>


    <?php echo '<script'; ?>
>
        function setLanguage(lang, prevLang) {
            if (lang !== prevLang) {
                let urlString = document.location.href;
                urlString = urlString.replace(`&lang=${prevLang}`, '');
                window.location.replace(`${urlString}&lang=${lang}`);
            }
        }
    <?php echo '</script'; ?>
>
<?php }
}
