<?php
/* Smarty version 3.1.39, created on 2021-10-07 13:32:13
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/navBar.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615edabdc8ac93_21532692',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '10157be747426700b1c64ce2b730e8d7ffa198aa' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/layout/navBar.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_615edabdc8ac93_21532692 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="bmd-layout-header fixed-top bg-theme-shade-1 bg-faded">
    <div class="bmd-layout-header--inner">
        <a class="navbar-brand" href="https://www.knowmates.de/shop/Plugins-fuer-JTL-Shop" target="_blank">
            <span>knowmates GmbH</span>
        </a>
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <div class="dark-design-toggle" onclick="toggleDarkDesign()">
                    <div class="checkbox checkbox-switch switch-primary">
                        <label>
                            <span class="pr-2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>"design",'section'=>"dashboard"),$_smarty_tpl ) );?>
:</span>
                            <span style="margin-right: 4px"><i class="fas fa-sun"></i></span>
                            <input id="design-swich" onchange="toggleDarkDesign()" type="checkbox"
                                   <?php if (!(isset($_COOKIE['knmDesign'])) || $_COOKIE['knmDesign'] == 'null' || $_COOKIE['knmDesign'] === 'dark') {?>checked=""<?php }?>>
                            <span></span>
                        </label>
                    </div>
                    <span><i class="fas fa-moon"></i></span>
                </div>
            </li>
        </ul>
    </div>
</div><?php }
}
