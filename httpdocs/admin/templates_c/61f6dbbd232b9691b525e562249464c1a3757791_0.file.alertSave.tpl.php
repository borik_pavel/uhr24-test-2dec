<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/alertSave.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fc55799_79289874',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '61f6dbbd232b9691b525e562249464c1a3757791' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/components/templates/alertSave.tpl',
      1 => 1623869889,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fc55799_79289874 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['params']->value['show']->getValue()) {?>
    <?php if ($_smarty_tpl->tpl_vars['params']->value['showSuccess']->getValue()) {?>
        <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['knm_alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['knm_alert'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'knm_alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('knm_alert', array('type'=>"success"));
$_block_repeat=true;
echo $_block_plugin12->render(array('type'=>"success"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>"settings saved",'section'=>"dashboard"),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin12->render(array('type'=>"success"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php } else { ?>
        <?php $_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['knm_alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['knm_alert'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'knm_alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('knm_alert', array('type'=>"danger"));
$_block_repeat=true;
echo $_block_plugin13->render(array('type'=>"danger"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['knmLang'][0], array( array('key'=>"settings not saved",'section'=>"dashboard"),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin13->render(array('type'=>"danger"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php }
}
}
}
