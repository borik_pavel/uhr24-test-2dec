<?php
/* Smarty version 3.1.39, created on 2021-10-01 16:24:47
  from '/var/www/vhosts/uhr24.de/httpdocs/plugins/ws5_doofinder/vendor/webstollen/jtl5-plugin/src/tpl/admin/root.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61571a2f9a1461_71148811',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac208e19f1742f44b09e9014d9b78e973b3f5088' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/plugins/ws5_doofinder/vendor/webstollen/jtl5-plugin/src/tpl/admin/root.tpl',
      1 => 1632584635,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61571a2f9a1461_71148811 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
    // <!--
    document.getElementById('content_wrapper').setAttribute('style', 'position:relative');
    const addCss = function (url) {
        const cssId = btoa(url).substr(btoa(url).length - 32, 32);  // you could encode the css path itself to generate id..
        if (!document.getElementById(cssId)) {
            const head = document.getElementsByTagName('head')[0];
            const link = document.createElement('link');
            link.id = cssId;
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = url;
            link.media = 'all';
            head.appendChild(link);
        }
    };
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['css']->value, 'file');
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
?>
    addCss('<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
');
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    // -->
<?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->tpl_vars['body']->value;
}
}
