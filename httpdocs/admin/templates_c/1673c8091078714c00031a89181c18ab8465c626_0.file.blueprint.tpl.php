<?php
/* Smarty version 3.1.39, created on 2021-11-04 19:45:31
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/opc/tpl/modals/blueprint.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61842a4bd2be41_47105078',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1673c8091078714c00031a89181c18ab8465c626' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/opc/tpl/modals/blueprint.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61842a4bd2be41_47105078 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="blueprintModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo __('Save this Portlet as a blueprint');?>
</h5>
            </div>
            <form action="javascript:void(0);" onsubmit="opc.gui.createBlueprint()">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="blueprintName"><?php echo __('Blueprint name');?>
</label>
                        <input type="text" class="form-control" id="blueprintName" name="blueprintName"
                               value="Neue Vorlage">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="opc-btn-secondary opc-small-btn" data-dismiss="modal">
                        <?php echo __('cancel');?>

                    </button>
                    <button type="submit" class="opc-btn-primary opc-small-btn" id="btnBlueprintSave">
                        <?php echo __('Save');?>

                    </button>
                </div>
            </form>
        </div>
    </div>
</div><?php }
}
