<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:30:13
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/zahlungsarten.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd0c514bd66_70551150',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '01df89a02acda6e154288dff92874f74e0d18648' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/zahlungsarten.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/zahlungsarten_uebersicht.tpl' => 1,
    'file:tpl_inc/zahlungsarten_einstellen.tpl' => 1,
    'file:tpl_inc/zahlungsarten_log.tpl' => 1,
    'file:tpl_inc/zahlungsarten_payments.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_619cd0c514bd66_70551150 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_smarty_tpl->tpl_vars['step']->value === 'uebersicht') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/zahlungsarten_uebersicht.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'einstellen') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/zahlungsarten_einstellen.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'log') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/zahlungsarten_log.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'payments') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/zahlungsarten_payments.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
$_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
