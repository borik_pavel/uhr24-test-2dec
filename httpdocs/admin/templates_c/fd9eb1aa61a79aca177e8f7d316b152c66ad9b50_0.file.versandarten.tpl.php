<?php
/* Smarty version 3.1.39, created on 2021-11-18 16:20:07
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/versandarten.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61966f27b07276_19836571',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fd9eb1aa61a79aca177e8f7d316b152c66ad9b50' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/versandarten.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/versandarten_uebersicht.tpl' => 1,
    'file:tpl_inc/versandarten_neue_Versandart.tpl' => 1,
    'file:tpl_inc/versandarten_zuschlagsliste.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_61966f27b07276_19836571 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_smarty_tpl->tpl_vars['step']->value === 'uebersicht') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/versandarten_uebersicht.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'neue Versandart') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/versandarten_neue_Versandart.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'Zuschlagsliste') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/versandarten_zuschlagsliste.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
$_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
