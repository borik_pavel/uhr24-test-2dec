<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/display_plugin_tabs.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16fb727d4_93148236',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2a30cfda117b993f9885d7ba0df815d70aedbd45' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/display_plugin_tabs.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16fb727d4_93148236 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('tabArrayLength', count($_smarty_tpl->tpl_vars['oPlugin']->value->getAdminMenu()->getItems()->all()));
if ($_smarty_tpl->tpl_vars['tabArrayLength']->value > 5 && $_smarty_tpl->tpl_vars['tabArrayLength']->value < 10) {?>
    <?php $_smarty_tpl->_assignInScope('breakerIndex', round((($_smarty_tpl->tpl_vars['tabArrayLength']->value-1)/2),0));
}?>
<div>
    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/tabBar/tabbarDefault.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/tabBar/tabarSm.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
    <div class="card-body tab-content" id="myTabContent">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oPlugin']->value->getAdminMenu()->getItems()->all(), 'oPluginAdminMenu', false, 'key', 'pluginadminmenutabs', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
$_smarty_tpl->tpl_vars['oPluginAdminMenu']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['oPluginAdminMenu']->value) {
$_smarty_tpl->tpl_vars['oPluginAdminMenu']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_pluginadminmenutabs']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_pluginadminmenutabs']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_pluginadminmenutabs']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_pluginadminmenutabs']->value['total'];
?>
            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/tabBar/tabContent.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('index'=>$_smarty_tpl->tpl_vars['oPluginAdminMenu']->value->nSort,'tabId'=>((string)$_smarty_tpl->tpl_vars['oPlugin']->value->getPluginID()).((string)$_smarty_tpl->tpl_vars['oPluginAdminMenu']->value->nSort),'tabName'=>$_smarty_tpl->tpl_vars['oPluginAdminMenu']->value->cName), 0, true);
?>
            <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_pluginadminmenutabs']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_pluginadminmenutabs']->value['last'] : null) == 1) {?>
                <?php if ($_smarty_tpl->tpl_vars['licenseType']->value != '(╯°□°）╯︵ ┻━┻)') {?>
                    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/tabBar/tabContent.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tabId'=>"lizenz_tab",'tabName'=>"Lizenz"), 0, true);
?>
                <?php }?>
                <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/tabBar/tabContent.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tabId'=>'about_tab','tabName'=>"About"), 0, true);
?>
            <?php }?>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    let page = 'pluginTabs';

    function updateURL(pluginMenuId) {
        if (history.pushState) {
            let newurl = '<?php echo $_smarty_tpl->tpl_vars['pluginTabBasepath']->value;?>
' + pluginMenuId + '';
            window.history.pushState({page, tabUrl: newurl}, null, newurl);
        }
    }

    function setTab(eleId, isMore = false) {
        updateURL(eleId);
        let slides = document.getElementsByClassName("tabItem");

        for (let i = 0; i < slides.length; i++) {
            let link = slides.item(i).children[0];
            if (link && slides.item(i).classList.contains(eleId + '-tab')) {
                link.classList.add('active', 'show');
            } else {
                link.classList.remove('active', 'show');
            }
        }
        slides = document.getElementById("myTabContent").children;
        for (let i = 0; i < slides.length; i++) {
            let link = slides.item(i);
            if (link && link.id === eleId) {
                link.classList.add('active', 'show');
            } else {
                link.classList.remove('active', 'show');
            }
        }
        if (isMore) {
            console.log("ismore true")
            $('.showMoreXsNav').addClass('active', 'show');
            $('.showMoreSmNav').addClass('active', 'show');
        } else {
            $('.showMoreXsNav').removeClass('active', 'show');
            $('.showMoreSmNav').removeClass('active', 'show');
        }
        knm_handle_endAdornments();
    }

    window.addEventListener("popstate", function () {
        // if the state is the expected page, pull the url and load it.
        if (history.state && history.state.page === page) {
            window.location = history.state.tabUrl;
        }
    });
<?php echo '</script'; ?>
>
<?php }
}
