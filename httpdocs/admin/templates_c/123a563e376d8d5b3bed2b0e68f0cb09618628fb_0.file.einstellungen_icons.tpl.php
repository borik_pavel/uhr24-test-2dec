<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:44
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/snippets/einstellungen_icons.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61652630ed4e70_21038944',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '123a563e376d8d5b3bed2b0e68f0cb09618628fb' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/snippets/einstellungen_icons.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/einstellungen_log_icon.tpl' => 1,
    'file:snippets/einstellungen_reset_icon.tpl' => 1,
  ),
),false)) {
function content_61652630ed4e70_21038944 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('wrapper', (($tmp = $_smarty_tpl->tpl_vars['wrapper']->value ?? null)===null||$tmp==='' ? true : $tmp));
if ($_smarty_tpl->tpl_vars['wrapper']->value) {?>
<div class="col-auto ml-sm-n4 order-2 order-sm-3 d-flex align-items-center">
<?php }?>
    <?php if (!empty($_smarty_tpl->tpl_vars['cnf']->value->cBeschreibung)) {?>
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getHelpDesc'][0], array( array('cDesc'=>$_smarty_tpl->tpl_vars['cnf']->value->cBeschreibung,'cID'=>$_smarty_tpl->tpl_vars['cnf']->value->kEinstellungenConf),$_smarty_tpl ) );?>

    <?php }?>
    <?php $_smarty_tpl->_subTemplateRender('file:snippets/einstellungen_log_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cnf'=>$_smarty_tpl->tpl_vars['cnf']->value), 0, false);
?>
    <?php $_smarty_tpl->_subTemplateRender('file:snippets/einstellungen_reset_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cnf'=>$_smarty_tpl->tpl_vars['cnf']->value), 0, false);
if ($_smarty_tpl->tpl_vars['wrapper']->value) {?>
</div>
<?php }
}
}
