<?php
/* Smarty version 3.1.39, created on 2021-11-02 21:37:03
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6181a16faf1ca3_04180180',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7db8307c1911368c4369c1f944714d61db908337' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/knm_dashboard_layout/tpl/index.tpl',
      1 => 1623869890,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6181a16faf1ca3_04180180 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/layout/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->_assignInScope('step', NULL);?>
<div class="knm_custom_dashboard" style="position:relative;">
    <div class="bmd-layout-container" style="min-height: 100%">
        <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/layout/navBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                <main class="bmd-layout-content">
            <div id="main_content" class="container">
                <div class="card">
                    <?php if ($_smarty_tpl->tpl_vars['oSubscription']->value) {?>
                        <?php if ((isset($_COOKIE['knmUpdateNotification'])) && $_COOKIE['knmUpdateNotification'] === 'hidden') {?>
                            <?php $_smarty_tpl->_assignInScope('notificationCookieIsHidden', true);?>
                        <?php } else { ?>
                            <?php $_smarty_tpl->_assignInScope('notificationCookieIsHidden', false);?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['bUpdateAvailable']->value && !$_smarty_tpl->tpl_vars['notificationCookieIsHidden']->value) {?>
                            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/updateShopMessage.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('version'=>$_smarty_tpl->tpl_vars['strLatestVersion']->value,'build'=>$_smarty_tpl->tpl_vars['strLatestBuild']->value), 0, true);
?>
                        <?php }?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['showLanguagePackages']->value) {?>
                        <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/languagePackages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('downloadableLanguages'=>$_smarty_tpl->tpl_vars['downloadableLanguages']->value), 0, true);
?>
                    <?php } else { ?>
                        <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/display_plugin_tabs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                    <?php }?>
                </div>
            </div>
        </main>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['knm_dashboard_tpl_path']->value)."/layout/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }
}
