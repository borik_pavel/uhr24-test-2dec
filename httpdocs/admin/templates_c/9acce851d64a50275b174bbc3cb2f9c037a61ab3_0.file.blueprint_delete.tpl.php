<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:20:57
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/opc/tpl/modals/blueprint_delete.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e6c9e035c8_85502861',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9acce851d64a50275b174bbc3cb2f9c037a61ab3' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/opc/tpl/modals/blueprint_delete.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e6c9e035c8_85502861 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="blueprintDeleteModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo __('Delete Blueprint?');?>
</h5>
            </div>
            <div class="modal-body">
                <p>"<span id="blueprintDeleteTitle">FOO</span>"</p>
                <p><?php echo __('templateDeleteSure');?>
</p>
            </div>
            <form action="javascript:void(0);" onsubmit="opc.gui.deleteBlueprint()">
                <div class="modal-footer">
                    <input type="hidden" id="blueprintDeleteId" name="id" value="">
                    <button type="button" class="opc-btn-secondary opc-small-btn" data-dismiss="modal">
                        <?php echo __('cancel');?>

                    </button>
                    <button type="submit" class="opc-btn-primary opc-small-btn">
                        <?php echo __('delete');?>

                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php }
}
