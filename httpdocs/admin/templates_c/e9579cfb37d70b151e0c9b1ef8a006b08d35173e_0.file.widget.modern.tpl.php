<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/kk_dropper/adminmenu/widget/widget.modern.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165261fb663f3_97891096',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e9579cfb37d70b151e0c9b1ef8a006b08d35173e' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/kk_dropper/adminmenu/widget/widget.modern.tpl',
      1 => 1632951797,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165261fb663f3_97891096 (Smarty_Internal_Template $_smarty_tpl) {
?><style>
.dropper-widget * {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.dropper-widget {
	margin: -1.5rem;
	margin-top: -2rem;
	padding: 3em;
	background: #e22672;
	text-align: center;
	border-radius: 0 0 6px 6px;
	position: relative;
	line-height: 0;
	overflow: hidden;
}

.dropper-widget .dropper-kk-logo {
	position: absolute;
	left: 0;
	right: -20px;
	top: 0;
	bottom: -20px;
	background: url(<?php echo $_smarty_tpl->tpl_vars['dropperWidget']->value['kk_logo'];?>
) no-repeat bottom right;
	background-size: 207px 335px;
}

.dropper-widget .dropper-logo {
	position: relative;
	max-width: 100%;
	width: 322px;
	height: 169px;
	vertical-align: middle;
	display: inline-block;
	background: url(<?php echo $_smarty_tpl->tpl_vars['dropperWidget']->value['dashboard_logo'];?>
) no-repeat center center;
	background-size: contain;
}
</style>

<div class="dropper-widget">
	<a class="dropper-link" href="plugin.php?kPlugin=<?php echo $_smarty_tpl->tpl_vars['dropperWidget']->value['plugin_id'];?>
">
		<div class="dropper-kk-logo"></div>
		<div class="dropper-logo"></div>
	</a>
</div><?php }
}
