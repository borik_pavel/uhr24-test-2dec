<?php
/* Smarty version 3.1.39, created on 2021-11-06 10:07:14
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/categorycheck.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_618645c28838b5_17067083',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3549f051e414ba6439ff410c9d163dcd661489cd' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/categorycheck.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_618645c28838b5_17067083 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('categorycheck'),'cBeschreibung'=>__('categorycheckDesc'),'cDokuURL'=>__('categorycheckURL')), 0, false);
?>
<div id="content">
    <div class="systemcheck">
        <?php if (!$_smarty_tpl->tpl_vars['passed']->value) {?>
            <div class="alert alert-warning">
                <?php echo __('errorCatsWithoutParents');?>

            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="col-xs-3 text-center"><?php echo __('id');?>
</th>
                            <th class="col-xs-9 text-center"><?php echo __('name');?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cateogries']->value, 'category');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>
                        <tr>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['category']->value->kKategorie;?>
</td>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['category']->value->cName;?>
</td>
                        </tr>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="alert alert-info"><?php echo __('infoNoOrphanedCats');?>
</div>
        <?php }?>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
