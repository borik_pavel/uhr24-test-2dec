<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:30:13
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/zahlungsarten_uebersicht.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd0c517e5e0_57605565',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '111885d57e29743342728fc5d22a30d220e75649' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/zahlungsarten_uebersicht.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/seite_header.tpl' => 1,
  ),
),false)) {
function content_619cd0c517e5e0_57605565 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('paymentmethods'),'cBeschreibung'=>__('installedPaymentmethods'),'cDokuURL'=>__('paymentmethodsURL')), 0, false);
?>
<div id="content" class="row mr-0">
    <div class="<?php if ($_smarty_tpl->tpl_vars['recommendations']->value->getRecommendations()->isNotEmpty()) {?>col-md-7<?php } else { ?>col-lg-9 col-xl-7<?php }?> pr-0 pr-md-4">
        <div class="card">
            <div class="card-body table-responsive">
                <table class="table table-content-center">
                    <thead>
                    <tr>
                        <th><?php echo __('installedPaymentTypes');?>
</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['zahlungsarten']->value, 'zahlungsart');
$_smarty_tpl->tpl_vars['zahlungsart']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['zahlungsart']->value) {
$_smarty_tpl->tpl_vars['zahlungsart']->do_else = false;
?>
                        <tr class="text-vcenter">
                            <td>
                                <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nActive == 1) {?>
                                    <span class="text-success" title="<?php echo __('active');?>
"><i class="fal fa-check text-success"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger" title="<?php echo __('inactive');?>
">
                                        <i class="fa fa-exclamation-triangle"></i>
                                    </span>
                                <?php }?>
                                <span class="ml-2"><?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->cName;?>

                                    <small><?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->cAnbieter;?>
</small>
                                </span>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a href="zahlungsarten.php?a=log&kZahlungsart=<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"
                                       class="btn btn-link sx-2 down
                                                  <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nLogCount > 0) {?>
                                                        <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nErrorLogCount) {?>text-danger<?php }?>
                                                  <?php } else { ?>
                                                        text-success disabled
                                                  <?php }?>"
                                       title="<?php echo __('viewLog');?>
"
                                       data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nLogCount > 0) {?>
                                                <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nErrorLogCount) {?>
                                                    <span class="fal fa-exclamation-triangle"></span>
                                                    <span class="fas fa-exclamation-triangle"></span>
                                                <?php } else { ?>
                                                    <span class="fal fa-bars"></span>
                                                    <span class="fas fa-bars"></span>
                                                <?php }?>
                                            <?php } else { ?>
                                                <span class="fal fa-check"></span>
                                                <span class="fas fa-check"></span>
                                            <?php }?>
                                        </span>
                                    </a>
                                    <a <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nEingangAnzahl > 0) {?>href="zahlungsarten.php?a=payments&kZahlungsart=<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"<?php }?>
                                       class="btn btn-link sx-2 <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->nEingangAnzahl === 0) {?>disabled<?php }?>"
                                       title="<?php echo __('paymentsReceived');?>
"
                                       data-toggle="tooltip">
                                        <span class="icon-hover">
                                            <span class="fal fa-hand-holding-usd"></span>
                                            <span class="fas fa-hand-holding-usd"></span>
                                        </span>
                                    </a>
                                    <?php if ($_smarty_tpl->tpl_vars['zahlungsart']->value->markedForDelete) {?>
                                        <a href="zahlungsarten.php?a=del&kZahlungsart=<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"
                                           class="btn btn-link sx-2"
                                           title="<?php echo __('delete');?>
"
                                           data-toggle="tooltip">
                                            <span class="icon-hover">
                                                <span class="fal fa-trash"></span>
                                                <span class="fas fa-trash"></span>
                                            </span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="zahlungsarten.php?kZahlungsart=<?php echo $_smarty_tpl->tpl_vars['zahlungsart']->value->kZahlungsart;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"
                                           class="btn btn-link sx-2"
                                           title="<?php echo __('edit');?>
"
                                           data-toggle="tooltip">
                                            <span class="icon-hover">
                                                <span class="fal fa-edit"></span>
                                                <span class="fas fa-edit"></span>
                                            </span>
                                        </a>
                                    <?php }?>
                                </div>
                            </td>
                        </tr>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer save-wrapper">
                <form method="post" action="zahlungsarten.php" class="top">
                    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                    <input type="hidden" name="checkNutzbar" value="1"/>
                    <div class="row">
                        <div class="ml-auto col-sm-6 col-xl-auto">
                            <button name="checkSubmit" type="submit" title="<?php echo __('paymentmethodsCheckAll');?>
" class="btn btn-outline-primary">
                                <i class="fa fa-refresh"></i> <?php echo __('paymentmethodsCheckAll');?>

                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['recommendations']->value->getRecommendations()->isNotEmpty()) {?>
    <div class="col-md-5 pr-0 pr-md-4">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th colspan="2"><?php echo __('weRecommend');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['recommendations']->value->getRecommendations(), 'recommendation');
$_smarty_tpl->tpl_vars['recommendation']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['recommendation']->value) {
$_smarty_tpl->tpl_vars['recommendation']->do_else = false;
?>
                                <tr>
                                    <td>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['recommendation']->value->getPreviewImage();?>
"
                                             style="max-width: 120px;"
                                             alt="<?php echo $_smarty_tpl->tpl_vars['recommendation']->value->getTitle();?>
" loading="lazy">
                                    </td>
                                    <td>
                                        <p><?php echo $_smarty_tpl->tpl_vars['recommendation']->value->getTeaser();?>
</p>
                                        <a href="premiumplugin.php?scope=<?php echo $_smarty_tpl->tpl_vars['recommendations']->value->getScope();?>
&id=<?php echo $_smarty_tpl->tpl_vars['recommendation']->value->getId();?>
"
                                           class="btn btn-primary">
                                            <?php echo __('getToKnowMore');?>

                                            <span class="fal fa-long-arrow-right ml-1"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-auto mx-auto">
                        <a href="<?php echo __('extensionStoreURL');?>
" class="btn btn-outline-primary my-3" target="_blank">
                            <i class="fas fa-puzzle-piece"></i>
                            <?php echo __('btnAdditionalExtensionStore');?>

                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
</div><?php }
}
