<?php
/* Smarty version 3.1.39, created on 2021-09-30 00:20:57
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/opc/tpl/modals/messagebox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6154e6c9e1c424_58648174',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '50ce19ec408b9bbecd48abee8dd0b320c95d0195' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/opc/tpl/modals/messagebox.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6154e6c9e1c424_58648174 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" tabindex="-1" id="messageboxModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" id="messageboxAlert"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="opc-btn-primary opc-small-btn" data-dismiss="modal">
                    <?php echo __('OK');?>

                </button>
            </div>
        </div>
    </div>
</div>
<?php }
}
