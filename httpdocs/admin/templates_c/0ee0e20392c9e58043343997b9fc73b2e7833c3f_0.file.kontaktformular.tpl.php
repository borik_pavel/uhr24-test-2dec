<?php
/* Smarty version 3.1.39, created on 2021-10-15 14:06:22
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/kontaktformular.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61696ebe6ae249_67231031',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0ee0e20392c9e58043343997b9fc73b2e7833c3f' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/kontaktformular.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/kontaktformular_uebersicht.tpl' => 1,
    'file:tpl_inc/kontaktformular_betreff.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_61696ebe6ae249_67231031 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_smarty_tpl->tpl_vars['step']->value === 'uebersicht') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/kontaktformular_uebersicht.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
} elseif ($_smarty_tpl->tpl_vars['step']->value === 'betreff') {?>
    <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/kontaktformular_betreff.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
$_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
