<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:25
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/widgets/help.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165261d346d39_49396964',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4b5587de07cfe443d0ee6b6ea6f2ffd322893b4' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/tpl_inc/widgets/help.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165261d346d39_49396964 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="widget-custom-data widget-help">

    <div class="row text-center">
        <div class="col-6 border-right">
            <a href="https://jtl-url.de/0762z" target="_blank" rel="noopener">
                <i class="fa fa-book text-four-times text-info"></i>
                <h4><?php echo __('docu');?>
</h4>
            </a>
        </div>
        <div class="col-6">
            <a href="https://forum.jtl-software.de" target="_blank" rel="noopener">
                <i class="far fa-comments text-four-times text-info"></i>
                <h4><?php echo __('communityForum');?>
</h4>
            </a>
        </div>
    </div>
</div>
<?php }
}
