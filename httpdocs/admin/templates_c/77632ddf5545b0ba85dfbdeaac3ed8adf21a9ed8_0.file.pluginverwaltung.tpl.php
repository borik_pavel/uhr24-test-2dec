<?php
/* Smarty version 3.1.39, created on 2021-09-30 08:21:44
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/pluginverwaltung.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_615557782d7c44_97175868',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77632ddf5545b0ba85dfbdeaac3ed8adf21a9ed8' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/pluginverwaltung.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/pluginverwaltung_uebersicht.tpl' => 1,
    'file:tpl_inc/pluginverwaltung_sprachvariablen.tpl' => 1,
    'file:tpl_inc/pluginverwaltung_lizenzkey.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_615557782d7c44_97175868 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_smarty_tpl->tpl_vars['pluginNotFound']->value === true) {?>
<div class="alert alert-danger"><?php echo __('pluginNotFound');?>
</div>
<?php } else { ?>
    <?php if ($_smarty_tpl->tpl_vars['step']->value === 'pluginverwaltung_uebersicht') {?>
        <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pluginverwaltung_uebersicht.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php } elseif ($_smarty_tpl->tpl_vars['step']->value === 'pluginverwaltung_sprachvariablen') {?>
        <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pluginverwaltung_sprachvariablen.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php } elseif ($_smarty_tpl->tpl_vars['step']->value === 'pluginverwaltung_lizenzkey') {?>
        <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/pluginverwaltung_lizenzkey.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php }
}
$_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
