<?php
/* Smarty version 3.1.39, created on 2021-09-30 18:07:50
  from '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/cron.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6155e0d61c1a73_90598750',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ab72304d29c19160a3ebceaa4af6c8c73debb90c' => 
    array (
      0 => '/var/www/vhosts/uhr24.de/httpdocs/admin/templates/bootstrap/cron.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/config_section.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_6155e0d61c1a73_90598750 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_smarty_tpl->tpl_vars['inserted']->value !== 0) {?>
    <div class="alert alert-info"><?php echo __('msgCreated');?>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['deleted']->value > 0) {?>
    <div class="alert alert-info"><?php echo __('msgDeleted');?>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['updated']->value > 0) {?>
    <div class="alert alert-info"><?php echo __('msgUpdated');?>
</div>
<?php }?>
<div class="tabs">
    <nav class="tabs-nav">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['tab']->value === 'overview') {?> active<?php }?>" data-toggle="tab" role="tab" href="#overview">
                    <?php echo __('queueEntries');?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['tab']->value === 'add-cron') {?> active<?php }?>" data-toggle="tab" role="tab" href="#add-cron">
                    <?php echo __('createQueueEntry');?>

                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if ($_smarty_tpl->tpl_vars['tab']->value === 'settings') {?> active<?php }?>" data-toggle="tab" role="tab" href="#config">
                    <?php echo __('settings');?>

                </a>
            </li>
        </ul>
    </nav>
    <div class="tab-content">
        <div id="overview" class="settings tab-pane fade<?php if ($_smarty_tpl->tpl_vars['tab']->value === 'overview') {?> active show<?php }?>">
            <?php if (count($_smarty_tpl->tpl_vars['jobs']->value) > 0) {?>
                <div>
                    <div class="subheading1"><?php echo __('queueEntries');?>
</div>
                    <hr class="mb-3">
                    <div class="table-responsive">
                        <form method="post">
                            <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?php echo __('headingType');?>
</th>
                                        <th class="text-center"><?php echo __('headingStartTime');?>
</th>
                                        <th class="text-center"><?php echo __('headingLastStarted');?>
</th>
                                        <th class="text-center"><?php echo __('headingFrequency');?>
</th>
                                        <th class="text-center"><?php echo __('headingRunning');?>
</th>
                                        <th class="text-center"><?php echo __('action');?>
</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['jobs']->value, 'job');
$_smarty_tpl->tpl_vars['job']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->do_else = false;
?>
                                        <tr>
                                            <td><?php echo __($_smarty_tpl->tpl_vars['job']->value->getType());
if ($_smarty_tpl->tpl_vars['job']->value->getName() !== null) {?> <?php echo $_smarty_tpl->tpl_vars['job']->value->getName();
}?></td>
                                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['job']->value->getStartTime()->format('H:i');?>
</td>
                                            <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['job']->value->getDateLastStarted() === null) {?>&dash;<?php } else {
echo $_smarty_tpl->tpl_vars['job']->value->getDateLastStarted()->format('d.m.Y H:i');
}?></td>
                                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['job']->value->getFrequency();?>
h</td>
                                            <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['job']->value->isRunning()) {?><i class="fal fa-check text-success"></i><?php } else { ?><i class="fal fa-times text-danger"></i><?php }?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                                                                            <button class="btn btn-link px-2" type="submit" name="reset" value="<?php echo $_smarty_tpl->tpl_vars['job']->value->getQueueID();?>
" title="<?php echo __('reset');?>
" data-toggle="tooltip">
                                                            <span class="icon-hover">
                                                                <span class="fal fa-refresh"></span>
                                                                <span class="fas fa-refresh"></span>
                                                            </span>
                                                        </button>
                                                                                                        <?php if ($_smarty_tpl->tpl_vars['job']->value->getType() !== \JTL\Cron\Type::LICENSE_CHECK) {?>
                                                        <button class="btn btn-link px-2 delete-confirm"
                                                                type="submit"
                                                                name="delete"
                                                                value="<?php echo $_smarty_tpl->tpl_vars['job']->value->getCronID();?>
"
                                                                title="<?php echo __('delete');?>
"
                                                                data-toggle="tooltip"
                                                                data-modal-body="<?php echo __($_smarty_tpl->tpl_vars['job']->value->getType());
if ($_smarty_tpl->tpl_vars['job']->value->getName() !== null) {?> <?php echo $_smarty_tpl->tpl_vars['job']->value->getName();
}?>">
                                                            <span class="icon-hover">
                                                                <span class="fal fa-trash-alt"></span>
                                                                <span class="fas fa-trash-alt"></span>
                                                            </span>
                                                        </button>
                                                    <?php } else { ?>
                                                        <span class="btn btn-link px-2">
                                                            <span class="icon-hover"></span>
                                                        </span>
                                                    <?php }?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            <?php } else { ?>
                <div class="alert alert-info" role="alert"><?php echo __('noDataAvailable');?>
</div>
            <?php }?>
        </div>

        <div id="add-cron" class="settings tab-pane fade<?php if ($_smarty_tpl->tpl_vars['tab']->value === 'add-cron') {?> active show<?php }?>">
            <div>
                <div class="subheading1"><?php echo __('createQueueEntry');?>
</div>
                <hr class="mb-3">
                <div>
                    <form method="post">
                        <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cron-type"><?php echo __('headingType');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <select name="type" class="custom-select" id="cron-type" required>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['available']->value, 'type');
$_smarty_tpl->tpl_vars['type']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->do_else = false;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
"><?php echo __($_smarty_tpl->tpl_vars['type']->value);?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cron-freq"><?php echo __('headingFrequency');?>
 (<?php echo __('hours');?>
):</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 config-type-number">
                                <div class="input-group form-counter">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-outline-secondary border-0" data-count-down>
                                            <span class="fas fa-minus"></span>
                                        </button>
                                    </div>
                                    <input id="cron-freq" type="number" min="1" value="24" name="frequency" class="form-control" required>
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-secondary border-0" data-count-up>
                                            <span class="fas fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cron-start"><?php echo __('headingStartTime');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input id="cron-start" type="time" name="time" value="00:00" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row align-items-center">
                            <label class="col col-sm-4 col-form-label text-sm-right" for="cron-start-date"><?php echo __('headingStartDate');?>
:</label>
                            <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                                <input id="cron-start-date" type="date" name="date" class="form-control" value="<?php echo smarty_modifier_date_format(time(),'%Y-%m-%d');?>
" required>
                            </div>
                        </div>
                        <div class="save-wrapper">
                            <div class="row">
                                <div class="ml-auto col-sm-6 col-xl-auto">
                                    <button type="submit" class="btn btn-primary btn-block" name="add-cron" value="1">
                                        <i class="fal fa-save"></i> <?php echo __('create');?>

                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="config" class="settings tab-pane fade<?php if ($_smarty_tpl->tpl_vars['tab']->value === 'settings') {?> active show<?php }?>">
            <?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/config_section.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('config'=>$_smarty_tpl->tpl_vars['oConfig_arr']->value,'name'=>'einstellen','a'=>'saveSettings','action'=>'cron.php','buttonCaption'=>__('saveWithIcon'),'tab'=>'einstellungen','title'=>__('settings')), 0, false);
?>
        </div>
    </div>
</div>

<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
