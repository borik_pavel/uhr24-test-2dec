<?php
/* Smarty version 3.1.39, created on 2021-11-04 16:22:37
  from '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/dbmanager.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6183fabd055b09_02262318',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates/bootstrap/dbmanager.tpl',
      1 => 1632904509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:tpl_inc/header.tpl' => 1,
    'file:tpl_inc/seite_header.tpl' => 1,
    'file:tpl_inc/footer.tpl' => 1,
  ),
),false)) {
function content_6183fabd055b09_02262318 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'table_scope_header' => 
  array (
    'compiled_filepath' => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates_c/c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff_0.file.dbmanager.tpl.php',
    'uid' => 'c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff',
    'call_name' => 'smarty_template_function_table_scope_header_1856074346183fabcea9648_74490896',
  ),
  'filter_operator' => 
  array (
    'compiled_filepath' => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates_c/c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff_0.file.dbmanager.tpl.php',
    'uid' => 'c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff',
    'call_name' => 'smarty_template_function_filter_operator_1856074346183fabcea9648_74490896',
  ),
  'filter_row' => 
  array (
    'compiled_filepath' => '/var/www/vhosts/test.uhr24.de/httpdocs/admin/templates_c/c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff_0.file.dbmanager.tpl.php',
    'uid' => 'c133f1e82cee6c1caef35dcda6f60b26b4c5a5ff',
    'call_name' => 'smarty_template_function_filter_row_1856074346183fabcea9648_74490896',
  ),
));
$_smarty_tpl->_subTemplateRender('file:tpl_inc/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
ob_start();
echo __('tableViews');
$_prefixVariable1=ob_get_clean();
ob_start();
echo count($_smarty_tpl->tpl_vars['tables']->value);
$_prefixVariable2=ob_get_clean();
$_smarty_tpl->_subTemplateRender('file:tpl_inc/seite_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cTitel'=>__('dbManager'),'cBeschreibung'=>"<kbd>".$_prefixVariable1."(".$_prefixVariable2.")</kbd>",'cDokuURL'=>__('dbcheckURL')), 0, false);
?>







<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'filter_row_tpl', 'filter_row_tpl_data', null);?>
    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'filter_row', array('headers'=>array_keys($_smarty_tpl->tpl_vars['columns']->value)), true);?>

<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>

<?php echo '<script'; ?>
>
var $add_row_tpl = $(<?php echo json_encode(preg_replace('!\s+!u', ' ',$_smarty_tpl->tpl_vars['filter_row_tpl_data']->value));?>
);
<?php if ((isset($_smarty_tpl->tpl_vars['info']->value)) && (isset($_smarty_tpl->tpl_vars['info']->value['statement']))) {?>var sql_query = <?php echo json_encode($_smarty_tpl->tpl_vars['info']->value['statement']);?>
;<?php }?>

$(function() {
    var new_content = '<form action="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?command" method="POST">';
    new_content += '<?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>
';
    $search = $('#db-search');

    $('table.table-sticky-header').stickyTableHeaders({
        fixedOffset: $('.navbar-header')
    });

    $search.keyup(function () {
        var val = $(this).val();
        var count = filter_tables(val);

        if (count > 0) {
            $search.parent().removeClass('has-error');
        }
        else {
            $search.parent().addClass('has-error');
        }
    });

    add_row_listener();
    var $pnate = $('#pagination');

    $pnate.bootpag({
        total: $pnate.data('total'),
        page: $pnate.data('current'),
        maxVisible: 10,
        leaps: true,
        firstLastUse: true,
        first: '&larr;',
        last: '&rarr;',
        wrapClass: 'pagination pagination-sm',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first',
    }).on('page', function(event, page){
        var p = get_params({ name: 'page', value: page });
        var url = location.pathname.split('/').slice(-1)[0];
        location.href = url + '?' + jQuery.param(p);
    });

    if ($('.query-code').length > 0) {
        highlight_sql($('.query-code'));
    }

    $(document).on('click', '.query .query-sub a', function () {
        if ($('#sql_query_edit').length) {
            return false;
        }

        var $inner_sql = $(this).parents('.query').find('code.sql');
        var old_text   = $inner_sql.html();

        var new_content = '<form action="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?command" method="POST">';
        new_content += '<?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>
';
        new_content += '<div class="form-group"><textarea name="sql_query_edit" id="sql_query_edit">' + sql_query + '</textarea></div>';
        new_content += '<div class="form-group btn-group-xs last-child">';
        new_content += '    <button type="submit" id="sql_query_edit_save" class="btn btn-primary">OK</button>';
        new_content += '    <button type="button" id="sql_query_edit_discard" class="btn btn-default">Abbrechen</button>';
        new_content += '</div>';

        new_content += '</form>';

        var $editor_area = $('div#inline_editor');
        if ($editor_area.length === 0) {
            $editor_area = $('<div id="inline_editor_outer"></div>');
            $editor_area.insertBefore($inner_sql);
        }

        $editor_area.html(new_content);
        $inner_sql.hide();

        bindCodeMirrorToInlineEditor();
        return false;
    });

    $(document).on('click', "#sql_query_edit_discard", function () {
        var $divEditor = $('div#inline_editor_outer');
        $divEditor.siblings('code.sql').show();
        $divEditor.remove();
    });
});

function get_params(p) {
    var params = $('#filter form').serializeArray();

    for (var i = 0; i < params.length; i++) {
        if (params[i].name === 'page') {
            delete params[i];
        }
    }

    params.push(p);
    return params;
}

function add_row_listener() {
    $(document).on('click', '*[data-action="add-row"] > a', function(e) {
        var $row = $(this).parent('.fieldset-row');
        var $body = $row.parent('.fieldset-body');

        if ($row.is('.fieldset-row:first')) {
            $add_row_tpl
                .clone()
                .appendTo($body);
        }
        else {
            $row.remove();
        }

        e.preventDefault();
        return false;
    });
}

function filter_tables(value) {
    var rex = new RegExp(value, 'i');
    var $nav = $('.db-sidenav');
    var $items = $nav.find('li');

    $items.hide();
    $nav.unhighlight();

    var $found = $items.filter(function () {
        return rex.test($(this).text());
    });

    $found.show();
    if ($found.length > 0) {
        $nav.highlight(value);
    }

    return $found.length;
}

/*****************************************************************************************************/

function highlight_sql($base) {
    var $elm = $base.find('code.sql');
    $elm.each(function () {
        var $sql = $(this),
            $div = $sql.find('div');
        if ($div.is(":visible")) {
            var $highlight = $('<div class="sql-highlight cm-s-default"></div>');
            $sql.append($highlight);
            if (typeof CodeMirror !== 'undefined') {
                CodeMirror.runMode($sql.text(), 'text/x-mysql', $highlight[0]);
                $div.hide();
            }
        }
    });
}

function get_sql_editor($textarea, options, resize, lintOptions) {
    if ($textarea.length > 0 && typeof CodeMirror !== 'undefined') {
        // merge options for CodeMirror
        var defaults = {
            lineNumbers: true,
            matchBrackets: true,
            extraKeys: { "Ctrl-Space": "autocomplete" },
            hintOptions: { "completeSingle": false, "completeOnSingleClick": true },
            indentUnit: 4,
            mode: "text/x-mysql",
            lineWrapping: true,
            scrollbarStyle: 'simple',
            smartIndent: true,
            autofocus: true
        };

        if (CodeMirror.sqlLint) {
            $.extend(defaults, {
                gutters: ["CodeMirror-lint-markers"],
                lint: {
                    "getAnnotations": CodeMirror.sqlLint,
                    "async": true,
                    "lintOptions": lintOptions
                }
            });
        }

        $.extend(true, defaults, options);

        // create CodeMirror editor
        var codemirrorEditor = CodeMirror.fromTextArea($textarea[0], defaults);
        codemirrorEditor.setCursor($textarea.val().length);

        // allow resizing
        if (! resize) {
            resize = 'vertical';
        }
        var handles = '';
        if (resize === 'vertical') {
            handles = 'n, s';
        }
        if (resize === 'both') {
            handles = 'all';
        }
        if (resize === 'horizontal') {
            handles = 'e, w';
        }
        $(codemirrorEditor.getWrapperElement())
            .css('resize', resize)
            .resizable({
                handles: handles,
                resize: function() {
                    codemirrorEditor.setSize($(this).width(), $(this).height());
                }
            });
        // enable autocomplete
        // codemirrorEditor.on("inputRead", codemirrorAutocompleteOnInputRead);

        return codemirrorEditor;
    }
    return null;
}

CodeMirror.runMode = function(string, modespec, callback, options) {
    var mode = CodeMirror.getMode(CodeMirror.defaults, modespec),
        ie = /MSIE \d/.test(navigator.userAgent),
        ie_lt9 = ie && (document.documentMode === null || document.documentMode < 9);

    if (callback.nodeType == 1) {
        var tabSize = (options && options.tabSize) || CodeMirror.defaults.tabSize,
            node = callback, col = 0;
        node.innerHTML = "";
        callback = function(text, style) {
            if (text == "\n") {
                node.appendChild(document.createTextNode(ie_lt9 ? '\r' : text));
                col = 0;
                return;
            }
            var content = "";
            for (var pos = 0;;) {
                var idx = text.indexOf("\t", pos);
                if (idx === -1) {
                    content += text.slice(pos);
                    col += text.length - pos;
                    break;
                } else {
                    col += idx - pos;
                    content += text.slice(pos, idx);
                    var size = tabSize - col % tabSize;
                    col += size;
                    for (var i = 0; i < size; ++i) content += " ";
                    pos = idx + 1;
                }
            }

            if (style) {
                var sp = node.appendChild(document.createElement("span"));
                sp.className = "cm-" + style.replace(/ +/g, " cm-");
                sp.appendChild(document.createTextNode(content));
            } else {
                node.appendChild(document.createTextNode(content));
            }
        };
    }

    var lines = CodeMirror.splitLines(string), state = (options && options.state) || CodeMirror.startState(mode);
    for (var i = 0, e = lines.length; i < e; ++i) {
        if (i) callback("\n");
        var stream = new CodeMirror.StringStream(lines[i]);
        if (!stream.string && mode.blankLine) mode.blankLine(state);
        while (!stream.eol()) {
            var style = mode.token(stream, state);
            callback(stream.current(), style, i, stream.start, state);
            stream.start = stream.pos;
        }
    }
};

function bindCodeMirrorToInlineEditor() {
    var $inline_editor = $('#sql_query_edit');
    if ($inline_editor.length > 0) {
        if (typeof CodeMirror !== 'undefined') {
            var height = $inline_editor.css('height');
            codemirror_inline_editor = get_sql_editor($inline_editor);
            codemirror_inline_editor.getWrapperElement().style.height = height;
            codemirror_inline_editor.refresh();
            codemirror_inline_editor.focus();
            $(codemirror_inline_editor.getWrapperElement())
                .bind('keydown', catchKeypressesFromSqlInlineEdit);
        } else {
            $inline_editor
                .focus()
                .bind('keydown', catchKeypressesFromSqlInlineEdit);
        }
    }
}

function catchKeypressesFromSqlInlineEdit(event) {
    // ctrl-enter is 10 in chrome and ie, but 13 in ff
    if (event.ctrlKey && (event.keyCode == 13 || event.keyCode == 10)) {
        $("#sql_query_edit_save").trigger('click');
    }
}

/*
$(function() {
    var offset = $('#paginator').offset();

    var paginator = $('#paginator input').bootstrapSlider({
        formatter: function(value) {
            return 'Seite: ' + value;
        }
    });

    paginator.on('slideStop', function(e) {
        $(paginator).bootstrapSlider('disable');
        var p = get_params({ name: 'page', value: e.value });
        var url = location.pathname.split('/').slice(-1)[0];
        location.href = url + '?' + jQuery.param(p);
    });

    $('#paginator')
        .css('left', offset.left)
        .addClass('paginator-bottom');

    //var slider = $('#paginator .slider');
    //slider.css('margin-left', (slider.width()/2) * -1);

    $(document).scroll(function() {
        var off = Math.max(0, offset.left - $(this).scrollLeft());
        $('#paginator')
            .css('left', off);
    });
});
*/
<?php echo '</script'; ?>
>

<div id="content">
    <div class="row">

        <div class="col-md-2">
            <div class="form-group">
                <input id="db-search" class="form-control" type="search" placeholder="<?php echo __('searchTable');?>
">
            </div>
            <nav class="db-sidebar hidden-print hidden-xs hidden-sm">
                <ul class="nav flex-column db-sidenav">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tables']->value, 'table');
$_smarty_tpl->tpl_vars['table']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['table']->key => $_smarty_tpl->tpl_vars['table']->value) {
$_smarty_tpl->tpl_vars['table']->do_else = false;
$__foreach_table_2_saved = $_smarty_tpl->tpl_vars['table'];
?>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?select=<?php echo $_smarty_tpl->tpl_vars['table']->key;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"><?php echo $_smarty_tpl->tpl_vars['table']->key;?>
</a></li>
                    <?php
$_smarty_tpl->tpl_vars['table'] = $__foreach_table_2_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
            </nav>
        </div>

        <div class="col-md-10">
            <ol class="simple-menu">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php"><?php echo __('overview');?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?token=<?php echo $_SESSION['jtl_token'];?>
&command"><span class="glyphicon glyphicon-flash"></span> <?php echo __('sqlCommand');?>
</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbcheck.php"><?php echo __('consistency');?>
</a></li>
            </ol>

            <?php if ($_smarty_tpl->tpl_vars['sub']->value === 'command') {?>
                <h2><?php echo __('sqlCommand');?>
</h2>

                <p class="text-muted">
                    <i class="fa fa-keyboard-o" aria-hidden="true"></i>
                    <?php echo __('codeCompletion');?>

                </p>

                <?php if ((isset($_smarty_tpl->tpl_vars['error']->value))) {?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo get_class($_smarty_tpl->tpl_vars['error']->value);?>
: <strong><?php echo $_smarty_tpl->tpl_vars['error']->value->getMessage();?>
</strong>
                    </div>
                <?php }?>

                <form action="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?command" method="POST">
                    <?php echo $_smarty_tpl->tpl_vars['jtl_token']->value;?>

                    <div class="form-group">
                        <textarea name="query" id="query" class="codemirror sql" data-hint='<?php echo json_encode($_smarty_tpl->tpl_vars['jsTypo']->value);?>
'><?php if ((isset($_smarty_tpl->tpl_vars['info']->value)) && (isset($_smarty_tpl->tpl_vars['info']->value['statement']))) {
echo $_smarty_tpl->tpl_vars['info']->value['statement'];
}?></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-share"></i> <?php echo __('execute');?>
</button>
                    </div>
                </form>

                <!-- ###################################################### -->
                <?php if ((isset($_smarty_tpl->tpl_vars['result']->value)) && !(isset($_smarty_tpl->tpl_vars['result']->value[0]))) {?>
                    <div class="alert alert-xs alert-success">
                        <p><?php echo __('noData');?>
</p>
                    </div>
                <?php } elseif ((isset($_smarty_tpl->tpl_vars['result']->value[0]))) {?>
                    <?php $_smarty_tpl->_assignInScope('headers', array_keys($_smarty_tpl->tpl_vars['result']->value[0]));?>
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered table-hover table-sql table-sticky-header nowrap">
                            <thead>
                                <tr>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['headers']->value, 'h');
$_smarty_tpl->tpl_vars['h']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['h']->value) {
$_smarty_tpl->tpl_vars['h']->do_else = false;
?>
                                        <th><?php echo $_smarty_tpl->tpl_vars['h']->value;?>
</th>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value, 'd');
$_smarty_tpl->tpl_vars['d']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['d']->value) {
$_smarty_tpl->tpl_vars['d']->do_else = false;
?>
                                <tr class="text-vcenter">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['headers']->value, 'h');
$_smarty_tpl->tpl_vars['h']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['h']->value) {
$_smarty_tpl->tpl_vars['h']->do_else = false;
?>
                                        <?php $_smarty_tpl->_assignInScope('value', call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( htmlspecialchars($_smarty_tpl->tpl_vars['d']->value[$_smarty_tpl->tpl_vars['h']->value], ENT_QUOTES, 'utf-8', true),100,'...' )));?>
                                        <td class="data data-mixed<?php if ($_smarty_tpl->tpl_vars['value']->value == null) {?> data-null<?php }?>"><span><?php if ($_smarty_tpl->tpl_vars['value']->value == null) {?>NULL<?php } else {
echo $_smarty_tpl->tpl_vars['value']->value;
}?></span></td>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </table>
                    </div>
                <?php }?>
                <!-- ###################################################### -->

            <?php } elseif ($_smarty_tpl->tpl_vars['sub']->value === 'default') {?>
                <?php if ((isset($_smarty_tpl->tpl_vars['tables']->value)) && count($_smarty_tpl->tpl_vars['tables']->value) > 0) {?>
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered table-hover table-sticky-header">
                            <thead>
                            <tr>
                                <th><?php echo __('table');?>
</th>
                                <th class="text-center"><?php echo __('action');?>
</th>
                                <th class="text-center"><?php echo __('type');?>
</th>
                                <th class="text-center"><?php echo __('collation');?>
</th>
                                <th class="text-right"><?php echo __('dataEntries');?>
</th>
                                <th class="text-right"><?php echo __('autoIncrement');?>
</th>
                            </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tables']->value, 'table');
$_smarty_tpl->tpl_vars['table']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['table']->key => $_smarty_tpl->tpl_vars['table']->value) {
$_smarty_tpl->tpl_vars['table']->do_else = false;
$__foreach_table_6_saved = $_smarty_tpl->tpl_vars['table'];
?>
                                <tr class="text-vcenter<?php if (count($_smarty_tpl->tpl_vars['definedTables']->value) > 0 && !(in_array($_smarty_tpl->tpl_vars['table']->key,$_smarty_tpl->tpl_vars['definedTables']->value) || substr($_smarty_tpl->tpl_vars['table']->key,0,8) === 'xplugin_')) {?> warning<?php }?>" id="table-<?php echo $_smarty_tpl->tpl_vars['table']->key;?>
">
                                    <td><a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?select=<?php echo $_smarty_tpl->tpl_vars['table']->key;?>
&token=<?php echo $_SESSION['jtl_token'];?>
"><?php echo $_smarty_tpl->tpl_vars['table']->key;?>
</a></td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?table=<?php echo $_smarty_tpl->tpl_vars['table']->key;?>
&token=<?php echo $_SESSION['jtl_token'];?>
" class="btn btn-default"><span class="glyphicon glyphicon-equalizer"></span> <?php echo __('structure');?>
</a>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?select=<?php echo $_smarty_tpl->tpl_vars['table']->key;?>
&token=<?php echo $_SESSION['jtl_token'];?>
" class="btn btn-default"><span class="glyphicon glyphicon-list"></span> <?php echo __('show');?>
</a>
                                        </div>
                                    </td>
                                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['table']->value->Engine;?>
</td>
                                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['table']->value->Collation;?>
</td>
                                    <td class="text-right"><?php echo number_format($_smarty_tpl->tpl_vars['table']->value->Rows);?>
</td>
                                    <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['table']->value->Auto_increment;?>
</td>
                                </tr>
                            <?php
$_smarty_tpl->tpl_vars['table'] = $__foreach_table_6_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </table>
                    </div>
                <?php }?>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub']->value === 'table') {?>
                <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'table_scope_header', array('table'=>$_smarty_tpl->tpl_vars['selectedTable']->value), true);?>

                <div class="row">
                    <div class="col-md-6">
                        <h3><?php echo __('structure');?>
</h3>
                        <table class="table table-striped table-condensed table-bordered table-hover table-sticky-header">
                            <thead>
                            <tr>
                                <th><?php echo __('column');?>
</th>
                                <th><?php echo __('type');?>
</th>
                                <th><?php echo __('collation');?>
</th>
                            </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['columns']->value, 'column');
$_smarty_tpl->tpl_vars['column']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['column']->value) {
$_smarty_tpl->tpl_vars['column']->do_else = false;
?>
                                <tr class="text-vcenter">
                                    <th><span class="text-vcenter"><?php echo $_smarty_tpl->tpl_vars['column']->value->Field;?>
</span> <?php if ($_smarty_tpl->tpl_vars['column']->value->Extra === 'auto_increment') {?><span class="label label-default text-vcenter"><abbr title="Auto-Inkrement">AI</abbr></span><?php }?></th>
                                    <td><?php echo $_smarty_tpl->tpl_vars['column']->value->Type;?>
 <?php if ($_smarty_tpl->tpl_vars['column']->value->Null === 'YES') {?><i class="text-danger">NULL</i><?php }?> <?php if ($_smarty_tpl->tpl_vars['column']->value->Default !== null) {?><strong class="text-muted">[<?php echo $_smarty_tpl->tpl_vars['column']->value->Default;?>
]</strong><?php }?></td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['column']->value->Collation;?>
</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h3><?php echo __('indices');?>
</h3>
                        <table class="table table-striped table-condensed table-bordered table-hover table-sticky-header">
                            <thead>
                            <tr>
                                <th><?php echo __('type');?>
</th>
                                <th><?php echo __('column');?>
</th>
                                <th><?php echo __('name');?>
</th>
                            </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['indexes']->value, 'index');
$_smarty_tpl->tpl_vars['index']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['index']->key => $_smarty_tpl->tpl_vars['index']->value) {
$_smarty_tpl->tpl_vars['index']->do_else = false;
$__foreach_index_8_saved = $_smarty_tpl->tpl_vars['index'];
?>
                                <tr class="text-vcenter">
                                    <th><?php echo $_smarty_tpl->tpl_vars['index']->value->Index_type;?>
</th>
                                    <td><?php echo implode('<strong>,</strong> ',array_keys($_smarty_tpl->tpl_vars['index']->value->Columns));?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['index']->key;?>
</td>
                                </tr>
                            <?php
$_smarty_tpl->tpl_vars['index'] = $__foreach_index_8_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </table>
                    </div>
                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub']->value === 'select') {?>
                <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'table_scope_header', array('table'=>$_smarty_tpl->tpl_vars['selectedTable']->value), true);?>

                <?php $_smarty_tpl->_assignInScope('headers', array_keys($_smarty_tpl->tpl_vars['columns']->value));?>
                <div id="filter">
                    <form method="GET" action="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php" data-sql=<?php echo json_encode($_smarty_tpl->tpl_vars['info']->value['statement']);?>
>
                        <input type="hidden" name="token" value="<?php echo $_SESSION['jtl_token'];?>
">
                        <input type="hidden" name="select" value="<?php echo $_smarty_tpl->tpl_vars['selectedTable']->value;?>
">

                        <fieldset>
                            <legend>
                                <a href="#filter-where"><?php echo __('search');?>
</a>
                            </legend>

                            <div class="fieldset-body">
                                <?php if ((isset($_smarty_tpl->tpl_vars['filter']->value['where']['col'])) && count($_smarty_tpl->tpl_vars['filter']->value['where']['col']) > 0) {?>
                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? count($_smarty_tpl->tpl_vars['filter']->value['where']['col'])-1+1 - (0) : 0-(count($_smarty_tpl->tpl_vars['filter']->value['where']['col'])-1)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration === 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration === $_smarty_tpl->tpl_vars['i']->total;?>
                                        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'filter_row', array('headers'=>$_smarty_tpl->tpl_vars['headers']->value,'col'=>$_smarty_tpl->tpl_vars['filter']->value['where']['col'][$_smarty_tpl->tpl_vars['i']->value],'op'=>$_smarty_tpl->tpl_vars['filter']->value['where']['op'][$_smarty_tpl->tpl_vars['i']->value],'val'=>$_smarty_tpl->tpl_vars['filter']->value['where']['val'][$_smarty_tpl->tpl_vars['i']->value],'remove'=>$_smarty_tpl->tpl_vars['i']->value), true);?>

                                    <?php }
}
?>
                                <?php } else { ?>
                                    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'filter_row', array('headers'=>$_smarty_tpl->tpl_vars['headers']->value,'remove'=>false), true);?>

                                <?php }?>
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('count');?>
</legend>
                            <div class="fieldset-body">
                                <input type="number" id="filter-limit" name="filter[limit]" class="form-control input-xs" placeholder="<?php echo __('count');?>
" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value['limit'];?>
" size="3">
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('action');?>
</legend>
                            <div class="fieldset-body">
                                <button type="submit" class="btn btn-sm btn-primary"><?php echo __('showData');?>
</button>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="query">
                    <div class="query-code">
                        <code class="sql"><div><?php echo $_smarty_tpl->tpl_vars['info']->value['statement'];?>
</div></code>
                    </div>
                    <div class="query-sub">
                        <span class="text-muted" title="Millisekunden"><i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp;<?php echo number_format(((string)($_smarty_tpl->tpl_vars['info']->value['time']*1000)),2);?>
 ms</span>
                        <span class="text-muted"><i class="fa fa-database" aria-hidden="true"></i> &nbsp;<?php echo number_format($_smarty_tpl->tpl_vars['count']->value,0);?>
 <?php echo __('dataEntries');?>
</span>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?command&query=<?php echo urlencode($_smarty_tpl->tpl_vars['info']->value['statement']);?>
&token=<?php echo $_SESSION['jtl_token'];?>
">
                            <i class="fa fa-pencil" aria-hidden="true"></i> <?php echo __('edit');?>

                        </a>
                    </div>
                </div>

                <?php if (count($_smarty_tpl->tpl_vars['data']->value) > 0) {?>
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-bordered table-hover table-sql table-sticky-header nowrap">
                            <thead>
                                <tr>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['headers']->value, 'h');
$_smarty_tpl->tpl_vars['h']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['h']->value) {
$_smarty_tpl->tpl_vars['h']->do_else = false;
?>
                                        <th><?php echo $_smarty_tpl->tpl_vars['h']->value;?>
</th>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'd');
$_smarty_tpl->tpl_vars['d']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['d']->value) {
$_smarty_tpl->tpl_vars['d']->do_else = false;
?>
                                <tr class="text-vcenter">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['headers']->value, 'h');
$_smarty_tpl->tpl_vars['h']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['h']->value) {
$_smarty_tpl->tpl_vars['h']->do_else = false;
?>
                                        <?php $_smarty_tpl->_assignInScope('value', $_smarty_tpl->tpl_vars['d']->value[$_smarty_tpl->tpl_vars['h']->value]);?>
                                        <?php $_smarty_tpl->_assignInScope('class', 'none');?>
                                        <?php $_smarty_tpl->_assignInScope('info', $_smarty_tpl->tpl_vars['columns']->value[$_smarty_tpl->tpl_vars['h']->value]->Type_info);?>

                                        <?php if (in_array($_smarty_tpl->tpl_vars['info']->value->Name,array('varchar','tinytext','text','mediumtext','longtext'))) {?>
                                            <?php $_smarty_tpl->_assignInScope('class', 'str');?>
                                            <?php $_smarty_tpl->_assignInScope('value', call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'utf-8', true),100,'...' )));?>
                                        <?php } elseif (in_array($_smarty_tpl->tpl_vars['info']->value->Name,array('float','decimal'))) {?>
                                            <?php $_smarty_tpl->_assignInScope('class', 'float');?>
                                            <?php if (is_array($_smarty_tpl->tpl_vars['info']->value->Size)) {?>
                                                <?php $_smarty_tpl->_assignInScope('decimals', (int)$_smarty_tpl->tpl_vars['info']->value->Size[1]);?>
                                                <?php $_smarty_tpl->_assignInScope('value', number_format($_smarty_tpl->tpl_vars['value']->value,$_smarty_tpl->tpl_vars['decimals']->value));?>
                                            <?php } else { ?>
                                                <?php $_smarty_tpl->_assignInScope('value', number_format($_smarty_tpl->tpl_vars['value']->value,4));?>
                                            <?php }?>
                                        <?php } elseif (in_array($_smarty_tpl->tpl_vars['info']->value->Name,array('double'))) {?>
                                            <?php $_smarty_tpl->_assignInScope('class', 'float');?>
                                            <?php $_smarty_tpl->_assignInScope('value', number_format($_smarty_tpl->tpl_vars['value']->value,4));?>
                                        <?php } elseif (in_array($_smarty_tpl->tpl_vars['info']->value->Name,array('tinyint','smallint','mediumint','int','bigint'))) {?>
                                            <?php $_smarty_tpl->_assignInScope('class', 'int');?>
                                        <?php } elseif (in_array($_smarty_tpl->tpl_vars['info']->value->Name,array('date','datetime','time','timestamp','year'))) {?>
                                            <?php $_smarty_tpl->_assignInScope('class', 'date');?>
                                        <?php } elseif (in_array($_smarty_tpl->tpl_vars['info']->value->Name,array('bit','char'))) {?>
                                            <?php $_smarty_tpl->_assignInScope('class', 'char');?>
                                        <?php }?>

                                        <td class="data data-<?php echo $_smarty_tpl->tpl_vars['class']->value;
if ($_smarty_tpl->tpl_vars['value']->value == null) {?> data-null<?php }?>"><span><?php if ($_smarty_tpl->tpl_vars['value']->value == null) {?>NULL<?php } else {
echo $_smarty_tpl->tpl_vars['value']->value;
}?></span></td>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </table>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-xs alert-success">
                        <p><?php echo __('noData');?>
</p>
                    </div>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['pages']->value > 1) {?>
                    <div id="pagination" class="pagination-static" data-total="<?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
" data-current="<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
"></div>
                <?php }?>

                            <?php }?>
        </div>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:tpl_inc/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
/* smarty_template_function_table_scope_header_1856074346183fabcea9648_74490896 */
if (!function_exists('smarty_template_function_table_scope_header_1856074346183fabcea9648_74490896')) {
function smarty_template_function_table_scope_header_1856074346183fabcea9648_74490896(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('table'=>null), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

    <h2><?php echo __('table');?>
: <?php echo $_smarty_tpl->tpl_vars['table']->value;?>

        <div class="btn-group btn-group-xs" role="group">
            <a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?table=<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&token=<?php echo $_SESSION['jtl_token'];?>
" class="btn btn-default"><span class="glyphicon glyphicon-equalizer"></span> <?php echo __('structure');?>
</a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['adminURL']->value;?>
/dbmanager.php?select=<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
&token=<?php echo $_SESSION['jtl_token'];?>
" class="btn btn-default"><span class="glyphicon glyphicon-list"></span> <?php echo __('show');?>
</a>
        </div>
    </h2>
<?php
}}
/*/ smarty_template_function_table_scope_header_1856074346183fabcea9648_74490896 */
/* smarty_template_function_filter_operator_1856074346183fabcea9648_74490896 */
if (!function_exists('smarty_template_function_filter_operator_1856074346183fabcea9648_74490896')) {
function smarty_template_function_filter_operator_1856074346183fabcea9648_74490896(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('selected'=>null), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

    <?php $_smarty_tpl->_assignInScope('ol', array('=','<','>','<=','>=','!=','LIKE','LIKE %%','REGEXP','IN','IS NULL','NOT LIKE','NOT REGEXP','NOT IN','IS NOT NULL','SQL'));?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ol']->value, 'o');
$_smarty_tpl->tpl_vars['o']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['o']->value) {
$_smarty_tpl->tpl_vars['o']->do_else = false;
?>
        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value, ENT_QUOTES, 'utf-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['selected']->value && $_smarty_tpl->tpl_vars['selected']->value == $_smarty_tpl->tpl_vars['o']->value) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value, ENT_QUOTES, 'utf-8', true);?>
</option>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}}
/*/ smarty_template_function_filter_operator_1856074346183fabcea9648_74490896 */
/* smarty_template_function_filter_row_1856074346183fabcea9648_74490896 */
if (!function_exists('smarty_template_function_filter_row_1856074346183fabcea9648_74490896')) {
function smarty_template_function_filter_row_1856074346183fabcea9648_74490896(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('headers'=>array(),'col'=>null,'op'=>null,'val'=>null,'remove'=>true), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

    <div class="fieldset-row" data-action="add-row">
        <select name="filter[where][col][]" class="custom-select input-xs">
            <option value="">(<?php echo __('any');?>
)</option>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['headers']->value, 'h');
$_smarty_tpl->tpl_vars['h']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['h']->value) {
$_smarty_tpl->tpl_vars['h']->do_else = false;
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['h']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['col']->value == $_smarty_tpl->tpl_vars['h']->value) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['h']->value;?>
</option>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </select>
        <select name="filter[where][op][]" class="custom-select input-xs">
            <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'filter_operator', array('selected'=>$_smarty_tpl->tpl_vars['op']->value), true);?>

        </select>
        <input type="text" name="filter[where][val][]" class="form-control input-xs" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['val']->value, ENT_QUOTES, 'utf-8', true);?>
">
        <a href="#" data-dismiss="row">
            <i class="fa <?php if ($_smarty_tpl->tpl_vars['remove']->value) {?>fa-trash text-danger<?php } else { ?>fa-plus text-success<?php }?>" aria-hidden="true"></i>
        </a>
    </div>
<?php
}}
/*/ smarty_template_function_filter_row_1856074346183fabcea9648_74490896 */
}
