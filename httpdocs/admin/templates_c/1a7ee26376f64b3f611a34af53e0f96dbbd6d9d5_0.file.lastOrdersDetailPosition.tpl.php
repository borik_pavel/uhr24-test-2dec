<?php
/* Smarty version 3.1.39, created on 2021-10-12 08:07:27
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/lastOrdersDetailPosition.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6165261fb54584_11895998',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1a7ee26376f64b3f611a34af53e0f96dbbd6d9d5' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_widgets/adminmenu/widget/templates/lastOrdersDetailPosition.tpl',
      1 => 1611753980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6165261fb54584_11895998 (Smarty_Internal_Template $_smarty_tpl) {
?><tr>
    <td>
        <?php if ($_smarty_tpl->tpl_vars['Position']->value->nPosTyp == (defined('C_WARENKORBPOS_TYP_ARTIKEL') ? constant('C_WARENKORBPOS_TYP_ARTIKEL') : null)) {?>
            <a class="text-decoration-underline" href="<?php echo $_smarty_tpl->tpl_vars['shopURL']->value;?>
/index.php?a=<?php echo $_smarty_tpl->tpl_vars['Position']->value->kArtikel;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
</a>
            <?php if (strlen($_smarty_tpl->tpl_vars['Position']->value->cUnique) > 0 && $_smarty_tpl->tpl_vars['Position']->value->kKonfigitem == 0) {?>
                <table class="ml-3 table">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Positionen, 'KonfigPos');
$_smarty_tpl->tpl_vars['KonfigPos']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['KonfigPos']->value) {
$_smarty_tpl->tpl_vars['KonfigPos']->do_else = false;
?>
                        <?php if ($_smarty_tpl->tpl_vars['Position']->value->cUnique == $_smarty_tpl->tpl_vars['KonfigPos']->value->cUnique) {?>
                            <tr>
                                <td>
                                    <?php if (!(strlen($_smarty_tpl->tpl_vars['KonfigPos']->value->cUnique) > 0 && $_smarty_tpl->tpl_vars['KonfigPos']->value->kKonfigitem == 0)) {
echo $_smarty_tpl->tpl_vars['KonfigPos']->value->nAnzahlEinzel;?>
x <?php }
echo $_smarty_tpl->tpl_vars['KonfigPos']->value->cName;?>

                                </td>
                                <td>
                                    <span class="price"><?php echo $_smarty_tpl->tpl_vars['KonfigPos']->value->cEinzelpreisLocalized[1];?>
</span>
                                </td>
                            </tr>
                        <?php }?>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </table>
            <?php }?>
            <?php if (count($_smarty_tpl->tpl_vars['Position']->value->WarenkorbPosEigenschaftArr) > 0) {?>
                <table class="mt-3 ml-4 table">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Position']->value->WarenkorbPosEigenschaftArr, 'WKPosEigenschaft');
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value) {
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = false;
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftName;?>
: <?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftWertName;?>
</td>
                        <td>
                            <?php if ($_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->fAufpreis) {?>
                                <?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cAufpreisLocalized[1];?>

                            <?php }?>
                        </td>
                    </tr>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </table>
            <?php }?>
        <?php } else { ?>
            <?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>

            <?php if (strlen($_smarty_tpl->tpl_vars['Position']->value->cHinweis) > 0) {?>
                <p>
                    <small><?php echo $_smarty_tpl->tpl_vars['Position']->value->cHinweis;?>
</small>
                </p>
            <?php }?>
        <?php }?>
    </td>
    <td>
        <?php if (strlen($_smarty_tpl->tpl_vars['Position']->value->cUnique) > 0 && $_smarty_tpl->tpl_vars['Position']->value->kKonfigitem == 0) {?>
            <?php echo $_smarty_tpl->tpl_vars['Position']->value->cKonfigeinzelpreisLocalized[1];?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->tpl_vars['Position']->value->cEinzelpreisLocalized[1];?>

        <?php }?>
    </td>
</tr>
<?php }
}
