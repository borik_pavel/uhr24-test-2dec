var doofinder_script ='//cdn.doofinder.com/media/js/doofinder-classic.7.latest.min.js';
(function(d,t){var f=d.createElement(t),s=d.getElementsByTagName(t)[0];f.async=1;
f.src=('https:'==location.protocol?'https:':'http:')+doofinder_script;
f.setAttribute('charset','utf-8');
s.parentNode.insertBefore(f,s)}(document,'script'));

var dfClassicLayers = [{
  "hashid": "cf907f0add0c108325631425da1db1b3",
  "zone": "us1",
  "display": {
    "lang": "de",
    "width": "70%",
    "facets": {
      "width": "200px",
      "attached": "right"
    },
"results": {
      "template": document.getElementById('df-results-template').innerHTML
    }
  },
  "queryInput": "#search-header, #search-header-mobile-top"
}];