<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:30:39
  from 'db:text41' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619dea1f47ef60_47995794',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '44f2a3842cc218eebe520f8f098ebfd4040de18f' => 
    array (
      0 => 'db:text41',
      1 => 1637739039,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619dea1f47ef60_47995794 (Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'plain'),$_smarty_tpl ) );?>


Sie haben Ihre Zugangsdaten zu unserem Shop vergessen oder verlegt?
Kein Problem! Klicken Sie bitte den folgenden Link, um Ihr Passwort
zurückzusetzen.

<?php echo $_smarty_tpl->tpl_vars['passwordResetLink']->value;?>


Wir empfehlen Ihnen, Ihr Passwort in regelmäßigen Abständen zu ändern,
um möglichem Mißbrauch vorzubeugen

<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'plain'),$_smarty_tpl ) );
}
}
