<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:57:13
  from 'db:html31' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619df05944d9a8_93179751',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ee9d73a5350819868f6c6fc6f9b734e92f72c0a4' => 
    array (
      0 => 'db:html31',
      1 => 1637740633,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619df05944d9a8_93179751 (Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'html'),$_smarty_tpl ) );?>


Guten Tag <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
,
<br>
vielen Dank für Ihre Bestellung bei <?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_shopname'];?>
.<br>
<br>
<?php if (count($_smarty_tpl->tpl_vars['Verfuegbarkeit_arr']->value['cArtikelName_arr']) > 0) {
echo $_smarty_tpl->tpl_vars['Verfuegbarkeit_arr']->value['cHinweis'];?>

<table cellpadding="0" cellspacing="0" border="0">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Verfuegbarkeit_arr']->value['cArtikelName_arr'], 'cArtikelname');
$_smarty_tpl->tpl_vars['cArtikelname']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cArtikelname']->value) {
$_smarty_tpl->tpl_vars['cArtikelname']->do_else = false;
?>
    <tr>
        <td width="18">•</td>
        <td align="left" valign="middle"><?php echo $_smarty_tpl->tpl_vars['cArtikelname']->value;?>
</td>
    </tr>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</table><br>
<?php }?>
Ihre Bestellung mit Bestellnummer <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>
 umfasst folgende Positionen:<br>
<br>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Positionen, 'Position');
$_smarty_tpl->tpl_vars['Position']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Position']->value) {
$_smarty_tpl->tpl_vars['Position']->do_else = false;
?>
    <table cellpadding="10" cellspacing="0" border="0" width="100%" style="border-bottom: 1px dotted #929292;">
        <tr>
            <td class="column" <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_einzelpreise_anzeigen'] === 'Y') {?>width="50%"<?php } else { ?>width="70%"<?php }?> align="left" valign="top">
                <?php if ($_smarty_tpl->tpl_vars['Position']->value->nPosTyp == 1) {?>
                    <?php if (!empty($_smarty_tpl->tpl_vars['Position']->value->kKonfigitem)) {?> * <?php }?><strong><?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
</strong> <?php if ($_smarty_tpl->tpl_vars['Position']->value->cArtNr) {?>(<?php echo $_smarty_tpl->tpl_vars['Position']->value->cArtNr;?>
)<?php }?>
                    <?php if ((isset($_smarty_tpl->tpl_vars['Position']->value->Artikel->nErscheinendesProdukt)) && $_smarty_tpl->tpl_vars['Position']->value->Artikel->nErscheinendesProdukt) {?>
                        <br>Verfügbar ab: <strong><?php echo $_smarty_tpl->tpl_vars['Position']->value->Artikel->Erscheinungsdatum_de;?>
</strong>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_lieferstatus_anzeigen'] === 'Y' && $_smarty_tpl->tpl_vars['Position']->value->cLieferstatus) {?>
                        <br><small>Lieferzeit: <?php echo $_smarty_tpl->tpl_vars['Position']->value->cLieferstatus;?>
</small>
                    <?php }?><br>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Position']->value->WarenkorbPosEigenschaftArr, 'WKPosEigenschaft');
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value) {
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = false;
?>
                        <br><strong><?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftName;?>
</strong>: <?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftWertName;?>

                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <?php } else { ?>
                    <strong><?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
</strong>
                <?php }?>
            </td>
            <td class="column" width="10%" align="left" valign="top">
                <strong class="mobile-only">Anzahl:</strong> <?php echo $_smarty_tpl->tpl_vars['Position']->value->nAnzahl;?>

            </td>
            <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_einzelpreise_anzeigen'] === 'Y' && (isset($_smarty_tpl->tpl_vars['Position']->value->cEinzelpreisLocalized))) {?>
                <td class="column" width="20%" align="right" valign="top">
                    <?php echo $_smarty_tpl->tpl_vars['Position']->value->cEinzelpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

                </td>
            <?php }?>
            <td class="column" width="20%" align="right" valign="top">
                <?php echo $_smarty_tpl->tpl_vars['Position']->value->cGesamtpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

            </td>
        </tr>
    </table>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
<table cellpadding="10" cellspacing="0" border="0" width="100%" style="border-bottom: 1px dotted #929292;">
    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_steuerpos_anzeigen'] !== 'N') {?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Steuerpositionen, 'Steuerposition');
$_smarty_tpl->tpl_vars['Steuerposition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Steuerposition']->value) {
$_smarty_tpl->tpl_vars['Steuerposition']->do_else = false;
?>
            <tr>
                <td align="right" valign="top">
                    <?php echo $_smarty_tpl->tpl_vars['Steuerposition']->value->cName;?>
:
                </td>
                <td width="90" align="right" valign="top">
                    <?php echo $_smarty_tpl->tpl_vars['Steuerposition']->value->cPreisLocalized;?>

                </td>
            </tr>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    <?php }?>
    <?php if ((isset($_smarty_tpl->tpl_vars['Bestellung']->value->GuthabenNutzen)) && $_smarty_tpl->tpl_vars['Bestellung']->value->GuthabenNutzen == 1) {?>
        <tr>
            <td align="right" valign="top">
                Gutschein:
            </td>
            <td width="90" align="right" valign="top">
                <strong>-<?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->GutscheinLocalized;?>
</strong>
            </td>
        </tr>
    <?php }?>
    <tr>
        <td align="right" valign="top">
            <strong>Gesamtsumme:</strong>
        </td>
        <td width="90" align="right" valign="top">
            <strong><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>
</strong>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-bottom: 1px dotted #929292;">
    <tr>
        <td class="column mobile-left" width="50%" align="left" valign="top">
            <strong>Lieferzeit:</strong>
        </td>
        <td class="column mobile-left" width="50%" align="right" valign="top">
            <?php if ((isset($_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDeliveryEx))) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDeliveryEx;
} else {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDelivery;
}?>
          </td>
    </tr>
</table><br>
<strong>Für Expresssendungen gilt: </strong>Sofort lieferbare Artikel werden am nächsten Arbeitstag Mo - Fr zugestellt, bei Bestellung an einem Arbeitstag Mo - Fr -13:00, Zahlungseingang vorausgesetzt. <br><br>
<strong>Für Samstag Expresssendungen gilt:</strong> Sofort lieferbare Artikel werden am Samstag zugestellt, bei Bestellung Donnerstags nach 13:00 - Freitag 13:00. <br><br>
<strong>Ihre Rechnungsadresse:</strong>
<br>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-bottom: 1px dotted #929292;">
    <tr>
        <td class="column mobile-left" width="25%" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <strong>Anschrift:</strong>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="column" width="80%" align="left" valign="top" bgcolor="#ffffff">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <?php if (!empty($_smarty_tpl->tpl_vars['Kunde']->value->cFirma)) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cFirma;?>
 - <?php if (!empty($_smarty_tpl->tpl_vars['Kunde']->value->cZusatz)) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cZusatz;
}?><br><?php }?>
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
<br>
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>
<br>
                            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz;?>
<br><?php }?>
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>
<br>
                            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cBundesland) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cBundesland;?>
<br><?php }?>
                            <font style="text-transform: uppercase;"><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;?>
</font>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cTel) {?>
    <tr>
        <td class="column mobile-left" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <strong>Telefon:</strong>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="column" align="left" valign="top" bgcolor="#ffffff">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTel;?>

                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cMobil) {?>
    <tr>
        <td class="column mobile-left" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <strong>Mobil:</strong>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="column" align="left" valign="top" bgcolor="#ffffff">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMobil;?>

                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cFax) {?>
    <tr>
        <td class="column mobile-left" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <strong>Fax:</strong>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="column" align="left" valign="top" bgcolor="#ffffff">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFax;?>

                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php }?>
    <tr>
        <td class="column mobile-left" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <strong>E-Mail:</strong>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="column" align="left" valign="top" bgcolor="#ffffff">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMail;?>

                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cUSTID) {?>
    <tr>
        <td class="column mobile-left" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <strong>Ust-ID:</strong>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="column" align="left" valign="top" bgcolor="#ffffff">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td>
                        <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                            <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cUSTID;?>

                        </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php }?>
    <tr>
        <td colspan="2" class="column" align="right" valign="top">
            <table cellpadding="0" cellspacing="6">
                <tr>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
</table><br>
<?php if (!empty($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->kLieferadresse)) {?>
    <strong>Ihre Lieferadresse:</strong><br>
    <br>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-bottom: 1px dotted #929292;">
        <tr>
            <td class="column mobile-left" width="25%" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Anschrift:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" width="80%" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php if (!empty($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFirma)) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFirma;?>
<br><?php }?>
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cNachname;?>
<br>
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cHausnummer;?>
<br>
                                <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cAdressZusatz;?>
<br><?php }?>
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cOrt;?>
<br>
                                <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cBundesland) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cBundesland;?>
<br><?php }?>
                                <font style="text-transform: uppercase;"><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cLand;?>
</font>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cTel) {?>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Telefon:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cTel;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMobil) {?>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Mobil:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMobil;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFax) {?>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Fax:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFax;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMail) {?>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>E-Mail:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMail;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php }?>
        <tr>
            <td colspan="2" class="column" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table><br>
<?php }?>
Sie haben folgende Zahlungsart gewählt: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cZahlungsartName;?>
<br>
<br>
<?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_ueberweisung_jtl') {?>
    <strong>Bitte führen Sie die folgende Überweisung durch:</strong><br>
    <br>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-bottom: 1px dotted #929292;">
        <tr>
            <td class="column mobile-left" width="20%" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Kontoinhaber:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" width="80%" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cKontoinhaber;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Bankinstitut:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cBank;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>IBAN:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cIBAN;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>BIC:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cBIC;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Verwendungszweck:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>

                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="column mobile-left" align="right" valign="top">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong>Gesamtsumme:</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="column" align="left" valign="top" bgcolor="#ffffff">
                <table cellpadding="0" cellspacing="6">
                    <tr>
                        <td>
                            <font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
                                <strong><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>
</strong>
                            </font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php } elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_nachnahme_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_kreditkarte_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_rechnung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_lastschrift_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_barzahlung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_paypal_jtl') {
}
if ((isset($_smarty_tpl->tpl_vars['Zahlungsart']->value->cHinweisText)) && strlen($_smarty_tpl->tpl_vars['Zahlungsart']->value->cHinweisText) > 0) {?>
    <?php echo $_smarty_tpl->tpl_vars['Zahlungsart']->value->cHinweisText;?>
<br>
    <br>
<?php }
if ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_rechnung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_lastschrift_jtl') {?>
    Wir belasten in Kürze folgendes Bankkonto mit der fälligen Summe:<br>
    <br>
    Kontoinhaber: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cInhaber;?>
<br>
    IBAN: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cIBAN;?>
<br>
    BIC: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cBIC;?>
<br>
    Bank: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cBankName;?>
<br>
    <br>
<?php } elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_barzahlung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_paypal_jtl') {?>
    Falls Sie Ihre Zahlung per PayPal noch nicht durchgeführt haben, nutzen Sie folgende E-Mail-Adresse als Empfänger: <?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['zahlungsarten']['zahlungsart_paypal_empfaengermail'];?>

<?php }?>
Über den weiteren Verlauf Ihrer Bestellung werden wir Sie jeweils gesondert informieren.<br>
<br>
<br>
Mit freundlichem Gruß<br>
Ihr Team von <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cName;?>


<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'html'),$_smarty_tpl ) );?>

<?php }
}
