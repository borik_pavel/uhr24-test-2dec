<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:20:08
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/step3_shipping_options.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196611805c820_72828920',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c3d8c6e04386b66fe065fe1b55c8a85339af1938' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/step3_shipping_options.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6196611805c820_72828920 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17805787856196611801d421_66845191', 'checkout-step3-shipping-options');
?>

<?php }
/* {block 'checkout-step3-shipping-options-alert'} */
class Block_11797548596196611801f353_45861778 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"danger"));
$_block_repeat=true;
echo $_block_plugin11->render(array('variant'=>"danger"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noShippingMethodsAvailable','section'=>'checkout'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin11->render(array('variant'=>"danger"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'checkout-step3-shipping-options-alert'} */
/* {block 'checkout-step3-shipping-options-legend-shipping-options'} */
class Block_137815154361966118023936_68285191 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="h2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingOptions'),$_smarty_tpl ) );?>
</div>
                                <?php
}
}
/* {/block 'checkout-step3-shipping-options-legend-shipping-options'} */
/* {block 'checkout-step3-shipping-options-shipping-address-link'} */
class Block_213365823619661180247f3_41514074 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="checkout-shipping-form-change">
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingTo','section'=>'checkout'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Lieferadresse']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Lieferadresse']->value->cHausnummer;?>
, <?php echo $_smarty_tpl->tpl_vars['Lieferadresse']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Lieferadresse']->value->cOrt;?>
, <?php echo $_smarty_tpl->tpl_vars['Lieferadresse']->value->cLand;?>

                                        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable6=ob_get_clean();
$_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('href'=>$_prefixVariable6."?editLieferadresse=1",'variant'=>"link",'size'=>"sm",'class'=>"font-size-sm"));
$_block_repeat=true;
echo $_block_plugin13->render(array('href'=>$_prefixVariable6."?editLieferadresse=1",'variant'=>"link",'size'=>"sm",'class'=>"font-size-sm"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <span class="text-decoration-underline"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'change'),$_smarty_tpl ) );?>
</span>
                                            <span class="checkout-shipping-form-change-icon fa fa-pencil-alt"></span>
                                        <?php $_block_repeat=false;
echo $_block_plugin13->render(array('href'=>$_prefixVariable6."?editLieferadresse=1",'variant'=>"link",'size'=>"sm",'class'=>"font-size-sm"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    </div>
                                <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-address-link'} */
/* {block 'checkout-step3-shipping-options-shipping-address-hr'} */
class Block_15974437196196611802a6d5_96741762 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <hr>
                                <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-address-hr'} */
/* {block 'checkout-step3-shipping-options-shipping-option-title-image'} */
class Block_343137699619661180323d8_63086457 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                        <?php if ($_smarty_tpl->tpl_vars['versandart']->value->cBild) {?>
                                                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('fluid'=>true,'class'=>"w-20",'src'=>$_smarty_tpl->tpl_vars['versandart']->value->cBild,'alt'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->angezeigterName ))),$_smarty_tpl ) );?>

                                                                        <?php }?>
                                                                    <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-title-image'} */
/* {block 'checkout-step3-shipping-options-shipping-option-title-title'} */
class Block_522199614619661180353d5_91662532 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                        <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->angezeigterName ));?>

                                                                        <?php if (!empty(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->angezeigterHinweistext )))) {?>
                                                                            <div>
                                                                                <small><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->angezeigterHinweistext ));?>
</small>
                                                                            </div>
                                                                        <?php }?>
                                                                    <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-title-title'} */
/* {block 'checkout-step3-shipping-options-shipping-option-title'} */
class Block_897671139619661180314a7_00286222 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                <?php $_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'sm'=>5,'class'=>'title'));
$_block_repeat=true;
echo $_block_plugin17->render(array('cols'=>12,'sm'=>5,'class'=>'title'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_343137699619661180323d8_63086457', 'checkout-step3-shipping-options-shipping-option-title-image', $this->tplIndex);
?>

                                                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_522199614619661180353d5_91662532', 'checkout-step3-shipping-options-shipping-option-title-title', $this->tplIndex);
?>

                                                                <?php $_block_repeat=false;
echo $_block_plugin17->render(array('cols'=>12,'sm'=>5,'class'=>'title'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-title'} */
/* {block 'checkout-step3-shipping-options-shipping-option-info'} */
class Block_69275056361966118039441_01360755 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                <?php $_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'sm'=>3));
$_block_repeat=true;
echo $_block_plugin18->render(array('cols'=>12,'sm'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?><small class="desc text-info"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->cLieferdauer ));?>
</small><?php $_block_repeat=false;
echo $_block_plugin18->render(array('cols'=>12,'sm'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-info'} */
/* {block 'checkout-step3-shipping-options-shipping-option-price'} */
class Block_13565317556196611803a9e2_54195451 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                <?php $_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'sm'=>4,'class'=>'font-weight-bold-util price-col'));
$_block_repeat=true;
echo $_block_plugin19->render(array('cols'=>12,'sm'=>4,'class'=>'font-weight-bold-util price-col'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                    <?php echo $_smarty_tpl->tpl_vars['versandart']->value->cPreisLocalized;?>

                                                                    <?php if (!empty($_smarty_tpl->tpl_vars['versandart']->value->Zuschlag->fZuschlag)) {?>
                                                                        <div>
                                                                            <small>
                                                                                (<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->Zuschlag->angezeigterName ));?>
 +<?php echo $_smarty_tpl->tpl_vars['versandart']->value->Zuschlag->cPreisLocalized;?>
)
                                                                            </small>
                                                                        </div>
                                                                    <?php }?>
                                                                <?php $_block_repeat=false;
echo $_block_plugin19->render(array('cols'=>12,'sm'=>4,'class'=>'font-weight-bold-util price-col'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-price'} */
/* {block 'checkout-step3-shipping-options-shipping-option-cost'} */
class Block_7331758616196611803ede2_94053317 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                        <?php $_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin20->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                            <?php $_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>8));
$_block_repeat=true;
echo $_block_plugin21->render(array('cols'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                                <ul>
                                                                                    <li>
                                                                                        <small><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['specificShippingcosts']->value->cName ));?>
</small>
                                                                                    </li>
                                                                                </ul>
                                                                            <?php $_block_repeat=false;
echo $_block_plugin21->render(array('cols'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                                            <?php $_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>4));
$_block_repeat=true;
echo $_block_plugin22->render(array('cols'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                                <small>
                                                                                    <?php echo $_smarty_tpl->tpl_vars['specificShippingcosts']->value->cPreisLocalized;?>

                                                                                </small>
                                                                            <?php $_block_repeat=false;
echo $_block_plugin22->render(array('cols'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                                        <?php $_block_repeat=false;
echo $_block_plugin20->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                                    <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-cost'} */
/* {block 'checkout-step3-shipping-options-shipment'} */
class Block_1719908266196611802ce36_68506720 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                    <?php $_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['radio'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['radio'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'radio\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('radio', array('name'=>"Versandart",'value'=>$_smarty_tpl->tpl_vars['versandart']->value->kVersandart,'id'=>"del".((string)$_smarty_tpl->tpl_vars['versandart']->value->kVersandart),'checked'=>(count($_smarty_tpl->tpl_vars['Versandarten']->value) == 1 || $_smarty_tpl->tpl_vars['AktiveVersandart']->value == $_smarty_tpl->tpl_vars['versandart']->value->kVersandart),'required'=>($_smarty_tpl->tpl_vars['versandart']->first),'class'=>"checkout-shipping-form-options-radio"));
$_block_repeat=true;
echo $_block_plugin15->render(array('name'=>"Versandart",'value'=>$_smarty_tpl->tpl_vars['versandart']->value->kVersandart,'id'=>"del".((string)$_smarty_tpl->tpl_vars['versandart']->value->kVersandart),'checked'=>(count($_smarty_tpl->tpl_vars['Versandarten']->value) == 1 || $_smarty_tpl->tpl_vars['AktiveVersandart']->value == $_smarty_tpl->tpl_vars['versandart']->value->kVersandart),'required'=>($_smarty_tpl->tpl_vars['versandart']->first),'class'=>"checkout-shipping-form-options-radio"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                        <?php $_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['formrow'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formrow'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'formrow\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formrow', array('class'=>"content"));
$_block_repeat=true;
echo $_block_plugin16->render(array('class'=>"content"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_897671139619661180314a7_00286222', 'checkout-step3-shipping-options-shipping-option-title', $this->tplIndex);
?>

                                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_69275056361966118039441_01360755', 'checkout-step3-shipping-options-shipping-option-info', $this->tplIndex);
?>

                                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13565317556196611803a9e2_54195451', 'checkout-step3-shipping-options-shipping-option-price', $this->tplIndex);
?>

                                                        <?php $_block_repeat=false;
echo $_block_plugin16->render(array('class'=>"content"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <span class="checkout-shipping-form-options-specific-cost">
                                                            <?php if ((isset($_smarty_tpl->tpl_vars['versandart']->value->specificShippingcosts_arr))) {?>
                                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['versandart']->value->specificShippingcosts_arr, 'specificShippingcosts');
$_smarty_tpl->tpl_vars['specificShippingcosts']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['specificShippingcosts']->value) {
$_smarty_tpl->tpl_vars['specificShippingcosts']->do_else = false;
?>
                                                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7331758616196611803ede2_94053317', 'checkout-step3-shipping-options-shipping-option-cost', $this->tplIndex);
?>

                                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                            <?php }?>
                                                        </span>
                                                    <?php $_block_repeat=false;
echo $_block_plugin15->render(array('name'=>"Versandart",'value'=>$_smarty_tpl->tpl_vars['versandart']->value->kVersandart,'id'=>"del".((string)$_smarty_tpl->tpl_vars['versandart']->value->kVersandart),'checked'=>(count($_smarty_tpl->tpl_vars['Versandarten']->value) == 1 || $_smarty_tpl->tpl_vars['AktiveVersandart']->value == $_smarty_tpl->tpl_vars['versandart']->value->kVersandart),'required'=>($_smarty_tpl->tpl_vars['versandart']->first),'class'=>"checkout-shipping-form-options-radio"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipment'} */
/* {block 'checkout-step3-shipping-options-shipping-options'} */
class Block_6971975776196611802b049_12371503 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="checkout-shipping-form-options form-group">
                                        <?php $_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['radiogroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['radiogroup'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'radiogroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('radiogroup', array('stacked'=>true,'class'=>'radio-w-100'));
$_block_repeat=true;
echo $_block_plugin14->render(array('stacked'=>true,'class'=>'radio-w-100'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Versandarten']->value, 'versandart');
$_smarty_tpl->tpl_vars['versandart']->index = -1;
$_smarty_tpl->tpl_vars['versandart']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['versandart']->value) {
$_smarty_tpl->tpl_vars['versandart']->do_else = false;
$_smarty_tpl->tpl_vars['versandart']->index++;
$_smarty_tpl->tpl_vars['versandart']->first = !$_smarty_tpl->tpl_vars['versandart']->index;
$__foreach_versandart_10_saved = $_smarty_tpl->tpl_vars['versandart'];
?>
                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1719908266196611802ce36_68506720', 'checkout-step3-shipping-options-shipment', $this->tplIndex);
?>

                                            <?php
$_smarty_tpl->tpl_vars['versandart'] = $__foreach_versandart_10_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        <?php $_block_repeat=false;
echo $_block_plugin14->render(array('stacked'=>true,'class'=>'radio-w-100'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    </div>
                                <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-options'} */
/* {block 'checkout-step3-shipping-options-fieldset-shipping-payment'} */
class Block_135338100061966118023387_37179881 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <fieldset id="checkout-shipping-payment">
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_137815154361966118023936_68285191', 'checkout-step3-shipping-options-legend-shipping-options', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_213365823619661180247f3_41514074', 'checkout-step3-shipping-options-shipping-address-link', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15974437196196611802a6d5_96741762', 'checkout-step3-shipping-options-shipping-address-hr', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6971975776196611802b049_12371503', 'checkout-step3-shipping-options-shipping-options', $this->tplIndex);
?>

                            </fieldset>
                        <?php
}
}
/* {/block 'checkout-step3-shipping-options-fieldset-shipping-payment'} */
/* {block 'checkout-step3-shipping-options-legend-payment'} */
class Block_193234988261966118042452_50771659 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="h2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'paymentOptions'),$_smarty_tpl ) );?>
</div>
                                <?php
}
}
/* {/block 'checkout-step3-shipping-options-legend-payment'} */
/* {block 'checkout-step3-shipping-options-fieldset-payment'} */
class Block_614223159619661180420c4_59435560 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <fieldset id="fieldset-payment">
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_193234988261966118042452_50771659', 'checkout-step3-shipping-options-legend-payment', $this->tplIndex);
?>

                                <hr>
                                <?php echo $_smarty_tpl->tpl_vars['step4_payment_content']->value;?>

                            </fieldset>
                        <?php
}
}
/* {/block 'checkout-step3-shipping-options-fieldset-payment'} */
/* {block 'checkout-step3-shipping-options-legend-packaging-types'} */
class Block_37090544761966118043f56_54937138 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <div class="h2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('section'=>'checkout','key'=>'additionalPackaging'),$_smarty_tpl ) );?>
</div>
                                    <?php
}
}
/* {/block 'checkout-step3-shipping-options-legend-packaging-types'} */
/* {block 'checkout-step3-shipping-options-legend-packaging-types-hr'} */
class Block_61901698661966118044910_41424321 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <hr>
                                    <?php
}
}
/* {/block 'checkout-step3-shipping-options-legend-packaging-types-hr'} */
/* {block 'checkout-step3-shipping-options-packaging'} */
class Block_120712234761966118045c27_28105435 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <div class="checkout-shipping-form-packaging">
                                                <?php $_block_plugin24 = isset($_smarty_tpl->smarty->registered_plugins['block']['checkbox'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['checkbox'][0][0] : null;
if (!is_callable(array($_block_plugin24, 'render'))) {
throw new SmartyException('block tag \'checkbox\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('checkbox', array('name'=>"kVerpackung[]",'value'=>$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung,'id'=>"pac".((string)$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung),'checked'=>((isset($_smarty_tpl->tpl_vars['oVerpackung']->value->bWarenkorbAktiv)) && $_smarty_tpl->tpl_vars['oVerpackung']->value->bWarenkorbAktiv === true || ((isset($_smarty_tpl->tpl_vars['AktiveVerpackung']->value[$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung])) && $_smarty_tpl->tpl_vars['AktiveVerpackung']->value[$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung] === 1))));
$_block_repeat=true;
echo $_block_plugin24->render(array('name'=>"kVerpackung[]",'value'=>$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung,'id'=>"pac".((string)$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung),'checked'=>((isset($_smarty_tpl->tpl_vars['oVerpackung']->value->bWarenkorbAktiv)) && $_smarty_tpl->tpl_vars['oVerpackung']->value->bWarenkorbAktiv === true || ((isset($_smarty_tpl->tpl_vars['AktiveVerpackung']->value[$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung])) && $_smarty_tpl->tpl_vars['AktiveVerpackung']->value[$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung] === 1))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <span class="checkout-shipping-form-packaging-title">
                                                        <?php echo $_smarty_tpl->tpl_vars['oVerpackung']->value->cName;?>

                                                    </span>
                                                    <span class="checkout-shipping-form-packaging-cost price-col">
                                                        <?php if ($_smarty_tpl->tpl_vars['oVerpackung']->value->nKostenfrei == 1) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'ExemptFromCharges'),$_smarty_tpl ) );
} else {
echo $_smarty_tpl->tpl_vars['oVerpackung']->value->fBruttoLocalized;
}?>
                                                    </span>
                                                    <span class="checkout-shipping-form-packaging-desc">
                                                        <small><?php echo $_smarty_tpl->tpl_vars['oVerpackung']->value->cBeschreibung;?>
</small>
                                                    </span>
                                                <?php $_block_repeat=false;
echo $_block_plugin24->render(array('name'=>"kVerpackung[]",'value'=>$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung,'id'=>"pac".((string)$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung),'checked'=>((isset($_smarty_tpl->tpl_vars['oVerpackung']->value->bWarenkorbAktiv)) && $_smarty_tpl->tpl_vars['oVerpackung']->value->bWarenkorbAktiv === true || ((isset($_smarty_tpl->tpl_vars['AktiveVerpackung']->value[$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung])) && $_smarty_tpl->tpl_vars['AktiveVerpackung']->value[$_smarty_tpl->tpl_vars['oVerpackung']->value->kVerpackung] === 1))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </div>
                                        <?php
}
}
/* {/block 'checkout-step3-shipping-options-packaging'} */
/* {block 'checkout-step3-shipping-options-fieldset-packaging-types'} */
class Block_65195309461966118043c09_66041801 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <fieldset>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_37090544761966118043f56_54937138', 'checkout-step3-shipping-options-legend-packaging-types', $this->tplIndex);
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_61901698661966118044910_41424321', 'checkout-step3-shipping-options-legend-packaging-types-hr', $this->tplIndex);
?>

                                    <?php $_block_plugin23 = isset($_smarty_tpl->smarty->registered_plugins['block']['checkboxgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['checkboxgroup'][0][0] : null;
if (!is_callable(array($_block_plugin23, 'render'))) {
throw new SmartyException('block tag \'checkboxgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('checkboxgroup', array('stacked'=>true));
$_block_repeat=true;
echo $_block_plugin23->render(array('stacked'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Verpackungsarten']->value, 'oVerpackung');
$_smarty_tpl->tpl_vars['oVerpackung']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oVerpackung']->value) {
$_smarty_tpl->tpl_vars['oVerpackung']->do_else = false;
?>
                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_120712234761966118045c27_28105435', 'checkout-step3-shipping-options-packaging', $this->tplIndex);
?>

                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin23->render(array('stacked'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                </fieldset>
                            <?php
}
}
/* {/block 'checkout-step3-shipping-options-fieldset-packaging-types'} */
/* {block 'checkout-step3-shipping-options-shipping-type-submit'} */
class Block_14720328286196611804ef23_38355621 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin25 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin25, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"checkout-button-row"));
$_block_repeat=true;
echo $_block_plugin25->render(array('class'=>"checkout-button-row"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin26 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin26, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>5,'class'=>'ml-auto-util order-1 order-md-2'));
$_block_repeat=true;
echo $_block_plugin26->render(array('cols'=>12,'md'=>5,'class'=>'ml-auto-util order-1 order-md-2'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"versandartwahl",'value'=>"1"),$_smarty_tpl ) );?>

                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"zahlungsartwahl",'value'=>"1"),$_smarty_tpl ) );?>

                                        <?php $_block_plugin27 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin27, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('type'=>"submit",'variant'=>"primary",'class'=>"submit_once d-none button-row-mb",'block'=>true));
$_block_repeat=true;
echo $_block_plugin27->render(array('type'=>"submit",'variant'=>"primary",'class'=>"submit_once d-none button-row-mb",'block'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'continueOrder','section'=>'account data'),$_smarty_tpl ) );?>

                                        <?php $_block_repeat=false;
echo $_block_plugin27->render(array('type'=>"submit",'variant'=>"primary",'class'=>"submit_once d-none button-row-mb",'block'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin26->render(array('cols'=>12,'md'=>5,'class'=>'ml-auto-util order-1 order-md-2'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin28 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin28, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>4,'class'=>'order-2 order-md-1'));
$_block_repeat=true;
echo $_block_plugin28->render(array('cols'=>12,'md'=>4,'class'=>'order-2 order-md-1'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable7=ob_get_clean();
$_block_plugin29 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin29, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('block'=>true,'type'=>"link",'href'=>$_prefixVariable7."?editRechnungsadresse=1",'variant'=>"outline-primary"));
$_block_repeat=true;
echo $_block_plugin29->render(array('block'=>true,'type'=>"link",'href'=>$_prefixVariable7."?editRechnungsadresse=1",'variant'=>"outline-primary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'back'),$_smarty_tpl ) );?>

                                        <?php $_block_repeat=false;
echo $_block_plugin29->render(array('block'=>true,'type'=>"link",'href'=>$_prefixVariable7."?editRechnungsadresse=1",'variant'=>"outline-primary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin28->render(array('cols'=>12,'md'=>4,'class'=>'order-2 order-md-1'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin25->render(array('class'=>"checkout-button-row"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-type-submit'} */
/* {block 'checkout-step3-shipping-options-form'} */
class Block_110103963661966118021544_90191615 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable5=ob_get_clean();
$_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('method'=>"post",'action'=>$_prefixVariable5,'class'=>"form checkout-shipping-form jtl-validate"));
$_block_repeat=true;
echo $_block_plugin12->render(array('method'=>"post",'action'=>$_prefixVariable5,'class'=>"form checkout-shipping-form jtl-validate"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_135338100061966118023387_37179881', 'checkout-step3-shipping-options-fieldset-shipping-payment', $this->tplIndex);
?>

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_614223159619661180420c4_59435560', 'checkout-step3-shipping-options-fieldset-payment', $this->tplIndex);
?>

                        <?php if ((isset($_smarty_tpl->tpl_vars['Verpackungsarten']->value)) && count($_smarty_tpl->tpl_vars['Verpackungsarten']->value) > 0) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_65195309461966118043c09_66041801', 'checkout-step3-shipping-options-fieldset-packaging-types', $this->tplIndex);
?>

                        <?php }?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['Versandarten']->value))) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14720328286196611804ef23_38355621', 'checkout-step3-shipping-options-shipping-type-submit', $this->tplIndex);
?>

                        <?php }?>
                    <?php $_block_repeat=false;
echo $_block_plugin12->render(array('method'=>"post",'action'=>$_prefixVariable5,'class'=>"form checkout-shipping-form jtl-validate"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'checkout-step3-shipping-options-form'} */
/* {block 'checkout-step3-shipping-options-script-scroll'} */
class Block_9318529226196611805aa43_05578771 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin30 = isset($_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0] : null;
if (!is_callable(array($_block_plugin30, 'inlineScript'))) {
throw new SmartyException('block tag \'inline_script\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inline_script', array());
$_block_repeat=true;
echo $_block_plugin30->inlineScript(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo '<script'; ?>
>
                $(document).ready(function () {
                    $.evo.extended().smoothScrollToAnchor('#fieldset-payment');
                });
            <?php echo '</script'; ?>
><?php $_block_repeat=false;
echo $_block_plugin30->inlineScript(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'checkout-step3-shipping-options-script-scroll'} */
/* {block 'checkout-step3-shipping-options'} */
class Block_17805787856196611801d421_66845191 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-step3-shipping-options' => 
  array (
    0 => 'Block_17805787856196611801d421_66845191',
  ),
  'checkout-step3-shipping-options-alert' => 
  array (
    0 => 'Block_11797548596196611801f353_45861778',
  ),
  'checkout-step3-shipping-options-form' => 
  array (
    0 => 'Block_110103963661966118021544_90191615',
  ),
  'checkout-step3-shipping-options-fieldset-shipping-payment' => 
  array (
    0 => 'Block_135338100061966118023387_37179881',
  ),
  'checkout-step3-shipping-options-legend-shipping-options' => 
  array (
    0 => 'Block_137815154361966118023936_68285191',
  ),
  'checkout-step3-shipping-options-shipping-address-link' => 
  array (
    0 => 'Block_213365823619661180247f3_41514074',
  ),
  'checkout-step3-shipping-options-shipping-address-hr' => 
  array (
    0 => 'Block_15974437196196611802a6d5_96741762',
  ),
  'checkout-step3-shipping-options-shipping-options' => 
  array (
    0 => 'Block_6971975776196611802b049_12371503',
  ),
  'checkout-step3-shipping-options-shipment' => 
  array (
    0 => 'Block_1719908266196611802ce36_68506720',
  ),
  'checkout-step3-shipping-options-shipping-option-title' => 
  array (
    0 => 'Block_897671139619661180314a7_00286222',
  ),
  'checkout-step3-shipping-options-shipping-option-title-image' => 
  array (
    0 => 'Block_343137699619661180323d8_63086457',
  ),
  'checkout-step3-shipping-options-shipping-option-title-title' => 
  array (
    0 => 'Block_522199614619661180353d5_91662532',
  ),
  'checkout-step3-shipping-options-shipping-option-info' => 
  array (
    0 => 'Block_69275056361966118039441_01360755',
  ),
  'checkout-step3-shipping-options-shipping-option-price' => 
  array (
    0 => 'Block_13565317556196611803a9e2_54195451',
  ),
  'checkout-step3-shipping-options-shipping-option-cost' => 
  array (
    0 => 'Block_7331758616196611803ede2_94053317',
  ),
  'checkout-step3-shipping-options-fieldset-payment' => 
  array (
    0 => 'Block_614223159619661180420c4_59435560',
  ),
  'checkout-step3-shipping-options-legend-payment' => 
  array (
    0 => 'Block_193234988261966118042452_50771659',
  ),
  'checkout-step3-shipping-options-fieldset-packaging-types' => 
  array (
    0 => 'Block_65195309461966118043c09_66041801',
  ),
  'checkout-step3-shipping-options-legend-packaging-types' => 
  array (
    0 => 'Block_37090544761966118043f56_54937138',
  ),
  'checkout-step3-shipping-options-legend-packaging-types-hr' => 
  array (
    0 => 'Block_61901698661966118044910_41424321',
  ),
  'checkout-step3-shipping-options-packaging' => 
  array (
    0 => 'Block_120712234761966118045c27_28105435',
  ),
  'checkout-step3-shipping-options-shipping-type-submit' => 
  array (
    0 => 'Block_14720328286196611804ef23_38355621',
  ),
  'checkout-step3-shipping-options-script-scroll' => 
  array (
    0 => 'Block_9318529226196611805aa43_05578771',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin9->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>9));
$_block_repeat=true;
echo $_block_plugin10->render(array('cols'=>12,'lg'=>9), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php if (!(isset($_smarty_tpl->tpl_vars['Versandarten']->value))) {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11797548596196611801f353_45861778', 'checkout-step3-shipping-options-alert', $this->tplIndex);
?>

            <?php } else { ?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_110103963661966118021544_90191615', 'checkout-step3-shipping-options-form', $this->tplIndex);
?>

            <?php }?>
        <?php $_block_repeat=false;
echo $_block_plugin10->render(array('cols'=>12,'lg'=>9), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php $_block_repeat=false;
echo $_block_plugin9->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php if ((isset($_GET['editZahlungsart']))) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9318529226196611805aa43_05578771', 'checkout-step3-shipping-options-script-scroll', $this->tplIndex);
?>

    <?php }
}
}
/* {/block 'checkout-step3-shipping-options'} */
}
