<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:32
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_steps.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f4e1c3a4_33230888',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba9dead77f6934464326151a08d4646511222c38' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_steps.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619660f4e1c3a4_33230888 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1071106111619660f4e08044_92373224', 'checkout-inc-steps');
?>

<?php }
/* {block 'checkout-inc-steps-first'} */
class Block_1868785408619660f4e0e160_73414349 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable5=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('section'=>'account data','key'=>'billingAndDeliveryAddress'),$_smarty_tpl ) );
$_prefixVariable6=ob_get_clean();
$_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable5."?editRechnungsadresse=1",'title'=>$_prefixVariable6,'class'=>"text-decoration-none-util"));
$_block_repeat=true;
echo $_block_plugin17->render(array('href'=>$_prefixVariable5."?editRechnungsadresse=1",'title'=>$_prefixVariable6,'class'=>"text-decoration-none-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <div class="step-content">
                            <?php $_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'badge\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('badge', array('variant'=>"primary",'class'=>"badge-pill"));
$_block_repeat=true;
echo $_block_plugin18->render(array('variant'=>"primary",'class'=>"badge-pill"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <span class="badge-count">1</span>
                            <?php $_block_repeat=false;
echo $_block_plugin18->render(array('variant'=>"primary",'class'=>"badge-pill"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <span class="step-text <?php if (!$_smarty_tpl->tpl_vars['step1_active']->value) {?>d-none d-md-inline-block<?php }?>">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('section'=>'account data','key'=>'billingAndDeliveryAddress'),$_smarty_tpl ) );?>

                            </span>
                            <?php if ($_smarty_tpl->tpl_vars['step2_active']->value || $_smarty_tpl->tpl_vars['step3_active']->value) {?>
                                <span class="fas fa-check step-check"></span>
                            <?php }?>
                        </div>
                    <?php $_block_repeat=false;
echo $_block_plugin17->render(array('href'=>$_prefixVariable5."?editRechnungsadresse=1",'title'=>$_prefixVariable6,'class'=>"text-decoration-none-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'checkout-inc-steps-first'} */
/* {block 'checkout-inc-steps-second'} */
class Block_813222940619660f4e14311_74198911 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable9=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('section'=>'account data','key'=>'shippingAndPaymentOptions'),$_smarty_tpl ) );
$_prefixVariable10=ob_get_clean();
$_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable9."?editVersandart=1",'title'=>$_prefixVariable10,'class'=>"text-decoration-none-util"));
$_block_repeat=true;
echo $_block_plugin20->render(array('href'=>$_prefixVariable9."?editVersandart=1",'title'=>$_prefixVariable10,'class'=>"text-decoration-none-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <div class="step-content">
                            <?php ob_start();
if ($_smarty_tpl->tpl_vars['step2_active']->value || $_smarty_tpl->tpl_vars['step3_active']->value) {
echo "primary";
} else {
echo "secondary";
}
$_prefixVariable11=ob_get_clean();
$_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'badge\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('badge', array('variant'=>$_prefixVariable11,'class'=>"badge-pill"));
$_block_repeat=true;
echo $_block_plugin21->render(array('variant'=>$_prefixVariable11,'class'=>"badge-pill"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <span class="badge-count">2</span>
                            <?php $_block_repeat=false;
echo $_block_plugin21->render(array('variant'=>$_prefixVariable11,'class'=>"badge-pill"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <span class="step-text <?php if (!$_smarty_tpl->tpl_vars['step2_active']->value) {?>d-none d-md-inline-block<?php }?>">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('section'=>'account data','key'=>'shippingAndPaymentOptions'),$_smarty_tpl ) );?>

                            </span>
                            <?php if ($_smarty_tpl->tpl_vars['step3_active']->value) {?>
                                <span class="fas fa-check step-check"></span>
                            <?php }?>
                        </div>
                    <?php $_block_repeat=false;
echo $_block_plugin20->render(array('href'=>$_prefixVariable9."?editVersandart=1",'title'=>$_prefixVariable10,'class'=>"text-decoration-none-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'checkout-inc-steps-second'} */
/* {block 'checkout-inc-steps-third'} */
class Block_1785011956619660f4e19324_89269877 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div class="step-content">
                        <?php ob_start();
if ($_smarty_tpl->tpl_vars['step3_active']->value) {
echo "primary";
} else {
echo "secondary";
}
$_prefixVariable13=ob_get_clean();
$_block_plugin23 = isset($_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0] : null;
if (!is_callable(array($_block_plugin23, 'render'))) {
throw new SmartyException('block tag \'badge\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('badge', array('variant'=>$_prefixVariable13,'class'=>"badge-pill"));
$_block_repeat=true;
echo $_block_plugin23->render(array('variant'=>$_prefixVariable13,'class'=>"badge-pill"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <span class="badge-count">3</span>
                        <?php $_block_repeat=false;
echo $_block_plugin23->render(array('variant'=>$_prefixVariable13,'class'=>"badge-pill"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <span class="step-text <?php if (!$_smarty_tpl->tpl_vars['step3_active']->value) {?>d-none d-md-inline-block<?php }?>">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('section'=>'checkout','key'=>'summary'),$_smarty_tpl ) );?>

                        </span>
                    </div>
                <?php
}
}
/* {/block 'checkout-inc-steps-third'} */
/* {block 'checkout-inc-steps'} */
class Block_1071106111619660f4e08044_92373224 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-inc-steps' => 
  array (
    0 => 'Block_1071106111619660f4e08044_92373224',
  ),
  'checkout-inc-steps-first' => 
  array (
    0 => 'Block_1868785408619660f4e0e160_73414349',
  ),
  'checkout-inc-steps-second' => 
  array (
    0 => 'Block_813222940619660f4e14311_74198911',
  ),
  'checkout-inc-steps-third' => 
  array (
    0 => 'Block_1785011956619660f4e19324_89269877',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_assignInScope('step1_active', ($_smarty_tpl->tpl_vars['bestellschritt']->value[1] == 1 || $_smarty_tpl->tpl_vars['bestellschritt']->value[2] == 1));?>
    <?php $_smarty_tpl->_assignInScope('step2_active', ($_smarty_tpl->tpl_vars['bestellschritt']->value[3] == 1 || $_smarty_tpl->tpl_vars['bestellschritt']->value[4] == 1));?>
    <?php $_smarty_tpl->_assignInScope('step3_active', ($_smarty_tpl->tpl_vars['bestellschritt']->value[5] == 1));?>
    <?php if ($_smarty_tpl->tpl_vars['bestellschritt']->value[1] != 3) {?>
        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'secureCheckout','section'=>'checkout'),$_smarty_tpl ) );
$_prefixVariable3=ob_get_clean();
$_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['nav'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['nav'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'nav\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('nav', array('class'=>'stepper checkout-steps','tag'=>'nav','aria'=>array("label"=>$_prefixVariable3)));
$_block_repeat=true;
echo $_block_plugin15->render(array('class'=>'stepper checkout-steps','tag'=>'nav','aria'=>array("label"=>$_prefixVariable3)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php ob_start();
if ($_smarty_tpl->tpl_vars['step1_active']->value) {
echo "step-current";
} else {
echo "col-auto";
}
$_prefixVariable4=ob_get_clean();
$_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('lg'=>4,'class'=>"nav-item step step-active ".$_prefixVariable4));
$_block_repeat=true;
echo $_block_plugin16->render(array('lg'=>4,'class'=>"nav-item step step-active ".$_prefixVariable4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1868785408619660f4e0e160_73414349', 'checkout-inc-steps-first', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin16->render(array('lg'=>4,'class'=>"nav-item step step-active ".$_prefixVariable4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php ob_start();
if ($_smarty_tpl->tpl_vars['step2_active']->value || $_smarty_tpl->tpl_vars['step3_active']->value) {
echo "step-active";
}
$_prefixVariable7=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['step2_active']->value) {
echo "step-current";
} else {
echo "col-auto";
}
$_prefixVariable8=ob_get_clean();
$_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('lg'=>4,'class'=>"nav-item step ".$_prefixVariable7." ".$_prefixVariable8));
$_block_repeat=true;
echo $_block_plugin19->render(array('lg'=>4,'class'=>"nav-item step ".$_prefixVariable7." ".$_prefixVariable8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_813222940619660f4e14311_74198911', 'checkout-inc-steps-second', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin19->render(array('lg'=>4,'class'=>"nav-item step ".$_prefixVariable7." ".$_prefixVariable8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php ob_start();
if ($_smarty_tpl->tpl_vars['step3_active']->value) {
echo "step-active step-current";
} else {
echo "col-auto";
}
$_prefixVariable12=ob_get_clean();
$_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('lg'=>4,'class'=>"nav-item step ".$_prefixVariable12));
$_block_repeat=true;
echo $_block_plugin22->render(array('lg'=>4,'class'=>"nav-item step ".$_prefixVariable12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1785011956619660f4e19324_89269877', 'checkout-inc-steps-third', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin22->render(array('lg'=>4,'class'=>"nav-item step ".$_prefixVariable12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_repeat=false;
echo $_block_plugin15->render(array('class'=>'stepper checkout-steps','tag'=>'nav','aria'=>array("label"=>$_prefixVariable3)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php }
}
}
/* {/block 'checkout-inc-steps'} */
}
