<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:23:32
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/maintenance.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953a94d4b925_41761051',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea456a0ca0ca103fdef79f94f51927f778c3049c' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/maintenance.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/language_dropdown.tpl' => 1,
  ),
),false)) {
function content_61953a94d4b925_41761051 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18952300261953a94d26138_88105347', 'snippets-maintenance');
?>

<?php }
/* {block 'snippets-maintenance-header-doctype'} */
class Block_57064677161953a94d26684_67468754 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html><?php
}
}
/* {/block 'snippets-maintenance-header-doctype'} */
/* {block 'snippets-maintenance-header-html-attributes'} */
class Block_7165416561953a94d26dd5_79006632 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
lang="<?php echo $_smarty_tpl->tpl_vars['meta_language']->value;?>
" itemscope <?php if ($_smarty_tpl->tpl_vars['nSeitenTyp']->value === (defined('URLART_ARTIKEL') ? constant('URLART_ARTIKEL') : null)) {?>itemtype="https://schema.org/ItemPage"
          <?php } elseif ($_smarty_tpl->tpl_vars['nSeitenTyp']->value === (defined('URLART_KATEGORIE') ? constant('URLART_KATEGORIE') : null)) {?>itemtype="https://schema.org/CollectionPage"
          <?php } else { ?>itemtype="https://schema.org/WebPage"<?php }
}
}
/* {/block 'snippets-maintenance-header-html-attributes'} */
/* {block 'snippets-maintenance-header-head-meta-description'} */
class Block_23565788861953a94d29ad2_23119606 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
"<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_description']->value,1000,'',true ));
}
}
/* {/block 'snippets-maintenance-header-head-meta-description'} */
/* {block 'snippets-maintenance-header-head-meta-keywords'} */
class Block_62419272861953a94d2b244_29705303 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_keywords']->value,255,'',true ));
}
}
/* {/block 'snippets-maintenance-header-head-meta-keywords'} */
/* {block 'snippets-maintenance-header-head-meta'} */
class Block_196494303461953a94d291e6_91330001 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <meta http-equiv="content-type" content="text/html; charset=<?php echo (defined('JTL_CHARSET') ? constant('JTL_CHARSET') : null);?>
">
                <meta name="description" itemprop="description" content=<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_23565788861953a94d29ad2_23119606', 'snippets-maintenance-header-head-meta-description', $this->tplIndex);
?>
">
                <?php if (!empty($_smarty_tpl->tpl_vars['meta_keywords']->value)) {?>
                    <meta name="keywords" itemprop="keywords" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_62419272861953a94d2b244_29705303', 'snippets-maintenance-header-head-meta-keywords', $this->tplIndex);
?>
">
                <?php }?>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="robots" content="<?php if ($_smarty_tpl->tpl_vars['robotsContent']->value) {
echo $_smarty_tpl->tpl_vars['robotsContent']->value;
} elseif ($_smarty_tpl->tpl_vars['bNoIndex']->value === true || ((isset($_smarty_tpl->tpl_vars['Link']->value)) && $_smarty_tpl->tpl_vars['Link']->value->getNoFollow() === true)) {?>noindex<?php } else { ?>index, follow<?php }?>">

                <meta itemprop="url" content="<?php echo $_smarty_tpl->tpl_vars['cCanonicalURL']->value;?>
"/>
                <meta property="og:type" content="website" />
                <meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
" />
                <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
" />
                <meta property="og:description" content="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_description']->value,1000,'',true ));?>
" />
                <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['cCanonicalURL']->value;?>
"/>

                <?php if ($_smarty_tpl->tpl_vars['nSeitenTyp']->value === (defined('PAGE_ARTIKEL') ? constant('PAGE_ARTIKEL') : null) && !empty($_smarty_tpl->tpl_vars['Artikel']->value->Bilder)) {?>
                    <meta itemprop="image" content="<?php echo $_smarty_tpl->tpl_vars['Artikel']->value->Bilder[0]->cURLGross;?>
" />
                    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['Artikel']->value->Bilder[0]->cURLGross;?>
">
                <?php } elseif ($_smarty_tpl->tpl_vars['nSeitenTyp']->value === (defined('PAGE_NEWSDETAIL') ? constant('PAGE_NEWSDETAIL') : null) && !empty($_smarty_tpl->tpl_vars['newsItem']->value->getPreviewImage())) {?>
                    <meta itemprop="image" content="<?php echo $_smarty_tpl->tpl_vars['imageBaseURL']->value;
echo $_smarty_tpl->tpl_vars['newsItem']->value->getPreviewImage();?>
" />
                    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['imageBaseURL']->value;
echo $_smarty_tpl->tpl_vars['newsItem']->value->getPreviewImage();?>
" />
                <?php } else { ?>
                    <meta itemprop="image" content="<?php echo $_smarty_tpl->tpl_vars['ShopLogoURL']->value;?>
" />
                    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['ShopLogoURL']->value;?>
" />
                <?php }?>

            <?php
}
}
/* {/block 'snippets-maintenance-header-head-meta'} */
/* {block 'snippets-maintenance-header-head-title'} */
class Block_51192528961953a94d328c1_40388396 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['meta_title']->value;
}
}
/* {/block 'snippets-maintenance-header-head-title'} */
/* {block 'snippets-maintenance-header-head-icons'} */
class Block_39807048461953a94d33915_08100206 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <link type="image/x-icon" href="<?php echo $_smarty_tpl->tpl_vars['shopFaviconURL']->value;?>
" rel="icon">
            <?php
}
}
/* {/block 'snippets-maintenance-header-head-icons'} */
/* {block 'snippets-maintenance-header-head-resources'} */
class Block_4717342961953a94d34071_68416304 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php if (empty($_smarty_tpl->tpl_vars['parentTemplateDir']->value)) {?>
                    <?php $_smarty_tpl->_assignInScope('templateDir', $_smarty_tpl->tpl_vars['currentTemplateDir']->value);?>
                <?php } else { ?>
                    <?php $_smarty_tpl->_assignInScope('templateDir', $_smarty_tpl->tpl_vars['parentTemplateDir']->value);?>
                <?php }?>
                <style id="criticalCSS">
                    <?php echo file_get_contents(((string)$_smarty_tpl->tpl_vars['currentThemeDir']->value).((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['theme']['theme_default'])."_crit.css");?>

                </style>
                                <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['general']['use_minify'] === 'N') {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cCSS_arr']->value, 'cCSS');
$_smarty_tpl->tpl_vars['cCSS']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cCSS']->value) {
$_smarty_tpl->tpl_vars['cCSS']->do_else = false;
?>
                        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cCSS']->value;?>
?v=<?php echo $_smarty_tpl->tpl_vars['nTemplateVersion']->value;?>
">
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    <noscript>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cCSS_arr']->value, 'cCSS');
$_smarty_tpl->tpl_vars['cCSS']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cCSS']->value) {
$_smarty_tpl->tpl_vars['cCSS']->do_else = false;
?>
                            <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cCSS']->value;?>
?v=<?php echo $_smarty_tpl->tpl_vars['nTemplateVersion']->value;?>
">
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['cPluginCss_arr']->value))) {?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cPluginCss_arr']->value, 'cCSS');
$_smarty_tpl->tpl_vars['cCSS']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cCSS']->value) {
$_smarty_tpl->tpl_vars['cCSS']->do_else = false;
?>
                                <link href="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cCSS']->value;?>
?v=<?php echo $_smarty_tpl->tpl_vars['nTemplateVersion']->value;?>
" rel="stylesheet">
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <?php }?>
                    </noscript>
                <?php } else { ?>
                    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['combinedCSS']->value;?>
" type="text/css" >
                    <noscript>
                        <link href="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['combinedCSS']->value;?>
" rel="stylesheet">
                    </noscript>
                <?php }?>

                <?php if (!empty($_SESSION['Sprachen']) && count($_SESSION['Sprachen']) > 1) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_SESSION['Sprachen'], 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                        <link rel="alternate" hreflang="<?php echo $_smarty_tpl->tpl_vars['language']->value->getIso639();?>
" href="<?php echo $_smarty_tpl->tpl_vars['language']->value->getUrl();?>
">
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <?php }?>
                <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['templateDir']->value;?>
js/jquery-3.5.1.min.js"><?php echo '</script'; ?>
>
                <?php echo '<script'; ?>
 defer src="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['templateDir']->value;?>
js/bootstrap.bundle.js"><?php echo '</script'; ?>
>
            <?php
}
}
/* {/block 'snippets-maintenance-header-head-resources'} */
/* {block 'snippets-maintenance-header-head'} */
class Block_21327915461953a94d28e68_04633071 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <head>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_196494303461953a94d291e6_91330001', 'snippets-maintenance-header-head-meta', $this->tplIndex);
?>

            <title itemprop="name"><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_51192528961953a94d328c1_40388396', 'snippets-maintenance-header-head-title', $this->tplIndex);
?>
</title>

            <?php if (!empty($_smarty_tpl->tpl_vars['cCanonicalURL']->value)) {?>
                <link rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['cCanonicalURL']->value;?>
">
            <?php }?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_39807048461953a94d33915_08100206', 'snippets-maintenance-header-head-icons', $this->tplIndex);
?>


            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4717342961953a94d34071_68416304', 'snippets-maintenance-header-head-resources', $this->tplIndex);
?>

        </head>
    <?php
}
}
/* {/block 'snippets-maintenance-header-head'} */
/* {block 'snippets-maintenance-content-include-language-dropdown'} */
class Block_14921835761953a94d40422_70692090 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:snippets/language_dropdown.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('dropdownClass'=>'mx-auto'), 0, false);
}
}
/* {/block 'snippets-maintenance-content-include-language-dropdown'} */
/* {block 'snippets-maintenance-content-language'} */
class Block_85235123261953a94d3f100_75583271 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin46 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin46, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin46->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin47 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin47, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"maintenance-main-item",'cols'=>12,'md'=>6,'offset-md'=>3));
$_block_repeat=true;
echo $_block_plugin47->render(array('class'=>"maintenance-main-item",'cols'=>12,'md'=>6,'offset-md'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin48 = isset($_smarty_tpl->smarty->registered_plugins['block']['nav'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['nav'][0][0] : null;
if (!is_callable(array($_block_plugin48, 'render'))) {
throw new SmartyException('block tag \'nav\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('nav', array('tag'=>'ul','class'=>'nav-dividers'));
$_block_repeat=true;
echo $_block_plugin48->render(array('tag'=>'ul','class'=>'nav-dividers'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14921835761953a94d40422_70692090', 'snippets-maintenance-content-include-language-dropdown', $this->tplIndex);
$_block_repeat=false;
echo $_block_plugin48->render(array('tag'=>'ul','class'=>'nav-dividers'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin47->render(array('class'=>"maintenance-main-item",'cols'=>12,'md'=>6,'offset-md'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin46->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'snippets-maintenance-content-language'} */
/* {block 'snippets-maintenance-content-maintenance-logo'} */
class Block_15273660161953a94d42645_33788599 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php if ((isset($_smarty_tpl->tpl_vars['ShopLogoURL']->value))) {?>
                                    <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['ShopLogoURL']->value,'.svg') !== false) {
echo "height: 100px;";
}
$_prefixVariable22=ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('src'=>$_smarty_tpl->tpl_vars['ShopLogoURL']->value,'alt'=>$_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_shopname'],'class'=>"maintenance-main-image",'style'=>$_prefixVariable22),$_smarty_tpl ) );?>

                                <?php } else { ?>
                                    <span class="h1"><?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_shopname'];?>
</span>
                                <?php }?>
                            <?php
}
}
/* {/block 'snippets-maintenance-content-maintenance-logo'} */
/* {block 'snippets-maintenance-content-maintenance-heading'} */
class Block_62629611461953a94d45f37_02025145 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <h1 class="maintenance-main-heading"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'maintainance'),$_smarty_tpl ) );?>
</h1>
                            <?php
}
}
/* {/block 'snippets-maintenance-content-maintenance-heading'} */
/* {block 'snippets-maintenance-content-maintenance-notice'} */
class Block_27115282161953a94d46868_71359601 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <div class="maintenance-main-notice">
                                    <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'maintenanceModeActive'),$_smarty_tpl ) );?>
</p>
                                </div>
                            <?php
}
}
/* {/block 'snippets-maintenance-content-maintenance-notice'} */
/* {block 'snippets-maintenance-content-maintenance'} */
class Block_188055058461953a94d41894_73549385 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                     <?php $_block_plugin49 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin49, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin49->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin50 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin50, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"maintenance-main-item",'cols'=>12,'md'=>6,'offset-md'=>3));
$_block_repeat=true;
echo $_block_plugin50->render(array('class'=>"maintenance-main-item",'cols'=>12,'md'=>6,'offset-md'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15273660161953a94d42645_33788599', 'snippets-maintenance-content-maintenance-logo', $this->tplIndex);
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_62629611461953a94d45f37_02025145', 'snippets-maintenance-content-maintenance-heading', $this->tplIndex);
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_27115282161953a94d46868_71359601', 'snippets-maintenance-content-maintenance-notice', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin50->render(array('class'=>"maintenance-main-item",'cols'=>12,'md'=>6,'offset-md'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin49->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'snippets-maintenance-content-maintenance'} */
/* {block 'snippets-maintenance-content-imprint'} */
class Block_11847644361953a94d484c0_22355899 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin51 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin51, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('id'=>"footer",'class'=>"flex-grow-1"));
$_block_repeat=true;
echo $_block_plugin51->render(array('id'=>"footer",'class'=>"flex-grow-1"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin52 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin52, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'class'=>"small",'md'=>6,'offset-md'=>3));
$_block_repeat=true;
echo $_block_plugin52->render(array('cols'=>12,'class'=>"small",'md'=>6,'offset-md'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <h2 class="mt-2"><?php echo $_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_IMPRESSUM') ? constant('LINKTYP_IMPRESSUM') : null)]->getTitle();?>
</h2>
                            <p><?php echo $_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_IMPRESSUM') ? constant('LINKTYP_IMPRESSUM') : null)]->getContent();?>
</p>
                        <?php $_block_repeat=false;
echo $_block_plugin52->render(array('cols'=>12,'class'=>"small",'md'=>6,'offset-md'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin51->render(array('id'=>"footer",'class'=>"flex-grow-1"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'snippets-maintenance-content-imprint'} */
/* {block 'snippets-maintenance-content'} */
class Block_200732698761953a94d3e532_41013704 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <body id="main-wrapper" class="maintenance-main-wrapper text-center-util font-size-1.5x vh-100">
            <?php $_block_plugin45 = isset($_smarty_tpl->smarty->registered_plugins['block']['container'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['container'][0][0] : null;
if (!is_callable(array($_block_plugin45, 'render'))) {
throw new SmartyException('block tag \'container\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('container', array('class'=>"maintenance-main",'fluid'=>true));
$_block_repeat=true;
echo $_block_plugin45->render(array('class'=>"maintenance-main",'fluid'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_85235123261953a94d3f100_75583271', 'snippets-maintenance-content-language', $this->tplIndex);
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_188055058461953a94d41894_73549385', 'snippets-maintenance-content-maintenance', $this->tplIndex);
?>

                <?php if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_IMPRESSUM') ? constant('LINKTYP_IMPRESSUM') : null)]))) {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11847644361953a94d484c0_22355899', 'snippets-maintenance-content-imprint', $this->tplIndex);
?>

                <?php }?>
            <?php $_block_repeat=false;
echo $_block_plugin45->render(array('class'=>"maintenance-main",'fluid'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        </body>
        <?php
}
}
/* {/block 'snippets-maintenance-content'} */
/* {block 'snippets-maintenance'} */
class Block_18952300261953a94d26138_88105347 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-maintenance' => 
  array (
    0 => 'Block_18952300261953a94d26138_88105347',
  ),
  'snippets-maintenance-header-doctype' => 
  array (
    0 => 'Block_57064677161953a94d26684_67468754',
  ),
  'snippets-maintenance-header-html-attributes' => 
  array (
    0 => 'Block_7165416561953a94d26dd5_79006632',
  ),
  'snippets-maintenance-header-head' => 
  array (
    0 => 'Block_21327915461953a94d28e68_04633071',
  ),
  'snippets-maintenance-header-head-meta' => 
  array (
    0 => 'Block_196494303461953a94d291e6_91330001',
  ),
  'snippets-maintenance-header-head-meta-description' => 
  array (
    0 => 'Block_23565788861953a94d29ad2_23119606',
  ),
  'snippets-maintenance-header-head-meta-keywords' => 
  array (
    0 => 'Block_62419272861953a94d2b244_29705303',
  ),
  'snippets-maintenance-header-head-title' => 
  array (
    0 => 'Block_51192528961953a94d328c1_40388396',
  ),
  'snippets-maintenance-header-head-icons' => 
  array (
    0 => 'Block_39807048461953a94d33915_08100206',
  ),
  'snippets-maintenance-header-head-resources' => 
  array (
    0 => 'Block_4717342961953a94d34071_68416304',
  ),
  'snippets-maintenance-content' => 
  array (
    0 => 'Block_200732698761953a94d3e532_41013704',
  ),
  'snippets-maintenance-content-language' => 
  array (
    0 => 'Block_85235123261953a94d3f100_75583271',
  ),
  'snippets-maintenance-content-include-language-dropdown' => 
  array (
    0 => 'Block_14921835761953a94d40422_70692090',
  ),
  'snippets-maintenance-content-maintenance' => 
  array (
    0 => 'Block_188055058461953a94d41894_73549385',
  ),
  'snippets-maintenance-content-maintenance-logo' => 
  array (
    0 => 'Block_15273660161953a94d42645_33788599',
  ),
  'snippets-maintenance-content-maintenance-heading' => 
  array (
    0 => 'Block_62629611461953a94d45f37_02025145',
  ),
  'snippets-maintenance-content-maintenance-notice' => 
  array (
    0 => 'Block_27115282161953a94d46868_71359601',
  ),
  'snippets-maintenance-content-imprint' => 
  array (
    0 => 'Block_11847644361953a94d484c0_22355899',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_57064677161953a94d26684_67468754', 'snippets-maintenance-header-doctype', $this->tplIndex);
?>

    <html <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7165416561953a94d26dd5_79006632', 'snippets-maintenance-header-html-attributes', $this->tplIndex);
?>
>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21327915461953a94d28e68_04633071', 'snippets-maintenance-header-head', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_200732698761953a94d3e532_41013704', 'snippets-maintenance-content', $this->tplIndex);
?>

    </html>
<?php
}
}
/* {/block 'snippets-maintenance'} */
}
