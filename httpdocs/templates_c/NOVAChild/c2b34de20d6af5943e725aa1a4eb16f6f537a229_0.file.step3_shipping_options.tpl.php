<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:20:08
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVAChild/checkout/step3_shipping_options.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196611800d2d0_97766239',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c2b34de20d6af5943e725aa1a4eb16f6f537a229' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVAChild/checkout/step3_shipping_options.tpl',
      1 => 1634540733,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6196611800d2d0_97766239 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>




                                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_130324335261966118006ce1_73443850', 'checkout-step3-shipping-options-shipping-option-price');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['parent_template_path']->value)."/checkout/step3_shipping_options.tpl");
}
/* {block 'checkout-step3-shipping-options-shipping-option-price'} */
class Block_130324335261966118006ce1_73443850 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-step3-shipping-options-shipping-option-price' => 
  array (
    0 => 'Block_130324335261966118006ce1_73443850',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'sm'=>4,'class'=>'font-weight-bold-util'));
$_block_repeat=true;
echo $_block_plugin8->render(array('cols'=>12,'sm'=>4,'class'=>'font-weight-bold-util'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
																
																<?php if (call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->angezeigterName )) != 'Abholung') {?>
                                                                    <?php echo $_smarty_tpl->tpl_vars['versandart']->value->cPreisLocalized;?>

                                                                    <?php if (!empty($_smarty_tpl->tpl_vars['versandart']->value->Zuschlag->fZuschlag)) {?>
                                                                        <div>
                                                                            <small>
                                                                                (<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['versandart']->value->Zuschlag->angezeigterName ));?>
 +<?php echo $_smarty_tpl->tpl_vars['versandart']->value->Zuschlag->cPreisLocalized;?>
)
                                                                            </small>
                                                                        </div>
                                                                    <?php }?>
																<?php }?>	
																	
                                                                <?php $_block_repeat=false;
echo $_block_plugin8->render(array('cols'=>12,'sm'=>4,'class'=>'font-weight-bold-util'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php
}
}
/* {/block 'checkout-step3-shipping-options-shipping-option-price'} */
}
