<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:23:33
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/shopauskunft/frontend/tpl/textRatings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953a951b2873_36500967',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '07177f0e796493d582406611842db9d449d397be' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/shopauskunft/frontend/tpl/textRatings.tpl',
      1 => 1632584625,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./starRating.tpl' => 1,
  ),
),false)) {
function content_61953a951b2873_36500967 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<div id="sa-ratings" class="sa-ratings col-xs-12">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ratingList']->value, 'item', false, 'key', 'sAaWidgetRatingText', array (
  'first' => true,
  'index' => true,
));
$_smarty_tpl->tpl_vars['item']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_sAaWidgetRatingText']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_sAaWidgetRatingText']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_sAaWidgetRatingText']->value['index'];
?>
    <div class="sa-rating" aria-hidden="<?php echo !(isset($_smarty_tpl->tpl_vars['__smarty_foreach_sAaWidgetRatingText']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_sAaWidgetRatingText']->value['first'] : null);?>
" style="height:100%; width: 100%;">
        <?php if ((isset($_smarty_tpl->tpl_vars['item']->value['text']))) {?>
            <div class="col-xs-12" style="display: flex;">
                <i class="fa fa-user" style="margin: auto 8px; margin-left: 0;font-size: 32px"></i>
                <div style="text-align: left;">
                    <span style="font-size: 10px"><?php echo $_smarty_tpl->tpl_vars['item']->value['evaluator']['username'];?>
</span><br/>
                    <span><?php $_smarty_tpl->_subTemplateRender("file:./starRating.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('rating'=>$_smarty_tpl->tpl_vars['item']->value['rating_stars']), 0, true);
?></span>
                </div>
                <span style="font-size: 10px;margin-left: auto"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value['date'],"%d.%m.%Y");?>
</span>
            </div>
            <div class="carousel-item-txt col-xs-12">
                <?php echo $_smarty_tpl->tpl_vars['item']->value['text'];?>

            </div>
            <div class="col-xs-12" style="text-align: right;font-size: 10px;padding: 0; margin-top: auto;">
                <a target="_blank" style="color: #435777" href="<?php echo $_smarty_tpl->tpl_vars['shopauskunft_deep_link']->value;?>
">zum <?php echo $_smarty_tpl->tpl_vars['shopName']->value;?>
 Bewertungsprofil</a>
            </div>
            </div>
        <?php }?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
    window.addEventListener("DOMContentLoaded", function(e) {
        let stage = document.getElementById("sa-ratings");
        let knm_fadeComplete = function(e) { stage.appendChild(arr[0]); };
        let arr = stage.getElementsByClassName('sa-rating');
        for(let i=0; i < arr.length; i++) {
            arr[i].addEventListener("animationend", knm_fadeComplete, false);
        }
    }, false);
<?php echo '</script'; ?>
><?php }
}
