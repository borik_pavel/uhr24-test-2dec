<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:33
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/password_check.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f510e2f6_20205505',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a5405e17b8fa72a4c9f14f846740387be9408d37' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/password_check.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619660f510e2f6_20205505 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1067298440619660f5109401_32469534', 'snippets-password-check');
?>

<?php }
/* {block 'snippets-password-check'} */
class Block_1067298440619660f5109401_32469534 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-password-check' => 
  array (
    0 => 'Block_1067298440619660f5109401_32469534',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if ($_smarty_tpl->tpl_vars['loadScript']->value) {?>
        <?php if (empty($_smarty_tpl->tpl_vars['parentTemplateDir']->value)) {?>
            <?php $_smarty_tpl->_assignInScope('templateDir', $_smarty_tpl->tpl_vars['currentTemplateDir']->value);?>
        <?php } else { ?>
            <?php $_smarty_tpl->_assignInScope('templateDir', $_smarty_tpl->tpl_vars['parentTemplateDir']->value);?>
        <?php }?>
        <?php echo '<script'; ?>
 defer src="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['templateDir']->value;?>
js/password/password.min.js"><?php echo '</script'; ?>
>
    <?php }?>
    <?php $_block_plugin115 = isset($_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0] : null;
if (!is_callable(array($_block_plugin115, 'inlineScript'))) {
throw new SmartyException('block tag \'inline_script\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inline_script', array());
$_block_repeat=true;
echo $_block_plugin115->inlineScript(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo '<script'; ?>
>
        $(window).on('load', function () {
            $('<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
').password({
                shortPass:         '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'passwordTooShort','section'=>'login','printf'=>$_smarty_tpl->tpl_vars['Einstellungen']->value['kunden']['kundenregistrierung_passwortlaenge']),$_smarty_tpl ) );?>
',
                badPass:           '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'passwordIsWeak','section'=>'login'),$_smarty_tpl ) );?>
',
                goodPass:          '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'passwordIsMedium','section'=>'login'),$_smarty_tpl ) );?>
',
                strongPass:        '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'passwordIsStrong','section'=>'login'),$_smarty_tpl ) );?>
',
                containsField:     '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'passwordhasUsername','section'=>'login'),$_smarty_tpl ) );?>
',
                enterPass:         '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'typeYourPassword','section'=>'login'),$_smarty_tpl ) );?>
',
                showPercent:       false,
                showText:          true,
                animate:           true,
                animateSpeed:      'fast',
                field:             false,
                fieldPartialMatch: true,
                minimumLength: <?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['kunden']['kundenregistrierung_passwortlaenge'];?>

            });
        });
    <?php echo '</script'; ?>
><?php $_block_repeat=false;
echo $_block_plugin115->inlineScript(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
/* {/block 'snippets-password-check'} */
}
