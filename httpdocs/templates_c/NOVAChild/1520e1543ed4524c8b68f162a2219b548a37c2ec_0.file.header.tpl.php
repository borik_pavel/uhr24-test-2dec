<?php
/* Smarty version 3.1.39, created on 2021-11-23 11:42:05
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVAChild/productlist/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cc57d354808_08686996',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1520e1543ed4524c8b68f162a2219b548a37c2ec' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVAChild/productlist/header.tpl',
      1 => 1637664122,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619cc57d354808_08686996 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

	    
		
				 
	<?php if (strstr($_smarty_tpl->tpl_vars['AktuelleKategorie']->value->cBeschreibung,'h1')) {?>	 
		<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_516979030619cc57d344735_16857085', 'productlist-header-description-heading');
?>

	<?php }?>
	
	
			 <!-- twenty/up borik 22 november from BLACK SALE -->
            <div class="title">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['opcMountPoint'][0], array( array('id'=>'opc_before_heading'),$_smarty_tpl ) );?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_548910964619cc57d3462e6_26898390', 'productlist-header-description-heading');
?>

            </div>
           <!-- twenty/up borik 22 november from BLACK SALE -->

				 
	<?php if (call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'count_characters' ][ 0 ], array( $_smarty_tpl->tpl_vars['AktuelleKategorie']->value->cBeschreibung )) > 751) {?>
		
	    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1265840420619cc57d3496e5_10370669', 'productlist-header-description-category');
?>

		
	<?php }?>
	
	
	
	  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_242251312619cc57d34a828_23702189', 'productlist-header-description');
?>


	
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_943988839619cc57d34b307_14790429', 'productlist-header-subcategories-link');
?>


	

	<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1486888021619cc57d34f107_99150869', 'productlist-header-subcategories-list');
?>

		<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['parent_template_path']->value)."/productlist/header.tpl");
}
/* {block 'productlist-header-description-heading'} */
class Block_516979030619cc57d344735_16857085 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productlist-header-description-heading' => 
  array (
    0 => 'Block_516979030619cc57d344735_16857085',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
   
		<?php
}
}
/* {/block 'productlist-header-description-heading'} */
/* {block 'productlist-header-description-heading'} */
class Block_548910964619cc57d3462e6_26898390 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productlist-header-description-heading' => 
  array (
    0 => 'Block_548910964619cc57d3462e6_26898390',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <!-- twenty/up borik 22 november from BLACK SALE productlist header -->
                    <h1 class="h2" <?php if ($_smarty_tpl->tpl_vars['oNavigationsinfo']->value->getName() === 'SALE %') {?>style="display: inline;color: #e7131e;background-color: black;padding: 3px 10px;border-radius: 3px;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['oNavigationsinfo']->value->getName();?>
</h1>
                <?php
}
}
/* {/block 'productlist-header-description-heading'} */
/* {block 'productlist-header-description-category'} */
class Block_1265840420619cc57d3496e5_10370669 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productlist-header-description-category' => 
  array (
    0 => 'Block_1265840420619cc57d3496e5_10370669',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		
		<div class="desc clearfix mb-5 mehr-lesen">
		    <p><?php echo $_smarty_tpl->tpl_vars['oNavigationsinfo']->value->getCategory()->cBeschreibung;?>
</p>
		    <p>&nbsp;</p>
		    <p class="read-more"><a href="#" class="mehr-text">mehr lesen...</a></p>
		</div>
		
	    <?php
}
}
/* {/block 'productlist-header-description-category'} */
/* {block 'productlist-header-description'} */
class Block_242251312619cc57d34a828_23702189 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productlist-header-description' => 
  array (
    0 => 'Block_242251312619cc57d34a828_23702189',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	  

	<?php echo '<script'; ?>
 type="text/javascript">
    belboonTag = {};
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">    
    (function(d) {
      setTimeout( function() {  // Borik 16 november setTimeout added
        var s = d.createElement("script"); 
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + 
"//containertags.belboon.com/js/79749/category/script.js";
        var a = d.getElementsByTagName("script")[0];
        a.parentNode.insertBefore(s, a);
            }, 5000); // Borik 16 november setTimeout added END 
    }(document));    
<?php echo '</script'; ?>
>

	
	  <?php
}
}
/* {/block 'productlist-header-description'} */
/* {block 'productlist-header-subcategories-link'} */
class Block_943988839619cc57d34b307_14790429 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productlist-header-subcategories-link' => 
  array (
    0 => 'Block_943988839619cc57d34b307_14790429',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div class="caption">
                        <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"btn btn-outline-secondary sp20up-btn-subcategory",'href'=>$_smarty_tpl->tpl_vars['subCategory']->value->getURL()));
$_block_repeat=true;
echo $_block_plugin1->render(array('class'=>"btn btn-outline-secondary sp20up-btn-subcategory",'href'=>$_smarty_tpl->tpl_vars['subCategory']->value->getURL()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                         <!-- twenty/up borik 22 november from BLACK SALE -->
                        <?php if ($_smarty_tpl->tpl_vars['oNavigationsinfo']->value->getName() === 'SALE %') {?> <div>BLACK SALE % </div>  <?php }?> <?php echo $_smarty_tpl->tpl_vars['subCategory']->value->getName();?>

                         <!-- twenty/up borik 22 november from BLACK SALE -->
                        <?php $_block_repeat=false;
echo $_block_plugin1->render(array('class'=>"btn btn-outline-secondary sp20up-btn-subcategory",'href'=>$_smarty_tpl->tpl_vars['subCategory']->value->getURL()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    </div>
            <?php
}
}
/* {/block 'productlist-header-subcategories-link'} */
/* {block 'productlist-header-subcategories-list'} */
class Block_1486888021619cc57d34f107_99150869 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productlist-header-subcategories-list' => 
  array (
    0 => 'Block_1486888021619cc57d34f107_99150869',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		<?php if (!strstr($_smarty_tpl->tpl_vars['AktuelleKategorie']->value->cKategoriePfad,'enuhren')) {?>
			<hr class="d-none d-md-block">
			<ul class="d-none d-md-block">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subCategory']->value->getChildren(), 'subChild');
$_smarty_tpl->tpl_vars['subChild']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['subChild']->value) {
$_smarty_tpl->tpl_vars['subChild']->do_else = false;
?>
				<li>
			<?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"btn sp20up-btn-subsubcategory",'href'=>$_smarty_tpl->tpl_vars['subChild']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['subChild']->value->getName()));
$_block_repeat=true;
echo $_block_plugin2->render(array('class'=>"btn sp20up-btn-subsubcategory",'href'=>$_smarty_tpl->tpl_vars['subChild']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['subChild']->value->getName()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['subChild']->value->getName();
$_block_repeat=false;
echo $_block_plugin2->render(array('class'=>"btn sp20up-btn-subsubcategory",'href'=>$_smarty_tpl->tpl_vars['subChild']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['subChild']->value->getName()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
			</li>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
		<?php }?>	
<?php
}
}
/* {/block 'productlist-header-subcategories-list'} */
}
