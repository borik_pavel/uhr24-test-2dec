<?php
/* Smarty version 3.1.39, created on 2021-11-18 21:43:18
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/order_item.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196bae63dc598_03705412',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7d17453297c5b6a6c89a0eb3450fedf43e51bb97' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/order_item.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6196bae63dc598_03705412 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8030674106196bae6389f34_90860941', 'account-order-item');
?>

<?php }
/* {block 'account-order-item-image'} */
class Block_7526247326196bae638fb72_97815753 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php $_block_plugin39 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin39, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cURLFull,'title'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))));
$_block_repeat=true;
echo $_block_plugin39->render(array('href'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cURLFull,'title'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('webp'=>true,'fluid'=>true,'lazy'=>true,'src'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cVorschaubild,'alt'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))),$_smarty_tpl ) );?>

                                        <?php $_block_repeat=false;
echo $_block_plugin39->render(array('href'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cURLFull,'title'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php
}
}
/* {/block 'account-order-item-image'} */
/* {block 'account-order-item-link'} */
class Block_14380267776196bae6393743_08868540 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php $_block_plugin41 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin41, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cURLFull,'title'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))));
$_block_repeat=true;
echo $_block_plugin41->render(array('href'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cURLFull,'title'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ));
$_block_repeat=false;
echo $_block_plugin41->render(array('href'=>$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cURLFull,'title'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php
}
}
/* {/block 'account-order-item-link'} */
/* {block 'account-order-item-sku'} */
class Block_12464130586196bae6395274_77768724 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <li class="sku"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productNo','section'=>'global'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cArtNr;?>
</li>
                                        <?php
}
}
/* {/block 'account-order-item-sku'} */
/* {block 'account-order-item-mhd'} */
class Block_9782627196196bae6397050_10609202 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <li title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productMHDTool','section'=>'global'),$_smarty_tpl ) );?>
" class="best-before">
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productMHD','section'=>'global'),$_smarty_tpl ) );?>
:<?php echo $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->dMHD_de;?>

                                                </li>
                                            <?php
}
}
/* {/block 'account-order-item-mhd'} */
/* {block 'account-order-item-base-price'} */
class Block_11108901206196bae6399376_18558381 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <li class="baseprice"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'basePrice','section'=>'global'),$_smarty_tpl ) );?>
:<?php echo $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cLocalizedVPE[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>
</li>
                                            <?php
}
}
/* {/block 'account-order-item-base-price'} */
/* {block 'account-order-item-variations'} */
class Block_5494285436196bae639bc62_64103467 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oPosition']->value->WarenkorbPosEigenschaftArr, 'Variation');
$_smarty_tpl->tpl_vars['Variation']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Variation']->value) {
$_smarty_tpl->tpl_vars['Variation']->do_else = false;
?>
                                                    <li class="variation">
                                                        <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['Variation']->value->cEigenschaftName ));?>
: <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['Variation']->value->cEigenschaftWertName ));?>
 <?php if (!empty($_smarty_tpl->tpl_vars['Variation']->value->cAufpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value])) {?>&raquo;
                                                            <?php if (substr($_smarty_tpl->tpl_vars['Variation']->value->cAufpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value],0,1) !== '-') {?>+<?php }
echo $_smarty_tpl->tpl_vars['Variation']->value->cAufpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>
 <?php }?>
                                                    </li>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            <?php
}
}
/* {/block 'account-order-item-variations'} */
/* {block 'account-order-item-notice'} */
class Block_2169788346196bae63a11d2_98694893 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <li class="text-info notice"><?php echo $_smarty_tpl->tpl_vars['oPosition']->value->cHinweis;?>
</li>
                                            <?php
}
}
/* {/block 'account-order-item-notice'} */
/* {block 'account-order-item-manufacturer'} */
class Block_14024244306196bae63a2de7_58181120 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <li class="manufacturer">
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'manufacturer','section'=>'productDetails'),$_smarty_tpl ) );?>
:
                                                    <span class="values">
                                                       <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cHersteller;?>

                                                    </span>
                                                </li>
                                            <?php
}
}
/* {/block 'account-order-item-manufacturer'} */
/* {block 'account-order-item-characteristics'} */
class Block_10699003336196bae63a4e99_27056904 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->oMerkmale_arr, 'oMerkmale_arr');
$_smarty_tpl->tpl_vars['oMerkmale_arr']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oMerkmale_arr']->value) {
$_smarty_tpl->tpl_vars['oMerkmale_arr']->do_else = false;
?>
                                                    <li class="characteristic">
                                                        <?php echo $_smarty_tpl->tpl_vars['oMerkmale_arr']->value->cName;?>
:
                                                        <span class="values">
                                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oMerkmale_arr']->value->oMerkmalWert_arr, 'oWert');
$_smarty_tpl->tpl_vars['oWert']->index = -1;
$_smarty_tpl->tpl_vars['oWert']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oWert']->value) {
$_smarty_tpl->tpl_vars['oWert']->do_else = false;
$_smarty_tpl->tpl_vars['oWert']->index++;
$_smarty_tpl->tpl_vars['oWert']->first = !$_smarty_tpl->tpl_vars['oWert']->index;
$__foreach_oWert_12_saved = $_smarty_tpl->tpl_vars['oWert'];
?>
                                                                <?php if (!$_smarty_tpl->tpl_vars['oWert']->first) {?>, <?php }?>
                                                                <?php echo $_smarty_tpl->tpl_vars['oWert']->value->cWert;?>

                                                            <?php
$_smarty_tpl->tpl_vars['oWert'] = $__foreach_oWert_12_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                        </span>
                                                    </li>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            <?php
}
}
/* {/block 'account-order-item-characteristics'} */
/* {block 'account-order-item-attributes'} */
class Block_4983982646196bae63a8770_46472236 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->Attribute, 'oAttribute_arr');
$_smarty_tpl->tpl_vars['oAttribute_arr']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oAttribute_arr']->value) {
$_smarty_tpl->tpl_vars['oAttribute_arr']->do_else = false;
?>
                                                    <li class="attribute">
                                                        <?php echo $_smarty_tpl->tpl_vars['oAttribute_arr']->value->cName;?>
:
                                                        <span class="values">
                                                            <?php echo $_smarty_tpl->tpl_vars['oAttribute_arr']->value->cWert;?>

                                                        </span>
                                                    </li>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            <?php
}
}
/* {/block 'account-order-item-attributes'} */
/* {block 'account-order-item-short-desc'} */
class Block_16814430476196bae63abd77_94719214 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <li class="shortdescription"><?php echo $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cKurzBeschreibung;?>
</li>
                                            <?php
}
}
/* {/block 'account-order-item-short-desc'} */
/* {block 'account-order-item-delivery-status'} */
class Block_19788512196196bae63acc50_04258717 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <li class="delivery-status"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderStatus','section'=>'login'),$_smarty_tpl ) );?>
:
                                                <strong>
                                                <?php if ($_smarty_tpl->tpl_vars['oPosition']->value->bAusgeliefert) {?>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'statusShipped','section'=>'order'),$_smarty_tpl ) );?>

                                                <?php } elseif ($_smarty_tpl->tpl_vars['oPosition']->value->nAusgeliefert > 0) {?>
                                                    <?php if (strlen($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) == 0) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'statusShipped','section'=>'order'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->nAusgeliefertGesamt;
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'statusPartialShipped','section'=>'order'),$_smarty_tpl ) );
}?>
                                                <?php } else { ?>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'notShippedYet','section'=>'login'),$_smarty_tpl ) );?>

                                                <?php }?>
                                                </strong>
                                            </li>
                                        <?php
}
}
/* {/block 'account-order-item-delivery-status'} */
/* {block 'account-order-item-details'} */
class Block_3636279716196bae63933d1_08641023 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14380267776196bae6393743_08868540', 'account-order-item-link', $this->tplIndex);
?>

                                    <ul class="list-unstyled text-muted-util small item-detail-list">
                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12464130586196bae6395274_77768724', 'account-order-item-sku', $this->tplIndex);
?>

                                        <?php if ((isset($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->dMHD) && isset($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->dMHD_de))) {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9782627196196bae6397050_10609202', 'account-order-item-mhd', $this->tplIndex);
?>

                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cLocalizedVPE && $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cVPE !== 'N') {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11108901206196bae6399376_18558381', 'account-order-item-base-price', $this->tplIndex);
?>

                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['warenkorb_varianten_varikombi_anzeigen'] === 'Y' && (isset($_smarty_tpl->tpl_vars['oPosition']->value->WarenkorbPosEigenschaftArr)) && !empty($_smarty_tpl->tpl_vars['oPosition']->value->WarenkorbPosEigenschaftArr)) {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5494285436196bae639bc62_64103467', 'account-order-item-variations', $this->tplIndex);
?>

                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['oPosition']->value->cHinweis)) {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2169788346196bae63a11d2_98694893', 'account-order-item-notice', $this->tplIndex);
?>

                                        <?php }?>

                                                                                <?php if ($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cHersteller && $_smarty_tpl->tpl_vars['Einstellungen']->value['artikeldetails']['artikeldetails_hersteller_anzeigen'] != "N") {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14024244306196bae63a2de7_58181120', 'account-order-item-manufacturer', $this->tplIndex);
?>

                                        <?php }?>

                                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_artikelmerkmale'] == 'Y' && !empty($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->oMerkmale_arr)) {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10699003336196bae63a4e99_27056904', 'account-order-item-characteristics', $this->tplIndex);
?>

                                        <?php }?>

                                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_artikelattribute'] == 'Y' && !empty($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->Attribute)) {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4983982646196bae63a8770_46472236', 'account-order-item-attributes', $this->tplIndex);
?>

                                        <?php }?>

                                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_artikelkurzbeschreibung'] == 'Y' && strlen($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cKurzBeschreibung) > 0) {?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16814430476196bae63abd77_94719214', 'account-order-item-short-desc', $this->tplIndex);
?>

                                        <?php }?>
                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19788512196196bae63acc50_04258717', 'account-order-item-delivery-status', $this->tplIndex);
?>

                                    </ul>
                                <?php
}
}
/* {/block 'account-order-item-details'} */
/* {block 'account-order-item-details-not-product'} */
class Block_1743510726196bae63b1048_91477033 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cName ));
if ((isset($_smarty_tpl->tpl_vars['oPosition']->value->discountForArticle))) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->discountForArticle ));
}?>
                                    <?php if ((isset($_smarty_tpl->tpl_vars['oPosition']->value->cArticleNameAffix))) {?>
                                        <?php if (is_array($_smarty_tpl->tpl_vars['oPosition']->value->cArticleNameAffix)) {?>
                                            <ul class="small text-muted-util">
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oPosition']->value->cArticleNameAffix, 'cArticleNameAffix');
$_smarty_tpl->tpl_vars['cArticleNameAffix']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cArticleNameAffix']->value) {
$_smarty_tpl->tpl_vars['cArticleNameAffix']->do_else = false;
?>
                                                    <li><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['cArticleNameAffix']->value ));?>
</li>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </ul>
                                        <?php } else { ?>
                                            <ul class="small text-muted-util">
                                                <li><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->cArticleNameAffix ));?>
</li>
                                            </ul>
                                        <?php }?>
                                    <?php }?>
                                    <?php if (!empty($_smarty_tpl->tpl_vars['oPosition']->value->cHinweis)) {?>
                                        <small class="text-info notice"><?php echo $_smarty_tpl->tpl_vars['oPosition']->value->cHinweis;?>
</small>
                                    <?php }?>
                                <?php
}
}
/* {/block 'account-order-item-details-not-product'} */
/* {block 'account-order-item-config-items'} */
class Block_3250610706196bae63b7af3_36867088 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <ul class="config-items text-muted-util small">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Positionen, 'KonfigPos');
$_smarty_tpl->tpl_vars['KonfigPos']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['KonfigPos']->value) {
$_smarty_tpl->tpl_vars['KonfigPos']->do_else = false;
?>
                                            <?php if ($_smarty_tpl->tpl_vars['oPosition']->value->cUnique == $_smarty_tpl->tpl_vars['KonfigPos']->value->cUnique && $_smarty_tpl->tpl_vars['KonfigPos']->value->kKonfigitem > 0) {?>
                                                <li>
                                                    <span class="qty"><?php if (!(is_string($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && !empty($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && (int)$_smarty_tpl->tpl_vars['oPosition']->value->kKonfigitem === 0)) {
echo $_smarty_tpl->tpl_vars['KonfigPos']->value->nAnzahlEinzel;
} else { ?>1<?php }?>x</span>
                                                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'trans' ][ 0 ], array( $_smarty_tpl->tpl_vars['KonfigPos']->value->cName ));?>
 &raquo;<br/>
                                                    <span class="price_value">
                                                        <?php if (substr($_smarty_tpl->tpl_vars['KonfigPos']->value->cEinzelpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value],0,1) !== '-') {?>+<?php }
echo $_smarty_tpl->tpl_vars['KonfigPos']->value->cEinzelpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

                                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'pricePerUnit','section'=>'checkout'),$_smarty_tpl ) );?>

                                                    </span>
                                                </li>
                                            <?php }?>
                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </ul>
                                <?php
}
}
/* {/block 'account-order-item-config-items'} */
/* {block 'account-order-item-items-data'} */
class Block_274844266196bae638d485_35875467 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php ob_start();
if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_einzelpreise_anzeigen'] === 'Y') {
echo "6";
} else {
echo "8";
}
$_prefixVariable9=ob_get_clean();
$_block_plugin36 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin36, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>$_prefixVariable9));
$_block_repeat=true;
echo $_block_plugin36->render(array('cols'=>12,'md'=>$_prefixVariable9), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin37 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin37, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin37->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin38 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin38, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>3,'md'=>4,'class'=>'order-item-image-wrapper'));
$_block_repeat=true;
echo $_block_plugin38->render(array('cols'=>3,'md'=>4,'class'=>'order-item-image-wrapper'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php if (!empty($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cVorschaubild)) {?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7526247326196bae638fb72_97815753', 'account-order-item-image', $this->tplIndex);
?>

                                <?php }?>
                            <?php $_block_repeat=false;
echo $_block_plugin38->render(array('cols'=>3,'md'=>4,'class'=>'order-item-image-wrapper'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin40 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin40, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin40->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php if ($_smarty_tpl->tpl_vars['oPosition']->value->nPosTyp == (defined('C_WARENKORBPOS_TYP_ARTIKEL') ? constant('C_WARENKORBPOS_TYP_ARTIKEL') : null)) {?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3636279716196bae63933d1_08641023', 'account-order-item-details', $this->tplIndex);
?>

                            <?php } else { ?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1743510726196bae63b1048_91477033', 'account-order-item-details-not-product', $this->tplIndex);
?>

                            <?php }?>

                            <?php if (is_string($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && !empty($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && (int)$_smarty_tpl->tpl_vars['oPosition']->value->kKonfigitem === 0) {?>                                 <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3250610706196bae63b7af3_36867088', 'account-order-item-config-items', $this->tplIndex);
?>

                            <?php }?>
                            <?php $_block_repeat=false;
echo $_block_plugin40->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin37->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin36->render(array('cols'=>12,'md'=>$_prefixVariable9), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'account-order-item-items-data'} */
/* {block 'account-order-item-price-qty'} */
class Block_12538728676196bae63be4b8_94721448 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin42 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin42, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>'qty-col text-right-util','md'=>2,'cols'=>6));
$_block_repeat=true;
echo $_block_plugin42->render(array('class'=>'qty-col text-right-util','md'=>2,'cols'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'replace_delim' ][ 0 ], array( $_smarty_tpl->tpl_vars['oPosition']->value->nAnzahl ));?>
 <?php if (!empty($_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cEinheit)) {
if (preg_match("/(\d)/",$_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cEinheit)) {?> x<?php }?> <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->Artikel->cEinheit;?>
 <?php }?>
                            <?php $_block_repeat=false;
echo $_block_plugin42->render(array('class'=>'qty-col text-right-util','md'=>2,'cols'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'account-order-item-price-qty'} */
/* {block 'account-order-item-price-single-price'} */
class Block_14438548566196bae63c1fc3_72854478 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin43 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin43, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>'price-col hidden-xs text-nowrap-util','md'=>2,'cols'=>3));
$_block_repeat=true;
echo $_block_plugin43->render(array('class'=>'price-col hidden-xs text-nowrap-util','md'=>2,'cols'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php if ($_smarty_tpl->tpl_vars['oPosition']->value->nPosTyp == (defined('C_WARENKORBPOS_TYP_ARTIKEL') ? constant('C_WARENKORBPOS_TYP_ARTIKEL') : null)) {?>
                                        <?php if (!(is_string($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && !empty($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && (int)$_smarty_tpl->tpl_vars['oPosition']->value->kKonfigitem === 0)) {?>                                             <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->cEinzelpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

                                        <?php } else { ?>
                                            <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->cKonfigeinzelpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

                                        <?php }?>
                                    <?php }?>
                                <?php $_block_repeat=false;
echo $_block_plugin43->render(array('class'=>'price-col hidden-xs text-nowrap-util','md'=>2,'cols'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-order-item-price-single-price'} */
/* {block 'account-order-item-price-overall'} */
class Block_7585123856196bae63c65a7_09574924 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin44 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin44, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>'price-col text-nowrap-util','md'=>2,'cols'=>3));
$_block_repeat=true;
echo $_block_plugin44->render(array('class'=>'price-col text-nowrap-util','md'=>2,'cols'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <strong class="price_overall">
                                    <?php if (is_string($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && !empty($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && (int)$_smarty_tpl->tpl_vars['oPosition']->value->kKonfigitem === 0) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->cKonfigpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

                                    <?php } else { ?>
                                        <?php echo $_smarty_tpl->tpl_vars['oPosition']->value->cGesamtpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

                                    <?php }?>
                                </strong>
                            <?php $_block_repeat=false;
echo $_block_plugin44->render(array('class'=>'price-col text-nowrap-util','md'=>2,'cols'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'account-order-item-price-overall'} */
/* {block 'account-order-item-price'} */
class Block_10059169896196bae63be128_77377176 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12538728676196bae63be4b8_94721448', 'account-order-item-price-qty', $this->tplIndex);
?>

                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_einzelpreise_anzeigen'] === 'Y') {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14438548566196bae63c1fc3_72854478', 'account-order-item-price-single-price', $this->tplIndex);
?>

                        <?php }?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7585123856196bae63c65a7_09574924', 'account-order-item-price-overall', $this->tplIndex);
?>

                    <?php
}
}
/* {/block 'account-order-item-price'} */
/* {block 'account-order-item-last-hr'} */
class Block_13723226096196bae63c9a66_38680140 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <hr>
                <?php
}
}
/* {/block 'account-order-item-last-hr'} */
/* {block 'account-order-item-items'} */
class Block_896871566196bae638a445_62430476 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Positionen, 'oPosition');
$_smarty_tpl->tpl_vars['oPosition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oPosition']->value) {
$_smarty_tpl->tpl_vars['oPosition']->do_else = false;
?>
            <?php if (!(is_string($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && !empty($_smarty_tpl->tpl_vars['oPosition']->value->cUnique) && (int)$_smarty_tpl->tpl_vars['oPosition']->value->kKonfigitem > 0)) {?>                 <?php $_block_plugin35 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin35, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"type-".((string)$_smarty_tpl->tpl_vars['oPosition']->value->nPosTyp)." order-item"));
$_block_repeat=true;
echo $_block_plugin35->render(array('class'=>"type-".((string)$_smarty_tpl->tpl_vars['oPosition']->value->nPosTyp)." order-item"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_274844266196bae638d485_35875467', 'account-order-item-items-data', $this->tplIndex);
?>


                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10059169896196bae63be128_77377176', 'account-order-item-price', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin35->render(array('class'=>"type-".((string)$_smarty_tpl->tpl_vars['oPosition']->value->nPosTyp)." order-item"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13723226096196bae63c9a66_38680140', 'account-order-item-last-hr', $this->tplIndex);
?>

            <?php }?>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php
}
}
/* {/block 'account-order-item-items'} */
/* {block 'account-order-items-total-price-net'} */
class Block_12984881816196bae63cb3d6_28425553 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin47 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin47, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"total-net"));
$_block_repeat=true;
echo $_block_plugin47->render(array('class'=>"total-net"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin48 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin48, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin48->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <span class="price_label"><strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'totalSum'),$_smarty_tpl ) );?>
 (<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'net'),$_smarty_tpl ) );?>
):</strong></span>
                                <?php $_block_repeat=false;
echo $_block_plugin48->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin49 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin49, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto price-col"));
$_block_repeat=true;
echo $_block_plugin49->render(array('class'=>"col-auto price-col"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <strong class="price total-sum"><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[1];?>
</strong>
                                <?php $_block_repeat=false;
echo $_block_plugin49->render(array('class'=>"col-auto price-col"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin47->render(array('class'=>"total-net"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'account-order-items-total-price-net'} */
/* {block 'account-order-items-total-customer-credit'} */
class Block_7939116696196bae63ce2f5_89891121 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin50 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin50, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"customer-credit"));
$_block_repeat=true;
echo $_block_plugin50->render(array('class'=>"customer-credit"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin51 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin51, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin51->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'useCredit','section'=>'account data'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin51->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin52 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin52, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto ml-auto-util text-right-util"));
$_block_repeat=true;
echo $_block_plugin52->render(array('class'=>"col-auto ml-auto-util text-right-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->GutscheinLocalized;?>

                                <?php $_block_repeat=false;
echo $_block_plugin52->render(array('class'=>"col-auto ml-auto-util text-right-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin50->render(array('class'=>"customer-credit"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'account-order-items-total-customer-credit'} */
/* {block 'account-order-items-total-tax'} */
class Block_10895714196196bae63d0a29_78982554 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Steuerpositionen, 'taxPosition');
$_smarty_tpl->tpl_vars['taxPosition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['taxPosition']->value) {
$_smarty_tpl->tpl_vars['taxPosition']->do_else = false;
?>
                                <?php $_block_plugin53 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin53, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"tax"));
$_block_repeat=true;
echo $_block_plugin53->render(array('class'=>"tax"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin54 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin54, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin54->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <span class="tax_label"><?php echo $_smarty_tpl->tpl_vars['taxPosition']->value->cName;?>
:</span>
                                    <?php $_block_repeat=false;
echo $_block_plugin54->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin55 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin55, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto price-col"));
$_block_repeat=true;
echo $_block_plugin55->render(array('class'=>"col-auto price-col"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <span class="tax_label"><?php echo $_smarty_tpl->tpl_vars['taxPosition']->value->cPreisLocalized;?>
</span>
                                    <?php $_block_repeat=false;
echo $_block_plugin55->render(array('class'=>"col-auto price-col"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin53->render(array('class'=>"tax"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <?php
}
}
/* {/block 'account-order-items-total-tax'} */
/* {block 'account-order-items-total-total'} */
class Block_11772707296196bae63d3c75_78704696 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <hr>
                        <?php $_block_plugin56 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin56, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin56->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin57 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin57, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin57->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <span class="price_label"><strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'totalSum'),$_smarty_tpl ) );?>
 <?php if ($_smarty_tpl->tpl_vars['NettoPreise']->value) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'gross','section'=>'global'),$_smarty_tpl ) );
}?>:</strong></span>
                            <?php $_block_repeat=false;
echo $_block_plugin57->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin58 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin58, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto price-col"));
$_block_repeat=true;
echo $_block_plugin58->render(array('class'=>"col-auto price-col"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <strong class="price total-sum"><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>
</strong>
                            <?php $_block_repeat=false;
echo $_block_plugin58->render(array('class'=>"col-auto price-col"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin56->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'account-order-items-total-total'} */
/* {block 'account-order-items-finance-costs'} */
class Block_8852653166196bae63d8b90_97378997 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <?php $_block_plugin60 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin60, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin60->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'financeCosts','section'=>'order'),$_smarty_tpl ) );?>

                                            <?php $_block_repeat=false;
echo $_block_plugin60->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php
}
}
/* {/block 'account-order-items-finance-costs'} */
/* {block 'account-order-items-finance-costs-value'} */
class Block_7853653036196bae63d99a6_96412975 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <?php $_block_plugin61 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin61, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto price-col"));
$_block_repeat=true;
echo $_block_plugin61->render(array('class'=>"col-auto price-col"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <strong class="price_overall">
                                                    <?php echo $_smarty_tpl->tpl_vars['attribute']->value->cValue;?>

                                                </strong>
                                            <?php $_block_repeat=false;
echo $_block_plugin61->render(array('class'=>"col-auto price-col"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php
}
}
/* {/block 'account-order-items-finance-costs-value'} */
/* {block 'account-order-items-total-order-attributes'} */
class Block_6365292446196bae63d6c13_49895749 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->OrderAttributes, 'attribute');
$_smarty_tpl->tpl_vars['attribute']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['attribute']->value) {
$_smarty_tpl->tpl_vars['attribute']->do_else = false;
?>
                                <?php if ($_smarty_tpl->tpl_vars['attribute']->value->cName === 'Finanzierungskosten') {?>
                                    <?php $_block_plugin59 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin59, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"type-".((string)(defined('C_WARENKORBPOS_TYP_ZINSAUFSCHLAG') ? constant('C_WARENKORBPOS_TYP_ZINSAUFSCHLAG') : null))));
$_block_repeat=true;
echo $_block_plugin59->render(array('class'=>"type-".((string)(defined('C_WARENKORBPOS_TYP_ZINSAUFSCHLAG') ? constant('C_WARENKORBPOS_TYP_ZINSAUFSCHLAG') : null))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8852653166196bae63d8b90_97378997', 'account-order-items-finance-costs', $this->tplIndex);
?>

                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7853653036196bae63d99a6_96412975', 'account-order-items-finance-costs-value', $this->tplIndex);
?>

                                    <?php $_block_repeat=false;
echo $_block_plugin59->render(array('class'=>"type-".((string)(defined('C_WARENKORBPOS_TYP_ZINSAUFSCHLAG') ? constant('C_WARENKORBPOS_TYP_ZINSAUFSCHLAG') : null))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php }?>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        <?php
}
}
/* {/block 'account-order-items-total-order-attributes'} */
/* {block 'account-order-items-total'} */
class Block_21306370106196bae63cae43_83441760 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php if ($_smarty_tpl->tpl_vars['NettoPreise']->value) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12984881816196bae63cb3d6_28425553', 'account-order-items-total-price-net', $this->tplIndex);
?>

                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->GuthabenNutzen == 1) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7939116696196bae63ce2f5_89891121', 'account-order-items-total-customer-credit', $this->tplIndex);
?>

                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_steuerpos_anzeigen'] !== 'N') {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10895714196196bae63d0a29_78982554', 'account-order-items-total-tax', $this->tplIndex);
?>

                    <?php }?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11772707296196bae63d3c75_78704696', 'account-order-items-total-total', $this->tplIndex);
?>

                    <?php if (!empty($_smarty_tpl->tpl_vars['Bestellung']->value->OrderAttributes)) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6365292446196bae63d6c13_49895749', 'account-order-items-total-order-attributes', $this->tplIndex);
?>

                    <?php }?>
                <?php
}
}
/* {/block 'account-order-items-total'} */
/* {block 'account-order-items-total-wrapper'} */
class Block_6020654796196bae63ca4b2_70348547 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_block_plugin45 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin45, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin45->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php $_block_plugin46 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin46, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('xl'=>5,'md'=>6,'class'=>'order-items-total'));
$_block_repeat=true;
echo $_block_plugin46->render(array('xl'=>5,'md'=>6,'class'=>'order-items-total'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21306370106196bae63cae43_83441760', 'account-order-items-total', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin46->render(array('xl'=>5,'md'=>6,'class'=>'order-items-total'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_repeat=false;
echo $_block_plugin45->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'account-order-items-total-wrapper'} */
/* {block 'account-order-item'} */
class Block_8030674106196bae6389f34_90860941 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'account-order-item' => 
  array (
    0 => 'Block_8030674106196bae6389f34_90860941',
  ),
  'account-order-item-items' => 
  array (
    0 => 'Block_896871566196bae638a445_62430476',
  ),
  'account-order-item-items-data' => 
  array (
    0 => 'Block_274844266196bae638d485_35875467',
  ),
  'account-order-item-image' => 
  array (
    0 => 'Block_7526247326196bae638fb72_97815753',
  ),
  'account-order-item-details' => 
  array (
    0 => 'Block_3636279716196bae63933d1_08641023',
  ),
  'account-order-item-link' => 
  array (
    0 => 'Block_14380267776196bae6393743_08868540',
  ),
  'account-order-item-sku' => 
  array (
    0 => 'Block_12464130586196bae6395274_77768724',
  ),
  'account-order-item-mhd' => 
  array (
    0 => 'Block_9782627196196bae6397050_10609202',
  ),
  'account-order-item-base-price' => 
  array (
    0 => 'Block_11108901206196bae6399376_18558381',
  ),
  'account-order-item-variations' => 
  array (
    0 => 'Block_5494285436196bae639bc62_64103467',
  ),
  'account-order-item-notice' => 
  array (
    0 => 'Block_2169788346196bae63a11d2_98694893',
  ),
  'account-order-item-manufacturer' => 
  array (
    0 => 'Block_14024244306196bae63a2de7_58181120',
  ),
  'account-order-item-characteristics' => 
  array (
    0 => 'Block_10699003336196bae63a4e99_27056904',
  ),
  'account-order-item-attributes' => 
  array (
    0 => 'Block_4983982646196bae63a8770_46472236',
  ),
  'account-order-item-short-desc' => 
  array (
    0 => 'Block_16814430476196bae63abd77_94719214',
  ),
  'account-order-item-delivery-status' => 
  array (
    0 => 'Block_19788512196196bae63acc50_04258717',
  ),
  'account-order-item-details-not-product' => 
  array (
    0 => 'Block_1743510726196bae63b1048_91477033',
  ),
  'account-order-item-config-items' => 
  array (
    0 => 'Block_3250610706196bae63b7af3_36867088',
  ),
  'account-order-item-price' => 
  array (
    0 => 'Block_10059169896196bae63be128_77377176',
  ),
  'account-order-item-price-qty' => 
  array (
    0 => 'Block_12538728676196bae63be4b8_94721448',
  ),
  'account-order-item-price-single-price' => 
  array (
    0 => 'Block_14438548566196bae63c1fc3_72854478',
  ),
  'account-order-item-price-overall' => 
  array (
    0 => 'Block_7585123856196bae63c65a7_09574924',
  ),
  'account-order-item-last-hr' => 
  array (
    0 => 'Block_13723226096196bae63c9a66_38680140',
  ),
  'account-order-items-total-wrapper' => 
  array (
    0 => 'Block_6020654796196bae63ca4b2_70348547',
  ),
  'account-order-items-total' => 
  array (
    0 => 'Block_21306370106196bae63cae43_83441760',
  ),
  'account-order-items-total-price-net' => 
  array (
    0 => 'Block_12984881816196bae63cb3d6_28425553',
  ),
  'account-order-items-total-customer-credit' => 
  array (
    0 => 'Block_7939116696196bae63ce2f5_89891121',
  ),
  'account-order-items-total-tax' => 
  array (
    0 => 'Block_10895714196196bae63d0a29_78982554',
  ),
  'account-order-items-total-total' => 
  array (
    0 => 'Block_11772707296196bae63d3c75_78704696',
  ),
  'account-order-items-total-order-attributes' => 
  array (
    0 => 'Block_6365292446196bae63d6c13_49895749',
  ),
  'account-order-items-finance-costs' => 
  array (
    0 => 'Block_8852653166196bae63d8b90_97378997',
  ),
  'account-order-items-finance-costs-value' => 
  array (
    0 => 'Block_7853653036196bae63d99a6_96412975',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="order-items card-table">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_896871566196bae638a445_62430476', 'account-order-item-items', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6020654796196bae63ca4b2_70348547', 'account-order-items-total-wrapper', $this->tplIndex);
?>

    </div>
<?php
}
}
/* {/block 'account-order-item'} */
}
