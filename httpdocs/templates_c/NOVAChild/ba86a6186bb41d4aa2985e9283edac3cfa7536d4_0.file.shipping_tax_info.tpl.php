<?php
/* Smarty version 3.1.39, created on 2021-11-18 11:08:07
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/shipping_tax_info.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619626071e72c4_17764946',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba86a6186bb41d4aa2985e9283edac3cfa7536d4' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/shipping_tax_info.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619626071e72c4_17764946 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_981491993619626071cbaa0_60717260', 'snippets-shipping-tax-info');
?>

<?php }
/* {block 'snippets-shipping-tax-info-tax-data'} */
class Block_195635965619626071cc344_44237847 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php if (!empty($_smarty_tpl->tpl_vars['taxdata']->value['text'])) {
echo $_smarty_tpl->tpl_vars['taxdata']->value['text'];
} else {
if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_ust_auszeichnung'] === 'auto') {
if ($_smarty_tpl->tpl_vars['taxdata']->value['net']) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'excl','section'=>'productDetails'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'incl','section'=>'productDetails'),$_smarty_tpl ) );
}?>&nbsp;<?php echo $_smarty_tpl->tpl_vars['taxdata']->value['tax'];?>
% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'vat','section'=>'productDetails'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_ust_auszeichnung'] === 'autoNoVat') {
if ($_smarty_tpl->tpl_vars['taxdata']->value['net']) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'excl','section'=>'productDetails'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'incl','section'=>'productDetails'),$_smarty_tpl ) );
}?>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'vat','section'=>'productDetails'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_ust_auszeichnung'] === 'endpreis') {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'finalprice','section'=>'productDetails'),$_smarty_tpl ) );
}
}?>
        <?php
}
}
/* {/block 'snippets-shipping-tax-info-tax-data'} */
/* {block 'snippets-shipping-tax-info-zzgl-comma'} */
class Block_1833384861619626071d31a0_53770553 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                ,
                <?php
}
}
/* {/block 'snippets-shipping-tax-info-zzgl-comma'} */
/* {block 'snippets-shipping-tax-info-zzgl-show-shipping-free-D'} */
class Block_1691939132619626071d4b94_80360525 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php ob_start();
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['taxdata']->value['countries'], 'country', true, 'cISO');
$_smarty_tpl->tpl_vars['country']->iteration = 0;
$_smarty_tpl->tpl_vars['country']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cISO']->value => $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->do_else = false;
$_smarty_tpl->tpl_vars['country']->iteration++;
$_smarty_tpl->tpl_vars['country']->last = $_smarty_tpl->tpl_vars['country']->iteration === $_smarty_tpl->tpl_vars['country']->total;
$__foreach_country_22_saved = $_smarty_tpl->tpl_vars['country'];
echo "<abbr title='";
echo (string)$_smarty_tpl->tpl_vars['country']->value;
echo "'>";
echo (string)$_smarty_tpl->tpl_vars['cISO']->value;
echo "</abbr>";
if (!$_smarty_tpl->tpl_vars['country']->last) {
echo "&nbsp;";
}
$_smarty_tpl->tpl_vars['country'] = $__foreach_country_22_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
$_prefixVariable26=ob_get_clean();
$_smarty_tpl->_assignInScope('countries', $_prefixVariable26);?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noShippingcostsTo'),$_smarty_tpl ) );?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noShippingCostsAtExtended','section'=>'basket','printf'=>$_smarty_tpl->tpl_vars['countries']->value),$_smarty_tpl ) );?>

                                <?php ob_start();
if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]))) {
echo (string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL();
echo "?shipping_calculator=0";
}
$_prefixVariable27=ob_get_clean();
$_block_plugin41 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin41, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable27,'rel'=>"nofollow",'class'=>"shipment popup"));
$_block_repeat=true;
echo $_block_plugin41->render(array('href'=>$_prefixVariable27,'rel'=>"nofollow",'class'=>"shipment popup"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shipping','section'=>'basket'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin41->render(array('href'=>$_prefixVariable27,'rel'=>"nofollow",'class'=>"shipment popup"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'snippets-shipping-tax-info-zzgl-show-shipping-free-D'} */
/* {block 'snippets-shipping-tax-info-zzgl-show-shipping-free-free-link'} */
class Block_932900295619626071db479_10682197 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'else'),$_smarty_tpl ) );
$_prefixVariable28=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'plus','section'=>'basket'),$_smarty_tpl ) );
$_prefixVariable29=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shipping','section'=>'basket'),$_smarty_tpl ) );
$_prefixVariable30=ob_get_clean();
$_block_plugin42 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin42, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>((string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL())."?shipping_calculator=0",'rel'=>"nofollow",'class'=>"shipment popup",'data-toggle'=>"tooltip",'data-placement'=>"left",'title'=>((string)$_smarty_tpl->tpl_vars['taxdata']->value['shippingFreeCountries']).", ".$_prefixVariable28." ".$_prefixVariable29." ".$_prefixVariable30));
$_block_repeat=true;
echo $_block_plugin42->render(array('href'=>((string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL())."?shipping_calculator=0",'rel'=>"nofollow",'class'=>"shipment popup",'data-toggle'=>"tooltip",'data-placement'=>"left",'title'=>((string)$_smarty_tpl->tpl_vars['taxdata']->value['shippingFreeCountries']).", ".$_prefixVariable28." ".$_prefixVariable29." ".$_prefixVariable30), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noShippingcostsTo'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin42->render(array('href'=>((string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL())."?shipping_calculator=0",'rel'=>"nofollow",'class'=>"shipment popup",'data-toggle'=>"tooltip",'data-placement'=>"left",'title'=>((string)$_smarty_tpl->tpl_vars['taxdata']->value['shippingFreeCountries']).", ".$_prefixVariable28." ".$_prefixVariable29." ".$_prefixVariable30), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'snippets-shipping-tax-info-zzgl-show-shipping-free-free-link'} */
/* {block 'snippets-shipping-tax-info-zzgl-show-shipping-free'} */
class Block_1607341578619626071d41e4_48012168 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandkostenfrei_darstellung'] === 'D') {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1691939132619626071d4b94_80360525', 'snippets-shipping-tax-info-zzgl-show-shipping-free-D', $this->tplIndex);
?>

                        <?php } elseif ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]))) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_932900295619626071db479_10682197', 'snippets-shipping-tax-info-zzgl-show-shipping-free-free-link', $this->tplIndex);
?>

                        <?php }?>
                    <?php
}
}
/* {/block 'snippets-shipping-tax-info-zzgl-show-shipping-free'} */
/* {block 'snippets-shipping-tax-info-zzgl-special-page'} */
class Block_1336398413619626071df6e9_59844746 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'plus','section'=>'basket'),$_smarty_tpl ) );?>

                        <?php $_block_plugin43 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin43, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>((string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL())."?shipping_calculator=0",'rel'=>"nofollow",'class'=>"shipment popup"));
$_block_repeat=true;
echo $_block_plugin43->render(array('href'=>((string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL())."?shipping_calculator=0",'rel'=>"nofollow",'class'=>"shipment popup"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shipping','section'=>'basket'),$_smarty_tpl ) );?>

                        <?php $_block_repeat=false;
echo $_block_plugin43->render(array('href'=>((string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL())."?shipping_calculator=0",'rel'=>"nofollow",'class'=>"shipment popup"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'snippets-shipping-tax-info-zzgl-special-page'} */
/* {block 'snippets-shipping-tax-info-zzgl'} */
class Block_534221875619626071d2e47_17188576 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1833384861619626071d31a0_53770553', 'snippets-shipping-tax-info-zzgl-comma', $this->tplIndex);
?>

                <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandfrei_anzeigen'] === 'Y' && $_smarty_tpl->tpl_vars['taxdata']->value['shippingFreeCountries']) {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1607341578619626071d41e4_48012168', 'snippets-shipping-tax-info-zzgl-show-shipping-free', $this->tplIndex);
?>

                <?php } elseif ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]))) {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1336398413619626071df6e9_59844746', 'snippets-shipping-tax-info-zzgl-special-page', $this->tplIndex);
?>

                <?php }?>
            <?php
}
}
/* {/block 'snippets-shipping-tax-info-zzgl'} */
/* {block 'snippets-shipping-tax-info-inkl'} */
class Block_840618849619626071e2283_64269352 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                , <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'incl','section'=>'productDetails'),$_smarty_tpl ) );?>
 <?php ob_start();
if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]))) {
echo (string)$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL();
}
$_prefixVariable31=ob_get_clean();
$_block_plugin44 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin44, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable31,'rel'=>"nofollow",'class'=>"shipment"));
$_block_repeat=true;
echo $_block_plugin44->render(array('href'=>$_prefixVariable31,'rel'=>"nofollow",'class'=>"shipment"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shipping','section'=>'basket'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin44->render(array('href'=>$_prefixVariable31,'rel'=>"nofollow",'class'=>"shipment"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'snippets-shipping-tax-info-inkl'} */
/* {block 'snippets-shipping-tax-info-content'} */
class Block_1838176644619626071cbfc0_61442043 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_195635965619626071cc344_44237847', 'snippets-shipping-tax-info-tax-data', $this->tplIndex);
?>

        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandhinweis'] === 'zzgl') {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_534221875619626071d2e47_17188576', 'snippets-shipping-tax-info-zzgl', $this->tplIndex);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandhinweis'] === 'inkl') {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_840618849619626071e2283_64269352', 'snippets-shipping-tax-info-inkl', $this->tplIndex);
?>

        <?php }?>
    <?php
}
}
/* {/block 'snippets-shipping-tax-info-content'} */
/* {block 'snippets-shipping-tax-info-shipping-class'} */
class Block_2116055539619626071e5153_22276956 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (!empty($_smarty_tpl->tpl_vars['taxdata']->value['shippingClass']) && $_smarty_tpl->tpl_vars['taxdata']->value['shippingClass'] !== 'standard' && $_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandklasse_anzeigen'] === 'Y') {?>
            (<?php echo $_smarty_tpl->tpl_vars['taxdata']->value['shippingClass'];?>
)
        <?php }?>
    <?php
}
}
/* {/block 'snippets-shipping-tax-info-shipping-class'} */
/* {block 'snippets-shipping-tax-info'} */
class Block_981491993619626071cbaa0_60717260 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-shipping-tax-info' => 
  array (
    0 => 'Block_981491993619626071cbaa0_60717260',
  ),
  'snippets-shipping-tax-info-content' => 
  array (
    0 => 'Block_1838176644619626071cbfc0_61442043',
  ),
  'snippets-shipping-tax-info-tax-data' => 
  array (
    0 => 'Block_195635965619626071cc344_44237847',
  ),
  'snippets-shipping-tax-info-zzgl' => 
  array (
    0 => 'Block_534221875619626071d2e47_17188576',
  ),
  'snippets-shipping-tax-info-zzgl-comma' => 
  array (
    0 => 'Block_1833384861619626071d31a0_53770553',
  ),
  'snippets-shipping-tax-info-zzgl-show-shipping-free' => 
  array (
    0 => 'Block_1607341578619626071d41e4_48012168',
  ),
  'snippets-shipping-tax-info-zzgl-show-shipping-free-D' => 
  array (
    0 => 'Block_1691939132619626071d4b94_80360525',
  ),
  'snippets-shipping-tax-info-zzgl-show-shipping-free-free-link' => 
  array (
    0 => 'Block_932900295619626071db479_10682197',
  ),
  'snippets-shipping-tax-info-zzgl-special-page' => 
  array (
    0 => 'Block_1336398413619626071df6e9_59844746',
  ),
  'snippets-shipping-tax-info-inkl' => 
  array (
    0 => 'Block_840618849619626071e2283_64269352',
  ),
  'snippets-shipping-tax-info-shipping-class' => 
  array (
    0 => 'Block_2116055539619626071e5153_22276956',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1838176644619626071cbfc0_61442043', 'snippets-shipping-tax-info-content', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2116055539619626071e5153_22276956', 'snippets-shipping-tax-info-shipping-class', $this->tplIndex);
?>

<?php
}
}
/* {/block 'snippets-shipping-tax-info'} */
}
