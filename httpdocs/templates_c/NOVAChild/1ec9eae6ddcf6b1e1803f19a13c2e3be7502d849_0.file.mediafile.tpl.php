<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:33:32
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/productdetails/mediafile.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196643cbda566_73401148',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ec9eae6ddcf6b1e1803f19a13c2e3be7502d849' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/productdetails/mediafile.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:productdetails/mediafile_youtube_embed.tpl' => 1,
  ),
),false)) {
function content_6196643cbda566_73401148 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4362352586196643cb6d5d3_96967597', 'productdetails-mediafile');
?>

<?php }
/* {block 'productdetails-mediafilealert'} */
class Block_801923196196643cb7da83_72927154 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"mediafiles-no-media"));
$_block_repeat=true;
echo $_block_plugin1->render(array('class'=>"mediafiles-no-media"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"danger"));
$_block_repeat=true;
echo $_block_plugin2->render(array('variant'=>"danger"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noMediaFile','section'=>'errorMessages'),$_smarty_tpl ) );?>

                            <?php $_block_repeat=false;
echo $_block_plugin2->render(array('variant'=>"danger"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin1->render(array('class'=>"mediafiles-no-media"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'productdetails-mediafilealert'} */
/* {block 'productdetails-mediafile-images'} */
class Block_11933174736196643cb8acc4_44008804 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_smarty_tpl->_assignInScope('cMediaAltAttr', '');?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oMedienDatei']->value->oMedienDateiAttribut_arr)) && count($_smarty_tpl->tpl_vars['oMedienDatei']->value->oMedienDateiAttribut_arr) > 0) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oMedienDatei']->value->oMedienDateiAttribut_arr, 'oAttribut');
$_smarty_tpl->tpl_vars['oAttribut']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oAttribut']->value) {
$_smarty_tpl->tpl_vars['oAttribut']->do_else = false;
?>
                                    <?php if ($_smarty_tpl->tpl_vars['oAttribut']->value->cName === 'img_alt') {?>
                                        <?php $_smarty_tpl->_assignInScope('cMediaAltAttr', $_smarty_tpl->tpl_vars['oAttribut']->value->cWert);?>
                                    <?php }?>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            <?php }?>
                            <?php ob_start();
if (!empty($_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad)) {
echo (string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null);
echo (string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad;
} elseif (!empty($_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL)) {
echo (string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL;
}
$_prefixVariable1=ob_get_clean();
$_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>"mediafiles-image",'img-src'=>$_prefixVariable1,'title-text'=>((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName),'img-top'=>true,'img-alt'=>((string)$_smarty_tpl->tpl_vars['cMediaAltAttr']->value)));
$_block_repeat=true;
echo $_block_plugin3->render(array('class'=>"mediafiles-image",'img-src'=>$_prefixVariable1,'title-text'=>((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName),'img-top'=>true,'img-alt'=>((string)$_smarty_tpl->tpl_vars['cMediaAltAttr']->value)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <p><?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cBeschreibung;?>
</p>
                            <?php $_block_repeat=false;
echo $_block_plugin3->render(array('class'=>"mediafiles-image",'img-src'=>$_prefixVariable1,'title-text'=>((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName),'img-top'=>true,'img-alt'=>((string)$_smarty_tpl->tpl_vars['cMediaAltAttr']->value)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'productdetails-mediafile-images'} */
/* {block 'productdetails-mediafile-audio'} */
class Block_12627547366196643cb9d881_80543455 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin4 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin4, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>"mediafiles-audio",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName));
$_block_repeat=true;
echo $_block_plugin4->render(array('class'=>"mediafiles-audio",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin5 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin5, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin5->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin6 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin6, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"mediafiles-description",'cols'=>12));
$_block_repeat=true;
echo $_block_plugin6->render(array('class'=>"mediafiles-description",'cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cBeschreibung;?>

                                        <?php $_block_repeat=false;
echo $_block_plugin6->render(array('class'=>"mediafiles-description",'cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php $_block_plugin7 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin7, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin7->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php if (strlen($_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad) > 1 || strlen($_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL) > 1) {?>
                                                <?php $_smarty_tpl->_assignInScope('audiosrc', $_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL);?>
                                                <?php if (strlen($_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad) > 1) {?>
                                                    <?php $_smarty_tpl->_assignInScope('audiosrc', ($_smarty_tpl->tpl_vars['ShopURL']->value).('/').((defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).($_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad));?>
                                                <?php }?>
                                                <?php if (strlen($_smarty_tpl->tpl_vars['audiosrc']->value) > 1) {?>
                                                    <audio controls controlsList="nodownload">
                                                        <source src="<?php echo $_smarty_tpl->tpl_vars['audiosrc']->value;?>
" type="audio/mpeg">
                                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'audioTagNotSupported','section'=>'errorMessages'),$_smarty_tpl ) );?>

                                                    </audio>
                                                <?php }?>
                                            <?php }?>
                                        <?php $_block_repeat=false;
echo $_block_plugin7->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin5->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin4->render(array('class'=>"mediafiles-audio",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'productdetails-mediafile-audio'} */
/* {block 'productdetails-mediafile-video'} */
class Block_3677660926196643cbb2486_64316013 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php if (($_smarty_tpl->tpl_vars['oMedienDatei']->value->videoType === 'mp4' || $_smarty_tpl->tpl_vars['oMedienDatei']->value->videoType === 'webm' || $_smarty_tpl->tpl_vars['oMedienDatei']->value->videoType === 'ogg')) {?>
                                <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>"mediafiles-video",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName));
$_block_repeat=true;
echo $_block_plugin8->render(array('class'=>"mediafiles-video",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin9->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"mediafiles-description",'cols'=>12));
$_block_repeat=true;
echo $_block_plugin10->render(array('class'=>"mediafiles-description",'cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cBeschreibung;?>

                                        <?php $_block_repeat=false;
echo $_block_plugin10->render(array('class'=>"mediafiles-description",'cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin11->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <video class="product-detail-video mw-100" controls>
                                                <source src="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
/<?php echo (defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null);
echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad;?>
" type="video/<?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->videoType;?>
">
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'videoTagNotSupported','section'=>'errorMessages'),$_smarty_tpl ) );?>

                                            </video>
                                        <?php $_block_repeat=false;
echo $_block_plugin11->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin9->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin8->render(array('class'=>"mediafiles-video",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php } else { ?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'videoTypeNotSupported','section'=>'errorMessages'),$_smarty_tpl ) );?>

                            <?php }?>
                        <?php
}
}
/* {/block 'productdetails-mediafile-video'} */
/* {block 'productdetails-mediafile-misc'} */
class Block_11950906796196643cbb8a17_46496339 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>"mediafiles-misc",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName));
$_block_repeat=true;
echo $_block_plugin12->render(array('class'=>"mediafiles-misc",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin13->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"mediafiles-description",'cols'=>12));
$_block_repeat=true;
echo $_block_plugin14->render(array('class'=>"mediafiles-description",'cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cBeschreibung;?>

                                    <?php $_block_repeat=false;
echo $_block_plugin14->render(array('class'=>"mediafiles-description",'cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin15->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php if (strpos($_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'youtube') !== false || strpos($_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'youtu.be') !== false) {?>
                                            <?php $_smarty_tpl->_subTemplateRender('file:productdetails/mediafile_youtube_embed.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                                        <?php } else { ?>
                                            <?php if ((isset($_smarty_tpl->tpl_vars['oMedienDatei']->value->oEmbed)) && $_smarty_tpl->tpl_vars['oMedienDatei']->value->oEmbed->code) {?>
                                                <?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->oEmbed->code;?>

                                            <?php }?>
                                            <?php if (!empty($_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad)) {?>
                                                <p>
                                                    <?php $_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>((string)$_smarty_tpl->tpl_vars['ShopURL']->value)."/".((string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad),'target'=>"_blank"));
$_block_repeat=true;
echo $_block_plugin16->render(array('href'=>((string)$_smarty_tpl->tpl_vars['ShopURL']->value)."/".((string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad),'target'=>"_blank"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cName;
$_block_repeat=false;
echo $_block_plugin16->render(array('href'=>((string)$_smarty_tpl->tpl_vars['ShopURL']->value)."/".((string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad),'target'=>"_blank"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                </p>
                                            <?php } elseif (!empty($_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL)) {?>
                                                <p>
                                                    <?php $_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'target'=>"_blank"));
$_block_repeat=true;
echo $_block_plugin17->render(array('href'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'target'=>"_blank"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?><i class="fa fa-external-link"></i> <?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cName;
$_block_repeat=false;
echo $_block_plugin17->render(array('href'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'target'=>"_blank"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                </p>
                                            <?php }?>
                                        <?php }?>
                                    <?php $_block_repeat=false;
echo $_block_plugin15->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin13->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin12->render(array('class'=>"mediafiles-misc",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'productdetails-mediafile-misc'} */
/* {block 'productdetails-mediafile-pdf'} */
class Block_12717830846196643cbcb065_29870520 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>"mediafiles-pdf",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName));
$_block_repeat=true;
echo $_block_plugin18->render(array('class'=>"mediafiles-pdf",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin19->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"mediafiles-description",'md'=>6));
$_block_repeat=true;
echo $_block_plugin20->render(array('class'=>"mediafiles-description",'md'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cBeschreibung;?>

                                    <?php $_block_repeat=false;
echo $_block_plugin20->render(array('class'=>"mediafiles-description",'md'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>6));
$_block_repeat=true;
echo $_block_plugin21->render(array('md'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad)) {?>
                                            <?php $_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"text-decoration-none-util",'href'=>((string)$_smarty_tpl->tpl_vars['ShopURL']->value)."/".((string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad),'target'=>"_blank"));
$_block_repeat=true;
echo $_block_plugin22->render(array('class'=>"text-decoration-none-util",'href'=>((string)$_smarty_tpl->tpl_vars['ShopURL']->value)."/".((string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad),'target'=>"_blank"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('alt'=>"PDF",'src'=>((string)(defined('PFAD_BILDER') ? constant('PFAD_BILDER') : null))."intern/file-pdf.png"),$_smarty_tpl ) );?>

                                                <span class="text-decoration-underline" ><?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cName;?>
</span>
                                            <?php $_block_repeat=false;
echo $_block_plugin22->render(array('class'=>"text-decoration-none-util",'href'=>((string)$_smarty_tpl->tpl_vars['ShopURL']->value)."/".((string)(defined('PFAD_MEDIAFILES') ? constant('PFAD_MEDIAFILES') : null)).((string)$_smarty_tpl->tpl_vars['oMedienDatei']->value->cPfad),'target'=>"_blank"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php } elseif (!empty($_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL)) {?>
                                            <?php $_block_plugin23 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin23, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"text-decoration-none-util",'href'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'target'=>"_blank"));
$_block_repeat=true;
echo $_block_plugin23->render(array('class'=>"text-decoration-none-util",'href'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'target'=>"_blank"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('alt'=>"PDF",'src'=>((string)(defined('PFAD_BILDER') ? constant('PFAD_BILDER') : null))."intern/file-pdf.png"),$_smarty_tpl ) );?>

                                                <span class="text-decoration-underline"><?php echo $_smarty_tpl->tpl_vars['oMedienDatei']->value->cName;?>
</span>
                                            <?php $_block_repeat=false;
echo $_block_plugin23->render(array('class'=>"text-decoration-none-util",'href'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cURL,'target'=>"_blank"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php }?>
                                    <?php $_block_repeat=false;
echo $_block_plugin21->render(array('md'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin19->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin18->render(array('class'=>"mediafiles-pdf",'title-text'=>$_smarty_tpl->tpl_vars['oMedienDatei']->value->cName), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'productdetails-mediafile-pdf'} */
/* {block 'productdetails-mediafile'} */
class Block_4362352586196643cb6d5d3_96967597 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productdetails-mediafile' => 
  array (
    0 => 'Block_4362352586196643cb6d5d3_96967597',
  ),
  'productdetails-mediafilealert' => 
  array (
    0 => 'Block_801923196196643cb7da83_72927154',
  ),
  'productdetails-mediafile-images' => 
  array (
    0 => 'Block_11933174736196643cb8acc4_44008804',
  ),
  'productdetails-mediafile-audio' => 
  array (
    0 => 'Block_12627547366196643cb9d881_80543455',
  ),
  'productdetails-mediafile-video' => 
  array (
    0 => 'Block_3677660926196643cbb2486_64316013',
  ),
  'productdetails-mediafile-misc' => 
  array (
    0 => 'Block_11950906796196643cbb8a17_46496339',
  ),
  'productdetails-mediafile-pdf' => 
  array (
    0 => 'Block_12717830846196643cbcb065_29870520',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if (!empty($_smarty_tpl->tpl_vars['Artikel']->value->oMedienDatei_arr)) {?>
        <?php $_smarty_tpl->_assignInScope('mp3List', false);?>
        <?php $_smarty_tpl->_assignInScope('titles', false);?>
        <div class="mediafiles card-columns <?php if ($_smarty_tpl->tpl_vars['mediaType']->value->count < 3) {?>card-columns-2<?php }?>">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Artikel']->value->oMedienDatei_arr, 'oMedienDatei', true);
$_smarty_tpl->tpl_vars['oMedienDatei']->iteration = 0;
$_smarty_tpl->tpl_vars['oMedienDatei']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oMedienDatei']->value) {
$_smarty_tpl->tpl_vars['oMedienDatei']->do_else = false;
$_smarty_tpl->tpl_vars['oMedienDatei']->iteration++;
$_smarty_tpl->tpl_vars['oMedienDatei']->last = $_smarty_tpl->tpl_vars['oMedienDatei']->iteration === $_smarty_tpl->tpl_vars['oMedienDatei']->total;
$__foreach_oMedienDatei_0_saved = $_smarty_tpl->tpl_vars['oMedienDatei'];
?>
            <?php if (($_smarty_tpl->tpl_vars['mediaType']->value->name == $_smarty_tpl->tpl_vars['oMedienDatei']->value->cMedienTyp && call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'count_characters' ][ 0 ], array( $_smarty_tpl->tpl_vars['oMedienDatei']->value->cAttributTab )) == 0) || (call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'count_characters' ][ 0 ], array( $_smarty_tpl->tpl_vars['oMedienDatei']->value->cAttributTab )) > 0 && $_smarty_tpl->tpl_vars['mediaType']->value->name == $_smarty_tpl->tpl_vars['oMedienDatei']->value->cAttributTab)) {?>
                <?php if ($_smarty_tpl->tpl_vars['oMedienDatei']->value->nErreichbar == 0) {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_801923196196643cb7da83_72927154', 'productdetails-mediafilealert', $this->tplIndex);
?>

                <?php } else { ?>
                    <?php $_smarty_tpl->_assignInScope('cName', $_smarty_tpl->tpl_vars['oMedienDatei']->value->cName);?>
                    <?php $_smarty_tpl->_assignInScope('titles', ($_smarty_tpl->tpl_vars['titles']->value).($_smarty_tpl->tpl_vars['cName']->value));?>
                    <?php if (!$_smarty_tpl->tpl_vars['oMedienDatei']->last) {?>
                        <?php $_smarty_tpl->_assignInScope('titles', ($_smarty_tpl->tpl_vars['titles']->value).('|'));?>
                    <?php }?>

                                        <?php if ($_smarty_tpl->tpl_vars['oMedienDatei']->value->nMedienTyp === 1) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11933174736196643cb8acc4_44008804', 'productdetails-mediafile-images', $this->tplIndex);
?>

                                            <?php } elseif ($_smarty_tpl->tpl_vars['oMedienDatei']->value->nMedienTyp === 2) {?>
                        <?php if (strlen($_smarty_tpl->tpl_vars['oMedienDatei']->value->cName) > 1) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12627547366196643cb9d881_80543455', 'productdetails-mediafile-audio', $this->tplIndex);
?>

                        <?php }?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['oMedienDatei']->value->nMedienTyp === 3) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3677660926196643cbb2486_64316013', 'productdetails-mediafile-video', $this->tplIndex);
?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['oMedienDatei']->value->nMedienTyp === 4) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11950906796196643cbb8a17_46496339', 'productdetails-mediafile-misc', $this->tplIndex);
?>

                                            <?php } elseif ($_smarty_tpl->tpl_vars['oMedienDatei']->value->nMedienTyp == 5) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12717830846196643cbcb065_29870520', 'productdetails-mediafile-pdf', $this->tplIndex);
?>

                    <?php }?>
                <?php }?>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['oMedienDatei'] = $__foreach_oMedienDatei_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    <?php }
}
}
/* {/block 'productdetails-mediafile'} */
}
