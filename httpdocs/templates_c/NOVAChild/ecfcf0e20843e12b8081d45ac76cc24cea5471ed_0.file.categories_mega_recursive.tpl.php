<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:40:38
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/categories_mega_recursive.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953e96c77414_35718882',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ecfcf0e20843e12b8081d45ac76cc24cea5471ed' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/categories_mega_recursive.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/image.tpl' => 1,
    'file:snippets/categories_mega_recursive.tpl' => 2,
  ),
),false)) {
function content_61953e96c77414_35718882 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_69565782361953e96c62360_53628828', 'snippets-categories-mega-recursive');
?>

<?php }
/* {block 'snippets-categories-mega-recursive-max-subsub-items'} */
class Block_139364058561953e96c62925_51646650 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php ob_start();
if ($_smarty_tpl->tpl_vars['isMobile']->value) {
echo "5";
} else {
echo "2";
}
$_prefixVariable130=ob_get_clean();
$_smarty_tpl->_assignInScope('max_subsub_items', $_prefixVariable130);?>
    <?php
}
}
/* {/block 'snippets-categories-mega-recursive-max-subsub-items'} */
/* {block 'snippets-categories-mega-recursive-main-link'} */
class Block_40548020061953e96c64739_10126273 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php ob_start();
if ($_smarty_tpl->tpl_vars['firstChild']->value) {
echo "submenu-headline submenu-headline-toplevel";
}
$_prefixVariable131=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['mainCategory']->value->hasChildren() && $_smarty_tpl->tpl_vars['subCategory']->value < $_smarty_tpl->tpl_vars['max_subsub_items']->value && $_smarty_tpl->tpl_vars['Einstellungen']->value['template']['megamenu']['show_subcategories'] !== 'N') {
echo "nav-link dropdown-toggle";
}
$_prefixVariable132=ob_get_clean();
$_block_plugin117 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin117, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getURL(),'class'=>"categories-recursive-link d-lg-block ".$_prefixVariable131." ".((string)$_smarty_tpl->tpl_vars['subCategory']->value)." ".$_prefixVariable132,'aria'=>array("expanded"=>"false")));
$_block_repeat=true;
echo $_block_plugin117->render(array('href'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getURL(),'class'=>"categories-recursive-link d-lg-block ".$_prefixVariable131." ".((string)$_smarty_tpl->tpl_vars['subCategory']->value)." ".$_prefixVariable132,'aria'=>array("expanded"=>"false")), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php if ($_smarty_tpl->tpl_vars['firstChild']->value && $_smarty_tpl->tpl_vars['Einstellungen']->value['template']['megamenu']['show_category_images'] !== 'N' && (!$_smarty_tpl->tpl_vars['isMobile']->value || $_smarty_tpl->tpl_vars['isTablet']->value)) {?>
                <?php $_smarty_tpl->_assignInScope('imgAlt', $_smarty_tpl->tpl_vars['mainCategory']->value->getAttribute('img_alt'));?>
                <?php ob_start();
if (empty($_smarty_tpl->tpl_vars['imgAlt']->value->cWert)) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'seofy' ][ 0 ], array( $_smarty_tpl->tpl_vars['mainCategory']->value->getName() ));
} else {
echo (string)$_smarty_tpl->tpl_vars['imgAlt']->value->cWert;
}
$_prefixVariable133=ob_get_clean();
$_smarty_tpl->_subTemplateRender('file:snippets/image.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('class'=>'submenu-headline-image','item'=>$_smarty_tpl->tpl_vars['mainCategory']->value,'square'=>false,'srcSize'=>'sm','alt'=>$_prefixVariable133), 0, false);
?>
            <?php }?>
            <span class="text-truncate d-block">
                <?php echo $_smarty_tpl->tpl_vars['mainCategory']->value->getShortName();
if ($_smarty_tpl->tpl_vars['mainCategory']->value->hasChildren() && $_smarty_tpl->tpl_vars['subCategory']->value >= $_smarty_tpl->tpl_vars['max_subsub_items']->value) {?><span class="more-subcategories">&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['mainCategory']->value->getChildren());?>
)</span><?php }?>
            </span>
        <?php $_block_repeat=false;
echo $_block_plugin117->render(array('href'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getURL(),'class'=>"categories-recursive-link d-lg-block ".$_prefixVariable131." ".((string)$_smarty_tpl->tpl_vars['subCategory']->value)." ".$_prefixVariable132,'aria'=>array("expanded"=>"false")), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'snippets-categories-mega-recursive-main-link'} */
/* {block 'snippets-categories-mega-recursive-child-header'} */
class Block_29069851961953e96c6f6a3_50281895 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <li class="nav-item d-lg-none">
                            <?php $_block_plugin119 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin119, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getURL(),'nofollow'=>true));
$_block_repeat=true;
echo $_block_plugin119->render(array('href'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getURL(),'nofollow'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <strong class="nav-mobile-heading">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'menuShow','printf'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getShortName()),$_smarty_tpl ) );?>

                                </strong>
                            <?php $_block_repeat=false;
echo $_block_plugin119->render(array('href'=>$_smarty_tpl->tpl_vars['mainCategory']->value->getURL(),'nofollow'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        </li>
                    <?php
}
}
/* {/block 'snippets-categories-mega-recursive-child-header'} */
/* {block 'snippets-categories-mega-recursive-child-category-child'} */
class Block_130174776661953e96c72601_40654824 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <li class="nav-item dropdown">
                                        <?php $_smarty_tpl->_subTemplateRender('file:snippets/categories_mega_recursive.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('mainCategory'=>$_smarty_tpl->tpl_vars['category']->value,'firstChild'=>false,'subCategory'=>$_smarty_tpl->tpl_vars['subCategory']->value+1), 0, true);
?>
                                    </li>
                                <?php
}
}
/* {/block 'snippets-categories-mega-recursive-child-category-child'} */
/* {block 'snippets-categories-mega-recursivechild-category-no-child'} */
class Block_134768291361953e96c73849_91010820 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_block_plugin120 = isset($_smarty_tpl->smarty->registered_plugins['block']['navitem'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['navitem'][0][0] : null;
if (!is_callable(array($_block_plugin120, 'render'))) {
throw new SmartyException('block tag \'navitem\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('navitem', array('href'=>$_smarty_tpl->tpl_vars['category']->value->getURL()));
$_block_repeat=true;
echo $_block_plugin120->render(array('href'=>$_smarty_tpl->tpl_vars['category']->value->getURL()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <span class="text-truncate d-block">
                                            <?php echo $_smarty_tpl->tpl_vars['category']->value->getShortName();
if ($_smarty_tpl->tpl_vars['category']->value->hasChildren()) {?><span class="more-subcategories">&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['category']->value->getChildren());?>
)</span><?php }?>
                                        </span>
                                    <?php $_block_repeat=false;
echo $_block_plugin120->render(array('href'=>$_smarty_tpl->tpl_vars['category']->value->getURL()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php
}
}
/* {/block 'snippets-categories-mega-recursivechild-category-no-child'} */
/* {block 'snippets-categories-mega-recursive-child-categories'} */
class Block_10992020161953e96c70e29_32809181 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mainCategory']->value->getChildren(), 'category');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>
                            <?php if ($_smarty_tpl->tpl_vars['category']->value->hasChildren() && $_smarty_tpl->tpl_vars['subCategory']->value+1 < $_smarty_tpl->tpl_vars['max_subsub_items']->value) {?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_130174776661953e96c72601_40654824', 'snippets-categories-mega-recursive-child-category-child', $this->tplIndex);
?>

                            <?php } else { ?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_134768291361953e96c73849_91010820', 'snippets-categories-mega-recursivechild-category-no-child', $this->tplIndex);
?>

                            <?php }?>
                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    <?php
}
}
/* {/block 'snippets-categories-mega-recursive-child-categories'} */
/* {block 'snippets-categories-mega-recursive-child-content'} */
class Block_154030382261953e96c6f056_21736980 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="categories-recursive-dropdown dropdown-menu">
                <?php $_block_plugin118 = isset($_smarty_tpl->smarty->registered_plugins['block']['nav'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['nav'][0][0] : null;
if (!is_callable(array($_block_plugin118, 'render'))) {
throw new SmartyException('block tag \'nav\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('nav', array());
$_block_repeat=true;
echo $_block_plugin118->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_29069851961953e96c6f6a3_50281895', 'snippets-categories-mega-recursive-child-header', $this->tplIndex);
?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10992020161953e96c70e29_32809181', 'snippets-categories-mega-recursive-child-categories', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin118->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            </div>
        <?php
}
}
/* {/block 'snippets-categories-mega-recursive-child-content'} */
/* {block 'snippets-categories-mega-recursive'} */
class Block_69565782361953e96c62360_53628828 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-categories-mega-recursive' => 
  array (
    0 => 'Block_69565782361953e96c62360_53628828',
  ),
  'snippets-categories-mega-recursive-max-subsub-items' => 
  array (
    0 => 'Block_139364058561953e96c62925_51646650',
  ),
  'snippets-categories-mega-recursive-main-link' => 
  array (
    0 => 'Block_40548020061953e96c64739_10126273',
  ),
  'snippets-categories-mega-recursive-child-content' => 
  array (
    0 => 'Block_154030382261953e96c6f056_21736980',
  ),
  'snippets-categories-mega-recursive-child-header' => 
  array (
    0 => 'Block_29069851961953e96c6f6a3_50281895',
  ),
  'snippets-categories-mega-recursive-child-categories' => 
  array (
    0 => 'Block_10992020161953e96c70e29_32809181',
  ),
  'snippets-categories-mega-recursive-child-category-child' => 
  array (
    0 => 'Block_130174776661953e96c72601_40654824',
  ),
  'snippets-categories-mega-recursivechild-category-no-child' => 
  array (
    0 => 'Block_134768291361953e96c73849_91010820',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_139364058561953e96c62925_51646650', 'snippets-categories-mega-recursive-max-subsub-items', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_40548020061953e96c64739_10126273', 'snippets-categories-mega-recursive-main-link', $this->tplIndex);
?>

    <?php if ($_smarty_tpl->tpl_vars['mainCategory']->value->hasChildren() && $_smarty_tpl->tpl_vars['Einstellungen']->value['template']['megamenu']['show_subcategories'] !== 'N' && $_smarty_tpl->tpl_vars['subCategory']->value < $_smarty_tpl->tpl_vars['max_subsub_items']->value) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_154030382261953e96c6f056_21736980', 'snippets-categories-mega-recursive-child-content', $this->tplIndex);
?>

    <?php }
}
}
/* {/block 'snippets-categories-mega-recursive'} */
}
