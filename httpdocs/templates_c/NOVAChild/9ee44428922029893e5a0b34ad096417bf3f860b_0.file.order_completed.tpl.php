<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:20:25
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/order_completed.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61966129e3ae66_60681419',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9ee44428922029893e5a0b34ad096417bf3f860b' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/order_completed.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout/header.tpl' => 1,
    'file:snippets/extension.tpl' => 1,
    'file:checkout/inc_paymentmodules.tpl' => 1,
    'file:checkout/inc_order_completed.tpl' => 1,
    'file:layout/footer.tpl' => 1,
  ),
),false)) {
function content_61966129e3ae66_60681419 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_146077117461966129e21e23_89601888', 'checkout-order-completed');
?>

<?php }
/* {block 'checkout-order-completed-include-header'} */
class Block_25720693761966129e226a4_09747612 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'checkout-order-completed-include-header'} */
/* {block 'checkout-order-completed-heading'} */
class Block_95547057661966129e24018_66309856 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['container'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['container'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'container\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('container', array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid()));
$_block_repeat=true;
echo $_block_plugin1->render(array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php if ((isset($_SESSION['Zahlungsart']->nWaehrendBestellung)) && $_SESSION['Zahlungsart']->nWaehrendBestellung == 1) {?>
                    <h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderCompletedPre','section'=>'checkout'),$_smarty_tpl ) );?>
</h2>
                <?php } elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId !== 'za_kreditkarte_jtl' && $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId !== 'za_lastschrift_jtl') {?>
                    <h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderCompletedPost','section'=>'checkout'),$_smarty_tpl ) );?>
</h2>
                <?php }?>
            <?php $_block_repeat=false;
echo $_block_plugin1->render(array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'checkout-order-completed-heading'} */
/* {block 'checkout-order-completed-include-extension'} */
class Block_109717470261966129e2e745_41639117 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:snippets/extension.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'checkout-order-completed-include-extension'} */
/* {block 'checkout-order-completed-include-inc-paymentmodules'} */
class Block_94960499361966129e30b74_65694125 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_paymentmodules.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php
}
}
/* {/block 'checkout-order-completed-include-inc-paymentmodules'} */
/* {block 'checkout-order-completed-include-inc-order-completed'} */
class Block_199526449561966129e32ff1_29727838 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_order_completed.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                            <?php
}
}
/* {/block 'checkout-order-completed-include-inc-order-completed'} */
/* {block 'checkout-order-completed-order-completed'} */
class Block_100442703461966129e31f96_63515252 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php if ((isset($_smarty_tpl->tpl_vars['abschlussseite']->value))) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_199526449561966129e32ff1_29727838', 'checkout-order-completed-include-inc-order-completed', $this->tplIndex);
?>

                        <?php }?>
                    <?php
}
}
/* {/block 'checkout-order-completed-order-completed'} */
/* {block 'checkout-order-completed-continue-shopping'} */
class Block_86258850661966129e34639_50507254 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin3->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin4 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin4, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4,'lg'=>3,'xl'=>2));
$_block_repeat=true;
echo $_block_plugin4->render(array('md'=>4,'lg'=>3,'xl'=>2), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php ob_start();
echo $_smarty_tpl->tpl_vars['ShopURL']->value;
$_prefixVariable1 = ob_get_clean();
$_block_plugin5 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin5, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('block'=>true,'type'=>"link",'href'=>$_prefixVariable1,'variant'=>"primary"));
$_block_repeat=true;
echo $_block_plugin5->render(array('block'=>true,'type'=>"link",'href'=>$_prefixVariable1,'variant'=>"primary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'continueShopping','section'=>'checkout'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin5->render(array('block'=>true,'type'=>"link",'href'=>$_prefixVariable1,'variant'=>"primary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin4->render(array('md'=>4,'lg'=>3,'xl'=>2), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin3->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'checkout-order-completed-continue-shopping'} */
/* {block 'checkout-order-completed-main'} */
class Block_207334451761966129e2f904_35502105 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['container'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['container'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'container\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('container', array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid()));
$_block_repeat=true;
echo $_block_plugin2->render(array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <div class="order-completed">
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_94960499361966129e30b74_65694125', 'checkout-order-completed-include-inc-paymentmodules', $this->tplIndex);
?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_100442703461966129e31f96_63515252', 'checkout-order-completed-order-completed', $this->tplIndex);
?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_86258850661966129e34639_50507254', 'checkout-order-completed-continue-shopping', $this->tplIndex);
?>

                </div>
            <?php $_block_repeat=false;
echo $_block_plugin2->render(array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'checkout-order-completed-main'} */
/* {block 'checkout-order-completed-content'} */
class Block_196148865861966129e23a49_66654182 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_95547057661966129e24018_66309856', 'checkout-order-completed-heading', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_109717470261966129e2e745_41639117', 'checkout-order-completed-include-extension', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_207334451761966129e2f904_35502105', 'checkout-order-completed-main', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'checkout-order-completed-content'} */
/* {block 'checkout-order-completed-include-footer'} */
class Block_27537354461966129e399c2_30937673 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'checkout-order-completed-include-footer'} */
/* {block 'checkout-order-completed'} */
class Block_146077117461966129e21e23_89601888 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-order-completed' => 
  array (
    0 => 'Block_146077117461966129e21e23_89601888',
  ),
  'checkout-order-completed-include-header' => 
  array (
    0 => 'Block_25720693761966129e226a4_09747612',
  ),
  'checkout-order-completed-content' => 
  array (
    0 => 'Block_196148865861966129e23a49_66654182',
  ),
  'checkout-order-completed-heading' => 
  array (
    0 => 'Block_95547057661966129e24018_66309856',
  ),
  'checkout-order-completed-include-extension' => 
  array (
    0 => 'Block_109717470261966129e2e745_41639117',
  ),
  'checkout-order-completed-main' => 
  array (
    0 => 'Block_207334451761966129e2f904_35502105',
  ),
  'checkout-order-completed-include-inc-paymentmodules' => 
  array (
    0 => 'Block_94960499361966129e30b74_65694125',
  ),
  'checkout-order-completed-order-completed' => 
  array (
    0 => 'Block_100442703461966129e31f96_63515252',
  ),
  'checkout-order-completed-include-inc-order-completed' => 
  array (
    0 => 'Block_199526449561966129e32ff1_29727838',
  ),
  'checkout-order-completed-continue-shopping' => 
  array (
    0 => 'Block_86258850661966129e34639_50507254',
  ),
  'checkout-order-completed-include-footer' => 
  array (
    0 => 'Block_27537354461966129e399c2_30937673',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_25720693761966129e226a4_09747612', 'checkout-order-completed-include-header', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_196148865861966129e23a49_66654182', 'checkout-order-completed-content', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_27537354461966129e399c2_30937673', 'checkout-order-completed-include-footer', $this->tplIndex);
?>

<?php
}
}
/* {/block 'checkout-order-completed'} */
}
