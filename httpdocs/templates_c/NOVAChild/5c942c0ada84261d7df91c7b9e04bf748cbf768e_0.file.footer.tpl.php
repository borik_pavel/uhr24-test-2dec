<?php
/* Smarty version 3.1.39, created on 2021-11-23 10:09:44
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVAChild/layout/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cafd89e8519_35465400',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c942c0ada84261d7df91c7b9e04bf748cbf768e' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVAChild/layout/footer.tpl',
      1 => 1637658296,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/scroll_top.tpl' => 1,
    'file:snippets/consent_manager.tpl' => 1,
  ),
),false)) {
function content_619cafd89e8519_35465400 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_584070476619cafd89414d8_43840313', 'layout-footer');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['parent_template_path']->value)."/layout/footer.tpl");
}
/* {block 'layout-footer-content-productlist-col-closingtag'} */
class Block_942574665619cafd896f082_22089229 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    </div>                <?php
}
}
/* {/block 'layout-footer-content-productlist-col-closingtag'} */
/* {block 'footer-sidepanel-left-content'} */
class Block_2128683561619cafd8970476_04740418 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['boxes']->value['left'];
}
}
/* {/block 'footer-sidepanel-left-content'} */
/* {block 'layout-footer-sidepanel-left'} */
class Block_1454603224619cafd89700b7_61156767 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <aside id="sidepanel_left" class="sidepanel-left d-print-none col-12 col-lg-4 col-xl-3 order-lg-0 dropdown-full-width">
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2128683561619cafd8970476_04740418', 'footer-sidepanel-left-content', $this->tplIndex);
?>

                    </aside>
                <?php
}
}
/* {/block 'layout-footer-sidepanel-left'} */
/* {block 'layout-footer-content-productlist-row-closingtag'} */
class Block_1164773273619cafd8978148_45675747 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    </div>                <?php
}
}
/* {/block 'layout-footer-content-productlist-row-closingtag'} */
/* {block 'layout-footer-aside'} */
class Block_428701393619cafd8942a34_61374440 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['has_boxes'][0], array( array('position'=>'left','assign'=>'hasLeftBox'),$_smarty_tpl ) );?>


            <?php if ((defined('PAGE_ARTIKELLISTE') ? constant('PAGE_ARTIKELLISTE') : null) === $_smarty_tpl->tpl_vars['nSeitenTyp']->value && !$_smarty_tpl->tpl_vars['bExclusive']->value && $_smarty_tpl->tpl_vars['hasLeftBox']->value && !empty(trim(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['boxes']->value['left'])))) {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_942574665619cafd896f082_22089229', 'layout-footer-content-productlist-col-closingtag', $this->tplIndex);
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1454603224619cafd89700b7_61156767', 'layout-footer-sidepanel-left', $this->tplIndex);
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1164773273619cafd8978148_45675747', 'layout-footer-content-productlist-row-closingtag', $this->tplIndex);
?>

            <?php }?>
        <?php
}
}
/* {/block 'layout-footer-aside'} */
/* {block 'layout-footer-content-closingtag'} */
class Block_238011443619cafd8978d87_85482250 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['opcMountPoint'][0], array( array('id'=>'opc_content','title'=>'Default Area','inContainer'=>false),$_smarty_tpl ) );?>

            </div>        <?php
}
}
/* {/block 'layout-footer-content-closingtag'} */
/* {block 'layout-footer-content-wrapper-closingtag'} */
class Block_756818634619cafd8979b52_43427440 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            </div>        <?php
}
}
/* {/block 'layout-footer-content-wrapper-closingtag'} */
/* {block 'layout-footer-content-all-closingtags'} */
class Block_1466968377619cafd8942447_30777993 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_428701393619cafd8942a34_61374440', 'layout-footer-aside', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_238011443619cafd8978d87_85482250', 'layout-footer-content-closingtag', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_756818634619cafd8979b52_43427440', 'layout-footer-content-wrapper-closingtag', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'layout-footer-content-all-closingtags'} */
/* {block 'layout-footer-main-wrapper-closingtag'} */
class Block_1826837539619cafd897a425_20917140 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        </main>     <?php
}
}
/* {/block 'layout-footer-main-wrapper-closingtag'} */
/* {block 'layout-footer-content'} */
class Block_1758545103619cafd897acf5_85800203 extends Smarty_Internal_Block
{
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	
	<div id="container_logoblock">
		<ul id="logoblock">
			<li id="armani_exchange"><a href="/marken/armani-exchange" title="Armani Exchange Uhren - klare Formen, exakte Linien"></a></li>
			<li id="bering"><a href="/marken/bering" title="Bering Uhren - inspiriert von der Sch&ouml;nheit der Arktis"></a></li>
			<li id="hugo_boss"><a href="/marken/hugo-boss" title="BOSS Uhren"></a></li>
			<li id="bruno_soehnle"><a href="/marken/bruno-soehnle" title="Bruno S&ouml;hnle Uhren"></a></li>
			<li id="cai"><a href="/marken/cai" title="cai Schmuck"></a></li>
			<li id="calvin_klein"><a href="/marken/calvin-klein" title="CALVIN KLEIN Schmuck und CALVIN KLEIN Uhren"></a></li>
			<li id="cluse"><a href="/marken/cluse" title="Cluse Damenuhren und Uhrenarmb&auml;nder"></a></li>
			<li id="daniel_wellington"><a href="/marken/daniel-wellington" title="Daniel Wellington Uhren"></a></li>
			<li id="davidoff"><a href="/marken/davidoff" title="Davidoff Uhren und Accessoires"></a></li>
			<li id="diesel"><a href="/marken/diesel" title="Diesel Uhren und Diesel Schmuck"></a></li>
			<li id="diesel_on"><a href="/marken/diesel/uhren/smartwatches" title="Diesel On Smartwatches"></a></li>
			<li id="dugena"><a href="/marken/dugena" title="Dugena Uhren"></a></li>
			<li id="emporio_armani"><a href="/marken/emporio-armani" title="Emporio Armani Uhren und Schmuck"></a></li>
			<li id="emporio_armani_connected"><a href="/marken/emporio-armani/uhren/smartwatches" title="Emporio Armani Connected Smartwatches"></a></li>
			<li id="flik_flak"><a href="/marken/flik-flak" title="Flik Flak Uhren"></a></li>
			<li id="filius"><a href="/marken/filius" title="Filius Zeitdesign, Wecker, Gro?uhren"></a></li>
			<li id="fossil"><a href="/marken/fossil" title="Fossil Uhren und Fossil Schmuck"></a></li>
			<li id="fossil_q"><a href="/marken/fossil/uhren/smartwatches" title="Fossil Q Smartwatches"></a></li>
			<li id="garmin"><a href="/marken/garmin" title="Garmin Smartwatches u. Sportuhren"></a></li>
			<li id="michel_herbelin"><a href="/marken/michel-herbelin" title="Michel Herbelin Uhren"></a></li>
			<li id="hirsch"><a href="/marken/hirsch" title="Hirsch Uhrenarmb&auml;nder"></a></li>
			<li id="ice_watch"><a href="/marken/ice-watch" title="Ice Watch Uhren"></a></li>
			<li id="junghans"><a href="/marken/junghans" title="Junghans Uhren"></a></li>
			<li id="luminox"><a href="/marken/luminox" title="Luminox Uhren"></a></li>
			<li id="michael_kors"><a href="/marken/michael-kors" title="Michael Kors Uhren"></a></li>
			<li id="michael_kors_access"><a href="/marken/michael-kors/uhren/smartwatches" title="Michael Kors Access Smartwatches f&uuml;r Damen"></a></li>
			<li id="mido"><a href="/marken/mido" title="Mido Uhren"></a></li>
			<li id="mondaine"><a href="/marken/mondaine" title="Mondaine Uhren"></a></li>
			<li id="pippi_langstrumpf"><a href="/marken/pippi-langstrumpf" title="Pippi Langstrumpf Kinderuhren"></a></li>
			<li id="police"><a href="/marken/police" title="Police Uhren und Police Schmuck"></a></li>
			<li id="regent"><a href="/marken/regent" title="Regent Uhren, Herrenuhren, Damenuhren"></a></li>
			<li id="skagen"><a href="/marken/skagen" title="Skagen Uhren"></a></li>
			<li id="skagen_connected"><a href="/marken/skagen/uhren/smartwatches" title="Skagen Connected Smartwatches"></a></li>
			<li id="s_oliver"><a href="/marken/s-oliver" title="s.Oliver Schmuck f&uuml;r jeden Tag"></a></li>
			<li id="swatch"><a href="/marken/swatch" title="Swatch Uhren"></a></li>
			<li id="tissot"><a href="/marken/tissot" title="Tissot Uhren"></a></li>
			<li id="tommy_hilfiger"><a href="/marken/tommy-hilfiger" title="Tommy Hilfiger Uhren und Schmuck"></a></li>
			<li id="withings"><a href="/marken/withings" title="Withings Smartwatches"></a></li>
			<li id="armani_exchange"><a href="/marken/armani-exchange" title="Armani Exchange Uhren - klare Formen, exakte Linien"></a></li>
			<li id="bering"><a href="/marken/bering" title="Bering Uhren - inspiriert von der Sch&ouml;nheit der Arktis"></a></li>
		</ul>
	</div>

			

	
	
	<?php
}
}
/* {/block 'layout-footer-content'} */
/* {block 'layout-footer-boxes'} */
class Block_23751010619cafd8982860_40068656 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getBoxesByPosition'][0], array( array('position'=>'bottom','assign'=>'footerBoxes'),$_smarty_tpl ) );?>

                        <?php if ((isset($_smarty_tpl->tpl_vars['footerBoxes']->value)) && count($_smarty_tpl->tpl_vars['footerBoxes']->value) > 0) {?>
                            <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('id'=>'footer-boxes'));
$_block_repeat=true;
echo $_block_plugin1->render(array('id'=>'footer-boxes'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['footerBoxes']->value, 'box', true);
$_smarty_tpl->tpl_vars['box']->iteration = 0;
$_smarty_tpl->tpl_vars['box']->index = -1;
$_smarty_tpl->tpl_vars['box']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['box']->value) {
$_smarty_tpl->tpl_vars['box']->do_else = false;
$_smarty_tpl->tpl_vars['box']->iteration++;
$_smarty_tpl->tpl_vars['box']->index++;
$_smarty_tpl->tpl_vars['box']->first = !$_smarty_tpl->tpl_vars['box']->index;
$_smarty_tpl->tpl_vars['box']->last = $_smarty_tpl->tpl_vars['box']->iteration === $_smarty_tpl->tpl_vars['box']->total;
$__foreach_box_0_saved = $_smarty_tpl->tpl_vars['box'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['box']->first || $_smarty_tpl->tpl_vars['box']->last) {
$_smarty_tpl->_assignInScope('we', 3);
} else {
$_smarty_tpl->_assignInScope('we', 2);
}?>
                                <?php if ($_smarty_tpl->tpl_vars['box']->last) {
$_smarty_tpl->_assignInScope('iamgod', 12);
} else {
$_smarty_tpl->_assignInScope('iamgod', 6);
}?>
                                    <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'sm'=>$_smarty_tpl->tpl_vars['iamgod']->value,'md'=>4,'lg'=>$_smarty_tpl->tpl_vars['we']->value));
$_block_repeat=true;
echo $_block_plugin2->render(array('cols'=>12,'sm'=>$_smarty_tpl->tpl_vars['iamgod']->value,'md'=>4,'lg'=>$_smarty_tpl->tpl_vars['we']->value), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo $_smarty_tpl->tpl_vars['box']->value->getRenderedContent();?>

                                    <?php $_block_repeat=false;
echo $_block_plugin2->render(array('cols'=>12,'sm'=>$_smarty_tpl->tpl_vars['iamgod']->value,'md'=>4,'lg'=>$_smarty_tpl->tpl_vars['we']->value), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php
$_smarty_tpl->tpl_vars['box'] = $__foreach_box_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            <?php $_block_repeat=false;
echo $_block_plugin1->render(array('id'=>'footer-boxes'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php }?>
                    <?php
}
}
/* {/block 'layout-footer-boxes'} */
/* {block 'layout-footer-newsletter-info'} */
class Block_252221199619cafd89a2857_28224247 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <p class="info">
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'newsletterInformedConsent','section'=>'newsletter','printf'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL()),$_smarty_tpl ) );?>

                                        </p>
                                    <?php
}
}
/* {/block 'layout-footer-newsletter-info'} */
/* {block 'layout-footer-form-content'} */
class Block_814550777619cafd89a52b7_35004108 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"abonnieren",'value'=>"2"),$_smarty_tpl ) );?>

                                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'emailadress'),$_smarty_tpl ) );
$_prefixVariable2=ob_get_clean();
$_block_plugin7 = isset($_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0] : null;
if (!is_callable(array($_block_plugin7, 'render'))) {
throw new SmartyException('block tag \'formgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formgroup', array('label-sr-only'=>$_prefixVariable2,'class'=>"newsletter-email-wrapper"));
$_block_repeat=true;
echo $_block_plugin7->render(array('label-sr-only'=>$_prefixVariable2,'class'=>"newsletter-email-wrapper"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['inputgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inputgroup'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'inputgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inputgroup', array());
$_block_repeat=true;
echo $_block_plugin8->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'emailadress'),$_smarty_tpl ) );
$_prefixVariable3=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'emailadress'),$_smarty_tpl ) );
$_prefixVariable4 = ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"email",'name'=>"cEmail",'id'=>"newsletter_email",'placeholder'=>$_prefixVariable3,'aria'=>array('label'=>$_prefixVariable4)),$_smarty_tpl ) );?>

                                                        <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['inputgroupaddon'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inputgroupaddon'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'inputgroupaddon\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inputgroupaddon', array('append'=>true));
$_block_repeat=true;
echo $_block_plugin9->render(array('append'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('type'=>'submit','variant'=>'secondary','class'=>''));
$_block_repeat=true;
echo $_block_plugin10->render(array('type'=>'submit','variant'=>'secondary','class'=>''), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                               <i class="fal fas fa-paper-plane"></i>
                                                            <?php $_block_repeat=false;
echo $_block_plugin10->render(array('type'=>'submit','variant'=>'secondary','class'=>''), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_repeat=false;
echo $_block_plugin9->render(array('append'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                    <?php $_block_repeat=false;
echo $_block_plugin8->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                <?php $_block_repeat=false;
echo $_block_plugin7->render(array('label-sr-only'=>$_prefixVariable2,'class'=>"newsletter-email-wrapper"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            <?php
}
}
/* {/block 'layout-footer-form-content'} */
/* {block 'layout-footer-form-captcha'} */
class Block_779667291619cafd89a93d9_67008873 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <div class="<?php if (!empty($_smarty_tpl->tpl_vars['plausiArr']->value['captcha']) && $_smarty_tpl->tpl_vars['plausiArr']->value['captcha'] === true) {?> has-error<?php }?>">
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['captchaMarkup'][0], array( array('getBody'=>true),$_smarty_tpl ) );?>

                                                </div>
                                            <?php
}
}
/* {/block 'layout-footer-form-captcha'} */
/* {block 'layout-footer-form'} */
class Block_788591234619cafd89a4381_12912047 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'newsletter.php'),$_smarty_tpl ) );
$_prefixVariable1=ob_get_clean();
$_block_plugin6 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin6, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('methopd'=>"post",'action'=>$_prefixVariable1));
$_block_repeat=true;
echo $_block_plugin6->render(array('methopd'=>"post",'action'=>$_prefixVariable1), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_814550777619cafd89a52b7_35004108', 'layout-footer-form-content', $this->tplIndex);
?>

                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_779667291619cafd89a93d9_67008873', 'layout-footer-form-captcha', $this->tplIndex);
?>

                                        <?php $_block_repeat=false;
echo $_block_plugin6->render(array('methopd'=>"post",'action'=>$_prefixVariable1), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php
}
}
/* {/block 'layout-footer-form'} */
/* {block 'layout-footer-newsletter'} */
class Block_1594691198619cafd89a0e50_58202313 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"newsletter-footer"));
$_block_repeat=true;
echo $_block_plugin3->render(array('class'=>"newsletter-footer"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin4 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin4, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>5));
$_block_repeat=true;
echo $_block_plugin4->render(array('cols'=>12,'lg'=>5), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                   
                                    <?php if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]))) {?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_252221199619cafd89a2857_28224247', 'layout-footer-newsletter-info', $this->tplIndex);
?>

                                    <?php }?>
                                <?php $_block_repeat=false;
echo $_block_plugin4->render(array('cols'=>12,'lg'=>5), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin5 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin5, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6));
$_block_repeat=true;
echo $_block_plugin5->render(array('cols'=>12,'lg'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_788591234619cafd89a4381_12912047', 'layout-footer-form', $this->tplIndex);
?>

                                <?php $_block_repeat=false;
echo $_block_plugin5->render(array('cols'=>12,'lg'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin3->render(array('class'=>"newsletter-footer"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                           
                        <?php
}
}
/* {/block 'layout-footer-newsletter'} */
/* {block 'layout-footer-socialmedia'} */
class Block_1564869417619cafd89acbc9_08161190 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'class'=>"footer-additional-wrapper col-auto mx-auto"));
$_block_repeat=true;
echo $_block_plugin12->render(array('cols'=>12,'class'=>"footer-additional-wrapper col-auto mx-auto"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    
                                    <div class="footnote-vat">
                        <?php if ($_smarty_tpl->tpl_vars['NettoPreise']->value == 1) {?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'footnoteExclusiveVat','assign'=>'footnoteVat'),$_smarty_tpl ) );?>

                        <?php } else { ?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'footnoteInclusiveVat','assign'=>'footnoteVat'),$_smarty_tpl ) );?>

                        <?php }?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]))) {?>
                            <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandhinweis'] === 'zzgl') {?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'footnoteExclusiveShipping','printf'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL(),'assign'=>'footnoteShipping'),$_smarty_tpl ) );?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_versandhinweis'] === 'inkl') {?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'footnoteInclusiveShipping','printf'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_VERSAND') ? constant('LINKTYP_VERSAND') : null)]->getURL(),'assign'=>'footnoteShipping'),$_smarty_tpl ) );?>

                            <?php }?>
                        <?php }?>
                                         </div>
                                    
                                    
                                 
                                    <?php $_block_repeat=false;
echo $_block_plugin12->render(array('cols'=>12,'class'=>"footer-additional-wrapper col-auto mx-auto"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php
}
}
/* {/block 'layout-footer-socialmedia'} */
/* {block 'layout-footer-additional'} */
class Block_262346201619cafd89abb74_66543185 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['socialmedia_footer'] === 'Y') {?>
                            <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"footer-social-media"));
$_block_repeat=true;
echo $_block_plugin11->render(array('class'=>"footer-social-media"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1564869417619cafd89acbc9_08161190', 'layout-footer-socialmedia', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin11->render(array('class'=>"footer-social-media"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>                        <?php }?>
                    <?php
}
}
/* {/block 'layout-footer-additional'} */
/* {block 'layout-footer-copyright'} */
class Block_883263833619cafd89b2da3_58460176 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              
       <!-- includel old panel -->
          
        
           
              <div id="newcopyright"> 
                <?php if (!empty($_smarty_tpl->tpl_vars['meta_copyright']->value)) {?>
            <div class="icon-mr-2" itemprop="copyrightHolder">&copy; <?php echo $_smarty_tpl->tpl_vars['meta_copyright']->value;?>
</div>
                 <?php }?>
                 <div class="small">* <?php echo $_smarty_tpl->tpl_vars['footnoteVat']->value;
if ((isset($_smarty_tpl->tpl_vars['footnoteShipping']->value))) {
echo $_smarty_tpl->tpl_vars['footnoteShipping']->value;
}?></div>
                 <div class="ready-for-loader">
                        <ul class="list-unstyled socialuns">
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['facebook'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['facebook'],'http') !== 0) {
echo "https://";
}
$_prefixVariable5=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Facebook'),$_smarty_tpl ) );
$_prefixVariable6=ob_get_clean();
$_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable5.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['facebook']),'class'=>"sociala btn-facebook btn-sm",'aria'=>array('label'=>$_prefixVariable6),'title'=>"Facebook",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin13->render(array('href'=>$_prefixVariable5.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['facebook']),'class'=>"sociala btn-facebook btn-sm",'aria'=>array('label'=>$_prefixVariable6),'title'=>"Facebook",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <span class="fab fa-facebook-f fa-fw fa-lg"></span>
                                                <?php $_block_repeat=false;
echo $_block_plugin13->render(array('href'=>$_prefixVariable5.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['facebook']),'class'=>"sociala btn-facebook btn-sm",'aria'=>array('label'=>$_prefixVariable6),'title'=>"Facebook",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['twitter'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['twitter'],'http') !== 0) {
echo "https://";
}
$_prefixVariable7=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Twitter'),$_smarty_tpl ) );
$_prefixVariable8=ob_get_clean();
$_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable7.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['twitter']),'class'=>"sociala btn-twitter btn-sm",'aria'=>array('label'=>$_prefixVariable8),'title'=>"Twitter",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin14->render(array('href'=>$_prefixVariable7.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['twitter']),'class'=>"sociala btn-twitter btn-sm",'aria'=>array('label'=>$_prefixVariable8),'title'=>"Twitter",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-twitter fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin14->render(array('href'=>$_prefixVariable7.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['twitter']),'class'=>"sociala btn-twitter btn-sm",'aria'=>array('label'=>$_prefixVariable8),'title'=>"Twitter",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['youtube'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['youtube'],'http') !== 0) {
echo "https://";
}
$_prefixVariable9=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'YouTube'),$_smarty_tpl ) );
$_prefixVariable10=ob_get_clean();
$_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable9.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['youtube']),'class'=>"sociala btn-youtube btn-sm",'aria'=>array('label'=>$_prefixVariable10),'title'=>"YouTube",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin15->render(array('href'=>$_prefixVariable9.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['youtube']),'class'=>"sociala btn-youtube btn-sm",'aria'=>array('label'=>$_prefixVariable10),'title'=>"YouTube",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-youtube fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin15->render(array('href'=>$_prefixVariable9.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['youtube']),'class'=>"sociala btn-youtube btn-sm",'aria'=>array('label'=>$_prefixVariable10),'title'=>"YouTube",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['vimeo'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['vimeo'],'http') !== 0) {
echo "https://";
}
$_prefixVariable11=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Vimeo'),$_smarty_tpl ) );
$_prefixVariable12=ob_get_clean();
$_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable11.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['vimeo']),'class'=>"sociala btn-vimeo btn-sm",'aria'=>array('label'=>$_prefixVariable12),'title'=>"Vimeo",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin16->render(array('href'=>$_prefixVariable11.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['vimeo']),'class'=>"sociala btn-vimeo btn-sm",'aria'=>array('label'=>$_prefixVariable12),'title'=>"Vimeo",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-vimeo-v fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin16->render(array('href'=>$_prefixVariable11.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['vimeo']),'class'=>"sociala btn-vimeo btn-sm",'aria'=>array('label'=>$_prefixVariable12),'title'=>"Vimeo",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['pinterest'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['pinterest'],'http') !== 0) {
echo "https://";
}
$_prefixVariable13=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Pinterest'),$_smarty_tpl ) );
$_prefixVariable14=ob_get_clean();
$_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable13.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['pinterest']),'class'=>"sociala btn-pinterest btn-sm",'aria'=>array('label'=>$_prefixVariable14),'title'=>"Pinterest",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin17->render(array('href'=>$_prefixVariable13.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['pinterest']),'class'=>"sociala btn-pinterest btn-sm",'aria'=>array('label'=>$_prefixVariable14),'title'=>"Pinterest",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-pinterest-p fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin17->render(array('href'=>$_prefixVariable13.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['pinterest']),'class'=>"sociala btn-pinterest btn-sm",'aria'=>array('label'=>$_prefixVariable14),'title'=>"Pinterest",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['instagram'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['instagram'],'http') !== 0) {
echo "https://";
}
$_prefixVariable15=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Instagram'),$_smarty_tpl ) );
$_prefixVariable16=ob_get_clean();
$_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable15.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['instagram']),'class'=>"sociala btn-instagram btn-sm",'aria'=>array('label'=>$_prefixVariable16),'title'=>"Instagram",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin18->render(array('href'=>$_prefixVariable15.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['instagram']),'class'=>"sociala btn-instagram btn-sm",'aria'=>array('label'=>$_prefixVariable16),'title'=>"Instagram",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-instagram fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin18->render(array('href'=>$_prefixVariable15.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['instagram']),'class'=>"sociala btn-instagram btn-sm",'aria'=>array('label'=>$_prefixVariable16),'title'=>"Instagram",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['skype'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['skype'],'http') !== 0) {
echo "https://";
}
$_prefixVariable17=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Skype'),$_smarty_tpl ) );
$_prefixVariable18=ob_get_clean();
$_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable17.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['skype']),'class'=>"sociala btn-skype btn-sm",'aria'=>array('label'=>$_prefixVariable18),'title'=>"Skype",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin19->render(array('href'=>$_prefixVariable17.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['skype']),'class'=>"sociala btn-skype btn-sm",'aria'=>array('label'=>$_prefixVariable18),'title'=>"Skype",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-skype fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin19->render(array('href'=>$_prefixVariable17.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['skype']),'class'=>"sociala btn-skype btn-sm",'aria'=>array('label'=>$_prefixVariable18),'title'=>"Skype",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['xing'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['xing'],'http') !== 0) {
echo "https://";
}
$_prefixVariable19=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Xing'),$_smarty_tpl ) );
$_prefixVariable20=ob_get_clean();
$_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable19.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['xing']),'class'=>"sociala btn-xing btn-sm",'aria'=>array('label'=>$_prefixVariable20),'title'=>"Xing",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin20->render(array('href'=>$_prefixVariable19.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['xing']),'class'=>"sociala btn-xing btn-sm",'aria'=>array('label'=>$_prefixVariable20),'title'=>"Xing",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-xing fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin20->render(array('href'=>$_prefixVariable19.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['xing']),'class'=>"sociala btn-xing btn-sm",'aria'=>array('label'=>$_prefixVariable20),'title'=>"Xing",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['linkedin'])) {?>
                                            <li>
                                                <?php ob_start();
if (strpos($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['linkedin'],'http') !== 0) {
echo "https://";
}
$_prefixVariable21=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'visit_us_on','section'=>'aria','printf'=>'Linkedin'),$_smarty_tpl ) );
$_prefixVariable22=ob_get_clean();
$_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable21.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['linkedin']),'class'=>"sociala btn-linkedin btn-sm",'aria'=>array('label'=>$_prefixVariable22),'title'=>"Linkedin",'target'=>"_blank",'rel'=>"noopener"));
$_block_repeat=true;
echo $_block_plugin21->render(array('href'=>$_prefixVariable21.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['linkedin']),'class'=>"sociala btn-linkedin btn-sm",'aria'=>array('label'=>$_prefixVariable22),'title'=>"Linkedin",'target'=>"_blank",'rel'=>"noopener"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <i class="fab fa-linkedin-in fa-fw fa-lg"></i>
                                                <?php $_block_repeat=false;
echo $_block_plugin21->render(array('href'=>$_prefixVariable21.((string)$_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['linkedin']),'class'=>"sociala btn-linkedin btn-sm",'aria'=>array('label'=>$_prefixVariable22),'title'=>"Linkedin",'target'=>"_blank",'rel'=>"noopener"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </li>
                                        <?php }?>
                                        </ul>
                                        </div>
              </div>
          
                <?php
}
}
/* {/block 'layout-footer-copyright'} */
/* {block 'layout-footer-scroll-top'} */
class Block_246039174619cafd89d3351_73957016 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['theme']['button_scroll_top'] === 'Y') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:snippets/scroll_top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>
                <?php
}
}
/* {/block 'layout-footer-scroll-top'} */
/* {block 'layout-footer-content'} */
class Block_1122759211619cafd897b5a3_01710879 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (!$_smarty_tpl->tpl_vars['bExclusive']->value) {?>
            <?php $_smarty_tpl->_assignInScope('newsletterActive', $_smarty_tpl->tpl_vars['Einstellungen']->value['template']['footer']['newsletter_footer'] === 'Y' && $_smarty_tpl->tpl_vars['Einstellungen']->value['newsletter']['newsletter_active'] === 'Y');?>
                
               
                <div class="addfoot">Beim Kauf per Vorkasse gewähren wir auf den Warenwert einen Vertrauensbonus von 5%!</div>
                 <footer id="footer" <?php if ($_smarty_tpl->tpl_vars['newsletterActive']->value) {?>class="newsletter-active"<?php }?>>
               
                <div class="nfoot d-print-none">
                   
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_23751010619cafd8982860_40068656', 'layout-footer-boxes', $this->tplIndex);
?>

                    
                    
                     <?php if ($_smarty_tpl->tpl_vars['newsletterActive']->value) {?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1594691198619cafd89a0e50_58202313', 'layout-footer-newsletter', $this->tplIndex);
?>

                    <?php }?>
                    

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_262346201619cafd89abb74_66543185', 'layout-footer-additional', $this->tplIndex);
?>
                    
                  </div>
             
              <!-- BEGIN Now copyright contains old copyright panel. Oborik 29 oktober -->
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_883263833619cafd89b2da3_58460176', 'layout-footer-copyright', $this->tplIndex);
?>

            <!-- END Now copyright contains old copyright panel. twenty/up Oborik 29 oktober -->
                
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_246039174619cafd89d3351_73957016', 'layout-footer-scroll-top', $this->tplIndex);
?>

            </footer>
                    <?php }?>
    <?php
}
}
/* {/block 'layout-footer-content'} */
/* {block 'layout-footer-io-path'} */
class Block_1452058754619cafd89e0779_44165215 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div id="jtl-io-path" data-path="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
" class="d-none"></div>
    <?php
}
}
/* {/block 'layout-footer-io-path'} */
/* {block 'layout-footer-js'} */
class Block_2069650367619cafd89e10f2_46902307 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo $_smarty_tpl->tpl_vars['dbgBarBody']->value;?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['captchaMarkup'][0], array( array('getBody'=>false),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'layout-footer-js'} */
/* {block 'layout-footer-consent-manager'} */
class Block_800075384619cafd89e1ca9_48823115 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['consentmanager']['consent_manager_active'] === 'Y' && !$_smarty_tpl->tpl_vars['isAjax']->value && $_smarty_tpl->tpl_vars['consentItems']->value->isNotEmpty()) {?>
            <input id="consent-manager-show-banner" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['consentmanager']['consent_manager_show_banner'];?>
">
            <?php $_smarty_tpl->_subTemplateRender('file:snippets/consent_manager.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php $_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'inlineScript'))) {
throw new SmartyException('block tag \'inline_script\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inline_script', array());
$_block_repeat=true;
echo $_block_plugin22->inlineScript(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php echo '<script'; ?>
>
                    setTimeout(function() {
                        $('#consent-manager, #consent-settings-btn').removeClass('d-none');
                    }, 100)
                    window.CM = new ConsentManager({
                        version: 1
                    });
                    var trigger = document.querySelectorAll('.trigger')
                    var triggerCall = function(e) {
                        e.preventDefault();
                        let type = e.target.dataset.consent;
                        if (CM.getSettings(type) === false) {
                            CM.openConfirmationModal(type, function() {
                                let data = CM._getLocalData();
                                if (data === null ) {
                                    data = { settings: {} };
                                }
                                data.settings[type] = true;
                                document.dispatchEvent(new CustomEvent('consent.updated', { detail: data.settings }));
                            });
                        }
                    }
                    for(let i = 0; i < trigger.length; ++i) {
                        trigger[i].addEventListener('click', triggerCall)
                    }
                    document.addEventListener('consent.updated', function(e) {
                        $.post('<?php echo $_smarty_tpl->tpl_vars['ShopURLSSL']->value;?>
/', {
                                'action': 'updateconsent',
                                'jtl_token': '<?php echo $_SESSION['jtl_token'];?>
',
                                'data': e.detail
                            }
                        );
                    });
                <?php echo '</script'; ?>
>
            <?php $_block_repeat=false;
echo $_block_plugin22->inlineScript(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php }?>
    <?php
}
}
/* {/block 'layout-footer-consent-manager'} */
/* {block 'layout-footer'} */
class Block_584070476619cafd89414d8_43840313 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'layout-footer' => 
  array (
    0 => 'Block_584070476619cafd89414d8_43840313',
  ),
  'layout-footer-content-all-closingtags' => 
  array (
    0 => 'Block_1466968377619cafd8942447_30777993',
  ),
  'layout-footer-aside' => 
  array (
    0 => 'Block_428701393619cafd8942a34_61374440',
  ),
  'layout-footer-content-productlist-col-closingtag' => 
  array (
    0 => 'Block_942574665619cafd896f082_22089229',
  ),
  'layout-footer-sidepanel-left' => 
  array (
    0 => 'Block_1454603224619cafd89700b7_61156767',
  ),
  'footer-sidepanel-left-content' => 
  array (
    0 => 'Block_2128683561619cafd8970476_04740418',
  ),
  'layout-footer-content-productlist-row-closingtag' => 
  array (
    0 => 'Block_1164773273619cafd8978148_45675747',
  ),
  'layout-footer-content-closingtag' => 
  array (
    0 => 'Block_238011443619cafd8978d87_85482250',
  ),
  'layout-footer-content-wrapper-closingtag' => 
  array (
    0 => 'Block_756818634619cafd8979b52_43427440',
  ),
  'layout-footer-main-wrapper-closingtag' => 
  array (
    0 => 'Block_1826837539619cafd897a425_20917140',
  ),
  'layout-footer-content' => 
  array (
    0 => 'Block_1758545103619cafd897acf5_85800203',
    1 => 'Block_1122759211619cafd897b5a3_01710879',
  ),
  'layout-footer-boxes' => 
  array (
    0 => 'Block_23751010619cafd8982860_40068656',
  ),
  'layout-footer-newsletter' => 
  array (
    0 => 'Block_1594691198619cafd89a0e50_58202313',
  ),
  'layout-footer-newsletter-info' => 
  array (
    0 => 'Block_252221199619cafd89a2857_28224247',
  ),
  'layout-footer-form' => 
  array (
    0 => 'Block_788591234619cafd89a4381_12912047',
  ),
  'layout-footer-form-content' => 
  array (
    0 => 'Block_814550777619cafd89a52b7_35004108',
  ),
  'layout-footer-form-captcha' => 
  array (
    0 => 'Block_779667291619cafd89a93d9_67008873',
  ),
  'layout-footer-additional' => 
  array (
    0 => 'Block_262346201619cafd89abb74_66543185',
  ),
  'layout-footer-socialmedia' => 
  array (
    0 => 'Block_1564869417619cafd89acbc9_08161190',
  ),
  'layout-footer-copyright' => 
  array (
    0 => 'Block_883263833619cafd89b2da3_58460176',
  ),
  'layout-footer-scroll-top' => 
  array (
    0 => 'Block_246039174619cafd89d3351_73957016',
  ),
  'layout-footer-io-path' => 
  array (
    0 => 'Block_1452058754619cafd89e0779_44165215',
  ),
  'layout-footer-js' => 
  array (
    0 => 'Block_2069650367619cafd89e10f2_46902307',
  ),
  'layout-footer-consent-manager' => 
  array (
    0 => 'Block_800075384619cafd89e1ca9_48823115',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>





    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1466968377619cafd8942447_30777993', 'layout-footer-content-all-closingtags', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1826837539619cafd897a425_20917140', 'layout-footer-main-wrapper-closingtag', $this->tplIndex);
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1758545103619cafd897acf5_85800203', 'layout-footer-content', $this->tplIndex);
?>
 <!-- Block layout-footer-content' prepend -->








    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1122759211619cafd897b5a3_01710879', 'layout-footer-content', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1452058754619cafd89e0779_44165215', 'layout-footer-io-path', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2069650367619cafd89e10f2_46902307', 'layout-footer-js', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_800075384619cafd89e1ca9_48823115', 'layout-footer-consent-manager', $this->tplIndex);
?>

    
    
 
    <?php echo '<script'; ?>
> 
  $(function() {

    $(".dropdown-toggle").click(function(e) {
        var cn = $(this).text();

        $('.append_name').html(cn);

    }); 

    $(".nav-offcanvas-title").click(function(e) {
        $('.append_name').text('');

    });


    $(".search_brands").on("keyup", function() {

        var value = $(this).val().toLowerCase();

        $(".row .mi_mobile").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            $('.modal').hide();

        });

    });

 <?php if ($_smarty_tpl->tpl_vars['nSeitenTyp']->value !== (defined('PAGE_BESTELLVORGANG') ? constant('PAGE_BESTELLVORGANG') : null)) {?>
    $(document).ready(function() {   // SCRAP and include in header menu
setTimeout( function() {
        var cont = '<li class="navbar-nav loaded-from-footer"></li>';
      //  var soc = $('.footer-additional-wrapper').html();
        var soc = $('.ready-for-loader').html();
     //   var top = $('.top-bar').html();
        var topcont = '<li class=loaded-from-top>';
        var arrow = '<div class="mob_disabled"><i class="fal fa-long-arrow-right" aria-hidden="true"></i>';
        if (soc.length) {

            $('.nav-scrollbar-inner').append(cont);
            $('.loaded-from-footer').append(soc);
            $('.loaded-from-footer').find('.footnote-vat').remove();


        }
   //     $('.nav-scrollbar-inner').append(topcont);
      //  $('.loaded-from-top').append(top);
        $('.loaded-from-top').find('a').addClass('nav-link');
        $('.loaded-from-top').find('.nav-link').append(arrow);
        $('.loaded-from-top').find('li').removeClass().addClass('nav-item nav-scrollbar-item');
        $('.loaded-from-top').find('ul').removeClass().addClass('navbar-nav nav-scrollbar-inner mr-auto');


        $('.loaded-from-top').find('.top-circle-label').addClass('circle-label').removeClass('top-circle-label');

}, 7000);
    }); // SCRAP END
<?php }?>
});

/*
// Sticky header
$(document).ready(function() {
    var leng = 370;
    if ($(window).width() > leng) {

        $(window).scroll(function() {
        // Get content
            var sticky = $('.navbar'),
              //  sr = $('#search').html(),
                tt = $('.sticky').length,
                ss = $('.accountflex').clone(),
               // borik = $('.uhr24logo').clone(),
                 borik = $('.logo-wrapper').clone(),
                scroll = $(window).scrollTop();


            if (scroll >= 145) {

                sticky.addClass('sticky');

                if (tt < 1) {
            
 <?php if ($_smarty_tpl->tpl_vars['nSeitenTyp']->value !== (defined('PAGE_BESTELLVORGANG') ? constant('PAGE_BESTELLVORGANG') : null)) {?>
              //      sticky.append(sr);
                    sticky.append(ss);
                <?php }?>
                    sticky.prepend(borik);
                }

            } else {

                sticky.removeClass('sticky');
              //  sticky.find('.search-wrapper').remove();
                sticky.find('.accountflex').remove();
                sticky.find('.logo-wrapper').remove();
            }


        });
    }

});
           */
     <?php echo '</script'; ?>
>                

<!-- Oborik 2 november TIME DELAY -->
<?php echo '<script'; ?>
>



/*
			
var po = document.createElement("script");
po.type = "text/javascript"; po.async = true;
po.src = "/plugins/wnm_order_countdown/frontend/js/countdown.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(po, s);
console.log (po);
*/



$(document).ready(function(){
setTimeout( function() {
var gplusjs = '(function() {var po = document.createElement("script");po.type = "text/javascript"; po.async = true;po.src = "/plugins/wnm_order_countdown/frontend/js/countdown.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po, s);})();';
var script = document.createElement("script");
script.textContent = gplusjs;
document.head.appendChild(script);
// console.log (script);
}, 500);
return false;
});

/*
$(document).ready(function(){
setTimeout( function() {
 $.ajax({
 type: 'POST',
 dataType: 'html',
 url: '/get_file.php',
 success: function(data) {
              alert ('loaded');   
  $( ".result" ).html( data );
                     
                },
            });



}, 2000);
return false;
});

*/
/*
$(document).ready(function(){
setTimeout( function() {
createCountDown("2021-11-03T13:00:00", "60", "#alert_versand_countdown", "#countdownmsg", "alert alert-success text-center", "alert alert-danger text-center", "", "", "hours", "minutes", "seconds");
	console.log ('script');
}, 1000);
});

*/

 

<?php echo '</script'; ?>
>




<div class="result"></div>
    
    </body>
    </html>
<?php
}
}
/* {/block 'layout-footer'} */
}
