<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:46:51
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/my_account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd4ab361975_03220110',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2496efa480de5755e8ec4be1fff476dd324a7e98' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/my_account.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:account/downloads.tpl' => 1,
  ),
),false)) {
function content_619cd4ab361975_03220110 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1545779214619cd4ab317e66_03848068', 'account-my-account');
?>

<?php }
/* {block 'heading'} */
class Block_1259158493619cd4ab318927_21946648 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div class="h2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'welcome','section'=>'login'),$_smarty_tpl ) );?>
 <?php echo $_SESSION['Kunde']->cVorname;?>
 <?php echo $_SESSION['Kunde']->cNachname;?>
</div>
    <?php
}
}
/* {/block 'heading'} */
/* {block 'account-my-account-alert'} */
class Block_1627465517619cd4ab3233c6_55661162 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'myAccountDesc','section'=>'login'),$_smarty_tpl ) );?>

                <?php
}
}
/* {/block 'account-my-account-alert'} */
/* {block 'account-my-account-account-credit'} */
class Block_2033062197619cd4ab324623_49118668 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin4 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin4, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>'account-head-data-credit'));
$_block_repeat=true;
echo $_block_plugin4->render(array('class'=>'account-head-data-credit'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourMoneyOnAccount','section'=>'login'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cGuthabenLocalized;?>

                    <?php $_block_repeat=false;
echo $_block_plugin4->render(array('class'=>'account-head-data-credit'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'account-my-account-account-credit'} */
/* {block 'account-my-account-head-data'} */
class Block_657691443619cd4ab31f933_55723497 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"account-head-data"));
$_block_repeat=true;
echo $_block_plugin1->render(array('class'=>"account-head-data"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6));
$_block_repeat=true;
echo $_block_plugin2->render(array('cols'=>12,'lg'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1627465517619cd4ab3233c6_55661162', 'account-my-account-alert', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin2->render(array('cols'=>12,'lg'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6));
$_block_repeat=true;
echo $_block_plugin3->render(array('cols'=>12,'lg'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2033062197619cd4ab324623_49118668', 'account-my-account-account-credit', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin3->render(array('cols'=>12,'lg'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_repeat=false;
echo $_block_plugin1->render(array('class'=>"account-head-data"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'account-my-account-head-data'} */
/* {block 'account-my-account-orders-content-header'} */
class Block_350459068619cd4ab327f40_71535434 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"align-items-center-util"));
$_block_repeat=true;
echo $_block_plugin9->render(array('class'=>"align-items-center-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin10->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <span class="h3">
                                            <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>'text-decoration-none-util','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?bestellungen=1"));
$_block_repeat=true;
echo $_block_plugin11->render(array('class'=>'text-decoration-none-util','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?bestellungen=1"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'myOrders'),$_smarty_tpl ) );?>

                                            <?php $_block_repeat=false;
echo $_block_plugin11->render(array('class'=>'text-decoration-none-util','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?bestellungen=1"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        </span>
                                    <?php $_block_repeat=false;
echo $_block_plugin10->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto font-size-sm"));
$_block_repeat=true;
echo $_block_plugin12->render(array('class'=>"col-auto font-size-sm"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?bestellungen=1"));
$_block_repeat=true;
echo $_block_plugin13->render(array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?bestellungen=1"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'showAll'),$_smarty_tpl ) );?>

                                        <?php $_block_repeat=false;
echo $_block_plugin13->render(array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?bestellungen=1"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin12->render(array('class'=>"col-auto font-size-sm"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin9->render(array('class'=>"align-items-center-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-my-account-orders-content-header'} */
/* {block 'account-my-account-orders-body'} */
class Block_34446510619cd4ab32c752_25590215 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <div class="table-responsive">
                                    <table class="table table-vertical-middle table-hover">
                                        <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellungen']->value, 'order');
$_smarty_tpl->tpl_vars['order']->index = -1;
$_smarty_tpl->tpl_vars['order']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->do_else = false;
$_smarty_tpl->tpl_vars['order']->index++;
$__foreach_order_1_saved = $_smarty_tpl->tpl_vars['order'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['order']->index === 5) {
break 1;
}?>
                                            <tr title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'showOrder','section'=>'login'),$_smarty_tpl ) );?>
: <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderNo','section'=>'login'),$_smarty_tpl ) );?>
 <?php echo $_smarty_tpl->tpl_vars['order']->value->cBestellNr;?>
"
                                                class="clickable-row cursor-pointer"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-boundary="window"
                                                data-href="<?php echo $_smarty_tpl->tpl_vars['cCanonicalURL']->value;?>
?bestellung=<?php echo $_smarty_tpl->tpl_vars['order']->value->kBestellung;?>
">
                                                <td><?php echo $_smarty_tpl->tpl_vars['order']->value->dBestelldatum;?>
</td>
                                                <td class="text-right-util"><?php echo $_smarty_tpl->tpl_vars['order']->value->cBestellwertLocalized;?>
</td>
                                                <td class="text-right-util">
                                                   <?php echo $_smarty_tpl->tpl_vars['order']->value->Status;?>

                                                </td>
                                                <td class="text-right-util d-none d-md-table-cell">
                                                    <i class="fa fa-eye"></i>
                                                </td>
                                            </tr>
                                        <?php
$_smarty_tpl->tpl_vars['order'] = $__foreach_order_1_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php
}
}
/* {/block 'account-my-account-orders-body'} */
/* {block 'account-my-account-orders-content-nodata'} */
class Block_1028931837619cd4ab3370c1_43773392 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array());
$_block_repeat=true;
echo $_block_plugin14->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noOrdersYet','section'=>'account data'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin14->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-my-account-orders-content-nodata'} */
/* {block 'account-my-account-orders-content'} */
class Block_1451915248619cd4ab327257_76861256 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin7 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin7, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true));
$_block_repeat=true;
echo $_block_plugin7->render(array('no-body'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin8->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_350459068619cd4ab327f40_71535434', 'account-my-account-orders-content-header', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin8->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php if (count($_smarty_tpl->tpl_vars['Bestellungen']->value) > 0) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_34446510619cd4ab32c752_25590215', 'account-my-account-orders-body', $this->tplIndex);
?>

                        <?php } else { ?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1028931837619cd4ab3370c1_43773392', 'account-my-account-orders-content-nodata', $this->tplIndex);
?>

                        <?php }?>
                    <?php $_block_repeat=false;
echo $_block_plugin7->render(array('no-body'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'account-my-account-orders-content'} */
/* {block 'account-my-account-billing-address-header'} */
class Block_1409065170619cd4ab3398f1_08614692 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"align-items-center-util"));
$_block_repeat=true;
echo $_block_plugin18->render(array('class'=>"align-items-center-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin19->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <span class="h3">
                                            <?php $_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>'text-decoration-none-util','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1"));
$_block_repeat=true;
echo $_block_plugin20->render(array('class'=>'text-decoration-none-util','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'myPersonalData'),$_smarty_tpl ) );?>

                                            <?php $_block_repeat=false;
echo $_block_plugin20->render(array('class'=>'text-decoration-none-util','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        </span>
                                    <?php $_block_repeat=false;
echo $_block_plugin19->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-auto font-size-sm"));
$_block_repeat=true;
echo $_block_plugin21->render(array('class'=>"col-auto font-size-sm"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1"));
$_block_repeat=true;
echo $_block_plugin22->render(array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'showAll'),$_smarty_tpl ) );?>

                                        <?php $_block_repeat=false;
echo $_block_plugin22->render(array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin21->render(array('class'=>"col-auto font-size-sm"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin18->render(array('class'=>"align-items-center-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-my-account-billing-address-header'} */
/* {block 'account-my-account-billing-address-billing-address'} */
class Block_983233503619cd4ab33cc47_82869653 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <tr>
                                            <td class="min-w-sm">
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'billingAdress','section'=>'account data'),$_smarty_tpl ) );?>

                                                <small class="text-muted-util d-block"><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>
, <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>
, <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;?>
</small>
                                            </td>
                                            <td class="text-right-util">
                                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'editBillingAdress','section'=>'account data'),$_smarty_tpl ) );
$_prefixVariable1 = ob_get_clean();
$_block_plugin23 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin23, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1",'aria'=>array("label"=>$_prefixVariable1)));
$_block_repeat=true;
echo $_block_plugin23->render(array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1",'aria'=>array("label"=>$_prefixVariable1)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <span class="fas fa-pencil-alt"></span>
                                                <?php $_block_repeat=false;
echo $_block_plugin23->render(array('href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1",'aria'=>array("label"=>$_prefixVariable1)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </td>
                                        </tr>
                                    <?php
}
}
/* {/block 'account-my-account-billing-address-billing-address'} */
/* {block 'account-my-account-billing-address-contact'} */
class Block_2146225974619cd4ab3406f7_41443230 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <tr>
                                            <td class="min-w-sm">
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'contactInformation','section'=>'account data'),$_smarty_tpl ) );?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'and'),$_smarty_tpl ) );?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'email','section'=>'account data'),$_smarty_tpl ) );?>

                                                <small class="text-muted-util d-block"><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMail;?>
</small>
                                            </td>
                                            <td class="text-right-util">
                                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'editCustomerData','section'=>'account data'),$_smarty_tpl ) );
$_prefixVariable2 = ob_get_clean();
$_block_plugin24 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin24, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>'float-right','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1",'aria'=>array("label"=>$_prefixVariable2)));
$_block_repeat=true;
echo $_block_plugin24->render(array('class'=>'float-right','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1",'aria'=>array("label"=>$_prefixVariable2)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <span class="fas fa-pencil-alt"></span>
                                                <?php $_block_repeat=false;
echo $_block_plugin24->render(array('class'=>'float-right','href'=>((string)$_smarty_tpl->tpl_vars['cCanonicalURL']->value)."?editRechnungsadresse=1",'aria'=>array("label"=>$_prefixVariable2)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </td>
                                        </tr>
                                    <?php
}
}
/* {/block 'account-my-account-billing-address-contact'} */
/* {block 'account-my-account-billing-address-password'} */
class Block_1541251267619cd4ab3431d6_29014283 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <tr>
                                            <td class="min-w-sm">
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'password','section'=>'account data'),$_smarty_tpl ) );?>

                                            </td>
                                            <td class="text-right-util">
                                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php','params'=>array('pass'=>1)),$_smarty_tpl ) );
$_prefixVariable3=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'changePassword','section'=>'login'),$_smarty_tpl ) );
$_prefixVariable4 = ob_get_clean();
$_block_plugin25 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin25, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable3,'aria'=>array("label"=>$_prefixVariable4)));
$_block_repeat=true;
echo $_block_plugin25->render(array('href'=>$_prefixVariable3,'aria'=>array("label"=>$_prefixVariable4)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <span class="fas fa-pencil-alt"></span>
                                                <?php $_block_repeat=false;
echo $_block_plugin25->render(array('href'=>$_prefixVariable3,'aria'=>array("label"=>$_prefixVariable4)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </td>
                                        </tr>
                                    <?php
}
}
/* {/block 'account-my-account-billing-address-password'} */
/* {block 'account-my-account-billing-address-body'} */
class Block_385642974619cd4ab33c8b6_51150318 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <div class="table-responsive">
                                <table class="table table-vertical-middle table-hover">
                                    <tbody>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_983233503619cd4ab33cc47_82869653', 'account-my-account-billing-address-billing-address', $this->tplIndex);
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2146225974619cd4ab3406f7_41443230', 'account-my-account-billing-address-contact', $this->tplIndex);
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1541251267619cd4ab3431d6_29014283', 'account-my-account-billing-address-password', $this->tplIndex);
?>

                                </table>
                            </div>
                        <?php
}
}
/* {/block 'account-my-account-billing-address-body'} */
/* {block 'account-my-account-billing-address'} */
class Block_606075668619cd4ab339028_50317787 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true));
$_block_repeat=true;
echo $_block_plugin16->render(array('no-body'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin17->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1409065170619cd4ab3398f1_08614692', 'account-my-account-billing-address-header', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin17->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_385642974619cd4ab33c8b6_51150318', 'account-my-account-billing-address-body', $this->tplIndex);
?>

                    <?php $_block_repeat=false;
echo $_block_plugin16->render(array('no-body'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'account-my-account-billing-address'} */
/* {block 'account-my-account-wishlist-header'} */
class Block_1659536754619cd4ab347bd9_29798005 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <span class="h3">
                                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'wunschliste.php'),$_smarty_tpl ) );
$_prefixVariable5=ob_get_clean();
$_block_plugin30 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin30, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>'text-decoration-none-util','href'=>$_prefixVariable5));
$_block_repeat=true;
echo $_block_plugin30->render(array('class'=>'text-decoration-none-util','href'=>$_prefixVariable5), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'myWishlists'),$_smarty_tpl ) );?>

                                    <?php $_block_repeat=false;
echo $_block_plugin30->render(array('class'=>'text-decoration-none-util','href'=>$_prefixVariable5), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                </span>
                            <?php
}
}
/* {/block 'account-my-account-wishlist-header'} */
/* {block 'account-my-account-wishlist-name'} */
class Block_651510925619cd4ab34a643_52097193 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <td>
                                                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'wunschliste.php'),$_smarty_tpl ) );
$_prefixVariable6=ob_get_clean();
$_block_plugin31 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin31, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable6."?wl=".((string)$_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste)));
$_block_repeat=true;
echo $_block_plugin31->render(array('href'=>$_prefixVariable6."?wl=".((string)$_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['wishlist']->value->cName;
$_block_repeat=false;
echo $_block_plugin31->render(array('href'=>$_prefixVariable6."?wl=".((string)$_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?><br />
                                                    <small><?php echo $_smarty_tpl->tpl_vars['wishlist']->value->productCount;?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'products'),$_smarty_tpl ) );?>
</small>
                                                </td>
                                            <?php
}
}
/* {/block 'account-my-account-wishlist-name'} */
/* {block 'account-my-account-wishlist-visibility'} */
class Block_268301284619cd4ab34c884_47787785 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <td class="text-right-util">
                                                    <div class="d-inline-flex flex-nowrap">
                                                        <span data-switch-label-state="public-<?php echo $_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste;?>
" class="<?php if ($_smarty_tpl->tpl_vars['wishlist']->value->nOeffentlich != 1) {?>d-none<?php }?>">
                                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'public'),$_smarty_tpl ) );?>

                                                        </span>
                                                        <span data-switch-label-state="private-<?php echo $_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste;?>
" class="<?php if ($_smarty_tpl->tpl_vars['wishlist']->value->nOeffentlich == 1) {?>d-none<?php }?>">
                                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'private'),$_smarty_tpl ) );?>

                                                        </span>
                                                        <div class="custom-control custom-switch">
                                                            <input type='checkbox'
                                                                   class='custom-control-input wl-visibility-switch'
                                                                   id="wl-visibility-<?php echo $_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste;?>
"
                                                                   data-wl-id="<?php echo $_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste;?>
"
                                                                   <?php if ($_smarty_tpl->tpl_vars['wishlist']->value->nOeffentlich == 1) {?>checked<?php }?>
                                                                   aria-label="<?php if ($_smarty_tpl->tpl_vars['wishlist']->value->nOeffentlich == 1) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'wishlistNoticePublic','section'=>'login'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'wishlistNoticePrivate','section'=>'login'),$_smarty_tpl ) );
}?>"
                                                            >
                                                            <label class="custom-control-label" for="wl-visibility-<?php echo $_smarty_tpl->tpl_vars['wishlist']->value->kWunschliste;?>
"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            <?php
}
}
/* {/block 'account-my-account-wishlist-visibility'} */
/* {block 'account-my-account-wishlists'} */
class Block_1028775798619cd4ab349737_31946807 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <div class="table-responsive">
                                <table class="table table-vertical-middle table-hover">
                                    <tbody>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oWunschliste_arr']->value, 'wishlist');
$_smarty_tpl->tpl_vars['wishlist']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['wishlist']->value) {
$_smarty_tpl->tpl_vars['wishlist']->do_else = false;
?>
                                        <tr>
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_651510925619cd4ab34a643_52097193', 'account-my-account-wishlist-name', $this->tplIndex);
?>

                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_268301284619cd4ab34c884_47787785', 'account-my-account-wishlist-visibility', $this->tplIndex);
?>

                                        </tr>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
}
}
/* {/block 'account-my-account-wishlists'} */
/* {block 'account-my-account-wishlist-no-data'} */
class Block_186918993619cd4ab3524c1_81672316 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin32 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin32, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array());
$_block_repeat=true;
echo $_block_plugin32->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noWishlist','section'=>'account data'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin32->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-my-account-wishlist-no-data'} */
/* {block 'account-my-account-wishlist-content'} */
class Block_899292277619cd4ab3471f9_09223927 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin28 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin28, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'id'=>'my-wishlists'));
$_block_repeat=true;
echo $_block_plugin28->render(array('no-body'=>true,'id'=>'my-wishlists'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin29 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin29, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin29->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1659536754619cd4ab347bd9_29798005', 'account-my-account-wishlist-header', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin29->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php if (count($_smarty_tpl->tpl_vars['oWunschliste_arr']->value) > 0) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1028775798619cd4ab349737_31946807', 'account-my-account-wishlists', $this->tplIndex);
?>

                        <?php } else { ?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_186918993619cd4ab3524c1_81672316', 'account-my-account-wishlist-no-data', $this->tplIndex);
?>

                        <?php }?>
                    <?php $_block_repeat=false;
echo $_block_plugin28->render(array('no-body'=>true,'id'=>'my-wishlists'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'account-my-account-wishlist-content'} */
/* {block 'account-my-account-comparelist-header'} */
class Block_1441955007619cd4ab355785_96266911 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <span class="h3">
                                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'vergleichsliste.php'),$_smarty_tpl ) );
$_prefixVariable7=ob_get_clean();
$_block_plugin36 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin36, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>'text-decoration-none-util','href'=>$_prefixVariable7));
$_block_repeat=true;
echo $_block_plugin36->render(array('class'=>'text-decoration-none-util','href'=>$_prefixVariable7), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'myCompareList'),$_smarty_tpl ) );?>

                                    <?php $_block_repeat=false;
echo $_block_plugin36->render(array('class'=>'text-decoration-none-util','href'=>$_prefixVariable7), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                </span>
                            <?php
}
}
/* {/block 'account-my-account-comparelist-header'} */
/* {block 'account-my-account-comparelist-body'} */
class Block_259993552619cd4ab356ea5_97727832 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <p>
                                    <?php if (count($_smarty_tpl->tpl_vars['compareList']->value->oArtikel_arr) > 0) {?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'compareListItemCount','section'=>'account data','printf'=>count($_smarty_tpl->tpl_vars['compareList']->value->oArtikel_arr)),$_smarty_tpl ) );?>

                                    <?php } else { ?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'compareListNoItems'),$_smarty_tpl ) );?>

                                    <?php }?>
                                </p>
                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'vergleichsliste.php'),$_smarty_tpl ) );
$_prefixVariable8=ob_get_clean();
$_block_plugin38 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin38, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"btn btn-outline-secondary btn-sm",'href'=>$_prefixVariable8));
$_block_repeat=true;
echo $_block_plugin38->render(array('class'=>"btn btn-outline-secondary btn-sm",'href'=>$_prefixVariable8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'goToCompareList','section'=>'comparelist'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin38->render(array('class'=>"btn btn-outline-secondary btn-sm",'href'=>$_prefixVariable8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-my-account-comparelist-body'} */
/* {block 'account-my-account-comparelist'} */
class Block_2113709171619cd4ab354bb3_51301305 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin34 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin34, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'class'=>"account-comparelist"));
$_block_repeat=true;
echo $_block_plugin34->render(array('no-body'=>true,'class'=>"account-comparelist"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin35 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin35, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin35->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1441955007619cd4ab355785_96266911', 'account-my-account-comparelist-header', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin35->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_plugin37 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin37, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array());
$_block_repeat=true;
echo $_block_plugin37->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_259993552619cd4ab356ea5_97727832', 'account-my-account-comparelist-body', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin37->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin34->render(array('no-body'=>true,'class'=>"account-comparelist"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'account-my-account-comparelist'} */
/* {block 'account-my-account-account-data'} */
class Block_1880567967619cd4ab326521_80246185 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_block_plugin5 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin5, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin5->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php $_block_plugin6 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin6, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-orders"));
$_block_repeat=true;
echo $_block_plugin6->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-orders"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1451915248619cd4ab327257_76861256', 'account-my-account-orders-content', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin6->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-orders"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-address"));
$_block_repeat=true;
echo $_block_plugin15->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-address"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_606075668619cd4ab339028_50317787', 'account-my-account-billing-address', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin15->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-address"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_repeat=false;
echo $_block_plugin5->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

        <?php $_block_plugin26 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin26, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin26->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_wunschliste_anzeigen'] === 'Y') {?>
            <?php $_block_plugin27 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin27, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-wishlist"));
$_block_repeat=true;
echo $_block_plugin27->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-wishlist"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_899292277619cd4ab3471f9_09223927', 'account-my-account-wishlist-content', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin27->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-wishlist"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['vergleichsliste']['vergleichsliste_anzeigen'] === 'Y') {?>
            <?php $_block_plugin33 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin33, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-comparelist"));
$_block_repeat=true;
echo $_block_plugin33->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-comparelist"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2113709171619cd4ab354bb3_51301305', 'account-my-account-comparelist', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin33->render(array('cols'=>12,'lg'=>6,'class'=>"account-data-item account-data-item-comparelist"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php }?>
        <?php $_block_repeat=false;
echo $_block_plugin26->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'account-my-account-account-data'} */
/* {block 'account-my-account-include-downloads'} */
class Block_1672859258619cd4ab35b2d9_31297411 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:account/downloads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'account-my-account-include-downloads'} */
/* {block 'account-my-account-actions'} */
class Block_1736406406619cd4ab35d189_74018718 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_block_plugin39 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin39, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"btn-row"));
$_block_repeat=true;
echo $_block_plugin39->render(array('class'=>"btn-row"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php $_block_plugin40 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin40, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>"auto",'cols'=>12));
$_block_repeat=true;
echo $_block_plugin40->render(array('md'=>"auto",'cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php','params'=>array('del'=>1)),$_smarty_tpl ) );
$_prefixVariable9=ob_get_clean();
$_block_plugin41 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin41, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>'btn btn-outline-danger btn-back','href'=>$_prefixVariable9));
$_block_repeat=true;
echo $_block_plugin41->render(array('class'=>'btn btn-outline-danger btn-back','href'=>$_prefixVariable9), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <span class="fa fa-chain-broken"></span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'deleteAccount','section'=>'login'),$_smarty_tpl ) );?>

                <?php $_block_repeat=false;
echo $_block_plugin41->render(array('class'=>'btn btn-outline-danger btn-back','href'=>$_prefixVariable9), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_repeat=false;
echo $_block_plugin40->render(array('md'=>"auto",'cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_plugin42 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin42, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>"auto",'cols'=>12,'class'=>"ml-auto-util"));
$_block_repeat=true;
echo $_block_plugin42->render(array('md'=>"auto",'cols'=>12,'class'=>"ml-auto-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php'),$_smarty_tpl ) );
$_prefixVariable10=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'logOut'),$_smarty_tpl ) );
$_prefixVariable11=ob_get_clean();
$_block_plugin43 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin43, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable10."?logout=1",'title'=>$_prefixVariable11,'class'=>"btn btn-primary btn-block min-w-sm"));
$_block_repeat=true;
echo $_block_plugin43->render(array('href'=>$_prefixVariable10."?logout=1",'title'=>$_prefixVariable11,'class'=>"btn btn-primary btn-block min-w-sm"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <span class="fa fa-sign-out-alt"></span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'logOut'),$_smarty_tpl ) );?>

                <?php $_block_repeat=false;
echo $_block_plugin43->render(array('href'=>$_prefixVariable10."?logout=1",'title'=>$_prefixVariable11,'class'=>"btn btn-primary btn-block min-w-sm"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_repeat=false;
echo $_block_plugin42->render(array('md'=>"auto",'cols'=>12,'class'=>"ml-auto-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_repeat=false;
echo $_block_plugin39->render(array('class'=>"btn-row"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'account-my-account-actions'} */
/* {block 'account-my-account'} */
class Block_1545779214619cd4ab317e66_03848068 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'account-my-account' => 
  array (
    0 => 'Block_1545779214619cd4ab317e66_03848068',
  ),
  'heading' => 
  array (
    0 => 'Block_1259158493619cd4ab318927_21946648',
  ),
  'account-my-account-head-data' => 
  array (
    0 => 'Block_657691443619cd4ab31f933_55723497',
  ),
  'account-my-account-alert' => 
  array (
    0 => 'Block_1627465517619cd4ab3233c6_55661162',
  ),
  'account-my-account-account-credit' => 
  array (
    0 => 'Block_2033062197619cd4ab324623_49118668',
  ),
  'account-my-account-account-data' => 
  array (
    0 => 'Block_1880567967619cd4ab326521_80246185',
  ),
  'account-my-account-orders-content' => 
  array (
    0 => 'Block_1451915248619cd4ab327257_76861256',
  ),
  'account-my-account-orders-content-header' => 
  array (
    0 => 'Block_350459068619cd4ab327f40_71535434',
  ),
  'account-my-account-orders-body' => 
  array (
    0 => 'Block_34446510619cd4ab32c752_25590215',
  ),
  'account-my-account-orders-content-nodata' => 
  array (
    0 => 'Block_1028931837619cd4ab3370c1_43773392',
  ),
  'account-my-account-billing-address' => 
  array (
    0 => 'Block_606075668619cd4ab339028_50317787',
  ),
  'account-my-account-billing-address-header' => 
  array (
    0 => 'Block_1409065170619cd4ab3398f1_08614692',
  ),
  'account-my-account-billing-address-body' => 
  array (
    0 => 'Block_385642974619cd4ab33c8b6_51150318',
  ),
  'account-my-account-billing-address-billing-address' => 
  array (
    0 => 'Block_983233503619cd4ab33cc47_82869653',
  ),
  'account-my-account-billing-address-contact' => 
  array (
    0 => 'Block_2146225974619cd4ab3406f7_41443230',
  ),
  'account-my-account-billing-address-password' => 
  array (
    0 => 'Block_1541251267619cd4ab3431d6_29014283',
  ),
  'account-my-account-wishlist-content' => 
  array (
    0 => 'Block_899292277619cd4ab3471f9_09223927',
  ),
  'account-my-account-wishlist-header' => 
  array (
    0 => 'Block_1659536754619cd4ab347bd9_29798005',
  ),
  'account-my-account-wishlists' => 
  array (
    0 => 'Block_1028775798619cd4ab349737_31946807',
  ),
  'account-my-account-wishlist-name' => 
  array (
    0 => 'Block_651510925619cd4ab34a643_52097193',
  ),
  'account-my-account-wishlist-visibility' => 
  array (
    0 => 'Block_268301284619cd4ab34c884_47787785',
  ),
  'account-my-account-wishlist-no-data' => 
  array (
    0 => 'Block_186918993619cd4ab3524c1_81672316',
  ),
  'account-my-account-comparelist' => 
  array (
    0 => 'Block_2113709171619cd4ab354bb3_51301305',
  ),
  'account-my-account-comparelist-header' => 
  array (
    0 => 'Block_1441955007619cd4ab355785_96266911',
  ),
  'account-my-account-comparelist-body' => 
  array (
    0 => 'Block_259993552619cd4ab356ea5_97727832',
  ),
  'account-my-account-include-downloads' => 
  array (
    0 => 'Block_1672859258619cd4ab35b2d9_31297411',
  ),
  'account-my-account-actions' => 
  array (
    0 => 'Block_1736406406619cd4ab35d189_74018718',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1259158493619cd4ab318927_21946648', 'heading', $this->tplIndex);
?>

    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['opcMountPoint'][0], array( array('id'=>'opc_before_account_page'),$_smarty_tpl ) );?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_657691443619cd4ab31f933_55723497', 'account-my-account-head-data', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1880567967619cd4ab326521_80246185', 'account-my-account-account-data', $this->tplIndex);
?>

    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['opcMountPoint'][0], array( array('id'=>'opc_after_account_page'),$_smarty_tpl ) );?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1672859258619cd4ab35b2d9_31297411', 'account-my-account-include-downloads', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1736406406619cd4ab35d189_74018718', 'account-my-account-actions', $this->tplIndex);
?>

<?php
}
}
/* {/block 'account-my-account'} */
}
