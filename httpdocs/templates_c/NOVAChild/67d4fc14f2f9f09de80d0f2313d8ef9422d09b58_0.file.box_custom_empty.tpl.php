<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:23:31
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/boxes/box_custom_empty.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953a930f9268_45094542',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '67d4fc14f2f9f09de80d0f2313d8ef9422d09b58' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/boxes/box_custom_empty.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61953a930f9268_45094542 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_210474836261953a930e4215_33267228', 'boxes-box-custom-empty');
?>

<?php }
/* {block 'boxes-box-custom-empty-content'} */
class Block_1880450561953a930f33f3_31918690 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="box-content-wrapper">
                <?php $_template = new JTL\Smarty\JTLSmartyTemplateClass('eval:'.$_smarty_tpl->tpl_vars['oBox']->value->getContent(), $_smarty_tpl->smarty, $_smarty_tpl);echo $_template->fetch(); ?>
            </div>
        <?php
}
}
/* {/block 'boxes-box-custom-empty-content'} */
/* {block 'boxes-box-custom-empty'} */
class Block_210474836261953a930e4215_33267228 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'boxes-box-custom-empty' => 
  array (
    0 => 'Block_210474836261953a930e4215_33267228',
  ),
  'boxes-box-custom-empty-content' => 
  array (
    0 => 'Block_1880450561953a930f33f3_31918690',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="box box-custom box-normal" id="sidebox<?php echo $_smarty_tpl->tpl_vars['oBox']->value->getID();?>
">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1880450561953a930f33f3_31918690', 'boxes-box-custom-empty-content', $this->tplIndex);
?>

    </div>
<?php
}
}
/* {/block 'boxes-box-custom-empty'} */
}
