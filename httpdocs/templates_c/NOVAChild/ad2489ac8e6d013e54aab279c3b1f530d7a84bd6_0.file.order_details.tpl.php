<?php
/* Smarty version 3.1.39, created on 2021-11-18 21:43:18
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/order_details.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196bae6365437_96049612',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad2489ac8e6d013e54aab279c3b1f530d7a84bd6' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/order_details.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:checkout/inc_billing_address.tpl' => 1,
    'file:checkout/inc_delivery_address.tpl' => 1,
    'file:account/order_item.tpl' => 2,
    'file:account/downloads.tpl' => 1,
    'file:account/uploads.tpl' => 1,
  ),
),false)) {
function content_6196bae6365437_96049612 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13374281106196bae63103f6_01825993', 'account-order-details');
?>

<?php }
/* {block 'account-order-details-script-location'} */
class Block_16783685906196bae6310a11_94716167 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo '<script'; ?>
>
            if (top.location !== self.location) {
                top.location = self.location.href;
            }
        <?php echo '</script'; ?>
>
    <?php
}
}
/* {/block 'account-order-details-script-location'} */
/* {block 'account-order-details-heading'} */
class Block_955711486196bae63112c3_40742535 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <h1><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderCompletedPre','section'=>'checkout'),$_smarty_tpl ) );?>
</h1>
    <?php
}
}
/* {/block 'account-order-details-heading'} */
/* {block 'account-order-details-order-heading-date'} */
class Block_10672317326196bae63132e0_31533724 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>3,'class'=>'border-lg-right'));
$_block_repeat=true;
echo $_block_plugin10->render(array('cols'=>12,'lg'=>3,'class'=>'border-lg-right'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <div class="order-details-date">
                                <span class="far fa-calendar"></span><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->dErstelldatum_de;?>

                            </div>
                        <?php $_block_repeat=false;
echo $_block_plugin10->render(array('cols'=>12,'lg'=>3,'class'=>'border-lg-right'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'account-order-details-order-heading-date'} */
/* {block 'account-order-details-order-heading'} */
class Block_14283917236196bae6312c14_20736107 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>'align-items-center-util'));
$_block_repeat=true;
echo $_block_plugin9->render(array('class'=>'align-items-center-util'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10672317326196bae63132e0_31533724', 'account-order-details-order-heading-date', $this->tplIndex);
?>

                    <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>6,'lg'=>'auto'));
$_block_repeat=true;
echo $_block_plugin11->render(array('cols'=>6,'lg'=>'auto'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourOrderId','section'=>'checkout'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>

                    <?php $_block_repeat=false;
echo $_block_plugin11->render(array('cols'=>6,'lg'=>'auto'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>6,'lg'=>'auto','class'=>'order-details-status'));
$_block_repeat=true;
echo $_block_plugin12->render(array('cols'=>6,'lg'=>'auto','class'=>'order-details-status'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderStatus','section'=>'login'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Status;?>

                    <?php $_block_repeat=false;
echo $_block_plugin12->render(array('cols'=>6,'lg'=>'auto','class'=>'order-details-status'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin9->render(array('class'=>'align-items-center-util'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'account-order-details-order-heading'} */
/* {block 'account-order-details-payment'} */
class Block_2273409126196bae6318dc2_16815524 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'paymentOptions','section'=>'global'),$_smarty_tpl ) );?>
:
                                        <span class="order-details-data-item">
                                            <dl class="list-unstyled">
                                                <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->dBezahldatum_de === '00.00.0000' || empty($_smarty_tpl->tpl_vars['Bestellung']->value->dBezahldatum_de)) {?>
                                                    <dt><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cZahlungsartName;?>
</dt>
                                                    <dd>
                                                        <?php if (($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_OFFEN || $_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_IN_BEARBEITUNG) && (($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId !== 'za_ueberweisung_jtl' && $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId !== 'za_nachnahme_jtl' && $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId !== 'za_rechnung_jtl' && $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId !== 'za_barzahlung_jtl') && ((isset($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->bPayAgain)) && $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->bPayAgain))) {?>
                                                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellab_again.php'),$_smarty_tpl ) );
$_prefixVariable3=ob_get_clean();
$_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable3."?kBestellung=".((string)$_smarty_tpl->tpl_vars['Bestellung']->value->kBestellung)));
$_block_repeat=true;
echo $_block_plugin16->render(array('href'=>$_prefixVariable3."?kBestellung=".((string)$_smarty_tpl->tpl_vars['Bestellung']->value->kBestellung)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'payNow','section'=>'global'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin16->render(array('href'=>$_prefixVariable3."?kBestellung=".((string)$_smarty_tpl->tpl_vars['Bestellung']->value->kBestellung)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php } else { ?>
                                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'notPayedYet','section'=>'login'),$_smarty_tpl ) );?>

                                                        <?php }?>
                                                    </dd>
                                                <?php }?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['incommingPayments']->value, 'incommingPayment', false, 'paymentProvider');
$_smarty_tpl->tpl_vars['incommingPayment']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['paymentProvider']->value => $_smarty_tpl->tpl_vars['incommingPayment']->value) {
$_smarty_tpl->tpl_vars['incommingPayment']->do_else = false;
?>
                                                    <dt><?php echo htmlentities($_smarty_tpl->tpl_vars['paymentProvider']->value);?>
</dt>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['incommingPayment']->value, 'payment');
$_smarty_tpl->tpl_vars['payment']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['payment']->value) {
$_smarty_tpl->tpl_vars['payment']->do_else = false;
?>
                                                        <dd><?php echo $_smarty_tpl->tpl_vars['payment']->value->paymentLocalization;?>
</dd>
                                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </dl>
                                        </span>
                                    <?php
}
}
/* {/block 'account-order-details-payment'} */
/* {block 'account-order-details-shipping'} */
class Block_9771648246196bae63277d4_94473012 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingOptions','section'=>'global'),$_smarty_tpl ) );?>
:
                                        <span class="order-details-data-item">
                                            <ul class="list-unstyled">
                                                <li><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cVersandartName;?>
</li>
                                                <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_VERSANDT) {?>
                                                    <li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippedOn','section'=>'login'),$_smarty_tpl ) );?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->dVersanddatum_de;?>
</li>
                                                <?php } elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_TEILVERSANDT) {?>
                                                    <li><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Status;?>
</li>
                                                <?php } else { ?>
                                                    <li><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'notShippedYet','section'=>'login'),$_smarty_tpl ) );?>
</span></li>
                                                    <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus != BESTELLUNG_STATUS_STORNO) {?>
                                                        <li>
                                                            <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingTime','section'=>'global'),$_smarty_tpl ) );?>
: <?php if ((isset($_smarty_tpl->tpl_vars['cEstimatedDeliveryEx']->value))) {
echo $_smarty_tpl->tpl_vars['cEstimatedDeliveryEx']->value;
} else {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDelivery;
}?></span>
                                                        </li>
                                                    <?php }?>
                                                <?php }?>
                                            </ul>
                                        </span>
                                    <?php
}
}
/* {/block 'account-order-details-shipping'} */
/* {block 'account-order-details-include-inc-billing-address'} */
class Block_2708719386196bae632cf10_84811531 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_billing_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('orderDetail'=>true), 0, false);
?>
                                            <?php
}
}
/* {/block 'account-order-details-include-inc-billing-address'} */
/* {block 'account-order-details-billing-address'} */
class Block_7008455886196bae632c787_73832812 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'billingAdress','section'=>'checkout'),$_smarty_tpl ) );?>
:
                                        <span class="order-details-data-item">
                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2708719386196bae632cf10_84811531', 'account-order-details-include-inc-billing-address', $this->tplIndex);
?>

                                        </span>
                                    <?php
}
}
/* {/block 'account-order-details-billing-address'} */
/* {block 'account-order-details-include-inc-delivery-address'} */
class Block_15527952176196bae632ec26_85683086 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_delivery_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('orderDetail'=>true), 0, false);
?>
                                                <?php
}
}
/* {/block 'account-order-details-include-inc-delivery-address'} */
/* {block 'account-order-details-shipping-address'} */
class Block_10682039056196bae632dc22_41613797 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingAdress','section'=>'checkout'),$_smarty_tpl ) );?>
:
                                        <span class="order-details-data-item">
                                            <?php if (!empty($_smarty_tpl->tpl_vars['Lieferadresse']->value->kLieferadresse)) {?>
                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15527952176196bae632ec26_85683086', 'account-order-details-include-inc-delivery-address', $this->tplIndex);
?>

                                            <?php } else { ?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingAdressEqualBillingAdress','section'=>'account data'),$_smarty_tpl ) );?>

                                            <?php }?>
                                        </span>
                                    <?php
}
}
/* {/block 'account-order-details-shipping-address'} */
/* {block 'account-order-details-order-subheading-basket'} */
class Block_1014306946196bae63307c6_90671446 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <span class="subheadline"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'basket'),$_smarty_tpl ) );?>
</span>
                            <?php
}
}
/* {/block 'account-order-details-order-subheading-basket'} */
/* {block 'account-order-details-include-order-item'} */
class Block_8942315486196bae63311a6_85539639 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_smarty_tpl->_subTemplateRender('file:account/order_item.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tplscope'=>'confirmation'), 0, false);
?>
                            <?php
}
}
/* {/block 'account-order-details-include-order-item'} */
/* {block 'account-order-details-order-body'} */
class Block_15526943896196bae6318317_64640234 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin14->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>3,'class'=>'border-lg-right'));
$_block_repeat=true;
echo $_block_plugin15->render(array('cols'=>12,'lg'=>3,'class'=>'border-lg-right'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <ul class="list-unstyled order-details-data">
                                <li>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2273409126196bae6318dc2_16815524', 'account-order-details-payment', $this->tplIndex);
?>

                                </li>
                                <li>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9771648246196bae63277d4_94473012', 'account-order-details-shipping', $this->tplIndex);
?>

                                </li>
                                <li>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7008455886196bae632c787_73832812', 'account-order-details-billing-address', $this->tplIndex);
?>

                                </li>
                                <li>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10682039056196bae632dc22_41613797', 'account-order-details-shipping-address', $this->tplIndex);
?>

                                </li>
                            </ul>
                        <?php $_block_repeat=false;
echo $_block_plugin15->render(array('cols'=>12,'lg'=>3,'class'=>'border-lg-right'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>9));
$_block_repeat=true;
echo $_block_plugin17->render(array('cols'=>12,'lg'=>9), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1014306946196bae63307c6_90671446', 'account-order-details-order-subheading-basket', $this->tplIndex);
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8942315486196bae63311a6_85539639', 'account-order-details-include-order-item', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin17->render(array('cols'=>12,'lg'=>9), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin14->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'account-order-details-order-body'} */
/* {block 'account-order-details-order-subheading-basket'} */
class Block_3853480866196bae6338dd4_81257571 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <span class="subheadline"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'basket'),$_smarty_tpl ) );?>
</span>
                                <?php
}
}
/* {/block 'account-order-details-order-subheading-basket'} */
/* {block 'account-order-details-include-order-item'} */
class Block_2778533086196bae6339723_40751970 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:account/order_item.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tplscope'=>'confirmation'), 0, true);
?>
                                <?php
}
}
/* {/block 'account-order-details-include-order-item'} */
/* {block 'account-order-details-request-plz'} */
class Block_16700761076196bae6332b71_69991880 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin19->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>6));
$_block_repeat=true;
echo $_block_plugin20->render(array('cols'=>12,'md'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array(),$_smarty_tpl ) );
$_prefixVariable4=ob_get_clean();
$_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('method'=>"post",'id'=>'request-plz','action'=>$_prefixVariable4,'class'=>"jtl-validate",'slide'=>true));
$_block_repeat=true;
echo $_block_plugin21->render(array('method'=>"post",'id'=>'request-plz','action'=>$_prefixVariable4,'class'=>"jtl-validate",'slide'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"uid",'value'=>((string)$_smarty_tpl->tpl_vars['uid']->value)),$_smarty_tpl ) );?>

                                    <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'enter_plz_for_details','section'=>'account data'),$_smarty_tpl ) );?>
</p>
                                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'plz','section'=>'account data'),$_smarty_tpl ) );
$_prefixVariable5 = ob_get_clean();
$_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'formgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formgroup', array('label-for'=>"postcode",'label'=>$_prefixVariable5));
$_block_repeat=true;
echo $_block_plugin22->render(array('label-for'=>"postcode",'label'=>$_prefixVariable5), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"text",'name'=>"plz",'value'=>'','id'=>"postcode",'class'=>"postcode_input",'placeholder'=>" ",'required'=>true,'autocomplete'=>"billing postal-code"),$_smarty_tpl ) );?>

                                    <?php $_block_repeat=false;
echo $_block_plugin22->render(array('label-for'=>"postcode",'label'=>$_prefixVariable5), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin23 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin23, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin23->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin24 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin24, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>'ml-auto-util col-md-auto'));
$_block_repeat=true;
echo $_block_plugin24->render(array('class'=>'ml-auto-util col-md-auto'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php $_block_plugin25 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin25, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('type'=>'submit','value'=>'1','block'=>true,'variant'=>'primary','class'=>'mb-3'));
$_block_repeat=true;
echo $_block_plugin25->render(array('type'=>'submit','value'=>'1','block'=>true,'variant'=>'primary','class'=>'mb-3'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'view','section'=>'global'),$_smarty_tpl ) );?>

                                            <?php $_block_repeat=false;
echo $_block_plugin25->render(array('type'=>'submit','value'=>'1','block'=>true,'variant'=>'primary','class'=>'mb-3'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php $_block_repeat=false;
echo $_block_plugin24->render(array('class'=>'ml-auto-util col-md-auto'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin23->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin21->render(array('method'=>"post",'id'=>'request-plz','action'=>$_prefixVariable4,'class'=>"jtl-validate",'slide'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin20->render(array('cols'=>12,'md'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin26 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin26, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>9));
$_block_repeat=true;
echo $_block_plugin26->render(array('cols'=>12,'md'=>9), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3853480866196bae6338dd4_81257571', 'account-order-details-order-subheading-basket', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2778533086196bae6339723_40751970', 'account-order-details-include-order-item', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin26->render(array('cols'=>12,'md'=>9), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin19->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'account-order-details-request-plz'} */
/* {block 'account-order-details-order-details-data'} */
class Block_14910523166196bae6311eb0_06181533 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_block_plugin7 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin7, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'class'=>'order-details'));
$_block_repeat=true;
echo $_block_plugin7->render(array('no-body'=>true,'class'=>'order-details'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin8->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14283917236196bae6312c14_20736107', 'account-order-details-order-heading', $this->tplIndex);
?>

            <?php $_block_repeat=false;
echo $_block_plugin8->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php if ((isset($_smarty_tpl->tpl_vars['Kunde']->value)) && $_smarty_tpl->tpl_vars['Kunde']->value->kKunde > 0) {?>
                <?php $_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array());
$_block_repeat=true;
echo $_block_plugin13->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15526943896196bae6318317_64640234', 'account-order-details-order-body', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin13->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php } else { ?>
                <?php $_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array('class'=>"order-details-request-plz"));
$_block_repeat=true;
echo $_block_plugin18->render(array('class'=>"order-details-request-plz"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16700761076196bae6332b71_69991880', 'account-order-details-request-plz', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin18->render(array('class'=>"order-details-request-plz"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php }?>
        <?php $_block_repeat=false;
echo $_block_plugin7->render(array('no-body'=>true,'class'=>'order-details'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'account-order-details-order-details-data'} */
/* {block 'account-order-details-include-downloads'} */
class Block_17407147276196bae633c376_82775985 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:account/downloads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'account-order-details-include-downloads'} */
/* {block 'account-order-details-include-uploads'} */
class Block_6496247466196bae633cc87_32004065 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:account/uploads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'account-order-details-include-uploads'} */
/* {block 'account-order-details-delivery-note-header'} */
class Block_13350592396196bae633f938_84427857 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <tr>
                                    <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingOrder','section'=>'order'),$_smarty_tpl ) );?>
</th>
                                    <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippedOn','section'=>'login'),$_smarty_tpl ) );?>
</th>
                                    <th class="text-right-util"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'packageTracking','section'=>'order'),$_smarty_tpl ) );?>
</th>
                                </tr>
                            <?php
}
}
/* {/block 'account-order-details-delivery-note-header'} */
/* {block 'account-order-details-delivery-notes'} */
class Block_4242008606196bae6340f01_49518975 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->oLieferschein_arr, 'oLieferschein');
$_smarty_tpl->tpl_vars['oLieferschein']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oLieferschein']->value) {
$_smarty_tpl->tpl_vars['oLieferschein']->do_else = false;
?>
                                    <tr>
                                        <td><?php $_block_plugin27 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin27, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('data'=>array("toggle"=>"modal","target"=>"#shipping-order-".((string)$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein())),'id'=>$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein(),'href'=>"#",'title'=>$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferscheinNr()));
$_block_repeat=true;
echo $_block_plugin27->render(array('data'=>array("toggle"=>"modal","target"=>"#shipping-order-".((string)$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein())),'id'=>$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein(),'href'=>"#",'title'=>$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferscheinNr()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferscheinNr();
$_block_repeat=false;
echo $_block_plugin27->render(array('data'=>array("toggle"=>"modal","target"=>"#shipping-order-".((string)$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein())),'id'=>$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein(),'href'=>"#",'title'=>$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferscheinNr()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?></td>
                                        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['oLieferschein']->value->getErstellt(),"%d.%m.%Y %H:%M");?>
</td>
                                        <td class="text-right-util">
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oLieferschein']->value->oVersand_arr, 'oVersand');
$_smarty_tpl->tpl_vars['oVersand']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oVersand']->value) {
$_smarty_tpl->tpl_vars['oVersand']->do_else = false;
?>
                                                <?php if ($_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()) {?>
                                                    <p><?php $_block_plugin28 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin28, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oVersand']->value->getLogistikVarUrl(),'target'=>"_blank",'class'=>"shipment",'title'=>$_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()));
$_block_repeat=true;
echo $_block_plugin28->render(array('href'=>$_smarty_tpl->tpl_vars['oVersand']->value->getLogistikVarUrl(),'target'=>"_blank",'class'=>"shipment",'title'=>$_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'packageTracking','section'=>'order'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin28->render(array('href'=>$_smarty_tpl->tpl_vars['oVersand']->value->getLogistikVarUrl(),'target'=>"_blank",'class'=>"shipment",'title'=>$_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?></p>
                                                <?php }?>
                                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            <?php
}
}
/* {/block 'account-order-details-delivery-notes'} */
/* {block 'account-order-details-delivery-note-popup-heading'} */
class Block_15910986496196bae634c136_91457870 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

                                <div class="shipping-order-modal-mb">
                                    <strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingOrder','section'=>'order'),$_smarty_tpl ) );?>
</strong>: <?php echo $_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferscheinNr();?>
<br />
                                    <strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippedOn','section'=>'login'),$_smarty_tpl ) );?>
</strong>: <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['oLieferschein']->value->getErstellt(),"%d.%m.%Y %H:%M");?>
<br />
                                </div>
                            <?php
}
}
/* {/block 'account-order-details-delivery-note-popup-heading'} */
/* {block 'account-order-details-delivery-note-popup-alert'} */
class Block_10324535166196bae634e842_55029815 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_block_plugin30 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin30, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"info",'class'=>"shipping-order-modal-mb"));
$_block_repeat=true;
echo $_block_plugin30->render(array('variant'=>"info",'class'=>"shipping-order-modal-mb"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['oLieferschein']->value->getHinweis();
$_block_repeat=false;
echo $_block_plugin30->render(array('variant'=>"info",'class'=>"shipping-order-modal-mb"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php
}
}
/* {/block 'account-order-details-delivery-note-popup-alert'} */
/* {block 'account-order-details-delivery-note-popup-tracking'} */
class Block_14747786266196bae634fae7_49572048 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <div class="shipping-order-modal-mb">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oLieferschein']->value->oVersand_arr, 'oVersand');
$_smarty_tpl->tpl_vars['oVersand']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oVersand']->value) {
$_smarty_tpl->tpl_vars['oVersand']->do_else = false;
?>
                                        <?php if ($_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()) {?>
                                            <p>
                                                <?php $_block_plugin31 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin31, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oVersand']->value->getLogistikVarUrl(),'target'=>"_blank",'class'=>"shipment",'title'=>$_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()));
$_block_repeat=true;
echo $_block_plugin31->render(array('href'=>$_smarty_tpl->tpl_vars['oVersand']->value->getLogistikVarUrl(),'target'=>"_blank",'class'=>"shipment",'title'=>$_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'packageTracking','section'=>'order'),$_smarty_tpl ) );?>

                                                <?php $_block_repeat=false;
echo $_block_plugin31->render(array('href'=>$_smarty_tpl->tpl_vars['oVersand']->value->getLogistikVarUrl(),'target'=>"_blank",'class'=>"shipment",'title'=>$_smarty_tpl->tpl_vars['oVersand']->value->getIdentCode()), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            </p>
                                        <?php }?>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </div>
                            <?php
}
}
/* {/block 'account-order-details-delivery-note-popup-tracking'} */
/* {block 'account-order-details-delivery-note-popup-table'} */
class Block_6915713466196bae6352b46_19029646 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>"partialShippedPosition",'section'=>"order"),$_smarty_tpl ) );?>
</th>
                                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>"partialShippedCount",'section'=>"order"),$_smarty_tpl ) );?>
</th>
                                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productNo','section'=>'global'),$_smarty_tpl ) );?>
</th>
                                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'product','section'=>'global'),$_smarty_tpl ) );?>
</th>
                                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>"order",'section'=>"global"),$_smarty_tpl ) );?>
</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oLieferschein']->value->oLieferscheinPos_arr, 'oLieferscheinpos');
$_smarty_tpl->tpl_vars['oLieferscheinpos']->iteration = 0;
$_smarty_tpl->tpl_vars['oLieferscheinpos']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oLieferscheinpos']->value) {
$_smarty_tpl->tpl_vars['oLieferscheinpos']->do_else = false;
$_smarty_tpl->tpl_vars['oLieferscheinpos']->iteration++;
$__foreach_oLieferscheinpos_6_saved = $_smarty_tpl->tpl_vars['oLieferscheinpos'];
?>
                                        <tr>
                                            <td><?php echo $_smarty_tpl->tpl_vars['oLieferscheinpos']->iteration;?>
</td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['oLieferscheinpos']->value->getAnzahl();?>
</td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->cArtNr;?>
</td>
                                            <td>
                                                <?php echo $_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->cName;?>

                                                <ul class="list-unstyled text-muted-util small">
                                                    <?php if (!empty($_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->cHinweis)) {?>
                                                        <li class="text-info notice"><?php echo $_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->cHinweis;?>
</li>
                                                    <?php }?>

                                                                                                        <?php if ($_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->Artikel->cHersteller && $_smarty_tpl->tpl_vars['Einstellungen']->value['artikeldetails']['artikeldetails_hersteller_anzeigen'] != "N") {?>
                                                        <li class="manufacturer">
                                                            <strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'manufacturer','section'=>'productDetails'),$_smarty_tpl ) );?>
</strong>:
                                                            <span class="values">
                                                               <?php echo $_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->Artikel->cHersteller;?>

                                                            </span>
                                                        </li>
                                                    <?php }?>

                                                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_artikelmerkmale'] == 'Y' && !empty($_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->Artikel->oMerkmale_arr)) {?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oLieferscheinpos']->value->oPosition->Artikel->oMerkmale_arr, 'oMerkmale_arr');
$_smarty_tpl->tpl_vars['oMerkmale_arr']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oMerkmale_arr']->value) {
$_smarty_tpl->tpl_vars['oMerkmale_arr']->do_else = false;
?>
                                                            <li class="characteristic">
                                                                <strong><?php echo $_smarty_tpl->tpl_vars['oMerkmale_arr']->value->cName;?>
</strong>:
                                                                <span class="values">
                                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oMerkmale_arr']->value->oMerkmalWert_arr, 'oWert');
$_smarty_tpl->tpl_vars['oWert']->index = -1;
$_smarty_tpl->tpl_vars['oWert']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oWert']->value) {
$_smarty_tpl->tpl_vars['oWert']->do_else = false;
$_smarty_tpl->tpl_vars['oWert']->index++;
$_smarty_tpl->tpl_vars['oWert']->first = !$_smarty_tpl->tpl_vars['oWert']->index;
$__foreach_oWert_8_saved = $_smarty_tpl->tpl_vars['oWert'];
?>
                                                                        <?php if (!$_smarty_tpl->tpl_vars['oWert']->first) {?>, <?php }?>
                                                                        <?php echo $_smarty_tpl->tpl_vars['oWert']->value->cWert;?>

                                                                    <?php
$_smarty_tpl->tpl_vars['oWert'] = $__foreach_oWert_8_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                                </span>
                                                            </li>
                                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                    <?php }?>
                                                </ul>
                                            </td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>
</td>
                                        </tr>
                                    <?php
$_smarty_tpl->tpl_vars['oLieferscheinpos'] = $__foreach_oLieferscheinpos_6_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            <?php
}
}
/* {/block 'account-order-details-delivery-note-popup-table'} */
/* {block 'account-order-details-delivery-note-popup'} */
class Block_6601484136196bae6349db4_86974104 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'partialShipped','section'=>'order'),$_smarty_tpl ) );
$_prefixVariable6 = ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shipped','section'=>'order'),$_smarty_tpl ) );
$_prefixVariable7 = ob_get_clean();
$_block_plugin29 = isset($_smarty_tpl->smarty->registered_plugins['block']['modal'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['modal'][0][0] : null;
if (!is_callable(array($_block_plugin29, 'render'))) {
throw new SmartyException('block tag \'modal\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('modal', array('id'=>"shipping-order-".((string)$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein()),'title'=>($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_TEILVERSANDT ? $_prefixVariable6 : $_prefixVariable7),'class'=>"fade shipping-order-modal",'size'=>"lg"));
$_block_repeat=true;
echo $_block_plugin29->render(array('id'=>"shipping-order-".((string)$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein()),'title'=>($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_TEILVERSANDT ? $_prefixVariable6 : $_prefixVariable7),'class'=>"fade shipping-order-modal",'size'=>"lg"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15910986496196bae634c136_91457870', 'account-order-details-delivery-note-popup-heading', $this->tplIndex);
?>

                            <?php if (call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'count_characters' ][ 0 ], array( $_smarty_tpl->tpl_vars['oLieferschein']->value->getHinweis() )) > 0) {?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10324535166196bae634e842_55029815', 'account-order-details-delivery-note-popup-alert', $this->tplIndex);
?>

                            <?php }?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14747786266196bae634fae7_49572048', 'account-order-details-delivery-note-popup-tracking', $this->tplIndex);
?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6915713466196bae6352b46_19029646', 'account-order-details-delivery-note-popup-table', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin29->render(array('id'=>"shipping-order-".((string)$_smarty_tpl->tpl_vars['oLieferschein']->value->getLieferschein()),'title'=>($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_TEILVERSANDT ? $_prefixVariable6 : $_prefixVariable7),'class'=>"fade shipping-order-modal",'size'=>"lg"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'account-order-details-delivery-note-popup'} */
/* {block 'account-order-details-delivery-note-content'} */
class Block_14723797346196bae633e747_48523801 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div class="h2"><?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == BESTELLUNG_STATUS_TEILVERSANDT) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'partialShipped','section'=>'order'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shipped','section'=>'order'),$_smarty_tpl ) );
}?></div>
                <div class="table-responsive mb-3">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13350592396196bae633f938_84427857', 'account-order-details-delivery-note-header', $this->tplIndex);
?>

                        </thead>
                        <tbody>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4242008606196bae6340f01_49518975', 'account-order-details-delivery-notes', $this->tplIndex);
?>

                        </tbody>
                    </table>
                </div>

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->oLieferschein_arr, 'oLieferschein');
$_smarty_tpl->tpl_vars['oLieferschein']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oLieferschein']->value) {
$_smarty_tpl->tpl_vars['oLieferschein']->do_else = false;
?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6601484136196bae6349db4_86974104', 'account-order-details-delivery-note-popup', $this->tplIndex);
?>

                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php
}
}
/* {/block 'account-order-details-delivery-note-content'} */
/* {block 'account-order-details-order-comment'} */
class Block_5038978716196bae6360002_67744119 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php if (!empty(trim($_smarty_tpl->tpl_vars['Bestellung']->value->cKommentar))) {?>
                <div class="h3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourOrderComment','section'=>'login'),$_smarty_tpl ) );?>
</div>
                <p><?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cKommentar;?>
</p>
            <?php }?>
        <?php
}
}
/* {/block 'account-order-details-order-comment'} */
/* {block 'account-order-details-actions'} */
class Block_7634699136196bae6362872_27942018 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin32 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin32, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"btn-row"));
$_block_repeat=true;
echo $_block_plugin32->render(array('class'=>"btn-row"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php $_block_plugin33 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin33, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>3,'cols'=>12));
$_block_repeat=true;
echo $_block_plugin33->render(array('md'=>3,'cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php'),$_smarty_tpl ) );
$_prefixVariable8=ob_get_clean();
$_block_plugin34 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin34, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"btn btn-outline-primary btn-block",'href'=>$_prefixVariable8."?bestellungen=1"));
$_block_repeat=true;
echo $_block_plugin34->render(array('class'=>"btn btn-outline-primary btn-block",'href'=>$_prefixVariable8."?bestellungen=1"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'back'),$_smarty_tpl ) );?>

                    <?php $_block_repeat=false;
echo $_block_plugin34->render(array('class'=>"btn btn-outline-primary btn-block",'href'=>$_prefixVariable8."?bestellungen=1"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin33->render(array('md'=>3,'cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_repeat=false;
echo $_block_plugin32->render(array('class'=>"btn-row"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'account-order-details-actions'} */
/* {block 'account-order-details'} */
class Block_13374281106196bae63103f6_01825993 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'account-order-details' => 
  array (
    0 => 'Block_13374281106196bae63103f6_01825993',
  ),
  'account-order-details-script-location' => 
  array (
    0 => 'Block_16783685906196bae6310a11_94716167',
  ),
  'account-order-details-heading' => 
  array (
    0 => 'Block_955711486196bae63112c3_40742535',
  ),
  'account-order-details-order-details-data' => 
  array (
    0 => 'Block_14910523166196bae6311eb0_06181533',
  ),
  'account-order-details-order-heading' => 
  array (
    0 => 'Block_14283917236196bae6312c14_20736107',
  ),
  'account-order-details-order-heading-date' => 
  array (
    0 => 'Block_10672317326196bae63132e0_31533724',
  ),
  'account-order-details-order-body' => 
  array (
    0 => 'Block_15526943896196bae6318317_64640234',
  ),
  'account-order-details-payment' => 
  array (
    0 => 'Block_2273409126196bae6318dc2_16815524',
  ),
  'account-order-details-shipping' => 
  array (
    0 => 'Block_9771648246196bae63277d4_94473012',
  ),
  'account-order-details-billing-address' => 
  array (
    0 => 'Block_7008455886196bae632c787_73832812',
  ),
  'account-order-details-include-inc-billing-address' => 
  array (
    0 => 'Block_2708719386196bae632cf10_84811531',
  ),
  'account-order-details-shipping-address' => 
  array (
    0 => 'Block_10682039056196bae632dc22_41613797',
  ),
  'account-order-details-include-inc-delivery-address' => 
  array (
    0 => 'Block_15527952176196bae632ec26_85683086',
  ),
  'account-order-details-order-subheading-basket' => 
  array (
    0 => 'Block_1014306946196bae63307c6_90671446',
    1 => 'Block_3853480866196bae6338dd4_81257571',
  ),
  'account-order-details-include-order-item' => 
  array (
    0 => 'Block_8942315486196bae63311a6_85539639',
    1 => 'Block_2778533086196bae6339723_40751970',
  ),
  'account-order-details-request-plz' => 
  array (
    0 => 'Block_16700761076196bae6332b71_69991880',
  ),
  'account-order-details-include-downloads' => 
  array (
    0 => 'Block_17407147276196bae633c376_82775985',
  ),
  'account-order-details-include-uploads' => 
  array (
    0 => 'Block_6496247466196bae633cc87_32004065',
  ),
  'account-order-details-delivery-note-content' => 
  array (
    0 => 'Block_14723797346196bae633e747_48523801',
  ),
  'account-order-details-delivery-note-header' => 
  array (
    0 => 'Block_13350592396196bae633f938_84427857',
  ),
  'account-order-details-delivery-notes' => 
  array (
    0 => 'Block_4242008606196bae6340f01_49518975',
  ),
  'account-order-details-delivery-note-popup' => 
  array (
    0 => 'Block_6601484136196bae6349db4_86974104',
  ),
  'account-order-details-delivery-note-popup-heading' => 
  array (
    0 => 'Block_15910986496196bae634c136_91457870',
  ),
  'account-order-details-delivery-note-popup-alert' => 
  array (
    0 => 'Block_10324535166196bae634e842_55029815',
  ),
  'account-order-details-delivery-note-popup-tracking' => 
  array (
    0 => 'Block_14747786266196bae634fae7_49572048',
  ),
  'account-order-details-delivery-note-popup-table' => 
  array (
    0 => 'Block_6915713466196bae6352b46_19029646',
  ),
  'account-order-details-order-comment' => 
  array (
    0 => 'Block_5038978716196bae6360002_67744119',
  ),
  'account-order-details-actions' => 
  array (
    0 => 'Block_7634699136196bae6362872_27942018',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16783685906196bae6310a11_94716167', 'account-order-details-script-location', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_955711486196bae63112c3_40742535', 'account-order-details-heading', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14910523166196bae6311eb0_06181533', 'account-order-details-order-details-data', $this->tplIndex);
?>


    <?php if ((isset($_smarty_tpl->tpl_vars['Kunde']->value)) && $_smarty_tpl->tpl_vars['Kunde']->value->kKunde > 0) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17407147276196bae633c376_82775985', 'account-order-details-include-downloads', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6496247466196bae633cc87_32004065', 'account-order-details-include-uploads', $this->tplIndex);
?>


        <?php if (count($_smarty_tpl->tpl_vars['Bestellung']->value->oLieferschein_arr) > 0) {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14723797346196bae633e747_48523801', 'account-order-details-delivery-note-content', $this->tplIndex);
?>

        <?php }?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5038978716196bae6360002_67744119', 'account-order-details-order-comment', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7634699136196bae6362872_27942018', 'account-order-details-actions', $this->tplIndex);
?>

    <?php }
}
}
/* {/block 'account-order-details'} */
}
