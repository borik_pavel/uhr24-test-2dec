<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:33
  from '/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/jtlshop/scc/src/scc/templates/checkbox.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f50fb927_91172665',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd458111182357b9562ba6116eb023906c791bb9c' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/jtlshop/scc/src/scc/templates/checkbox.tpl',
      1 => 1632904512,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619660f50fb927_91172665 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('id', (($tmp = $_smarty_tpl->tpl_vars['params']->value['id']->getValue() ?? null)===null||$tmp==='' ? uniqid() : $tmp));
$_smarty_tpl->_assignInScope('buttons', false);
$_smarty_tpl->_assignInScope('stacked', false);
$_smarty_tpl->_assignInScope('plain', false);
$_smarty_tpl->_assignInScope('parentParams', (($tmp = $_smarty_tpl->tpl_vars['parentSmarty']->value->getTemplateVars('pbp') ?? null)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['parentBlockParams']->value : $tmp));?>

<?php if ((isset($_smarty_tpl->tpl_vars['parentParams']->value['stacked']))) {?>
    <?php $_smarty_tpl->_assignInScope('stacked', $_smarty_tpl->tpl_vars['parentParams']->value['stacked']->getValue());
}
if ((isset($_smarty_tpl->tpl_vars['parentParams']->value['plain']))) {?>
    <?php $_smarty_tpl->_assignInScope('plain', $_smarty_tpl->tpl_vars['parentParams']->value['plain']->getValue());
}
if ((isset($_smarty_tpl->tpl_vars['parentParams']->value['buttons']))) {?>
    <?php $_smarty_tpl->_assignInScope('buttons', $_smarty_tpl->tpl_vars['parentParams']->value['buttons']->getValue());
}
if ($_smarty_tpl->tpl_vars['buttons']->value === false) {?>
    <div class="<?php if ($_smarty_tpl->tpl_vars['plain']->value === false) {?>custom-control custom-checkbox<?php if ($_smarty_tpl->tpl_vars['stacked']->value === false) {?> custom-control-inline<?php }
} else { ?>form-check<?php if ($_smarty_tpl->tpl_vars['stacked']->value === false) {?> form-check-inline<?php }
}?>"
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemprop']->hasValue()) {?>itemprop="<?php echo $_smarty_tpl->tpl_vars['params']->value['itemprop']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemscope']->getValue() === true) {?>itemscope <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemtype']->hasValue()) {?>itemtype="<?php echo $_smarty_tpl->tpl_vars['params']->value['itemtype']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemid']->hasValue()) {?>itemid="<?php echo $_smarty_tpl->tpl_vars['params']->value['itemid']->getValue();?>
"<?php }?>
    >
        <input class="<?php if ($_smarty_tpl->tpl_vars['plain']->value === false) {?>custom-control-input<?php } else { ?>form-check-input<?php }?> <?php echo $_smarty_tpl->tpl_vars['params']->value['class']->getValue();?>
"
            type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"
            <?php if ($_smarty_tpl->tpl_vars['params']->value['title']->hasValue()) {?> title="<?php echo $_smarty_tpl->tpl_vars['params']->value['title']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['value']->hasValue()) {?>value="<?php echo $_smarty_tpl->tpl_vars['params']->value['value']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['name']->hasValue()) {?>name="<?php echo $_smarty_tpl->tpl_vars['params']->value['name']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['disabled']->getValue() === true) {?>disabled<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['required']->getValue() === true) {?>required<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['checked']->getValue() === true) {?>checked<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['role']->hasValue()) {?>role="<?php echo $_smarty_tpl->tpl_vars['params']->value['role']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['aria']->hasValue()) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['params']->value['aria']->getValue(), 'ariaVal', false, 'ariaKey');
$_smarty_tpl->tpl_vars['ariaVal']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ariaKey']->value => $_smarty_tpl->tpl_vars['ariaVal']->value) {
$_smarty_tpl->tpl_vars['ariaVal']->do_else = false;
?>aria-<?php echo $_smarty_tpl->tpl_vars['ariaKey']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['ariaVal']->value;?>
" <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['data']->hasValue()) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['params']->value['data']->getValue(), 'dataVal', false, 'dataKey');
$_smarty_tpl->tpl_vars['dataVal']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['dataKey']->value => $_smarty_tpl->tpl_vars['dataVal']->value) {
$_smarty_tpl->tpl_vars['dataVal']->do_else = false;
?>data-<?php echo $_smarty_tpl->tpl_vars['dataKey']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['dataVal']->value;?>
" <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['attribs']->hasValue()) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['params']->value['attribs']->getValue(), 'val', false, 'key');
$_smarty_tpl->tpl_vars['val']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->do_else = false;
?> <?php echo $_smarty_tpl->tpl_vars['key']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php }?>
        >
        <?php if (!empty($_smarty_tpl->tpl_vars['blockContent']->value)) {?>
            <label for="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['plain']->value === false) {?>custom-control-label<?php } else { ?>form-check-label<?php }?>">
                <?php echo $_smarty_tpl->tpl_vars['blockContent']->value;?>

            </label>
        <?php }?>
    </div>
<?php } else { ?>
    <label class="btn btn-<?php echo $_smarty_tpl->tpl_vars['parentParams']->value['button-variant']->getValue();?>
 btn-<?php echo $_smarty_tpl->tpl_vars['parentParams']->value['size']->getValue();
if ($_smarty_tpl->tpl_vars['params']->value['disabled']->getValue() === true) {?> disabled<?php }?>"
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemprop']->hasValue()) {?>itemprop="<?php echo $_smarty_tpl->tpl_vars['params']->value['itemprop']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemscope']->getValue() === true) {?>itemscope <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemtype']->hasValue()) {?>itemtype="<?php echo $_smarty_tpl->tpl_vars['params']->value['itemtype']->getValue();?>
"<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['params']->value['itemid']->hasValue()) {?>itemid="<?php echo $_smarty_tpl->tpl_vars['params']->value['itemid']->getValue();?>
"<?php }?>
    >
        <input id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" type="checkbox"
            <?php if ($_smarty_tpl->tpl_vars['params']->value['value']->hasValue()) {?>value="<?php echo $_smarty_tpl->tpl_vars['params']->value['value']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['name']->hasValue()) {?>name="<?php echo $_smarty_tpl->tpl_vars['params']->value['name']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['title']->hasValue()) {?> title="<?php echo $_smarty_tpl->tpl_vars['params']->value['title']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['disabled']->getValue() === true) {?>disabled<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['required']->getValue() === true) {?>required<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['checked']->getValue() === true) {?>checked<?php }?>
            class="<?php echo $_smarty_tpl->tpl_vars['params']->value['class']->getValue();?>
"
            <?php if ($_smarty_tpl->tpl_vars['params']->value['role']->hasValue()) {?>role="<?php echo $_smarty_tpl->tpl_vars['params']->value['role']->getValue();?>
"<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['aria']->hasValue()) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['params']->value['aria']->getValue(), 'ariaVal', false, 'ariaKey');
$_smarty_tpl->tpl_vars['ariaVal']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['ariaKey']->value => $_smarty_tpl->tpl_vars['ariaVal']->value) {
$_smarty_tpl->tpl_vars['ariaVal']->do_else = false;
?>aria-<?php echo $_smarty_tpl->tpl_vars['ariaKey']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['ariaVal']->value;?>
" <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>
            <?php if ($_smarty_tpl->tpl_vars['params']->value['data']->hasValue()) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['params']->value['data']->getValue(), 'dataVal', false, 'dataKey');
$_smarty_tpl->tpl_vars['dataVal']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['dataKey']->value => $_smarty_tpl->tpl_vars['dataVal']->value) {
$_smarty_tpl->tpl_vars['dataVal']->do_else = false;
?>data-<?php echo $_smarty_tpl->tpl_vars['dataKey']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['dataVal']->value;?>
" <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}?>
        >
        <span><span><?php echo $_smarty_tpl->tpl_vars['blockContent']->value;?>
</span></span>
    </label>
<?php }
}
}
