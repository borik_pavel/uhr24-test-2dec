<?php
/* Smarty version 3.1.39, created on 2021-11-18 11:08:13
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/boxes/box_last_seen.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196260dac18d5_97798233',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '40895875954fb6dd9bcae179f9c7e809f4f7b8b3' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/boxes/box_last_seen.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/image.tpl' => 1,
    'file:productdetails/price.tpl' => 1,
  ),
),false)) {
function content_6196260dac18d5_97798233 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9895103966196260dab2279_53286623', 'boxes-box-last-seen');
?>

<?php }
/* {block 'boxes-box-last-seen-title'} */
class Block_13572386826196260dab5be9_43229741 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div class="productlist-filter-headline">
                    <?php echo $_smarty_tpl->tpl_vars['boxtitle']->value;?>

                </div>
            <?php
}
}
/* {/block 'boxes-box-last-seen-title'} */
/* {block 'boxes-box-last-seen-image-link'} */
class Block_481446336196260dab8da4_88501159 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div class="productbox productbox-row productbox-sidebar">
                            <div class="productbox-inner">
                                <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['formrow'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formrow'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'formrow\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formrow', array());
$_block_repeat=true;
echo $_block_plugin1->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4,'lg'=>6,'xl'=>3));
$_block_repeat=true;
echo $_block_plugin2->render(array('md'=>4,'lg'=>6,'xl'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"image-wrapper",'href'=>$_smarty_tpl->tpl_vars['product']->value->cURLFull));
$_block_repeat=true;
echo $_block_plugin3->render(array('class'=>"image-wrapper",'href'=>$_smarty_tpl->tpl_vars['product']->value->cURLFull), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php $_smarty_tpl->_subTemplateRender('file:snippets/image.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('item'=>$_smarty_tpl->tpl_vars['product']->value,'srcSize'=>'sm'), 0, true);
?>
                                        <?php $_block_repeat=false;
echo $_block_plugin3->render(array('class'=>"image-wrapper",'href'=>$_smarty_tpl->tpl_vars['product']->value->cURLFull), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_repeat=false;
echo $_block_plugin2->render(array('md'=>4,'lg'=>6,'xl'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin4 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin4, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('class'=>"col-md"));
$_block_repeat=true;
echo $_block_plugin4->render(array('class'=>"col-md"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php $_block_plugin5 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin5, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"productbox-title",'href'=>$_smarty_tpl->tpl_vars['product']->value->cURLFull));
$_block_repeat=true;
echo $_block_plugin5->render(array('class'=>"productbox-title",'href'=>$_smarty_tpl->tpl_vars['product']->value->cURLFull), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php echo $_smarty_tpl->tpl_vars['product']->value->cKurzbezeichnung;?>

                                        <?php $_block_repeat=false;
echo $_block_plugin5->render(array('class'=>"productbox-title",'href'=>$_smarty_tpl->tpl_vars['product']->value->cURLFull), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php $_smarty_tpl->_subTemplateRender('file:productdetails/price.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('Artikel'=>$_smarty_tpl->tpl_vars['product']->value,'tplscope'=>'box'), 0, true);
?>
                                    <?php $_block_repeat=false;
echo $_block_plugin4->render(array('class'=>"col-md"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin1->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            </div>
                        </div>
                    <?php
}
}
/* {/block 'boxes-box-last-seen-image-link'} */
/* {block 'boxes-box-last-seen-content'} */
class Block_8008356716196260dab6634_21371779 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="box-content-wrapper">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oBox']->value->getProducts(), 'product');
$_smarty_tpl->tpl_vars['product']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->do_else = false;
?>
                <div class="box-last-seen-item">
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_481446336196260dab8da4_88501159', 'boxes-box-last-seen-image-link', $this->tplIndex);
?>

                </div>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
            <?php
}
}
/* {/block 'boxes-box-last-seen-content'} */
/* {block 'boxes-box-last-seen-hr-end'} */
class Block_15457742996196260dac0df6_48892950 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <hr class="box-normal-hr">
            <?php
}
}
/* {/block 'boxes-box-last-seen-hr-end'} */
/* {block 'boxes-box-last-seen-content'} */
class Block_9192461136196260dab5803_36736003 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13572386826196260dab5be9_43229741', 'boxes-box-last-seen-title', $this->tplIndex);
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8008356716196260dab6634_21371779', 'boxes-box-last-seen-content', $this->tplIndex);
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15457742996196260dac0df6_48892950', 'boxes-box-last-seen-hr-end', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'boxes-box-last-seen-content'} */
/* {block 'boxes-box-last-seen'} */
class Block_9895103966196260dab2279_53286623 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'boxes-box-last-seen' => 
  array (
    0 => 'Block_9895103966196260dab2279_53286623',
  ),
  'boxes-box-last-seen-content' => 
  array (
    0 => 'Block_9192461136196260dab5803_36736003',
    1 => 'Block_8008356716196260dab6634_21371779',
  ),
  'boxes-box-last-seen-title' => 
  array (
    0 => 'Block_13572386826196260dab5be9_43229741',
  ),
  'boxes-box-last-seen-image-link' => 
  array (
    0 => 'Block_481446336196260dab8da4_88501159',
  ),
  'boxes-box-last-seen-hr-end' => 
  array (
    0 => 'Block_15457742996196260dac0df6_48892950',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'lastViewed','assign'=>'boxtitle'),$_smarty_tpl ) );?>

    <div class="box box-last-seen box-normal" id="sidebox<?php echo $_smarty_tpl->tpl_vars['oBox']->value->getID();?>
">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9192461136196260dab5803_36736003', 'boxes-box-last-seen-content', $this->tplIndex);
?>

    </div>
<?php
}
}
/* {/block 'boxes-box-last-seen'} */
}
