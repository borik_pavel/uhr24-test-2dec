<?php
/* Smarty version 3.1.39, created on 2021-11-18 11:08:13
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/productdetails/pushed_success.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196260ddc3b96_68955033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a6221d6f9b94cc913493e2030a43fff41a5bbfcc' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/productdetails/pushed_success.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/image.tpl' => 1,
    'file:productdetails/rating.tpl' => 1,
    'file:snippets/product_slider.tpl' => 1,
  ),
),false)) {
function content_6196260ddc3b96_68955033 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4670999176196260dda0440_61788242', 'productdetails-pushed-success');
?>

<?php }
/* {block 'productdetails-pushed-success-cart-note-heading'} */
class Block_2272699516196260dda4a47_78062486 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div class="card-header alert-success">
                    <?php if ((isset($_smarty_tpl->tpl_vars['cartNote']->value))) {?>
                        <?php echo $_smarty_tpl->tpl_vars['cartNote']->value;?>

                    <?php }?>
                </div>
            <?php
}
}
/* {/block 'productdetails-pushed-success-cart-note-heading'} */
/* {block 'productdetails-pushed-success-product-cell-subheading'} */
class Block_13200133656196260dda82a3_02082205 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <div class="productbox-title subheadline"><?php echo $_smarty_tpl->tpl_vars['pushedArtikel']->value->cName;?>
</div>
                                    <?php
}
}
/* {/block 'productdetails-pushed-success-product-cell-subheading'} */
/* {block 'productdetails-pushed-success-product-cell-image'} */
class Block_14676256106196260dda9682_52347823 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php $_smarty_tpl->_subTemplateRender('file:snippets/image.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('item'=>$_smarty_tpl->tpl_vars['pushedArtikel']->value,'square'=>false,'class'=>'image','srcSize'=>'sm'), 0, false);
?>
                                    <?php
}
}
/* {/block 'productdetails-pushed-success-product-cell-image'} */
/* {block 'productdetails-pushed-success-characteristics'} */
class Block_5899243666196260ddad7c4_15130727 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pushedArtikel']->value->oMerkmale_arr, 'oMerkmal', true);
$_smarty_tpl->tpl_vars['oMerkmal']->iteration = 0;
$_smarty_tpl->tpl_vars['oMerkmal']->index = -1;
$_smarty_tpl->tpl_vars['oMerkmal']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oMerkmal']->value) {
$_smarty_tpl->tpl_vars['oMerkmal']->do_else = false;
$_smarty_tpl->tpl_vars['oMerkmal']->iteration++;
$_smarty_tpl->tpl_vars['oMerkmal']->index++;
$_smarty_tpl->tpl_vars['oMerkmal']->last = $_smarty_tpl->tpl_vars['oMerkmal']->iteration === $_smarty_tpl->tpl_vars['oMerkmal']->total;
$__foreach_oMerkmal_4_saved = $_smarty_tpl->tpl_vars['oMerkmal'];
?>
                                                                    <?php echo $_smarty_tpl->tpl_vars['oMerkmal']->value->cName;?>

                                                                    <?php if ($_smarty_tpl->tpl_vars['oMerkmal']->index === 10 && !$_smarty_tpl->tpl_vars['oMerkmal']->last) {?>&hellip;<?php break 1;
}?>
                                                                    <?php if (!$_smarty_tpl->tpl_vars['oMerkmal']->last) {?>, <?php }?>
                                                                <?php
$_smarty_tpl->tpl_vars['oMerkmal'] = $__foreach_oMerkmal_4_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                            <?php
}
}
/* {/block 'productdetails-pushed-success-characteristics'} */
/* {block 'productdetails-pushed-success-include-rating'} */
class Block_18951703496196260ddb7aa4_90814879 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                <?php $_smarty_tpl->_subTemplateRender('file:productdetails/rating.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('stars'=>$_smarty_tpl->tpl_vars['pushedArtikel']->value->fDurchschnittsBewertung), 0, false);
?>
                                                            <?php
}
}
/* {/block 'productdetails-pushed-success-include-rating'} */
/* {block 'productdetails-pushed-success-product-cell-details'} */
class Block_10520540696196260ddaa878_23415520 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin12->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php $_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin13->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <dl class="form-row">
                                                    <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productNo'),$_smarty_tpl ) );?>
:</dt>
                                                    <dd class="col-6"><?php echo $_smarty_tpl->tpl_vars['pushedArtikel']->value->cArtNr;?>
</dd>
                                                    <?php if (!empty($_smarty_tpl->tpl_vars['pushedArtikel']->value->cHersteller)) {?>
                                                        <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'manufacturer','section'=>'productDetails'),$_smarty_tpl ) );?>
:</dt>
                                                        <dd class="col-6"><?php echo $_smarty_tpl->tpl_vars['pushedArtikel']->value->cHersteller;?>
</dd>
                                                    <?php }?>
                                                    <?php if (!empty($_smarty_tpl->tpl_vars['pushedArtikel']->value->oMerkmale_arr)) {?>
                                                        <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'characteristics','section'=>'comparelist'),$_smarty_tpl ) );?>
:</dt>
                                                        <dd class="col-6 attr-characteristic">
                                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5899243666196260ddad7c4_15130727', 'productdetails-pushed-success-characteristics', $this->tplIndex);
?>

                                                        </dd>
                                                    <?php }?>
                                                    <?php if ((isset($_smarty_tpl->tpl_vars['pushedArtikel']->value->dMHD)) && (isset($_smarty_tpl->tpl_vars['pushedArtikel']->value->dMHD_de))) {?>
                                                        <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productMHDTool'),$_smarty_tpl ) );?>
:</dt>
                                                        <dd class="col-6"><?php echo $_smarty_tpl->tpl_vars['pushedArtikel']->value->dMHD_de;?>
</dd>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['artikeluebersicht']['artikeluebersicht_gewicht_anzeigen'] === 'Y' && (isset($_smarty_tpl->tpl_vars['pushedArtikel']->value->cGewicht)) && $_smarty_tpl->tpl_vars['pushedArtikel']->value->fGewicht > 0) {?>
                                                        <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingWeight'),$_smarty_tpl ) );?>
:</dt>
                                                        <dd class="col-6"><?php echo $_smarty_tpl->tpl_vars['pushedArtikel']->value->cGewicht;?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'weightUnit'),$_smarty_tpl ) );?>
</dd>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['artikeluebersicht']['artikeluebersicht_artikelgewicht_anzeigen'] === 'Y' && (isset($_smarty_tpl->tpl_vars['pushedArtikel']->value->cArtikelgewicht)) && $_smarty_tpl->tpl_vars['pushedArtikel']->value->fArtikelgewicht > 0) {?>
                                                        <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'productWeight'),$_smarty_tpl ) );?>
:</dt>
                                                        <dd class="col-6"><?php echo $_smarty_tpl->tpl_vars['pushedArtikel']->value->cArtikelgewicht;?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'weightUnit'),$_smarty_tpl ) );?>
</dd>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['bewertung']['bewertung_anzeigen'] === 'Y' && (int)$_smarty_tpl->tpl_vars['pushedArtikel']->value->fDurchschnittsBewertung !== 0) {?>
                                                        <dt class="col-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'ratingAverage'),$_smarty_tpl ) );?>
:</dt>
                                                        <dd class="col-6">
                                                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18951703496196260ddb7aa4_90814879', 'productdetails-pushed-success-include-rating', $this->tplIndex);
?>

                                                        </dd>
                                                    <?php }?>
                                                </dl>
                                            <?php $_block_repeat=false;
echo $_block_plugin13->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php $_block_repeat=false;
echo $_block_plugin12->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php
}
}
/* {/block 'productdetails-pushed-success-product-cell-details'} */
/* {block 'productdetails-pushed-success-product-cell-content'} */
class Block_12879694486196260dda7224_39586625 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <div class="productbox-inner<?php if ((isset($_smarty_tpl->tpl_vars['class']->value))) {?> <?php echo $_smarty_tpl->tpl_vars['class']->value;
}?>">
                            <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin8->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin9->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13200133656196260dda82a3_02082205', 'productdetails-pushed-success-product-cell-subheading', $this->tplIndex);
?>

                                <?php $_block_repeat=false;
echo $_block_plugin9->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>4,'class'=>"pushed-success-image-wrapper"));
$_block_repeat=true;
echo $_block_plugin10->render(array('cols'=>12,'md'=>4,'class'=>"pushed-success-image-wrapper"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14676256106196260dda9682_52347823', 'productdetails-pushed-success-product-cell-image', $this->tplIndex);
?>

                                <?php $_block_repeat=false;
echo $_block_plugin10->render(array('cols'=>12,'md'=>4,'class'=>"pushed-success-image-wrapper"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array());
$_block_repeat=true;
echo $_block_plugin11->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10520540696196260ddaa878_23415520', 'productdetails-pushed-success-product-cell-details', $this->tplIndex);
?>

                                <?php $_block_repeat=false;
echo $_block_plugin11->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin8->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        </div>
                    <?php
}
}
/* {/block 'productdetails-pushed-success-product-cell-content'} */
/* {block 'productdetails-pushed-success-product-cell-links'} */
class Block_20681947906196260ddb9b46_50147996 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"pushed-success-buttons"));
$_block_repeat=true;
echo $_block_plugin14->render(array('class'=>"pushed-success-buttons"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>6));
$_block_repeat=true;
echo $_block_plugin15->render(array('cols'=>12,'md'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php ob_start();
if (!$_smarty_tpl->tpl_vars['card']->value) {
echo "modal";
} else {
echo "alert";
}
$_prefixVariable2=ob_get_clean();
$_block_plugin16 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin16, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['pushedArtikel']->value->cURLFull,'class'=>"btn btn-outline-primary btn-block continue-shopping",'data'=>array("dismiss"=>$_prefixVariable2),'aria'=>array("label"=>"Close")));
$_block_repeat=true;
echo $_block_plugin16->render(array('href'=>$_smarty_tpl->tpl_vars['pushedArtikel']->value->cURLFull,'class'=>"btn btn-outline-primary btn-block continue-shopping",'data'=>array("dismiss"=>$_prefixVariable2),'aria'=>array("label"=>"Close")), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <i class="fa fa-arrow-circle-right"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'continueShopping','section'=>'checkout'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin16->render(array('href'=>$_smarty_tpl->tpl_vars['pushedArtikel']->value->cURLFull,'class'=>"btn btn-outline-primary btn-block continue-shopping",'data'=>array("dismiss"=>$_prefixVariable2),'aria'=>array("label"=>"Close")), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin15->render(array('cols'=>12,'md'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin17 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin17, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>6));
$_block_repeat=true;
echo $_block_plugin17->render(array('cols'=>12,'md'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'warenkorb.php'),$_smarty_tpl ) );
$_prefixVariable3=ob_get_clean();
$_block_plugin18 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin18, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable3,'class'=>"btn btn-primary btn-basket btn-block"));
$_block_repeat=true;
echo $_block_plugin18->render(array('href'=>$_prefixVariable3,'class'=>"btn btn-primary btn-basket btn-block"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <i class="fas fa-shopping-cart"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'gotoBasket'),$_smarty_tpl ) );?>

                                <?php $_block_repeat=false;
echo $_block_plugin18->render(array('href'=>$_prefixVariable3,'class'=>"btn btn-primary btn-basket btn-block"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin17->render(array('cols'=>12,'md'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin14->render(array('class'=>"pushed-success-buttons"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'productdetails-pushed-success-product-cell-links'} */
/* {block 'productdetails-pushed-success-product-cell'} */
class Block_5012591846196260dda5ea0_97467237 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php ob_start();
if ($_smarty_tpl->tpl_vars['showXSellingCart']->value) {
echo "6";
} else {
echo "12";
}
$_prefixVariable1=ob_get_clean();
$_block_plugin7 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin7, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>$_prefixVariable1));
$_block_repeat=true;
echo $_block_plugin7->render(array('cols'=>12,'md'=>$_prefixVariable1), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12879694486196260dda7224_39586625', 'productdetails-pushed-success-product-cell-content', $this->tplIndex);
?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20681947906196260ddb9b46_50147996', 'productdetails-pushed-success-product-cell-links', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin7->render(array('cols'=>12,'md'=>$_prefixVariable1), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'productdetails-pushed-success-product-cell'} */
/* {block 'productdetails-pushed-success-x-sell-heading'} */
class Block_20921797446196260ddc0397_23744601 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="productbox-title subheadline"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'customerWhoBoughtXBoughtAlsoY','section'=>'productDetails'),$_smarty_tpl ) );?>
</div>
                                <?php
}
}
/* {/block 'productdetails-pushed-success-x-sell-heading'} */
/* {block 'productdetails-pushed-success-include-product-slider'} */
class Block_21359817486196260ddc1457_73035004 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:snippets/product_slider.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('id'=>'','productlist'=>$_smarty_tpl->tpl_vars['Xselling']->value->Kauf->Artikel,'title'=>'','tplscope'=>'half'), 0, false);
?>
                                <?php
}
}
/* {/block 'productdetails-pushed-success-include-product-slider'} */
/* {block 'productdetails-pushed-success-x-sell'} */
class Block_15221294036196260ddbefb5_83473018 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php if ($_smarty_tpl->tpl_vars['showXSellingCart']->value) {?>
                    <?php $_block_plugin19 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin19, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>6,'class'=>"x-selling"));
$_block_repeat=true;
echo $_block_plugin19->render(array('cols'=>6,'class'=>"x-selling"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin20 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin20, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin20->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin21->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20921797446196260ddc0397_23744601', 'productdetails-pushed-success-x-sell-heading', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin21->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin22->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21359817486196260ddc1457_73035004', 'productdetails-pushed-success-include-product-slider', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin22->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin20->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin19->render(array('cols'=>6,'class'=>"x-selling"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php }?>
            <?php
}
}
/* {/block 'productdetails-pushed-success-x-sell'} */
/* {block 'productdetails-pushed-success'} */
class Block_4670999176196260dda0440_61788242 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'productdetails-pushed-success' => 
  array (
    0 => 'Block_4670999176196260dda0440_61788242',
  ),
  'productdetails-pushed-success-cart-note-heading' => 
  array (
    0 => 'Block_2272699516196260dda4a47_78062486',
  ),
  'productdetails-pushed-success-product-cell' => 
  array (
    0 => 'Block_5012591846196260dda5ea0_97467237',
  ),
  'productdetails-pushed-success-product-cell-content' => 
  array (
    0 => 'Block_12879694486196260dda7224_39586625',
  ),
  'productdetails-pushed-success-product-cell-subheading' => 
  array (
    0 => 'Block_13200133656196260dda82a3_02082205',
  ),
  'productdetails-pushed-success-product-cell-image' => 
  array (
    0 => 'Block_14676256106196260dda9682_52347823',
  ),
  'productdetails-pushed-success-product-cell-details' => 
  array (
    0 => 'Block_10520540696196260ddaa878_23415520',
  ),
  'productdetails-pushed-success-characteristics' => 
  array (
    0 => 'Block_5899243666196260ddad7c4_15130727',
  ),
  'productdetails-pushed-success-include-rating' => 
  array (
    0 => 'Block_18951703496196260ddb7aa4_90814879',
  ),
  'productdetails-pushed-success-product-cell-links' => 
  array (
    0 => 'Block_20681947906196260ddb9b46_50147996',
  ),
  'productdetails-pushed-success-x-sell' => 
  array (
    0 => 'Block_15221294036196260ddbefb5_83473018',
  ),
  'productdetails-pushed-success-x-sell-heading' => 
  array (
    0 => 'Block_20921797446196260ddc0397_23744601',
  ),
  'productdetails-pushed-success-include-product-slider' => 
  array (
    0 => 'Block_21359817486196260ddc1457_73035004',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div id="pushed-success" <?php if ($_smarty_tpl->tpl_vars['card']->value) {?>role="alert" class="card alert alert-dismissable"<?php }?>>
        <?php if ((isset($_smarty_tpl->tpl_vars['zuletztInWarenkorbGelegterArtikel']->value))) {?>
            <?php $_smarty_tpl->_assignInScope('pushedArtikel', $_smarty_tpl->tpl_vars['zuletztInWarenkorbGelegterArtikel']->value);?>
        <?php } else { ?>
            <?php $_smarty_tpl->_assignInScope('pushedArtikel', $_smarty_tpl->tpl_vars['Artikel']->value);?>
        <?php }?>
        <?php $_smarty_tpl->_assignInScope('showXSellingCart', (isset($_smarty_tpl->tpl_vars['Xselling']->value->Kauf)) && count($_smarty_tpl->tpl_vars['Xselling']->value->Kauf->Artikel) > 0);?>
        <?php if ($_smarty_tpl->tpl_vars['card']->value) {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2272699516196260dda4a47_78062486', 'productdetails-pushed-success-cart-note-heading', $this->tplIndex);
?>

            <div class="card-body">
        <?php }?>

        <?php $_block_plugin6 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin6, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin6->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5012591846196260dda5ea0_97467237', 'productdetails-pushed-success-product-cell', $this->tplIndex);
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15221294036196260ddbefb5_83473018', 'productdetails-pushed-success-x-sell', $this->tplIndex);
?>

        <?php $_block_repeat=false;
echo $_block_plugin6->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php if ($_smarty_tpl->tpl_vars['card']->value) {?></div><?php }?>
    </div>
<?php
}
}
/* {/block 'productdetails-pushed-success'} */
}
