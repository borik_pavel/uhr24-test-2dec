<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:40:39
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/blog/preview.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953e972d2f71_41842828',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b1284d06e403c69692025ae28e79f6f382bb8388' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/blog/preview.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/image.tpl' => 1,
    'file:snippets/author.tpl' => 1,
  ),
),false)) {
function content_61953e972d2f71_41842828 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_152952912861953e972bb6e0_01683295', 'blog-preview');
?>

<?php }
/* {block 'blog-preview-news-image'} */
class Block_12713141761953e972be0e3_48563178 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin162 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin162, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value));
$_block_repeat=true;
echo $_block_plugin162->render(array('href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <div class="newsbox-image">
                                <?php ob_start();
echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['newsItem']->value->getMetaTitle());
$_prefixVariable167=ob_get_clean();
$_smarty_tpl->_subTemplateRender('file:snippets/image.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('item'=>$_smarty_tpl->tpl_vars['newsItem']->value,'square'=>false,'alt'=>((string)$_smarty_tpl->tpl_vars['title']->value)." - ".$_prefixVariable167), 0, false);
?>
                            </div>
                            <meta itemprop="image" content="<?php echo $_smarty_tpl->tpl_vars['imageBaseURL']->value;
echo $_smarty_tpl->tpl_vars['newsItem']->value->getPreviewImage();?>
">
                        <?php $_block_repeat=false;
echo $_block_plugin162->render(array('href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'blog-preview-news-image'} */
/* {block 'blog-preview-news-header'} */
class Block_21692034361953e972bd303_55331213 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="newsbox-header">
                <?php if (!empty($_smarty_tpl->tpl_vars['newsItem']->value->getPreviewImage())) {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12713141761953e972be0e3_48563178', 'blog-preview-news-image', $this->tplIndex);
?>

                <?php }?>
            </div>
        <?php
}
}
/* {/block 'blog-preview-news-header'} */
/* {block 'blog-preview-include-author'} */
class Block_159719504061953e972c38a4_96890949 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                        <?php $_smarty_tpl->_subTemplateRender('file:snippets/author.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('oAuthor'=>$_smarty_tpl->tpl_vars['newsItem']->value->getAuthor(),'showModal'=>false), 0, false);
?>
                                    <?php
}
}
/* {/block 'blog-preview-include-author'} */
/* {block 'blog-preview-author'} */
class Block_171229645861953e972c2bd8_37910265 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php if ($_smarty_tpl->tpl_vars['newsItem']->value->getAuthor() !== null) {?>
                                <div class="newsbox-author">
                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_159719504061953e972c38a4_96890949', 'blog-preview-include-author', $this->tplIndex);
?>

                                </div>
                            <?php } else { ?>
                                <div itemprop="author publisher" itemscope itemtype="https://schema.org/Organization" class="d-none">
                                    <span itemprop="name"><?php echo $_smarty_tpl->tpl_vars['meta_publisher']->value;?>
</span>
                                    <meta itemprop="url" content="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
">
                                    <meta itemprop="logo" content="<?php echo $_smarty_tpl->tpl_vars['ShopLogoURL']->value;?>
">
                                </div>
                            <?php }?>
                            <time itemprop="dateModified" class="d-none"><?php echo $_smarty_tpl->tpl_vars['newsItem']->value->getDateCreated()->format('Y-m-d');?>
</time>
                            <time itemprop="datePublished" datetime="<?php echo $_smarty_tpl->tpl_vars['dDate']->value;?>
" class="d-none"><?php echo $_smarty_tpl->tpl_vars['dDate']->value;?>
</time>
                            <span class="align-middle"><?php echo $_smarty_tpl->tpl_vars['newsItem']->value->getDateValidFrom()->format('d.m.Y');?>
</span>
                        <?php
}
}
/* {/block 'blog-preview-author'} */
/* {block 'blog-preview-comments'} */
class Block_148291245361953e972c92d4_31219580 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin166 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin166, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>($_smarty_tpl->tpl_vars['newsItem']->value->getURL()).('#comments')));
$_block_repeat=true;
echo $_block_plugin166->render(array('href'=>($_smarty_tpl->tpl_vars['newsItem']->value->getURL()).('#comments')), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <span class="fas fa-comments"></span>
                                    <span class="sr-only">
                                            <?php if ($_smarty_tpl->tpl_vars['newsItem']->value->getCommentCount() === 1) {?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'newsComment','section'=>'news'),$_smarty_tpl ) );?>

                                            <?php } else { ?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'newsComments','section'=>'news'),$_smarty_tpl ) );?>

                                            <?php }?>
                                        </span>
                                    <span itemprop="commentCount"><?php echo $_smarty_tpl->tpl_vars['newsItem']->value->getCommentCount();?>
</span>
                                <?php $_block_repeat=false;
echo $_block_plugin166->render(array('href'=>($_smarty_tpl->tpl_vars['newsItem']->value->getURL()).('#comments')), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'blog-preview-comments'} */
/* {block 'blog-preview-heading'} */
class Block_70367167461953e972cd139_48776382 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin167 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin167, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('itemprop'=>"url",'href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value,'class'=>"newsbox-headline"));
$_block_repeat=true;
echo $_block_plugin167->render(array('itemprop'=>"url",'href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value,'class'=>"newsbox-headline"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <span itemprop="headline"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</span>
                    <?php $_block_repeat=false;
echo $_block_plugin167->render(array('itemprop'=>"url",'href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value,'class'=>"newsbox-headline"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'blog-preview-heading'} */
/* {block 'blog-preview-description'} */
class Block_203827812561953e972ce870_43078536 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <p itemprop="description">
                        <?php if (strlen(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['newsItem']->value->getPreview())) > 0) {?>
                            <?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['newsItem']->value->getPreview());?>

                        <?php } else { ?>
                            <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['newsItem']->value->getContent()),200,'' ));?>

                        <?php }?>
                    </p>
                <?php
}
}
/* {/block 'blog-preview-description'} */
/* {block 'blog-preview-news-body'} */
class Block_212521963761953e972c1547_34608668 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="newsbox-body">
                <?php $_block_plugin163 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin163, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin163->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php $_block_plugin164 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin164, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>8,'class'=>"blog-preview-author"));
$_block_repeat=true;
echo $_block_plugin164->render(array('cols'=>8,'class'=>"blog-preview-author"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_smarty_tpl->_assignInScope('dDate', $_smarty_tpl->tpl_vars['newsItem']->value->getDateValidFrom()->format('Y-m-d'));?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_171229645861953e972c2bd8_37910265', 'blog-preview-author', $this->tplIndex);
?>

                    <?php $_block_repeat=false;
echo $_block_plugin164->render(array('cols'=>8,'class'=>"blog-preview-author"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_plugin165 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin165, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>"auto",'class'=>"blog-preview-comment"));
$_block_repeat=true;
echo $_block_plugin165->render(array('cols'=>"auto",'class'=>"blog-preview-comment"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php if ((isset($_smarty_tpl->tpl_vars['Einstellungen']->value['news']['news_kommentare_nutzen'])) && $_smarty_tpl->tpl_vars['Einstellungen']->value['news']['news_kommentare_nutzen'] === 'Y') {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_148291245361953e972c92d4_31219580', 'blog-preview-comments', $this->tplIndex);
?>

                        <?php }?>
                    <?php $_block_repeat=false;
echo $_block_plugin165->render(array('cols'=>"auto",'class'=>"blog-preview-comment"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin163->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_70367167461953e972cd139_48776382', 'blog-preview-heading', $this->tplIndex);
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_203827812561953e972ce870_43078536', 'blog-preview-description', $this->tplIndex);
?>

            </div>
        <?php
}
}
/* {/block 'blog-preview-news-body'} */
/* {block 'blog-preview-news-footer'} */
class Block_148457281961953e972d1704_24951413 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="newsbox-footer">
                <?php $_block_plugin168 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin168, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value));
$_block_repeat=true;
echo $_block_plugin168->render(array('href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'moreLink','section'=>'news'),$_smarty_tpl ) );?>

                    <i class="fas fa-long-arrow-alt-right icon-mr-2"></i>
                <?php $_block_repeat=false;
echo $_block_plugin168->render(array('href'=>$_smarty_tpl->tpl_vars['newsItem']->value->getURL(),'title'=>$_smarty_tpl->tpl_vars['title']->value), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            </div>
        <?php
}
}
/* {/block 'blog-preview-news-footer'} */
/* {block 'blog-preview'} */
class Block_152952912861953e972bb6e0_01683295 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'blog-preview' => 
  array (
    0 => 'Block_152952912861953e972bb6e0_01683295',
  ),
  'blog-preview-news-header' => 
  array (
    0 => 'Block_21692034361953e972bd303_55331213',
  ),
  'blog-preview-news-image' => 
  array (
    0 => 'Block_12713141761953e972be0e3_48563178',
  ),
  'blog-preview-news-body' => 
  array (
    0 => 'Block_212521963761953e972c1547_34608668',
  ),
  'blog-preview-author' => 
  array (
    0 => 'Block_171229645861953e972c2bd8_37910265',
  ),
  'blog-preview-include-author' => 
  array (
    0 => 'Block_159719504061953e972c38a4_96890949',
  ),
  'blog-preview-comments' => 
  array (
    0 => 'Block_148291245361953e972c92d4_31219580',
  ),
  'blog-preview-heading' => 
  array (
    0 => 'Block_70367167461953e972cd139_48776382',
  ),
  'blog-preview-description' => 
  array (
    0 => 'Block_203827812561953e972ce870_43078536',
  ),
  'blog-preview-news-footer' => 
  array (
    0 => 'Block_148457281961953e972d1704_24951413',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_assignInScope('title', preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['newsItem']->value->getTitle()));?>
    <div itemprop="blogPost" itemscope=true itemtype="https://schema.org/BlogPosting" class="newsbox blog-preview">
        <meta itemprop="mainEntityOfPage" content="<?php echo $_smarty_tpl->tpl_vars['newsItem']->value->getURL();?>
">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21692034361953e972bd303_55331213', 'blog-preview-news-header', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_212521963761953e972c1547_34608668', 'blog-preview-news-body', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_148457281961953e972d1704_24951413', 'blog-preview-news-footer', $this->tplIndex);
?>

    </div>
<?php
}
}
/* {/block 'blog-preview'} */
}
