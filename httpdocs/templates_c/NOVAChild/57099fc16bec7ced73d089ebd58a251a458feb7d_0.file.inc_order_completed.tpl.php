<?php
/* Smarty version 3.1.39, created on 2021-11-19 20:33:50
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_order_completed.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6197fc1e95a7d7_87654682',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '57099fc16bec7ced73d089ebd58a251a458feb7d' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_order_completed.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6197fc1e95a7d7_87654682 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5218680626197fc1e952990_04137681', 'checkout-inc-order-completed');
?>

<?php }
/* {block 'checkout-inc-order-completed-alert'} */
class Block_2865383796197fc1e956489_42498649 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <p class="order-confirmation-note"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderConfirmationPost','section'=>'checkout'),$_smarty_tpl ) );?>
</p>
        <?php
}
}
/* {/block 'checkout-inc-order-completed-alert'} */
/* {block 'checkout-inc-order-completed-id-payment'} */
class Block_18397018996197fc1e957c25_32561987 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <ul class="list-unstyled order-confirmation-details">
                <li><strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourOrderId','section'=>'checkout'),$_smarty_tpl ) );?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>
</li>
                <li><strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourChosenPaymentOption','section'=>'checkout'),$_smarty_tpl ) );?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cZahlungsartName;?>
</li>
            </ul>
        <?php
}
}
/* {/block 'checkout-inc-order-completed-id-payment'} */
/* {block 'checkout-inc-order-completed'} */
class Block_5218680626197fc1e952990_04137681 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-inc-order-completed' => 
  array (
    0 => 'Block_5218680626197fc1e952990_04137681',
  ),
  'checkout-inc-order-completed-alert' => 
  array (
    0 => 'Block_2865383796197fc1e956489_42498649',
  ),
  'checkout-inc-order-completed-id-payment' => 
  array (
    0 => 'Block_18397018996197fc1e957c25_32561987',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('id'=>"order-confirmation"));
$_block_repeat=true;
echo $_block_plugin1->render(array('id'=>"order-confirmation"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2865383796197fc1e956489_42498649', 'checkout-inc-order-completed-alert', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18397018996197fc1e957c25_32561987', 'checkout-inc-order-completed-id-payment', $this->tplIndex);
?>

    <?php $_block_repeat=false;
echo $_block_plugin1->render(array('id'=>"order-confirmation"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
/* {/block 'checkout-inc-order-completed'} */
}
