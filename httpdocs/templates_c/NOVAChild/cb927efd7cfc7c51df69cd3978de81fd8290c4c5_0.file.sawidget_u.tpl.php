<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:23:33
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/shopauskunft/frontend/tpl/sawidget_u.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953a9516d3e7_37045284',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cb927efd7cfc7c51df69cd3978de81fd8290c4c5' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/shopauskunft/frontend/tpl/sawidget_u.tpl',
      1 => 1632584625,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./starRating.tpl' => 1,
    'file:./single_star_evaluation.tpl' => 1,
    'file:./textRatings.tpl' => 1,
  ),
),false)) {
function content_61953a9516d3e7_37045284 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style>
    #sa_widget_knm {
        margin-<?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->vertical_key;?>
: <?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->vertical_value;?>
;
        margin-<?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->horizontal_key;?>
: <?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->horizontal_value;?>
;
    }
    @media (max-width: 992px) {
        #sa_widget_knm {
        margin-<?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->vertical_key;?>
: <?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->vertical_value;?>
;
        margin-<?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->horizontal_key;?>
: <?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->horizontal_value;?>
;
        }
    }

</style>

<div class="<?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->position_class;?>
 <?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->position_class;?>
 sa_widget_zIndex" >
    <div class="<?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->position_class;?>
 <?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->position_class;?>
 sa_widget_zIndex" style="position: absolute">
        <div id="sa_widget_knm" class="<?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->position_class;?>
 <?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->position_class;?>
 sa_widget_hidden-xs">
            <div id="ocd" class="ocd sa_box_shadow" style="text-align: center;background-color: #fff;padding: 8px">
                <div style="position: relative; min-height: 65px; display: flex; align-items: center; flex-direction: column;">
                    <i class="fa fa-spinner fa-pulse" style=" position: absolute; margin-top: 15px;"></i>
                    <img style="z-index: 1; max-width: 64px; margin-left: auto; margin-right: auto;"
                         src="<?php echo $_smarty_tpl->tpl_vars['shopauskunt_logo_path']->value;?>
" alt="Shopauskunft_logo" title="Shopauskunft"/>
                </div>
                <span id="closing_cross_small" class="fa fa-times closeBtn"
                      style="position: absolute; top: 4px;right: 4px;"></span>
                <div>
                    <p>Shopauskunft</p>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['reviewCount']->value > 0) {?>
                    <div>
                        <?php $_smarty_tpl->_subTemplateRender("file:./starRating.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('rating'=>$_smarty_tpl->tpl_vars['shopauskunft_overAllRating']->value), 0, false);
?>
                    </div>
                    <div>
                        <span><?php echo $_smarty_tpl->tpl_vars['shopauskunft_overAllRating']->value;?>
 / 5,00</span><br/>
                        <span style="font-size: 10px"><?php echo $_smarty_tpl->tpl_vars['reviewCount']->value;?>
 Bewertungen</span>
                    </div>
                <?php } else { ?>
                    <span>keine</span>
                    <br>
                    <span>Bewertungen</span>
                <?php }?>
            </div>
        </div>
        <div id="sa_widget_bar_extended" class="hide_module sa_box_shadow sa_widget_hidden-xs">
            <div style="width: 100%; max-width: 320px; background-color: #fff;"
                 class="sa_box_shadow <?php echo $_smarty_tpl->tpl_vars['settingsMd']->value->position_class;?>
 <?php echo $_smarty_tpl->tpl_vars['settingsSm']->value->position_class;?>
">
                <a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['shopauskunft_deep_link']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['shopName']->value;?>
"
                   style="color: #000;text-decoration: none;">
                    <div class="col-xs-12"
                         style="display: flex; text-align: center; font-size: 18px; margin-top: 20px;position: relative">
                        <div class="shopauskunftLogoBox">
                            <div class="shopauskunftLogo" style="margin: 0 6px 0 0;">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['shopauskunt_logo_path']->value;?>
" style=" width: 100%"
                                     alt="<?php echo $_smarty_tpl->tpl_vars['statisfiedCustomerPercentage']->value;?>
% zufriedene Kunden (<?php echo $_smarty_tpl->tpl_vars['reviewCount']->value;?>
 Bewertungen)"/>
                            </div>
                        </div>
                        <div class="sa-head-text" style="margin: 0 0 0 8px; display: flex; flex-direction: column;">
                            <span style="margin-right: auto; font-size: 18px">Shopauskunft</span>
                            <?php if ($_smarty_tpl->tpl_vars['reviewCount']->value > 0) {?>
                                <span style="margin-right: auto; font-size: 14px"><?php echo $_smarty_tpl->tpl_vars['shopauskunft_overAllRating']->value;?>
 / 5,00 (<?php echo $_smarty_tpl->tpl_vars['reviewCount']->value;?>
 Bewertungen)</span>
                            <?php } else { ?>
                                <span style="margin-right: auto; font-size: 14px">keine Bewertungen</span>
                            <?php }?>
                        </div>
                    </div>
                </a>
                <span id="closing_cross" class="fa fa-times closeBtn"
                      style="position: absolute; top: 15px;right: 15px; font-size: 18px;"></span>
                <div>
                    <div class="col-xs-12 sahrbox">
                        <hr class="sahr"/>
                    </div>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['detailedReview']->value, 'review', false, 'key', 'sahr', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
$_smarty_tpl->tpl_vars['review']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['review']->value) {
$_smarty_tpl->tpl_vars['review']->do_else = false;
$_smarty_tpl->tpl_vars['__smarty_foreach_sahr']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_sahr']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_sahr']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_sahr']->value['total'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['key']->value == 0 || ($_smarty_tpl->tpl_vars['key']->value+1)%2 == 0) {?>
                            <div style="padding: 0 15px;">
                        <?php }?>
                        <div class="col-xs-6<?php if ($_smarty_tpl->tpl_vars['key']->value%2 == 0) {?> vr<?php }?>" style="cursor: default;">
                            <?php $_smarty_tpl->_subTemplateRender('file:./single_star_evaluation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('iconClass'=>$_smarty_tpl->tpl_vars['review']->value['iconClass'],'rating'=>$_smarty_tpl->tpl_vars['review']->value['rating'],'evaluationTitle'=>$_smarty_tpl->tpl_vars['review']->value['name']), 0, true);
?>
                        </div>
                        <?php if ($_smarty_tpl->tpl_vars['key']->value%2 == 0) {?>
                            </div>
                            <?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_sahr']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_sahr']->value['last'] : null)) {?>
                                <div class="col-xs-12 sahrbox">
                                    <hr class="sahr"/>
                                </div>
                            <?php } else { ?>
                                <div class="col-xs-12 sahrbox">
                                    <hr class="sahr"/>
                                </div>
                                <?php $_smarty_tpl->_subTemplateRender("file:./textRatings.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('ratingList'=>$_smarty_tpl->tpl_vars['ratingList']->value), 0, true);
?>
                                <div id="safoot"></div>
                            <?php }?>
                        <?php }?>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['settingsGeneral']->value->jQuery == "1") {?>
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"><?php echo '</script'; ?>
>
<?php }?>

<?php echo '<script'; ?>
 <?php if ($_smarty_tpl->tpl_vars['settingsGeneral']->value->consentManagerRule != '') {?> data-cmp-vendor="<?php echo $_smarty_tpl->tpl_vars['settingsGeneral']->value->consentManagerRule;?>
" class="cmplazyload"<?php }?>>
    function knmSaWidgetSetCookie() {
        let now = new Date();
        let time = now.getTime();
        let expireTime = time + <?php echo $_smarty_tpl->tpl_vars['sessionLifetime']->value*1000;?>
;
        now.setTime(expireTime);
        document.cookie = 'hideKnmSaWidget=true;expires=' + now.toGMTString() + ';';
    }


<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 <?php if ($_smarty_tpl->tpl_vars['settingsGeneral']->value->consentManagerRule != '') {?>data-cmp-vendor="<?php echo $_smarty_tpl->tpl_vars['settingsGeneral']->value->consentManagerRule;?>
" class="cmplazyload"<?php }?>>
    let specifiedElement = document.getElementById('sa_widget_bar_extended');
    let smallWidget = document.getElementById('sa_widget_knm');
    let bigWidgetClosingCross = document.getElementById('closing_cross');
    let smallWidgetClosingCross = document.getElementById('closing_cross_small');
    let hideCompeletly = false;
    let animationspeed = 750;

    function fadeEleOut(eleId) {
        $(eleId).fadeOut(animationspeed, function () {
            $(eleId).addClass("hide_module");
        });
    }

    function fadeEleIn(eleId) {
        $(eleId).fadeIn(animationspeed, function () {
            $(eleId).removeClass("hide_module");
        });
    }

    document.addEventListener('scroll', function () {
        fadeEleOut("#sa_widget_bar_extended");
        if (!hideCompeletly) {
            fadeEleIn("#sa_widget_knm");
        }
    });

    document.addEventListener('click', function (event) {
        let isClickInside = specifiedElement.contains(event.target);
        let isClickInsideOtherElement = smallWidget.contains(event.target);
        let bigCrossIsClicked = bigWidgetClosingCross.contains(event.target);
        let smallCrossIsClicked = smallWidgetClosingCross.contains(event.target);

        if (!hideCompeletly) {
            if (smallCrossIsClicked) {
                fadeEleOut("#sa_widget_knm");
                $("#sa_widget_bar_extended").addClass("hide_module");
            }
            if (bigCrossIsClicked) {
                fadeEleIn("#sa_widget_knm");
                fadeEleOut("#sa_widget_bar_extended");
            }
            if (!isClickInside && !isClickInsideOtherElement) {
                fadeEleIn("#sa_widget_knm");
                fadeEleOut("#sa_widget_bar_extended");
            }
            if (isClickInsideOtherElement && !smallCrossIsClicked) {
                fadeEleIn("#sa_widget_bar_extended");
                fadeEleOut("#sa_widget_knm");
            }
            if (smallCrossIsClicked) {
                fadeEleOut("#sa_widget_knm");
                hideCompeletly = true;
                if (typeof knmSaWidgetSetCookie !== "undefined") {
                    knmSaWidgetSetCookie();
                }
            }
        }
    });
<?php echo '</script'; ?>
>
<?php }
}
