<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:33:26
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/filter/active_filter.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61966436825343_16554222',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c3556aeff73fdc3c111fad21bfae0c40ca95736b' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/filter/active_filter.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61966436825343_16554222 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3194909476196643680f1e1_36689864', 'snippets-filter-active-filter');
?>

<?php }
/* {block 'snippets-filter-active-filter-values'} */
class Block_1233496150619664368172b2_34615100 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'deleteFilter'),$_smarty_tpl ) );
$_prefixVariable22=ob_get_clean();
$_block_plugin33 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin33, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['activeFilter']->value->getUnsetFilterURL($_smarty_tpl->tpl_vars['filterOption']->value->getValue()),'rel'=>"nofollow",'title'=>$_prefixVariable22,'class'=>"btn btn-outline-secondary btn-sm filter-type-".((string)$_smarty_tpl->tpl_vars['activeFilter']->value->getNiceName())." snippets-filter-item js-filter-item"));
$_block_repeat=true;
echo $_block_plugin33->render(array('href'=>$_smarty_tpl->tpl_vars['activeFilter']->value->getUnsetFilterURL($_smarty_tpl->tpl_vars['filterOption']->value->getValue()),'rel'=>"nofollow",'title'=>$_prefixVariable22,'class'=>"btn btn-outline-secondary btn-sm filter-type-".((string)$_smarty_tpl->tpl_vars['activeFilter']->value->getNiceName())." snippets-filter-item js-filter-item"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo $_smarty_tpl->tpl_vars['filterOption']->value->getFrontendName();?>
<span class="fa fa-times snippets-filter-item-icon-left"></span>
                                <?php $_block_repeat=false;
echo $_block_plugin33->render(array('href'=>$_smarty_tpl->tpl_vars['activeFilter']->value->getUnsetFilterURL($_smarty_tpl->tpl_vars['filterOption']->value->getValue()),'rel'=>"nofollow",'title'=>$_prefixVariable22,'class'=>"btn btn-outline-secondary btn-sm filter-type-".((string)$_smarty_tpl->tpl_vars['activeFilter']->value->getNiceName())." snippets-filter-item js-filter-item"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'snippets-filter-active-filter-values'} */
/* {block 'snippets-filter-active-filter-value'} */
class Block_8653170126196643681bf02_99762427 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'deleteFilter'),$_smarty_tpl ) );
$_prefixVariable23=ob_get_clean();
$_block_plugin34 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin34, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['activeFilter']->value->getUnsetFilterURL($_smarty_tpl->tpl_vars['activeFilter']->value->getValue()),'rel'=>"nofollow",'title'=>$_prefixVariable23,'class'=>"btn btn-outline-secondary btn-sm filter-type-".((string)$_smarty_tpl->tpl_vars['activeFilter']->value->getNiceName())." snippets-filter-item js-filter-item"));
$_block_repeat=true;
echo $_block_plugin34->render(array('href'=>$_smarty_tpl->tpl_vars['activeFilter']->value->getUnsetFilterURL($_smarty_tpl->tpl_vars['activeFilter']->value->getValue()),'rel'=>"nofollow",'title'=>$_prefixVariable23,'class'=>"btn btn-outline-secondary btn-sm filter-type-".((string)$_smarty_tpl->tpl_vars['activeFilter']->value->getNiceName())." snippets-filter-item js-filter-item"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php echo $_smarty_tpl->tpl_vars['activeValues']->value->getFrontendName();?>
<span class="fa fa-times snippets-filter-item-icon-left"></span>
                            <?php $_block_repeat=false;
echo $_block_plugin34->render(array('href'=>$_smarty_tpl->tpl_vars['activeFilter']->value->getUnsetFilterURL($_smarty_tpl->tpl_vars['activeFilter']->value->getValue()),'rel'=>"nofollow",'title'=>$_prefixVariable23,'class'=>"btn btn-outline-secondary btn-sm filter-type-".((string)$_smarty_tpl->tpl_vars['activeFilter']->value->getNiceName())." snippets-filter-item js-filter-item"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'snippets-filter-active-filter-value'} */
/* {block 'snippets-filter-active-filter-remove'} */
class Block_171664679461966436821415_71663361 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'removeFilters'),$_smarty_tpl ) );
$_prefixVariable24=ob_get_clean();
$_block_plugin35 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin35, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['NaviFilter']->value->getURL()->getUnsetAll(),'title'=>$_prefixVariable24,'class'=>'snippets-filter-item-all js-filter-item'));
$_block_repeat=true;
echo $_block_plugin35->render(array('href'=>$_smarty_tpl->tpl_vars['NaviFilter']->value->getURL()->getUnsetAll(),'title'=>$_prefixVariable24,'class'=>'snippets-filter-item-all js-filter-item'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'removeFilters'),$_smarty_tpl ) );?>

                    <?php $_block_repeat=false;
echo $_block_plugin35->render(array('href'=>$_smarty_tpl->tpl_vars['NaviFilter']->value->getURL()->getUnsetAll(),'title'=>$_prefixVariable24,'class'=>'snippets-filter-item-all js-filter-item'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'snippets-filter-active-filter-remove'} */
/* {block 'snippets-filter-active-filter-content'} */
class Block_12533480461966436810407_64366919 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div class="active-filters">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['NaviFilter']->value->getActiveFilters(), 'activeFilter');
$_smarty_tpl->tpl_vars['activeFilter']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['activeFilter']->value) {
$_smarty_tpl->tpl_vars['activeFilter']->do_else = false;
?>
                <?php $_smarty_tpl->_assignInScope('activeFilterValue', $_smarty_tpl->tpl_vars['activeFilter']->value->getValue());?>
                <?php $_smarty_tpl->_assignInScope('activeValues', $_smarty_tpl->tpl_vars['activeFilter']->value->getActiveValues());?>
                <?php if ($_smarty_tpl->tpl_vars['activeFilterValue']->value !== null) {?>
                    <?php if (is_array($_smarty_tpl->tpl_vars['activeValues']->value)) {?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['activeValues']->value, 'filterOption');
$_smarty_tpl->tpl_vars['filterOption']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['filterOption']->value) {
$_smarty_tpl->tpl_vars['filterOption']->do_else = false;
?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1233496150619664368172b2_34615100', 'snippets-filter-active-filter-values', $this->tplIndex);
?>

                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    <?php } else { ?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8653170126196643681bf02_99762427', 'snippets-filter-active-filter-value', $this->tplIndex);
?>

                    <?php }?>
                <?php }?>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php if ($_smarty_tpl->tpl_vars['NaviFilter']->value->getURL()->getUnsetAll() !== null) {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_171664679461966436821415_71663361', 'snippets-filter-active-filter-remove', $this->tplIndex);
?>

            <?php }?>
        </div>
    <?php
}
}
/* {/block 'snippets-filter-active-filter-content'} */
/* {block 'snippets-filter-active-filter'} */
class Block_3194909476196643680f1e1_36689864 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-filter-active-filter' => 
  array (
    0 => 'Block_3194909476196643680f1e1_36689864',
  ),
  'snippets-filter-active-filter-content' => 
  array (
    0 => 'Block_12533480461966436810407_64366919',
  ),
  'snippets-filter-active-filter-values' => 
  array (
    0 => 'Block_1233496150619664368172b2_34615100',
  ),
  'snippets-filter-active-filter-value' => 
  array (
    0 => 'Block_8653170126196643681bf02_99762427',
  ),
  'snippets-filter-active-filter-remove' => 
  array (
    0 => 'Block_171664679461966436821415_71663361',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if ($_smarty_tpl->tpl_vars['NaviFilter']->value->getFilterCount() > 0) {?>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12533480461966436810407_64366919', 'snippets-filter-active-filter-content', $this->tplIndex);
?>

<?php }
}
}
/* {/block 'snippets-filter-active-filter'} */
}
