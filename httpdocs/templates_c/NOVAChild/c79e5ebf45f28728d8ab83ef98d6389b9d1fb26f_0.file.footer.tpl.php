<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:40:39
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_search/frontend/template/layout/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953e97321728_01292493',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c79e5ebf45f28728d8ab83ef98d6389b9d1fb26f' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/jtl_search/frontend/template/layout/footer.tpl',
      1 => 1624876656,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61953e97321728_01292493 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_110991544561953e9731c1e7_41171184', "layout-footer-js");
?>

<?php }
/* {block "layout-footer-js"} */
class Block_110991544561953e9731c1e7_41171184 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'layout-footer-js' => 
  array (
    0 => 'Block_110991544561953e9731c1e7_41171184',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_block_plugin170 = isset($_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0] : null;
if (!is_callable(array($_block_plugin170, 'inlineScript'))) {
throw new SmartyException('block tag \'inline_script\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inline_script', array());
$_block_repeat=true;
echo $_block_plugin170->inlineScript(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php echo '<script'; ?>
>
        window.addEventListener('load',function () {
            if (typeof $.fn.jtl_search !== 'undefined') {
                <?php if (!$_smarty_tpl->tpl_vars['isMobile']->value || $_smarty_tpl->tpl_vars['isTablet']->value) {?>
                    $('#search-header').jtl_search({
                        'align' : '<?php echo $_smarty_tpl->tpl_vars['jtl_search_align']->value;?>
',
                        'url' : '<?php echo $_smarty_tpl->tpl_vars['jtl_search_frontendURL']->value;?>
'
                    });
                <?php }?>
                $('#search-header-mobile-top').jtl_search({
                    'align' : 'full-width',
                    'url' : '<?php echo $_smarty_tpl->tpl_vars['jtl_search_frontendURL']->value;?>
',
                    'class': 'jtl-search-mobile-top'
                });
                $('#search-header-mobile-fixed').jtl_search({
                    'align' : 'full-width',
                    'url' : '<?php echo $_smarty_tpl->tpl_vars['jtl_search_frontendURL']->value;?>
',
                    'class': 'jtl-search-mobile-fixed'
                });
                <?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['template']['theme']['mobile_search_type'] === 'dropdown') {?>
                    $('#search-header-desktop').jtl_search({
                        'align' : 'full-width',
                        'url' : '<?php echo $_smarty_tpl->tpl_vars['jtl_search_frontendURL']->value;?>
',
                        'class': 'jtl-search-mobile-dropdown'
                    });
                <?php }?>
            }
        });
        <?php echo '</script'; ?>
>
    <?php $_block_repeat=false;
echo $_block_plugin170->inlineScript(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
/* {/block "layout-footer-js"} */
}
