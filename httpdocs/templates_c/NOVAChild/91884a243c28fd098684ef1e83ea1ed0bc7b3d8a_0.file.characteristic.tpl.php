<?php
/* Smarty version 3.1.39, created on 2021-11-19 20:14:17
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/filter/characteristic.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6197f78987c0a7_54073626',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '91884a243c28fd098684ef1e83ea1ed0bc7b3d8a' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/snippets/filter/characteristic.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/filter/search_in_items.tpl' => 1,
  ),
),false)) {
function content_6197f78987c0a7_54073626 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16766633566197f7898450c9_01630293', 'snippets-filter-characteristic');
?>

<?php }
/* {block 'snippets-filter-characteristic-include-search-in-items'} */
class Block_6002531416197f789848840_03830859 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:snippets/filter/search_in_items.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('itemCount'=>count($_smarty_tpl->tpl_vars['Merkmal']->value->getOptions()),'name'=>$_smarty_tpl->tpl_vars['Merkmal']->value->getName()), 0, false);
?>
    <?php
}
}
/* {/block 'snippets-filter-characteristic-include-search-in-items'} */
/* {block 'snippets-filter-characteristics-dropdown'} */
class Block_6385102246197f78984f360_53701334 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php ob_start();
if ($_smarty_tpl->tpl_vars['attributeValue']->value->isActive()) {
echo "active";
}
$_prefixVariable12=ob_get_clean();
ob_start();
if (!empty($_smarty_tpl->tpl_vars['attributeValue']->value->getURL())) {
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getURL();
} else {
echo "#";
}
$_prefixVariable13=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') === 'BILD') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
}
$_prefixVariable14=ob_get_clean();
$_block_plugin21 = isset($_smarty_tpl->smarty->registered_plugins['block']['dropdownitem'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['dropdownitem'][0][0] : null;
if (!is_callable(array($_block_plugin21, 'render'))) {
throw new SmartyException('block tag \'dropdownitem\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('dropdownitem', array('class'=>$_prefixVariable12." filter-item",'href'=>$_prefixVariable13,'title'=>$_prefixVariable14));
$_block_repeat=true;
echo $_block_plugin21->render(array('class'=>$_prefixVariable12." filter-item",'href'=>$_prefixVariable13,'title'=>$_prefixVariable14), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <div class="box-link-wrapper">
                        <i class="far fa-<?php if ($_smarty_tpl->tpl_vars['attributeValue']->value->isActive()) {?>check-<?php }?>square snippets-filter-item-icon-right"></i>
                        <?php if (!empty($_smarty_tpl->tpl_vars['attributeImageURL']->value)) {?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('lazy'=>true,'webp'=>true,'src'=>$_smarty_tpl->tpl_vars['attributeImageURL']->value,'alt'=>htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true),'class'=>"vmiddle"),$_smarty_tpl ) );?>

                        <?php }?>
                        <span class="word-break filter-item-value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {?>
                            <?php $_block_plugin22 = isset($_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0] : null;
if (!is_callable(array($_block_plugin22, 'render'))) {
throw new SmartyException('block tag \'badge\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('badge', array('variant'=>"outline-secondary"));
$_block_repeat=true;
echo $_block_plugin22->render(array('variant'=>"outline-secondary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
$_block_repeat=false;
echo $_block_plugin22->render(array('variant'=>"outline-secondary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php }?>
                    </div>
                <?php $_block_repeat=false;
echo $_block_plugin21->render(array('class'=>$_prefixVariable12." filter-item",'href'=>$_prefixVariable13,'title'=>$_prefixVariable14), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'snippets-filter-characteristics-dropdown'} */
/* {block 'snippets-filter-characteristics-more-top'} */
class Block_14081818906197f78985a089_06062266 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <div class="collapse <?php if ($_smarty_tpl->tpl_vars['Merkmal']->value->isActive()) {?> show<?php }?>" id="box-collps-filter-attribute-<?php echo $_smarty_tpl->tpl_vars['Merkmal']->value->getValue();?>
" aria-expanded="false" role="button">
                        <ul class="nav <?php if ($_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') !== 'BILD') {?>flex-column<?php }?>">
                    <?php $_smarty_tpl->_assignInScope('collapseInit', true);?>
                <?php
}
}
/* {/block 'snippets-filter-characteristics-more-top'} */
/* {block 'snippets-filter-characteristics-nav-text'} */
class Block_11842066566197f78985d105_86148247 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php ob_start();
if ($_smarty_tpl->tpl_vars['attributeValue']->value->isActive()) {
echo "active";
}
$_prefixVariable16=ob_get_clean();
ob_start();
if (!empty($_smarty_tpl->tpl_vars['attributeValue']->value->getURL())) {
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getURL();
} else {
echo "#";
}
$_prefixVariable17=ob_get_clean();
ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
$_prefixVariable18=ob_get_clean();
$_block_plugin23 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin23, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>$_prefixVariable16." filter-item",'href'=>$_prefixVariable17,'title'=>$_prefixVariable18,'rel'=>"nofollow"));
$_block_repeat=true;
echo $_block_plugin23->render(array('class'=>$_prefixVariable16." filter-item",'href'=>$_prefixVariable17,'title'=>$_prefixVariable18,'rel'=>"nofollow"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <div class="box-link-wrapper">
                                <i class="far fa-<?php if ($_smarty_tpl->tpl_vars['attributeValue']->value->isActive()) {?>check-<?php }?>square snippets-filter-item-icon-right"></i>
                                <?php if (!empty($_smarty_tpl->tpl_vars['attributeImageURL']->value)) {?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('lazy'=>true,'webp'=>true,'src'=>$_smarty_tpl->tpl_vars['attributeImageURL']->value,'alt'=>htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true),'class'=>"vmiddle"),$_smarty_tpl ) );?>

                                <?php }?>
                                <span class="word-break filter-item-value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);?>
</span>
                                <?php if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {?>
                                    <?php $_block_plugin24 = isset($_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0] : null;
if (!is_callable(array($_block_plugin24, 'render'))) {
throw new SmartyException('block tag \'badge\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('badge', array('variant'=>"outline-secondary"));
$_block_repeat=true;
echo $_block_plugin24->render(array('variant'=>"outline-secondary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
$_block_repeat=false;
echo $_block_plugin24->render(array('variant'=>"outline-secondary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php }?>
                            </div>
                        <?php $_block_repeat=false;
echo $_block_plugin23->render(array('class'=>$_prefixVariable16." filter-item",'href'=>$_prefixVariable17,'title'=>$_prefixVariable18,'rel'=>"nofollow"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'snippets-filter-characteristics-nav-text'} */
/* {block 'snippets-filter-characteristics-nav-image'} */
class Block_18018930966197f789864bd3_28886789 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php ob_start();
if (!empty($_smarty_tpl->tpl_vars['attributeValue']->value->getURL())) {
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getURL();
} else {
echo "#";
}
$_prefixVariable19=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
echo ": ";
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
}
$_prefixVariable20=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['attributeValue']->value->isActive()) {
echo "active";
}
$_prefixVariable21=ob_get_clean();
$_block_plugin25 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin25, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable19,'title'=>$_prefixVariable20,'data'=>array("toggle"=>"tooltip","placement"=>"top","boundary"=>"window"),'class'=>$_prefixVariable21." filter-item",'rel'=>"nofollow"));
$_block_repeat=true;
echo $_block_plugin25->render(array('href'=>$_prefixVariable19,'title'=>$_prefixVariable20,'data'=>array("toggle"=>"tooltip","placement"=>"top","boundary"=>"window"),'class'=>$_prefixVariable21." filter-item",'rel'=>"nofollow"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php ob_start();
if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
echo ": ";
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
}
$_prefixVariable22=ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('lazy'=>true,'webp'=>true,'src'=>$_smarty_tpl->tpl_vars['attributeImageURL']->value,'alt'=>htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true),'title'=>$_prefixVariable22,'class'=>"vmiddle filter-img"),$_smarty_tpl ) );?>

                            <span class="d-none filter-item-value">
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);?>

                            </span>
                        <?php $_block_repeat=false;
echo $_block_plugin25->render(array('href'=>$_prefixVariable19,'title'=>$_prefixVariable20,'data'=>array("toggle"=>"tooltip","placement"=>"top","boundary"=>"window"),'class'=>$_prefixVariable21." filter-item",'rel'=>"nofollow"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'snippets-filter-characteristics-nav-image'} */
/* {block 'snippets-filter-characteristics-nav-else'} */
class Block_4421183006197f78986dad8_43118718 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php ob_start();
if (!empty($_smarty_tpl->tpl_vars['attributeValue']->value->getURL())) {
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getURL();
} else {
echo "#";
}
$_prefixVariable23=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
echo ": ";
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
}
$_prefixVariable24=ob_get_clean();
ob_start();
if ($_smarty_tpl->tpl_vars['attributeValue']->value->isActive()) {
echo "active";
}
$_prefixVariable25=ob_get_clean();
$_block_plugin26 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin26, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable23,'title'=>$_prefixVariable24,'class'=>$_prefixVariable25." filter-item",'rel'=>"nofollow"));
$_block_repeat=true;
echo $_block_plugin26->render(array('href'=>$_prefixVariable23,'title'=>$_prefixVariable24,'class'=>$_prefixVariable25." filter-item",'rel'=>"nofollow"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <div class="box-link-wrapper">
                                <?php if (!empty($_smarty_tpl->tpl_vars['attributeImageURL']->value)) {?>
                                    <?php ob_start();
if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
echo ": ";
echo (string)$_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);
}
$_prefixVariable26=ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['image'][0], array( array('lazy'=>true,'webp'=>true,'src'=>$_smarty_tpl->tpl_vars['attributeImageURL']->value,'alt'=>htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true),'title'=>$_prefixVariable26,'class'=>"vmiddle filter-img"),$_smarty_tpl ) );?>

                                <?php }?>
                                <span class="word-break filter-item-value">
                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attributeValue']->value->getValue(), ENT_QUOTES, 'utf-8', true);?>

                                </span>
                                <?php if ($_smarty_tpl->tpl_vars['showFilterCount']->value) {?>
                                    <?php $_block_plugin27 = isset($_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['badge'][0][0] : null;
if (!is_callable(array($_block_plugin27, 'render'))) {
throw new SmartyException('block tag \'badge\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('badge', array('variant'=>"outline-secondary"));
$_block_repeat=true;
echo $_block_plugin27->render(array('variant'=>"outline-secondary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo $_smarty_tpl->tpl_vars['attributeValue']->value->getCount();
$_block_repeat=false;
echo $_block_plugin27->render(array('variant'=>"outline-secondary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php }?>
                            </div>
                        <?php $_block_repeat=false;
echo $_block_plugin26->render(array('href'=>$_prefixVariable23,'title'=>$_prefixVariable24,'class'=>$_prefixVariable25." filter-item",'rel'=>"nofollow"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'snippets-filter-characteristics-nav-else'} */
/* {block 'snippets-filter-characteristics-nav'} */
class Block_12058077376197f78985c293_93585666 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php ob_start();
echo $_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp');
$_prefixVariable15 = ob_get_clean();
if ($_prefixVariable15 === 'TEXT') {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11842066566197f78985d105_86148247', 'snippets-filter-characteristics-nav-text', $this->tplIndex);
?>

                <?php } elseif ($_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') === 'BILD' && $_smarty_tpl->tpl_vars['attributeImageURL']->value !== null) {?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18018930966197f789864bd3_28886789', 'snippets-filter-characteristics-nav-image', $this->tplIndex);
?>

                <?php } else { ?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4421183006197f78986dad8_43118718', 'snippets-filter-characteristics-nav-else', $this->tplIndex);
?>

                <?php }?>
            <?php
}
}
/* {/block 'snippets-filter-characteristics-nav'} */
/* {block 'snippets-filter-characteristics-more-bottom'} */
class Block_13282072616197f789878ef6_64737661 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                </ul>
            </div>
            <div class="snippets-filter-show-all">
                <?php $_block_plugin28 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin28, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('variant'=>"link",'role'=>"button",'data'=>array("toggle"=>"collapse","target"=>"#box-collps-filter-attribute-".((string)$_smarty_tpl->tpl_vars['Merkmal']->value->getValue()))));
$_block_repeat=true;
echo $_block_plugin28->render(array('variant'=>"link",'role'=>"button",'data'=>array("toggle"=>"collapse","target"=>"#box-collps-filter-attribute-".((string)$_smarty_tpl->tpl_vars['Merkmal']->value->getValue()))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'showAll'),$_smarty_tpl ) );?>

                <?php $_block_repeat=false;
echo $_block_plugin28->render(array('variant'=>"link",'role'=>"button",'data'=>array("toggle"=>"collapse","target"=>"#box-collps-filter-attribute-".((string)$_smarty_tpl->tpl_vars['Merkmal']->value->getValue()))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            </div>
        <?php
}
}
/* {/block 'snippets-filter-characteristics-more-bottom'} */
/* {block 'snippets-filter-characteristic'} */
class Block_16766633566197f7898450c9_01630293 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-filter-characteristic' => 
  array (
    0 => 'Block_16766633566197f7898450c9_01630293',
  ),
  'snippets-filter-characteristic-include-search-in-items' => 
  array (
    0 => 'Block_6002531416197f789848840_03830859',
  ),
  'snippets-filter-characteristics-dropdown' => 
  array (
    0 => 'Block_6385102246197f78984f360_53701334',
  ),
  'snippets-filter-characteristics-more-top' => 
  array (
    0 => 'Block_14081818906197f78985a089_06062266',
  ),
  'snippets-filter-characteristics-nav' => 
  array (
    0 => 'Block_12058077376197f78985c293_93585666',
  ),
  'snippets-filter-characteristics-nav-text' => 
  array (
    0 => 'Block_11842066566197f78985d105_86148247',
  ),
  'snippets-filter-characteristics-nav-image' => 
  array (
    0 => 'Block_18018930966197f789864bd3_28886789',
  ),
  'snippets-filter-characteristics-nav-else' => 
  array (
    0 => 'Block_4421183006197f78986dad8_43118718',
  ),
  'snippets-filter-characteristics-more-bottom' => 
  array (
    0 => 'Block_13282072616197f789878ef6_64737661',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_assignInScope('is_dropdown', ($_smarty_tpl->tpl_vars['Merkmal']->value->cTyp === 'SELECTBOX'));?>
    <?php $_smarty_tpl->_assignInScope('limit', $_smarty_tpl->tpl_vars['Einstellungen']->value['template']['productlist']['filter_max_options']);?>
    <?php $_smarty_tpl->_assignInScope('collapseInit', false);?>
    <?php $_smarty_tpl->_assignInScope('showFilterCount', $_smarty_tpl->tpl_vars['Einstellungen']->value['navigationsfilter']['merkmalfilter_trefferanzahl_anzeigen'] !== 'N' && !($_smarty_tpl->tpl_vars['Einstellungen']->value['navigationsfilter']['merkmalfilter_trefferanzahl_anzeigen'] === 'E' && $_smarty_tpl->tpl_vars['Merkmal']->value->getData('isMultiSelect')));?>
    <div class="filter-search-wrapper">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6002531416197f789848840_03830859', 'snippets-filter-characteristic-include-search-in-items', $this->tplIndex);
?>

    <?php if ($_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') === 'BILD') {?>
        <ul class="nav nav-filter-has-image">
    <?php }?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Merkmal']->value->getOptions(), 'attributeValue');
$_smarty_tpl->tpl_vars['attributeValue']->iteration = 0;
$_smarty_tpl->tpl_vars['attributeValue']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['attributeValue']->value) {
$_smarty_tpl->tpl_vars['attributeValue']->do_else = false;
$_smarty_tpl->tpl_vars['attributeValue']->iteration++;
$__foreach_attributeValue_4_saved = $_smarty_tpl->tpl_vars['attributeValue'];
?>
        <?php $_smarty_tpl->_assignInScope('attributeImageURL', null);?>
        <?php if (($_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') === 'BILD' || $_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') === 'BILD-TEXT')) {?>
            <?php $_smarty_tpl->_assignInScope('attributeImageURL', $_smarty_tpl->tpl_vars['attributeValue']->value->getImage(\JTL\Media\Image::SIZE_XS));?>
            <?php if (strpos($_smarty_tpl->tpl_vars['attributeImageURL']->value,(defined('BILD_KEIN_ARTIKELBILD_VORHANDEN') ? constant('BILD_KEIN_ARTIKELBILD_VORHANDEN') : null)) !== false || strpos($_smarty_tpl->tpl_vars['attributeImageURL']->value,(defined('BILD_KEIN_MERKMALWERTBILD_VORHANDEN') ? constant('BILD_KEIN_MERKMALWERTBILD_VORHANDEN') : null)) !== false) {?>
                <?php $_smarty_tpl->_assignInScope('attributeImageURL', null);?>
            <?php }?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['is_dropdown']->value) {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6385102246197f78984f360_53701334', 'snippets-filter-characteristics-dropdown', $this->tplIndex);
?>

        <?php } else { ?>
            <?php if ($_smarty_tpl->tpl_vars['limit']->value != -1 && $_smarty_tpl->tpl_vars['attributeValue']->iteration > $_smarty_tpl->tpl_vars['limit']->value && !$_smarty_tpl->tpl_vars['collapseInit']->value) {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14081818906197f78985a089_06062266', 'snippets-filter-characteristics-more-top', $this->tplIndex);
?>

            <?php }?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12058077376197f78985c293_93585666', 'snippets-filter-characteristics-nav', $this->tplIndex);
?>

        <?php }?>
    <?php
$_smarty_tpl->tpl_vars['attributeValue'] = $__foreach_attributeValue_4_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    <?php if (!$_smarty_tpl->tpl_vars['is_dropdown']->value && $_smarty_tpl->tpl_vars['limit']->value != -1 && count($_smarty_tpl->tpl_vars['Merkmal']->value->getOptions()) > $_smarty_tpl->tpl_vars['limit']->value) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13282072616197f789878ef6_64737661', 'snippets-filter-characteristics-more-bottom', $this->tplIndex);
?>

    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['Merkmal']->value->getData('cTyp') === 'BILD') {?>
        </ul>
    <?php }?>
    </div>
<?php
}
}
/* {/block 'snippets-filter-characteristic'} */
}
