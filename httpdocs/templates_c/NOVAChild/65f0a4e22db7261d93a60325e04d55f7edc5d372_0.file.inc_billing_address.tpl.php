<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:20:15
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_billing_address.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196611fce1362_24994840',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '65f0a4e22db7261d93a60325e04d55f7edc5d372' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_billing_address.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6196611fce1362_24994840 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19200535446196611fcd1a40_12316303', 'checkout-inc-billing-address');
?>

<?php }
/* {block 'checkout-inc-billing-address'} */
class Block_19200535446196611fcd1a40_12316303 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-inc-billing-address' => 
  array (
    0 => 'Block_19200535446196611fcd1a40_12316303',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <ul class="list-unstyled inc-billing-address">
        <?php if ((isset($_smarty_tpl->tpl_vars['orderDetail']->value))) {?>
            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cFirma) {?><li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFirma;?>
</li><?php }?>
            <li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTitel;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
</li>
            <li>
                <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>
 <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz;
}?>,
                <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>
,
                <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;?>

            </li>
        <?php } else { ?>
            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cFirma) {?><li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFirma;?>
</li><?php }?>
            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cZusatz) {?><li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cZusatz;?>
</li><?php }?>
            <li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTitel;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
</li>
            <li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>
</li>
            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz) {?><li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz;?>
</li><?php }?>
            <li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>
</li>
            <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cBundesland) {?><li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cBundesland;?>
</li><?php }?>
            <li><?php if ($_smarty_tpl->tpl_vars['Kunde']->value->angezeigtesLand) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->angezeigtesLand;
} else {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;
}?></li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cUSTID) {?><li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'ustid','section'=>'account data'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cUSTID;?>
</li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cTel) {?><li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'tel','section'=>'account data'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTel;?>
</li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cFax) {?><li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'fax','section'=>'account data'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFax;?>
</li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cMobil) {?><li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'mobile','section'=>'account data'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMobil;?>
</li><?php }?>
        <li><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMail;?>
</li>
    </ul>
<?php
}
}
/* {/block 'checkout-inc-billing-address'} */
}
