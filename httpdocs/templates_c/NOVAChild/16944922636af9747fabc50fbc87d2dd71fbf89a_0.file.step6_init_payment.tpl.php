<?php
/* Smarty version 3.1.39, created on 2021-11-18 20:57:42
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/step6_init_payment.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196b036e1e206_41952121',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '16944922636af9747fabc50fbc87d2dd71fbf89a' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/step6_init_payment.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout/header.tpl' => 1,
    'file:checkout/inc_order_items.tpl' => 1,
    'file:checkout/inc_paymentmodules.tpl' => 1,
    'file:layout/footer.tpl' => 1,
  ),
),false)) {
function content_6196b036e1e206_41952121 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16041349646196b036e14549_53361989', 'checkout-step6-init-payment');
?>

<?php }
/* {block 'checkout-step6-init-payment-include-header'} */
class Block_11103987886196b036e15813_91965672 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'checkout-step6-init-payment-include-header'} */
/* {block 'checkout-step6-init-payment-heading'} */
class Block_7099374516196b036e17488_66348500 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php if ($_SESSION['Zahlungsart']->nWaehrendBestellung == 1) {?>
                    <h1><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderCompletedPre','section'=>'checkout'),$_smarty_tpl ) );?>
</h1>
                <?php } else { ?>
                    <h1><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderCompletedPost','section'=>'checkout'),$_smarty_tpl ) );?>
</h1>
                <?php }?>
            <?php
}
}
/* {/block 'checkout-step6-init-payment-heading'} */
/* {block 'checkout-step6-init-payment-include-inc-order-items'} */
class Block_17570125236196b036e1bd63_57810908 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_order_items.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tplscope'=>'init-payment'), 0, false);
?>
                <?php
}
}
/* {/block 'checkout-step6-init-payment-include-inc-order-items'} */
/* {block 'checkout-step6-init-payment-include-inc-paymentmodules'} */
class Block_11172582666196b036e1ca04_09866964 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_paymentmodules.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                <?php
}
}
/* {/block 'checkout-step6-init-payment-include-inc-paymentmodules'} */
/* {block 'checkout-step6-init-payment-content'} */
class Block_5321170196196b036e170f9_18774444 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div id="content">
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7099374516196b036e17488_66348500', 'checkout-step6-init-payment-heading', $this->tplIndex);
?>

            <div class="order_process">
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17570125236196b036e1bd63_57810908', 'checkout-step6-init-payment-include-inc-order-items', $this->tplIndex);
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11172582666196b036e1ca04_09866964', 'checkout-step6-init-payment-include-inc-paymentmodules', $this->tplIndex);
?>

            </div>
        </div>
    <?php
}
}
/* {/block 'checkout-step6-init-payment-content'} */
/* {block 'checkout-step6-init-payment-include-footer'} */
class Block_12218381176196b036e1d650_37213254 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'checkout-step6-init-payment-include-footer'} */
/* {block 'checkout-step6-init-payment'} */
class Block_16041349646196b036e14549_53361989 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-step6-init-payment' => 
  array (
    0 => 'Block_16041349646196b036e14549_53361989',
  ),
  'checkout-step6-init-payment-include-header' => 
  array (
    0 => 'Block_11103987886196b036e15813_91965672',
  ),
  'checkout-step6-init-payment-content' => 
  array (
    0 => 'Block_5321170196196b036e170f9_18774444',
  ),
  'checkout-step6-init-payment-heading' => 
  array (
    0 => 'Block_7099374516196b036e17488_66348500',
  ),
  'checkout-step6-init-payment-include-inc-order-items' => 
  array (
    0 => 'Block_17570125236196b036e1bd63_57810908',
  ),
  'checkout-step6-init-payment-include-inc-paymentmodules' => 
  array (
    0 => 'Block_11172582666196b036e1ca04_09866964',
  ),
  'checkout-step6-init-payment-include-footer' => 
  array (
    0 => 'Block_12218381176196b036e1d650_37213254',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11103987886196b036e15813_91965672', 'checkout-step6-init-payment-include-header', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5321170196196b036e170f9_18774444', 'checkout-step6-init-payment-content', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12218381176196b036e1d650_37213254', 'checkout-step6-init-payment-include-footer', $this->tplIndex);
?>

<?php
}
}
/* {/block 'checkout-step6-init-payment'} */
}
