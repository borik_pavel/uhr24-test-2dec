<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:46:23
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/register/form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd48f8d5b18_93469888',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '90db7c689a02cf30fd119a959f3cc7329d2a894e' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/register/form.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:register/form/customer_account.tpl' => 1,
    'file:checkout/inc_shipping_address.tpl' => 1,
  ),
),false)) {
function content_619cd48f8d5b18_93469888 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1391599071619cd48f8c8657_80361839', 'register-form');
?>

<?php }
/* {block 'register-form-include-customer-account'} */
class Block_1697653346619cd48f8c9fa4_82871415 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_smarty_tpl->_subTemplateRender('file:register/form/customer_account.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php
}
}
/* {/block 'register-form-include-customer-account'} */
/* {block 'register-form-hr'} */
class Block_372804522619cd48f8caa53_86208609 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <hr>
            <?php
}
}
/* {/block 'register-form-hr'} */
/* {block 'register-form-include-inc-shipping-address'} */
class Block_1810895457619cd48f8cb9d1_44227190 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_shipping_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                <?php
}
}
/* {/block 'register-form-include-inc-shipping-address'} */
/* {block 'register-form-submit-privacy'} */
class Block_450780166619cd48f8cdff2_77842905 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL(),'class'=>"popup"));
$_block_repeat=true;
echo $_block_plugin13->render(array('href'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL(),'class'=>"popup"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'privacyNotice'),$_smarty_tpl ) );?>

                            <?php $_block_repeat=false;
echo $_block_plugin13->render(array('href'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL(),'class'=>"popup"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'register-form-submit-privacy'} */
/* {block 'register-form-submit-button'} */
class Block_994877256619cd48f8d3433_31870614 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <?php $_block_plugin15 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin15, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('type'=>"submit",'value'=>"1",'variant'=>"primary",'class'=>"submit_once",'block'=>true));
$_block_repeat=true;
echo $_block_plugin15->render(array('type'=>"submit",'value'=>"1",'variant'=>"primary",'class'=>"submit_once",'block'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'sendCustomerData','section'=>'account data'),$_smarty_tpl ) );?>

                            <?php $_block_repeat=false;
echo $_block_plugin15->render(array('type'=>"submit",'value'=>"1",'variant'=>"primary",'class'=>"submit_once",'block'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php
}
}
/* {/block 'register-form-submit-button'} */
/* {block 'register-form-submit'} */
class Block_1246805057619cd48f8cc5f5_46496223 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"checkout-button-row"));
$_block_repeat=true;
echo $_block_plugin11->render(array('class'=>"checkout-button-row"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]))) {?>
                    <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'class'=>"checkout-register-form-buttons-privacy"));
$_block_repeat=true;
echo $_block_plugin12->render(array('cols'=>12,'class'=>"checkout-register-form-buttons-privacy"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_450780166619cd48f8cdff2_77842905', 'register-form-submit-privacy', $this->tplIndex);
?>

                    <?php $_block_repeat=false;
echo $_block_plugin12->render(array('cols'=>12,'class'=>"checkout-register-form-buttons-privacy"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php }?>
                    <?php $_block_plugin14 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin14, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>4,'xl'=>3,'class'=>"checkout-button-row-submit"));
$_block_repeat=true;
echo $_block_plugin14->render(array('cols'=>12,'md'=>4,'xl'=>3,'class'=>"checkout-button-row-submit"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"checkout",'value'=>(($tmp = $_smarty_tpl->tpl_vars['checkout']->value ?? null)===null||$tmp==='' ? '' : $tmp)),$_smarty_tpl ) );?>

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"form",'value'=>"1"),$_smarty_tpl ) );?>

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"editRechnungsadresse",'value'=>$_smarty_tpl->tpl_vars['editRechnungsadresse']->value),$_smarty_tpl ) );?>

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['opcMountPoint'][0], array( array('id'=>'opc_before_submit'),$_smarty_tpl ) );?>

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_994877256619cd48f8d3433_31870614', 'register-form-submit-button', $this->tplIndex);
?>

                    <?php $_block_repeat=false;
echo $_block_plugin14->render(array('cols'=>12,'md'=>4,'xl'=>3,'class'=>"checkout-button-row-submit"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin11->render(array('class'=>"checkout-button-row"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'register-form-submit'} */
/* {block 'register-form-content'} */
class Block_1707232252619cd48f8c9c24_19197583 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1697653346619cd48f8c9fa4_82871415', 'register-form-include-customer-account', $this->tplIndex);
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_372804522619cd48f8caa53_86208609', 'register-form-hr', $this->tplIndex);
?>

            <?php if ((isset($_smarty_tpl->tpl_vars['checkout']->value)) && $_smarty_tpl->tpl_vars['checkout']->value === 1) {?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1810895457619cd48f8cb9d1_44227190', 'register-form-include-inc-shipping-address', $this->tplIndex);
?>

            <?php }?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1246805057619cd48f8cc5f5_46496223', 'register-form-submit', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'register-form-content'} */
/* {block 'register-form'} */
class Block_1391599071619cd48f8c8657_80361839 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'register-form' => 
  array (
    0 => 'Block_1391599071619cd48f8c8657_80361839',
  ),
  'register-form-content' => 
  array (
    0 => 'Block_1707232252619cd48f8c9c24_19197583',
  ),
  'register-form-include-customer-account' => 
  array (
    0 => 'Block_1697653346619cd48f8c9fa4_82871415',
  ),
  'register-form-hr' => 
  array (
    0 => 'Block_372804522619cd48f8caa53_86208609',
  ),
  'register-form-include-inc-shipping-address' => 
  array (
    0 => 'Block_1810895457619cd48f8cb9d1_44227190',
  ),
  'register-form-submit' => 
  array (
    0 => 'Block_1246805057619cd48f8cc5f5_46496223',
  ),
  'register-form-submit-privacy' => 
  array (
    0 => 'Block_450780166619cd48f8cdff2_77842905',
  ),
  'register-form-submit-button' => 
  array (
    0 => 'Block_994877256619cd48f8d3433_31870614',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'registrieren.php'),$_smarty_tpl ) );
$_prefixVariable5=ob_get_clean();
$_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('action'=>$_prefixVariable5,'class'=>"jtl-validate register-form clearfix",'slide'=>true));
$_block_repeat=true;
echo $_block_plugin10->render(array('action'=>$_prefixVariable5,'class'=>"jtl-validate register-form clearfix",'slide'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1707232252619cd48f8c9c24_19197583', 'register-form-content', $this->tplIndex);
?>

    <?php $_block_repeat=false;
echo $_block_plugin10->render(array('action'=>$_prefixVariable5,'class'=>"jtl-validate register-form clearfix",'slide'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
/* {/block 'register-form'} */
}
