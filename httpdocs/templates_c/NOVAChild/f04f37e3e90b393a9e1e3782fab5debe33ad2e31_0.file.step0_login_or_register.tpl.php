<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:32
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/step0_login_or_register.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f4e76a48_03263236',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f04f37e3e90b393a9e1e3782fab5debe33ad2e31' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/step0_login_or_register.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:register/form/customer_login.tpl' => 1,
    'file:register/form/customer_account.tpl' => 1,
    'file:checkout/inc_shipping_address.tpl' => 1,
  ),
),false)) {
function content_619660f4e76a48_03263236 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1776646928619660f4e5e404_94234655', 'checkout-step0-login-or-register');
?>

<?php }
/* {block 'checkout-step0-login-or-register-alert'} */
class Block_1901219976619660f4e5e970_54439837 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (!empty($_smarty_tpl->tpl_vars['fehlendeAngaben']->value) && !$_smarty_tpl->tpl_vars['alertNote']->value) {?>
            <?php $_block_plugin24 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin24, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"danger"));
$_block_repeat=true;
echo $_block_plugin24->render(array('variant'=>"danger"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'mandatoryFieldNotification','section'=>'errorMessages'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin24->render(array('variant'=>"danger"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['fehlendeAngaben']->value['email_vorhanden'])) && $_smarty_tpl->tpl_vars['fehlendeAngaben']->value['email_vorhanden'] == 1) {?>
            <?php $_block_plugin25 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin25, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"danger"));
$_block_repeat=true;
echo $_block_plugin25->render(array('variant'=>"danger"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'emailAlreadyExists','section'=>'account data'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin25->render(array('variant'=>"danger"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php }?>
        <?php if ((isset($_smarty_tpl->tpl_vars['fehlendeAngaben']->value['formular_zeit'])) && $_smarty_tpl->tpl_vars['fehlendeAngaben']->value['formular_zeit'] == 1) {?>
            <?php $_block_plugin26 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin26, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"danger"));
$_block_repeat=true;
echo $_block_plugin26->render(array('variant'=>"danger"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'formToFast','section'=>'account data'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin26->render(array('variant'=>"danger"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php }?>
    <?php
}
}
/* {/block 'checkout-step0-login-or-register-alert'} */
/* {block 'checkout-step0-login-or-register-headline-form-login-content'} */
class Block_1725441567619660f4e68518_51650250 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="card-title h3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'alreadyCustomer'),$_smarty_tpl ) );?>
</div>
                                <?php
}
}
/* {/block 'checkout-step0-login-or-register-headline-form-login-content'} */
/* {block 'checkout-step0-login-or-register-include-customer-login'} */
class Block_426503732619660f4e69081_74822403 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:register/form/customer_login.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                                <?php
}
}
/* {/block 'checkout-step0-login-or-register-include-customer-login'} */
/* {block 'checkout-step0-login-or-register-fieldset-form-login-content'} */
class Block_18161353619660f4e68145_66032989 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                            <fieldset>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1725441567619660f4e68518_51650250', 'checkout-step0-login-or-register-headline-form-login-content', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_426503732619660f4e69081_74822403', 'checkout-step0-login-or-register-include-customer-login', $this->tplIndex);
?>

                            </fieldset>
                        <?php
}
}
/* {/block 'checkout-step0-login-or-register-fieldset-form-login-content'} */
/* {block 'checkout-step0-login-or-register-form-login'} */
class Block_644829094619660f4e666b9_62670060 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_block_plugin29 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin29, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('class'=>'card-gray'));
$_block_repeat=true;
echo $_block_plugin29->render(array('class'=>'card-gray'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable14=ob_get_clean();
$_block_plugin30 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin30, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('method'=>"post",'action'=>$_prefixVariable14,'class'=>"jtl-validate",'id'=>"order_register_or_login",'slide'=>true));
$_block_repeat=true;
echo $_block_plugin30->render(array('method'=>"post",'action'=>$_prefixVariable14,'class'=>"jtl-validate",'id'=>"order_register_or_login",'slide'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18161353619660f4e68145_66032989', 'checkout-step0-login-or-register-fieldset-form-login-content', $this->tplIndex);
?>

                    <?php $_block_repeat=false;
echo $_block_plugin30->render(array('method'=>"post",'action'=>$_prefixVariable14,'class'=>"jtl-validate",'id'=>"order_register_or_login",'slide'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin29->render(array('class'=>'card-gray'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'checkout-step0-login-or-register-form-login'} */
/* {block 'checkout-step0-login-or-hr'} */
class Block_167606528619660f4e6a610_24817085 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div class="checkout-existing-customer-hr">
                    <div class="hr-sect"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'or'),$_smarty_tpl ) );?>
</div>
                </div>
            <?php
}
}
/* {/block 'checkout-step0-login-or-hr'} */
/* {block 'checkout-step0-login-or-register-include-customer-account'} */
class Block_734979212619660f4e6cdf5_64996547 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:register/form/customer_account.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('checkout'=>1,'step'=>"formular"), 0, false);
?>
                        <hr>
                    <?php
}
}
/* {/block 'checkout-step0-login-or-register-include-customer-account'} */
/* {block 'checkout-step0-login-or-register-include-inc-shipping-address'} */
class Block_267865079619660f4e6dbe8_42077339 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_shipping_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php
}
}
/* {/block 'checkout-step0-login-or-register-include-inc-shipping-address'} */
/* {block 'checkout-step0-login-or-register-modal-privacy'} */
class Block_2071255739619660f4e6fcd4_88656669 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin34 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin34, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'class'=>"checkout-register-form-buttons-privacy"));
$_block_repeat=true;
echo $_block_plugin34->render(array('cols'=>12,'class'=>"checkout-register-form-buttons-privacy"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin35 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin35, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL(),'class'=>"popup"));
$_block_repeat=true;
echo $_block_plugin35->render(array('href'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL(),'class'=>"popup"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'privacyNotice'),$_smarty_tpl ) );?>

                                    <?php $_block_repeat=false;
echo $_block_plugin35->render(array('href'=>$_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]->getURL(),'class'=>"popup"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin34->render(array('cols'=>12,'class'=>"checkout-register-form-buttons-privacy"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'checkout-step0-login-or-register-modal-privacy'} */
/* {block 'checkout-step0-login-or-register-submit-button'} */
class Block_1060142642619660f4e72048_59893701 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php $_block_plugin36 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin36, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>5,'xl'=>4,'class'=>"checkout-button-row-submit"));
$_block_repeat=true;
echo $_block_plugin36->render(array('cols'=>12,'md'=>5,'xl'=>4,'class'=>"checkout-button-row-submit"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"checkout",'value'=>"1"),$_smarty_tpl ) );?>

                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"form",'value'=>"1"),$_smarty_tpl ) );?>

                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"editRechnungsadresse",'value'=>"0"),$_smarty_tpl ) );?>

                                    <?php $_block_plugin37 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin37, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('type'=>"submit",'variant'=>"primary",'class'=>"submit_once",'block'=>true));
$_block_repeat=true;
echo $_block_plugin37->render(array('type'=>"submit",'variant'=>"primary",'class'=>"submit_once",'block'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'sendCustomerData','section'=>'account data'),$_smarty_tpl ) );?>

                                    <?php $_block_repeat=false;
echo $_block_plugin37->render(array('type'=>"submit",'variant'=>"primary",'class'=>"submit_once",'block'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin36->render(array('cols'=>12,'md'=>5,'xl'=>4,'class'=>"checkout-button-row-submit"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'checkout-step0-login-or-register-submit-button'} */
/* {block 'checkout-step0-login-or-register-form-submit'} */
class Block_754519545619660f4e6e6f5_24915843 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin33 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin33, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"checkout-button-row"));
$_block_repeat=true;
echo $_block_plugin33->render(array('class'=>"checkout-button-row"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['oSpezialseiten_arr']->value[(defined('LINKTYP_DATENSCHUTZ') ? constant('LINKTYP_DATENSCHUTZ') : null)]))) {?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2071255739619660f4e6fcd4_88656669', 'checkout-step0-login-or-register-modal-privacy', $this->tplIndex);
?>

                            <?php }?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1060142642619660f4e72048_59893701', 'checkout-step0-login-or-register-submit-button', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin33->render(array('class'=>"checkout-button-row"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'checkout-step0-login-or-register-form-submit'} */
/* {block 'checkout-step0-login-or-register-form'} */
class Block_1999019907619660f4e6b932_79256876 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );
$_prefixVariable15=ob_get_clean();
$_block_plugin32 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin32, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('method'=>"post",'action'=>$_prefixVariable15,'class'=>"form checkout-register-form jtl-validate",'id'=>"form-register",'slide'=>true));
$_block_repeat=true;
echo $_block_plugin32->render(array('method'=>"post",'action'=>$_prefixVariable15,'class'=>"form checkout-register-form jtl-validate",'id'=>"form-register",'slide'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_734979212619660f4e6cdf5_64996547', 'checkout-step0-login-or-register-include-customer-account', $this->tplIndex);
?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_267865079619660f4e6dbe8_42077339', 'checkout-step0-login-or-register-include-inc-shipping-address', $this->tplIndex);
?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_754519545619660f4e6e6f5_24915843', 'checkout-step0-login-or-register-form-submit', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin32->render(array('method'=>"post",'action'=>$_prefixVariable15,'class'=>"form checkout-register-form jtl-validate",'id'=>"form-register",'slide'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'checkout-step0-login-or-register-form'} */
/* {block 'checkout-step0-login-or-register'} */
class Block_1776646928619660f4e5e404_94234655 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-step0-login-or-register' => 
  array (
    0 => 'Block_1776646928619660f4e5e404_94234655',
  ),
  'checkout-step0-login-or-register-alert' => 
  array (
    0 => 'Block_1901219976619660f4e5e970_54439837',
  ),
  'checkout-step0-login-or-register-form-login' => 
  array (
    0 => 'Block_644829094619660f4e666b9_62670060',
  ),
  'checkout-step0-login-or-register-fieldset-form-login-content' => 
  array (
    0 => 'Block_18161353619660f4e68145_66032989',
  ),
  'checkout-step0-login-or-register-headline-form-login-content' => 
  array (
    0 => 'Block_1725441567619660f4e68518_51650250',
  ),
  'checkout-step0-login-or-register-include-customer-login' => 
  array (
    0 => 'Block_426503732619660f4e69081_74822403',
  ),
  'checkout-step0-login-or-hr' => 
  array (
    0 => 'Block_167606528619660f4e6a610_24817085',
  ),
  'checkout-step0-login-or-register-form' => 
  array (
    0 => 'Block_1999019907619660f4e6b932_79256876',
  ),
  'checkout-step0-login-or-register-include-customer-account' => 
  array (
    0 => 'Block_734979212619660f4e6cdf5_64996547',
  ),
  'checkout-step0-login-or-register-include-inc-shipping-address' => 
  array (
    0 => 'Block_267865079619660f4e6dbe8_42077339',
  ),
  'checkout-step0-login-or-register-form-submit' => 
  array (
    0 => 'Block_754519545619660f4e6e6f5_24915843',
  ),
  'checkout-step0-login-or-register-modal-privacy' => 
  array (
    0 => 'Block_2071255739619660f4e6fcd4_88656669',
  ),
  'checkout-step0-login-or-register-submit-button' => 
  array (
    0 => 'Block_1060142642619660f4e72048_59893701',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1901219976619660f4e5e970_54439837', 'checkout-step0-login-or-register-alert', $this->tplIndex);
?>

    <?php $_block_plugin27 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin27, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('id'=>"register-customer"));
$_block_repeat=true;
echo $_block_plugin27->render(array('id'=>"register-customer"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php $_block_plugin28 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin28, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'id'=>"existing-customer",'lg'=>4,'class'=>"checkout-existing-customer"));
$_block_repeat=true;
echo $_block_plugin28->render(array('cols'=>12,'id'=>"existing-customer",'lg'=>4,'class'=>"checkout-existing-customer"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_644829094619660f4e666b9_62670060', 'checkout-step0-login-or-register-form-login', $this->tplIndex);
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_167606528619660f4e6a610_24817085', 'checkout-step0-login-or-hr', $this->tplIndex);
?>

        <?php $_block_repeat=false;
echo $_block_plugin28->render(array('cols'=>12,'id'=>"existing-customer",'lg'=>4,'class'=>"checkout-existing-customer"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_plugin31 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin31, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'id'=>"customer",'lg'=>8));
$_block_repeat=true;
echo $_block_plugin31->render(array('cols'=>12,'id'=>"customer",'lg'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1999019907619660f4e6b932_79256876', 'checkout-step0-login-or-register-form', $this->tplIndex);
?>

        <?php $_block_repeat=false;
echo $_block_plugin31->render(array('cols'=>12,'id'=>"customer",'lg'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php $_block_repeat=false;
echo $_block_plugin27->render(array('id'=>"register-customer"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
/* {/block 'checkout-step0-login-or-register'} */
}
