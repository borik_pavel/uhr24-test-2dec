<?php
/* Smarty version 3.1.39, created on 2021-11-18 20:57:43
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/ws5_mollie/paymentmethod/template/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6196b0371407c5_24780319',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b4947d40820a5d35578b946b31d757b975c0dedb' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/ws5_mollie/paymentmethod/template/index.tpl',
      1 => 1632584552,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6196b0371407c5_24780319 (Smarty_Internal_Template $_smarty_tpl) {
if ((isset($_smarty_tpl->tpl_vars['redirect']->value)) && $_smarty_tpl->tpl_vars['redirect']->value != '') {?>
  <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin1->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
    <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4,'lg'=>3,'xl'=>2));
$_block_repeat=true;
echo $_block_plugin2->render(array('md'=>4,'lg'=>3,'xl'=>2), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
      <?php ob_start();
echo $_smarty_tpl->tpl_vars['redirect']->value;
$_prefixVariable1 = ob_get_clean();
$_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('block'=>true,'type'=>"link",'href'=>$_prefixVariable1,'variant'=>"primary"));
$_block_repeat=true;
echo $_block_plugin3->render(array('block'=>true,'type'=>"link",'href'=>$_prefixVariable1,'variant'=>"primary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'payNow','section'=>'global'),$_smarty_tpl ) );?>

      <?php $_block_repeat=false;
echo $_block_plugin3->render(array('block'=>true,'type'=>"link",'href'=>$_prefixVariable1,'variant'=>"primary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php $_block_repeat=false;
echo $_block_plugin2->render(array('md'=>4,'lg'=>3,'xl'=>2), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
  <?php $_block_repeat=false;
echo $_block_plugin1->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
  <?php if ($_smarty_tpl->tpl_vars['checkoutMode']->value == 'D') {?>
    <meta http-equiv="refresh" content="<?php echo (defined('MOLLIE_REDIRECT_DELAY') ? constant('MOLLIE_REDIRECT_DELAY') : null);?>
; URL=<?php echo $_smarty_tpl->tpl_vars['redirect']->value;?>
">
  <?php }
}?>

<?php }
}
