<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:45:53
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/orders.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619dedb18157f3_24781326',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4eacc89e7c736a0b07cb2e37297a47dd89d445f3' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/orders.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:snippets/pagination.tpl' => 1,
  ),
),false)) {
function content_619dedb18157f3_24781326 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_348466932619dedb17e7074_91645664', 'account-orders');
?>

<?php }
/* {block 'heading'} */
class Block_1661509579619dedb17e8ae6_71981726 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div class="h1"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourOrders','section'=>'login'),$_smarty_tpl ) );?>
</div>
    <?php
}
}
/* {/block 'heading'} */
/* {block 'account-orders-orders'} */
class Block_832474470619dedb17edcc6_21348728 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderPagination']->value->getPageItems(), 'order');
$_smarty_tpl->tpl_vars['order']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->do_else = false;
?>
                    <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'class'=>'account-orders-item'));
$_block_repeat=true;
echo $_block_plugin1->render(array('no-body'=>true,'class'=>'account-orders-item'), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin2->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php'),$_smarty_tpl ) );
$_prefixVariable1=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'showOrder','section'=>'login'),$_smarty_tpl ) );
$_prefixVariable2=ob_get_clean();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderNo','section'=>'login'),$_smarty_tpl ) );
$_prefixVariable3=ob_get_clean();
$_block_plugin3 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin3, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('href'=>$_prefixVariable1."?bestellung=".((string)$_smarty_tpl->tpl_vars['order']->value->kBestellung),'title'=>$_prefixVariable2.": ".$_prefixVariable3." ".((string)$_smarty_tpl->tpl_vars['order']->value->cBestellNr),'data'=>array("toggle"=>"tooltip","placement"=>"bottom")));
$_block_repeat=true;
echo $_block_plugin3->render(array('href'=>$_prefixVariable1."?bestellung=".((string)$_smarty_tpl->tpl_vars['order']->value->kBestellung),'title'=>$_prefixVariable2.": ".$_prefixVariable3." ".((string)$_smarty_tpl->tpl_vars['order']->value->cBestellNr),'data'=>array("toggle"=>"tooltip","placement"=>"bottom")), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php $_block_plugin4 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin4, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin4->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin5 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin5, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>6,'md'=>3,'order'=>1));
$_block_repeat=true;
echo $_block_plugin5->render(array('cols'=>6,'md'=>3,'order'=>1), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <strong><i class="far fa-calendar-alt"></i> <?php echo $_smarty_tpl->tpl_vars['order']->value->dBestelldatum;?>
</strong>
                                    <?php $_block_repeat=false;
echo $_block_plugin5->render(array('cols'=>6,'md'=>3,'order'=>1), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin6 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin6, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>6,'md'=>2,'order'=>4,'order-md'=>2));
$_block_repeat=true;
echo $_block_plugin6->render(array('cols'=>6,'md'=>2,'order'=>4,'order-md'=>2), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo $_smarty_tpl->tpl_vars['order']->value->cBestellwertLocalized;?>

                                    <?php $_block_repeat=false;
echo $_block_plugin6->render(array('cols'=>6,'md'=>2,'order'=>4,'order-md'=>2), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin7 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin7, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>4,'md'=>2,'order'=>2,'order-md'=>3));
$_block_repeat=true;
echo $_block_plugin7->render(array('cols'=>4,'md'=>2,'order'=>2,'order-md'=>3), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo $_smarty_tpl->tpl_vars['order']->value->cBestellNr;?>

                                    <?php $_block_repeat=false;
echo $_block_plugin7->render(array('cols'=>4,'md'=>2,'order'=>2,'order-md'=>3), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin8 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin8, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>6,'md'=>4,'order'=>5,'order-md'=>4));
$_block_repeat=true;
echo $_block_plugin8->render(array('cols'=>6,'md'=>4,'order'=>5,'order-md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'orderStatus','section'=>'login'),$_smarty_tpl ) );?>
: <?php echo $_smarty_tpl->tpl_vars['order']->value->Status;?>

                                    <?php $_block_repeat=false;
echo $_block_plugin8->render(array('cols'=>6,'md'=>4,'order'=>5,'order-md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin9 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin9, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>2,'md'=>1,'order'=>3,'order-md'=>5,'class'=>"text-right-util"));
$_block_repeat=true;
echo $_block_plugin9->render(array('cols'=>2,'md'=>1,'order'=>3,'order-md'=>5,'class'=>"text-right-util"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <i class="fa fa-eye"></i>
                                    <?php $_block_repeat=false;
echo $_block_plugin9->render(array('cols'=>2,'md'=>1,'order'=>3,'order-md'=>5,'class'=>"text-right-util"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin4->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_repeat=false;
echo $_block_plugin3->render(array('href'=>$_prefixVariable1."?bestellung=".((string)$_smarty_tpl->tpl_vars['order']->value->kBestellung),'title'=>$_prefixVariable2.": ".$_prefixVariable3." ".((string)$_smarty_tpl->tpl_vars['order']->value->cBestellNr),'data'=>array("toggle"=>"tooltip","placement"=>"bottom")), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin2->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin1->render(array('no-body'=>true,'class'=>'account-orders-item'), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php
}
}
/* {/block 'account-orders-orders'} */
/* {block 'account-orders-include-pagination'} */
class Block_157024119619dedb1804fe4_93498475 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_smarty_tpl->_subTemplateRender('file:snippets/pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('oPagination'=>$_smarty_tpl->tpl_vars['orderPagination']->value,'cThisUrl'=>'jtl.php','cParam_arr'=>array('bestellungen'=>1),'parts'=>array('pagi','label')), 0, false);
?>
            <?php
}
}
/* {/block 'account-orders-include-pagination'} */
/* {block 'account-orders-alert'} */
class Block_1175492086619dedb180f8e4_01989802 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_block_plugin10 = isset($_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['alert'][0][0] : null;
if (!is_callable(array($_block_plugin10, 'render'))) {
throw new SmartyException('block tag \'alert\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('alert', array('variant'=>"info"));
$_block_repeat=true;
echo $_block_plugin10->render(array('variant'=>"info"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'noEntriesAvailable'),$_smarty_tpl ) );
$_block_repeat=false;
echo $_block_plugin10->render(array('variant'=>"info"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'account-orders-alert'} */
/* {block 'account-orders-actions'} */
class Block_1055970230619dedb1811b99_52590450 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin11 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin11, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin11->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php $_block_plugin12 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin12, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>3,'cols'=>12));
$_block_repeat=true;
echo $_block_plugin12->render(array('md'=>3,'cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php'),$_smarty_tpl ) );
$_prefixVariable4=ob_get_clean();
$_block_plugin13 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin13, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"btn btn-outline-primary btn-block",'href'=>$_prefixVariable4));
$_block_repeat=true;
echo $_block_plugin13->render(array('class'=>"btn btn-outline-primary btn-block",'href'=>$_prefixVariable4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'back'),$_smarty_tpl ) );?>

                    <?php $_block_repeat=false;
echo $_block_plugin13->render(array('class'=>"btn btn-outline-primary btn-block",'href'=>$_prefixVariable4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin12->render(array('md'=>3,'cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_repeat=false;
echo $_block_plugin11->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'account-orders-actions'} */
/* {block 'account-orders-content'} */
class Block_903331132619dedb17ea535_29922830 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (count($_smarty_tpl->tpl_vars['Bestellungen']->value) > 0) {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_832474470619dedb17edcc6_21348728', 'account-orders-orders', $this->tplIndex);
?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_157024119619dedb1804fe4_93498475', 'account-orders-include-pagination', $this->tplIndex);
?>

        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1175492086619dedb180f8e4_01989802', 'account-orders-alert', $this->tplIndex);
?>

        <?php }?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1055970230619dedb1811b99_52590450', 'account-orders-actions', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'account-orders-content'} */
/* {block 'account-orders'} */
class Block_348466932619dedb17e7074_91645664 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'account-orders' => 
  array (
    0 => 'Block_348466932619dedb17e7074_91645664',
  ),
  'heading' => 
  array (
    0 => 'Block_1661509579619dedb17e8ae6_71981726',
  ),
  'account-orders-content' => 
  array (
    0 => 'Block_903331132619dedb17ea535_29922830',
  ),
  'account-orders-orders' => 
  array (
    0 => 'Block_832474470619dedb17edcc6_21348728',
  ),
  'account-orders-include-pagination' => 
  array (
    0 => 'Block_157024119619dedb1804fe4_93498475',
  ),
  'account-orders-alert' => 
  array (
    0 => 'Block_1175492086619dedb180f8e4_01989802',
  ),
  'account-orders-actions' => 
  array (
    0 => 'Block_1055970230619dedb1811b99_52590450',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1661509579619dedb17e8ae6_71981726', 'heading', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_903331132619dedb17ea535_29922830', 'account-orders-content', $this->tplIndex);
?>

<?php
}
}
/* {/block 'account-orders'} */
}
