<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:40:39
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/ws5_mollie/frontend/template/applepay.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953e97514214_27576407',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '86ed696f801d25c275c0948fa8676c076c32b609' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/ws5_mollie/frontend/template/applepay.tpl',
      1 => 1632584552,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61953e97514214_27576407 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="application/javascript" defer>
    // <!--
    if (window.jQuery) {
        $(function () {
            const setApplePayStatus = function (status) {

                $.ajax(<?php echo $_smarty_tpl->tpl_vars['applePayCheckURL']->value;?>
, {
                    method: 'POST',
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    data: {
                        available: status
                    }
                });

            }
            setApplePayStatus(window.ApplePaySession && window.ApplePaySession.canMakePayments() ? 1 : 0);
        });
    } else if (window.console) {
        console.warn('jQuery not loaded as yet, ApplePay not available!');
    }
    // -->
<?php echo '</script'; ?>
><?php }
}
