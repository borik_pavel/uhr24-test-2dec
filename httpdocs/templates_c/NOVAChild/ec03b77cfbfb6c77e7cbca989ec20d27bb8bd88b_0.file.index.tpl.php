<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:32
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f4c8df14_56926012',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ec03b77cfbfb6c77e7cbca989ec20d27bb8bd88b' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/index.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:layout/header.tpl' => 1,
    'file:checkout/inc_steps.tpl' => 1,
    'file:snippets/extension.tpl' => 1,
    'file:checkout/step0_login_or_register.tpl' => 1,
    'file:checkout/step1_edit_customer_address.tpl' => 1,
    'file:checkout/step3_shipping_options.tpl' => 1,
    'file:checkout/step4_payment_additional.tpl' => 1,
    'file:checkout/step5_confirmation.tpl' => 1,
    'file:layout/footer.tpl' => 1,
  ),
),false)) {
function content_619660f4c8df14_56926012 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_915299959619660f4c7b693_92947036', 'checkout-index');
?>

<?php }
/* {block 'checkout-index-include-header'} */
class Block_2053709808619660f4c7be38_59494317 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (!(isset($_smarty_tpl->tpl_vars['bAjaxRequest']->value)) || !$_smarty_tpl->tpl_vars['bAjaxRequest']->value) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
    <?php
}
}
/* {/block 'checkout-index-include-header'} */
/* {block 'checkout-index-include-inc-steps'} */
class Block_782064389619660f4c80c89_66388593 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/inc_steps.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                <?php
}
}
/* {/block 'checkout-index-include-inc-steps'} */
/* {block 'checkout-index-include-extension'} */
class Block_1051573308619660f4c81846_19086105 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_smarty_tpl->_subTemplateRender('file:snippets/extension.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                <?php
}
}
/* {/block 'checkout-index-include-extension'} */
/* {block 'checkout-index-script-basket-merge'} */
class Block_863728962619660f4c85ee2_44160219 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php $_block_plugin2 = isset($_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0] : null;
if (!is_callable(array($_block_plugin2, 'inlineScript'))) {
throw new SmartyException('block tag \'inline_script\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inline_script', array());
$_block_repeat=true;
echo $_block_plugin2->inlineScript(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo '<script'; ?>
>
                    $(window).on('load', function() {
                        $(function() {
                            eModal.addLabel('<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yes','section'=>'global'),$_smarty_tpl ) );?>
', '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'no','section'=>'global'),$_smarty_tpl ) );?>
');
                            var options = {
                                message: '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'basket2PersMerge','section'=>'login'),$_smarty_tpl ) );?>
',
                                label: '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yes','section'=>'global'),$_smarty_tpl ) );?>
',
                                title: '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'basket','section'=>'global'),$_smarty_tpl ) );?>
'
                            };
                            eModal.confirm(options).then(
                                function() {
                                    window.location = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'bestellvorgang.php'),$_smarty_tpl ) );?>
?basket2Pers=1&token=<?php echo $_SESSION['jtl_token'];?>
"
                                }
                            );
                        });
                    });
                <?php echo '</script'; ?>
><?php $_block_repeat=false;
echo $_block_plugin2->inlineScript(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
}
}
/* {/block 'checkout-index-script-basket-merge'} */
/* {block 'checkout-index-script-location'} */
class Block_917662753619660f4c8b234_70768687 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo '<script'; ?>
>
                if (top.location !== self.location) {
                    top.location = self.location.href;
                }
            <?php echo '</script'; ?>
>
        <?php
}
}
/* {/block 'checkout-index-script-location'} */
/* {block 'checkout-index-content'} */
class Block_612258861619660f4c7e556_17155314 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


        <div id="result-wrapper" data-wrapper="true">
            <?php $_block_plugin1 = isset($_smarty_tpl->smarty->registered_plugins['block']['container'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['container'][0][0] : null;
if (!is_callable(array($_block_plugin1, 'render'))) {
throw new SmartyException('block tag \'container\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('container', array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid(),'id'=>"checkout"));
$_block_repeat=true;
echo $_block_plugin1->render(array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid(),'id'=>"checkout"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_782064389619660f4c80c89_66388593', 'checkout-index-include-inc-steps', $this->tplIndex);
?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1051573308619660f4c81846_19086105', 'checkout-index-include-extension', $this->tplIndex);
?>

                <?php if ($_smarty_tpl->tpl_vars['step']->value === 'accountwahl') {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/step0_login_or_register.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                <?php } elseif ($_smarty_tpl->tpl_vars['step']->value === 'edit_customer_address' || $_smarty_tpl->tpl_vars['step']->value === 'Lieferadresse') {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/step1_edit_customer_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                <?php } elseif ($_smarty_tpl->tpl_vars['step']->value === 'Versand' || $_smarty_tpl->tpl_vars['step']->value === 'Zahlung') {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/step3_shipping_options.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                <?php } elseif ($_smarty_tpl->tpl_vars['step']->value === 'ZahlungZusatzschritt') {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/step4_payment_additional.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                <?php } elseif ($_smarty_tpl->tpl_vars['step']->value === 'Bestaetigung') {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/step5_confirmation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>                <?php }?>
            <?php $_block_repeat=false;
echo $_block_plugin1->render(array('fluid'=>$_smarty_tpl->tpl_vars['Link']->value->getIsFluid(),'id'=>"checkout"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        </div>

        <?php if (((isset($_smarty_tpl->tpl_vars['nWarenkorb2PersMerge']->value)) && $_smarty_tpl->tpl_vars['nWarenkorb2PersMerge']->value === 1)) {?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_863728962619660f4c85ee2_44160219', 'checkout-index-script-basket-merge', $this->tplIndex);
?>

        <?php }?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_917662753619660f4c8b234_70768687', 'checkout-index-script-location', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'checkout-index-content'} */
/* {block 'checkout-index-include-footer'} */
class Block_1518694818619660f4c8c202_68557913 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (!(isset($_smarty_tpl->tpl_vars['bAjaxRequest']->value)) || !$_smarty_tpl->tpl_vars['bAjaxRequest']->value) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
    <?php
}
}
/* {/block 'checkout-index-include-footer'} */
/* {block 'checkout-index'} */
class Block_915299959619660f4c7b693_92947036 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-index' => 
  array (
    0 => 'Block_915299959619660f4c7b693_92947036',
  ),
  'checkout-index-include-header' => 
  array (
    0 => 'Block_2053709808619660f4c7be38_59494317',
  ),
  'checkout-index-content' => 
  array (
    0 => 'Block_612258861619660f4c7e556_17155314',
  ),
  'checkout-index-include-inc-steps' => 
  array (
    0 => 'Block_782064389619660f4c80c89_66388593',
  ),
  'checkout-index-include-extension' => 
  array (
    0 => 'Block_1051573308619660f4c81846_19086105',
  ),
  'checkout-index-script-basket-merge' => 
  array (
    0 => 'Block_863728962619660f4c85ee2_44160219',
  ),
  'checkout-index-script-location' => 
  array (
    0 => 'Block_917662753619660f4c8b234_70768687',
  ),
  'checkout-index-include-footer' => 
  array (
    0 => 'Block_1518694818619660f4c8c202_68557913',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2053709808619660f4c7be38_59494317', 'checkout-index-include-header', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_612258861619660f4c7e556_17155314', 'checkout-index-content', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1518694818619660f4c8c202_68557913', 'checkout-index-include-footer', $this->tplIndex);
?>

<?php
}
}
/* {/block 'checkout-index'} */
}
