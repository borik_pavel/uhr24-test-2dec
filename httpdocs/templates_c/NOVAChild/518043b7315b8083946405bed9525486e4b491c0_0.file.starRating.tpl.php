<?php
/* Smarty version 3.1.39, created on 2021-11-17 18:23:33
  from '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/shopauskunft/frontend/tpl/starRating.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61953a9517e326_58482008',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '518043b7315b8083946405bed9525486e4b491c0' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/plugins/shopauskunft/frontend/tpl/starRating.tpl',
      1 => 1632584625,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61953a9517e326_58482008 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('ratingArr', array(1,2,3,4,5));
if ($_smarty_tpl->tpl_vars['rating']->value && $_smarty_tpl->tpl_vars['rating']->value > 0) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ratingArr']->value, 'index', false, 'key');
$_smarty_tpl->tpl_vars['index']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['index']->value) {
$_smarty_tpl->tpl_vars['index']->do_else = false;
?>
    <?php if ($_smarty_tpl->tpl_vars['rating']->value >= $_smarty_tpl->tpl_vars['index']->value) {?>
        <span class="starEmpty fa fa-star">
            <span class="fa fa-star starPartiallyFilled" style="width: <?php echo 1*100;?>
%;"></span>
        </span>
    <?php } elseif ($_smarty_tpl->tpl_vars['rating']->value >= ($_smarty_tpl->tpl_vars['index']->value-1)) {?>
        <span class="starEmpty fa fa-star">
            <span class="fa fa-star starPartiallyFilled" style="width: <?php echo 100*($_smarty_tpl->tpl_vars['rating']->value-$_smarty_tpl->tpl_vars['index']->value+1);?>
%;"></span>
        </span>
    <?php } else { ?>
        <span class="starEmpty fa fa-star">
            <span class="fa fa-star starPartiallyFilled" style="width: 0%;"></span>
        </span>
    <?php }
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
} else {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ratingArr']->value, 'fillStand');
$_smarty_tpl->tpl_vars['fillStand']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['fillStand']->value) {
$_smarty_tpl->tpl_vars['fillStand']->do_else = false;
?>
    <span class="starEmpty fa fa-star" style="text-shadow: 0 0 3px">
        <span class="fa fa-star starPartiallyFilled" style="width: 100%; color:#fff;"></span>
    </span>
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
}
