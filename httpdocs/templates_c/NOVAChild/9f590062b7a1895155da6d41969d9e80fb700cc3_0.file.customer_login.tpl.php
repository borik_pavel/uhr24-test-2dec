<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:32
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/register/form/customer_login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f4e9e4a5_32282011',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f590062b7a1895155da6d41969d9e80fb700cc3' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/register/form/customer_login.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619660f4e9e4a5_32282011 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1469139400619660f4e8c274_02819925', 'register-form-customer-login');
?>

<?php }
/* {block 'register-form-customer-login-email'} */
class Block_996637000619660f4e8c7b1_19587231 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'email','section'=>'account data'),$_smarty_tpl ) );
$_prefixVariable16=ob_get_clean();
$_block_plugin38 = isset($_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0] : null;
if (!is_callable(array($_block_plugin38, 'render'))) {
throw new SmartyException('block tag \'formgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formgroup', array('label'=>$_prefixVariable16,'label-for'=>"login_email"));
$_block_repeat=true;
echo $_block_plugin38->render(array('label'=>$_prefixVariable16,'label-for'=>"login_email"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"text",'name'=>"email",'id'=>"login_email",'placeholder'=>" ",'required'=>true,'autocomplete'=>"email",'value'=>''),$_smarty_tpl ) );?>

        <?php $_block_repeat=false;
echo $_block_plugin38->render(array('label'=>$_prefixVariable16,'label-for'=>"login_email"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'register-form-customer-login-email'} */
/* {block 'register-form-customer-login-password'} */
class Block_1200872271619660f4e8f534_24256358 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'password','section'=>'account data'),$_smarty_tpl ) );
$_prefixVariable17=ob_get_clean();
$_block_plugin39 = isset($_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0] : null;
if (!is_callable(array($_block_plugin39, 'render'))) {
throw new SmartyException('block tag \'formgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formgroup', array('label'=>$_prefixVariable17,'label-for'=>"login_password"));
$_block_repeat=true;
echo $_block_plugin39->render(array('label'=>$_prefixVariable17,'label-for'=>"login_password"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"password",'name'=>"passwort",'id'=>"login_password",'placeholder'=>" ",'required'=>true,'autocomplete'=>"current-password",'value'=>''),$_smarty_tpl ) );?>

        <?php $_block_repeat=false;
echo $_block_plugin39->render(array('label'=>$_prefixVariable17,'label-for'=>"login_password"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'register-form-customer-login-password'} */
/* {block 'register-form-customer-login-captcha'} */
class Block_1851186593619660f4e92ea4_54229529 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="form-group simple-captcha-wrapper">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['captchaMarkup'][0], array( array('getBody'=>true),$_smarty_tpl ) );?>

            </div>
        <?php
}
}
/* {/block 'register-form-customer-login-captcha'} */
/* {block 'register-form-customer-submit'} */
class Block_2135811348619660f4e9a6b1_11942619 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php $_block_plugin43 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin43, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('type'=>"submit",'variant'=>"primary",'block'=>true));
$_block_repeat=true;
echo $_block_plugin43->render(array('type'=>"submit",'variant'=>"primary",'block'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'login','section'=>'checkout'),$_smarty_tpl ) );?>

                        <?php $_block_repeat=false;
echo $_block_plugin43->render(array('type'=>"submit",'variant'=>"primary",'block'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'register-form-customer-submit'} */
/* {block 'register-form-customer-forgot'} */
class Block_760215571619660f4e9c235_29652146 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                        <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'pass.php'),$_smarty_tpl ) );
$_prefixVariable19=ob_get_clean();
$_block_plugin45 = isset($_smarty_tpl->smarty->registered_plugins['block']['link'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['link'][0][0] : null;
if (!is_callable(array($_block_plugin45, 'render'))) {
throw new SmartyException('block tag \'link\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('link', array('class'=>"btn btn-link customer-login-buttons-forgot",'href'=>$_prefixVariable19));
$_block_repeat=true;
echo $_block_plugin45->render(array('class'=>"btn btn-link customer-login-buttons-forgot",'href'=>$_prefixVariable19), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'forgotPassword'),$_smarty_tpl ) );?>

                        <?php $_block_repeat=false;
echo $_block_plugin45->render(array('class'=>"btn btn-link customer-login-buttons-forgot",'href'=>$_prefixVariable19), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php
}
}
/* {/block 'register-form-customer-forgot'} */
/* {block 'register-form-customer-login-submit'} */
class Block_1251891975619660f4e93b99_24433610 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_block_plugin40 = isset($_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formgroup'][0][0] : null;
if (!is_callable(array($_block_plugin40, 'render'))) {
throw new SmartyException('block tag \'formgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formgroup', array('label-for'=>" ",'class'=>"customer-login-buttons"));
$_block_repeat=true;
echo $_block_plugin40->render(array('label-for'=>" ",'class'=>"customer-login-buttons"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"login",'value'=>"1"),$_smarty_tpl ) );?>

            <?php ob_start();
if ((isset($_smarty_tpl->tpl_vars['one_step_wk']->value))) {
echo (string)$_smarty_tpl->tpl_vars['one_step_wk']->value;
} else {
echo "0";
}
$_prefixVariable18=ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"wk",'value'=>$_prefixVariable18),$_smarty_tpl ) );?>

            <?php if (!empty($_smarty_tpl->tpl_vars['oRedirect']->value->cURL)) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oRedirect']->value->oParameter_arr, 'oParameter');
$_smarty_tpl->tpl_vars['oParameter']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oParameter']->value) {
$_smarty_tpl->tpl_vars['oParameter']->do_else = false;
?>
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>$_smarty_tpl->tpl_vars['oParameter']->value->Name,'value'=>$_smarty_tpl->tpl_vars['oParameter']->value->Wert),$_smarty_tpl ) );?>

                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"r",'value'=>$_smarty_tpl->tpl_vars['oRedirect']->value->nRedirect),$_smarty_tpl ) );?>

                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"cURL",'value'=>$_smarty_tpl->tpl_vars['oRedirect']->value->cURL),$_smarty_tpl ) );?>

            <?php }?>
            <?php $_block_plugin41 = isset($_smarty_tpl->smarty->registered_plugins['block']['formrow'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['formrow'][0][0] : null;
if (!is_callable(array($_block_plugin41, 'render'))) {
throw new SmartyException('block tag \'formrow\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('formrow', array());
$_block_repeat=true;
echo $_block_plugin41->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php $_block_plugin42 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin42, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6));
$_block_repeat=true;
echo $_block_plugin42->render(array('cols'=>12,'lg'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2135811348619660f4e9a6b1_11942619', 'register-form-customer-submit', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin42->render(array('cols'=>12,'lg'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_plugin44 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin44, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'lg'=>6));
$_block_repeat=true;
echo $_block_plugin44->render(array('cols'=>12,'lg'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_760215571619660f4e9c235_29652146', 'register-form-customer-forgot', $this->tplIndex);
?>

                <?php $_block_repeat=false;
echo $_block_plugin44->render(array('cols'=>12,'lg'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_repeat=false;
echo $_block_plugin41->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_repeat=false;
echo $_block_plugin40->render(array('label-for'=>" ",'class'=>"customer-login-buttons"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php
}
}
/* {/block 'register-form-customer-login-submit'} */
/* {block 'register-form-customer-login'} */
class Block_1469139400619660f4e8c274_02819925 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'register-form-customer-login' => 
  array (
    0 => 'Block_1469139400619660f4e8c274_02819925',
  ),
  'register-form-customer-login-email' => 
  array (
    0 => 'Block_996637000619660f4e8c7b1_19587231',
  ),
  'register-form-customer-login-password' => 
  array (
    0 => 'Block_1200872271619660f4e8f534_24256358',
  ),
  'register-form-customer-login-captcha' => 
  array (
    0 => 'Block_1851186593619660f4e92ea4_54229529',
  ),
  'register-form-customer-login-submit' => 
  array (
    0 => 'Block_1251891975619660f4e93b99_24433610',
  ),
  'register-form-customer-submit' => 
  array (
    0 => 'Block_2135811348619660f4e9a6b1_11942619',
  ),
  'register-form-customer-forgot' => 
  array (
    0 => 'Block_760215571619660f4e9c235_29652146',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_996637000619660f4e8c7b1_19587231', 'register-form-customer-login-email', $this->tplIndex);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1200872271619660f4e8f534_24256358', 'register-form-customer-login-password', $this->tplIndex);
?>

    <?php if ((isset($_smarty_tpl->tpl_vars['showLoginCaptcha']->value)) && $_smarty_tpl->tpl_vars['showLoginCaptcha']->value) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1851186593619660f4e92ea4_54229529', 'register-form-customer-login-captcha', $this->tplIndex);
?>

    <?php }?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1251891975619660f4e93b99_24433610', 'register-form-customer-login-submit', $this->tplIndex);
?>

<?php
}
}
/* {/block 'register-form-customer-login'} */
}
