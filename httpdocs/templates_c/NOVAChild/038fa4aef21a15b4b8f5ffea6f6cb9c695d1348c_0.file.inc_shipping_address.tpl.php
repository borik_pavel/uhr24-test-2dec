<?php
/* Smarty version 3.1.39, created on 2021-11-18 15:19:33
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_shipping_address.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619660f51390a9_36213305',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '038fa4aef21a15b4b8f5ffea6f6cb9c695d1348c' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/checkout/inc_shipping_address.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:checkout/customer_shipping_address.tpl' => 2,
    'file:checkout/customer_shipping_contact.tpl' => 2,
  ),
),false)) {
function content_619660f51390a9_36213305 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1757067103619660f511ad23_55671786', 'checkout-inc-shipping-address');
?>

<?php }
/* {block 'checkout-inc-shipping-address-checkbox-equals'} */
class Block_841395447619660f511daf6_60556660 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <div class="form-group checkbox control-toggle">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('type'=>"hidden",'name'=>"shipping_address",'value'=>"1"),$_smarty_tpl ) );?>

                    <?php ob_start();
if ((isset($_smarty_tpl->tpl_vars['forceDeliveryAddress']->value))) {
echo "d-none";
}
$_prefixVariable84=ob_get_clean();
$_block_plugin118 = isset($_smarty_tpl->smarty->registered_plugins['block']['checkbox'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['checkbox'][0][0] : null;
if (!is_callable(array($_block_plugin118, 'render'))) {
throw new SmartyException('block tag \'checkbox\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('checkbox', array('id'=>"checkout_register_shipping_address",'name'=>"shipping_address",'value'=>"0",'checked'=>!$_smarty_tpl->tpl_vars['showShippingAddress']->value,'data'=>array("toggle"=>"collapse","target"=>"#select_shipping_address"),'class'=>$_prefixVariable84));
$_block_repeat=true;
echo $_block_plugin118->render(array('id'=>"checkout_register_shipping_address",'name'=>"shipping_address",'value'=>"0",'checked'=>!$_smarty_tpl->tpl_vars['showShippingAddress']->value,'data'=>array("toggle"=>"collapse","target"=>"#select_shipping_address"),'class'=>$_prefixVariable84), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'shippingAdressEqualBillingAdress','section'=>'account data'),$_smarty_tpl ) );?>

                    <?php $_block_repeat=false;
echo $_block_plugin118->render(array('id'=>"checkout_register_shipping_address",'name'=>"shipping_address",'value'=>"0",'checked'=>!$_smarty_tpl->tpl_vars['showShippingAddress']->value,'data'=>array("toggle"=>"collapse","target"=>"#select_shipping_address"),'class'=>$_prefixVariable84), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                </div>
            <?php
}
}
/* {/block 'checkout-inc-shipping-address-checkbox-equals'} */
/* {block 'checkout-inc-shipping-address-legend-address'} */
class Block_943062624619660f5124304_52738044 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="h3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'deviatingDeliveryAddress','section'=>'account data'),$_smarty_tpl ) );?>
</div>
                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-legend-address'} */
/* {block 'checkout-inc-shipping-address-address'} */
class Block_700560397619660f51270c8_04299125 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                    <?php $_block_plugin124 = isset($_smarty_tpl->smarty->registered_plugins['block']['listgroupitem'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['listgroupitem'][0][0] : null;
if (!is_callable(array($_block_plugin124, 'render'))) {
throw new SmartyException('block tag \'listgroupitem\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('listgroupitem', array('tag'=>"li"));
$_block_repeat=true;
echo $_block_plugin124->render(array('tag'=>"li"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                        <label class="btn-block no-caret text-wrap" for="delivery<?php echo $_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse;?>
" data-toggle="collapse" data-target="#register_shipping_address.show">
                                                            <?php $_block_plugin125 = isset($_smarty_tpl->smarty->registered_plugins['block']['radio'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['radio'][0][0] : null;
if (!is_callable(array($_block_plugin125, 'render'))) {
throw new SmartyException('block tag \'radio\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('radio', array('name'=>"kLieferadresse",'value'=>$_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse,'id'=>"delivery".((string)$_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse),'checked'=>($_smarty_tpl->tpl_vars['kLieferadresse']->value == $_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse)));
$_block_repeat=true;
echo $_block_plugin125->render(array('name'=>"kLieferadresse",'value'=>$_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse,'id'=>"delivery".((string)$_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse),'checked'=>($_smarty_tpl->tpl_vars['kLieferadresse']->value == $_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                <span class="control-label label-default"><?php if ($_smarty_tpl->tpl_vars['adresse']->value->cFirma) {
echo $_smarty_tpl->tpl_vars['adresse']->value->cFirma;?>
,<?php }?> <?php echo $_smarty_tpl->tpl_vars['adresse']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['adresse']->value->cNachname;?>

                                                                    , <?php echo $_smarty_tpl->tpl_vars['adresse']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['adresse']->value->cHausnummer;?>
, <?php echo $_smarty_tpl->tpl_vars['adresse']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['adresse']->value->cOrt;?>

                                                                    , <?php echo $_smarty_tpl->tpl_vars['adresse']->value->angezeigtesLand;?>
</span>
                                                            <?php $_block_repeat=false;
echo $_block_plugin125->render(array('name'=>"kLieferadresse",'value'=>$_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse,'id'=>"delivery".((string)$_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse),'checked'=>($_smarty_tpl->tpl_vars['kLieferadresse']->value == $_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        </label>
                                                    <?php $_block_repeat=false;
echo $_block_plugin124->render(array('tag'=>"li"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-address'} */
/* {block 'checkout-inc-shipping-address-new-address'} */
class Block_873175389619660f512cb11_38942849 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <?php $_block_plugin126 = isset($_smarty_tpl->smarty->registered_plugins['block']['listgroupitem'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['listgroupitem'][0][0] : null;
if (!is_callable(array($_block_plugin126, 'render'))) {
throw new SmartyException('block tag \'listgroupitem\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('listgroupitem', array('tag'=>"li"));
$_block_repeat=true;
echo $_block_plugin126->render(array('tag'=>"li"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <label class="btn-block" for="delivery_new" data-toggle="collapse" data-target="#register_shipping_address:not(.show)">
                                                    <?php $_block_plugin127 = isset($_smarty_tpl->smarty->registered_plugins['block']['radio'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['radio'][0][0] : null;
if (!is_callable(array($_block_plugin127, 'render'))) {
throw new SmartyException('block tag \'radio\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('radio', array('name'=>"kLieferadresse",'value'=>"-1",'id'=>"delivery_new",'checked'=>($_smarty_tpl->tpl_vars['kLieferadresse']->value == -1),'required'=>true,'aria-required'=>true));
$_block_repeat=true;
echo $_block_plugin127->render(array('name'=>"kLieferadresse",'value'=>"-1",'id'=>"delivery_new",'checked'=>($_smarty_tpl->tpl_vars['kLieferadresse']->value == -1),'required'=>true,'aria-required'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                        <span class="control-label label-default"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'createNewShippingAdress','section'=>'account data'),$_smarty_tpl ) );?>
</span>
                                                    <?php $_block_repeat=false;
echo $_block_plugin127->render(array('name'=>"kLieferadresse",'value'=>"-1",'id'=>"delivery_new",'checked'=>($_smarty_tpl->tpl_vars['kLieferadresse']->value == -1),'required'=>true,'aria-required'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                </label>
                                            <?php $_block_repeat=false;
echo $_block_plugin126->render(array('tag'=>"li"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php
}
}
/* {/block 'checkout-inc-shipping-address-new-address'} */
/* {block 'checkout-inc-shipping-address-fieldset-address'} */
class Block_2100015046619660f5125264_05686171 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <fieldset>
                                        <?php $_block_plugin123 = isset($_smarty_tpl->smarty->registered_plugins['block']['listgroup'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['listgroup'][0][0] : null;
if (!is_callable(array($_block_plugin123, 'render'))) {
throw new SmartyException('block tag \'listgroup\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('listgroup', array('class'=>"form-group exclude-from-label-slide",'tag'=>"ul"));
$_block_repeat=true;
echo $_block_plugin123->render(array('class'=>"form-group exclude-from-label-slide",'tag'=>"ul"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Lieferadressen']->value, 'adresse');
$_smarty_tpl->tpl_vars['adresse']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['adresse']->value) {
$_smarty_tpl->tpl_vars['adresse']->do_else = false;
?>
                                            <?php if ($_smarty_tpl->tpl_vars['adresse']->value->kLieferadresse > 0) {?>
                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_700560397619660f51270c8_04299125', 'checkout-inc-shipping-address-address', $this->tplIndex);
?>

                                            <?php }?>
                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_873175389619660f512cb11_38942849', 'checkout-inc-shipping-address-new-address', $this->tplIndex);
?>

                                        <?php $_block_repeat=false;
echo $_block_plugin123->render(array('class'=>"form-group exclude-from-label-slide",'tag'=>"ul"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    </fieldset>
                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-fieldset-address'} */
/* {block 'checkout-inc-shipping-address-legend-register'} */
class Block_1477450635619660f51304d5_67077552 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <legend><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'createNewShippingAdress','section'=>'account data'),$_smarty_tpl ) );?>
</legend>
                                        <?php
}
}
/* {/block 'checkout-inc-shipping-address-legend-register'} */
/* {block 'checkout-inc-shipping-address-include-customer-shipping-address'} */
class Block_1928721262619660f5131006_02185976 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <?php $_smarty_tpl->_subTemplateRender('file:checkout/customer_shipping_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('prefix'=>"register",'fehlendeAngaben'=>$_smarty_tpl->tpl_vars['fehlendeAngabenShipping']->value), 0, false);
?>
                                        <?php
}
}
/* {/block 'checkout-inc-shipping-address-include-customer-shipping-address'} */
/* {block 'checkout-inc-shipping-address-include-customer-shipping-contact'} */
class Block_973960247619660f5131d60_83700703 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                            <?php $_smarty_tpl->_subTemplateRender('file:checkout/customer_shipping_contact.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('prefix'=>"register",'fehlendeAngaben'=>$_smarty_tpl->tpl_vars['fehlendeAngabenShipping']->value), 0, false);
?>
                                        <?php
}
}
/* {/block 'checkout-inc-shipping-address-include-customer-shipping-contact'} */
/* {block 'checkout-inc-shipping-address-fieldset-register'} */
class Block_1464601390619660f512f347_49530379 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <fieldset id="register_shipping_address" class="checkout-register-shipping-address collapse collapse-non-validate <?php if ($_smarty_tpl->tpl_vars['kLieferadresse']->value == -1) {?>} show<?php }?>" aria-expanded="<?php if ($_smarty_tpl->tpl_vars['kLieferadresse']->value == -1) {?>}true<?php } else { ?>false<?php }?>">
                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1477450635619660f51304d5_67077552', 'checkout-inc-shipping-address-legend-register', $this->tplIndex);
?>

                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1928721262619660f5131006_02185976', 'checkout-inc-shipping-address-include-customer-shipping-address', $this->tplIndex);
?>

                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_973960247619660f5131d60_83700703', 'checkout-inc-shipping-address-include-customer-shipping-contact', $this->tplIndex);
?>

                                    </fieldset>
                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-fieldset-register'} */
/* {block 'checkout-inc-shipping-address-legend-register-first'} */
class Block_603762874619660f51339e8_59494818 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <div class="h3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'createNewShippingAdress','section'=>'account data'),$_smarty_tpl ) );?>
</div>
                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-legend-register-first'} */
/* {block 'checkout-inc-shipping-address-include-customer-shipping-address-first'} */
class Block_1751633965619660f51348d0_51136699 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/customer_shipping_address.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('prefix'=>"register",'fehlendeAngaben'=>$_smarty_tpl->tpl_vars['fehlendeAngabenShipping']->value), 0, true);
?>
                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-include-customer-shipping-address-first'} */
/* {block 'checkout-inc-shipping-address-include-customer-shipping-contact-first'} */
class Block_1759346473619660f5135577_20248590 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php $_smarty_tpl->_subTemplateRender('file:checkout/customer_shipping_contact.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('prefix'=>"register",'fehlendeAngaben'=>$_smarty_tpl->tpl_vars['fehlendeAngabenShipping']->value), 0, true);
?>
                                <?php
}
}
/* {/block 'checkout-inc-shipping-address-include-customer-shipping-contact-first'} */
/* {block 'checkout-inc-shipping-address-shipping-address-body'} */
class Block_86305272619660f51223a9_52186064 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php if (!empty($_SESSION['Kunde']->kKunde) && (isset($_smarty_tpl->tpl_vars['Lieferadressen']->value)) && count($_smarty_tpl->tpl_vars['Lieferadressen']->value) > 0) {?>
                        <?php $_block_plugin120 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin120, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin120->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin121 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin121, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>4));
$_block_repeat=true;
echo $_block_plugin121->render(array('cols'=>12,'md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_943062624619660f5124304_52738044', 'checkout-inc-shipping-address-legend-address', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin121->render(array('cols'=>12,'md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin122 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin122, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin122->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2100015046619660f5125264_05686171', 'checkout-inc-shipping-address-fieldset-address', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1464601390619660f512f347_49530379', 'checkout-inc-shipping-address-fieldset-register', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin122->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin120->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php } else { ?>
                        <?php $_block_plugin128 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin128, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin128->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php $_block_plugin129 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin129, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>4));
$_block_repeat=true;
echo $_block_plugin129->render(array('cols'=>12,'md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_603762874619660f51339e8_59494818', 'checkout-inc-shipping-address-legend-register-first', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin129->render(array('cols'=>12,'md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php $_block_plugin130 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin130, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin130->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1751633965619660f51348d0_51136699', 'checkout-inc-shipping-address-include-customer-shipping-address-first', $this->tplIndex);
?>

                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1759346473619660f5135577_20248590', 'checkout-inc-shipping-address-include-customer-shipping-contact-first', $this->tplIndex);
?>

                            <?php $_block_repeat=false;
echo $_block_plugin130->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_repeat=false;
echo $_block_plugin128->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php }?>
                <?php
}
}
/* {/block 'checkout-inc-shipping-address-shipping-address-body'} */
/* {block 'checkout-inc-shipping-address-shipping-address'} */
class Block_1204527196619660f51215c6_45988077 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div id="select_shipping_address" class="collapse collapse-non-validate<?php if ($_smarty_tpl->tpl_vars['showShippingAddress']->value) {?> show<?php }?>" aria-expanded="<?php if ($_smarty_tpl->tpl_vars['showShippingAddress']->value) {?>true<?php } else { ?>false<?php }?>">
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_86305272619660f51223a9_52186064', 'checkout-inc-shipping-address-shipping-address-body', $this->tplIndex);
?>

            </div>
            <?php
}
}
/* {/block 'checkout-inc-shipping-address-shipping-address'} */
/* {block 'checkout-inc-shipping-address-script-show-shipping-address'} */
class Block_1349759583619660f5137bc3_55166004 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin131 = isset($_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['inline_script'][0][0] : null;
if (!is_callable(array($_block_plugin131, 'inlineScript'))) {
throw new SmartyException('block tag \'inline_script\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('inline_script', array());
$_block_repeat=true;
echo $_block_plugin131->inlineScript(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo '<script'; ?>
>
                $(window).on('load', function () {
                    var $registerShippingAddress = $('#checkout_register_shipping_address');
                    if ($registerShippingAddress.prop('checked')) {
                        $registerShippingAddress.click();
                    }
                    $.evo.extended().smoothScrollToAnchor('#checkout_register_shipping_address');
                });
            <?php echo '</script'; ?>
><?php $_block_repeat=false;
echo $_block_plugin131->inlineScript(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'checkout-inc-shipping-address-script-show-shipping-address'} */
/* {block 'checkout-inc-shipping-address'} */
class Block_1757067103619660f511ad23_55671786 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'checkout-inc-shipping-address' => 
  array (
    0 => 'Block_1757067103619660f511ad23_55671786',
  ),
  'checkout-inc-shipping-address-checkbox-equals' => 
  array (
    0 => 'Block_841395447619660f511daf6_60556660',
  ),
  'checkout-inc-shipping-address-shipping-address' => 
  array (
    0 => 'Block_1204527196619660f51215c6_45988077',
  ),
  'checkout-inc-shipping-address-shipping-address-body' => 
  array (
    0 => 'Block_86305272619660f51223a9_52186064',
  ),
  'checkout-inc-shipping-address-legend-address' => 
  array (
    0 => 'Block_943062624619660f5124304_52738044',
  ),
  'checkout-inc-shipping-address-fieldset-address' => 
  array (
    0 => 'Block_2100015046619660f5125264_05686171',
  ),
  'checkout-inc-shipping-address-address' => 
  array (
    0 => 'Block_700560397619660f51270c8_04299125',
  ),
  'checkout-inc-shipping-address-new-address' => 
  array (
    0 => 'Block_873175389619660f512cb11_38942849',
  ),
  'checkout-inc-shipping-address-fieldset-register' => 
  array (
    0 => 'Block_1464601390619660f512f347_49530379',
  ),
  'checkout-inc-shipping-address-legend-register' => 
  array (
    0 => 'Block_1477450635619660f51304d5_67077552',
  ),
  'checkout-inc-shipping-address-include-customer-shipping-address' => 
  array (
    0 => 'Block_1928721262619660f5131006_02185976',
  ),
  'checkout-inc-shipping-address-include-customer-shipping-contact' => 
  array (
    0 => 'Block_973960247619660f5131d60_83700703',
  ),
  'checkout-inc-shipping-address-legend-register-first' => 
  array (
    0 => 'Block_603762874619660f51339e8_59494818',
  ),
  'checkout-inc-shipping-address-include-customer-shipping-address-first' => 
  array (
    0 => 'Block_1751633965619660f51348d0_51136699',
  ),
  'checkout-inc-shipping-address-include-customer-shipping-contact-first' => 
  array (
    0 => 'Block_1759346473619660f5135577_20248590',
  ),
  'checkout-inc-shipping-address-script-show-shipping-address' => 
  array (
    0 => 'Block_1349759583619660f5137bc3_55166004',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_assignInScope('fehlendeAngabenShipping', (($tmp = $_smarty_tpl->tpl_vars['fehlendeAngaben']->value['shippingAddress'] ?? null)===null||$tmp==='' ? null : $tmp));?>
    <?php $_smarty_tpl->_assignInScope('showShippingAddress', ((isset($_smarty_tpl->tpl_vars['Lieferadresse']->value)) || !empty($_smarty_tpl->tpl_vars['kLieferadresse']->value) || (isset($_smarty_tpl->tpl_vars['forceDeliveryAddress']->value))));?>
    <?php $_block_plugin116 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin116, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array('class'=>"inc-shipping-address"));
$_block_repeat=true;
echo $_block_plugin116->render(array('class'=>"inc-shipping-address"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
        <?php $_block_plugin117 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin117, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin117->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_841395447619660f511daf6_60556660', 'checkout-inc-shipping-address-checkbox-equals', $this->tplIndex);
?>

        <?php $_block_repeat=false;
echo $_block_plugin117->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php $_block_plugin119 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin119, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12));
$_block_repeat=true;
echo $_block_plugin119->render(array('cols'=>12), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1204527196619660f51215c6_45988077', 'checkout-inc-shipping-address-shipping-address', $this->tplIndex);
?>

        <?php $_block_repeat=false;
echo $_block_plugin119->render(array('cols'=>12), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php $_block_repeat=false;
echo $_block_plugin116->render(array('class'=>"inc-shipping-address"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php if ((isset($_GET['editLieferadresse'])) || $_smarty_tpl->tpl_vars['step']->value === 'Lieferadresse') {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1349759583619660f5137bc3_55166004', 'checkout-inc-shipping-address-script-show-shipping-address', $this->tplIndex);
?>

    <?php }
}
}
/* {/block 'checkout-inc-shipping-address'} */
}
