<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:46:51
  from '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/downloads.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd4ab3e4282_39260611',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ff0a91885ecede34f7be99fae94aa77dba2bfe29' => 
    array (
      0 => '/var/www/vhosts/test.uhr24.de/httpdocs/templates/NOVA/account/downloads.tpl',
      1 => 1632904510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619cd4ab3e4282_39260611 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1146029503619cd4ab39e462_03820702', 'account-downloads');
?>

<?php }
/* {block 'account-downloads-subheading'} */
class Block_1604535500619cd4ab39f7b0_26365641 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div class="h2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'yourDownloads'),$_smarty_tpl ) );?>
</div>
        <?php
}
}
/* {/block 'account-downloads-subheading'} */
/* {block 'account-downloads-order-downloads-item-heading'} */
class Block_152845722619cd4ab3a4721_24993402 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <?php echo $_smarty_tpl->tpl_vars['oDownload']->value->oDownloadSprache->getName();?>

                            <?php
}
}
/* {/block 'account-downloads-order-downloads-item-heading'} */
/* {block 'account-downloads-order-downloads-item-download-button'} */
class Block_1372262744619cd4ab3b3fb4_70881491 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                    <?php $_block_plugin62 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin62, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('size'=>"sm",'type'=>"submit",'variant'=>"outline-primary"));
$_block_repeat=true;
echo $_block_plugin62->render(array('size'=>"sm",'type'=>"submit",'variant'=>"outline-primary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                        <i class="fa fa-download"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'download'),$_smarty_tpl ) );?>

                                                    <?php $_block_repeat=false;
echo $_block_plugin62->render(array('size'=>"sm",'type'=>"submit",'variant'=>"outline-primary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                <?php
}
}
/* {/block 'account-downloads-order-downloads-item-download-button'} */
/* {block 'account-downloads-order-downloads-item-body'} */
class Block_1818362129619cd4ab3a6437_32398284 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

                                <?php $_block_plugin49 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin49, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin49->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin50 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin50, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin50->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'downloadOrderDate'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin50->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin51 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin51, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin51->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo smarty_modifier_date_format((($tmp = $_smarty_tpl->tpl_vars['Bestellung']->value->dErstellt ?? null)===null||$tmp==='' ? "--" : $tmp),"%d.%m.%Y %H:%M");
$_block_repeat=false;
echo $_block_plugin51->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin49->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin52 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin52, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin52->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin53 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin53, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin53->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'downloadLimit'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin53->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin54 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin54, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin54->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'unlimited'),$_smarty_tpl ) );
$_prefixVariable12 = ob_get_clean();
echo (($tmp = $_smarty_tpl->tpl_vars['oDownload']->value->cLimit ?? null)===null||$tmp==='' ? $_prefixVariable12 : $tmp);
$_block_repeat=false;
echo $_block_plugin54->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin52->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin55 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin55, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin55->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin56 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin56, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin56->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'validUntil'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin56->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin57 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin57, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin57->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'unlimited'),$_smarty_tpl ) );
$_prefixVariable13 = ob_get_clean();
echo (($tmp = $_smarty_tpl->tpl_vars['oDownload']->value->dGueltigBis ?? null)===null||$tmp==='' ? $_prefixVariable13 : $tmp);
$_block_repeat=false;
echo $_block_plugin57->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin55->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_plugin58 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin58, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin58->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                    <?php $_block_plugin59 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin59, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin59->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'download'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin59->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php $_block_plugin60 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin60, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin60->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                        <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == (defined('BESTELLUNG_STATUS_BEZAHLT') ? constant('BESTELLUNG_STATUS_BEZAHLT') : null) || $_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == (defined('BESTELLUNG_STATUS_VERSANDT') ? constant('BESTELLUNG_STATUS_VERSANDT') : null) || $_smarty_tpl->tpl_vars['Bestellung']->value->cStatus == (defined('BESTELLUNG_STATUS_TEILVERSANDT') ? constant('BESTELLUNG_STATUS_TEILVERSANDT') : null)) {?>
                                            <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php'),$_smarty_tpl ) );
$_prefixVariable14=ob_get_clean();
$_block_plugin61 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin61, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('method'=>"post",'action'=>$_prefixVariable14,'slide'=>true));
$_block_repeat=true;
echo $_block_plugin61->render(array('method'=>"post",'action'=>$_prefixVariable14,'slide'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('name'=>"a",'type'=>"hidden",'value'=>"getdl"),$_smarty_tpl ) );?>

                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('name'=>"bestellung",'type'=>"hidden",'value'=>$_smarty_tpl->tpl_vars['Bestellung']->value->kBestellung),$_smarty_tpl ) );?>

                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('name'=>"dl",'type'=>"hidden",'value'=>$_smarty_tpl->tpl_vars['oDownload']->value->getDownload()),$_smarty_tpl ) );?>

                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1372262744619cd4ab3b3fb4_70881491', 'account-downloads-order-downloads-item-download-button', $this->tplIndex);
?>

                                            <?php $_block_repeat=false;
echo $_block_plugin61->render(array('method'=>"post",'action'=>$_prefixVariable14,'slide'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php } else { ?>
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'downloadPending'),$_smarty_tpl ) );?>

                                        <?php }?>
                                    <?php $_block_repeat=false;
echo $_block_plugin60->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                <?php $_block_repeat=false;
echo $_block_plugin58->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                            <?php
}
}
/* {/block 'account-downloads-order-downloads-item-body'} */
/* {block 'account-downloads-order-downloads'} */
class Block_1756602342619cd4ab3a02f1_75324843 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->oDownload_arr, 'oDownload');
$_smarty_tpl->tpl_vars['oDownload']->iteration = 0;
$_smarty_tpl->tpl_vars['oDownload']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oDownload']->value) {
$_smarty_tpl->tpl_vars['oDownload']->do_else = false;
$_smarty_tpl->tpl_vars['oDownload']->iteration++;
$__foreach_oDownload_3_saved = $_smarty_tpl->tpl_vars['oDownload'];
?>
                <?php $_block_plugin44 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin44, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'class'=>"cols-12 col-md-8 download-item"));
$_block_repeat=true;
echo $_block_plugin44->render(array('no-body'=>true,'class'=>"cols-12 col-md-8 download-item"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php $_block_plugin45 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin45, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array('id'=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)));
$_block_repeat=true;
echo $_block_plugin45->render(array('id'=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin46 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin46, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('variant'=>"link",'role'=>"button",'block'=>true,'aria'=>array("expanded"=>"false","controls"=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("toggle"=>"collapse","target"=>"#collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration))));
$_block_repeat=true;
echo $_block_plugin46->render(array('variant'=>"link",'role'=>"button",'block'=>true,'aria'=>array("expanded"=>"false","controls"=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("toggle"=>"collapse","target"=>"#collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_152845722619cd4ab3a4721_24993402', 'account-downloads-order-downloads-item-heading', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin46->render(array('variant'=>"link",'role'=>"button",'block'=>true,'aria'=>array("expanded"=>"false","controls"=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("toggle"=>"collapse","target"=>"#collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin45->render(array('id'=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_plugin47 = isset($_smarty_tpl->smarty->registered_plugins['block']['collapse'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['collapse'][0][0] : null;
if (!is_callable(array($_block_plugin47, 'render'))) {
throw new SmartyException('block tag \'collapse\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('collapse', array('id'=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration),'visible'=>false));
$_block_repeat=true;
echo $_block_plugin47->render(array('id'=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration),'visible'=>false), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin48 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin48, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array());
$_block_repeat=true;
echo $_block_plugin48->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1818362129619cd4ab3a6437_32398284', 'account-downloads-order-downloads-item-body', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin48->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin47->render(array('id'=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration),'visible'=>false), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin44->render(array('no-body'=>true,'class'=>"cols-12 col-md-8 download-item"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php
$_smarty_tpl->tpl_vars['oDownload'] = $__foreach_oDownload_3_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php
}
}
/* {/block 'account-downloads-order-downloads'} */
/* {block 'account-downloads-customer-downloads-heading'} */
class Block_1818916309619cd4ab3badc4_96580446 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                <span class="h3">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'myDownloads'),$_smarty_tpl ) );?>

                                </span>
                            <?php
}
}
/* {/block 'account-downloads-customer-downloads-heading'} */
/* {block 'account-downloads-customer-downloads-item-heading'} */
class Block_1645751701619cd4ab3c2ab9_93823402 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                        <?php echo $_smarty_tpl->tpl_vars['oDownload']->value->oDownloadSprache->getName();?>

                                                    <?php
}
}
/* {/block 'account-downloads-customer-downloads-item-heading'} */
/* {block 'account-downloads-customer-downloads-item-download-button'} */
class Block_657904241619cd4ab3dd716_32881561 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                                                        <?php $_block_plugin86 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin86, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('size'=>"sm",'type'=>"submit",'variant'=>"outline-primary"));
$_block_repeat=true;
echo $_block_plugin86->render(array('size'=>"sm",'type'=>"submit",'variant'=>"outline-primary"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                            <i class="fa fa-download"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'download'),$_smarty_tpl ) );?>

                                                                        <?php $_block_repeat=false;
echo $_block_plugin86->render(array('size'=>"sm",'type'=>"submit",'variant'=>"outline-primary"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                                    <?php
}
}
/* {/block 'account-downloads-customer-downloads-item-download-button'} */
/* {block 'account-downloads-customer-downloads-item-body'} */
class Block_1201105303619cd4ab3c5b07_84771752 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

                                                        <?php $_smarty_tpl->_assignInScope('cStatus', (defined('BESTELLUNG_STATUS_OFFEN') ? constant('BESTELLUNG_STATUS_OFFEN') : null));?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellungen']->value, 'Bestellung');
$_smarty_tpl->tpl_vars['Bestellung']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Bestellung']->value) {
$_smarty_tpl->tpl_vars['Bestellung']->do_else = false;
?>
                                                            <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->kBestellung == $_smarty_tpl->tpl_vars['oDownload']->value->kBestellung) {?>
                                                                <?php $_smarty_tpl->_assignInScope('cStatus', $_smarty_tpl->tpl_vars['Bestellung']->value->cStatus);?>
                                                                <?php $_smarty_tpl->_assignInScope('dErstellt', $_smarty_tpl->tpl_vars['Bestellung']->value->dErstellt);?>
                                                            <?php }?>
                                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                        <?php $_block_plugin73 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin73, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin73->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php $_block_plugin74 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin74, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin74->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'downloadOrderDate'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin74->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php $_block_plugin75 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin75, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin75->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo smarty_modifier_date_format((($tmp = $_smarty_tpl->tpl_vars['dErstellt']->value ?? null)===null||$tmp==='' ? "--" : $tmp),"%d.%m.%Y %H:%M");
$_block_repeat=false;
echo $_block_plugin75->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_repeat=false;
echo $_block_plugin73->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_plugin76 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin76, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin76->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php $_block_plugin77 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin77, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin77->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'downloadLimit'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin77->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php $_block_plugin78 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin78, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin78->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'unlimited'),$_smarty_tpl ) );
$_prefixVariable15 = ob_get_clean();
echo (($tmp = $_smarty_tpl->tpl_vars['oDownload']->value->cLimit ?? null)===null||$tmp==='' ? $_prefixVariable15 : $tmp);
$_block_repeat=false;
echo $_block_plugin78->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_repeat=false;
echo $_block_plugin76->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_plugin79 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin79, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin79->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php $_block_plugin80 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin80, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin80->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'validUntil'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin80->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php $_block_plugin81 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin81, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin81->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'unlimited'),$_smarty_tpl ) );
$_prefixVariable16 = ob_get_clean();
echo (($tmp = $_smarty_tpl->tpl_vars['oDownload']->value->dGueltigBis ?? null)===null||$tmp==='' ? $_prefixVariable16 : $tmp);
$_block_repeat=false;
echo $_block_plugin81->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_repeat=false;
echo $_block_plugin79->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_plugin82 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin82, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin82->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php $_block_plugin83 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin83, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>4));
$_block_repeat=true;
echo $_block_plugin83->render(array('md'=>4), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'download'),$_smarty_tpl ) );?>
:<?php $_block_repeat=false;
echo $_block_plugin83->render(array('md'=>4), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php $_block_plugin84 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin84, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('md'=>8));
$_block_repeat=true;
echo $_block_plugin84->render(array('md'=>8), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                            <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_static_route'][0], array( array('id'=>'jtl.php'),$_smarty_tpl ) );
$_prefixVariable17=ob_get_clean();
$_block_plugin85 = isset($_smarty_tpl->smarty->registered_plugins['block']['form'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['form'][0][0] : null;
if (!is_callable(array($_block_plugin85, 'render'))) {
throw new SmartyException('block tag \'form\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('method'=>"post",'action'=>$_prefixVariable17,'slide'=>true));
$_block_repeat=true;
echo $_block_plugin85->render(array('method'=>"post",'action'=>$_prefixVariable17,'slide'=>true), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('name'=>"kBestellung",'type'=>"hidden",'value'=>$_smarty_tpl->tpl_vars['oDownload']->value->kBestellung),$_smarty_tpl ) );?>

                                                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('name'=>"kKunde",'type'=>"hidden",'value'=>$_SESSION['Kunde']->kKunde),$_smarty_tpl ) );?>

                                                                <?php if ($_smarty_tpl->tpl_vars['cStatus']->value == (defined('BESTELLUNG_STATUS_BEZAHLT') ? constant('BESTELLUNG_STATUS_BEZAHLT') : null) || $_smarty_tpl->tpl_vars['cStatus']->value == (defined('BESTELLUNG_STATUS_VERSANDT') ? constant('BESTELLUNG_STATUS_VERSANDT') : null) || $_smarty_tpl->tpl_vars['cStatus']->value == (defined('BESTELLUNG_STATUS_TEILVERSANDT') ? constant('BESTELLUNG_STATUS_TEILVERSANDT') : null)) {?>
                                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['input'][0], array( array('name'=>"dl",'type'=>"hidden",'value'=>$_smarty_tpl->tpl_vars['oDownload']->value->getDownload()),$_smarty_tpl ) );?>

                                                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_657904241619cd4ab3dd716_32881561', 'account-downloads-customer-downloads-item-download-button', $this->tplIndex);
?>

                                                                <?php } else { ?>
                                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['lang'][0], array( array('key'=>'downloadPending'),$_smarty_tpl ) );?>

                                                                <?php }?>
                                                            <?php $_block_repeat=false;
echo $_block_plugin85->render(array('method'=>"post",'action'=>$_prefixVariable17,'slide'=>true), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                            <?php $_block_repeat=false;
echo $_block_plugin84->render(array('md'=>8), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                        <?php $_block_repeat=false;
echo $_block_plugin82->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                                    <?php
}
}
/* {/block 'account-downloads-customer-downloads-item-body'} */
/* {block 'account-downloads-customer-downloads'} */
class Block_40545783619cd4ab3bcbd8_45585979 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oDownload_arr']->value, 'oDownload');
$_smarty_tpl->tpl_vars['oDownload']->iteration = 0;
$_smarty_tpl->tpl_vars['oDownload']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oDownload']->value) {
$_smarty_tpl->tpl_vars['oDownload']->do_else = false;
$_smarty_tpl->tpl_vars['oDownload']->iteration++;
$__foreach_oDownload_4_saved = $_smarty_tpl->tpl_vars['oDownload'];
?>
                                        <?php $_block_plugin68 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin68, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'class'=>"download-item"));
$_block_repeat=true;
echo $_block_plugin68->render(array('no-body'=>true,'class'=>"download-item"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                            <?php $_block_plugin69 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin69, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array('id'=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)));
$_block_repeat=true;
echo $_block_plugin69->render(array('id'=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php $_block_plugin70 = isset($_smarty_tpl->smarty->registered_plugins['block']['button'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['button'][0][0] : null;
if (!is_callable(array($_block_plugin70, 'render'))) {
throw new SmartyException('block tag \'button\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('button', array('variant'=>"link",'role'=>"button",'block'=>true,'aria'=>array("expanded"=>"false","controls"=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("toggle"=>"collapse","target"=>"#collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration))));
$_block_repeat=true;
echo $_block_plugin70->render(array('variant'=>"link",'role'=>"button",'block'=>true,'aria'=>array("expanded"=>"false","controls"=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("toggle"=>"collapse","target"=>"#collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration))), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1645751701619cd4ab3c2ab9_93823402', 'account-downloads-customer-downloads-item-heading', $this->tplIndex);
?>

                                                <?php $_block_repeat=false;
echo $_block_plugin70->render(array('variant'=>"link",'role'=>"button",'block'=>true,'aria'=>array("expanded"=>"false","controls"=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("toggle"=>"collapse","target"=>"#collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration))), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            <?php $_block_repeat=false;
echo $_block_plugin69->render(array('id'=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            <?php $_block_plugin71 = isset($_smarty_tpl->smarty->registered_plugins['block']['collapse'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['collapse'][0][0] : null;
if (!is_callable(array($_block_plugin71, 'render'))) {
throw new SmartyException('block tag \'collapse\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('collapse', array('id'=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration),'visible'=>false,'aria'=>array("labelledby"=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("parent"=>"#account-download-accordion")));
$_block_repeat=true;
echo $_block_plugin71->render(array('id'=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration),'visible'=>false,'aria'=>array("labelledby"=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("parent"=>"#account-download-accordion")), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                <?php $_block_plugin72 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin72, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array());
$_block_repeat=true;
echo $_block_plugin72->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1201105303619cd4ab3c5b07_84771752', 'account-downloads-customer-downloads-item-body', $this->tplIndex);
?>

                                                <?php $_block_repeat=false;
echo $_block_plugin72->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                            <?php $_block_repeat=false;
echo $_block_plugin71->render(array('id'=>"collapse-download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration),'visible'=>false,'aria'=>array("labelledby"=>"download-".((string)$_smarty_tpl->tpl_vars['oDownload']->iteration)),'data'=>array("parent"=>"#account-download-accordion")), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                        <?php $_block_repeat=false;
echo $_block_plugin68->render(array('no-body'=>true,'class'=>"download-item"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                                    <?php
$_smarty_tpl->tpl_vars['oDownload'] = $__foreach_oDownload_4_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                <?php
}
}
/* {/block 'account-downloads-customer-downloads'} */
/* {block 'account-downloads-customer-downloads'} */
class Block_322184503619cd4ab3b8cb7_93804187 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_block_plugin63 = isset($_smarty_tpl->smarty->registered_plugins['block']['row'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['row'][0][0] : null;
if (!is_callable(array($_block_plugin63, 'render'))) {
throw new SmartyException('block tag \'row\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('row', array());
$_block_repeat=true;
echo $_block_plugin63->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                <?php $_block_plugin64 = isset($_smarty_tpl->smarty->registered_plugins['block']['col'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['col'][0][0] : null;
if (!is_callable(array($_block_plugin64, 'render'))) {
throw new SmartyException('block tag \'col\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('col', array('cols'=>12,'md'=>6));
$_block_repeat=true;
echo $_block_plugin64->render(array('cols'=>12,'md'=>6), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                    <?php $_block_plugin65 = isset($_smarty_tpl->smarty->registered_plugins['block']['card'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['card'][0][0] : null;
if (!is_callable(array($_block_plugin65, 'render'))) {
throw new SmartyException('block tag \'card\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('card', array('no-body'=>true,'class'=>"download-main"));
$_block_repeat=true;
echo $_block_plugin65->render(array('no-body'=>true,'class'=>"download-main"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php $_block_plugin66 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardheader'][0][0] : null;
if (!is_callable(array($_block_plugin66, 'render'))) {
throw new SmartyException('block tag \'cardheader\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardheader', array());
$_block_repeat=true;
echo $_block_plugin66->render(array(), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1818916309619cd4ab3badc4_96580446', 'account-downloads-customer-downloads-heading', $this->tplIndex);
?>

                        <?php $_block_repeat=false;
echo $_block_plugin66->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                        <?php $_block_plugin67 = isset($_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['cardbody'][0][0] : null;
if (!is_callable(array($_block_plugin67, 'render'))) {
throw new SmartyException('block tag \'cardbody\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('cardbody', array('class'=>"download-main-body"));
$_block_repeat=true;
echo $_block_plugin67->render(array('class'=>"download-main-body"), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                            <div id="account-download-accordion">
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_40545783619cd4ab3bcbd8_45585979', 'account-downloads-customer-downloads', $this->tplIndex);
?>

                            </div>
                        <?php $_block_repeat=false;
echo $_block_plugin67->render(array('class'=>"download-main-body"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                    <?php $_block_repeat=false;
echo $_block_plugin65->render(array('no-body'=>true,'class'=>"download-main"), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php $_block_repeat=false;
echo $_block_plugin64->render(array('cols'=>12,'md'=>6), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
            <?php $_block_repeat=false;
echo $_block_plugin63->render(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
        <?php
}
}
/* {/block 'account-downloads-customer-downloads'} */
/* {block 'account-downloads'} */
class Block_1146029503619cd4ab39e462_03820702 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'account-downloads' => 
  array (
    0 => 'Block_1146029503619cd4ab39e462_03820702',
  ),
  'account-downloads-subheading' => 
  array (
    0 => 'Block_1604535500619cd4ab39f7b0_26365641',
  ),
  'account-downloads-order-downloads' => 
  array (
    0 => 'Block_1756602342619cd4ab3a02f1_75324843',
  ),
  'account-downloads-order-downloads-item-heading' => 
  array (
    0 => 'Block_152845722619cd4ab3a4721_24993402',
  ),
  'account-downloads-order-downloads-item-body' => 
  array (
    0 => 'Block_1818362129619cd4ab3a6437_32398284',
  ),
  'account-downloads-order-downloads-item-download-button' => 
  array (
    0 => 'Block_1372262744619cd4ab3b3fb4_70881491',
  ),
  'account-downloads-customer-downloads' => 
  array (
    0 => 'Block_322184503619cd4ab3b8cb7_93804187',
    1 => 'Block_40545783619cd4ab3bcbd8_45585979',
  ),
  'account-downloads-customer-downloads-heading' => 
  array (
    0 => 'Block_1818916309619cd4ab3badc4_96580446',
  ),
  'account-downloads-customer-downloads-item-heading' => 
  array (
    0 => 'Block_1645751701619cd4ab3c2ab9_93823402',
  ),
  'account-downloads-customer-downloads-item-body' => 
  array (
    0 => 'Block_1201105303619cd4ab3c5b07_84771752',
  ),
  'account-downloads-customer-downloads-item-download-button' => 
  array (
    0 => 'Block_657904241619cd4ab3dd716_32881561',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if (!empty($_smarty_tpl->tpl_vars['Bestellung']->value->oDownload_arr)) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1604535500619cd4ab39f7b0_26365641', 'account-downloads-subheading', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1756602342619cd4ab3a02f1_75324843', 'account-downloads-order-downloads', $this->tplIndex);
?>

    <?php } elseif (!empty($_smarty_tpl->tpl_vars['oDownload_arr']->value)) {?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_322184503619cd4ab3b8cb7_93804187', 'account-downloads-customer-downloads', $this->tplIndex);
?>

    <?php }
}
}
/* {/block 'account-downloads'} */
}
