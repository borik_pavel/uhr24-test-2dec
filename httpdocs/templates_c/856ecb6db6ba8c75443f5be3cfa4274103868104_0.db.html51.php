<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:46:50
  from 'db:html51' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd4aa30bd91_45684574',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '856ecb6db6ba8c75443f5be3cfa4274103868104' => 
    array (
      0 => 'db:html51',
      1 => 1637668010,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619cd4aa30bd91_45684574 (Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'html'),$_smarty_tpl ) );?>


Sehr <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAnrede == "w") {?>geehrte<?php } else { ?>geehrter<?php }?> <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cAnredeLocalized;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
,<br>
<br>
vielen Dank für die Registrierung in unserem Onlineshop unter <a href="<?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
" target="_blank"><strong><?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>
</strong></a><br>
<br>
Zur Kontrolle hier noch einmal Ihre Kundendaten:<br>
<br>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="column mobile-left" width="25%" align="right" valign="top">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<strong>Anschrift:</strong>
						</font>
					</td>
				</tr>
			</table>
		</td>
		<td class="column" align="left" valign="top" bgcolor="#ffffff">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cAnredeLocalized;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
<br>
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>
<br>
							<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz;?>
<br><?php }?>
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>
<br>
							<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cBundesland) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cBundesland;?>
<br><?php }?>
							<font style="text-transform: uppercase;"><?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;?>
</font>
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cTel) {?>
	<tr>
		<td class="column mobile-left" align="right" valign="top">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<strong>Telefon:</strong>
						</font>
					</td>
				</tr>
			</table>
		</td>
		<td class="column" align="left" valign="top" bgcolor="#ffffff">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTel;?>

						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cMobil) {?>
	<tr>
		<td class="column mobile-left" align="right" valign="top">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<strong>Mobil:</strong>
						</font>
					</td>
				</tr>
			</table>
		</td>
		<td class="column" align="left" valign="top" bgcolor="#ffffff">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMobil;?>

						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cFax) {?>
	<tr>
		<td class="column mobile-left" align="right" valign="top">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<strong>Fax:</strong>
						</font>
					</td>
				</tr>
			</table>
		</td>
		<td class="column" align="left" valign="top" bgcolor="#ffffff">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFax;?>

						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php }?>
	<tr>
		<td class="column mobile-left" align="right" valign="top">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<strong>E-Mail:</strong>
						</font>
					</td>
				</tr>
			</table>
		</td>
		<td class="column" align="left" valign="top" bgcolor="#ffffff">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMail;?>

						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cUSTID) {?>
	<tr>
		<td class="column mobile-left" align="right" valign="top">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<strong>UstID:</strong>
						</font>
					</td>
				</tr>
			</table>
		</td>
		<td class="column" align="left" valign="top" bgcolor="#ffffff">
			<table cellpadding="0" cellspacing="6">
				<tr>
					<td>
						<font color="#313131" face="Helvetica, Arial, sans-serif" size="3" style="color: #313131; font-family: Helvetica, Arial, sans-serif; font-size: 15px; line-height: 18px;">
							<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cUSTID;?>

						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php }?>
</table><br>
Mit diesen Daten können Sie sich ab sofort in Ihrem persönlichen Kundenkonto anmelden und den aktuellen Status Ihrer Bestellungen verfolgen.<br>
<br>
Wir freuen uns sehr, Sie als neuen Kunden bei uns begrüßen zu dürfen. Wenn sie Fragen zu unserem Angebot oder speziellen Produkten haben, nehmen Sie einfach Kontakt mit uns auf.<br>
<br>
Wir wünschen Ihnen viel Spaß beim Stöbern in unserem Sortiment.<br>
<br>
Mit freundlichem Gruß,<br>
Ihr Team von <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cName;?>


<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'html'),$_smarty_tpl ) );
}
}
