<?php
/* Smarty version 3.1.39, created on 2021-11-23 12:46:50
  from 'db:text51' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619cd4aa336427_72385896',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e5d1abd87225ad324c28738dd5155e11175499b9' => 
    array (
      0 => 'db:text51',
      1 => 1637668010,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619cd4aa336427_72385896 (Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'plain'),$_smarty_tpl ) );?>


Sehr <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAnrede == "w") {?>geehrte<?php } else { ?>geehrter<?php }?> <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cAnredeLocalized;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
,

vielen Dank für die Registrierung in unserem Onlineshop unter <?php echo $_smarty_tpl->tpl_vars['ShopURL']->value;?>


Zur Kontrolle hier noch einmal Ihre Kundendaten:

<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cAnredeLocalized;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>

<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz;?>

<?php }
echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cBundesland) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cBundesland;?>

<?php }
echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cTel) {?>Tel: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTel;?>

<?php }
if ($_smarty_tpl->tpl_vars['Kunde']->value->cMobil) {?>Mobil: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMobil;?>

<?php }
if ($_smarty_tpl->tpl_vars['Kunde']->value->cFax) {?>Fax: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFax;?>

<?php }?>
Email: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMail;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cUSTID) {?>UstID: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cUSTID;?>

<?php }?>

Mit diesen Daten können Sie sich ab sofort in Ihrem persönlichen Kundenkonto anmelden und den aktuellen Status Ihrer Bestellungen verfolgen.

Wir freuen uns sehr, Sie als neuen Kunden bei uns begrüßen zu dürfen. Wenn sie Fragen zu unserem Angebot oder speziellen Produkten haben, nehmen Sie einfach Kontakt mit uns auf.

Wir wünschen Ihnen viel Spaß beim Stöbern in unserem Sortiment.

Mit freundlichem Gruß,
Ihr Team von <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cName;?>


<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'plain'),$_smarty_tpl ) );
}
}
