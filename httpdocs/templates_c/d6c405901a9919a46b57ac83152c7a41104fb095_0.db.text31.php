<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:57:13
  from 'db:text31' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619df059496c62_29355561',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd6c405901a9919a46b57ac83152c7a41104fb095' => 
    array (
      0 => 'db:text31',
      1 => 1637740633,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619df059496c62_29355561 (Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'plain'),$_smarty_tpl ) );?>


Guten Tag <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
,

vielen Dank für Ihre Bestellung bei <?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_shopname'];?>
.

<?php if (count($_smarty_tpl->tpl_vars['Verfuegbarkeit_arr']->value['cArtikelName_arr']) > 0) {?>
    <?php echo $_smarty_tpl->tpl_vars['Verfuegbarkeit_arr']->value['cHinweis'];?>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Verfuegbarkeit_arr']->value['cArtikelName_arr'], 'cArtikelname');
$_smarty_tpl->tpl_vars['cArtikelname']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['cArtikelname']->value) {
$_smarty_tpl->tpl_vars['cArtikelname']->do_else = false;
?>
        <?php echo $_smarty_tpl->tpl_vars['cArtikelname']->value;?>


    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php }?>
Ihre Bestellung mit Bestellnummer <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>
 umfasst folgende Positionen:

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Positionen, 'Position');
$_smarty_tpl->tpl_vars['Position']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Position']->value) {
$_smarty_tpl->tpl_vars['Position']->do_else = false;
?>
    <?php if ($_smarty_tpl->tpl_vars['Position']->value->nPosTyp == 1) {?>
        <?php if (!empty($_smarty_tpl->tpl_vars['Position']->value->kKonfigitem)) {?> * <?php }
echo $_smarty_tpl->tpl_vars['Position']->value->nAnzahl;?>
x <?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
 - <?php echo $_smarty_tpl->tpl_vars['Position']->value->cGesamtpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];
if ((isset($_smarty_tpl->tpl_vars['Position']->value->Artikel->nErscheinendesProdukt)) && $_smarty_tpl->tpl_vars['Position']->value->Artikel->nErscheinendesProdukt) {?>
        Verfügbar ab: <?php echo $_smarty_tpl->tpl_vars['Position']->value->Artikel->Erscheinungsdatum_de;
}
if ($_smarty_tpl->tpl_vars['Einstellungen']->value['kaufabwicklung']['bestellvorgang_lieferstatus_anzeigen'] === 'Y' && $_smarty_tpl->tpl_vars['Position']->value->cLieferstatus) {?>

        Lieferzeit: <?php echo $_smarty_tpl->tpl_vars['Position']->value->cLieferstatus;
}?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Position']->value->WarenkorbPosEigenschaftArr, 'WKPosEigenschaft');
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value) {
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = false;
?>

            <?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftName;?>
: <?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftWertName;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    <?php } else { ?>
        <?php echo $_smarty_tpl->tpl_vars['Position']->value->nAnzahl;?>
x <?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
 - <?php echo $_smarty_tpl->tpl_vars['Position']->value->cGesamtpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php if ($_smarty_tpl->tpl_vars['Einstellungen']->value['global']['global_steuerpos_anzeigen'] !== 'N') {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Steuerpositionen, 'Steuerposition');
$_smarty_tpl->tpl_vars['Steuerposition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Steuerposition']->value) {
$_smarty_tpl->tpl_vars['Steuerposition']->do_else = false;
?>
    <?php echo $_smarty_tpl->tpl_vars['Steuerposition']->value->cName;?>
: <?php echo $_smarty_tpl->tpl_vars['Steuerposition']->value->cPreisLocalized;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
if ((isset($_smarty_tpl->tpl_vars['Bestellung']->value->GuthabenNutzen)) && $_smarty_tpl->tpl_vars['Bestellung']->value->GuthabenNutzen == 1) {?>
    Gutschein: -<?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->GutscheinLocalized;?>

<?php }?>

Gesamtsumme: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>


Lieferzeit: <?php if ((isset($_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDeliveryEx))) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDeliveryEx;
} else {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->cEstimatedDelivery;
}?>

Für Expresssendungen gilt: Sofort lieferbare Artikel werden am nächsten Arbeitstag Mo - Fr zugestellt, bei Bestellung an einem Arbeitstag Mo - Fr -13:00, Zahlungseingang vorrausgesetzt.

Für Samstag Expresssendungen gilt:Sofort lieferbare Artikel werden am Samstag zugestellt, bei Bestellung Donerstags nach 13:00 - Freitag 13:00. 

Ihre Rechnungsadresse:
<?php if (!empty($_smarty_tpl->tpl_vars['Kunde']->value->cFirma)) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cFirma;?>
 - <?php if (!empty($_smarty_tpl->tpl_vars['Kunde']->value->cZusatz)) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cZusatz;
}
}
echo $_smarty_tpl->tpl_vars['Kunde']->value->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>

<?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cHausnummer;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cAdressZusatz;?>

<?php }
echo $_smarty_tpl->tpl_vars['Kunde']->value->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cOrt;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cBundesland) {
echo $_smarty_tpl->tpl_vars['Kunde']->value->cBundesland;?>

<?php }
echo $_smarty_tpl->tpl_vars['Kunde']->value->cLand;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cTel) {?>Tel.: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cTel;?>

<?php }
if ($_smarty_tpl->tpl_vars['Kunde']->value->cMobil) {?>Mobil: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMobil;?>

<?php }
if ($_smarty_tpl->tpl_vars['Kunde']->value->cFax) {?>Fax: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cFax;?>

<?php }?>
E-Mail: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cMail;?>

<?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cUSTID) {?>Ust-ID: <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cUSTID;?>

<?php }?>

<?php if (!empty($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->kLieferadresse)) {?>
    Ihre Lieferadresse:

    <?php if (!empty($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFirma)) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFirma;
}?>
    <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cVorname;?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cNachname;?>

    <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cStrasse;?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cHausnummer;?>

    <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cAdressZusatz) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cAdressZusatz;?>

    <?php }
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cPLZ;?>
 <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cOrt;?>

    <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cBundesland) {
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cBundesland;?>

    <?php }
echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cLand;?>

    <?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cTel) {?>Tel.: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cTel;?>

    <?php }
if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMobil) {?>Mobil: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMobil;?>

<?php }
if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFax) {?>Fax: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cFax;?>

<?php }
if ($_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMail) {?>E-Mail: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Lieferadresse->cMail;?>

<?php }
} else { ?>
    Lieferadresse ist gleich Rechnungsadresse.
<?php }?>

Sie haben folgende Zahlungsart gewählt: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cZahlungsartName;?>


<?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_ueberweisung_jtl') {?>
    Bitte führen Sie die folgende Überweisung durch:

    Kontoinhaber:<?php echo $_smarty_tpl->tpl_vars['Firma']->value->cKontoinhaber;?>

    Bankinstitut:<?php echo $_smarty_tpl->tpl_vars['Firma']->value->cBank;?>

    IBAN:<?php echo $_smarty_tpl->tpl_vars['Firma']->value->cIBAN;?>

    BIC:<?php echo $_smarty_tpl->tpl_vars['Firma']->value->cBIC;?>


    Verwendungszweck:<?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>

    Gesamtsumme:<?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>


<?php } elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_nachnahme_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_kreditkarte_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_rechnung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_lastschrift_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_barzahlung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_paypal_jtl') {
}?>

<?php if ((isset($_smarty_tpl->tpl_vars['Zahlungsart']->value->cHinweisText)) && strlen($_smarty_tpl->tpl_vars['Zahlungsart']->value->cHinweisText) > 0) {?>  <?php echo $_smarty_tpl->tpl_vars['Zahlungsart']->value->cHinweisText;?>



<?php }?>

<?php if ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_rechnung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_lastschrift_jtl') {?>
    Wir belasten in Kürze folgendes Bankkonto mit der fälligen Summe:

    Kontoinhaber: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cInhaber;?>

    IBAN: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cIBAN;?>

    BIC: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cBIC;?>

    Bank: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsinfo->cBankName;?>


<?php } elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_barzahlung_jtl') {
} elseif ($_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cModulId === 'za_paypal_jtl') {?>
    Falls Sie Ihre Zahlung per PayPal noch nicht durchgeführt haben, nutzen Sie folgende E-Mail-Adresse als Empfänger: <?php echo $_smarty_tpl->tpl_vars['Einstellungen']->value['zahlungsarten']['zahlungsart_paypal_empfaengermail'];?>

<?php }?>

Über den weiteren Verlauf Ihrer Bestellung werden wir Sie jeweils gesondert informieren.


Mit freundlichem Gruß
Ihr Team von <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cName;?>


<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'plain'),$_smarty_tpl ) );?>

<?php }
}
