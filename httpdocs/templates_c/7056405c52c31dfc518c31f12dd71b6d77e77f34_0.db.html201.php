<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:58:03
  from 'db:html201' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619df08b07a892_99234254',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7056405c52c31dfc518c31f12dd71b6d77e77f34' => 
    array (
      0 => 'db:html201',
      1 => 1637740683,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619df08b07a892_99234254 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'quantityStatisticRow' => 
  array (
    'compiled_filepath' => '/var/www/vhosts/test.uhr24.de/httpdocs/templates_c/7056405c52c31dfc518c31f12dd71b6d77e77f34_0.db.html201.php',
    'uid' => '7056405c52c31dfc518c31f12dd71b6d77e77f34',
    'call_name' => 'smarty_template_function_quantityStatisticRow_1708470655619df08b056a92_40786465',
  ),
));
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/vhosts/test.uhr24.de/httpdocs/includes/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'html'),$_smarty_tpl ) );?>




<h1><?php echo $_smarty_tpl->tpl_vars['oMailObjekt']->value->cIntervall;?>
</h1>
<h2>Zeitraum: <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['oMailObjekt']->value->dVon,"d.m.Y - H:i");?>
 bis <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['oMailObjekt']->value->dBis,"d.m.Y - H:i");?>
</h2>

<div style="display:table">
    <?php if (is_array($_smarty_tpl->tpl_vars['oMailObjekt']->value->oAnzahlArtikelProKundengruppe)) {?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['oMailObjekt']->value->oAnzahlArtikelProKundengruppe, 'oArtikelProKundengruppe');
$_smarty_tpl->tpl_vars['oArtikelProKundengruppe']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['oArtikelProKundengruppe']->value) {
$_smarty_tpl->tpl_vars['oArtikelProKundengruppe']->do_else = false;
?>
            <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>('Produkte pro Kundengruppe: ').($_smarty_tpl->tpl_vars['oArtikelProKundengruppe']->value->cName),'nAnzahlVar'=>$_smarty_tpl->tpl_vars['oArtikelProKundengruppe']->value->nAnzahl), true);?>

        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    <?php }?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Neukunden','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlNeukunden), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Neukunden, die etwas kauften','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlNeukundenGekauft), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bestellungen','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlBestellungen), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bestellungen von Neukunden','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlBestellungenNeukunden), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bestellungen, die bezahlt wurden','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlZahlungseingaengeVonBestellungen), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bestellungen, die versendet wurden','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlVersendeterBestellungen), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Besucher','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlBesucher), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Besucher von Suchmaschinen','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlBesucherSuchmaschine), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bewertungen','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlBewertungen), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bewertungen, nicht freigeschaltet','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlBewertungenNichtFreigeschaltet), true);?>


    <?php if ((isset($_smarty_tpl->tpl_vars['oMailObjekt']->value->oAnzahlGezahltesGuthaben->fSummeGuthaben)) && (isset($_smarty_tpl->tpl_vars['oMailObjekt']->value->oAnzahlGezahltesGuthaben->nAnzahl))) {?>
        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bewertungsguthaben gezahlt','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->oAnzahlGezahltesGuthaben->nAnzahl), true);?>

        <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Bewertungsguthaben Summe','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->oAnzahlGezahltesGuthaben->fSummeGuthaben), true);?>

    <?php }?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Tags','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlTags), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Tags, nicht freigeschaltet','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlTagsNichtFreigeschaltet), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Geworbene Kunden','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlGeworbenerKunden), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Geworbene Kunden, die etwas kauften','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlErfolgreichGeworbenerKunden), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Versendete Wunschlisten','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlVersendeterWunschlisten), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Durchgeführte Umfragen','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlDurchgefuehrteUmfragen), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Neue Beitragskommentare','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlNewskommentare), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Beitragskommentare, nicht freigeschaltet','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlNewskommentareNichtFreigeschaltet), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Neue Produktanfragen','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlProduktanfrageArtikel), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Neue Verfügbarkeitsanfragen','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlProduktanfrageVerfuegbarkeit), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Produktvergleiche','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlVergleiche), true);?>

    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'quantityStatisticRow', array('cAnzahlTitle'=>'Genutzte Kupons','nAnzahlVar'=>$_smarty_tpl->tpl_vars['oMailObjekt']->value->nAnzahlGenutzteKupons), true);?>

</div>

<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'html'),$_smarty_tpl ) );
}
/* smarty_template_function_quantityStatisticRow_1708470655619df08b056a92_40786465 */
if (!function_exists('smarty_template_function_quantityStatisticRow_1708470655619df08b056a92_40786465')) {
function smarty_template_function_quantityStatisticRow_1708470655619df08b056a92_40786465(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

    <?php if ($_smarty_tpl->tpl_vars['nAnzahlVar']->value !== -1) {?>
        <div style="display:table-row">
            <div style="display:table-cell;padding:0.5em;text-align:right;">
                <?php echo $_smarty_tpl->tpl_vars['cAnzahlTitle']->value;?>

            </div>
            <div style="display:table-cell;padding:0.5em;">
                <?php echo $_smarty_tpl->tpl_vars['nAnzahlVar']->value;?>

            </div>
        </div>
    <?php }
}}
/*/ smarty_template_function_quantityStatisticRow_1708470655619df08b056a92_40786465 */
}
