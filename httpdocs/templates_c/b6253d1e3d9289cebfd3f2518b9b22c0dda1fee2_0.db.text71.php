<?php
/* Smarty version 3.1.39, created on 2021-11-24 08:38:49
  from 'db:text71' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_619dec0966d194_54627618',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b6253d1e3d9289cebfd3f2518b9b22c0dda1fee2' => 
    array (
      0 => 'db:text71',
      1 => 1637739529,
      2 => 'db',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_619dec0966d194_54627618 (Smarty_Internal_Template $_smarty_tpl) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'header','type'=>'plain'),$_smarty_tpl ) );?>


Sehr <?php if ($_smarty_tpl->tpl_vars['Kunde']->value->cAnrede == "w") {?>geehrte<?php } else { ?>geehrter<?php }?> <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cAnredeLocalized;?>
 <?php echo $_smarty_tpl->tpl_vars['Kunde']->value->cNachname;?>
,

die Zahlung für Ihre Bestellung mit Bestellnummer <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->cBestellNr;?>
 vom <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->dErstelldatum_de;?>
 in Höhe von <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>
 ist per <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->Zahlungsart->cName;?>
 bei uns eingegangen.

Nachfolgend erhalten Sie nochmals einen Überblick über Ihre Bestellung:

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Positionen, 'Position', false, NULL, 'pos', array (
));
$_smarty_tpl->tpl_vars['Position']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Position']->value) {
$_smarty_tpl->tpl_vars['Position']->do_else = false;
if ($_smarty_tpl->tpl_vars['Position']->value->nPosTyp == 1) {
echo $_smarty_tpl->tpl_vars['Position']->value->nAnzahl;?>
x <?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
 - <?php echo $_smarty_tpl->tpl_vars['Position']->value->cGesamtpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Position']->value->WarenkorbPosEigenschaftArr, 'WKPosEigenschaft', false, NULL, 'variationen', array (
));
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value) {
$_smarty_tpl->tpl_vars['WKPosEigenschaft']->do_else = false;
echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftName;?>
: <?php echo $_smarty_tpl->tpl_vars['WKPosEigenschaft']->value->cEigenschaftWertName;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
} else {
echo $_smarty_tpl->tpl_vars['Position']->value->nAnzahl;?>
x <?php echo $_smarty_tpl->tpl_vars['Position']->value->cName;?>
 - <?php echo $_smarty_tpl->tpl_vars['Position']->value->cGesamtpreisLocalized[$_smarty_tpl->tpl_vars['NettoPreise']->value];?>

<?php }
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['Bestellung']->value->Steuerpositionen, 'Steuerposition', false, NULL, 'steuerpositionen', array (
));
$_smarty_tpl->tpl_vars['Steuerposition']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['Steuerposition']->value) {
$_smarty_tpl->tpl_vars['Steuerposition']->do_else = false;
echo $_smarty_tpl->tpl_vars['Steuerposition']->value->cName;?>
: <?php echo $_smarty_tpl->tpl_vars['Steuerposition']->value->cPreisLocalized;?>

<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php if ((isset($_smarty_tpl->tpl_vars['GuthabenNutzen']->value)) && $_smarty_tpl->tpl_vars['GuthabenNutzen']->value == 1) {?>
Gutschein: -<?php echo $_smarty_tpl->tpl_vars['GutscheinLocalized']->value;?>

<?php }?>

Gesamtsumme: <?php echo $_smarty_tpl->tpl_vars['Bestellung']->value->WarensummeLocalized[0];?>



Über den Versand der Ware werden wir Sie gesondert informieren.

Mit freundlichem Gruß,
Ihr Team von <?php echo $_smarty_tpl->tpl_vars['Firma']->value->cName;?>


<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['includeMailTemplate'][0], array( array('template'=>'footer','type'=>'plain'),$_smarty_tpl ) );
}
}
