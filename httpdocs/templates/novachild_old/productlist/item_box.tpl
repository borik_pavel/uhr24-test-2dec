{extends file="{$parent_template_path}/productlist/item_box.tpl"}

	{block name='productlist-item-box-caption-short-desc'}
	    <div class="productbox-title mt-2" itemprop="name" style="text-align:center">
	    	<p class="artikelliste_hersteller">{$Artikel->cHersteller}</p>
		{link href=$Artikel->cURLFull class="text-clamp-2"}
		    <div class="artikelliste_bezeichnung">{$Artikel->cHersteller} &raquo;{$Artikel->cKurzbezeichnung}, {$Artikel->cHAN}&laquo;</div>
		{/link}
	    </div>
	{/block}
