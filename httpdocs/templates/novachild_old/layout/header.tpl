{extends file="{$parent_template_path}/layout/header.tpl"}

	{block name='layout-header-container-inner' prepend}
			<ul style="background-color: #f1f1f1; text-align: center; margin: 0; padding: .5em"> 
				<li style="display:inline-block; padding: 0 2em 0 2em">&#10004 Versandkostenfrei in DE ab 40&nbsp;&#8364;</li>
				<li style="display:inline-block; padding: 0 2em 0 2em">&#10004 5% Vorteilskauf bei Vorkasse</li>
				{if !$isMobile}<li style="display:inline-block; padding: 0 2em 0 2em">&#10004 Expresslieferung/Samstag m&ouml;glich</li>
				<li style="display:inline-block; padding: 0 2em 0 2em">&#10004 30 Tage R&uuml;ckgaberecht</li>{/if}
			</ul>
	{/block}


	{block name='layout-header-main-wrapper-starttag' append}
	{if !$isMobile}
			<p style="background-color: #264267; color: #efefef; text-align: center; margin: 0.8em 0 -3em 0; padding: 0 0 1px 0">
			Sichern Sie sich 5%! Ihr pers&ouml;nlicher Gutschein-Code ist: <span style="background-color: #ffcc00; color: #000; padding-left: 6px; padding-right: 6px">GL-UHR24-77</span> Im Warenkorb eintragen!  *Gilt nicht f&uuml;r reduzierte Artikel
			</p>
	{/if}
	{/block}