{extends file="{$parent_template_path}/productlist/item_slider.tpl"}

                {block name='productlist-item-slider-caption-short-desc'}
                    <span class="text-clamp-2 d-block">
                        {if isset($showPartsList) && $showPartsList === true && isset($Artikel->fAnzahl_stueckliste)}
                            {block name='productlist-item-slider-caption-bundle'}
                                {$Artikel->fAnzahl_stueckliste}x
                            {/block}
                        {/if}
			<p class="artikelslider_hersteller">{$Artikel->cHersteller}</p>
			<div {if $tplscope !== 'box'}itemprop="name"{/if} class="artikelslider_bezeichnung">{$Artikel->cHersteller} &raquo;{$Artikel->cKurzbezeichnung}, {$Artikel->cHAN}&laquo;</div>
                    </span>
                {/block}
