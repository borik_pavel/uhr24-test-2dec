{extends file="{$parent_template_path}/productlist/header.tpl"}
	    
	    {block name='productlist-header-description-category'}
		<div class="desc clearfix mb-5 mehr-lesen">
		    <p>{$oNavigationsinfo->getCategory()->cBeschreibung}</p>
		    <p>&nbsp;</p>
		    <p class="read-more"><a href="#" class="mehr-text">mehr lesen...</a></p>
		</div>
	    {/block}