{extends file="{$parent_template_path}/layout/header.tpl"}

        {block name='layout-header-head-meta' prepend}
			<meta name="google-site-verification" content="8rt4HJIb5HEg9IF4xBBvojKwVEgIgO4fRPnQnyuM7yA" />
	{/block}

	{block name='layout-header-header' prepend}
		<ul id="header-top-leiste"> 
			<li>&#10004 Versandkostenfrei in DE ab 40&nbsp;&#8364;</li>
			<li>&#10004 5% Vorteilskauf bei Vorkasse</li>
			{if !$isMobile}
				<li>&#10004 Expresslieferung/Samstag m&ouml;glich</li>
				<li>&#10004 30 Tage R&uuml;ckgaberecht</li>
			{/if}
		</ul>
	{/block}

	{block name='layout-header-content-wrapper-starttag' append}
		
			<p id="header-bottom-leiste">
			{if !$isMobile}Sichern Sie sich {/if}3%{if !$isMobile}! Ihr pers&ouml;nlicher{/if} Gutschein-Code{if !$isMobile} ist{/if}: <span id="header-bottom-leiste-code">GL-UHR24-77</span>{if !$isMobile} Im Warenkorb eintragen!  *Gilt nicht f&uuml;r reduzierte Artikel{/if}
			</p>
		

			<div id="menue-start" style="display: none">
			<ul id="menue-liste">
				<a href="/uhren/damenuhren"><li class="menue-liste">Damenuhren<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/herrenuhren"><li class="menue-liste">Herrenuhren<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/kinderuhren"><li class="menue-liste">Kinderuhren<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/schmuck/ringe/damenringe"><li class="menue-liste">Damenringe<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/smartwatches"><li class="menue-liste">Smartwatches<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/grossuhren/wecker"><li class="menue-liste">Wecker<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/zubehoer/uhrenarmbaender"><li class="menue-liste">Uhrenarmb&auml;nder<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
			</ul>
			</div>

	{/block}
