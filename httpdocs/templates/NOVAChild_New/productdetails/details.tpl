{extends file="{$parent_template_path}/productdetails/details.tpl"}

		{block name='productdetails-details-info-product-title'}
		    {opcMountPoint id='opc_before_headline'}
		    <h1 class="product-title h2" itemprop="name">{$Artikel->cHersteller}<br>{$Artikel->cName}<br><span style="font-size: 80%" itemprop="sku">{$Artikel->cHAN}</span></h1>
		{/block}

		{block name='productdetails-details-info-item-id'}
		    {if isset($Artikel->cArtNr)}
			<li class='product-sku' style="display: none">
			    <span class="font-weight-bold">
				{lang key='sortProductno'}:
			    </span>
			    <span itemprop="sku">{$Artikel->cHAN}</span>
			</li>
		    {/if}
		{/block}


		{block name='productdetails-details-product-info-manufacturer'}
		    <li itemprop="brand" itemscope="true" itemtype="http://schema.org/Organization">
			<span class="font-weight-bold" style="display: none">{lang key='manufacturers'}:</span>
			<a href="{$Artikel->cHerstellerSeo}"{if $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen === 'B'} data-toggle="tooltip" data-placement="left" title="{$Artikel->cHersteller}"{/if} itemprop="url">
			    {if ($Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen === 'B'
				|| $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen === 'BT')
				&& !empty($Artikel->cHerstellerBildURLKlein)}
				{image lazy=true webp=true
				    src=$Artikel->cHerstellerBildURLKlein
				    alt=$Artikel->cHersteller
				}
				<meta itemprop="image" content="{$Artikel->cHerstellerBildURLKlein}">
			    {/if}
			    {if $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen !== 'B'}
				<span itemprop="name">{$Artikel->cHersteller}</span>
			    {/if}
			</a>
		    </li>
		{/block}


		{block name='productdetails-details-info-description'}
			    {opcMountPoint id='opc_before_short_desc'}
			    <div class="shortdesc mb-2 d-none d-md-block" itemprop="description">
				{$Artikel->cKurzBeschreibung}
			    </div>
               {/block}
