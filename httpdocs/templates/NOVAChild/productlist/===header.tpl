{extends file="{$parent_template_path}/productlist/header.tpl"}
	    
		
{*  Entfernen des h1 Titel bei vorhandenem h1 in der Beschreibung  *}
				 
	{if $AktuelleKategorie->cBeschreibung|strstr:h1}	 
		{block name='productlist-header-description-heading'}   
		{/block}
	{/if}
	
	
				 

{*  mehr lesen nur anzeigen bei mehr als 701 Zeichen  *}
				 
	{if $AktuelleKategorie->cBeschreibung|count_characters > 751}
		
	    {block name='productlist-header-description-category'}
		
		<div class="desc clearfix mb-5 mehr-lesen">
		    <p>{$oNavigationsinfo->getCategory()->cBeschreibung}</p>
		    <p>&nbsp;</p>
		    <p class="read-more"><a href="#" class="mehr-text">mehr lesen...</a></p>
		</div>
		
	    {/block}
		
	{/if}
	
	
{*  Belboon Code  *}
	
	  {block name='productlist-header-description' append}
	  
{literal}
	<script type="text/javascript">
    belboonTag = {};
</script>

<script type="text/javascript">    
    (function(d) {
      setTimeout( function() {  // Borik 16 november setTimeout added
        var s = d.createElement("script"); 
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + 
"//containertags.belboon.com/js/79749/category/script.js";
        var a = d.getElementsByTagName("script")[0];
        a.parentNode.insertBefore(s, a);
            }, 5000); // Borik 16 november setTimeout added END 
    }(document));    
</script>
{/literal}
	
	  {/block}

{*  Unterkategorie - als Button  *}
	
            {block name='productlist-header-subcategories-link'}
                    <div class="caption">
                        {link class="btn btn-outline-secondary sp20up-btn-subcategory" href=$subCategory->getURL()}
                            {$subCategory->getName()}
                        {/link}
                    </div>
            {/block}

{*  Unterunterkategorie - als Button  *}
{* Unter unterkategorien werder nicht mehr bei Herrenuhren und Damenuhren angezeigt *}	

	{block name='productlist-header-subcategories-list'}
		{if !$AktuelleKategorie->cKategoriePfad|strstr:enuhren}
			<hr class="d-none d-md-block">
			<ul class="d-none d-md-block">
			{foreach $subCategory->getChildren() as $subChild}
				<li>
			{link class="btn sp20up-btn-subsubcategory" href=$subChild->getURL() title=$subChild->getName()}{$subChild->getName()}{/link}
			</li>
			{/foreach}
			</ul>
		{/if}	
{/block}
		