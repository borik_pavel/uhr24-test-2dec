{extends file="{$parent_template_path}/layout/footer.tpl"}


{block name='layout-footer'}




    {block name='layout-footer-content-all-closingtags'}

        {block name='layout-footer-aside'}
            {has_boxes position='left' assign='hasLeftBox'}

            {if $smarty.const.PAGE_ARTIKELLISTE === $nSeitenTyp
            && !$bExclusive
            && $hasLeftBox
            && !empty($boxes.left|strip_tags|trim)}
                {block name='layout-footer-content-productlist-col-closingtag'}
                    </div>{* /col *}
                {/block}
                {block name='layout-footer-sidepanel-left'}
                    <aside id="sidepanel_left" class="sidepanel-left d-print-none col-12 col-lg-4 col-xl-3 order-lg-0 dropdown-full-width">
                        {block name='footer-sidepanel-left-content'}{$boxes.left}{/block}
                    </aside>
                {/block}
                {block name='layout-footer-content-productlist-row-closingtag'}
                    </div>{* /row *}
                {/block}
            {/if}
        {/block}

        {block name='layout-footer-content-closingtag'}
            {opcMountPoint id='opc_content' title='Default Area' inContainer=false}
            </div>{* /content *}
        {/block}

        {block name='layout-footer-content-wrapper-closingtag'}
            </div>{* /content-wrapper*}
        {/block}
    {/block}

    {block name='layout-footer-main-wrapper-closingtag'}
        </main> {* /mainwrapper *}
    {/block}





{block name='layout-footer-content' prepend}
	
	<div id="container_logoblock">
		<ul id="logoblock">
			<li id="armani_exchange"><a href="/marken/armani-exchange" title="Armani Exchange Uhren - klare Formen, exakte Linien"></a></li>
			<li id="bering"><a href="/marken/bering" title="Bering Uhren - inspiriert von der Sch&ouml;nheit der Arktis"></a></li>
			<li id="hugo_boss"><a href="/marken/hugo-boss" title="BOSS Uhren"></a></li>
			<li id="bruno_soehnle"><a href="/marken/bruno-soehnle" title="Bruno S&ouml;hnle Uhren"></a></li>
			<li id="cai"><a href="/marken/cai" title="cai Schmuck"></a></li>
			<li id="calvin_klein"><a href="/marken/calvin-klein" title="CALVIN KLEIN Schmuck und CALVIN KLEIN Uhren"></a></li>
			<li id="cluse"><a href="/marken/cluse" title="Cluse Damenuhren und Uhrenarmb&auml;nder"></a></li>
			<li id="daniel_wellington"><a href="/marken/daniel-wellington" title="Daniel Wellington Uhren"></a></li>
			<li id="davidoff"><a href="/marken/davidoff" title="Davidoff Uhren und Accessoires"></a></li>
			<li id="diesel"><a href="/marken/diesel" title="Diesel Uhren und Diesel Schmuck"></a></li>
			<li id="diesel_on"><a href="/marken/diesel/uhren/smartwatches" title="Diesel On Smartwatches"></a></li>
			<li id="dugena"><a href="/marken/dugena" title="Dugena Uhren"></a></li>
			<li id="emporio_armani"><a href="/marken/emporio-armani" title="Emporio Armani Uhren und Schmuck"></a></li>
			<li id="emporio_armani_connected"><a href="/marken/emporio-armani/uhren/smartwatches" title="Emporio Armani Connected Smartwatches"></a></li>
			<li id="flik_flak"><a href="/marken/flik-flak" title="Flik Flak Uhren"></a></li>
			<li id="filius"><a href="/marken/filius" title="Filius Zeitdesign, Wecker, Gro?uhren"></a></li>
			<li id="fossil"><a href="/marken/fossil" title="Fossil Uhren und Fossil Schmuck"></a></li>
			<li id="fossil_q"><a href="/marken/fossil/uhren/smartwatches" title="Fossil Q Smartwatches"></a></li>
			<li id="garmin"><a href="/marken/garmin" title="Garmin Smartwatches u. Sportuhren"></a></li>
			<li id="michel_herbelin"><a href="/marken/michel-herbelin" title="Michel Herbelin Uhren"></a></li>
			<li id="hirsch"><a href="/marken/hirsch" title="Hirsch Uhrenarmb&auml;nder"></a></li>
			<li id="ice_watch"><a href="/marken/ice-watch" title="Ice Watch Uhren"></a></li>
			<li id="junghans"><a href="/marken/junghans" title="Junghans Uhren"></a></li>
			<li id="luminox"><a href="/marken/luminox" title="Luminox Uhren"></a></li>
			<li id="michael_kors"><a href="/marken/michael-kors" title="Michael Kors Uhren"></a></li>
			<li id="michael_kors_access"><a href="/marken/michael-kors/uhren/smartwatches" title="Michael Kors Access Smartwatches f&uuml;r Damen"></a></li>
			<li id="mido"><a href="/marken/mido" title="Mido Uhren"></a></li>
			<li id="mondaine"><a href="/marken/mondaine" title="Mondaine Uhren"></a></li>
			<li id="pippi_langstrumpf"><a href="/marken/pippi-langstrumpf" title="Pippi Langstrumpf Kinderuhren"></a></li>
			<li id="police"><a href="/marken/police" title="Police Uhren und Police Schmuck"></a></li>
			<li id="regent"><a href="/marken/regent" title="Regent Uhren, Herrenuhren, Damenuhren"></a></li>
			<li id="skagen"><a href="/marken/skagen" title="Skagen Uhren"></a></li>
			<li id="skagen_connected"><a href="/marken/skagen/uhren/smartwatches" title="Skagen Connected Smartwatches"></a></li>
			<li id="s_oliver"><a href="/marken/s-oliver" title="s.Oliver Schmuck f&uuml;r jeden Tag"></a></li>
			<li id="swatch"><a href="/marken/swatch" title="Swatch Uhren"></a></li>
			<li id="tissot"><a href="/marken/tissot" title="Tissot Uhren"></a></li>
			<li id="tommy_hilfiger"><a href="/marken/tommy-hilfiger" title="Tommy Hilfiger Uhren und Schmuck"></a></li>
			<li id="withings"><a href="/marken/withings" title="Withings Smartwatches"></a></li>
			<li id="armani_exchange"><a href="/marken/armani-exchange" title="Armani Exchange Uhren - klare Formen, exakte Linien"></a></li>
			<li id="bering"><a href="/marken/bering" title="Bering Uhren - inspiriert von der Sch&ouml;nheit der Arktis"></a></li>
		</ul>
	</div>

			
{*****
AWIN AND BING
						
{if $smarty.server.REQUEST_URI|strstr:estellabschluss }
								   
					   
	<script>
            window.uetq = window.uetq || [];
            window.uetq.push('event', '', {literal}{{/literal}"revenue_value":{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"},"currency“:“EUR“{literal}}{/literal});
	</script>
	
	
	{literal}
	<!-- Event snippet for Kauf conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-306606323/hmPWCLXL-vECEPPhmZIB',
      'value': {/literal}{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}{literal},
      'currency': 'EUR',
      'transaction_id': ''		 
															 
					 
  });
</script>
	
{/literal}		

{/if}

	
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5217521"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
<noscript><img src="//bat.bing.com/action/0?ti=5217521&Ver=2" height="0" width="0" style="display:none; visibility:hidden;" /></noscript>
***}

<script defer="defer" src="https://www.dwin1.com/13641.js" type="text/javascript"></script>
	
	
<noscript><img src="//bat.bing.com/action/0?ti=5217521&Ver=2" height="0" width="0" style="display:none; visibility:hidden;" /></noscript>

												 
								   

{literal}	
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"12102052"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
{/literal}
	
	
	
	
	{/block} <!-- Block layout-footer-content' prepend -->








    {block name='layout-footer-content'}
        {if !$bExclusive}
            {$newsletterActive = $Einstellungen.template.footer.newsletter_footer === 'Y'
                && $Einstellungen.newsletter.newsletter_active === 'Y'}
                
               
                <div class="addfoot">Erhalten Sie 5% Rabatt</div>
                 <footer id="footer" {if $newsletterActive}class="newsletter-active"{/if}>
               
                <div class="nfoot d-print-none">
                   
                    {block name='layout-footer-boxes'}
                        {getBoxesByPosition position='bottom' assign='footerBoxes'}
                        {if isset($footerBoxes) && count($footerBoxes) > 0}
                            {row id='footer-boxes'}
                                {foreach $footerBoxes as $box}
                                {if $box@first || $box@last}{$we=3}{else}{$we=2}{/if}
                                {if $box@last}{$iamgod=12}{else}{$iamgod=6}{/if}
                                    {col cols=12 sm=$iamgod md=4 lg=$we}
                                        {$box->getRenderedContent()}
                                    {/col}
                                {/foreach}
                            {/row}
                        {/if}
                    {/block}
                    
                    
                     {if $newsletterActive}
                        {block name='layout-footer-newsletter'}
                            {row class="newsletter-footer"}
                                {col cols=12 lg=5}
                                   
                                    {if isset($oSpezialseiten_arr[$smarty.const.LINKTYP_DATENSCHUTZ])}
                                    {block name='layout-footer-newsletter-info'}
                                        <p class="info">
                                            {lang key='newsletterInformedConsent' section='newsletter' printf=$oSpezialseiten_arr[$smarty.const.LINKTYP_DATENSCHUTZ]->getURL()}
                                        </p>
                                    {/block}
                                    {/if}
                                {/col}
                                {col cols=12 lg=6}
                                    {block name='layout-footer-form'}
                                        {form methopd="post" action="{get_static_route id='newsletter.php'}"}
                                            {block name='layout-footer-form-content'}
                                                {input type="hidden" name="abonnieren" value="2"}
                                                {formgroup label-sr-only="{lang key='emailadress'}" class="newsletter-email-wrapper"}
                                                    {inputgroup}
                                                        {input type="email" name="cEmail" id="newsletter_email" placeholder="{lang key='emailadress'}" aria=['label' => {lang key='emailadress'}]}
                                                        {inputgroupaddon append=true}
                                                            {button type='submit' variant='secondary' class=''}
                                                               <i class="fal fas fa-paper-plane"></i>
                                                            {/button}
                                                        {/inputgroupaddon}
                                                    {/inputgroup}
                                                {/formgroup}
                                            {/block}
                                            {block name='layout-footer-form-captcha'}
                                                <div class="{if !empty($plausiArr.captcha) && $plausiArr.captcha === true} has-error{/if}">
                                                    {captchaMarkup getBody=true}
                                                </div>
                                            {/block}
                                        {/form}
                                    {/block}
                                {/col}
                            {/row}
                           
                        {/block}
                    {/if}
                    

                    {block name='layout-footer-additional'}
                        {if $Einstellungen.template.footer.socialmedia_footer === 'Y'}
                            {row class="footer-social-media"}
                                {block name='layout-footer-socialmedia'}
                                    {col cols=12 class="footer-additional-wrapper col-auto mx-auto"}
                                    
                                    <div class="footnote-vat">
                        {if $NettoPreise == 1}
                            {lang key='footnoteExclusiveVat' assign='footnoteVat'}
                        {else}
                            {lang key='footnoteInclusiveVat' assign='footnoteVat'}
                        {/if}
                        {if isset($oSpezialseiten_arr[$smarty.const.LINKTYP_VERSAND])}
                            {if $Einstellungen.global.global_versandhinweis === 'zzgl'}
                                {lang key='footnoteExclusiveShipping' printf=$oSpezialseiten_arr[$smarty.const.LINKTYP_VERSAND]->getURL() assign='footnoteShipping'}
                            {elseif $Einstellungen.global.global_versandhinweis === 'inkl'}
                                {lang key='footnoteInclusiveShipping' printf=$oSpezialseiten_arr[$smarty.const.LINKTYP_VERSAND]->getURL() assign='footnoteShipping'}
                            {/if}
                        {/if}
                     {*   {block name='footer-vat-notice'}
                            <span class="small">* {$footnoteVat}{if isset($footnoteShipping)}{$footnoteShipping}{/if}</span>
                        {/block} *}
                    </div>
                                    
                                    
                                 
                                    {/col}
                                {/block}
                            {/row}{* /row footer-additional *}
                        {/if}
                    {/block}{* /footer-additional *}
                    
                  </div>
             
              <!-- BEGIN Now copyright contains old copyright panel. Oborik 29 oktober -->
              {block name='layout-footer-copyright'}
              
     {* include file='layout/copyright.tpl' *}  <!-- includel old panel -->
          
        
           
              <div id="newcopyright"> 
                {if !empty($meta_copyright)}
            <div class="icon-mr-2" itemprop="copyrightHolder">&copy; {$meta_copyright}</div>
                 {/if}
                 <div class="small">* {$footnoteVat}{if isset($footnoteShipping)}{$footnoteShipping}{/if}</div>
                 <div class="ready-for-loader">
                        <ul class="list-unstyled socialuns">
                                        {if !empty($Einstellungen.template.footer.facebook)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.facebook|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.facebook}"
                                                    class="sociala btn-facebook btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Facebook'}"] title="Facebook" target="_blank" rel="noopener"}
                                                    <span class="fab fa-facebook-f fa-fw fa-lg"></span>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.twitter)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.twitter|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.twitter}"
                                                    class="sociala btn-twitter btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Twitter'}"] title="Twitter" target="_blank" rel="noopener"}
                                                    <i class="fab fa-twitter fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.youtube)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.youtube|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.youtube}"
                                                    class="sociala btn-youtube btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='YouTube'}"] title="YouTube" target="_blank" rel="noopener"}
                                                    <i class="fab fa-youtube fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.vimeo)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.vimeo|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.vimeo}"
                                                    class="sociala btn-vimeo btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Vimeo'}"]  title="Vimeo" target="_blank" rel="noopener"}
                                                    <i class="fab fa-vimeo-v fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.pinterest)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.pinterest|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.pinterest}"
                                                    class="sociala btn-pinterest btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Pinterest'}"]  title="Pinterest" target="_blank" rel="noopener"}
                                                    <i class="fab fa-pinterest-p fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.instagram)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.instagram|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.instagram}"
                                                    class="sociala btn-instagram btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Instagram'}"]  title="Instagram" target="_blank" rel="noopener"}
                                                    <i class="fab fa-instagram fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.skype)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.skype|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.skype}"
                                                    class="sociala btn-skype btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Skype'}"]  title="Skype" target="_blank" rel="noopener"}
                                                    <i class="fab fa-skype fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.xing)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.xing|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.xing}"
                                                    class="sociala btn-xing btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Xing'}"]  title="Xing" target="_blank" rel="noopener"}
                                                    <i class="fab fa-xing fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        {if !empty($Einstellungen.template.footer.linkedin)}
                                            <li>
                                                {link href="{if $Einstellungen.template.footer.linkedin|strpos:'http' !== 0}https://{/if}{$Einstellungen.template.footer.linkedin}"
                                                    class="sociala btn-linkedin btn-sm" aria=['label'=>"{lang key='visit_us_on' section='aria' printf='Linkedin'}"]  title="Linkedin" target="_blank" rel="noopener"}
                                                    <i class="fab fa-linkedin-in fa-fw fa-lg"></i>
                                                {/link}
                                            </li>
                                        {/if}
                                        </ul>
                                        </div>
              </div>
          
                {/block}
            <!-- END Now copyright contains old copyright panel. Oborik 29 oktober -->
                
                {block name='layout-footer-scroll-top'}
                    {if $Einstellungen.template.theme.button_scroll_top === 'Y'}
                        {include file='snippets/scroll_top.tpl'}
                    {/if}
                {/block}
            </footer>
                    {/if}
    {/block}
    {block name='layout-footer-io-path'}
        <div id="jtl-io-path" data-path="{$ShopURL}" class="d-none"></div>
    {/block}

    {* JavaScripts *}
    {block name='layout-footer-js'}
        {$dbgBarBody}
        {captchaMarkup getBody=false}
    {/block}

    {block name='layout-footer-consent-manager'}
        {if $Einstellungen.consentmanager.consent_manager_active === 'Y' && !$isAjax && $consentItems->isNotEmpty()}
            <input id="consent-manager-show-banner" type="hidden" value="{$Einstellungen.consentmanager.consent_manager_show_banner}">
            {include file='snippets/consent_manager.tpl'}
            {inline_script}
                <script>
                    setTimeout(function() {
                        $('#consent-manager, #consent-settings-btn').removeClass('d-none');
                    }, 100)
                    window.CM = new ConsentManager({
                        version: 1
                    });
                    var trigger = document.querySelectorAll('.trigger')
                    var triggerCall = function(e) {
                        e.preventDefault();
                        let type = e.target.dataset.consent;
                        if (CM.getSettings(type) === false) {
                            CM.openConfirmationModal(type, function() {
                                let data = CM._getLocalData();
                                if (data === null ) {
                                    data = { settings: {} };
                                }
                                data.settings[type] = true;
                                document.dispatchEvent(new CustomEvent('consent.updated', { detail: data.settings }));
                            });
                        }
                    }
                    for(let i = 0; i < trigger.length; ++i) {
                        trigger[i].addEventListener('click', triggerCall)
                    }
                    document.addEventListener('consent.updated', function(e) {
                        $.post('{$ShopURLSSL}/', {
                                'action': 'updateconsent',
                                'jtl_token': '{$smarty.session.jtl_token}',
                                'data': e.detail
                            }
                        );
                    });
                </script>
            {/inline_script}
        {/if}
    {/block}
    
    
 {literal}
    <script> 
  $(function() {

    $(".dropdown-toggle").click(function(e) {
        var cn = $(this).text();

        $('.append_name').html(cn);

    }); 

    $(".nav-offcanvas-title").click(function(e) {
        $('.append_name').text('');

    });


    $(".search_brands").on("keyup", function() {

        var value = $(this).val().toLowerCase();

        $(".row .mi_mobile").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            $('.modal').hide();

        });

    });

{/literal} {if $nSeitenTyp !== $smarty.const.PAGE_BESTELLVORGANG}
    $(document).ready(function() {   // SCRAP and include in header menu

        var cont = '<li class="navbar-nav loaded-from-footer"></li>';
      //  var soc = $('.footer-additional-wrapper').html();
        var soc = $('.ready-for-loader').html();
        var top = $('.top-bar').html();
        var topcont = '<li class=loaded-from-top>';
        var arrow = '<div class="mob_disabled"><i class="fal fa-long-arrow-right" aria-hidden="true"></i>';
        if (soc.length) {

            $('.nav-scrollbar-inner').append(cont);
            $('.loaded-from-footer').append(soc);
            $('.loaded-from-footer').find('.footnote-vat').remove();


        }
        $('.nav-scrollbar-inner').append(topcont);
        $('.loaded-from-top').append(top);
        $('.loaded-from-top').find('a').addClass('nav-link');
        $('.loaded-from-top').find('.nav-link').append(arrow);
        $('.loaded-from-top').find('li').removeClass().addClass('nav-item nav-scrollbar-item');
        $('.loaded-from-top').find('ul').removeClass().addClass('navbar-nav nav-scrollbar-inner mr-auto');


        $('.loaded-from-top').find('.top-circle-label').addClass('circle-label').removeClass('top-circle-label');


    }); // SCRAP END
{/if}{literal}
});


// Sticky header
$(document).ready(function() {
    var leng = 370;
    if ($(window).width() > leng) {

        $(window).scroll(function() {
        // Get content
            var sticky = $('.navbar'),
              //  sr = $('#search').html(),
                tt = $('.sticky').length,
                ss = $('.accountflex').clone(),
               // borik = $('.uhr24logo').clone(),
                 borik = $('.logo-wrapper').clone(),
                scroll = $(window).scrollTop();


            if (scroll >= 145) {

                sticky.addClass('sticky');

                if (tt < 1) {
            
{/literal} {if $nSeitenTyp !== $smarty.const.PAGE_BESTELLVORGANG}
              //      sticky.append(sr);
                    sticky.append(ss);
                {/if}{literal}
                    sticky.prepend(borik);
                }

            } else {

                sticky.removeClass('sticky');
              //  sticky.find('.search-wrapper').remove();
                sticky.find('.accountflex').remove();
                sticky.find('.logo-wrapper').remove();
            }


        });
    }

});
           
     </script>                
{/literal}
    
    </body>
    </html>
{/block}
