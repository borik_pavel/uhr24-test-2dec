     <div id="copyright">
                        {container fluid=true}
                            {row}
                                {assign var=isBrandFree value=JTL\Shop::isBrandfree()}
                                {block name='layout-footer-copyright-copyright'}
                                    {col}
                                        {if !empty($meta_copyright)}
                                            <span class="icon-mr-2" itemprop="copyrightHolder">&copy; {$meta_copyright}</span>
                                        {/if}
                                        {if $Einstellungen.global.global_zaehler_anzeigen === 'Y'}
                                            {lang key='counter'}: {$Besucherzaehler}
                                        {/if}
                                        {if !empty($Einstellungen.global.global_fusszeilehinweis)}
                                            <span class="ml-2">{$Einstellungen.global.global_fusszeilehinweis}</span>
                                        {/if}
                                    {/col}
                                {/block}
                               {* {if !$isBrandFree}
                                    {block name='layout-footer-copyright-brand'}
                                        {col class="col-auto ml-auto-util{if $Einstellungen.template.theme.button_scroll_top === 'Y'} pr-8{/if}" id="system-credits"}
                                            Powered by {link href="https://jtl-url.de/jtlshop" class="text-white text-decoration-underline" title="JTL-Shop" target="_blank" rel="noopener nofollow"}JTL-Shop{/link}
                                        {/col}
                                    {/block}
                                {/if} *}
                            {/row}
                        {/container}
                    </div>