{extends file="{$parent_template_path}/layout/breadcrumb.tpl"}

		{block name='layout-breadcrumb-last-item'}
		    {breadcrumbitem class="last active"
			router-tag-itemprop="url"
			href="{if $oItem->getHasChild() === true}{$oItem->getURLFull()}{/if}"
			title=$oItem->getName()|escape:'html'
			itemprop="itemListElement"
			itemscope=true
			itemtype="http://schema.org/ListItem"
		    }
		    <!-- twenty/up borik 22 november from BLACK SALE layout breadcrumb -->
			<span itemprop="name" {if $oItem->getName() === 'SALE %'}style="color: #e7131e;background-color: black;padding: 3px 6px;border-radius: 3px;"{/if}>
			    {if $oItem->getName() !== null}
				{$Artikel->cHersteller} {$oItem->getName()}
			    {elseif !empty($Suchergebnisse->getSearchTermWrite())}
				{$Suchergebnisse->getSearchTermWrite()}
			    {/if}
			</span>
			<meta itemprop="item" content="{$oItem->getURLFull()}" />
			<meta itemprop="position" content="{$oItem@iteration}" />
		    {/breadcrumbitem}
		{/block}
  