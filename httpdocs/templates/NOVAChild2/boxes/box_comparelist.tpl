{extends file="{$parent_template_path}/boxes/box_comparelist.tpl"}

	{block name='boxes-box-comparelist-products'}
	    {listgroup}
		{foreach $oBox->getProducts() as $oArtikel}
		    {if $oArtikel@iteration > $maxItems}
			{break}
		    {/if}
		    {$id = '"a"'}
		    {listgroupitem data-id=$oArtikel->kArtikel class="border-0"}
			{link href=$oArtikel->cURLDEL class="remove float-right"
			    title="{lang section="comparelist" key="removeFromCompareList"}"
			    data=["name"=>"Vergleichsliste.remove",
				"toggle"=>"product-actions",
				"value"=>"{ldelim}{$id|escape:'html'}:{$oArtikel->kArtikel}{rdelim}"]
			    aria=["label"=>{lang section="comparelist" key="removeFromCompareList"}]
			}
			    <span class="fas fa-times"></span>
			{/link}
			{link href=$oArtikel->cURLFull}
			    {image fluid=true webp=true lazy=true
				src=$oArtikel->Bilder[0]->cURLMini
				srcset="{$oArtikel->Bilder[0]->cURLMini} {$Einstellungen.bilder.bilder_artikel_mini_breite}w,
					{$oArtikel->Bilder[0]->cURLKlein} {$Einstellungen.bilder.bilder_artikel_klein_breite}w,
					{$oArtikel->Bilder[0]->cURLNormal} {$Einstellungen.bilder.bilder_artikel_normal_breite}w"
				sizes="24px"
				alt=$oArtikel->cName|strip_tags|truncate:60|escape:'html' class="img-xs mr-2"
			    }
			    <div class="box_comparelist"><span class="box_comparelist_hersteller">{$oArtikel->cHersteller}</span>
			    {$oArtikel->cName|truncate:22:'...'}<br />
			    {$oArtikel->cHAN}
			    {include file='productdetails/price.tpl' Artikel=$oArtikel tplscope='box'}</div>
			{/link}
		    {/listgroupitem}
		{/foreach}
	    {/listgroup}
	{/block}
