{extends file="{$parent_template_path}/basket/cart_dropdown_label.tpl"}
{block name='basket-cart-dropdown-label'}
    <div class="cart-icon-dropdown pro-flex nav-item dropdown {if $WarenkorbArtikelPositionenanzahl != 0}not-empty{/if}">
        {block name='basket-cart-dropdown-label-link'}
            {link class='nav-link' aria=['expanded' => 'false', 'label' => {lang key='basket'}] data=['toggle' => 'dropdown']}
                {block name='basket-cart-dropdown-label-count'}
                    <i class='far fas fa-shopping-bag cart-icon-dropdown-icon'>
                    
                        {if $WarenkorbArtikelPositionenanzahl >= 1}
                        <span class="fa-sup" title="{$WarenkorbArtikelPositionenanzahl}">
                            {$WarenkorbArtikelPositionenanzahl}
                        </span>
                        {/if}
                        
                    </i>
                    <div class="pro-word">Warenkorb</div>
                {/block}
            {*    {block name='basket-cart-dropdown-labelprice'}
                    <span class="cart-icon-dropdown-price">{$WarensummeLocalized[0]}</span>
                {/block} *}
            {/link}
        {/block}
        {block name='basket-cart-dropdown-label-include-cart-dropdown'}
            {include file='basket/cart_dropdown.tpl'}
        {/block}
    </div>
{/block}
