{extends file="{$parent_template_path}/layout/header_nav_search.tpl"}
{block name='layout-header-nav-search'}
    {block name='layout-header-nav-search-search'}
        <div class="nav-item" id="search">
            <div class="search-wrapper">
                {form action='index.php' method='get'}
                    <div class="form-icon">
                        {inputgroup}
                            {input id="search-header" name="qs" type="text" class="ac_input" placeholder="{lang key='search'}" autocomplete="off" aria=["label"=>"{lang key='search'}"]}
                            {inputgroupaddon append=true}
                                {button type="submit" name='search' variant="secondary" aria=["label"=>{lang key='search'}]}<span class="fal fa-search"></span>{/button}
                            {/inputgroupaddon}
                            <span class="form-clear d-none"><i class="fal fa-times"></i></span>
                        {/inputgroup}
                    </div>
                {/form}
            </div>
        </div>
    {/block}
   {* {block name='layout-header-nav-search-search-dropdown'}
        {if $Einstellungen.template.theme.mobile_search_type === 'dropdown'}
            {navitemdropdown class='search-wrapper-dropdown d-block d-lg-none'
                text='<i id="mobile-search-dropdown" class="fal fa-search"></i>'
                right=true
                no-caret=true
                router-aria=['label'=>{lang key='findProduct'}]}
                <div class="dropdown-body">
                    {include file='snippets/search_form.tpl' id='search-header-desktop'}
                </div>
            {/navitemdropdown}
        {/if}
    {/block} *}
{/block}
