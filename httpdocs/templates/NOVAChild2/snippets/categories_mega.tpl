{extends file="{$parent_template_path}/snippets/categories_mega.tpl"}

{block name='snippets-categories-mega'}
    {strip}
    {if !isset($i)}
        {assign var=i value=0}
    {/if}

    {block name='snippets-categories-mega-categories'}
    {if $Einstellungen.template.megamenu.show_categories !== 'N'
        && ($Einstellungen.global.global_sichtbarkeit != 3 || \JTL\Session\Frontend::getCustomer()->getID() > 0)}
        {get_category_array categoryId=0 assign='categories'}
        {if !empty($categories)}
            {if !isset($activeId)}
                {if $NaviFilter->hasCategory()}
                    {$activeId = $NaviFilter->getCategory()->getValue()}
                {elseif $nSeitenTyp === $smarty.const.PAGE_ARTIKEL && isset($Artikel)}
                    {assign var=activeId value=$Artikel->gibKategorie()}
                {elseif $nSeitenTyp === $smarty.const.PAGE_ARTIKEL && isset($smarty.session.LetzteKategorie)}
                    {$activeId = $smarty.session.LetzteKategorie}
                {else}
                    {$activeId = 0}
                {/if}
            {/if}
            {if !isset($activeParents)
            && ($nSeitenTyp === $smarty.const.PAGE_ARTIKEL || $nSeitenTyp === $smarty.const.PAGE_ARTIKELLISTE)}
                {get_category_parents categoryId=$activeId assign='activeParents'}
            {/if}
            {block name='snippets-categories-mega-categories-inner'}
            
            {$home = '<i class="fal fa-home-lg-alt"></i>'}
            {$marken = '<svg width="28" height="25" viewBox="0 0 28 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M15.8558 7.34307L13.0273 4.51465L10.1989 7.34307L13.0273 10.1715L15.8558 7.34307Z" stroke="#333333" stroke-linecap="square"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M17.976 9.46387L20.8044 12.2923L17.976 15.1207L15.1475 12.2923L17.976 9.46387Z" stroke="#333333" stroke-linecap="square"/>
<path d="M10.9056 12.2923L8.07715 9.46387L5.24872 12.2923L8.07715 15.1207L10.9056 12.2923Z" stroke="#333333" stroke-linecap="square"/>
<path d="M15.8558 17.2425L13.0273 14.4141L10.1989 17.2425L13.0273 20.0709L15.8558 17.2425Z" stroke="#333333" stroke-linecap="square"/>
</svg>
'}
             {$uhren = '<svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M8.36035 6.46508V1.66675H12.527V6.46508" stroke="#333333" stroke-linecap="round"/>
<path d="M12.527 12.7014V18.3331H8.36035V12.7014" stroke="#333333" stroke-linecap="round"/>
<path d="M10.4443 13.3333C12.5154 13.3333 14.1943 11.6543 14.1943 9.58325C14.1943 7.51218 12.5154 5.83325 10.4443 5.83325C8.37327 5.83325 6.69434 7.51218 6.69434 9.58325C6.69434 11.6543 8.37327 13.3333 10.4443 13.3333Z" stroke="#333333" stroke-linecap="square"/>
</svg>'}
              {$schmuk = '<i class="far fa-gem"></i>'}
            
              {$zubehor = '<svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.7773 2.25V12.75H2.27734" stroke="#333333" stroke-linecap="square"/>
<path d="M5.27734 15.75V5.25H15.7773" stroke="#333333" stroke-linecap="square"/>
</svg>'}
              {$accessiores = '<svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.6523 6.125H4.40234V16.625H16.6523V6.125Z" stroke="#333333" stroke-linecap="square"/>
<path d="M7.02734 6.125C7.02734 5.19674 7.39609 4.3065 8.05247 3.65013C8.70885 2.99375 9.59909 2.625 10.5273 2.625C11.4556 2.625 12.3458 2.99375 13.0022 3.65013C13.6586 4.3065 14.0273 5.19674 14.0273 6.125" stroke="#333333" stroke-linecap="square"/>
</svg>'}
               
               {$sale = '<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.439 9.23327C16.3666 9.08508 16.3666 8.91514 16.439 8.76699L17.1104 7.39361C17.4842 6.62892 17.188 5.7175 16.4362 5.31858L15.0858 4.60209C14.9401 4.52482 14.8402 4.38729 14.8117 4.2249L14.5476 2.71919C14.4006 1.88086 13.6251 1.31751 12.7825 1.43673L11.2689 1.65083C11.1056 1.67389 10.944 1.62137 10.8255 1.50672L9.72683 0.443879C9.11508 -0.147942 8.15672 -0.147977 7.545 0.443879L6.44629 1.50683C6.32775 1.62151 6.1662 1.67393 6.00287 1.65094L4.48928 1.43683C3.64641 1.31755 2.87121 1.88096 2.72416 2.7193L2.46006 4.22494C2.43155 4.38736 2.33167 4.52486 2.18602 4.60217L0.835665 5.31865C0.0838129 5.71753 -0.212344 6.62903 0.161438 7.39372L0.832747 8.76706C0.905169 8.91524 0.905169 9.08519 0.832747 9.23334L0.161403 10.6067C-0.212379 11.3714 0.0837776 12.2828 0.83563 12.6817L2.18598 13.3982C2.33167 13.4755 2.43155 13.613 2.46006 13.7754L2.72416 15.2811C2.85803 16.0443 3.51243 16.5795 4.26453 16.5794C4.3386 16.5794 4.41377 16.5742 4.48932 16.5635L6.0029 16.3494C6.16613 16.3263 6.32778 16.3789 6.44633 16.4935L7.545 17.5564C7.85093 17.8523 8.24334 18.0003 8.6359 18.0002C9.02835 18.0002 9.42101 17.8523 9.7268 17.5564L10.8255 16.4935C10.944 16.3789 11.1056 16.3265 11.2689 16.3494L12.7825 16.5635C13.6255 16.6828 14.4006 16.1194 14.5476 15.2811L14.8118 13.7754C14.8403 13.613 14.9402 13.4755 15.0858 13.3982L16.4362 12.6817C17.188 12.2828 17.4842 11.3713 17.1104 10.6066L16.439 9.23327ZM6.55953 4.32837C7.60902 4.32837 8.46286 5.18221 8.46286 6.23169C8.46286 7.28118 7.60902 8.13502 6.55953 8.13502C5.51005 8.13502 4.65621 7.28118 4.65621 6.23169C4.65621 5.18221 5.51005 4.32837 6.55953 4.32837ZM5.57712 12.7929C5.47577 12.8943 5.34291 12.945 5.21009 12.945C5.07727 12.945 4.94438 12.8943 4.84306 12.7929C4.64035 12.5902 4.64035 12.2616 4.84306 12.0588L11.6946 5.20727C11.8973 5.00456 12.226 5.00456 12.4287 5.20727C12.6314 5.40999 12.6314 5.73866 12.4287 5.94137L5.57712 12.7929ZM10.7122 13.6719C9.66271 13.6719 8.80887 12.8181 8.80887 11.7686C8.80887 10.7191 9.66271 9.86527 10.7122 9.86527C11.7617 9.86527 12.6155 10.7191 12.6155 11.7686C12.6155 12.8181 11.7617 13.6719 10.7122 13.6719Z" fill="#333333"/>
</svg>'}
<li class="home nav-item nav-scrollbar-item dropdown dropdown-full">
<div class="circle-label">{$home}</div>
<a href="/" title="Home Page" class="nav-link dropdown-toggle" target="_self" aria-expanded="false">
    <span class="nav-mobile-heading">Home</span>
</a>
</li>
            {foreach $categories as $category}
           
            {$firstChild=true}{$i=$i + 1} 
                {if isset($activeParents) && is_array($activeParents) && isset($activeParents[$i])}
                    {assign var=activeParent value=$activeParents[$i]}
                {/if}
                {if $category->isOrphaned() === false}
                    {if $category->hasChildren()}
                        {block name='snippets-categories-mega-category-child'}
                     
                            <li class="nav-item nav-scrollbar-item dropdown dropdown-full{if $category->getID() === $activeId
                            || ((isset($activeParent)
                                && isset($activeParent->kKategorie))
                                && $activeParent->kKategorie == $category->getID())} active{/if}">
                              
                 {$r = $category->getName()|lower|truncate:3:""}          
                                <div class="circle-label">
            {if $r == 'mar'}
            {$marken}
            {elseif $r == 'uhr'}
            {$uhren}
            {elseif $r == 'sch'}
            {$schmuk}
            {elseif $r == 'zub'}
            {$zubehor}
            {elseif $r == 'acc'}
            {$accessiores}
            {elseif $r == 'sal'}
            {$sale}
            {/if}
                                </div>
                                
                                
  {link href=$category->getURL() title=$category->getName() class="nav-link dropdown-toggle" target="_self"}

                                <span class="nav-mobile-heading">{$category->getName()}</span> 
                                <div class="mob_disabled"><i class="fal fa-long-arrow-right"></i></div>
                                {/link}
                                <div class="dropdown-menu transpar">
                                               {if $i==1}   <div class="filter_brands">
                                        <input type="text" class="search_brands" placeholder="Search">
                                        </div>{/if}
                                    <div class="dropdown-body goodback {if $i!=1}short{/if}">
                                  {if $i!=1}  <h2 class="cath">Kategorien</h2>{/if}
                                   
                                        {container class="subcategory-wrapper"}
                                            {row class="lg-row-lg nav{if $i!=1} short_sub{/if}"}
                                                
                                                {block name='snippets-categories-mega-sub-categories'}
                                                    {if $category->hasChildren()}
                                                        {if !empty($category->getChildren())}
                                                            {assign var=sub_categories value=$category->getChildren()}
                                                        {else}
                                                            {get_category_array categoryId=$category->getID() assign='sub_categories'}
                                                        {/if}
                                       
                                                        
                                                        {foreach $sub_categories as $sub}
                                                                {if $i!=1}{$lg =5}{$xl =5}{else}{$lg =4}{$xl =3}{/if}          
                                                                {col lg={$lg} xl={$xl} class="nav-item-lg-m nav-item  {if $i!=1}short_item{/if} {if $sub->hasChildren()}dropdown{/if}"}
                                                                {block name='snippets-categories-mega-category-child-body-include-categories-mega-recursive'}
                                                                {include file='snippets/categories_mega_recursive.tpl' mainCategory=$sub firstChild=true subCategory=$i}
                                                                {/block}
                                                            {/col}
                                                        {/foreach}
                                                    {/if}
                                                {/block}
                                            {/row}
                                        {/container}
                                    </div>
                                </div>
                            </li>
                        {/block}
                    {else}
                        {block name='snippets-categories-mega-category-no-child'}
                       
                                <li class="nav-item nav-scrollbar-item{if $category->getID() === $activeId} active{/if}">    
                  
                   {$r = $category->getName()|lower|truncate:3:""}          
                                <div class="circle-label">
            {if $r == 'mar'}
            {$marken}
            {elseif $r == 'uhr'}
            {$uhren}
            {elseif $r == 'sch'}
            {$schmuk}
            {elseif $r == 'zub'}
            {$zubehor}
            {elseif $r == 'acc'}
            {$accessiores}
            {elseif $r == 'sal'}
            {$sale}
            {/if}
                                </div>
                   {link href=$category->getURL() title=$category->getName() class="nav-link" target="_self"}   
                   <div class="mob_disabled"><i class="fal fa-long-arrow-right"></i></div>       
                                <span class="text-truncate d-block">{$category->getShortName()}</span>
                   {/link}
                                
                        
                        {/block}
                    {/if}
                {/if}
            {/foreach}
            {/block}
        {/if}
    {/if}
    {/block}{* /megamenu-categories*}

    {block name='snippets-categories-mega-manufacturers'}
    {if $Einstellungen.template.megamenu.show_manufacturers !== 'N'
        && ($Einstellungen.global.global_sichtbarkeit != 3
            || isset($smarty.session.Kunde->kKunde)
            && $smarty.session.Kunde->kKunde != 0)}
        {get_manufacturers assign='manufacturers'}
        {if !empty($manufacturers)}
            {assign var=manufacturerOverview value=null}
            {if isset($oSpezialseiten_arr[$smarty.const.LINKTYP_HERSTELLER])}
                {$manufacturerOverview=$oSpezialseiten_arr[$smarty.const.LINKTYP_HERSTELLER]}
            {/if}
            {block name='snippets-categories-mega-manufacturers-inner'}
                <li class="nav-item nav-scrollbar-item dropdown dropdown-full {if $nSeitenTyp === $smarty.const.PAGE_HERSTELLER}active{/if}">
                   
                   
                   
                    {link href="{if $manufacturerOverview !== null}{$manufacturerOverview->getURL()}{else}#{/if}" title={lang key='manufacturers'} class="nav-link dropdown-toggle" target="_self"}
                      
                    
                        <span class="text-truncate">
                            {if $manufacturerOverview !== null && !empty($manufacturerOverview->getName())}
                                {$manufacturerOverview->getName()}
                            {else}
                                {lang key='manufacturers'}
                            {/if}
                        </span>
                    {/link}
                    <div class="dropdown-menu">
                        <div class="dropdown-body">
                            {container}
                                {row class="lg-row-lg nav"}
                                    {if $manufacturerOverview !== null}
                                        {col lg=4 xl=3 class="nav-item-lg-m nav-item d-lg-none"}
                                            {block name='snippets-categories-mega-manufacturers-header'}
                                                {link href="{$manufacturerOverview->getURL()}" rel="nofollow"}
                                                    <strong class="nav-mobile-heading">
                                                        {if !empty($manufacturerOverview->getName())}
                                                            {$manufacturerTitle = $manufacturerOverview->getName()}
                                                        {else}
                                                            {$manufacturerTitle = {lang key='manufacturers'}}
                                                        {/if}
                                                        {lang key='menuShow' printf=$manufacturerTitle}
                                                    </strong>
                                                {/link}
                                            {/block}
                                        {/col}
                                    {/if}
                                    {foreach $manufacturers as $mft}
                                        {col lg=4 xl=3 class='nav-item-lg-m nav-item'}
                                            {block name='snippets-categories-mega-manufacturers-link'}
                                                {link href=$mft->cURLFull title=$mft->cSeo class='submenu-headline submenu-headline-toplevel nav-link '}
                                                    {if $Einstellungen.template.megamenu.show_manufacturer_images !== 'N'
                                                        && (!$isMobile || $isTablet)}
                                                        {include file='snippets/image.tpl'
                                                            class='submenu-headline-image'
                                                            item=$mft
                                                            square=false
                                                            srcSize='sm'}
                                                    {/if}
                                                    {$mft->getName()}
                                                {/link}
                                            {/block}
                                        {/col}
                                    {/foreach}
                                {/row}
                            {/container}
                        </div>
                    </div>
                </li>
            {/block}
        {/if}
    {/if}
    {/block} {* /megamenu-manufacturers*}
    {if $Einstellungen.template.megamenu.show_pages !== 'N'}
        {block name='snippets-categories-mega-include-linkgroup-list'}
            {include file='snippets/linkgroup_list.tpl' linkgroupIdentifier='megamenu' dropdownSupport=true tplscope='megamenu'}
        {/block}
    {/if} {* /megamenu-pages*}

 {*   {if $isMobile}
        {block name='snippets-categories-mega-top-links-hr'}
            <li class="d-lg-none"><hr></li>
        {/block}
        {if $Einstellungen.global.global_wunschliste_anzeigen === 'Y'}
            {navitem href="{get_static_route id='wunschliste.php'}" class="wl-nav-scrollbar-item nav-scrollbar-item"}
                {lang key='wishlist'}
                {badge id="badge-wl-count" variant="primary" class="product-count"}
                    {if isset($smarty.session.Wunschliste) && !empty($smarty.session.Wunschliste->CWunschlistePos_arr|count)}
                        {$smarty.session.Wunschliste->CWunschlistePos_arr|count}
                    {else}
                        0
                    {/if}
                {/badge}
            {/navitem}
        {/if}
        {if $Einstellungen.vergleichsliste.vergleichsliste_anzeigen === 'Y'}
            {navitem href="{get_static_route id='vergleichsliste.php'}" class="comparelist-nav-scrollbar-item nav-scrollbar-item"}
                {lang key='compare'}
                {badge id="comparelist-badge" variant="primary" class="product-count"}
                    {if !empty($smarty.session.Vergleichsliste->oArtikel_arr)}{$smarty.session.Vergleichsliste->oArtikel_arr|count}{else}0{/if}
                {/badge}
            {/navitem}
        {/if}
        {if $linkgroups->getLinkGroupByTemplate('Kopf') !== null}
        {block name='snippets-categories-mega-top-links'}
            {foreach $linkgroups->getLinkGroupByTemplate('Kopf')->getLinks() as $Link}
                {navitem class="nav-scrollbar-item" active=$Link->getIsActive() href=$Link->getURL() title=$Link->getTitle()}
                    {$Link->getName()}
                {/navitem}
            {/foreach}
        {/block}
        {/if}
        {block name='layout-header-top-bar-user-settings'}
            {block name='layout-header-top-bar-user-settings-currency'}
                {if isset($smarty.session.Waehrungen) && $smarty.session.Waehrungen|@count > 1}
                    <li class="currency-nav-scrollbar-item nav-item nav-scrollbar-item dropdown dropdown-full">
                        {block name='layout-header-top-bar-user-settings-currency-link'}
                            {link id='currency-dropdown' href='#' title={lang key='currency'} class="nav-link dropdown-toggle" target="_self"}
                                {lang key='currency'}
                            {/link}
                        {/block}
                        {block name='layout-header-top-bar-user-settings-currency-body'}
                            <div class="dropdown-menu">
                                <div class="dropdown-body">
                                    {container}
                                        {row class="lg-row-lg nav"}
                                            {col lg=4 xl=3 class="nav-item-lg-m nav-item dropdown d-lg-none"}
                                                {block name='layout-header-top-bar-user-settings-currency-header'}
                                                    <strong class="nav-mobile-heading">{lang key='currency'}</strong>
                                                {/block}
                                            {/col}
                                            {foreach $smarty.session.Waehrungen as $currency}
                                                {col lg=4 xl=3 class='nav-item-lg-m nav-item'}
                                                    {block name='layout-header-top-bar-user-settings-currency-header-items'}
                                                        {dropdownitem href=$currency->getURLFull() rel="nofollow" active=($smarty.session.Waehrung->getName() === $currency->getName())}
                                                            {$currency->getName()}
                                                        {/dropdownitem}
                                                    {/block}
                                                {/col}
                                            {/foreach}
                                        {/row}
                                    {/container}
                                </div>
                            </div>
                        {/block}
                    </li>
                {/if}
            {/block}
        {/block}
    {/if} *}

    {/strip}
{/block}
