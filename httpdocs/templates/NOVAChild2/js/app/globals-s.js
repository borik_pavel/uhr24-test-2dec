
export const breakpoints = {
	xl: 1300,
	lg: 992,
	md: 768,
	sm: 576
}

window.globals = {
	breakpoints: breakpoints
}


const Classes = {
	counter	: 'form-counter'
};

const Data = {
	up		: 'data-count-up',
	down	: 'data-count-down'
};


const proxy = function(fn, ...args) {
	return function() {
		fn.call(this, ...args)
	}
};

const updateCount = function(increase = false) {
	let $input = $(this).parents(`.${Classes.counter}`).find('.form-control');
	let input = $input.get(0);
	let stepDirection = increase ? 'stepUp' : 'stepDown';

	try {
		input[stepDirection]()
		$input.trigger('change')
	} catch(e) {
		let step = input.step > 0 ? parseFloat(input.step) : 1;

		let newValue = increase ? parseFloat(input.value) + step : parseFloat(input.value) - step;

		if(newValue > parseFloat(input.max)) return;
		if(newValue < parseFloat(input.min)) return;

		input.value = (newValue).toFixed($input.data('decimals'));
	}
};

window.initNumberInput = function () {
	$(`[${Data.up}]`).on('click', proxy(updateCount, true));
	$(`[${Data.down}]`).on('click', proxy(updateCount));
	$(`.${Classes.counter} .form-control`).on('blur', function() {
		let min = parseInt(this.min);
		let max = parseInt(this.max);
		let value = parseInt(this.value);

		if(value < min) this.value = min;
		if(value > max) this.value = max;
	})
};




import { debounce, random } from './../helpers.js'

const NAME					= 'navscrollbar'
const VERSION				= '1.0.0'
const UNIQID				= random()
const DATA_KEY				= `jtl.${NAME}`
const EVENT_KEY				= `.${DATA_KEY}#${UNIQID}`
const JQUERY_NO_CONFLICT	= $.fn[NAME]

const Default = {
    classes: {
        scrollbarInner		: 'nav-scrollbar-inner',
        scrollbarItems		: 'nav-scrollbar-item',
        scrollbarArrow		: 'nav-scrollbar-arrow'
    },
    templates: {
        arrowLeft: `
			<div class="left">
				<span class="fas fa-chevron-left"></span>
			</div>
		`,
        arrowRight: `
			<div class="right">
				<span class="fas fa-chevron-right"></span>
			</div>
		`
    },
    showArrowOffset: 0,
    clickTransitionDuration: 500,
    clickScrollLength: 70, // percentage
    enableDrag: false,
}

const Event = {
    CLICK			: `click${EVENT_KEY}`,
    MOUSEDOWN		: `mousedown${EVENT_KEY}`,
    MOUSEMOVE		: `mousemove${EVENT_KEY}`,
    MOUSEUP			: `mouseup${EVENT_KEY}`,
    RESIZE			: `resize${EVENT_KEY}`,
    SCROLL			: `scroll${EVENT_KEY}`
}

let $document	= $(document)
let $window		= $(window)


export default class NavScrollbar {
    constructor(element, config = {}) {
        let self     = this;
        this.element = $(element)
        this.config  = $.extend(true, {}, Default, config)

        this._isDragging   = false
        this._start        = 0
        this._startClientX = 0
        this._distance     = 0
        this._itemWidth    = 0

        this.$scrollBarInner = this.element.find(`.${this.config.classes.scrollbarInner}`)
        this.$scrollBarItems = this.element.find(`.${this.config.classes.scrollbarItems}`)

        this.$scrollBarArrowLeft	= $(this.config.templates.arrowLeft).addClass(`${this.config.classes.scrollbarArrow} disabled`)
        this.$scrollBarArrowRight	= $(this.config.templates.arrowRight).addClass(`${this.config.classes.scrollbarArrow} disabled`)

        this.element.prepend(this.$scrollBarArrowLeft)
        this.element.append(this.$scrollBarArrowRight)

        setTimeout(function () {
            self.update();
            self._bindEvents();
        }, 100);
    }

    /* Public */

    scrollToPrev() {
        this.$scrollBarInner.animate({
            scrollLeft: this.$scrollBarInner.scrollLeft() - (this.$scrollBarInner.width() / 100 * this.config.clickScrollLength - this.$scrollBarArrowLeft.width())
        }, { duration: this.config.clickTransitionDuration })
    }

    scrollToNext() {
        this.$scrollBarInner.animate({
            scrollLeft: this.$scrollBarInner.scrollLeft() + (this.$scrollBarInner.width() / 100 * this.config.clickScrollLength - this.$scrollBarArrowLeft.width())
        }, { duration: this.config.clickTransitionDuration })
    }

    update() {
        this._updateItemWidth()

        let widthDifference = Math.round(this._itemWidth - this.$scrollBarInner.width())
        let scrolledFromLeft = this.$scrollBarInner.scrollLeft()

        if(widthDifference <= 0) {
            this.$scrollBarArrowLeft.addClass('disabled')
            this.$scrollBarArrowRight.addClass('disabled')
            return
        }

        this.$scrollBarArrowLeft[scrolledFromLeft > this.config.showArrowOffset ? 'removeClass' : 'addClass']('disabled')
        this.$scrollBarArrowRight[scrolledFromLeft + this.config.showArrowOffset < (widthDifference - 8) ? 'removeClass' : 'addClass']('disabled')
    }

    destroy() {
        this.element.removeData(DATA_KEY)
        this.element = null
        this.$scrollBarInner.off(EVENT_KEY)
        this.$scrollBarArrowLeft.off(EVENT_KEY).remove()
        this.$scrollBarArrowRight.off(EVENT_KEY).remove()
        $document.off(EVENT_KEY)
        $window.off(EVENT_KEY)
    }

    /* Private */

    _dragStart(e) {
        e.preventDefault()

        this._startClientX = e.clientX

        this._isDragging = true
        this._start = e.clientX + this.$scrollBarInner.scrollLeft()
    }

    _dragMove(e) {
        if(!this._isDragging)
            return

        this.$scrollBarInner.scrollLeft(this._start - e.clientX)
    }

    _dragEnd(e) {
        if(!this._isDragging)
            return

        this._isDragging = false

        if($.contains(this.element.get(0), e.target)) {
            e.target.removeEventListener('click', this._preventDragClick)

            if(this._startClientX - e.clientX != 0)
                e.target.addEventListener('click', this._preventDragClick)
        }

        this.$scrollBarInner.scrollLeft(this._start - e.clientX)
    }

    _preventDragClick(e) {
        e.preventDefault()
        e.stopImmediatePropagation()
    }

    _updateItemWidth() {
        this._itemWidth = 0

        $.each(this.$scrollBarItems, (i, element) => {
            this._itemWidth += Math.round(element.offsetWidth)
        })
    }

    _bindEvents() {
        this.$scrollBarInner.on(Event.SCROLL, debounce(() => this.update()))

        this.$scrollBarArrowLeft.on(Event.CLICK, () => this.scrollToPrev())
        this.$scrollBarArrowRight.on(Event.CLICK, () => this.scrollToNext())

        if(this.config.enableDrag) {
            this.$scrollBarInner.on(Event.MOUSEDOWN, (e) => this._dragStart(e))
            $document.on(Event.MOUSEMOVE, (e) => this._dragMove(e))
            $document.on(Event.MOUSEUP, (e) => this._dragEnd(e))
        }

        $window.on(Event.RESIZE, debounce(() => this.update()))
    }

    /* Static */

    static _jQueryInterface(config = {}) {
        let _arguments = arguments || null

        return this.each(function() {
            const $element	= $(this)
            let data		= $element.data(DATA_KEY)

            if(!data) {
                if(typeof config === 'object') {
                    data = new NavScrollbar(this, config)
                    $element.data(DATA_KEY, data)
                } else {
                    $.error(`cannot call methods on ${NAME} prior to initialization`)
                }
            } else {
                if(typeof data[config] === 'function') {
                    data[config].apply(data, Array.prototype.slice.call(_arguments, 1))
                } else {
                    $.error(`method ${config} does not exist.`)
                }
            }
        })
    }
}

/**
 * ------------------------------------------------------------------------
 * jQuery
 * ------------------------------------------------------------------------
 */

$.fn[NAME]             = NavScrollbar._jQueryInterface
$.fn[NAME].Constructor = NavScrollbar
$.fn[NAME].noConflict  = () => {
    $.fn[NAME] = JQUERY_NO_CONFLICT
    return NavScrollbar._jQueryInterface
}



const Tabdrop = function(element, options) {
	let _this = this;

	this.element = element;
	this.options = options;
	this.tabdrop = $('<li class="nav-item tab-drop d-none">' +
						'<a class="nav-link' + ((this.options.showCaret) ? ' dropdown-toggle' : '') + '" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"></a>' +
						'<div class="dropdown-menu dropdown-menu-right"></div>' +
					'</li>').appendTo(this.element);
	this.collection = [];

	this.layout = function() {
		$.each(this.tabdrop.find('.dropdown-item'), function() {
			_this._swapClasses(this).insertBefore(_this.tabdrop);
		});

		this._checkOffsetAndPush();
	};

	this._checkOffsetAndPush = function(recursion) {
		recursion = recursion || false;

		$(this.element).find('.nav-item:not(.tab-drop), .dropdown-item').each(function() {
			if(this.offsetTop > _this.options.offsetTop) {
				_this.collection.push(_this._swapClasses(this));
			}
		});

		if(this.collection.length > 0) {
			if(!recursion) {
				this.tabdrop.removeClass('d-none');
				this.tabdrop.parent().css('padding-right', this.tabdrop.width());
				this.tabdrop.find('.dropdown-menu').empty();
			}

			this.tabdrop.find('.dropdown-menu').prepend(this.collection);

			if(this.tabdrop.find('.dropdown-menu .active').length == 1) {
				this.tabdrop.find('> .nav-link').addClass('active');
			} else {
				this.tabdrop.find('> .nav-link').removeClass('active');
			}

			this.collection = [];

			this._checkOffsetAndPush(true);
		} else {
			if (!recursion) {
				this.tabdrop.addClass('d-none');
				this.tabdrop.parent().css('padding-right', 0);
			}
		}
	};

	this._swapClasses = function(element) {
		let item;

		if($(element).hasClass('nav-item')) {
			item = $(element).find('.nav-link').removeClass('nav-link').addClass('dropdown-item');
			$(element).detach();
		} else {
			item = $(element).removeClass('dropdown-item').addClass('nav-link');
			item = $('<li />').addClass('nav-item').append(item);
		}

		return item;
	};

	$(window).on('resize', $.proxy(this.layout, this));
	$(this.element).on('shown.bs.tab', $.proxy(this.layout, this));

	this.layout();
};

$.fn.tabdrop = function(option) {
	return this.each(function() {
		let data = $(this).data('tabdrop'),
			options = typeof option === 'object' && option;

		if(!data) {
			options = $.extend({}, $.fn.tabdrop.defaults, options);
			$(this).data('tabdrop', new Tabdrop(this, options));
		}

		if(typeof option == 'string')
			data[option]();
	})
};

$.fn.tabdrop.defaults = {
	offsetTop : 0,
	icon : '',
	showCaret : true
};

$.fn.tabdrop.Constructor = Tabdrop;



/* imports */

import {
    debounce, isMobile, onMobile, onDesktop, hasTouch,
    isDesktop, lockScreen, unlockScreen, backdrop
} from './../helpers.js'

import './../plugins/navscrollbar.js'

/* vars */

const header					= '#jtl-nav-wrapper'
const search					= '#search'
const mainNavigation			= '#mainNavigation'
const navRightDropdowns			= `${header} .nav-right .dropdown`
const navbarNav					= `${mainNavigation} .navbar-nav`
const mobileBackLink			= '[data-menu-back]'

const $document					= $(document)
const $window					= $(window)
const $backdropDropdowns		= backdrop()
const $backdropMobileNav		= backdrop()
const $header					= $(header)
const $mainNavigation			= $(mainNavigation)
const $navRightDropdowns		= $(navRightDropdowns)
const $navbarNav				= $(navbarNav)
const $consentManager           = $('#consent-manager')

const delayDropdownFadeIn		= 400
const delayDropdownFadeOut		= 200

let mobileCurrentLevel			= 0
let dropdownInTo				= null
let dropdownOutTo				= null
let $activeDropdown				= null
let activeDropdowns				= []
let isDropdownActive			= false
let isMenuActive				= false

/* functions */

const hasNavScrollbar = () => $mainNavigation.data('jtl.navscrollbar') !== undefined

const onInitOrResize = () => {
    updateVH()

    if(isMobile()) {
        hideDropdown()
        $backdropDropdowns.removeClass('zindex-dropdown').detach()

        if(hasNavScrollbar())
            $mainNavigation.navscrollbar('destroy')
    } else {
        $mainNavigation.collapse('hide')
        resetMobileNavigation()

        if(!hasNavScrollbar())
            $mainNavigation.navscrollbar()
    }
}

const showDropdown = (dropdown) => {
    $activeDropdown = $(dropdown)
    $activeDropdown.parent().addClass('show')
    $activeDropdown.next().addClass('show')
    $activeDropdown.attr('aria-expanded', true)
    isMenuActive = true

    activeDropdowns.push($activeDropdown)
}

const hideDropdown = () => {
    if($activeDropdown === null)
        return

    $activeDropdown.parent().removeClass('show')
    $activeDropdown.next().removeClass('show')
    $activeDropdown.attr('aria-expanded', false)

    activeDropdowns.splice(-1, 1)

    if(activeDropdowns.length === 0) {
        $activeDropdown = null
        isMenuActive = false
    } else {
        $activeDropdown = activeDropdowns[activeDropdowns.length - 1]
    }
}

const showMobileLevel = (level) => {
    mobileCurrentLevel = level < 0 ? 0 : mobileCurrentLevel
    $navbarNav.css('transform', `translateX(${mobileCurrentLevel * -100}%)`)
    $(`${mainNavigation} .nav-mobile-body`).scrollTop(0)
    updateMenuTitle()
}

const updateMenuTitle = () => {
    if(mobileCurrentLevel === 0) {
        $('span.nav-offcanvas-title').removeClass('d-none')
        $('a.nav-offcanvas-title').addClass('d-none')
    } else {
        $('span.nav-offcanvas-title').addClass('d-none')
        $('a.nav-offcanvas-title').removeClass('d-none')
    }
}

const resetMobileNavigation = () =>Â {
    mobileCurrentLevel = 0
    updateMenuTitle()
    $(`${mainNavigation} .show`).removeClass('show')
    $(`${mainNavigation} .dropdown-toggle`).attr('aria-expanded', false)
    $navbarNav.removeAttr('style')
}

const updateVH = () => {
    let vh = window.innerHeight * .01
    document.documentElement.style.setProperty('--vh', `${vh}px`)
}

onInitOrResize()
updateVH()

/* global events */

$window.on('resize', debounce(onInitOrResize))
$document.on('focus blur', search, () => setTimeout(() => { if(hasNavScrollbar()) $mainNavigation.navscrollbar('update') }, 250))

$document.on('show.bs.dropdown', navRightDropdowns, (e) => {
    isDropdownActive = true
    $backdropDropdowns.insertBefore($header)
})

$document.on('shown.bs.dropdown', navRightDropdowns, () => {
    $backdropDropdowns.addClass('show zindex-dropdown')
})

$document.on('hide.bs.dropdown', navRightDropdowns, () => {
    isDropdownActive = false

    if(!isMenuActive)
        $backdropDropdowns.removeClass('show')
})

$document.on('hidden.bs.dropdown', navRightDropdowns, () => {
    if(!isMenuActive)
        $backdropDropdowns.removeClass('zindex-dropdown').detach()
})

/* mobile events */

$document.on('shown.bs.dropdown', `.search-mobile`, (e) => {
    $(e.currentTarget).find('input').focus()
})

$backdropMobileNav.on('click', onMobile(() => {
    $mainNavigation.collapse('hide')
}))

$document.on('show.bs.collapse', mainNavigation, () => {
    lockScreen()
    $backdropMobileNav.insertBefore($mainNavigation)
    $consentManager.addClass('d-none');
})

$document.on('shown.bs.collapse', mainNavigation, () => {
    $backdropMobileNav.addClass('show')
    $(`${mainNavigation} .nav-mobile-body`).scrollTop(0)
})

$document.on('hide.bs.collapse', mainNavigation, () => {
    $backdropMobileNav.removeClass('show')
    $consentManager.removeClass('d-none');
})

$document.on('hidden.bs.collapse', mainNavigation, () => {
    unlockScreen()
    $backdropMobileNav.detach()
})

$document.on('click', mobileBackLink, onMobile((e) => {
    e.preventDefault()

    showMobileLevel(--mobileCurrentLevel)
    hideDropdown()
}))

$document.on('click', `${mainNavigation} .dropdown-toggle`, onMobile((e) => {
    e.preventDefault()

    showDropdown(e.currentTarget)
    showMobileLevel(++mobileCurrentLevel)
}))

/* desktop events */

if(hasTouch()) {
    $document.on('click', `${mainNavigation} .dropdown-toggle:not(.categories-recursive-link)`, onDesktop((e) => {
        e.preventDefault()

        if($activeDropdown !== null && $activeDropdown.get(0) === e.currentTarget) {
            hideDropdown()
            $backdropDropdowns.removeClass('show').detach()
            return
        }

        if($activeDropdown !== null) {
            hideDropdown()
            $backdropDropdowns.removeClass('show').detach()
        }

        showDropdown(e.currentTarget)
        $backdropDropdowns.insertBefore($header).addClass('show zindex-dropdown')
    }))

    $backdropDropdowns.on('click', onDesktop(() => {
        if($activeDropdown !== null) {
            hideDropdown()
            $backdropDropdowns.removeClass('show').detach()
        }
    }))

    $document.on('show.bs.dropdown', navRightDropdowns, () => {
        if($activeDropdown !== null)
            hideDropdown()
    })
}

$document.on('mouseenter', `${mainNavigation} .navbar-nav > .dropdown`, onDesktop((e) => {
    if(hasTouch())
        return

    if(dropdownOutTo != undefined)
        clearTimeout(dropdownOutTo)

    dropdownInTo = setTimeout(() => {
        if($activeDropdown !== null) {
            hideDropdown()
        }
        showDropdown($(e.currentTarget).find('> .dropdown-toggle'))
        $backdropDropdowns.insertBefore($header).addClass('show zindex-dropdown')
    }, delayDropdownFadeIn)
})).on('mouseleave', `${mainNavigation} .navbar-nav > .dropdown`, onDesktop((e) => {
    if(hasTouch())
        return

    if(dropdownInTo != undefined)
        clearTimeout(dropdownInTo)

    dropdownOutTo = setTimeout(() => {
        hideDropdown()

        if(!isDropdownActive)
            $backdropDropdowns.removeClass('show').detach()
    }, delayDropdownFadeOut)
}))






import { isMobile } from './../helpers.js'

const productImages		 = '#gallery img'
const productImagesModal = '#gallery_preview img'
const imageModal		 = '#productImagesModal'

const $document			 = $(document)
const $productImages	 = $(productImages)

$document.on('click', productImages, function () {
    if (isMobile()) {
        $(imageModal).modal('show');
        $(imageModal).on('shown.bs.modal', () => {
            $(imageModal + ' img[data-index="' + $(this).data('index') + '"]')[0].scrollIntoView({
                behavior: 'smooth',
                block: 'center'
            });
        });
    }
});