/*
 * Platz für eigenes Javascript
 * Die hier gemachten Änderungen überschreiben ggfs. andere Funktionen, da diese Datei als letzte geladen wird.
 */

 // JavaScript Document

// DOM Ready
$(function() {

	var $el, $ps, $up, totalHeight;

	$(".mehr-lesen .mehr-text").click(function() {

		// IE 7 doesn't even get this far.

		totalHeight = 0

		$el = $(this);
		$p  = $el.parent();
		$up = $p.parent();
		$ps = $up.find("p:not('.read-more')");

		// measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
		$ps.each(function() {
			totalHeight += $(this).outerHeight()+24;
			// FAIL totalHeight += $(this).css("margin-bottom");
		});

		$up
			.css({
				// Set height to prevent instant jumpdown when max height is removed
				"height": $up.height(),
				"max-height": 9999
			})
			.animate({
				"height": totalHeight
			});

		// fade out read-more
		$p.fadeOut();

		$(".mehr-lesen").click(function() {
		//After expanding, click paragraph to revert to original state
		$p.fadeIn();
			$up.animate({
			"height": 200
			})
		});

		// prevent jump-down
		return false;

	});
});

