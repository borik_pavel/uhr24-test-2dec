{extends file="{$parent_template_path}/page/404.tpl"}

{block name='page-404'}
    <div id="page-not-found">
        {block name='page-404-include-sitemap'}
            {* {include file='page/sitemap.tpl'} *}
        {/block}
    </div>
{/block}
