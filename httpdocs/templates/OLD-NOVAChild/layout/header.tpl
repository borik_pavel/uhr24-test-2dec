{extends file="{$parent_template_path}/layout/header.tpl"}

{*********
Setting all pages that contain "hersteller" to noindex 
*******}
{*********
AND -  insert google verification tag 
*******}

        {block name='layout-header-head-meta'}
		 {literal}
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TVBKXFK');</script>
<!-- End Google Tag Manager -->

 {/literal}
            <meta http-equiv="content-type" content="text/html; charset={$smarty.const.JTL_CHARSET}">
            <meta name="description" itemprop="description" content={block name='layout-header-head-meta-description'}"{$meta_description|truncate:1000:"":true}{/block}">
            {if !empty($meta_keywords)}
                <meta name="keywords" itemprop="keywords" content="{block name='layout-header-head-meta-keywords'}{$meta_keywords|truncate:255:'':true}{/block}">
            {/if}
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            {$noindex = $bNoIndex === true  || (isset($Link) && $Link->getNoFollow() === true)}
			{if $smarty.server.REQUEST_URI|strstr:hersteller }
				<meta name="robots" content="noindex">
			{else}
				<meta name="robots" content="{if $robotsContent}{$robotsContent}{elseif $noindex}noindex{else}index, follow{/if}">
			{/if}
            <meta itemprop="url" content="{$cCanonicalURL}"/>
            {block name='layout-header-head-theme-color'}
                <meta name="theme-color" content="{if $Einstellungen.template.theme.theme_default === 'clear'}#f8bf00{else}#1C1D2C{/if}">
            {/block}
            <meta property="og:type" content="website" />
            <meta property="og:site_name" content="{$meta_title}" />
            <meta property="og:title" content="{$meta_title}" />
            <meta property="og:description" content="{$meta_description|truncate:1000:"":true}" />
            <meta property="og:url" content="{$cCanonicalURL}"/>

            {if $nSeitenTyp === $smarty.const.PAGE_ARTIKEL && !empty($Artikel->Bilder)}
                <meta itemprop="image" content="{$Artikel->Bilder[0]->cURLGross}" />
                <meta property="og:image" content="{$Artikel->Bilder[0]->cURLGross}">
            {elseif $nSeitenTyp === $smarty.const.PAGE_NEWSDETAIL && !empty($newsItem->getPreviewImage())}
                <meta itemprop="image" content="{$imageBaseURL}{$newsItem->getPreviewImage()}" />
                <meta property="og:image" content="{$imageBaseURL}{$newsItem->getPreviewImage()}" />
            {else}
                <meta itemprop="image" content="{$ShopLogoURL}" />
                <meta property="og:image" content="{$ShopLogoURL}" />
            {/if}
				<meta name="google-site-verification" content="8rt4HJIb5HEg9IF4xBBvojKwVEgIgO4fRPnQnyuM7yA" />
				
				{literal}
				<!-- Global site tag (gtag.js) - Google Ads: 306606323 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-306606323"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-306606323');
</script>

		{/literal}		
				
				
        {/block}

 {block name='layout-header-body-tag' append}
 
 <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TVBKXFK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

{/block}
		
		
	{block name='layout-header-header' prepend}
		<ul id="header-top-leiste"> 
			<li>&#10004 Versandkostenfrei in DE ab 40&nbsp;&#8364;</li>
			<li>&#10004 5% Vorteilskauf bei Vorkasse</li>
			{if !$isMobile}
				<li>&#10004 Expresslieferung/Samstag m&ouml;glich</li>
				<li>&#10004 30 Tage R&uuml;ckgaberecht</li>
			{/if}
		</ul>
	{/block}

	{block name='layout-header-content-wrapper-starttag' append}
		
			<p id="header-bottom-leiste">
			
			{if $smarty.server.REQUEST_URI|strstr:Bestellvorgang}
			
				{if $isMobile} {lang key="sp20upUhr24HeaderCheckoutMobile" section="custom"} {else} {lang key="sp20upUhr24HeaderCheckout" section="custom"} {/if}
				
			
			{else}
		
			
			{if !$isMobile}Sichern Sie sich {/if}5%{if !$isMobile}! Ihr pers&ouml;nlicher{/if} Gutschein-Code{if !$isMobile} ist{/if}: <span id="header-bottom-leiste-code">GL-UHR24-77</span>{if !$isMobile} Im Warenkorb eintragen!  *Gilt nicht f&uuml;r reduzierte Artikel{/if}

			{/if}
			</p>
		

			<div id="menue-start" style="display: none">
			<ul id="menue-liste">
				<a href="/uhren/damenuhren"><li class="menue-liste">Damenuhren<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/herrenuhren"><li class="menue-liste">Herrenuhren<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/kinderuhren"><li class="menue-liste">Kinderuhren<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/schmuck/ringe/damenringe"><li class="menue-liste">Damenringe<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/smartwatches"><li class="menue-liste">Smartwatches<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/uhren/grossuhren/wecker"><li class="menue-liste">Wecker<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
				<a href="/zubehoer/uhrenarmbaender"><li class="menue-liste">Uhrenarmb&auml;nder<i class="fas fa-caret-right menue-liste-fa-caret-right"></i></li></a>
			</ul>
			</div>

	{/block}
