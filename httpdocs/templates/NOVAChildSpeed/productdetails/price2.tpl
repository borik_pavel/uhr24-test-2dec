{extends file="{$parent_template_path}/productdetails/price.tpl"}

{block name='productdetails-price-detail-vat-info' prepend}

{block name='productvorkasse' }

	{assign var='preisrabatt' value=100-{lang key='sp20upUhr24VorkasserabattProzent' section='custom'}}
	{assign var='preis' value=$Artikel->Preise->fVKBrutto/100*$preisrabatt}

	<div class="sp20upUhr24Vorkassepreis">{lang key='sp20upUhr24VorkasserabattText' section='custom'} {$preis|string_format:"%.2f"|replace:'.':','} € </div>

{/block}
{/block}