{extends file="{$parent_template_path}/checkout/order_completed.tpl"}

{block name='checkout-order-completed-include-footer' prepend}


<!-- /***** BING CONVERSION TRACKING *****/ -->

{literal}
<script>
window.uetq = window.uetq || [];
window.uetq.push('event', '', {"revenue_value":{/literal}{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}{literal},"currency":"EUR"});
</script>	 {/literal}

	
	{literal}
	<!-- Event snippet for Kauf conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-306606323/hmPWCLXL-vECEPPhmZIB',
      'value': {/literal}{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}{literal},
      'currency': 'EUR',
      'transaction_id': '{/literal}{$Bestellung->cBestellNr}{literal}'
  });
</script>
{/literal}


<script type="text/javascript">
/*** Do not change ***/
var AWIN = {};
AWIN.Tracking = {};
AWIN.Tracking.Sale = {};
/*** Set your transaction parameters ***/
AWIN.Tracking.Sale.amount = "{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}";
AWIN.Tracking.Sale.channel = "aw";
AWIN.Tracking.Sale.orderRef = "{$Bestellung->cBestellNr}";
AWIN.Tracking.Sale.parts = "DEFAULT:{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}";
AWIN.Tracking.Sale.currency = "EUR";
AWIN.Tracking.Sale.voucher = "{foreach $Bestellung->Positionen as $position}{if $position->cName|strstr:"Glücksgutschein"}GL-UHR24-77{/if}{/foreach}";
AWIN.Tracking.Sale.test = "0";
</script>


<img border="0" height="0" src="https://www.awin1.com/sread.img?tt=ns&tv=2&merchant=13641&amount='{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}&ch=aw&parts=DEFAULT:'{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}'&ref='{$Bestellung->cBestellNr}'&cr=EUR&vc='{foreach $Bestellung->Positionen as $position}{if $position->cName|strstr:"Glücksgutschein"}GL-UHR24-77{/if}{/foreach}'&testmode='0'" style="display: none;" width="0">


<!-- BEGIN BELBOON - PERFORMANCE MARKETING NETWORK TRACKING CODE version 4.25 -->
<script type="text/javascript">
var itsConv = {

	// MANDATORY FIELD: This is the product category for this conversion target.
	// Important: The value for this product category needs to be set in your network configuration beforehand.
	// Your possible product categories are: 'default',
    trcCat : 'default',
	// MANDATORY FIELD: This is the name of the conversion target.
	// Important: The value for this conversion target needs to be set in your network configuration beforehand.
    // Your possible conversion targets are: 'sale','lead',
	convTarget : 'sale',

	// MANDATORY FIELD: This is a short description of the conversion page.
	// Examples: Check-Out, Registration Complete, Valentines Day Promotion.
	siteId : 'check-out',

	// MANDATORY FIELD: This the unique conversion identifier from your system. Examples: OrderID, CustomerID, LeadID.
	// If you can't provide a correct unique conversion identifier you can use 'auto' and our system will generate it automatically.
	// This could lead to validation problems due to the fact that an exact matching from your system to ours will not exist.
    convId : '{$Bestellung->cBestellNr}',

    // MANDATORY FIELD: This is the net order value (without shipping and handling costs). Use a value of '0.00' for conversion targets without net order value.
    ordValue : '{$Bestellung->fGesamtsummeNetto|string_format:"%.2f"}',

	// TCFv2.0 parameters
    gdpr : '',         // '0' GDPR does not apply; '1' GDPR applies
    gdprConsent : '',  // Only meaningful if gdpr=1. URL-safe base64-encoded TC string obtained from the CMP JS API or OpenRTB.
                       // Must contain consent for Vendor ID of advertiser or for 871 as default 

	// OPTIONAL FIELDS: These are session tracking parameters
    clickId  : '', // OPTIONAL FIELD. A click ID.
    // MANDATORY FIELD: This is the ISO currency code (ISO 4217).
    // Examples: 'EUR', 'GBP', 'CHF'.
    ordCurr : 'EUR',


	locationHref : window.location.href, // The complete Browser-URL on which the tag was fired. Used to increase accuracy in case of strict referrer browser policy. 

    // DO NOT CHANGE. The following parameters are used to identify the advertiser in the network.
    advId : 'i5036838',
    trcDomain : 'atlas.r.akipam.com'

};

{literal}
// DO NOT CHANGE. The following lines assure tracking functionality.
en=function(v){if(v){if(typeof(encodeURIComponent)=='function'){return(encodeURIComponent(v));}return(escape(v));}};ts=function(){var d=new Date();var t=d.getTime();return(t);};im=function(s){if(document.images){if(typeof(ia)!="object"){
var ia=new Array();};var i=ia.length;ia[i]=new Image();ia[i].src=s;ia[i].onload=function(){};}else{document.write('<img src="'+s+'" height="1" width="1" border="0" alt="" style="display:none;">');}};var pr='https:';
fr=function(s){var d=document;var i=d.createElement("iframe");i.src=s;i.frameBorder=0;i.width=0;i.height=0;i.vspace=0;i.hspace=0;i.marginWidth=0;i.marginHeight=0;i.scrolling="no";i.allowTransparency=true;i.style.display="none";try{d.body.insertBefore(i,d.body.firstChild);}catch(e){
d.write('<ifr'+'ame'+' src="'+s+'" width="0" height="0" frameborder="0" vspace="0" hspace="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="true" style="display:none;"></ifr'+'ame>');}};ap=function(o){var v='tst='+ts();if(o.trcCat){v+='&trc='+en(o.trcCat);}
v+='&ctg='+en(o.convTarget);v+='&cid='+en(o.convId);if(o.ordValue){v+='&orv='+en(o.ordValue);}if(o.ordCurr){v+='&orc='+en(o.ordCurr);}if(o.discValue){v+='&dsv='+en(o.discValue);}if(o.discOrdValue){v+='&ovd='+en(o.discOrdValue);}if(o.discCode){v+='&dsc='+en(o.discCode);}
if(o.invValue){v+='&inv='+en(o.invValue);}if(o.confStat){v+='&cfs='+en(o.confStat);}if(o.admCode){v+='&amc='+en(o.admCode);}if(o.payMethod){v+='&pmt='+en(o.payMethod);}if(o.subCode){v+='&smc='+en(o.subCode);}if(o.userVal1){v+='&uv1='+en(o.userVal1);}if(o.userVal2){v+='&uv2='+en(o.userVal2);}if(o.userVal3){
v+='&uv3='+en(o.userVal3);}if(o.userVal4){v+='&uv4='+en(o.userVal4);}if(o.isCustNew){var n=o.isCustNew.toLowerCase();v+='&csn=';v+=(n=="true"||n=="false")?n:"null";}if(o.custId){v+='&csi='+en(o.custId);}if(o.custGend){var g=o.custGend.toLowerCase();v+='&csg=';
v+=(g=="m"||g=="f")?g:"null";}if(o.custAge){v+='&csa='+en(o.custAge);}if(o.basket){v+='&bsk='+en(o.basket);}if(o.addData){v+='&adt='+en(o.addData);}if(o.uniqid){v+='&uniqid='+en(o.uniqid);}if(o.clickIds && o.clickIds.length > 0){v+='&cli='+en(o.clickIds.join(','));}else if(o.clickId){v+='&cli='+en(o.clickId);}if(o.custSurv){v+='&csr='+en(o.custSurv);}if(o.siteId){v+='&sid='+en(o.siteId);}var s=(screen.width)?screen.width:"0";
s+="X";s+=(screen.height)?screen.height:"0";s+="X";s+=(screen.colorDepth)?screen.colorDepth:"0";v+='&scr='+s;v+='&nck=';v+=(navigator.cookieEnabled)?navigator.cookieEnabled:"null";v+='&njv=';v+=(navigator.javaEnabled())?navigator.javaEnabled():"null";if (o.locationHref){v+='&hrf='+en(o.locationHref);}if(o.gdpr){v+='&gdpr='+en(o.gdpr);}if(o.gdprConsent){v+='&gdpr_consent='+en(o.gdprConsent);}v+='&ver='+en('4.25');return(v);};
itsStartConv=function(o){var s=pr+'//'+o.trcDomain+'/ts/'+o.advId+'/tsa?typ=f&'+ap(o);fr(s);};itsStartConv(itsConv);

var a = document.createElement('script'); a.type = 'text/javascript'; a.async = true; a.src = 'https://'+itsConv.trcDomain+'/scripts/ts/'+itsConv.advId+'contA.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
{/literal}
</script>


<noscript>
{literal}
<!-- ------------ SET THE PARAMETERS FOR NOSCRIPT PART HERE ---------------- -->
    <!-- The parameter values have to be set in realtime. Every special characters have to be URL-encoded. -->
	<!-- gdpr: '0' GDPR does not apply; '1' GDPR applies -->
	<!-- gdpr_consent: Only meaningful if gdpr=1. URL-safe base64-encoded TC string obtained from the CMP JS API or OpenRTB. -->
	<!--               Must contain consent for Vendor ID of advertiser or for 871 as default  -->
    <img src="https://atlas.r.akipam.com/ts/i5036838/tsa?typ=i&tst=!!TIME_STAMP!!&trc=default&ctg=sale&sid=check-out&cid=!!convId!!&orv=!!ordValue!!&orc=EUR&hrf=!!HTTP_href!!&gdpr=!!gdpr!!&gdpr_consent=!!gdpr_consent!!&ver=4.25&cli=!!clickId!!" width="1" height="1" border="0" style="display:none;">
{/literal}
</noscript>
<!-- END BELBOON - PERFORMANCE MARKETING NETWORK TRACKING CODE -->


{/block}
