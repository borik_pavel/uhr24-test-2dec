{extends file="{$parent_template_path}/checkout/step3_shipping_options.tpl"}



                                                            {block name='checkout-step3-shipping-options-shipping-option-price'}
                                                                {col cols=12 sm=4 class='font-weight-bold-util'}
																
																{if $versandart->angezeigterName|trans!=Abholung }
                                                                    {$versandart->cPreisLocalized}
                                                                    {if !empty($versandart->Zuschlag->fZuschlag)}
                                                                        <div>
                                                                            <small>
                                                                                ({$versandart->Zuschlag->angezeigterName|trans} +{$versandart->Zuschlag->cPreisLocalized})
                                                                            </small>
                                                                        </div>
                                                                    {/if}
																{/if}	
																	
                                                                {/col}
                                                            {/block}